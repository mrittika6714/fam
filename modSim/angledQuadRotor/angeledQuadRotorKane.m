function [t,VAR,Output] = angeledQuadRotorKane
%===========================================================================
% File: angeledQuadRotorKane.m created Nov 11 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
eventDetectedByIntegratorTerminate1OrContinue0 = [];
F1=0; F2=0; F3=0; F4=0; qBxDt=0; qByDt=0; qBzDt=0; wBxDt=0; wByDt=0; wBzDt=0; xBDDt=0; yBDDt=0; zBDDt=0; T1=0; T2=0; T3=0; T4=0;
BCOM = zeros( 3, 1 );


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
bDrag                           =  1.5E-09;                % UNITS               Constant
bThrust                         =  6.11E-08;               % UNITS               Constant
g                               =  9.8;                    % m/s^2               Constant
IBxx                            =  1;                      % kg*m^2              Constant
IBxy                            =  0;                      % UNITS               Constant
IByy                            =  1;                      % kg*m^2              Constant
IByz                            =  0;                      % UNITS               Constant
IBzx                            =  0;                      % UNITS               Constant
IBzz                            =  1;                      % kg*m^2              Constant
lA                              =  0.18;                   % UNITS               Constant
mB                              =  1;                      % kg                  Constant
qP1x                            =  0;                      % UNITS               Constant
qP1y                            =  0;                      % UNITS               Constant
qP2x                            =  0;                      % UNITS               Constant
qP2y                            =  0;                      % UNITS               Constant
qP3x                            =  0;                      % UNITS               Constant
qP3y                            =  0;                      % UNITS               Constant
qP4x                            =  0;                      % UNITS               Constant
qP4y                            =  0;                      % UNITS               Constant

qBx                             =  0;                      % deg                 Initial Value
qBy                             =  0;                      % deg                 Initial Value
qBz                             =  0;                      % deg                 Initial Value
wBx                             =  0;                      % UNITS               Initial Value
wBy                             =  0;                      % UNITS               Initial Value
wBz                             =  0;                      % UNITS               Initial Value
xB                              =  0;                      % UNITS               Initial Value
yB                              =  0;                      % UNITS               Initial Value
zB                              =  0;                      % UNITS               Initial Value
xBDt                            =  0;                      % UNITS               Initial Value
yBDt                            =  0;                      % UNITS               Initial Value
zBDt                            =  0;                      % UNITS               Initial Value

tInitial                        =  0.0;                    % second              Initial Time
tFinal                          =  10;                     % sec                 Final Time
tStep                           =  0.1;                    % sec                 Integration Step
printIntScreen                  =  1;                      % 0 or +integer       0 is NO screen output
printIntFile                    =  1;                      % 0 or +integer       0 is NO file   output
absError                        =  1.0E-08;                %                     Absolute Error
relError                        =  1.0E-08;                %                     Relative Error
%-------------------------------+--------------------------+-------------------+-----------------

% Unit conversions
DEGtoRAD = pi / 180.0;
RADtoDEG = 180.0 / pi;
qBx = qBx * DEGtoRAD;
qBy = qBy * DEGtoRAD;
qBz = qBz * DEGtoRAD;

% Evaluate constants
F1 = 0;
F2 = 0;
F3 = 0;
F4 = 0;
T1 = bDrag*F1/bThrust;
T2 = -bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = -bDrag*F4/bThrust;


VAR = SetMatrixFromNamedQuantities;
[t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile );
OutputToScreenOrFile( [], 0, 0 );   % Close output files.
Output = Output(1:13);               % Truncate Output matrix for proper return size.


%===========================================================================
function sys = mdlDerivatives( t, VAR, uSimulink )
%===========================================================================
SetNamedQuantitiesFromMatrix( VAR );
qBxDt = wBx*cos(qBy) + wBz*sin(qBy);
qByDt = wBy + tan(qBx)*(wBx*sin(qBy)-wBz*cos(qBy));
qBzDt = -(wBx*sin(qBy)-wBz*cos(qBy))/cos(qBx);

COEF = zeros( 6, 6 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = mB;
COEF(4,4) = IBxx;
COEF(4,5) = IBxy;
COEF(4,6) = IBzx;
COEF(5,4) = IBxy;
COEF(5,5) = IByy;
COEF(5,6) = IByz;
COEF(6,4) = IBzx;
COEF(6,5) = IByz;
COEF(6,6) = IBzz;
RHS = zeros( 1, 6 );
RHS(1) = F1*(sin(qP1x)*cos(qP1y)*sin(qBz)*cos(qBx)+sin(qP1y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP1x)*cos(qP1y)*(  ...
sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))) + F2*(sin(qP2x)*cos(qP2y)*sin(qBz)*cos(qBx)+sin(qP2y)*(cos(qBy)*cos(qBz)-sin(qBx)*  ...
sin(qBy)*sin(qBz))+cos(qP2x)*cos(qP2y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))) + F3*(sin(qP3x)*cos(qP3y)*sin(qBz)*cos(qBx)+  ...
sin(qP3y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP3x)*cos(qP3y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))  ...
+ F4*(sin(qP4x)*cos(qP4y)*sin(qBz)*cos(qBx)+sin(qP4y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP4x)*cos(qP4y)*(sin(qBy)*  ...
cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)));
RHS(2) = -F1*(sin(qP1x)*cos(qP1y)*cos(qBx)*cos(qBz)-sin(qP1y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP1x)*cos(qP1y)*(  ...
sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))) - F2*(sin(qP2x)*cos(qP2y)*cos(qBx)*cos(qBz)-sin(qP2y)*(sin(qBz)*cos(qBy)+sin(qBx)*  ...
sin(qBy)*cos(qBz))-cos(qP2x)*cos(qP2y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))) - F3*(sin(qP3x)*cos(qP3y)*cos(qBx)*cos(qBz)-  ...
sin(qP3y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP3x)*cos(qP3y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))  ...
- F4*(sin(qP4x)*cos(qP4y)*cos(qBx)*cos(qBz)-sin(qP4y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP4x)*cos(qP4y)*(sin(qBy)*  ...
sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)));
RHS(3) = -g*mB - F1*(sin(qP1x)*cos(qP1y)*sin(qBx)+sin(qP1y)*sin(qBy)*cos(qBx)-cos(qP1x)*cos(qP1y)*cos(qBx)*cos(qBy)) - F2*(sin(qP2x)*  ...
cos(qP2y)*sin(qBx)+sin(qP2y)*sin(qBy)*cos(qBx)-cos(qP2x)*cos(qP2y)*cos(qBx)*cos(qBy)) - F3*(sin(qP3x)*cos(qP3y)*sin(qBx)+sin(qP3y)*  ...
sin(qBy)*cos(qBx)-cos(qP3x)*cos(qP3y)*cos(qBx)*cos(qBy)) - F4*(sin(qP4x)*cos(qP4y)*sin(qBx)+sin(qP4y)*sin(qBy)*cos(qBx)-cos(qP4x)*  ...
cos(qP4y)*cos(qBx)*cos(qBy));
RHS(4) = lA*(cos(qP2x)*cos(qP2y)*F2-cos(qP4x)*cos(qP4y)*F4) + wBz*(IBxy*wBx+IByy*wBy+IByz*wBz) - wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz);
RHS(5) = wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz) - lA*(cos(qP1x)*cos(qP1y)*F1-cos(qP3x)*cos(qP3y)*F3) - wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz);
RHS(6) = lA*sin(qP4y)*F4 + lA*sin(qP3x)*cos(qP3y)*F3 + T1 + T2 + T3 + T4 + wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz) - lA*sin(qP2y)*F2  ...
- lA*sin(qP1x)*cos(qP1y)*F1 - wBx*(IBxy*wBx+IByy*wBy+IByz*wBz);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
zBDDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);

sys = transpose( SetMatrixOfDerivativesPriorToIntegrationStep );
end



%===========================================================================
function VAR = SetMatrixFromNamedQuantities
%===========================================================================
VAR = zeros( 1, 12 );
VAR(1) = qBx;
VAR(2) = qBy;
VAR(3) = qBz;
VAR(4) = wBx;
VAR(5) = wBy;
VAR(6) = wBz;
VAR(7) = xB;
VAR(8) = yB;
VAR(9) = zB;
VAR(10) = xBDt;
VAR(11) = yBDt;
VAR(12) = zBDt;
end


%===========================================================================
function SetNamedQuantitiesFromMatrix( VAR )
%===========================================================================
qBx = VAR(1);
qBy = VAR(2);
qBz = VAR(3);
wBx = VAR(4);
wBy = VAR(5);
wBz = VAR(6);
xB = VAR(7);
yB = VAR(8);
zB = VAR(9);
xBDt = VAR(10);
yBDt = VAR(11);
zBDt = VAR(12);
end


%===========================================================================
function VARp = SetMatrixOfDerivativesPriorToIntegrationStep
%===========================================================================
VARp = zeros( 1, 12 );
VARp(1) = qBxDt;
VARp(2) = qByDt;
VARp(3) = qBzDt;
VARp(4) = wBxDt;
VARp(5) = wByDt;
VARp(6) = wBzDt;
VARp(7) = xBDt;
VARp(8) = yBDt;
VARp(9) = zBDt;
VARp(10) = xBDDt;
VARp(11) = yBDDt;
VARp(12) = zBDDt;
end



%===========================================================================
function Output = mdlOutputs( t, VAR, uSimulink )
%===========================================================================
Output = zeros( 1, 14 );
Output(1) = t;
Output(2) = qBx*RADtoDEG;
Output(3) = qBy*RADtoDEG;
Output(4) = qBz*RADtoDEG;
Output(5) = wBx;
Output(6) = wBy;
Output(7) = wBz;
Output(8) = xB;
Output(9) = yB;
Output(10) = zB;
Output(11) = xBDt;
Output(12) = yBDt;
Output(13) = zBDt;

Output(14) = xB;

BCOM(1) = xB;
BCOM(2) = yB;
BCOM(3) = zB;

end


%===========================================================================
function OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile )
%===========================================================================
persistent FileIdentifier hasHeaderInformationBeenWritten;

if( isempty(Output) ),
   if( ~isempty(FileIdentifier) ),
      fclose( FileIdentifier(1) );
      clear FileIdentifier;
      fprintf( 1, '\n Output is in the file angeledQuadRotorKane.1\n' );
      fprintf( 1, '\n Note: To automate plotting, issue the command OutputPlot in MotionGenesis.\n' );
      fprintf( 1, '\n To load and plot columns 1 and 2 with a solid line and columns 1 and 3 with a dashed line, enter:\n' );
      fprintf( 1, '    someName = load( ''angeledQuadRotorKane.1'' );\n' );
      fprintf( 1, '    plot( someName(:,1), someName(:,2), ''-'', someName(:,1), someName(:,3), ''--'' )\n\n' );
   end
   clear hasHeaderInformationBeenWritten;
   return;
end

if( isempty(hasHeaderInformationBeenWritten) ),
   if( shouldPrintToScreen ),
      fprintf( 1,                '%%       t             qBx            qBy            qBz            wBx            wBy            wBz            xB             yB             zB             xB''            yB''            zB''\n' );
      fprintf( 1,                '%%   (second)         (deg)          (deg)          (deg)         (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)\n\n' );
   end
   if( shouldPrintToFile && isempty(FileIdentifier) ),
      FileIdentifier(1) = fopen('angeledQuadRotorKane.1', 'wt');   if( FileIdentifier(1) == -1 ), error('Error: unable to open file angeledQuadRotorKane.1'); end
      fprintf(FileIdentifier(1), '%% FILE: angeledQuadRotorKane.1\n%%\n' );
      fprintf(FileIdentifier(1), '%%       t             qBx            qBy            qBz            wBx            wBy            wBz            xB             yB             zB             xB''            yB''            zB''\n' );
      fprintf(FileIdentifier(1), '%%   (second)         (deg)          (deg)          (deg)         (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)\n\n' );
   end
   hasHeaderInformationBeenWritten = 1;
end

if( shouldPrintToScreen ), WriteNumericalData( 1,                 Output(1:13) );  end
if( shouldPrintToFile ),   WriteNumericalData( FileIdentifier(1), Output(1:13) );  end
end


%===========================================================================
function WriteNumericalData( fileIdentifier, Output )
%===========================================================================
numberOfOutputQuantities = length( Output );
if( numberOfOutputQuantities > 0 ),
   for( i = 1 : numberOfOutputQuantities ),
      fprintf( fileIdentifier, ' %- 14.6E', Output(i) );
   end
   fprintf( fileIdentifier, '\n' );
end
end



%===========================================================================
function [functionsToEvaluateForEvent, eventTerminatesIntegration1Otherwise0ToContinue, eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1] = EventDetection( t, VAR, uSimulink )
%===========================================================================
% Detects when designated functions are zero or cross zero with positive or negative slope.
% Step 1: Uncomment call to mdlDerivatives and mdlOutputs.
% Step 2: Change functionsToEvaluateForEvent,                      e.g., change  []  to  [t - 5.67]  to stop at t = 5.67.
% Step 3: Change eventTerminatesIntegration1Otherwise0ToContinue,  e.g., change  []  to  [1]  to stop integrating.
% Step 4: Change eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1,  e.g., change  []  to  [1].
% Step 5: Possibly modify function EventDetectedByIntegrator (if eventTerminatesIntegration1Otherwise0ToContinue is 0).
%---------------------------------------------------------------------------
% mdlDerivatives( t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
% mdlOutputs(     t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
functionsToEvaluateForEvent = [];
eventTerminatesIntegration1Otherwise0ToContinue = [];
eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1 = [];
eventDetectedByIntegratorTerminate1OrContinue0 = eventTerminatesIntegration1Otherwise0ToContinue;
end


%===========================================================================
function [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents )
%===========================================================================
isIntegrationFinished = eventDetectedByIntegratorTerminate1OrContinue0( nIndexOfEvents );
if( ~isIntegrationFinished ),
   SetNamedQuantitiesFromMatrix( VAR );
%  Put code here to modify how integration continues.
   VAR = SetMatrixFromNamedQuantities;
end
end



%===========================================================================
function [t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile )
%===========================================================================
OdeMatlabOptions = odeset( 'RelTol',relError, 'AbsTol',absError, 'MaxStep',tStep, 'Events',@EventDetection );
t = tInitial;                 epsilonT = 0.001*tStep;                   tFinalMinusEpsilonT = tFinal - epsilonT;
printCounterScreen = 0;       integrateForward = tFinal >= tInitial;    tAtEndOfIntegrationStep = t + tStep;
printCounterFile   = 0;       isIntegrationFinished = 0;
mdlDerivatives( t, VAR, 0 );
while 1,
   if( (integrateForward && t >= tFinalMinusEpsilonT) || (~integrateForward && t <= tFinalMinusEpsilonT) ), isIntegrationFinished = 1;  end
   shouldPrintToScreen = printIntScreen && ( isIntegrationFinished || printCounterScreen <= 0.01 );
   shouldPrintToFile   = printIntFile   && ( isIntegrationFinished || printCounterFile   <= 0.01 );
   if( isIntegrationFinished || shouldPrintToScreen || shouldPrintToFile ),
      Output = mdlOutputs( t, VAR, 0 );
      OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile );
      if( isIntegrationFinished ), break;  end
      if( shouldPrintToScreen ), printCounterScreen = printIntScreen;  end
      if( shouldPrintToFile ),   printCounterFile   = printIntFile;    end
   end
   [TimeOdeArray, VarOdeArray, timeEventOccurredInIntegrationStep, nStatesArraysAtEvent, nIndexOfEvents] = ode45( @mdlDerivatives, [t tAtEndOfIntegrationStep], VAR, OdeMatlabOptions, 0 );
   if( isempty(timeEventOccurredInIntegrationStep) ),
      lastIndex = length( TimeOdeArray );
      t = TimeOdeArray( lastIndex );
      VAR = VarOdeArray( lastIndex, : );
      printCounterScreen = printCounterScreen - 1;
      printCounterFile   = printCounterFile   - 1;
      if( abs(tAtEndOfIntegrationStep - t) >= abs(epsilonT) ), warning('numerical integration failed'); break;  end
      tAtEndOfIntegrationStep = t + tStep;
      if( (integrateForward && tAtEndOfIntegrationStep > tFinal) || (~integrateForward && tAtEndOfIntegrationStep < tFinal) ) tAtEndOfIntegrationStep = tFinal;  end
   else
      t = timeEventOccurredInIntegrationStep( 1 );    % time  at firstEvent = 1 during this integration step.
      VAR = nStatesArraysAtEvent( 1, : );             % state at firstEvent = 1 during this integration step.
      printCounterScreen = 0;
      printCounterFile   = 0;
      [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents(1) );
   end
end
end


%=============================================
end    % End of function angeledQuadRotorKane
%=============================================
