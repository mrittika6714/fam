%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function quadRotorThrustAnimation(t, states,params)

% Unpack positions
xB= states(:,1);
yB= states(:,2);
zB= states(:,3);
qBx= states(:,4);
qBy= states(:,5);
qBz= states(:,6);
setPoint = params.uDesired;
lA = params.lA;
bHeight = lA/2;
numPoints = length(t);

[bodyVertices, bodyFaces] =  plotCube([lA, lA, bHeight],[xB(1), yB(1), zB(1)], [qBz(1), qBy(1), qBx(1)]);
myVideo = VideoWriter('quadRotorThrustAnimation');
myVideo.FrameRate = 100;
open(myVideo)
animationFigure = figure('units','normalized','outerposition',[0 0 1 1]);
setPointPlot = plot3(setPoint(1),setPoint(2),setPoint(3), 'r.','markersize',25);
%view(0,90)
hold on
bodyPlot = patch('Vertices',bodyVertices,'Faces',bodyFaces, 'FaceColor', [0.5, 0.5, 0.5]);
pathPlot = plot3(xB(1), yB(1), zB(1), 'k--', 'linewidth',1);
xMin = min( xB) - lA*2.5;
xMax = max( xB) + lA*2.5;
yMin = min( yB) - lA*2.5;
yMax = max( yB) + lA*2.5;
zMin = min( zB) - lA*2.5;
zMax = max( zB) + lA*2.5;
axis([xMin xMax yMin yMax zMin zMax])
xlabel('x Position [m]');
ylabel('y Position [m]');
zlabel('z Position [m]');
title('Quad Rotor Simulation','fontweight', 'bold')
legend('Set Point', 'VehicleBody')
for i =1:numPoints
    setPointPlot.XData = setPoint(1);
    setPointPlot.YData = setPoint(2);
    setPointPlot.ZData = setPoint(3);
    %bodyPlot.XData = xB(i);
    %bodyPlot.YData = yB(i);
    %bodyPlot.ZData = zB(i);
    
    [ bodyPlot.Vertices, bodyPlot.Faces] =  plotCube([lA, lA, bHeight],[xB(i), yB(i), zB(i)], [qBz(i), qBy(i), qBx(i)]);
    
    
    pathPlot.XData = xB(1:i);
    pathPlot.YData = yB(1:i);
    pathPlot.ZData = zB(1:i);
    drawnow
    frame = getframe(animationFigure);
    writeVideo(myVideo,frame);
end
close(myVideo)



end