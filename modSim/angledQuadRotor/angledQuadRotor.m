%% Setup workspace and define System Paramaters
clc; clear; close all
animate = 1;
tStep = 0.01;
tFinal = 10;
tSpan = 0:tStep:tFinal;

params.IB(1,1) = 0.0117; %kg*m^2% ;
params.IB(2,2)= 0.0117; %kg*m^2;
params.IB(3,3) = 0.00234; %kg*m^2
params.IB(1,2) = 0;%-24.31 * gcmsq2kgmsq;
params.IB(2,3) = 0;%-9.95 * gcmsq2kgmsq;
params.IB(3,1) = 0;%-7.89 * gcmsq2kgmsq;
params.mB = 0.478;
params.lA = 0.18;
params.bThrust = 6.11*10^-8;
params.bDrag = 1.5*10^-9;
params.qP1x = 45*pi/180;
params.qP1y = 0*pi/180;
params.qP2x = 0*pi/180;
params.qP2y = -45*pi/180;
params.qP3x = -45*pi/180;
params.qP3y = 0*pi/180;
params.qP4x = 0*pi/180;
params.qP4y = 45*pi/180;

u0 = [0; 0; 5; 0; 0; 0; 0; 0; 0; 0; 0; 0];
uDesired = [ 1; 2; 5; 0; 0; 0; 0; 0; 0; 0; 0; 0];
numInputs = 4;

%% Uncontrolled Dynamics 
u = @(states) zeros(numInputs,size(uDesired,1))*states;
odeFunc = @(t,states) angledQuadRotorDynamics(t, states, params, u);
[tUncon, xUncon] = ode45(odeFunc, tSpan, u0);

%% Linear Controller 

load('Klqr.mat')
load('uEq.mat')
u = @(states) -Klqr*(states-uDesired) + uEqVal;
odeFunc = @(t,states) angledQuadRotorDynamics(t, states, params, u);
[tControlled, xControlled] = ode45(odeFunc, tSpan, u0);

numPoints = length(tControlled);
propRates = zeros(numPoints, 4);
for i = 1 :  numPoints
    [~,propRates(i,:) ] = angledQuadRotorDynamics(tControlled(i), xControlled(i,:)', params, u);
end
drawStatesControlled = xControlled(:,:);

%% Plot Responses
figure
hold on 
for i = 1: 6
  subplot(6,1,i)
  %plot(tUncon, xUncon(:,i), 'k', 'linewidth', 2)
  hold on
  plot(tControlled, xControlled(:,i), 'r--', 'linewidth', 2)
%legend('Uncontrolled', 'Controlled')
end

figure 
plot(tControlled, propRates)
title('Propellor Rate Time Histories')
%% Animate Responses
if animate ==1
params.uDesired = uDesired;    
angledQuadRotorAnimation(tControlled,drawStatesControlled,params)
end


