function params = dubinsCar_params(controllerType)

params.mB = 1;
params.IBzz = 1;

params.g = 9.8;

params.markerSize = 35;

params.controller.kp = 10;
params.controller.kd = 10;
params.controller.DOF = 2;

params.trajectoryParams = dubinsCar_trajectoryParams(controllerType);


end