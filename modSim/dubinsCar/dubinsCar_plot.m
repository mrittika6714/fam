function  dubinsCar_plot(plotData, params, savePlots)

numTrajectories = params.trajectoryParams.numTrajectories;
styleVec ={'k-','r--','y-' };

% if strcmp(params.controller.controllerType, 'FF_Task') || strcmp(params.controller.controllerType, 'FF_Task_PID')|| strcmp(params.controller.controllerType, 'FF_Null')
%    taskPos = plotData.additionalOutputs(:,13:15);
%    taskVel = plotData.additionalOutputs(:,16:18);
%    taskStates = [taskPos, taskVel];
% else
%    
% end
desiredTrajectory = [plotData.posDes(:,1:2)];
stateError = plotData.states(:,1:2) - desiredTrajectory;
%taskError = taskStates - desiredTrajectory ;
trajectories = figure;
hold on
for i = 1:2
plot(plotData.t, plotData.states(:,i),styleVec{1})
hold on
plot(plotData.t, desiredTrajectory(:,i), styleVec{2})
title(' State Trajectory Time Histories');
xlabel('Time [s]');
ylabel('y [units]');
legend( 'Actual','Desired')

end

error = figure;
plot(plotData.t, stateError)
title(' State Error');
if savePlots
  saveas(trajectories, fullfile(params.resultsFolder,['trajectories','.png'] ));
end

end