function trajectoryParams = dubinsCar_trajectoryParams(controllerType)

tFinal = 30;


    states0 = [-1; 2; -pi/2; 0]; %load('ICs.mat')
    %initialOffset = [pi/9; 0; -pi/12; 0; 0.1; -0.2];
    initialOffset = zeros(6,1);
    % xB trajectory 
    xBTrajectory = trajectory;
    xBTrajectory.type = 'minJerk';
    xBTrajectory.wayPoints = [ states0(1)+initialOffset(1); 5];
    xBTrajectory.velocities = [initialOffset(4); 0 ];
    xBTrajectory.tVec = [0; tFinal];
    xBTrajectory.accelerations = [0; 0];

    % yB trajectory 
    yBTrajectory = trajectory;
    yBTrajectory.type = 'minJerk';
    yBTrajectory.wayPoints = [ states0(2)+initialOffset(2); 10];
    yBTrajectory.velocities = [initialOffset(5); 0 ];
    yBTrajectory.tVec = [0; tFinal];
    yBTrajectory.accelerations = [0; 0];

    % qBz trajectory 
    qBZTrajectory = trajectory;
    qBZTrajectory.type = 'minJerk';
    qBZTrajectory.wayPoints = [ states0(3)++initialOffset(3); pi/4];
    qBZTrajectory.velocities = [+initialOffset(6); 0 ];
    qBZTrajectory.tVec = [0; tFinal];
    qBZTrajectory.accelerations = [0; 0];

    trajectoryParams.numTrajectories = 3;
    trajectoryParams.ICs = states0;
    trajectoryParams.trajectories{1} = xBTrajectory;
    trajectoryParams.trajectories{2} = yBTrajectory;
    trajectoryParams.trajectories{3} = qBZTrajectory;

%end
end