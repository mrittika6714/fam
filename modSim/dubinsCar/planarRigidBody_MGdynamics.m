function integrationOutputs = planarRigidBody_MGdynamics( g, IBzz, mB, FBx, FBy, TBz, qBz, qBzDt, xBDt, yBDt )
if( nargin ~= 10 ) error( 'planarRigidBody_MGdynamics expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarRigidBody_MGdynamics.m created Sep 02 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
integrationOutputs = zeros( 1, 6 );





%===========================================================================
COEF = zeros( 3, 3 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = IBzz;
RHS = zeros( 1, 3 );
RHS(1) = FBx*cos(qBz) - FBy*sin(qBz);
RHS(2) = FBx*sin(qBz) + FBy*cos(qBz) - g*mB;
RHS(3) = TBz;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
qBzDDt = SolutionToAlgebraicEquations(3);



%===========================================================================
Output = [];

integrationOutputs(1,1) = xBDt;
integrationOutputs(1,2) = yBDt;
integrationOutputs(1,3) = qBzDt;
integrationOutputs(1,4) = xBDDt;
integrationOutputs(1,5) = yBDDt;
integrationOutputs(1,6) = qBzDDt;


%===================================================
end    % End of function planarRigidBody_MGdynamics
%===================================================
