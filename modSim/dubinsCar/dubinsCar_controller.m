function control = dubinsCar_controller(t, states, trajectories, trajectoryFunc, params)

% Unpack params
mB = params.mB;
IBzz = params.IBzz;
g = params.g;

%Unpack States
xB = states(1);
yB = states(2);
qBz = states(3);
z = states(4);


controllerType = params.controller.controllerType;

%% Get Desired Trajectory
% Unpack Desired Trajectory 
if (strcmp(controllerType, 'FF_Task') || strcmp(controllerType, 'FF_Task_PID') || strcmp(controllerType, 'FF_Null'))
    [posDes, vDes, aDes, rDot3Des , rDot4Des] = trajectoryFunc(trajectories, t);
    xDes = posDes(1);
    xDotDes = vDes(1);
    xDDotDes = aDes(1);
    xDot3Des = rDot3Des(1);
    xDot4Des = rDot4Des(1);
    yDes = posDes(2);
    yDotDes = vDes(2);
    yDDotDes = aDes(2);
    yDot3Des = rDot3Des(2);
    yDot4Des = rDot4Des(2);
    %qDes = atan(-xDDotDes/(g+yDDotDes))
    qDes = atan2((g+yDDotDes), -xDDotDes)
    qDotDes = cos(qDes)^2*(xDDotDes*yDot3Des-xDot3Des*(g+yDDotDes))/(g+yDDotDes)^2;
    qDDotDes =  -cos(qDes)^2*(2*sin(qDes)*qDotDes^2/cos(qDes)^3+(2*yDot3Des*(xDDotDes*yDot3Des-xDot3Des*(g+yDDotDes))-(g+yDDotDes)*(xDDotDes*yDot4Des-xDot4Des*(g+yDDotDes)))/(g+yDDotDes)^3);
    
%     qBxCalc = atan2((g+zDDotDes),-yDDotDes); 
%     qBxDotCalc = cos(qBxCalc)^2*(yDDotDes*zDot3Des-yDot3Des*(g+zDDotDes))/(g+zDDotDes)^2;
%     qBxDDotCalc =   -cos(qBxCalc)^2*(2*sin(qBxCalc)*qBxDotCalc^2/cos(qBxCalc)^3-(yDDotDes*zDot4Des*(g+zDDotDes)+2*yDot3Des*zDot3Des*(g+zDDotDes)-2*yDDotDes*zDot3Des^2-yDot4Des*(g+zDDotDes)^2)/(g+zDDotDes)^3);
%     
%     qDes =  posDes(3);
%     qDotDes= vDes(3); 
%     qDDotDes = aDes(3);
%     [taskPos,taskVel] = RzRzRzManip_ForwardKinematics( q1, q2, q3, q1Dot, q2Dot, q3Dot, Len1, Len2, Len3, g );
%     error = posDes' - taskPos;
%     errorDot = vDes' - taskVel;
posDes = [xDes, yDes, qDes];
vDes = [xDotDes, yDotDes, qDotDes];
aDes = [xDDotDes, yDDotDes, qDDotDes];
%[posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
error = posDes' - states(1:3);
errorDot = vDes' - states(4:6);
else
    [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
    %Calculate errors
    
%     error = posDes' - states(1:3);
%     errorDot = vDes' - states(4:6);

end
    
%% Controller
if strcmp(controllerType, 'none')
    control = zeros(3, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;
   
elseif strcmp(controllerType, 'dynamicStateFB')
  %Unpack States
  x1 = states(1);
  x2 = states(2);
  x3 = states(3);
  z1 = states(4);
  w = aDes(1:2)'; 
  B = [cos(x3), - z1*sin(x3); sin(x3), z1*cos(x3)];
  u = pinv(B)*w;
  
  control = u;
  
else  
  error('Controller Type not handled');
end 

end