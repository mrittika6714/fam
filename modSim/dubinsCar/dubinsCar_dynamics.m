function [statesDot, addionalOutputs] = dubinsCar_dynamics(t, states, params, controllerFunc)
u = controllerFunc(t, states);


% Unpack params

%Unpack States
x1 = states(1);
x2 = states(2);
x3 = states(3);
z1 = states(4);

% dynamics (including dynamically extended states) 
dxl = cos(x3)*z1;
dx2 = sin(x3)*z1; 
dx3 = u(2);
dx4 = u(1);
statesDot = [dxl; dx2; dx3; dx4];


addionalOutputs = [1]';


end