function [statesDot, addionalOutputs] = planarRigidBody_dynamics(t, states, params, controllerFunc)
u = controllerFunc(t, states);

% Unpack Inputs
FBx = u(1);
FBy = u(2);
TBz = u(3);

% Unpack params
mB = params.mB;
IBzz = params.IBzz;
g = params.g;

%Unpack States
xB = states(1);
yB = states(2);
qBz = states(3);
xBDt = states(4);
yBDt = states(5);
qBzDt = states(6);


integrationOutputs = planarRigidBody_MGdynamics( g, IBzz, mB, FBx, FBy, ...
  TBz, qBz, qBzDt, xBDt, yBDt );

% [rL1o_No_N, rL2o_No_N, rL3o_No_N, rEE_No_N ] = ...
%           RzRzRzManip_MGanimationOutputs(Len1, Len2, Len3, q1, q2, q3 );
% [taskPos,taskVel] = RzRzRzManip_ForwardKinematics( q1, q2, q3, q1Dt, ...
%     q2Dt, q3Dt, Len1, Len2, Len3, g );
addionalOutputs = [1]';

statesDot = integrationOutputs';
end