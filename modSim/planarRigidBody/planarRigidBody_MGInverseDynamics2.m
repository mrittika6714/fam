function [SolutionToAlgebraicEquations] = planarRigidBody_MGInverseDynamics2( g, IBzz, mB, xBDDt, yBDDt, qBz, xBDDDt, xBDDDDt )
if( nargin ~= 8 ) error( 'planarRigidBody_MGInverseDynamics2 expects 8 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarRigidBody_MGInverseDynamics2.m created Sep 03 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================
qBzDt = xBDDDt/(tan(qBz)*(1+1/tan(qBz)^2)*xBDDt);
qBzDDt = (cos(qBz)*xBDDDDt/sin(qBz)+2*qBzDt*(cos(qBz)*qBzDt*xBDDt/sin(qBz)^3-(1+1/tan(qBz)^2)*xBDDDt))/((1+1/tan(qBz)^2)*xBDDt);
FBy = -mB*xBDDt/sin(qBz);

COEF = zeros( 3, 3 );
COEF(1,1) = -cos(qBz);
COEF(1,2) = sin(qBz);
COEF(2,1) = -sin(qBz);
COEF(2,2) = -cos(qBz);
COEF(3,3) = -1;
RHS = zeros( 1, 3 );
RHS(1) = -mB*xBDDt;
yBDDt = -g - cos(qBz)*xBDDt/sin(qBz);
RHS(2) = -mB*(g+yBDDt);
RHS(3) = -IBzz*qBzDDt;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
FBx = SolutionToAlgebraicEquations(1);
FBy = SolutionToAlgebraicEquations(2);
TBz = SolutionToAlgebraicEquations(3);



%===========================================================
end    % End of function planarRigidBody_MGInverseDynamics2
%===========================================================
