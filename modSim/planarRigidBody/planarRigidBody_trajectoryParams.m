function trajectoryParams = planarRigidBody_trajectoryParams(controllerType)

tFinal = 30;

% if (strcmp(controllerType, 'FF_Task') || strcmp(controllerType, 'FF_Task_PID') || strcmp(controllerType, 'FF_Null'))
% 
%     states0 = [0; pi/8; 0; 0; 0; 0]; %load('ICs.mat')
%     %initialOffset = [pi/9; 0; -pi/12; 0; 0.1; -0.2];
%     %initialOffset = zeros(6,1);
%     % x trajectory 
%     xTrajectory = trajectory;
%     xTrajectory.type = 'minJerk';
%     xTrajectory.wayPoints = [ 1; 0; 1; 1];
%     xTrajectory.velocities = [0; 0; 0; 0 ];
%     xTrajectory.tVec = [0; tFinal/3; 2*tFinal/3; tFinal];
%     xTrajectory.accelerations = [0; 0; 0; 0];
% 
%     % y trajectory 
%     yTrajectory = trajectory;
%     yTrajectory.type = 'minJerk';
%     yTrajectory.wayPoints = [ 2; 2; 2; -0.25];
%     yTrajectory.velocities = [0; 0; 0; 0 ];
%     yTrajectory.tVec = [0; tFinal/3; 2*tFinal/3; tFinal];
%     yTrajectory.accelerations = [0; 0; 0; 0];
% 
%     % qTask trajectory 
%     qTrajectory = trajectory;
%     qTrajectory.type = 'minJerk';
%     qTrajectory.wayPoints = [ pi/2; pi/4; pi/4; pi/4];
%     qTrajectory.velocities = [0; 0; 0; 0 ];
%     qTrajectory.tVec = [0; tFinal/3; 2*tFinal/3; tFinal];
%     qTrajectory.accelerations = [0; 0; 0; 0];
% 
%     trajectoryParams.numTrajectories = 3;
%     trajectoryParams.ICs = states0;
%     trajectoryParams.trajectories{1} = xTrajectory;
%     trajectoryParams.trajectories{2} = yTrajectory;
%     trajectoryParams.trajectories{3} = qTrajectory;
%     
% else
    states0 = [0; 0; 0; 0; 0; 0; ]; %load('ICs.mat')
    %initialOffset = [pi/9; 0; -pi/12; 0; 0.1; -0.2];
    initialOffset = zeros(6,1);
    % xB trajectory 
    xBTrajectory = trajectory;
    xBTrajectory.type = 'minJerk';
    xBTrajectory.wayPoints = [ states0(1)+initialOffset(1); 5];
    xBTrajectory.velocities = [states0(4)+initialOffset(4); 0 ];
    xBTrajectory.tVec = [0; tFinal];
    xBTrajectory.accelerations = [0; 0];

    % yB trajectory 
    yBTrajectory = trajectory;
    yBTrajectory.type = 'minJerk';
    yBTrajectory.wayPoints = [ states0(2)+initialOffset(2); 10];
    yBTrajectory.velocities = [states0(5)++initialOffset(5); 0 ];
    yBTrajectory.tVec = [0; tFinal];
    yBTrajectory.accelerations = [0; 0];

    % qBz trajectory 
    qBZTrajectory = trajectory;
    qBZTrajectory.type = 'minJerk';
    qBZTrajectory.wayPoints = [ states0(3)++initialOffset(3); pi/4];
    qBZTrajectory.velocities = [states0(6)++initialOffset(6); 0 ];
    qBZTrajectory.tVec = [0; tFinal];
    qBZTrajectory.accelerations = [0; 0];

    trajectoryParams.numTrajectories = 3;
    trajectoryParams.ICs = states0;
    trajectoryParams.trajectories{1} = xBTrajectory;
    trajectoryParams.trajectories{2} = yBTrajectory;
    trajectoryParams.trajectories{3} = qBZTrajectory;

%end
end