function planarRigidBody_ForwardKinematics( qBz, xB, yB, qBzDt, xBDt, yBDt, qBzDDt, xBDDt, yBDDt )
if( nargin ~= 9 ) error( 'planarRigidBody_ForwardKinematics expects 9 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarRigidBody_ForwardKinematics.m created Sep 02 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
taskPos = zeros( 3, 1 );
taskVel = zeros( 3, 1 );
taskAccel = zeros( 3, 1 );





%===========================================================================


%===========================================================================
Output = [];

taskPos(1) = xB;
taskPos(2) = yB;
taskPos(3) = qBz;

taskVel(1) = xBDt;
taskVel(2) = yBDt;
taskVel(3) = qBzDt;

taskAccel(1) = xBDDt;
taskAccel(2) = yBDDt;
taskAccel(3) = qBzDDt;


%==========================================================
end    % End of function planarRigidBody_ForwardKinematics
%==========================================================
