function [NMat, MMat, GMat, CMat, J_N, JDot_N] = planarRigidBody_dynamicMatrices2( g, IBzz, mB, qBz )
if( nargin ~= 4 ) error( 'planarRigidBody_dynamicMatrices2 expects 4 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarRigidBody_dynamicMatrices2.m created Sep 03 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
NMat = zeros( 3, 2 );
MMat = zeros( 3, 3 );
GMat = zeros( 3, 1 );
CMat = zeros( 3, 1 );
J_N = zeros( 3, 3 );
JDot_N = zeros( 3, 3 );


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
FBx                             =  0.0;                    % UNITS               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================


%===========================================================================
Output = [];

NMat(1,1) = -sin(qBz);
NMat(1,2) = 0;
NMat(2,1) = cos(qBz);
NMat(2,2) = 0;
NMat(3,1) = 0;
NMat(3,2) = 1;

MMat(1,1) = mB;
MMat(1,2) = 0;
MMat(1,3) = 0;
MMat(2,1) = 0;
MMat(2,2) = mB;
MMat(2,3) = 0;
MMat(3,1) = 0;
MMat(3,2) = 0;
MMat(3,3) = IBzz;

GMat(1) = 0;
GMat(2) = g*mB;
GMat(3) = 0;

CMat(1) = -FBx*cos(qBz);
CMat(2) = -FBx*sin(qBz);
CMat(3) = 0;

J_N(1,1) = 1;
J_N(1,2) = 0;
J_N(1,3) = 0;
J_N(2,1) = 0;
J_N(2,2) = 1;
J_N(2,3) = 0;
J_N(3,1) = 0;
J_N(3,2) = 0;
J_N(3,3) = 1;

JDot_N(1,1) = 0;
JDot_N(1,2) = 0;
JDot_N(1,3) = 0;
JDot_N(2,1) = 0;
JDot_N(2,2) = 0;
JDot_N(2,3) = 0;
JDot_N(3,1) = 0;
JDot_N(3,2) = 0;
JDot_N(3,3) = 0;


%=========================================================
end    % End of function planarRigidBody_dynamicMatrices2
%=========================================================
