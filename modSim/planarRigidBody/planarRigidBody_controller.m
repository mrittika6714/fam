function control = planarRigidBody_controller(t, states, trajectories, trajectoryFunc, params)

% Unpack params
mB = params.mB;
IBzz = params.IBzz;
g = params.g;

%Unpack States
xB = states(1);
yB = states(2);
qBz = states(3);
xBDt = states(4);
yBDt = states(5);
qBzDt = states(6);

controllerType = params.controller.controllerType;

%% Get Desired Trajectory
% Unpack Desired Trajectory 
if (strcmp(controllerType, 'FF_Task') || strcmp(controllerType, 'FF_Task_PID') || strcmp(controllerType, 'FF_Null'))
    [posDes, vDes, aDes, rDot3Des , rDot4Des] = trajectoryFunc(trajectories, t);
    xDes = posDes(1);
    xDotDes = vDes(1);
    xDDotDes = aDes(1);
    xDot3Des = rDot3Des(1);
    xDot4Des = rDot4Des(1);
    yDes = posDes(2);
    yDotDes = vDes(2);
    yDDotDes = aDes(2);
    yDot3Des = rDot3Des(2);
    yDot4Des = rDot4Des(2);
    %qDes = atan(-xDDotDes/(g+yDDotDes))
    qDes = atan2((g+yDDotDes), -xDDotDes)
    qDotDes = cos(qDes)^2*(xDDotDes*yDot3Des-xDot3Des*(g+yDDotDes))/(g+yDDotDes)^2;
    qDDotDes =  -cos(qDes)^2*(2*sin(qDes)*qDotDes^2/cos(qDes)^3+(2*yDot3Des*(xDDotDes*yDot3Des-xDot3Des*(g+yDDotDes))-(g+yDDotDes)*(xDDotDes*yDot4Des-xDot4Des*(g+yDDotDes)))/(g+yDDotDes)^3);
    
%     qBxCalc = atan2((g+zDDotDes),-yDDotDes); 
%     qBxDotCalc = cos(qBxCalc)^2*(yDDotDes*zDot3Des-yDot3Des*(g+zDDotDes))/(g+zDDotDes)^2;
%     qBxDDotCalc =   -cos(qBxCalc)^2*(2*sin(qBxCalc)*qBxDotCalc^2/cos(qBxCalc)^3-(yDDotDes*zDot4Des*(g+zDDotDes)+2*yDot3Des*zDot3Des*(g+zDDotDes)-2*yDDotDes*zDot3Des^2-yDot4Des*(g+zDDotDes)^2)/(g+zDDotDes)^3);
%     
%     qDes =  posDes(3);
%     qDotDes= vDes(3); 
%     qDDotDes = aDes(3);
%     [taskPos,taskVel] = RzRzRzManip_ForwardKinematics( q1, q2, q3, q1Dot, q2Dot, q3Dot, Len1, Len2, Len3, g );
%     error = posDes' - taskPos;
%     errorDot = vDes' - taskVel;
posDes = [xDes, yDes, qDes];
vDes = [xDotDes, yDotDes, qDotDes];
aDes = [xDDotDes, yDDotDes, qDDotDes];
%[posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
error = posDes' - states(1:3);
errorDot = vDes' - states(4:6);
else
    [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
    %Calculate errors
    
    error = posDes' - states(1:3);
    errorDot = vDes' - states(4:6);

end
    
%% Controller
if strcmp(controllerType, 'none')
    control = zeros(3, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;
   
elseif strcmp(controllerType, 'FF_Joint')
     [NMat, MMat, GMat, CMat, J_N, JDot_N] = planarRigidBody_dynamicMatrices( g, IBzz, mB, qBz );
      %NMat(1, :) = zeros(1,3)
     control = pinv(NMat) * (MMat * aDes' + GMat + CMat); 
    %control = RzRzRzManip_MGInverseDynamics( q1, q2, q3, q1Dot, q2Dot, q3Dot, ...
     %    q1DDotDes, q2DDotDes, q3DDotDes, m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, ...
     %    Len2, Len3, g );
     
% elseif strcmp(controllerType, 'FF_Joint_PID')
%     kp = params.controller.kp;
%     kd = params.controller.kd;
%     a_x_Control = aDes' + kd*errorDot + kp * error; % control signal for acceleration in joint space
%     a_q_Control = pinv(J) * (a_x_Control - JDot * qDot );
%     q1DDot_Con = a_q_Control(1);
%     q2DDot_Con = a_q_Control(2);
%     q3DDot_Con = a_q_Control(3);
%      [NMat, MMat, GMat, VMat, J, JDot] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
%      control = pinv(NMat) * (MMat * a_q_Control + GMat + VMat);
%      %control = RzRzRzManip_MGInverseDynamics( q1, q2, q3, q1Dot, q2Dot, q3Dot, ...
%       %   q1DDot_Con, q2DDot_Con, q3DDot_Con, m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, ...
%        %  Len2, Len3, g );
  
elseif strcmp(controllerType, 'FF_Task')
    [NMat, MMat, GMat, CMat, J_N, JDot_N] = planarRigidBody_dynamicMatrices( g, IBzz, mB, qBz );
    [NMat, MMat, GMat, CMat, J_N, JDot_N] = planarRigidBody_dynamicMatrices2( g, IBzz, mB, qBz );
     %NMat(:, 1) = zeros(3,1);
     %control = J' * F;
     control = pinv(NMat)*(MMat * pinv(J_N) *(aDes' - JDot_N*pinv(J_N)*vDes') + CMat + GMat);
     control = [0; control];
     
     FBy = mB * (g +yDDotDes )
     control(2)
     TBz = IBzz* qDDotDes 
     control(3)
     
     control = [0; FBy; TBz];
     %control = planarRigidBody_MGInverseDynamics2( g, IBzz, mB, xDDotDes, yDDotDes, qDes, xDot3Des , xDot4Des  )
     
     %a_q = pinv(J)*(aDes'-JDot*pinv(J)*vDes');
     %control = pinv(NMat) * (MMat * a_q + GMat + CMat); 
%     control = RzRzRzManip_MGInverseDynamics( q1, q2, q3, q1Dot, q2Dot, q3Dot, ...
%          a_q(1), a_q(2), a_q(3), m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, ...
%          Len2, Len3, g );
elseif strcmp(controllerType, 'FF_Task_PID')
    kp = params.controller.kp;
    kd = params.controller.kd;
    [NMat, MMat, GMat, CMat, J, JDot] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
    %     https://www.mecharithm.com/computed-torque-control-of-a-3r-robot-arm-playing-ping-pong/
    % https://menloservice.sandia.gov/https://studywolf.wordpress.com/2013/09/17/robot-control-4-operation-space-control/
     aCon =  aDes'+kd*errorDot + kp * error;
     M_tilda = pinv(J*pinv(MMat)*J');
     C_tilda = pinv(J*pinv(MMat)*J')* (J*pinv(MMat)*CMat - JDot*qDot); 
     G_tilda = pinv(J*pinv(MMat)*J')* J*pinv(MMat) *GMat;
     F = M_tilda*aCon + C_tilda +G_tilda;
     
     control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes') + CMat + GMat);
     
     
        elseif strcmp(controllerType, 'FF_Null')
       % 3.2.3 of Nakanishi, Cory, Mistry, Peters, and Schaal / Operational Space Control: A Theoretical and Empirical Comparison
    kp = params.controller.kp;
    kd = params.controller.kd;
    [NMat, MMat, GMat, CMat, J, JDot] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
    %     https://www.mecharithm.com/computed-torque-control-of-a-3r-robot-arm-playing-ping-pong/
    % https://menloservice.sandia.gov/https://studywolf.wordpress.com/2013/09/17/robot-control-4-operation-space-control/
    J = J(1:2,:);
    JDot = JDot(1:2,:);
    aCon =  aDes'+kd*errorDot + kp * error;
    aCon = aCon(1:2);
     qPos = [ pi/8; pi/8; pi/8];
     control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes(1:2)') + CMat + GMat);
    %control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes(1:2)') + CMat + GMat) + (eye(3,3) - pinv(J)*J) * (0.1*(qPos-[q1; q2; q3]));
else  
  error('Controller Type not handled');
end 

end