function J = planarThrustFullAct_J( IBxx, lA, mB, qBx, qP2, qP4, qP5, F2, F4, F5 )
if( nargin ~= 10 ) error( 'planarThrustFullAct_J expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarThrustFullAct_J.m created Feb 11 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
J = zeros( 6, 6 );





%===========================================================================


%===========================================================================
Output = [];

J(1,1) = 0;
J(1,2) = 0;
J(1,3) = 0;
J(1,4) = 1;
J(1,5) = 0;
J(1,6) = 0;
J(2,1) = 0;
J(2,2) = 0;
J(2,3) = 0;
J(2,4) = 0;
J(2,5) = 1;
J(2,6) = 0;
J(3,1) = 0;
J(3,2) = 0;
J(3,3) = 0;
J(3,4) = 0;
J(3,5) = 0;
J(3,6) = 1;
J(4,1) = 0;
J(4,2) = 0;
J(4,3) = -(F2*cos(qBx+qP2)+F4*cos(qBx+qP4)+F5*cos(qBx+qP5))/mB;
J(4,4) = 0;
J(4,5) = 0;
J(4,6) = 0;
J(5,1) = 0;
J(5,2) = 0;
J(5,3) = -(F2*sin(qBx+qP2)+F4*sin(qBx+qP4)+F5*sin(qBx+qP5))/mB;
J(5,4) = 0;
J(5,5) = 0;
J(5,6) = 0;
J(6,1) = 0;
J(6,2) = 0;
J(6,3) = 0;
J(6,4) = 0;
J(6,5) = 0;
J(6,6) = 0;


%==============================================
end    % End of function planarThrustFullAct_J
%==============================================
