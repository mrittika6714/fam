clc; clear; %close all;
F2 = linspace(0,1,100);
F4 = linspace(0,1,100);
F5 = linspace(0,1,100);
[F2Mesh, F4Mesh, F5Mesh] = meshgrid(F2,F4,F5);

numPoints = length(F2Mesh(:));
[Fy_B, Fz_B, Tx_B] = calcWrench_B(F2Mesh(:)', F4Mesh(:)', F5Mesh(:)');

figure
plot3(Fy_B, Fz_B, Tx_B, 'k*')

figure
[k1,av1]=convhull(Fy_B, Fz_B, Tx_B);


trisurf(k1,Fy_B, Fz_B, Tx_B,'FaceColor','cyan')
axis equal


% figure
% plot3(F2Mesh(:,:,1),F4Mesh(:,:,1),F5Mesh(:,:,1), 'k*');
% xlabel('x')
% ylabel('y')
% zlabel('z')
% hold on
% plot3(F2Mesh(:,:,2),F4Mesh(:,:,2),F5Mesh(:,:,2), 'r*');
% plot3(F2Mesh(:,:,3),F4Mesh(:,:,3),F5Mesh(:,:,3), 'y*');
% 
% 
% figure
% plot3(F2Mesh(:), F4Mesh(:), F5Mesh(:), 'k*')
% 
% figure
% [k1,av1]=convhull(F2Mesh(:), F4Mesh(:), F5Mesh(:));
% 
% 
% trisurf(k1,F2Mesh(:), F4Mesh(:), F5Mesh(:),'FaceColor','cyan')
% axis equal

%%

function [Fy_B, Fz_B, Tx_B] = calcWrench_B(F2, F4, F5)
qP2 = 0.052013671256767;
qP4 = 0.052013716409034;
qP5 = 1.015559296616098;
lA = 1;
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;


fMat = [ F2; F4; F5];
wrench_B = B(2:4, :)*  fMat;
Fy_B = wrench_B(1,:);
Fz_B = wrench_B(2,:);
Tx_B = wrench_B(3,:);



end
