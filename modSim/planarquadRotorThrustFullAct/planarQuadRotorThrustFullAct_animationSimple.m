%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function planarQuadRotorThrustFullAct_animationSimple(results ,params, saveVideo)

t = results.t;
animationTimeScale = params.animationTimeScale;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;
states = results.additionalOutputs(:, 1:4);
% Unpack positions
A2y= states(:,1);
A2z= states(:,2);
A4y= states(:,3);
A4z= states(:,4);
setPoint = results.posDes;
lA = params.lA;
numPoints = length(t);


if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end


animationFigure = figure();
setPointPlot = plot(setPoint(1,1),setPoint(2,1), 'r.','markersize',25);
hold on
bodyPlot = plot([A2y(1);A2z(1)],[A4y(1);A4z(1)], 'k-', 'linewidth',3);
xMin = min( A2y) - lA*2.5;
xMax = max( A2y) + lA*2.5;
yMin = min( A2z) - lA*2.5;
yMax = max( A2z) + lA*2.5;

axis([xMin xMax yMin yMax])
xlabel('y Position [m]');
ylabel('z Position [m]');
title('Fully Actuated Planar Quad Rotor Simulation','fontweight', 'bold')
legend('Set Point', 'VehicleBody')
for i =1:numPoints
    setPointPlot.XData = setPoint(i,1);
    setPointPlot.YData = setPoint(i,2);
    bodyPlot.XData = [A2y(i);A4y(i)];
    bodyPlot.YData = [A2z(i); A4z(i) ];
    drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end