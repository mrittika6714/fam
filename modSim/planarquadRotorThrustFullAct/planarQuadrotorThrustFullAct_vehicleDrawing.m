
function vehicleDrawing = planarQuadrotorThrustFullAct_vehicleDrawing(Bcmy, Bcmz, qBody, params)

% Main Body
LBody = params.bodyLength;
WBody = params.bodyWidth;
LProp = params.LProp;
WProp = params.WProp;
propHeightOffset = params.propOffset;
centerBody = [Bcmy;Bcmz];
bodyShape = drawRectangle(LBody, WBody, centerBody, qBody );

% Propellor 2

qP2 = params.qP2;

LBar = propHeightOffset;
WBar = WProp/2;


offsetP2 = [-LProp; propHeightOffset];
offsetP2_B = rotateX(offsetP2 , qBody);
centerP2 = offsetP2_B + centerBody; 
P2 = drawRectangle(LProp, WProp, centerP2, (qP2 + qBody) );
centerP2Bar = rotateX(offsetP2 - [0; propHeightOffset/2 ],qBody)   + centerBody; 
P2Bar = drawRectangle(LBar, WBar, centerP2Bar, qBody+pi/2 );
%P2_Bar0_B = rotateX(offsetP2- [0;propHeightOffset] , qBody)+ centerBody;
%P2Bar = [centerP2'; P2_Bar0_B'];

% Proppellor 4
qP4 = params.qP4;
offsetP4 = [LProp; propHeightOffset];
offsetP4_B = rotateX(offsetP4 , qBody);
centerP4 = offsetP4_B + centerBody; 
P4 = drawRectangle(LProp, WProp, centerP4, (qP4 + qBody) );
centerP4Bar = rotateX(offsetP4 - [0; propHeightOffset/2 ],qBody)   + centerBody; 
P4Bar = drawRectangle(LBar, WBar, centerP4Bar, qBody+pi/2 );

% Proppellor 5
qP5 = params.qP5;
offsetP5 = [0; propHeightOffset];
offsetP5_B = rotateX(offsetP5 , qBody);
centerP5 = offsetP5_B + centerBody; 
P5 = drawRectangle(LProp, WProp, centerP5, (qP5 + qBody) );
centerP5Bar = rotateX(offsetP5 - [0; propHeightOffset/2 ],qBody)   + centerBody; 
P5Bar = drawRectangle(LBar, WBar, centerP5Bar, qBody+pi/2 );


vehicleDrawing.Body = bodyShape;
vehicleDrawing.P2 = P2;
vehicleDrawing.P4 = P4;
vehicleDrawing.P5 = P5;
vehicleDrawing.P2Bar = P2Bar;
vehicleDrawing.P4Bar = P4Bar;
vehicleDrawing.P5Bar = P5Bar;

% figure 
% axis equal
% hold on
% plot(P2Bar,'FaceAlpha',1, 'FaceColor','k')
% plot(P4Bar,'FaceAlpha',1, 'FaceColor','k')
% plot(P5Bar,'FaceAlpha',1, 'FaceColor','k')
% plot(P2,'FaceAlpha',1)
% plot(P4,'FaceAlpha',1)
% plot(P5,'FaceAlpha',1)
% plot(bodyShape,'FaceAlpha',1)


end

function rectangle = drawRectangle(L, W, center, theta)
X = [-L/2  L/2 L/2  -L/2] ;
Y = [-W/2  -W/2  W/2  W/2] ;
rectanglePoints = rotateX([X;Y], theta)+ [center(1); center(2)];
rectangle = polyshape(rectanglePoints(1,:),rectanglePoints(2,:) );
end

function rotatedPoints = rotateX(points, theta)
rotatedPoints  = [cos(theta), -sin(theta); sin(theta), cos(theta)] * points; 
end