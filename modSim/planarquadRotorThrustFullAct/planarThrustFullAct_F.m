function planarThrustFullAct_F
%===========================================================================
% File: planarThrustFullAct_F.m created Mar 20 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
function = zeros( 6, 1 );


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
IBxx                            =  0.0;                    % UNITS               Constant
lA                              =  0.0;                    % UNITS               Constant
mB                              =  0.0;                    % UNITS               Constant
F2                              =  0.0;                    % UNITS               Constant
F4                              =  0.0;                    % UNITS               Constant
F5                              =  0.0;                    % UNITS               Constant
qBx                             =  0.0;                    % UNITS               Constant
qP2                             =  0.0;                    % UNITS               Constant
qP4                             =  0.0;                    % UNITS               Constant
qP5                             =  0.0;                    % UNITS               Constant
qBxDt                           =  0.0;                    % UNITS               Constant
yDt                             =  0.0;                    % UNITS               Constant
zDt                             =  0.0;                    % UNITS               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================


%===========================================================================
Output = [];

function(1) = yDt;
function(2) = zDt;
function(3) = qBxDt;
function(4) = -(F2*sin(qBx+qP2)+F4*sin(qBx+qP4)+F5*sin(qBx+qP5))/mB;
function(5) = (F2*cos(qBx+qP2)+F4*cos(qBx+qP4)+F5*cos(qBx+qP5))/mB - g;
function(6) = lA*(F2*cos(qP2)-F4*cos(qP4))/IBxx;


%==============================================
end    % End of function planarThrustFullAct_F
%==============================================
