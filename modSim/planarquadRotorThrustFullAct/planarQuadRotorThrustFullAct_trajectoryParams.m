function trajectoryParams = planarQuadRotorThrustFullAct_trajectoryParams()

tFinal = 10;
t1 = tFinal/3;
t2 = 2*tFinal/3;
t3 = tFinal;
  
states0 = [0; 0; 0*pi/180; 0; 0; 0]; %load('ICs.mat')


% y trajectory 
yTrajectory = trajectory;
% yTrajectory.type = 'minJerk';
% yTrajectory.wayPoints = [ states0(1); 100];
% yTrajectory.velocities = [states0(4); 0 ];
% yTrajectory.tVec = [0; tFinal];
% yTrajectory.accelerations = [0; 0];
yTrajectory.type = 'cos';
yTrajectory.A = 5;
ytrajectory.w = 0.5;



% z Trajectory 
zTrajectory = trajectory;
% zTrajectory.type = 'constantPos';
% zTrajectory.wayPoints = states0(2);
zTrajectory.type = 'sin';
zTrajectory.A = 5;
ztrajectory.w = .5;

  
% qBx Trajectory 
qBxTrajectory = trajectory;
qBxTrajectory.type = 'minJerk';
qBxTrajectory.tVec = [0; t1;  t2; t3];
qBxTrajectory.wayPoints = [ 0; 0; 0; 0];
qBxTrajectory.velocities = [0; 0; 0; 0];
qBxTrajectory.accelerations = [0; 0; 0; 0];



trajectoryParams.numTrajectories = 3;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = yTrajectory;
trajectoryParams.trajectories{2} = zTrajectory;
trajectoryParams.trajectories{3} = qBxTrajectory;



end