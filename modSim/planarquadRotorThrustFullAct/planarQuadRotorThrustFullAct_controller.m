function control = planarQuadRotorThrustFullAct_controller(t, states, trajectories, trajectoryFunc, params)
persistent errorSum

% Unpack Params
IBxx = params.IB(1,1);
mB = params.mB;
lA = params.lA;
g = params.g;
qP2 = params.qP2;
qP4 = params.qP4;
qP5 = params.qP5;

% Unpack states
y = states(1);
z = states(2);
qBx = states(3);
yDot = states(4);
zDot = states(5); 
qBxDot = states(6);

controllerType = params.controller.type;

if isempty(errorSum)
  %disp(' Starting Controller For First Time')S
end
if strcmp(controllerType, 'none')
    control = zeros(length(states), 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.value;

elseif strcmp(controllerType, 'sin')
   control = ones(lengthStates, 1) * sin(t);
   
%% LQR Controller
elseif strcmp(controllerType, 'LQR')
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
  yDes = posDes(1);
  yDotDes = vDes(1);
  yDDotDes = aDes(1);
  zDes = posDes(2);
  zDotDes = vDes(2);
  zDDotDes = aDes(2);
  qBxDes =  posDes(3);
  qBxDotDes= vDes(3); 
  setPoint = [ yDes; zDes; qBxDes ; yDotDes; zDotDes; qBxDotDes ];
  Klqr = params.controller.Klqr;
  control = -Klqr*(states-setPoint) + params.controller.uEq;

%% Time Varying LQR Controller
elseif strcmp(controllerType, 'LQR_TV')
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
  yDes = posDes(1);
  yDotDes = vDes(1);
  yDDotDes = aDes(1);
  zDes = posDes(2);
  zDotDes = vDes(2);
  zDDotDes = aDes(2);
  qBxDes =  posDes(3);
  qBxDotDes= vDes(3); 
  setPoint = [ yDes; zDes; qBxDes ; yDotDes; zDotDes; qBxDotDes ];
  
  %% Calculate Current Equilibrium Thrust
%   syms F2 F4 F5
%   xDot= [-(F2*sin(qBx+qP2)+F4*sin(qBx+qP4)+F5*sin(qBx+qP5))/mB;  ...
%       (F2*cos(qBx+qP2)+F4*cos(qBx+qP4)+F5*cos(qBx+qP5))/mB - g; lA*(F2*cos(qP2)-F4*cos(qP4))/IBxx];
%   EqPoints = solve ( xDot  == 0, [F2; F4; F5] );
%   F2 = double(EqPoints.F2);
%   F4 = double(EqPoints.F4);
%   F5 = double(EqPoints.F5);
%   uEq = [F2; F4; F5 ];
  F2 = params.uEq(1);
  F4 = params.uEq(2);
  F5 = params.uEq(3);
  
  % Create LQR Gain Matrix
  A = planarThrustFullAct_J( IBxx, lA, mB, qBx, qP2, qP4, qP5, F2, F4, F5 );
  B = planarThrustFullAct_B( IBxx, lA, mB, qBx, qP2, qP4, qP5 );
  v = [ 10 10 1000 1 1 1];
  Q = diag(v);
  R = eye(3)*0.01;
  Klqr = lqr(A, B, Q, R );
  control = -Klqr*(states-setPoint) + params.uEq;
  
%% Nonlinear PID 
elseif strcmp(controllerType, 'PID')
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
  yDes = posDes(1);
  yDotDes = vDes(1);
  yDDotDes = aDes(1);
  zDes = posDes(2);
  zDotDes = vDes(2);
  zDDotDes = aDes(2);
  %Kpid= params.controller.Kpid;
  y = states(1);
  z = states(2);
  kpz = 80;
  kdz = 20;
  kiz = 0;
  kpy = 20;
  kdy = 5;
  kiy = 0;
  kdq = 10;
  kpq = 1000;
  kiq = 0;
  qBx = states(3);
  yDot = states(4);
  zDot = states(5); 
  qBxDot = states(6);
  qBxDes = -yDDotDes/(g+zDDotDes);
  
  
   currentError = [yDes - y, zDes - z, qBxDes - qBx];
   errorSum = [ 0 0 0];
   if isempty(errorSum)
     errorSum = [0 0 0];
   else
     errorSum = errorSum+currentError;
   end
  
  zDDotCon = zDDotDes + kdz*(zDotDes - zDot) +kpz * (zDes - z) + kiz*errorSum(2);
  yDDotCon = yDDotDes + kdy*(yDotDes - yDot) +kpy * (yDes - y) + kiy*errorSum(1);
  qBxCalc = -atan(yDDotCon/(g+zDDotCon)) ;
  zDDDotDes = 0;
  yDDDotDes = 0;
  qBxDotCalc= 0;
  qBxDDotCalc = 0;
  qBxDDotCon = qBxDDotCalc+kpq*(qBxCalc - qBx) + kdq*(qBxDotCalc - qBxDot) + kiq*errorSum(3);

  F2 = 0.5*(qBxDDotCon * IBxx)/lA  + 0.5*(mB*(zDDotCon+g))/cos(qBxCalc);
  F4 = 0.5*(mB*(zDDotCon+g))/cos(qBxCalc) - 0.5*(qBxDDotCon * IBxx)/lA ; 

  control = [F2; F4] ;
  
%% Linear PID Controller
elseif strcmp(controllerType, 'PIDLin')
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
  yDes = posDes(1);
  yDotDes = vDes(1);
  yDDotDes = aDes(1);
  zDes = posDes(2);
  zDotDes = vDes(2);
  zDDotDes = aDes(2);
  Kpid= params.Kpid;
  y = states(1);
  z = states(2);
  kpz = 80;
  kdz = 20;
  kiz = 0;
  kpy = 20;
  kdy = 5;
  kiy = 0;
  kdq = 10;
  kpq = 1000;
  kiq = 0;
  qBx = states(3);
  yDot = states(4);
  zDot = states(5); 
  qBxDot = states(6);
  qBxDes = -yDDotDes/(g+zDDotDes);
  
  
   currentError = [yDes - y, zDes - z, qBxDes - qBx];
   errorSum = [ 0 0 0];
   if isempty(errorSum)
     errorSum = [0 0 0];
   else
     errorSum = errorSum+currentError;
   end
  
  zDDotCon = zDDotDes + kdz*(zDotDes - zDot) +kpz * (zDes - z) + kiz*errorSum(2);
  yDDotCon = yDDotDes + kdy*(yDotDes - yDot) +kpy * (yDes - y) + kiy*errorSum(1);
  qBxCalc = -yDDotCon/(g+zDDotCon) ;
  zDDDotDes = 0;
  yDDDotDes = 0;
  qBxDotCalc= 0;%(yDDotCon*zDDDotDes-yDDDotDes*(g+zDDotCon))/(g+zDDotCon)^2;
  qBxDDotCalc = 0;
  %qBxDDotCalc= -(2*zDDDotDes*(y''*z'''-y'''*(g+z''))-(g+z'')*(y''*z''''-y''''*(g+z'')))/(g+z'')^3
  qBxDDotCon = qBxDDotCalc+kpq*(qBxCalc - qBx) + kdq*(qBxDotCalc - qBxDot) + kiq*errorSum(3);

  F2 = 0.5*(qBxDDotCon * IBxx)/lA  + 0.5*(mB*(zDDotCon+g));
  F4 = 0.5*(mB*(zDDotCon+g)) - 0.5*(qBxDDotCon * IBxx)/lA ; 

  control = [F2; F4] ;

%%
else
  error('Controller Type not handled');
end 



end