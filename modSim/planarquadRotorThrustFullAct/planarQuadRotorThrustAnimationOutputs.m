function [Output] = planarQuadRotorThrustAnimationOutputs( lA, qBx, y, z )
if( nargin ~= 4 ) error( 'planarQuadRotorThrustAnimationOutputs expects 4 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrustAnimationOutputs.m created Feb 11 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================


%===========================================================================
Output = zeros( 1, 4 );

Output(1) = y+lA*cos(qBx);
Output(2) = z+lA*sin(qBx);
Output(3) = y-lA*cos(qBx);
Output(4) = z-lA*sin(qBx);

%==============================================================
end    % End of function planarQuadRotorThrustAnimationOutputs
%==============================================================
