function  planarQuadRotorThrustFullAct_plot(plotData, params, savePlots)

styleVec ={'g-','b--','y-' };
inputForces = plotData.additionalOutputs(:,5:7);
propRates = plotData.additionalOutputs(:,7:9);
desiredTrajectory = [plotData.posDes, plotData.vDes];
stateError = plotData.states - desiredTrajectory;

forceVTime = figure;
hold on
for i = 1:size(inputForces,2)
  plot(plotData.t, inputForces(:,i), styleVec{i},'linewidth', 2)
end

title(' Input Force Time Histories');
xlabel('Time [s]');
ylabel('Thrust Forces [n]');
legend('FP2', 'FP4', 'FP5')

errorVTime = figure; 
plot(plotData.t, stateError)
title('State Error vs Time')
xlabel('Time (s)');
ylabel('Error');
legend('y', 'z', 'qBx', 'yDot', 'zDot', 'qBxDot')


if savePlots
  saveas(forceVTime, fullfile(params.resultsFolder,['forceVtime_', ...
      params.controller, '.png'] ));
  saveas(errorVTime, fullfile(params.resultsFolder,['errorVtime_', ...
      params.controller, '.png'] ));
end

end