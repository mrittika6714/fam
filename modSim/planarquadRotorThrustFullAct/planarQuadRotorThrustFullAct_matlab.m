function [SolutionToAlgebraicEquations,Output] = planarQuadRotorThrustFullAct_matlab( F2, F4, F5, qBx, yDt, zDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, lA, qP2, qP4, qP5 )
if( nargin ~= 18 ) error( 'planarQuadRotorThrustMatlab expects 18 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrustMatlab.m created Feb 11 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================
COEF = zeros( 3, 3 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = IBxx;
RHS = zeros( 1, 3 );
RHS(1) = -F2*sin(qBx+qP2) - F4*sin(qBx+qP4) - F5*sin(qBx+qP5);
RHS(2) = F2*cos(qBx+qP2) + F4*cos(qBx+qP4) + F5*cos(qBx+qP5) - g*mB;
RHS(3) = lA*(F2*cos(qP2)-F4*cos(qP4));
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
yDDt = SolutionToAlgebraicEquations(1);
zDDt = SolutionToAlgebraicEquations(2);
qBxDDt = SolutionToAlgebraicEquations(3);



%===========================================================================
Output = zeros( 1, 6 );

Output(1) = yDt;
Output(2) = zDt;
Output(3) = qBxDt;

Output(4) = yDDt;
Output(5) = zDDt;
Output(6) = qBxDDt;

%====================================================
end    % End of function planarQuadRotorThrustMatlab
%====================================================
