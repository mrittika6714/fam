function BMat = planarThrustFullAct_B( IBxx, lA, mB, qBx, qP2, qP4, qP5 )
if( nargin ~= 7 ) error( 'planarThrustFullAct_B expects 7 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarThrustFullAct_B.m created Feb 11 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
BMat = zeros( 6, 3 );





%===========================================================================


%===========================================================================
Output = [];

BMat(1,1) = 0;
BMat(1,2) = 0;
BMat(1,3) = 0;
BMat(2,1) = 0;
BMat(2,2) = 0;
BMat(2,3) = 0;
BMat(3,1) = 0;
BMat(3,2) = 0;
BMat(3,3) = 0;
BMat(4,1) = -sin(qBx+qP2)/mB;
BMat(4,2) = -sin(qBx+qP4)/mB;
BMat(4,3) = -sin(qBx+qP5)/mB;
BMat(5,1) = cos(qBx+qP2)/mB;
BMat(5,2) = cos(qBx+qP4)/mB;
BMat(5,3) = cos(qBx+qP5)/mB;
BMat(6,1) = lA*cos(qP2)/IBxx;
BMat(6,2) = -lA*cos(qP4)/IBxx;
BMat(6,3) = 0;


%==============================================
end    % End of function planarThrustFullAct_B
%==============================================
