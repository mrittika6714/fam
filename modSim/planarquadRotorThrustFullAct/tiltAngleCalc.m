

clc;clear;close all;
format long;
options = optimoptions('fmincon');
options = optimoptions(options,'constraintTolerance',1e-12,'MaxIterations',10000,'MaxFunctionEvaluations',100000,'Display', 'iter');
x0 = [0; 0; 0;];
A= [];
B = [];
lb = [ -pi; -pi; -pi];
ub = [ pi; pi; pi;];
[out fval, exitFlag] = fmincon(@objectiveFunc, x0, A, B,[],[],lb, ub, @nonLinConFunc, options)
180/pi*out

lA = 1;
qP2 = out(1);
qP4 = out(2);
qP5 = out(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
rank(B)
B
diff = abs(max(svd(B)) - min(svd(B)))
wrench = calcWrench(out)
wrench(4)

qBx = -pi : 0.01:pi;

for i = 1: length(qBx)
[F_N(i,:) T_N(i,:) ] = calcWrench_N(qP2, qP4, qP5, qBx(i));
end

figure
plot(F_N(:,2), F_N(:,3))
xlabel('yThrust' )
ylabel('zThrust')
figure 
plot(qBx, T_N(:,1))
%plot(qBx, wrench_N(:,2))
%plot(qBx, wrench_N(:,3))


%% Private Functions

function [c, cEq ]= nonLinConFunc(x)
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
S = svd(B);
sDiff = (max(S)- min(S))^2;
c = 0;
cEq = 0;

end


function J = objectiveFunc(x)
weight = [ 1; 1; 1; 1; 1; 1];
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
fMat = [ 1; 1; 1];
func = B*fMat;
func = func.*weight;
%S = svd(B);
%sDiff = (max(S)- min(S))^2;
%J = -(norm(func)) + 0 * sDiff;
J = cond(B);
end

function func = calcWrench(x)
weight = [ 1; 1; 1; 1; 1; 1];
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
fMat = [ 1; 1; 1];
func = B*fMat;

end


function [F_N, T_N] = calcWrench_N(qP2, qP4, qP5, qBx)
lA = 1;
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
N_R_B = [1, 0, 0; 0, cos(qBx), -sin(qBx); 0, sin(qBx), cos(qBx)];

fMat = [ .1; 0.1; 1];
wrench_B =B(2:4, :) * fMat
Fy_B = wrench_B(1);
Fz_B = wrench_B(2);
Tx_B = wrench_B(3);

F_B = [ 0; Fy_B; Fz_B]
T_B = [ Tx_B; 0; 0] 
F_N = (N_R_B * F_B)'

T_N = (N_R_B * T_B)'
end
