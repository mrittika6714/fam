%% Analysis of the planarQuadRotor

clc;clear;close all
saveParams = false;
addpath('..')
clear GenerateTrajectoryPoint
% Define System
DOF = 3;
params.IB(1,1) = 0.00025;
params.IB(2,2)= 0.0117;
params.IB(3,3) = 0.00234;
params.IB(1,2) = 0;
params.IB(2,3) = 0;
params.IB(3,1) =0;
params.mB = 0.18;
params.lA = 0.186;
params.g  = 9.8;
params.bThrust = 6.11 * 10^-8; %N/rpm^2
params.markerSize = 35;
params.bodyLength = 2;
params.bodyWidth = 0.5;
params.LProp = 0.75;
params.WProp = 0.125;
params.propOffset = 1;
%save('planarQuadRotorThrustFullAct_modelParams.mat', 'params')
%% Define EOMs
syms y z  qBx mB F4 IBxx yDot zDot qBxDot F2 g lA qP2 qP4 qP5 F5 real
symParams = [ mB; IBxx; g; lA;];
subParams = [ params.mB; params.IB(1,1); params.g; params.lA; ];
T = [F2; F4; F5];
x = [y;  z;  qBx;  yDot;  zDot;  qBxDot];
xDot= [yDot; zDot; qBxDot;  -(F2*sin(qBx+qP2)+F4*sin(qBx+qP4)+F5*sin(qBx+qP5))/mB;  ...
      (F2*cos(qBx+qP2)+F4*cos(qBx+qP4)+F5*cos(qBx+qP5))/mB - g; lA*(F2*cos(qP2)-F4*cos(qP4))/IBxx];

%% Check Conditions for Underactuation
Mat2 = zeros(6,3);
Mat2 = sym(Mat2);
Mat2(1,1) = 0;
Mat2(1,2) = 0;
Mat2(1,3) = 0;
Mat2(2,1) = 0;
Mat2(2,2) = 0;
Mat2(2,3) = 0;
Mat2(3,1) = 0;
Mat2(3,2) = 0;
Mat2(3,3) = 0;
Mat2(4,1) = -sin(qP2)/mB;
Mat2(4,2) = -sin(qP4)/mB;
Mat2(4,3) = -sin(qP5)/mB;
Mat2(5,1) = cos(qP2)/mB;
Mat2(5,2) = cos(qP4)/mB;
Mat2(5,3) = cos(qP5)/mB;
Mat2(6,1) = lA*cos(qP2)/IBxx;
Mat2(6,2) = -lA*cos(qP4)/IBxx;
Mat2(6,3) = 0;
controlledDOF = rank(Mat2(4:6,:));

if controlledDOF  < DOF
  warning('System is Always Underactuated')
end


determinant = simplify(det(Mat2(4:end, :)));
%determinant = -lA*(cos(qP2)*sin(qP4-qP5)+cos(qP4)*sin(qP2-qP5))/(IBxx*mB^2)
underActuatedConditions = solve ( determinant == 0, [ qP2, qP4 ,qP5,],  'Real',true);
qP2Conditions = underActuatedConditions.qP2
qP4Conditions = underActuatedConditions.qP4
qP5Conditions = underActuatedConditions.qP5

%% Evaluate and Plot Underactuated Configurations
Mat2Sub = subs(Mat2, symParams, subParams );
qP2Test = 1;
n = floor(qP2Test/(pi/2))
qP4Test = (n+1)*pi - qP2Test
qP5Test = 0;
Mat2SubTest = rank(subs(Mat2Sub, [qP2, qP4, qP5], [qP2Test, qP4Test, qP5Test]))
determinantSubTest = vpa(subs(determinant, [qP2, qP4, qP5], [qP2Test, qP4Test, qP5Test]))
% for i = 1: length(qP2Conditions)
% Mat2Sub_2{i} = double(subs(Mat2Sub, [qP2, qP4, qP5], [qP2Conditions(i), qP4Conditions(i), qP5Conditions(i)]));
% determinantSub{i} = subs(determinant, [qP2, qP4, qP5], [qP2Conditions(i), qP4Conditions(i), qP5Conditions(i)]);
% underActConditions.qP2 = double(qP2Conditions(i));
% underActConditions.qP4 = double(qP4Conditions(i));
% underActConditions.qP5 = double(qP5Conditions(i));
% vehicleBody = planarQuadrotorThrustFullAct_vehicleDrawing(0, 0, 0, underActConditions);
% fieldNames = fieldnames(vehicleBody);
% numPlots = length(fieldNames);
% figure;
% hold on
% axis equal
% plotHandles = matlab.graphics.primitive.Polygon.empty(0,numPlots);
% for ii = 1:numPlots
%   plotHandles(ii) = plot(vehicleBody.(fieldNames{ii}),'FaceAlpha',1, 'FaceColor', 'k');
% end
% set(gca,'visible','off')
% end

%% Determine Optimal Rotor Angles Minimizing Condition Number of ForceTorque Matrix 
options = optimoptions('fmincon');
options = optimoptions(options,'constraintTolerance',1e-12,'MaxIterations',10000,'MaxFunctionEvaluations',100000,'Display', 'iter');
x0 = [0; 0; 0;];
A= [];
B = [];
lb = [ -2*pi; -2*pi; -2*pi];
ub = [ 2*pi; 2*pi; 2*pi;];
[optimalAngles fval, exitFlag] = fmincon(@objectiveFunc, x0, A, B,[],[],lb, ub, @nonLinConFunc, options)
fprintf('Optimal angle values found to be: [%0.2f deg, %0.2f deg, %0.2f deg] \n', ...
  180/pi*optimalAngles(1), 180/pi*optimalAngles(2), 180/pi*optimalAngles(3) );

params.qP2 = optimalAngles(1);
params.qP4 = optimalAngles(2);
params.qP5 = optimalAngles(3);

symParams = [ mB; IBxx; g; lA; qP2; qP4; qP5];
subParams = [ params.mB; params.IB(1,1); params.g; params.lA; params.qP2; ...
    params.qP4; params.qP5];
  
  
  

Mat2Sub_Optmal1 = double(subs(Mat2Sub, [qP2, qP4, qP5], [optimalAngles(1), optimalAngles(2), optimalAngles(3)]));
determinantSub_Optmal1 = subs(determinant, [qP2, qP4, qP5], [optimalAngles(1), optimalAngles(2), optimalAngles(3)]);
vehicleBody = planarQuadrotorThrustFullAct_vehicleDrawing(0, 0, 0, params);
fieldNames = fieldnames(vehicleBody);
numPlots = length(fieldNames);
figure;
hold on
axis equal
plotHandles = matlab.graphics.primitive.Polygon.empty(0,numPlots);
for ii = 1:numPlots
  plotHandles(ii) = plot(vehicleBody.(fieldNames{ii}),'FaceAlpha',1, 'FaceColor', 'k');
end
title(sprintf('Optimal Configuration for Minimizing Condition Number, [%.2f, %.2f, %.2f ]', ...
  optimalAngles(1), optimalAngles(2), optimalAngles(3)))
set(gca, 'visible', 'off')
set(findall(gca, 'type', 'text'), 'visible', 'on')
  
%% Determine Optimal Rotor Angles Minimizing Required F/T over a Desired Trajectory

% options = optimoptions('fmincon');
% options = optimoptions(options,'constraintTolerance',1e-12,'MaxIterations',10000,'MaxFunctionEvaluations',100000,'Display', 'iter');
% x0 = [0; 0; 0.001;];
% A= [];
% B = [];
% lb = [ -pi; -pi; -pi];
% ub = [ pi; pi; pi;];
% [optimalAngles2 fval, exitFlag] = fmincon(@objectiveFunc2, x0, A, B,[],[],lb, ub, @nonLinConFunc2, options)
% fprintf('Optimal angle values found to be: [%0.2f deg, %0.2f deg, %0.2f deg] \n', ...
%   180/pi*optimalAngles2(1), 180/pi*optimalAngles2(2), 180/pi*optimalAngles2(3) );
% 
% params.qP2 = optimalAngles2(1);
% params.qP4 = optimalAngles2(2);
% params.qP5 = optimalAngles2(3);
% 
% % symParams = [ mB; IBxx; g; lA; qP2; qP4; qP5];
% % subParams = [ params.mB; params.IB(1,1); params.g; params.lA; params.qP2; ...
% %     params.qP4; params.qP5];
%   
% optimalAnglesStruct.qP2 = optimalAngles2(1);
% optimalAnglesStruct.qP4 = optimalAngles2(2);
% optimalAnglesStruct.qP5 = optimalAngles2(3);
%   
% 
% Mat2Sub_Optmal2 = double(subs(Mat2Sub, [qP2, qP4, qP5], [optimalAngles2(1), optimalAngles2(2), optimalAngles2(3)]));
% determinantSub_Optmal2 = subs(determinant, [qP2, qP4, qP5], [optimalAngles2(1), optimalAngles2(2), optimalAngles2(3)]);
% vehicleBody = planarQuadrotorThrustFullAct_vehicleDrawing(0, 0, 0, optimalAnglesStruct);
% fieldNames = fieldnames(vehicleBody);
% numPlots = length(fieldNames);
% figure;
% hold on
% axis equal
% plotHandles = matlab.graphics.primitive.Polygon.empty(0,numPlots);
% shapeColors = { 'k', 'k', 'k', 'k', 'k', 'k', 'k'};
% for ii = 1:numPlots
%   plotHandles(ii) = plot(vehicleBody.(fieldNames{ii}),'FaceAlpha',1, 'FaceColor', shapeColors{ii});
% end
% title(sprintf('Optimal Configuration over a Trajectory, [%.2f, %.2f, %.2f ]', ...
%   optimalAngles2(1), optimalAngles2(2), optimalAngles2(3)))
% set(gca, 'visible', 'off')
% set(findall(gca, 'type', 'text'), 'visible', 'on')
%   
% params.qp2 = optimalAngles2(1);
% params.qp4 = optimalAngles2(2);
% params.qp5 = optimalAngles2(3);
% save('params.mat', 'params')


%% Evaluation of Full Actuation
% calcDet = matlabFunction(Mat2)
% detAct = double(subs(determinant, symParams, subParams ))
% 
% qBx =linspace(1, 1, 1)
% F2 = linspace(0, 1, 2)
% F4 = linspace(0, 1, 2)
% F5 = linspace(0, 1, 2)
% 
% for i = 1:length(qBx)
%   for ii = 1:length(F2)
%     for iii = 1:length(F4)
%       for iiii = 1:length(F5)
%         [F_N(i*ii*iii*iiii,:), T_N(i*ii*iii*iiii,:) ] = calcWrench_N(params.qP2, params.qP4, params.qP5, qBx(i),F2(ii), F4(iii), F5(iiii))
%       end
%     end
%   end
% end
% 
% figure
% plot(F_N(:,2), F_N(:,3))
% xlabel('yThrust' )
% ylabel('zThrust')
% 
% figure
% plot(qBx, F_N(:,2))
% xlabel('qBx' )
% ylabel('yThrust')
% 
% figure
% plot(qBx, F_N(:,3))
% xlabel('qBx' )
% ylabel('zThrust')
% 


%% Perform analysis of linearized system
Df=simplify(jacobian(xDot, x));
J = zeros(6,6);
J = sym(J);
J(1,1) = 0;
J(1,2) = 0;
J(1,3) = 0;
J(1,4) = 1;
J(1,5) = 0;
J(1,6) = 0;
J(2,1) = 0;
J(2,2) = 0;
J(2,3) = 0;
J(2,4) = 0;
J(2,5) = 1;
J(2,6) = 0;
J(3,1) = 0;
J(3,2) = 0;
J(3,3) = 0;
J(3,4) = 0;
J(3,5) = 0;
J(3,6) = 1;
J(4,1) = 0;
J(4,2) = 0;
J(4,3) = -(F2*cos(qBx+qP2)+F4*cos(qBx+qP4)+F5*cos(qBx+qP5))/mB;
J(4,4) = 0;
J(4,5) = 0;
J(4,6) = 0;
J(5,1) = 0;
J(5,2) = 0;
J(5,3) = -(F2*sin(qBx+qP2)+F4*sin(qBx+qP4)+F5*sin(qBx+qP5))/mB;
J(5,4) = 0;
J(5,5) = 0;
J(5,6) = 0;
J(6,1) = 0;
J(6,2) = 0;
J(6,3) = 0;
J(6,4) = 0;
J(6,5) = 0;
J(6,6) = 0;

checkDf = simplify(Df-J)
numEqualElements = 0;
try
numEqualElements = sum(double(checkDf)== 0, 'all');

catch E
  warning('Matlab Calculated Jacobian doesnt match MG calculated Jacobian')
end

numElements = numel(checkDf);

if numEqualElements ~= numElements 
  warning('Matlab Calculated Jacobian doesnt match MG calculated Jacobian')
else
  fprintf('Jacboian calculations match \n')
end


DfSubs = subs(J, symParams, subParams );
FSubs= subs(xDot, [symParams; ], [subParams; ]);
FSubs= subs(xDot, [qBx ], [0 ]);
EqPoints = solve ( FSubs  == 0, [qBxDot; yDot; zDot;  F2; F4; F5] );

% note from eqpoints we can linearize about any point
eqPoint = [0, 0, 0, EqPoints.yDot(1), EqPoints.zDot(1), EqPoints.qBxDot(1)]
uEq = [EqPoints.F2(1); EqPoints.F4(1); EqPoints.F5(1)];
uEqVal = double(subs(uEq, symParams, subParams));
A =subs(Df, [x; T], [eqPoint';EqPoints.F2(1); EqPoints.F4(1);  EqPoints.F5(1)] );
ASub = double(subs(A, symParams,subParams))

eigenvalues=expand(eig(A))

% note stabiility/instability of eigenvalyes 
eigs = eig(ASub)

 % Get B From Motion Genesis ToDO is get in matlab
Df_B=simplify(jacobian(xDot, T));
% B_MG = [0, 0;...  
%      0, 0; ...
%      0, 0;...  
%      -sin(qBx)/mB, 0; ... 
%      cos(qBx)/mB, 0;...  
%      0, 1/IBxx];
     
BSub = subs(Df_B, x, eqPoint');   
BSub = double(subs(BSub, symParams, subParams));
rankSys = rank(ctrb(ASub,BSub));


if rankSys < length(x)
  warning('System Not Controllable')
else
  fprintf('System is controllable. Rank of controllability matrix is = %i \n', rankSys)
end

% Create LQR Gain Matrix
v = [ 10 10 1000 1 1 1];
Q = diag(v);
%Q = eye(6,6)*1;
R = eye(3)*0.01;

Klqr = lqr(ASub, BSub, Q, R )
eigs = eig(ASub-BSub*Klqr)
%save('Klqr.mat', 'Klqr')
%save('uEq.mat', 'uEqVal')

%% Helper Functions

function forceTorqueNorm = forceTorqueMap(x)
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
fMat = [ 1; 1; 1];
func = B*fMat;
forceTorqueNorm = -(norm(func));
end
  
function [c, cEq ]= nonLinConFunc(x)
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
S = svd(B);
sDiff = (max(S)- min(S))^2;
c = 0;
cEq = 0;

end


function J = objectiveFunc(x)
weight = [ 1; 1; 1; 1; 1; 1];
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
fMat = [ 1; 1; 1];
func = B*fMat;
func = func.*weight;
%S = svd(B);
%sDiff = (max(S)- min(S))^2;
%J = -(norm(func)) + 0 * sDiff;
J = cond(B);
end


function [F_N, T_N] = calcWrench_N(qP2, qP4, qP5, qBx, F2, F4, F5)
lA = 1;
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
N_R_B = [1, 0, 0; 0, cos(qBx), -sin(qBx); 0, sin(qBx), cos(qBx)];

fMat = [ F2; F4; F5];
wrench_B =B(2:4, :) * fMat;
Fy_B = wrench_B(1);
Fz_B = wrench_B(2);
Tx_B = wrench_B(3);

F_B = [ 0; Fy_B; Fz_B];
T_B = [ Tx_B; 0; 0] ;
F_N = (N_R_B * F_B)';

T_N = (N_R_B * T_B)';
end


function J = objectiveFunc2(x)
lA = 1;
IBxx = 1;
mB = 1;
qBx = 0;
% qBxDDt = 0;
% yDDt = 1;
% zDDt =0;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);

  tStep = .001; 
  tInitial = 0;
  tFinal = 60;
  t = (0:tStep:tFinal)';
  omega = 15*pi/180;
  trajectories.type = 'sine';
  trajectories.omega = omega;
  trajectories.qBx = 0*pi/180;
  trajectories.A = 0;
  clear GenerateTrajectoryPoint;
  [posDes, vDes, aDes]  = GenerateTrajectoryPoint(trajectories, t);
  zDDt = aDes;
  
  trajectories = [];
  trajectories.type = 'minJerk';
trajectories.trajectoryIndex = 3; % fieldname index in which trajectories start
trajectories.numTrajectories = 1;
t1 = tFinal/2;
trajectories.ytrajectory.tVec = [0; t1;  tFinal];
trajectories.ytrajectory.wayPoints = [ 0; 100; 100; ];
trajectories.ytrajectory.velocities = [0; 0; 0; ];
trajectories.ytrajectory.accelerations = [0; 0; 0;];
clear GenerateTrajectoryPoint;
[posDes, vDes, aDes] = GenerateTrajectoryPoint(trajectories, t);
yDDt = aDes;
  
  qBx = 0* ones(size(posDes,1),1);
  qBxDot = zeros(size(posDes,1),1);
  qBxDDt = qBxDot;
  [yDDt, zDDt, qBxDDt];
u = planarQuadRotorThrustFullAct_InverseDynamics2( IBxx, lA, mB, qBx, qP2, qP4, qP5, qBxDDt, yDDt, zDDt );
diff = norm(sqrt((u(:,1) - u(:,2)).^2 + (u(:,1) - u(:,3)).^2 +(u(:,2) - u(:,3)).^2));
J = sum(sqrt(sum(u.*u,2))); %+ diff;

end

function [c, cEq ]= nonLinConFunc2(x)

% qP2 = x(1);
% qP4 = x(2);
% qP5 = x(3);

% if numel(unique(round(x,6))) == 1
%   allEqual = 1;
% else
%   allEqual = 0;
% end
c = 0;
cEq = 0;%allEqual;

end
