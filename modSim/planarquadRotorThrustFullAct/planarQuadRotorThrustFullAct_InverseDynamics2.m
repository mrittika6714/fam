function KaneinverseDynamics = planarQuadRotorThrustFullAct_InverseDynamics2( IBxx, lA, mB, qBx, qP2, qP4, qP5, qBxDDt, yDDt, zDDt )
if( nargin ~= 10 ) error( 'planarQuadRotorThrustFullAct_InverseDynamics2 expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrustFullAct_InverseDynamics2.m created Mar 20 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
numPoints = length(qBxDDt);
KaneinverseDynamics = zeros( numPoints, 3 );


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================


%===========================================================================
Output = [];

KaneinverseDynamics(:,1) = IBxx.*sin(qP4-qP5).*qBxDDt./(lA.*(cos(qP2).*sin(qP4-qP5)+cos(qP4).*sin(qP2-qP5))) - mB.*cos(qP4).*(cos(qBx+qP5).*  ...
yDDt+sin(qBx+qP5).*(g+zDDt))./(cos(qP2).*sin(qP4-qP5)+cos(qP4).*sin(qP2-qP5));
KaneinverseDynamics(:,2) = -IBxx.*sin(qP2-qP5).*qBxDDt./(lA.*(cos(qP2).*sin(qP4-qP5)+cos(qP4).*sin(qP2-qP5))) - mB.*cos(qP2).*(cos(qBx+qP5).*  ...
yDDt+sin(qBx+qP5).*(g+zDDt))./(cos(qP2).*sin(qP4-qP5)+cos(qP4).*sin(qP2-qP5));
KaneinverseDynamics(:,3) = IBxx.*sin(qP2-qP4).*qBxDDt./(lA.*(cos(qP2).*sin(qP4-qP5)+cos(qP4).*sin(qP2-qP5))) + mB.*((cos(qP2).*cos(qBx+qP4)+  ...
cos(qP4).*cos(qBx+qP2)).*yDDt+(cos(qP2).*sin(qBx+qP4)+cos(qP4).*sin(qBx+qP2)).*(g+zDDt))./(cos(qP2).*sin(qP4-qP5)+cos(qP4).*sin(qP2-qP5));



%======================================================================
end    % End of function planarQuadRotorThrustFullAct_InverseDynamics2
%======================================================================
