function [posDes, vDes, aDes] = planarQuadRotorThrustFullAct_trajGen(trajectories, t)

%% Generate Trajectories

posDes = zeros(length(t), trajectories.numTrajectories);
vDes = zeros(length(t), trajectories.numTrajectories);
aDes = zeros(length(t), trajectories.numTrajectories);

for i = 1:trajectories.numTrajectories
  [posDes(:,i), vDes(:,i), aDes(:,i)] = trajectories.trajectories{i}.getTrajectory(t);
end



 
%   [zposDes, zvDes, zaDes] = GenerateTrajectoryPoint(trajectories.trajectories{1}, t);
%   [yposDes, yvDes, yaDes] = GenerateTrajectoryPoint1(trajectories.trajectories{2}, t);
% 
%   % need to define qBx as sine only gives trajectories for y and z
%   qBx = 0* ones(size(yposDes,1),1);
%   qBxDot = zeros(size(yposDes,1),1);
%   qBxDDot = qBxDot;
%   
%   posDes = [yposDes, zposDes, qBx];
%   vDes = [yvDes, zvDes, qBxDot];
%   aDes = [yaDes, zaDes, qBxDDot];

% end

end