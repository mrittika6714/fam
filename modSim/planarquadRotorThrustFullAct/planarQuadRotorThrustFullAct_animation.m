%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function planarQuadRotorThrustFullAct_animation(results ,params, saveVideo)

t = results.t;
animationTimeScale = params.animationTimeScale;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;
states = results.states;

% Unpack positions
Bcmy = states(:,1);
Bcmz= states(:,2);
qBody= states(:,3);
lA = params.bodyLength;
numPoints = length(t);
setPoint = results.posDes;

if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end
xMin = min( Bcmy) - lA*2.5;
xMax = max( Bcmy) + lA*2.5;
yMin = min( Bcmz) - lA*2.5;
yMax = max( Bcmz) + lA*2.5;


vehicleBody = planarQuadrotorThrustFullAct_vehicleDrawing(Bcmy(1), Bcmz(1), qBody(1), params);
fieldNames = fieldnames(vehicleBody);
numPlots = length(fieldNames);
plotHandles = matlab.graphics.primitive.Polygon.empty(0,numPlots);

animationFigure = figure();
hold on
for i = 1:numPlots
  plotHandles(i) = plot(vehicleBody.(fieldNames{i}),'FaceAlpha',1, 'FaceColor', 'k');
end
setPointPlot = plot(setPoint(1,1),setPoint(2,1), 'r.','markersize',params.markerSize);
setPointPath = plot(setPoint(1,1),setPoint(2,1), 'r--');
bodyPath = plot(Bcmy(1),Bcmz(1), 'k--');
axis([xMin xMax yMin yMax])
xlabel('y Position [m]');
ylabel('z Position [m]');
title('Fully Actuated Planar Quad Rotor Simulation','fontweight', 'bold')
handlevec = [setPointPlot plotHandles(1)];
legend(handlevec,'Set Point', 'VehicleBody');
txt = sprintf('t = %0.2f', t(1));
textPos = [ 0, 8];
timeText = text(textPos(1),textPos(2),txt,'HorizontalAlignment','center','FontSize',14);

%legend('Set Point', 'VehicleBody')
for i =1:numPoints
  setPointPlot.XData = setPoint(i,1);
  setPointPlot.YData = setPoint(i,2);
  setPointPath.XData = setPoint(1:i,1);
  setPointPath.YData = setPoint(1:i,2);
  bodyPath.XData = Bcmy(1:i);
  bodyPath.YData = Bcmz(1:i);
  vehicleBody = planarQuadrotorThrustFullAct_vehicleDrawing(Bcmy(i), Bcmz(i), qBody(i), params);  
  timeText.String = sprintf('t = %0.2f', t(i));;
  drawnow
  for i = 1:numPlots
    plotHandles(i).Shape = vehicleBody.(fieldNames{i});
  end
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end