clc; clear; close all
tFinal = 60

t = 0:.1:tFinal
trajectoryParams = planarQuadRotorThrustFullAct_trajectoryParams(tFinal)
results.trajectories = planarQuadRotorThrustFullAct_trajGen(trajectoryParams, t');

results.trajectories.trajectories{1}

plot(t,results.trajectories.trajectories{1}.posDes)
