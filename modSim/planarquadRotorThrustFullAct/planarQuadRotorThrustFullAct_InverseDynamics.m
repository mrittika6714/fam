function [SolutionToAlgebraicEquations] = planarQuadRotorThrustFullAct_InverseDynamics( IBxx, lA, mB, qBx, qP2, qP4, qP5, qBxDDt, yDDt, zDDt )
if( nargin ~= 10 ) error( 'planarQuadRotorThrustFullAct_InverseDynamics expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrustFullAct_InverseDynamics.m created Mar 20 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================
COEF = zeros( 3, 3 );
COEF(1,1) = sin(qBx+qP2);
COEF(1,2) = sin(qBx+qP4);
COEF(1,3) = sin(qBx+qP5);
COEF(2,1) = -cos(qBx+qP2);
COEF(2,2) = -cos(qBx+qP4);
COEF(2,3) = -cos(qBx+qP5);
COEF(3,1) = -lA*cos(qP2);
COEF(3,2) = lA*cos(qP4);
RHS = zeros( 1, 3 );
RHS(1) = -mB*yDDt;
RHS(2) = -mB*(g+zDDt);
RHS(3) = -IBxx*qBxDDt;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
F2 = SolutionToAlgebraicEquations(1);
F4 = SolutionToAlgebraicEquations(2);
F5 = SolutionToAlgebraicEquations(3);



%=====================================================================
end    % End of function planarQuadRotorThrustFullAct_InverseDynamics
%=====================================================================
