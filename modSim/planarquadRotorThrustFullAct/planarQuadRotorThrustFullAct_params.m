function params = planarQuadRotorThrustFullAct_params(simParams)

%% Store simulation Params
params.sim = simParams;

params.IB = [0.003, 0, 0; 0, 0.0117, 0; 0, 0, 0.0023];
params.mB = 0.18;
params.g = 9.8;

params.mP = 0.0001;
params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 0.00005];
params.lA = 0.186;
params.qP2 = -0.7854;
params.qP4 = 0.7854;
params.qP5 = 0.2618;
params.bThrust = 6.1100e-08;

%% Controller Params
params.controller.constantValue = 0;  % rad/sec
params.controller.Klqr = load('Klqr.mat');
params.controller.Klqr = params.controller.Klqr.Klqr;
params.controller.uEq = load('uEq.mat');
params.controller.uEq = params.controller.uEq.uEqVal;
params.controller.type = simParams.control.type;

%% Plotting Params
params.markerSize = 35;
params.bodyLength = 2;
params.bodyWidth = 0.5;
params.LProp = 0.75;
params.WProp = 0.125;
params.propOffset = 1;

%% Trajectory Params
params.trajectory = planarQuadRotorThrustFullAct_trajectoryParams(params.sim.tFinal);
params.trajectory.trajectoryFunc = simParams.trajectoryFunc;

function trajectoryParams = planarQuadRotorThrustFullAct_trajectoryParams(tFinal)

t1 = tFinal/3;
t2 = 2*tFinal/3;
t3 = tFinal;
  
states0 = [0; 0; 0*pi/180; 0; 0; 0]; %load('ICs.mat')


% y trajectory 
yTrajectory = trajectory;
% yTrajectory.type = 'minJerk';
% yTrajectory.wayPoints = [ states0(1); 100];
% yTrajectory.velocities = [states0(4); 0 ];
% yTrajectory.tVec = [0; tFinal];
% yTrajectory.accelerations = [0; 0];
yTrajectory.type = 'cos';
yTrajectory.A = 5;
ytrajectory.w = 0.5;



% z Trajectory 
zTrajectory = trajectory;
% zTrajectory.type = 'constantPos';
% zTrajectory.wayPoints = states0(2);
zTrajectory.type = 'sin';
zTrajectory.A = 5;
ztrajectory.w = .5;

  
% qBx Trajectory 
qBxTrajectory = trajectory;
qBxTrajectory.type = 'minJerk';
qBxTrajectory.tVec = [0; t1;  t2; t3];
qBxTrajectory.wayPoints = [ 0; 0; 0; 0];
qBxTrajectory.velocities = [0; 0; 0; 0];
qBxTrajectory.accelerations = [0; 0; 0; 0];



trajectoryParams.numTrajectories = 3;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = yTrajectory;
trajectoryParams.trajectories{2} = zTrajectory;
trajectoryParams.trajectories{3} = qBxTrajectory;



end




end