function [statesDot, addionalOutputs] = planarQuadRotorThrustFullAct_dynamics(t, states, params, inputFunc)
u = inputFunc(t, states);
F2 = u(1);
F4= u(2);
F5 = u(3);
mB = params.mB;
y = states(1);
z = states(2);
qBx = states(3);
yDt = states(4);
zDt = states(5); 
qBxDt = states(6);
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBzx = params.IB(3,1);
lA = params.lA;
qP2 = params.qP2;
qP4 = params.qP4; 
qP5 = params.qP5;

bThrust = params.bThrust;

[~, BodyOutput] = planarQuadRotorThrustFullAct_matlab( F2, F4, F5, qBx, yDt, zDt, ...
    qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, lA, qP2, qP4, qP5 );

animationOutputs = planarQuadRotorThrustAnimationOutputs( lA, qBx, y, z );
inputForces = u';
propRates = thrust2rates( F2, F4, F5, bThrust );

addionalOutputs = [animationOutputs, inputForces, propRates];
statesDot = [BodyOutput'];
end