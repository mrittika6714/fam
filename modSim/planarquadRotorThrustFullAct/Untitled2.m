clc;clear; close all;
qBody = linspace(0, pi,25);
Bcmy = 0;
Bcmz = 0;
params = [];
L = 1;
W = 0.1;
X = [-L/2  L/2 L/2  -L/2] ;
Y = [-W/2  -W/2  W/2  W/2] ;
center = [0;0];
vehicleColors = {'k','k' };



vehicleBody = planarQuadrotorThrustFullAct_vehicleDrawing(Bcmy, Bcmz, qBody(1), params);
fieldNames = fieldnames(vehicleBody);
numPlots = length(fieldNames);
figure;
hold on
axis equal
plotHandles = matlab.graphics.primitive.Polygon.empty(0,numPlots);
for i = 1:numPlots
  plotHandles(i) = plot(vehicleBody.(fieldNames{i}),'FaceAlpha',1, 'FaceColor', 'k');
end

for i = 1:length(qBody)
  vehicleBody = planarQuadrotorThrustFullAct_vehicleDrawing(Bcmy, Bcmz, qBody(i), params);  
  for i = 1:numPlots
    plotHandles(i).Shape = vehicleBody.(fieldNames{i});
  end
  drawnow
end

