%% Setup workspace and define System Paramaters
clc; clear; close all
clear planarQuadRotorThrustController
%trajectoryGenFolder = '\\snl\home\mrittik\Matlab\Trajectory Generation';
trajectoryGenFolder = '..';
addpath(genpath(trajectoryGenFolder))
clear GenerateTrajectoryPoint
videoName = 'testVideo';
model= 'planarQuadRotorThrustFullAct';
controllerType = 'LQR';
plotResults = true;
savePlots = true;
animate = true;
saveAnimation = true;
saveData = true;

LQR = true;
PID = false;
trajectoryType = 'circle';
tStep = 0.01;
tFinal = 30;
tSpan = 0:tStep:tFinal;
opts = odeset('RelTol',1e-6,'AbsTol',1e-6);
load('params.mat')
x0 = [0; 0; 15*pi/180; 0; 0; 0];
xDesired = [ 5; 0 ; pi/8; 0; 0; 0];
numInputs = 3;
stateNames = {'y', 'z','q','yDot', 'zDOt','qDot'};

if strcmp(trajectoryType, 'minJerk')
t1 = 1;
t2 = 2;
t3 = tFinal;
trajectories.type =  trajectoryType ;
trajectories.trajectoryIndex = 3; % fieldname index in which trajectories start
trajectories.numTrajectories = 3;
trajectories.ytrajectory.tVec = [0; t1;  t2; t3];
trajectories.ytrajectory.wayPoints = [ 0; 5; 0; 5];
trajectories.ytrajectory.velocities = [0; 0; 0; 0];
trajectories.ytrajectory.accelerations = [0; 0; 0; 0];
trajectories.ztrajectory.tVec = [0; t1;  t2; t3];
trajectories.ztrajectory.wayPoints = [ 0; 0; 0; 0];
trajectories.ztrajectory.velocities = [0; 0; 0; 0];
trajectories.ztrajectory.accelerations = [0; 0; 0; 0];
trajectories.qtrajectory.tVec = [0; t1;  t2; t3];
trajectories.qtrajectory.wayPoints = [15*pi/180; 15*pi/180; 15*pi/180; 15*pi/180];
trajectories.qtrajectory.velocities = [0; 0; 0; 0];
trajectories.qtrajectory.accelerations = [0; 0; 0; 0];

elseif strcmp(trajectoryType, 'sine')
  tStep = .01; 
  tInitial = 0;
  tFinal = 10;
  t = (0:tStep:tFinal)';
  omega = 15*pi/180;
  trajectories.type = trajectoryType ;
  trajectories.omega = omega;
  trajectories.qBx = 5*pi/180;


elseif strcmp(trajectoryType, 'circle')
  tStep = .01; 
  tInitial = 0;
  tFinal = 10;
  t = (0:tStep:tFinal)';
  omega = 60*pi/180;
  trajectories.type =  trajectoryType ;
  trajectories.omega = omega;
  trajectories.qBx = 15*pi/180;
  trajectories.r = 2;
else
  
  error('Trajectory type not handeled')
end


%% Uncontrolled Dynamics 
params.controller = 'none';
setPoint = xDesired;
trajectoryFunc = @(trajectories, t) planarQuadRotorThrustFullAct_trajGen(trajectories, t);
u = @(t, states) planarQuadRotorThrustFullAct_controller(t, states, trajectories, trajectoryFunc, params);
odeFunc = @(t,states) planarQuadRotorThrustFullAct_dynamics(t, states, params, u);
[tUncon, xUncon] = ode45(odeFunc, tSpan, x0, opts);

%% Linear Quadratic Regulator Controller (linearized about PI)
if(LQR)
disp('Beginning simulation with LQR Control');
load('Klqr.mat')
load('uEq.mat')
params.controller = controllerType;
params.Klqr = Klqr;
params.uEq = uEqVal;
u = @(t, states) planarQuadRotorThrustFullAct_controller(t, states, trajectories, trajectoryFunc, params);
odeFunc = @(t,states) planarQuadRotorThrustFullAct_dynamics(t, states, params, u);
[results.t, results.states] = ode45(odeFunc, tSpan, x0, opts);
clear GenerateTrajectoryPoint
[results.posDes, results.vDes, results.aDes] = trajectoryFunc(trajectories, results.t);

numPoints = length(results.t);
for i = 1 :  numPoints
    [~,results.additionalOutputs(i,:)] = planarQuadRotorThrustFullAct_dynamics(results.t(i), results.states(i,:)', params, u);
end

end
%% PID Controller 
if PID
disp('Beginning simulation with PID Control');
load('uEq.mat');
params.uEq = uEqVal;
Kpid = [0 0 0];
params.mode = 'PID';
params.Kpid = Kpid;
statesDesired = xDesired;
params.uEq = uEqVal;
trajectoryFuncHandle = @(trajectories, t) GenerateTrajectoryPoint(trajectories, t);
u = @(t, states) planarQuadRotorThrustController(t, states, trajectories, trajectoryFuncHandle, params);
odeFunc = @(t,states) planarQuadRotorThrustDynamics(t, states, params, u);
[tPID, xPID] = ode45(odeFunc, tSpan, x0);
 
%  
numPoints = length(tPID);
extraOutputsPID=zeros(numPoints, 4);
inputForcesPID = zeros(numPoints, 2);
propRatesPID = inputForcesPID;
for i = 1 :  numPoints
    [~,extraOutputsPID(i,:),inputForcesPID(i,:),propRatesPID(i,:) ] = planarQuadRotorThrustDynamics(tPID(i), xPID(i,:)', params, u);
end


end
%% Create Results Folder
resultsFolderName = 'generated';
if ~exist(resultsFolderName, 'dir')
  mkdir(resultsFolderName);
end

%% Plot Responses
if plotResults == 1
  params.resultsFolder = resultsFolderName;
  planarQuadRotorThrustFullAct_plot(results, params, savePlots);
end

%% Animate Responses
if animate == 1
  videoName = fullfile(resultsFolderName, videoName);
  params.videoName = videoName;
  planarQuadRotorThrustFullAct_animation(results, params, saveAnimation);
end

%% Save Data
if saveData
  resultsFileName = [model, '_', controllerType, '_', trajectoryType];
    save([resultsFolderName,'\',resultsFileName], 'results');
end