function JVel_N_2DOF = RzRzRzManip_J_2DOF( Len1, Len2, Len3, q1, q2, q3 )
if( nargin ~= 6 ) error( 'RzRzRzManip_J_2DOF expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_J_2DOF.m created Jun 10 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
JVel_N_2DOF = zeros( 2, 3 );
z = zeros( 1, 144 );




%===========================================================================


%===========================================================================
Output = [];

JVel_N_2DOF(1,1) = -Len1*sin(q1) - Len2*sin(q1+q2) - Len3*sin(q1+q2+q3);
JVel_N_2DOF(1,2) = -Len2*sin(q1+q2) - Len3*sin(q1+q2+q3);
JVel_N_2DOF(1,3) = -Len3*sin(q1+q2+q3);
JVel_N_2DOF(2,1) = Len1*cos(q1) + Len2*cos(q1+q2) + Len3*cos(q1+q2+q3);
JVel_N_2DOF(2,2) = Len2*cos(q1+q2) + Len3*cos(q1+q2+q3);
JVel_N_2DOF(2,3) = Len3*cos(q1+q2+q3);


%===========================================
end    % End of function RzRzRzManip_J_2DOF
%===========================================
