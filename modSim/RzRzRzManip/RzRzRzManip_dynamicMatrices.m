function [NMat, MMat, GMat, VMat, JVel_N, JDot_Vel_N] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dt, q2Dt, q3Dt )
if( nargin ~= 16 ) error( 'RzRzRzManip_dynamicMatrices expects 16 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_dynamicMatrices.m created Aug 05 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
NMat = zeros( 3, 3 );
MMat = zeros( 3, 3 );
GMat = zeros( 3, 1 );
VMat = zeros( 3, 1 );
JVel_N = zeros( 3, 3 );
JDot_Vel_N = zeros( 3, 3 );
z = zeros( 1, 169 );



%===========================================================================
z(3) = cos(q2);
z(4) = sin(q2);
z(5) = cos(q3);
z(6) = sin(q3);
z(10) = Len1*z(4);
z(11) = Len1*z(3);
z(12) = 0.5*Len2 + z(11);
z(18) = z(3)*z(5) - z(4)*z(6);
z(20) = z(3)*z(6) + z(4)*z(5);
z(21) = Len1*z(20) + Len2*z(6);
z(22) = Len2*z(6);
z(23) = Len1*z(18) + Len2*z(5);
z(24) = Len2*z(5);
z(25) = 0.5*Len3 + z(23);
z(26) = 0.5*Len3 + z(24);
z(41) = m2*Len2;
z(51) = m3*Len3;
z(93) = IL2zz + IL3zz;
z(119) = IL1zz + IL2zz + IL3zz + 0.25*m1*Len1^2;
z(120) = z(119) + m2*(z(10)^2+z(12)^2) + m3*(z(21)^2+z(25)^2);
z(121) = z(93) + 0.5*z(41)*z(12) + m3*(z(21)*z(22)+z(25)*z(26));
z(122) = IL3zz + 0.5*z(51)*z(25);
z(124) = IL2zz + IL3zz + 0.25*m2*Len2^2;
z(125) = z(124) + m3*(z(22)^2+z(26)^2);
z(126) = IL3zz + 0.5*z(51)*z(26);
z(128) = IL3zz + 0.25*m3*Len3^2;



%===========================================================================
Output = [];

NMat(1,1) = 1;
NMat(1,2) = 1;
NMat(1,3) = 1;
NMat(2,1) = 0;
NMat(2,2) = 1;
NMat(2,3) = 1;
NMat(3,1) = 0;
NMat(3,2) = 0;
NMat(3,3) = 1;

MMat(1,1) = z(120);
MMat(1,2) = z(121);
MMat(1,3) = z(122);
MMat(2,1) = z(121);
MMat(2,2) = z(125);
MMat(2,3) = z(126);
MMat(3,1) = z(122);
MMat(3,2) = z(126);
MMat(3,3) = z(128);

GMat(1) = 0.5*g*(m1*Len1*cos(q1)+m2*(2*Len1*sin(q2)*sin(q1+q2)+cos(q1+q2)*(Len2+2*Len1*cos(q2)))+m3*(2*sin(q1+q2+q3)*(Len2*sin(q3)+  ...
Len1*sin(q2+q3))+cos(q1+q2+q3)*(Len3+2*Len2*cos(q3)+2*Len1*cos(q2+q3))));
GMat(2) = 0.5*g*(m2*Len2*cos(q1+q2)+m3*(2*Len2*sin(q3)*sin(q1+q2+q3)+cos(q1+q2+q3)*(Len3+2*Len2*cos(q3))));
GMat(3) = 0.5*m3*g*Len3*cos(q1+q2+q3);

VMat(1) = 0.5*m2*Len1*Len2*sin(q2)*(q1Dt^2-(q1Dt+q2Dt)^2) + 0.5*m3*((Len3+2*Len2*cos(q3)+2*Len1*cos(q2+q3))*(Len1*sin(q2+q3)*q1Dt^2+  ...
Len2*sin(q3)*(q1Dt+q2Dt)^2)-(Len2*sin(q3)+Len1*sin(q2+q3))*(Len3*(q1Dt+q2Dt+q3Dt)^2+2*Len1*cos(q2+q3)*q1Dt^2+2*Len2*cos(q3)*(q1Dt+  ...
q2Dt)^2));
VMat(2) = 0.5*m2*Len1*Len2*sin(q2)*q1Dt^2 + 0.5*m3*((Len3+2*Len2*cos(q3))*(Len1*sin(q2+q3)*q1Dt^2+Len2*sin(q3)*(q1Dt+q2Dt)^2)-Len2*  ...
sin(q3)*(Len3*(q1Dt+q2Dt+q3Dt)^2+2*Len1*cos(q2+q3)*q1Dt^2+2*Len2*cos(q3)*(q1Dt+q2Dt)^2));
VMat(3) = 0.5*m3*Len3*(Len1*sin(q2+q3)*q1Dt^2+Len2*sin(q3)*(q1Dt+q2Dt)^2);

JVel_N(1,1) = -Len1*sin(q1) - Len2*sin(q1+q2) - Len3*sin(q1+q2+q3);
JVel_N(1,2) = -Len2*sin(q1+q2) - Len3*sin(q1+q2+q3);
JVel_N(1,3) = -Len3*sin(q1+q2+q3);
JVel_N(2,1) = Len1*cos(q1) + Len2*cos(q1+q2) + Len3*cos(q1+q2+q3);
JVel_N(2,2) = Len2*cos(q1+q2) + Len3*cos(q1+q2+q3);
JVel_N(2,3) = Len3*cos(q1+q2+q3);
JVel_N(3,1) = 1;
JVel_N(3,2) = 1;
JVel_N(3,3) = 1;

JDot_Vel_N(1,1) = -Len1*cos(q1)*q1Dt - Len2*cos(q1+q2)*(q1Dt+q2Dt) - Len3*cos(q1+q2+q3)*(q1Dt+q2Dt+q3Dt);
JDot_Vel_N(1,2) = -Len2*cos(q1+q2)*(q1Dt+q2Dt) - Len3*cos(q1+q2+q3)*(q1Dt+q2Dt+q3Dt);
JDot_Vel_N(1,3) = -Len3*cos(q1+q2+q3)*(q1Dt+q2Dt+q3Dt);
JDot_Vel_N(2,1) = -Len1*sin(q1)*q1Dt - Len2*sin(q1+q2)*(q1Dt+q2Dt) - Len3*sin(q1+q2+q3)*(q1Dt+q2Dt+q3Dt);
JDot_Vel_N(2,2) = -Len2*sin(q1+q2)*(q1Dt+q2Dt) - Len3*sin(q1+q2+q3)*(q1Dt+q2Dt+q3Dt);
JDot_Vel_N(2,3) = -Len3*sin(q1+q2+q3)*(q1Dt+q2Dt+q3Dt);
JDot_Vel_N(3,1) = 0;
JDot_Vel_N(3,2) = 0;
JDot_Vel_N(3,3) = 0;


%====================================================
end    % End of function RzRzRzManip_dynamicMatrices
%====================================================
