function  RzRzRzManip_plot(plotData, params, savePlots)

numTrajectories = params.trajectoryParams.numTrajectories;
styleVec ={'k-','r--','y-' };
controllerSpace = params.controller.space;
if (strcmp(controllerSpace, 'Task'))  
%if strcmp(params.controller.controllerType, 'FF_Task') || strcmp(params.controller.controllerType, 'FF_Task_PID')|| strcmp(params.controller.controllerType, 'FF_Null')
   taskPos = plotData.additionalOutputs(:,13:15);
   taskVel = plotData.additionalOutputs(:,16:18);
   desiredStates = [taskPos, taskVel];
else
   desiredStates = plotData.states;
end
desiredTrajectory = [plotData.posDes, plotData.vDes];

stateError = desiredStates - desiredTrajectory ;
trajectories = figure;
hold on
for i = 1:numTrajectories
plot(plotData.t, desiredStates(:,i),styleVec{1})
hold on
plot(plotData.t, desiredTrajectory(:,i), styleVec{2})

title(' Trajectory Time Histories');
xlabel('Time [s]');
ylabel('y [units]');
legend( 'Actual','Desired')
end

error = figure;
plot(plotData.t, stateError)
if savePlots
  saveas(trajectories, fullfile(params.resultsFolder,['trajectories','.png'] ));
end

end