function control = RzRzRzManip_controller(t, states, trajectories, trajectoryFunc, params)

% Unpack Params
m1 = params.m1;
m2 = params.m2;
m3 = params.m3;
IL1zz = params.IL1zz;
IL2zz = params.IL2zz;
IL3zz = params.IL3zz;
Len1 = params.Len1;
Len2 = params.Len2;
Len3 = params.Len3;
g = params.g;
     
% Unpack states
q1 = states(1);
q2 = states(2);
q3 = states(3);

q1Dot = states(4);
q2Dot = states(5);
q3Dot = states(6);
qDot = states(4:6);

controllerType = params.controller.type;
controllerSpace = params.controller.space;

% Unpack Desired Trajectory 
if (strcmp(controllerSpace, 'Task'))  
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
    xDes = posDes(1);
    xDotDes = vDes(1);
    xDDotDes = aDes(1);
    yDes = posDes(2);
    yDotDes = vDes(2);
    yDDotDes = aDes(2);
    qDes =  posDes(3);
    qDotDes= vDes(3); 
    qDDotDes = aDes(3);
    [taskPos,taskVel] = RzRzRzManip_ForwardKinematics( q1, q2, q3, q1Dot, q2Dot, q3Dot, Len1, Len2, Len3, g );
    error = posDes' - taskPos;
    errorDot = vDes' - taskVel;
else
    [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
    q1Des = posDes(1);
    q1DotDes = vDes(1);
    q1DDotDes = aDes(1);
    q2Des = posDes(2);
    q2DotDes = vDes(2);
    q2DDotDes = aDes(2);
    q3Des =  posDes(3);
    q3DotDes= vDes(3); 
    q3DDotDes = aDes(3);
    % Calculate errors
    error = posDes' - states(1:3);
    errorDot = vDes' - states(4:6);

end
    

if strcmp(controllerType, 'none')
    control = zeros(3, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;
elseif strcmp(controllerType, 'PID_Task')
    kp = params.controller.kp;
    kd = params.controller.kd;
    [NMat, MMat, ~, ~, J, ~] = hexRotorRRManip_dynamicMatrices( vBx, vBy, vBz, qBx, qBy, qBz, wBx, wBy, wBz, mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, ...
    xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, ...
    qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, ...
    IL1xx, IL1yy, IL1zz, IL2xx, IL2yy, IL2zz, len1, len2, mL1, mL2, qL1, qL2, qL1Dt, qL2Dt );
    %     https://www.mecharithm.com/computed-torque-control-of-a-3r-robot-arm-playing-ping-pong/
    % https://menloservice.sandia.gov/https://studywolf.wordpress.com/2013/09/17/robot-control-4-operation-space-control/
     aCon =  aDes'+kd*errorDot + kp * error;
     control = pinv(NMat)*(MMat * pinv(J) *(aCon ));
     
  
elseif strcmp(controllerType, 'FF_Joint')
     [NMat, MMat, GMat, VMat, ~, ~] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
     control = pinv(NMat) * (MMat * aDes' + GMat + VMat); 
    %control = RzRzRzManip_MGInverseDynamics( q1, q2, q3, q1Dot, q2Dot, q3Dot, ...
     %    q1DDotDes, q2DDotDes, q3DDotDes, m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, ...
     %    Len2, Len3, g );
     
elseif strcmp(controllerType, 'FF_Joint_PID')
    kp = params.controller.kp;
    kd = params.controller.kd;
    a_q_Control = aDes' + kd*errorDot + kp * error; % control signal for acceleration in joint space
    [NMat, MMat, GMat, VMat, J, JDot] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
    control = pinv(NMat) * (MMat * a_q_Control +GMat + Vmat);
  
elseif strcmp(controllerType, 'FF_Joint_PID2')
    kp = params.controller.kp;
    kd = params.controller.kd;
    a_q_Control = aDes' + kd*errorDot + kp * error; % control signal for acceleration in joint space
    q1DDot_Con = a_q_Control(1);
    q2DDot_Con = a_q_Control(2);
    q3DDot_Con = a_q_Control(3);
    control = RzRzRzManip_MGInverseDynamics( q1, q2, q3, q1Dot, q2Dot, q3Dot, ...
       q1DDot_Con, q2DDot_Con, q3DDot_Con, m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, ...
      Len2, Len3, g );

elseif strcmp(controllerType, 'FF_Task')
    [NMat, MMat, GMat, CMat, J, JDot] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
     %control = J' * F;
     control = pinv(NMat)*(MMat * pinv(J) *(aDes' - JDot*pinv(J)*vDes') + CMat + GMat);
     %a_q = pinv(J)*(aDes'-JDot*pinv(J)*vDes');
     %control = pinv(NMat) * (MMat * a_q + GMat + CMat); 
%     control = RzRzRzManip_MGInverseDynamics( q1, q2, q3, q1Dot, q2Dot, q3Dot, ...
%          a_q(1), a_q(2), a_q(3), m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, ...
%          Len2, Len3, g );

elseif strcmp(controllerType, 'FF_Task_PID')
    kp = params.controller.kp;
    kd = params.controller.kd;
    [NMat, MMat, GMat, CMat, J, JDot] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
    %     https://www.mecharithm.com/computed-torque-control-of-a-3r-robot-arm-playing-ping-pong/
    % https://menloservice.sandia.gov/https://studywolf.wordpress.com/2013/09/17/robot-control-4-operation-space-control/
     aCon =  aDes'+kd*errorDot + kp * error;
     M_tilda = pinv(J*pinv(MMat)*J');
     C_tilda = pinv(J*pinv(MMat)*J')* (J*pinv(MMat)*CMat - JDot*qDot); 
     G_tilda = pinv(J*pinv(MMat)*J')* J*pinv(MMat) *GMat;
     F = M_tilda*aCon + C_tilda +G_tilda;
     
     control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes') + CMat + GMat);
     
     
elseif strcmp(controllerType, 'FF_Null')
       % 3.2.3 of Nakanishi, Cory, Mistry, Peters, and Schaal / Operational Space Control: A Theoretical and Empirical Comparison
    kp = params.controller.kp;
    kd = params.controller.kd;
    [NMat, MMat, GMat, CMat, J, JDot] = RzRzRzManip_dynamicMatrices( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, q1Dot, q2Dot, q3Dot );
    %     https://www.mecharithm.com/computed-torque-control-of-a-3r-robot-arm-playing-ping-pong/
    % https://menloservice.sandia.gov/https://studywolf.wordpress.com/2013/09/17/robot-control-4-operation-space-control/
    J = J(1:2,:);
    JDot = JDot(1:2,:);
    aCon =  aDes'+kd*errorDot + kp * error;
    aCon = aCon(1:2);
     qPos = [ pi/8; pi/8; pi/8];
     %control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes(1:2)') + CMat + GMat);
    control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes(1:2)') + CMat + GMat) + (eye(3,3) - pinv(J)*J) * (0.1*(qPos-[q1; q2; q3]));
else  
  error('Controller Type not handled');
end 

end