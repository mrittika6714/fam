clc; clear; close all;
Len1 = 1;
Len2 = 1;
Len3 = 1;
qMin = 0*[ -90, -60, -75] * pi/180;
qMax = [360, 360, 360] * pi/180;
numPoints = 75;

q1 = linspace(qMin(1), qMax(1), numPoints);
q2 = linspace(qMin(2), qMax(2), numPoints);
q3 = linspace(qMin(3), qMax(3), numPoints);
currentPoint = 1;
rEE_No_N = zeros(numPoints, 3);
for i = 1:numPoints
  for ii = 1:numPoints
    for iii = 1:numPoints
      rEE_No_N(currentPoint,:)  = RzRzRzManip_EEPosition( Len1, Len2, Len3, q1(i), q2(ii), q3(iii) );
      currentPoint = currentPoint +1;
    end
  end
end

plot(rEE_No_N(:,1), rEE_No_N(:,2), 'k.')