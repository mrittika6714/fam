function RzRzRzManip_func( g, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, m1, m2, m3, q1, q2, q3, T1, T2, T3, q1Dt, q2Dt, q3Dt )
if( nargin ~= 19 ) error( 'RzRzRzManip_func expects 19 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_func.m created Jun 10 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
func = zeros( 6, 1 );
z = zeros( 1, 144 );




%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(3) = cos(q2);
z(4) = sin(q2);
z(5) = cos(q3);
z(6) = sin(q3);
z(7) = q1Dt + q2Dt;
z(8) = Len1*q1Dt;
z(9) = q1Dt*z(8);
z(10) = Len1*z(4);
z(11) = Len1*z(3);
z(12) = 0.5*Len2 + z(11);
z(13) = z(3)*z(9);
z(14) = z(4)*z(9);
z(15) = Len2*(q1Dt+q2Dt);
z(16) = -z(13) - 0.5*z(7)*z(15);
z(17) = z(7)*z(15);
z(18) = z(3)*z(5) - z(4)*z(6);
z(19) = -z(3)*z(6) - z(4)*z(5);
z(20) = z(3)*z(6) + z(4)*z(5);
z(21) = Len1*z(20) + Len2*z(6);
z(22) = Len2*z(6);
z(23) = Len1*z(18) + Len2*z(5);
z(24) = Len2*z(5);
z(25) = 0.5*Len3 + z(23);
z(26) = 0.5*Len3 + z(24);
z(27) = -z(5)*z(17) - z(18)*z(9);
z(28) = z(6)*z(17) - z(19)*z(9);
z(29) = Len3*(q1Dt+q2Dt+q3Dt);
z(30) = q1Dt + q2Dt + q3Dt;
z(31) = z(27) - 0.5*z(29)*z(30);
z(33) = m1*g;
z(34) = m2*g;
z(35) = m3*g;
z(41) = m2*Len2;
z(51) = m3*Len3;
z(57) = Len1*z(33);
z(59) = z(1)*z(3) - z(2)*z(4);
z(60) = z(1)*z(4) + z(2)*z(3);
z(64) = Len2*z(34);
z(67) = z(5)*z(60) + z(6)*z(59);
z(69) = z(5)*z(59) - z(6)*z(60);
z(74) = Len3*z(35);
z(90) = z(51)*z(28);
z(93) = IL2zz + IL3zz;
z(119) = IL1zz + IL2zz + IL3zz + 0.25*m1*Len1^2;
z(120) = z(119) + m2*(z(10)^2+z(12)^2) + m3*(z(21)^2+z(25)^2);
z(121) = z(93) + 0.5*z(41)*z(12) + m3*(z(21)*z(22)+z(25)*z(26));
z(122) = IL3zz + 0.5*z(51)*z(25);
z(123) = m2*(z(10)*z(16)+z(12)*z(14)) + m3*(z(21)*z(31)+z(25)*z(28));
z(124) = IL2zz + IL3zz + 0.25*m2*Len2^2;
z(125) = z(124) + m3*(z(22)^2+z(26)^2);
z(126) = IL3zz + 0.5*z(51)*z(26);
z(127) = 0.5*z(41)*z(14) + m3*(z(22)*z(31)+z(26)*z(28));
z(128) = IL3zz + 0.25*m3*Len3^2;
z(129) = T1 + T2 + T3 - 0.5*z(57)*z(1) - z(34)*(z(10)*z(60)+z(12)*z(59)) - z(35)*(z(21)*z(67)+z(25)*z(69));
z(130) = T2 + T3 - 0.5*z(64)*z(59) - z(35)*(z(22)*z(67)+z(26)*z(69));
z(131) = T3 - 0.5*z(74)*z(69);
z(132) = z(123) - z(129);
z(133) = z(127) - z(130);
z(134) = 0.5*z(90) - z(131);
z(135) = z(120)*z(125) - z(121)^2;
z(136) = z(120)*z(126) - z(121)*z(122);
z(137) = z(121)*z(126) - z(122)*z(125);
z(138) = z(128)*z(135) + z(122)*z(137) - z(126)*z(136);
z(139) = z(128)*z(125) - z(126)^2;
z(140) = z(128)*z(121) - z(122)*z(126);
z(141) = z(128)*z(120) - z(122)^2;
z(142) = (z(137)*z(134)+z(139)*z(132)-z(140)*z(133))/z(138);
z(143) = (z(136)*z(134)+z(140)*z(132)-z(141)*z(133))/z(138);
z(144) = (z(136)*z(133)-z(135)*z(134)-z(137)*z(132))/z(138);



%===========================================================================
Output = [];

func(1) = q1Dt;
func(2) = q2Dt;
func(3) = q3Dt;
func(4) = -z(142);
func(5) = z(143);
func(6) = z(144);


%=========================================
end    % End of function RzRzRzManip_func
%=========================================
