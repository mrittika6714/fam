function trajectoryParams = RzRzRzManip_trajectoryParams(controllerSpace)

tFinal = 30;

%if (strcmp(controllerType, 'FF_Task') || strcmp(controllerType, 'FF_Task_PID') || strcmp(controllerType, 'FF_Null'))
if (strcmp(controllerSpace, 'Task'))
    states0 = [0; pi/8; 0; 0; 0; 0]; %load('ICs.mat')
    %initialOffset = [pi/9; 0; -pi/12; 0; 0.1; -0.2];
    %initialOffset = zeros(6,1);
    % x trajectory 
    xTrajectory = trajectory;
    xTrajectory.type = 'minJerk';
    xTrajectory.wayPoints = [ 1; 0; 1; 1];
    xTrajectory.velocities = [0; 0; 0; 0 ];
    xTrajectory.tVec = [0; tFinal/3; 2*tFinal/3; tFinal];
    xTrajectory.accelerations = [0; 0; 0; 0];

    % y trajectory 
    yTrajectory = trajectory;
    yTrajectory.type = 'minJerk';
    yTrajectory.wayPoints = [ 2; 2; 2; -0.25];
    yTrajectory.velocities = [0; 0; 0; 0 ];
    yTrajectory.tVec = [0; tFinal/3; 2*tFinal/3; tFinal];
    yTrajectory.accelerations = [0; 0; 0; 0];

    % qTask trajectory 
    qTrajectory = trajectory;
    qTrajectory.type = 'minJerk';
    qTrajectory.wayPoints = [ pi/2; pi/4; pi/4; pi/4];
    qTrajectory.velocities = [0; 0; 0; 0 ];
    qTrajectory.tVec = [0; tFinal/3; 2*tFinal/3; tFinal];
    qTrajectory.accelerations = [0; 0; 0; 0];

    trajectoryParams.numTrajectories = 3;
    trajectoryParams.ICs = states0;
    trajectoryParams.trajectories{1} = xTrajectory;
    trajectoryParams.trajectories{2} = yTrajectory;
    trajectoryParams.trajectories{3} = qTrajectory;
    
else
    states0 = [pi/6; pi/6; pi/6; 0; 0; 0]; %load('ICs.mat')
    initialOffset = [pi/9; 0; -pi/12; 0; 0.1; -0.2];
    %initialOffset = zeros(6,1);
    % q1 trajectory 
    q1Trajectory = trajectory;
    q1Trajectory.type = 'minJerk';
    q1Trajectory.wayPoints = [ states0(1)+initialOffset(1); -pi/8];
    q1Trajectory.velocities = [states0(4)+initialOffset(4); 0 ];
    q1Trajectory.tVec = [0; tFinal];
    q1Trajectory.accelerations = [0; 0];

    % q2 trajectory 
    q2Trajectory = trajectory;
    q2Trajectory.type = 'minJerk';
    q2Trajectory.wayPoints = [ states0(2)+initialOffset(2); pi/2];
    q2Trajectory.velocities = [states0(5)++initialOffset(5); 0 ];
    q2Trajectory.tVec = [0; tFinal];
    q2Trajectory.accelerations = [0; 0];

    % q3 trajectory 
    q3Trajectory = trajectory;
    q3Trajectory.type = 'minJerk';
    q3Trajectory.wayPoints = [ states0(3)+initialOffset(3); 0];
    q3Trajectory.velocities = [states0(6)+initialOffset(6); 0 ];
    q3Trajectory.tVec = [0; tFinal];
    q3Trajectory.accelerations = [0; 0];

    trajectoryParams.numTrajectories = 3;
    trajectoryParams.ICs = states0;
    trajectoryParams.trajectories{1} = q1Trajectory;
    trajectoryParams.trajectories{2} = q2Trajectory;
    trajectoryParams.trajectories{3} = q3Trajectory;

end
end