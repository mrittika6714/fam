function [SolutionToAlgebraicEquations] = RzRzRzManip_MGInverseDynamics( q1, q2, q3, q1Dt, q2Dt, q3Dt, q1DDt, q2DDt, q3DDt, m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, g )
if( nargin ~= 19 ) error( 'RzRzRzManip_MGInverseDynamics expects 19 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_MGInverseDynamics.m created Aug 02 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 147 );



%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(3) = cos(q2);
z(4) = sin(q2);
z(5) = cos(q3);
z(6) = sin(q3);
z(7) = q1Dt + q2Dt;
z(8) = Len1*q1Dt;
z(9) = q1Dt*z(8);
z(10) = Len1*z(4);
z(11) = Len1*z(3);
z(12) = 0.5*Len2 + z(11);
z(13) = z(3)*z(9);
z(14) = z(4)*z(9);
z(15) = Len2*(q1Dt+q2Dt);
z(16) = -z(13) - 0.5*z(7)*z(15);
z(17) = z(7)*z(15);
z(18) = z(3)*z(5) - z(4)*z(6);
z(19) = -z(3)*z(6) - z(4)*z(5);
z(20) = z(3)*z(6) + z(4)*z(5);
z(21) = Len1*z(20) + Len2*z(6);
z(22) = Len2*z(6);
z(23) = Len1*z(18) + Len2*z(5);
z(24) = Len2*z(5);
z(25) = 0.5*Len3 + z(23);
z(26) = 0.5*Len3 + z(24);
z(27) = -z(5)*z(17) - z(18)*z(9);
z(28) = z(6)*z(17) - z(19)*z(9);
z(29) = Len3*(q1Dt+q2Dt+q3Dt);
z(30) = q1Dt + q2Dt + q3Dt;
z(31) = z(27) - 0.5*z(29)*z(30);
z(33) = m1*g;
z(34) = m2*g;
z(35) = m3*g;
z(41) = m2*Len2;
z(51) = m3*Len3;
z(57) = Len1*z(33);
z(59) = z(1)*z(3) - z(2)*z(4);
z(60) = z(1)*z(4) + z(2)*z(3);
z(64) = Len2*z(34);
z(67) = z(5)*z(60) + z(6)*z(59);
z(69) = z(5)*z(59) - z(6)*z(60);
z(74) = Len3*z(35);
z(90) = z(51)*z(28);
z(93) = IL2zz + IL3zz;
z(119) = IL1zz + IL2zz + IL3zz + 0.25*m1*Len1^2;
z(120) = z(119) + m2*(z(10)^2+z(12)^2) + m3*(z(21)^2+z(25)^2);
z(121) = z(93) + 0.5*z(41)*z(12) + m3*(z(21)*z(22)+z(25)*z(26));
z(122) = IL3zz + 0.5*z(51)*z(25);
z(123) = m2*(z(10)*z(16)+z(12)*z(14)) + m3*(z(21)*z(31)+z(25)*z(28));
z(124) = IL2zz + IL3zz + 0.25*m2*Len2^2;
z(125) = z(124) + m3*(z(22)^2+z(26)^2);
z(126) = IL3zz + 0.5*z(51)*z(26);
z(127) = 0.5*z(41)*z(14) + m3*(z(22)*z(31)+z(26)*z(28));
z(128) = IL3zz + 0.25*m3*Len3^2;
z(145) = -0.5*z(57)*z(1) - z(34)*(z(10)*z(60)+z(12)*z(59)) - z(35)*(z(21)*z(67)+z(25)*z(69)) - z(123) - z(120)*q1DDt - z(121)*q2DDt -   ...
z(122)*q3DDt;
z(146) = -0.5*z(64)*z(59) - z(35)*(z(22)*z(67)+z(26)*z(69)) - z(127) - z(121)*q1DDt - z(125)*q2DDt - z(126)*q3DDt;
z(147) = -0.5*z(74)*z(69) - 0.5*z(90) - z(128)*q3DDt - z(122)*q1DDt - z(126)*q2DDt;

COEF = zeros( 3, 3 );
COEF(1,1) = -1;
COEF(1,2) = -1;
COEF(1,3) = -1;
COEF(2,2) = -1;
COEF(2,3) = -1;
COEF(3,3) = -1;
RHS = zeros( 1, 3 );
RHS(1) = z(145);
RHS(2) = z(146);
RHS(3) = z(147);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
T1 = SolutionToAlgebraicEquations(1);
T2 = SolutionToAlgebraicEquations(2);
T3 = SolutionToAlgebraicEquations(3);



%======================================================
end    % End of function RzRzRzManip_MGInverseDynamics
%======================================================
