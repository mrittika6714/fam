function params = RzRzRzManip_params(controlParams)

params.Len1 = 1;
params.m1 = 1;
params.IL1zz = 1;

params.Len2 = 1;
params.m2 = 1;
params.IL2zz = 1;


params.Len3 = 1;
params.m3 = 1;
params.IL3zz = 1;

params.g = 0;

params.markerSize = 35;

params.controller.kp = 40;
params.controller.kd = 25;
params.controller.DOF = 2;
params.controller.type = controlParams.type;
params.controller.space = controlParams.space;
params.trajectoryParams = RzRzRzManip_trajectoryParams(params.controller.type);


end