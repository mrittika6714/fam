function [taskPos, taskVel, taskAccel ] = RzRzRzManip_ForwardKinematics( q1, q2, q3, q1Dt, q2Dt, q3Dt, Len1, Len2, Len3, g )
if( nargin ~= 10 ) error( 'RzRzRzManip_ForwardKinematics expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_ForwardKinematics.m created Aug 05 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
taskPos = zeros( 3, 1 );
taskVel = zeros( 3, 1 );
taskAccel = zeros( 3, 1 );
z = zeros( 1, 144 );

%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
q1DDt                           =  0.0;                    % UNITS               Constant
q2DDt                           =  0.0;                    % UNITS               Constant
q3DDt                           =  0.0;                    % UNITS               Constant
%-------------------------------+--------------------------+-------------------+-----------------


%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(3) = cos(q2);
z(4) = sin(q2);
z(5) = cos(q3);
z(6) = sin(q3);
z(7) = q1Dt + q2Dt;
z(8) = Len1*q1Dt;
z(9) = q1Dt*z(8);
z(15) = Len2*(q1Dt+q2Dt);
z(17) = z(7)*z(15);
z(29) = Len3*(q1Dt+q2Dt+q3Dt);
z(30) = q1Dt + q2Dt + q3Dt;
z(32) = z(29)*z(30);
z(59) = z(1)*z(3) - z(2)*z(4);
z(60) = z(1)*z(4) + z(2)*z(3);
z(61) = -z(1)*z(4) - z(2)*z(3);
z(66) = z(5)*z(59) + z(6)*z(61);
z(67) = z(5)*z(60) + z(6)*z(59);
z(68) = z(5)*z(61) - z(6)*z(59);
z(69) = z(5)*z(59) - z(6)*z(60);
qTask = acos(z(66));



%===========================================================================
Output = [];

taskPos(1) = Len1*z(1) + Len2*z(59) + Len3*z(66);
taskPos(2) = Len1*z(2) + Len2*z(60) + Len3*z(67);
taskPos(3) = qTask;

taskVel(1) = Len2*z(61)*(q1Dt+q2Dt) + Len3*z(68)*(q1Dt+q2Dt+q3Dt) - Len1*z(2)*q1Dt;
taskVel(2) = Len1*z(1)*q1Dt + Len2*z(59)*(q1Dt+q2Dt) + Len3*z(69)*(q1Dt+q2Dt+q3Dt);
taskVel(3) = q1Dt + q2Dt + q3Dt;

taskAccel(1) = Len2*z(61)*(q1DDt+q2DDt) + Len3*z(68)*(q1DDt+q2DDt+q3DDt) - z(1)*z(9) - z(59)*z(17) - z(66)*z(32) - Len1*z(2)*q1DDt;
taskAccel(2) = Len1*z(1)*q1DDt + Len2*z(59)*(q1DDt+q2DDt) + Len3*z(69)*(q1DDt+q2DDt+q3DDt) - z(2)*z(9) - z(60)*z(17) - z(67)*z(32);
taskAccel(3) = q1DDt + q2DDt + q3DDt;


%======================================================
end    % End of function RzRzRzManip_ForwardKinematics
%======================================================
