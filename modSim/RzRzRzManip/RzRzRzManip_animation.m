%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function RzRzRzManip_animation(results ,params, saveVideo)

t = results.t;
animationTimeScale = params.animationTimeScale;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;
states = results.states;


% Unpack results
numPoints = length(t);
setPoint = results.posDes;
rL1o_No_N = results.additionalOutputs(:,1:3);
rL2o_No_N = results.additionalOutputs(:,4:6);
rL3o_No_N =results.additionalOutputs(:,7:9);
rEE_No_N =results.additionalOutputs(:,10:12);

if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end

rMax = params.Len1 + params.Len2 + params.Len3;
xMin = -1.1 * rMax;
xMax = 1.1 * rMax;
yMin = -1.1 * rMax;
yMax = 1.1 * rMax;

animationFigure = figure();
L1Plot = plot([rL1o_No_N(1,1); rL2o_No_N(1,1)], [rL1o_No_N(1,2); rL2o_No_N(1,2)], ...
    'k.-','markersize',params.markerSize, 'linewidth',4);
hold on
L2Plot = plot([rL2o_No_N(1,1); rL3o_No_N(1,1)], [rL2o_No_N(1,2); rL3o_No_N(1,2)], ...
    'k.-','markersize',params.markerSize, 'linewidth',4);
L3Plot = plot([rL3o_No_N(1,1); rEE_No_N(1,1)], [rL3o_No_N(1,2); rEE_No_N(1,2)], ...
    'k.-','markersize',params.markerSize, 'linewidth',4);

axis([xMin xMax yMin yMax])
xlabel('x Position [m]');
ylabel('y Position [m]');
title('RzRzRz Manipulator','fontweight', 'bold')

%legend('Actual Position', 'Desired Position')
for i =1:numPoints
  L1Plot.XData = [rL1o_No_N(i,1); rL2o_No_N(i,1)];
  L1Plot.YData =[rL1o_No_N(i,2); rL2o_No_N(i,2)];
  L2Plot.XData = [rL2o_No_N(i,1); rL3o_No_N(i,1)];
  L2Plot.YData =[rL2o_No_N(i,2); rL3o_No_N(i,2)];
  L3Plot.XData = [rL3o_No_N(i,1); rEE_No_N(i,1)];
  L3Plot.YData =[rL3o_No_N(i,2); rEE_No_N(i,2)];
  drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end