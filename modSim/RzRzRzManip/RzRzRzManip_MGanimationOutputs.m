function [rL1o_No_N, rL2o_No_N, rL3o_No_N, rEE_No_N ] = RzRzRzManip_MGanimationOutputs( Len1, Len2, Len3, q1, q2, q3 )
if( nargin ~= 6 ) error( 'RzRzRzManip_MGanimationOutputs expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_MGanimationOutputs.m created Jun 06 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
rL1o_No_N = zeros( 3, 1 );
rL2o_No_N = zeros( 3, 1 );
rL3o_No_N = zeros( 3, 1 );
rEE_No_N = zeros( 3, 1 );
z = zeros( 1, 144 );



%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(3) = cos(q2);
z(4) = sin(q2);
z(5) = cos(q3);
z(6) = sin(q3);
z(59) = z(1)*z(3) - z(2)*z(4);
z(60) = z(1)*z(4) + z(2)*z(3);
z(61) = -z(1)*z(4) - z(2)*z(3);
z(66) = z(5)*z(59) + z(6)*z(61);
z(67) = z(5)*z(60) + z(6)*z(59);



%===========================================================================
Output = [];

rL1o_No_N(:) = 0;

rL2o_No_N(1) = Len1*z(1);
rL2o_No_N(2) = Len1*z(2);
rL2o_No_N(3) = 0;

rL3o_No_N(1) = Len1*z(1) + Len2*z(59);
rL3o_No_N(2) = Len1*z(2) + Len2*z(60);
rL3o_No_N(3) = 0;

rEE_No_N(1) = Len1*z(1) + Len2*z(59) + Len3*z(66);
rEE_No_N(2) = Len1*z(2) + Len2*z(60) + Len3*z(67);
rEE_No_N(3) = 0;


%=======================================================
end    % End of function RzRzRzManip_MGanimationOutputs
%=======================================================
