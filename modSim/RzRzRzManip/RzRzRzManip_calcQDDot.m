function [SolutionToAlgebraicEquations] = RzRzRzManip_calcQDDot( Len1, Len2, Len3, q1, q2, q3, qTaskDDot, xDDot, yDDot, q1Dt, q2Dt, q3Dt )
if( nargin ~= 12 ) error( 'RzRzRzManip_calcQDDot expects 12 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_calcQDDot.m created Aug 03 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 158 );



%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(147) = sin(q1+q2);
z(148) = sin(q1+q2+q3);
z(149) = -Len1*z(2) - Len2*z(147) - Len3*z(148);
z(150) = -Len2*z(147) - Len3*z(148);
z(151) = Len3*z(148);
z(152) = cos(q1+q2+q3);
z(153) = cos(q1+q2);
z(154) = xDDot + (Len2*z(153)+Len3*z(152))*q2Dt*(q1Dt+q2Dt) + Len3*z(152)*q3Dt*(q3Dt+2*q1Dt+2*q2Dt) + q1Dt*(Len1*z(1)*q1Dt+Len2*  ...
z(153)*(q1Dt+q2Dt)+Len3*z(152)*(q1Dt+q2Dt));
z(155) = Len1*z(1) + Len2*z(153) + Len3*z(152);
z(156) = Len2*z(153) + Len3*z(152);
z(157) = Len3*z(152);
z(158) = yDDot + (Len2*z(147)+Len3*z(148))*q2Dt*(q1Dt+q2Dt) + Len3*z(148)*q3Dt*(q3Dt+2*q1Dt+2*q2Dt) + q1Dt*(Len1*z(2)*q1Dt+Len2*  ...
z(147)*(q1Dt+q2Dt)+Len3*z(148)*(q1Dt+q2Dt));

COEF = zeros( 3, 3 );
COEF(1,1) = z(149);
COEF(1,2) = z(150);
COEF(1,3) = -z(151);
COEF(2,1) = z(155);
COEF(2,2) = z(156);
COEF(2,3) = z(157);
COEF(3,1) = 1;
COEF(3,2) = 1;
COEF(3,3) = 1;
RHS = zeros( 1, 3 );
RHS(1) = z(154);
RHS(2) = z(158);
RHS(3) = qTaskDDot;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
q1DDt = SolutionToAlgebraicEquations(1);
q2DDt = SolutionToAlgebraicEquations(2);
q3DDt = SolutionToAlgebraicEquations(3);



%==============================================
end    % End of function RzRzRzManip_calcQDDot
%==============================================
