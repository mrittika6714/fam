function L3_R_N = RzRzRzManip_L3_R_N( q1, q2, q3 )
if( nargin ~= 3 ) error( 'RzRzRzManip_L3_R_N expects 3 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_L3_R_N.m created Jun 10 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
L3_R_N = zeros( 3, 3 );
z = zeros( 1, 146 );




%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(3) = cos(q2);
z(4) = sin(q2);
z(5) = cos(q3);
z(6) = sin(q3);
z(59) = z(1)*z(3) - z(2)*z(4);
z(60) = z(1)*z(4) + z(2)*z(3);
z(61) = -z(1)*z(4) - z(2)*z(3);
z(66) = z(5)*z(59) + z(6)*z(61);
z(67) = z(5)*z(60) + z(6)*z(59);
z(68) = z(5)*z(61) - z(6)*z(59);
z(69) = z(5)*z(59) - z(6)*z(60);



%===========================================================================
Output = [];

L3_R_N(1,1) = z(66);
L3_R_N(1,2) = z(67);
L3_R_N(1,3) = 0;
L3_R_N(2,1) = z(68);
L3_R_N(2,2) = z(69);
L3_R_N(2,3) = 0;
L3_R_N(3,1) = 0;
L3_R_N(3,2) = 0;
L3_R_N(3,3) = 1;


%===========================================
end    % End of function RzRzRzManip_L3_R_N
%===========================================
