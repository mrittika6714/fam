% MotionGenesis file:  RzRzRzManip.txt
% Copyright (c) 2018 Motion Genesis LLC.  All rights reserved.
% Problem:  s.
%--------------------------------------------------------------------
autozee(off)

%---------------------------------------------------------------
% Rigid Bodies, Frames, Points and Particles
%---------------------------------------------------------------
NewtonianFrame  N                 % Intertial NED Plane.
RigidBody       L1, L2

%---------------------------------------------------------------
% Variables, Constants, and Specifieds
%---------------------------------------------------------------
Variable  q1'', q2''
Variable T1, T2

Constant Len1, Len2, Len3

Point EE(L2)

Constant G

%---------------------------------------------------------------
% Set Mass and Inertia Properties
%---------------------------------------------------------------

L1.SetMass( m1 )
L1.SetInertia( L1Cm,  IL1xx, IL1yy, IL1zz, 0, 0, 0 )

L2.SetMass( m2 )
L2.SetInertia( L2Cm,  IL2xx, IL2yy, IL2zz, 0, 0, 0 )


%--------------------------------------------------------------------
%       Rotational kinematics.
%---------------------------------------------------------------

L1.RotateZ( N, q1 )  
L2.RotateZ( L1, q2 )  


%---------------------------------------------------------------
%   Translational kinematics.
%---------------------------------------------------------------
L1o.Translate ( No, 0>)
L1cm.Translate ( L1o, 0.5*Len1*L1x>)

L2o.Translate ( L1o, Len1*L1x>)
L2cm.Translate ( L2o, 0.5*Len2*L2x>)


EE.Translate(L2o, Len2*L2x>)

EE.getPosition(No)

%---------------------------------------------------------------
% Add relevant forces and aerodynamic damping torque.
%---------------------------------------------------------------

L1.AddTorque(T1 * L1z>)
L2.AddTorque(T2 * L2z>)

Variables Fx, Fy, Fz

FEE_L2> = [ Fx*l2X> + Fy * L2Y>]
EE.AddForce(FEE_L2>)
%--------------------------------------------------------------------
%  MG Road Map Dynamics
%--------------------------------------------------------------------
MGRoadMapEOMs[1] = Dot(L1z>, System(L1,L2).GetDynamics(L1o))  		% 
MGRoadMapEOMs[2] = Dot(L2z>, System(L2).GetDynamics(L2o))  		% 

MGRoadMapSolution = solve(MGRoadMapEOMs =0, q1'', q2'')

%--------------------------------------------------------------------
%	Kane's equations of motion -- [Use System.GetDynamics(Kane,Fx,Fy) for reaction forces].
%--------------------------------------------------------------------
SetGeneralizedSpeed( q1', q2')
KaneEOMs = System.GetDynamicsKane()
KaneSolution  =  solve(KaneEOMs =0,  q1'', q2'')

%--------------------------------------------------------------------
%	Check Exuivalency of Dynamic Solutions
%--------------------------------------------------------------------
issimplifyzero(expand(explicit(MGRoadMapSolution),0:3)- expand(explicit(KaneSolution),0:3) )



%--------------------------------------------------------------------
%  Additional Calculations
%-------------------------------------------------------------------- 

rEE_No_N = Matrix(N,EE.getPosition(No))
vEE_N_N = Matrix(N,EE.getVelocity(N))
vEE_N_L2 = Matrix(L2,EE.getVelocity(N))
rEE_No_L2 = Matrix(L2,EE.getPosition(No))

q = [q1; q2]
qDot = dt(q)
states = [q; qDot]
statesDot = dt(states)
JVel_N = explicit(D(rEE_No_N, getTranspose(q)))  % Jacobian in N frame
JVel_L2 = L2.GetRotationMatrix(N) * JVel_N          % Jacobian in L2 (EE) frame

JVel_N_Test = getCoefficent(vEE_N_N, qDot)       % Should match above calculation
JVel_L2_Test = getCoefficent(vEE_N_L2, qDot)     % Should match above calculation


jointTorques = [ T1; T2]
FEE_Vec = [Fx; Fy; Fz]
TJ1> = Exclude( system.getStatics(L1o), jointTorques)
TJ2> = Exclude( system.getStatics(L2o), jointTorques)

% To isolate actuated DOF we dot in that direction
TJ1 = dot(TJ1>, L1Z>)
TJ2 = dot(TJ2>, L2z>)

JForce_N = getCoefficient([TJ1; TJ2], FEE_Vec )
stop


stop
run RzRzManip





%--------------------------------------------------------------------
%  Output functions for Matlab simulation
%--------------------------------------------------------------------
% %Outputs to integrate
% OutputEncode xB', yB', zB', qBx', qBy', qBz'
% OutputEncode xB'', yB'', zB'', wBx', wBy', wBz' 

% Code Algebraic(KaneSimplifiedEOMs := 0, xB'', yB'', zB'', wBx', wBy', wBz') &
%  hexRotorRates_MGdynamicsSimplified2.m(xB', yB', zB', qBx, qBy, qBz, wBx, wBy, wBz, &
%                   mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, &
%                   xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, &
%                   xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, & 
%                   qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, &
%                   qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, &
%                   qP1z', qP2z', qP3z', qP4z', qP5z', qP6z', &
%                    mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir)    
% 
% clear outputEncode

% Code Algebraic(KaneEOMs := 0, xB'', yB'', zB'', wBx', wBy', wBz') &
%  hexRotorRates_MGdynamics.m(xB', yB', zB', qBx, qBy, qBz, &
%                   wBx, wBy, wBz, bThrust, bDrag, lA, &
%                   mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, &
%                   mProp, IPropxx, IPropyy, IPropzz, &
%                   q1, q2, q3, q4, q1', q2', q3', q4', &
%                   q1'', q2'', q3'', q4'', g)                                      
% 
% clear outputEncode


%--------------------------------------------------------------------
%  Output functions for Visualizations
%--------------------------------------------------------------------
% Note this simplifies equations based off certain assumptions
input   IPxx = 0, IPyy = 0, qP1z'' = 0, qP2z'' = 0, qP3z'' = 0, qP4z'' = 0, qP5z'' = 0, qP6z'' = 0

rP1_No_N = matrix(N,express(P1cm.getPosition(No),N))
rP2_No_N = matrix(N,express(P2cm.getPosition(No),N))
rP3_No_N = matrix(N,express(P3cm.getPosition(No),N))
rP4_No_N = matrix(N,express(P4cm.getPosition(No),N))
rP5_No_N = matrix(N,express(P5cm.getPosition(No),N))
rP6_No_N = matrix(N,express(P6cm.getPosition(No),N))

N_R_P1 = P1.GetRotationMatrix(N)
N_R_P2 = P2.GetRotationMatrix(N)
N_R_P3 = P3.GetRotationMatrix(N)
N_R_P4 = P4.GetRotationMatrix(N)
N_R_P5 = P5.GetRotationMatrix(N)
N_R_P6 = P6.GetRotationMatrix(N)


OutputEncode rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N
OutputEncode N_R_P1, N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6


Code Algebraic() &
 hexRotorRates_MGanimationOutputs.m( xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, &
                                     xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, &
                                     xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, &
                                     qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z)                                      

clear outputEncode



%--------------------------------------------------------------------
%  Extra Ouputs for Optimization
% ToDo Why does B_FTMap depend on qPzs?!?!?!?!?!?!?!
%--------------------------------------------------------------------

 %--------------------------------------------------------------------
%  Extra Ouputs for Analysis and Linearization
%-------------------------------------------------------------------- 


%-------------------------------------------------------
% NT = MUDot+R+G Form
%-------------------------------------------------------
TMat = [ qP1z'; qP2z'; qP3z'; qP4z'; qP5z'; qP6z']
q =[xB; yB; zB; qBx; qBy; qBz]
qDot = dt(q)
qDotTrans = getTranspose(qDot)
qDotDot=dt(qDot)
x = [xB; yB; zB; qBx; qBy; qBz; xB'; yB'; zB'; wBx; wBy; wBz ]
xDot = dt(x)
MqDotDot = includeA(KaneEOMs)
F = [explicit(qDot); explicit(kaneSimplifiedSolution, [x;TMat])]

%N =  getCoefficient(-KaneEOMs, TMat) 
%M = getCoefficent(KaneEOMS, qDotDot)
%R = (KaneEOMs + N*TMat - M*qDotDot)
J = D(F, getTranspose(x))

BMat = D(F, getTranspose(TMat))

stop
OutputEncode F
Code Algebraic() quadRotorRatesF.m(bThrust, bDrag, lA, &
                  mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, &
                  mProp, IPropxx, IPropyy, IPropzz, &
                  qBx, qBy, qBz, wBx, wBy, wBz, q1', q2', q3', q4', &
                  xB', yB', zB') 
clearoutputEncode()

OutputEncode J
Code Algebraic() quadRotorRatesJ.m(bThrust, bDrag, lA, &
                  mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, &
                  mProp, IPropxx, IPropyy, IPropzz, &
                  qBx, qBy, qBz, wBx, wBy, wBz, q1', q2', q3', q4', &
                  xB', yB', zB')  
clearoutputEncode()

OutputEncode BMat
Code Algebraic() quadRotorRatesB.m(bThrust, bDrag, lA, &
                  mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, &
                  mProp, IPropxx, IPropyy, IPropzz, &
                  qBx, qBy, qBz, wBx, wBy, wBz, q1', q2', q3', q4', &
                  xB', yB', zB') 
clearoutputEncode()

%--------------------------------------------------------------------
%   Set Motion Variables 
%	Provide expressions for specified quantities.
%   For example, if t < 0,  TA = cos(t)  else  TA = exp(t)
% 	TA = IsNegative(t)*cos(t) + IsPositiveOr0(t) * exp(t)
%--------------------------------------------------------------------

% For forward dynamics specify forces/torques and solve for rates
% Note the propellor rates do not correspond to a DOF but to force inputs
q1'' =  ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )
q2'' =  ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )
q3'' = ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )
q4'' = ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )


% For inverse dynamics specify rates and solve for forces
% Note System is underactuated so need to look into inverse 
% dynamics for underactuated systems (differential flatness)
%xB'' =  ConvertUnits( 0 m/sec^2, UnitSystem )
%yB'' =  ConvertUnits( 0 m/sec^2, UnitSystem )
%zB'' =  ConvertUnits( 0 m/sec^2, UnitSystem )
%wBx' =  ConvertUnits( 0 deg/sec^2, UnitSystem )
%wBy' =  ConvertUnits( 0 deg/sec^2, UnitSystem )
%wBz' =  ConvertUnits( 0 detg/sec^2, UnitSystem )


% For mixed dynamics specify a mixture of above and solve for
% appropriate forces and rates


%--------------------------------------------------------------------
%       Integration parameters and initial values for variables (e.g., for ODE command).
%--------------------------------------------------------------------
Input  tFinal = 60 sec,  tStep = 0.1 sec,  absError = 1.0E-12,  relError = 1.0E-12

Output t, xB, yB, zB, xb', yB', zB', Xb'', yB'', zB'', qBx, qBy, qBz, wBx, wBy, wBz

% State Initial Conditions
Input   xB = 0 m, yB = 0 m, zB = 0 m, &
        xB' = 0 m, yB' = 0 m, zB' = 0 m, &
        qBx = 0 deg, qBy = 0 deg, qBz = 0 deg, & 
        wBx = 0 deg/s, wBy = 0 deg/s, wBz = 0 deg/s, &
        q1 = 0, q2 = 0, q3 = 0, q4 = 0, &
        q1' = 0, q2' = 0, q3' =0, q4' = 0
% System Paramerter Inputs
Input mB = 0.478 kg, IBxx = 0.0117 kg*m^2, IByy =  0.0117 kg*m^2, IBzz = 0.00234 kg*m^2, &
      IBxy = 0, IByz = 0, IBxz = 0, lA = 0.18 m
Input mProp = 0.001 kg, IPropxx = 0, IPropyy = 0, IPropzz = 0.00005 kg*m^2, & 
      bThrust = 6.11*10^(-8), bDrag = 1.5*10^(-9)


%--------------------------------------------------------------------
%  Solve ODEs and save input/output
%--------------------------------------------------------------------
ODE(KaneEOMs := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesKane.m
ODE(MGRoadMapEOMs := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesMG.m
ODE(ManualEOMs := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesManual.m
ODE(ManualEOMs2 := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesManua2.m




% %--------------------------------------------------------------------
% %  Manual MG Road Map Dynamics
% %--------------------------------------------------------------------
% ManualEOMs[1] = Dot(Nx>, mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
% + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) + mprop*prop4cm.getacceleration(N) - System.GetStatics())  	
% ManualEOMs[2] = Dot(Ny>, mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
% + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) + mprop*prop4cm.getacceleration(N) - System.GetStatics())  	
% ManualEOMs[3] = Dot(Nz>, mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
% + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) + mprop*prop4cm.getacceleration(N) - System.GetStatics())  
% ManualEOMs[4] = Dot(Bx>, dt(System.getangularmomentum(Bo),N) - System.GetStatics(Bo))  
% ManualEOMs[5] = Dot(By>, dt(System.getangularmomentum(Bo),N) - System.GetStatics(Bo))  
% ManualEOMs[6] = Dot(Bz>, dt(System.getangularmomentum(Bo),N) - System.GetStatics(Bo))  
% ManualSolution = solve(ManualEOMs =0, xB'', yB'', zB'', wBx', wBy', wBz')
% 
% 
% 
% 
% %--------------------------------------------------------------------
% %  Manual MG Road Map Dynamics 2 (Fully decomposed expressions)
% %--------------------------------------------------------------------
% manualSolution2Eq1> = mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
%                    + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) &
%                    + mprop*prop4cm.getacceleration(N) - ((F1 + F2 + F3 + F4) * Bz> - g*(mB + 4*mProp)*Nz> )
% 
% BChangeAngMom> = dot(B.getInertiaDyadic(Bo), B.getangularAcceleration(N)) + cross(B.getangularvelocity(N), dot(B.getInertiaDyadic(Bo), B.getangularVelocity(N)))
% P1ChangeAngMom> = dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularAcceleration(N)) &
%                 + cross(prop1.getangularVelocity(N),dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) ) &
%                 + cross(prop1cm.getvelocityrelative(Bo,N), mprop*prop1cm.getVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getacceleration(N) )
% 
% P2ChangeAngMom> =  dot(Prop2.getInertiaDyadic(Prop2cm), Prop2.getangularAcceleration(N)) &
%                 + cross(prop2.getangularVelocity(N),dot(Prop2.getInertiaDyadic(Prop2cm), Prop2.getangularVelocity(N)) ) &
%                 + cross(prop2cm.getvelocityrelative(Bo,N), mprop*prop2cm.getVelocity(N)) + cross(prop2cm.getposition(Bo), mprop*prop2cm.getacceleration(N) )
% P3ChangeAngMom> =  dot(Prop3.getInertiaDyadic(Prop3cm), Prop3.getangularAcceleration(N)) &
%                 + cross(prop3.getangularVelocity(N),dot(Prop3.getInertiaDyadic(Prop3cm), Prop3.getangularVelocity(N)) ) &
%                 + cross(prop3cm.getvelocityrelative(Bo,N), mprop*prop3cm.getVelocity(N)) + cross(prop3cm.getposition(Bo), mprop*prop3cm.getacceleration(N) )
% P4ChangeAngMom> =  dot(Prop4.getInertiaDyadic(Prop4cm), Prop4.getangularAcceleration(N)) &
%                 + cross(prop4.getangularVelocity(N),dot(Prop4.getInertiaDyadic(Prop4cm), Prop4.getangularVelocity(N)) ) &
%                 + cross(prop4cm.getvelocityrelative(Bo,N), mprop*prop4cm.getVelocity(N)) + cross(prop4cm.getposition(Bo), mprop*prop4cm.getacceleration(N) )
% 
% manualSolution2Eq2> = BChangeAngMom> + P1ChangeAngMom> + P2ChangeAngMom> + P3ChangeAngMom>+ P4ChangeAngMom> & 
%                     -( (T1 + T2 + T3 + T4) * Bz>)   
% 
% ManualEOMs2[1] = Dot(Nx>, manualSolution2Eq1>)  	
% ManualEOMs2[2] = Dot(Ny>, manualSolution2Eq1>)  	
% ManualEOMs2[3] = Dot(Nz>, manualSolution2Eq1>)  
% ManualEOMs2[4] = Dot(Bx>, manualSolution2Eq2>)  
% ManualEOMs2[5] = Dot(By>, manualSolution2Eq2>)  
% ManualEOMs2[6] = Dot(Bz>, manualSolution2Eq2>)  
% ManualSolution2 = solve(ManualEOMs2 =0, xB'', yB'', zB'', wBx', wBy', wBz')
% 
% test3> = explicit((prop1.getangularmomentum(Bo)))
% p1angmom> = dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N))
% issimplifyequal(expand(explicit(test3>), 1:3), expand(explicit(p1angmom>),1:3))
% 
% test8> = dt(dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)), N) + dt( cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N)),N)
% test9> = dt( p1angmom>,N)
% test10> = dt(dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N)),N)
% issimplifyequal(expand(explicit(test10>), 1:3), expand(explicit(test9>),1:3))
% issimplifyequal(expand(explicit(dot(test8>,bx>)), 1:3), expand(explicit(dot(test9>,bx>)),1:3))
% issimplifyzero(expand(explicit(dot(test8>,by>)), 1:3) - expand(explicit(dot(test9>,by>)),1:3))
% issimplifyequal(expand(explicit(dot(test8>,bz>)), 1:3), expand(explicit(dot(test9>,bz>)),1:3))
% 
% issimplifyzero(expand(explicit(dot(test8> - test9>,Nx>)),1:3))
% issimplifyzero(expand(explicit(dot(test8> - test9>,Ny>)),1:3))
% issimplifyzero(expand(explicit(dot(test8> - test9>,Nz>)),1:3))
% issimplifyzero(expand(explicit(test8> - test9>),1:3))
% 
% 
% test6> = dt(dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)),N)
% test7> = dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularAcceleration(N)) &
%                 + cross(prop1.getangularVelocity(N),dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) )
% issimplifyequal(expand(explicit(test6>), 1:3), expand(explicit(test7>),1:3))
% 
% test4> = explicit(dt(cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N)),N))
% test5> = explicit(cross(prop1cm.getvelocityrelative(Bo,N), mprop*prop1cm.getVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getacceleration(N) ))
% issimplifyequal(expand(explicit(test4>), 1:3), expand(explicit(test5>),1:3))
% 
% 
% 
% 
% %--------------------------------------------------------------------
% %	Compare solutions for a sanity Check 
% %--------------------------------------------------------------------
% % If Symbolic comparison of solution is not feasible. Use matlab generated
% % output files to numerically compare solutions. 
% %issimplifyzero(expand(explicit(Kanesolution), 1:3), expandexplicit((MGRoadMapSolution),1:3))
% %issimplifyequal(expand(explicit(Kanesolution), 1:3), expandexplicit((ManualSolution),1:3))
% 
% %issimplifyequal(expand(explicit(ManualEOMS), 1:3), expand(explicit(manualEOMS2),1:3))
