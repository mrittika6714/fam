function rEE_No_N  = RzRzRzManip_EEPosition( Len1, Len2, Len3, q1, q2, q3 )
if( nargin ~= 6 ) error( 'RzRzRzManip_EEPosition expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_EEPosition.m created Jun 03 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
rEE_No_N = zeros( 1,3 );
z = zeros( 1, 128 );



%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(3) = cos(q2);
z(4) = sin(q2);
z(5) = cos(q3);
z(6) = sin(q3);
z(122) = z(1)*z(3) - z(2)*z(4);
z(123) = z(1)*z(4) + z(2)*z(3);
z(124) = -z(1)*z(4) - z(2)*z(3);
z(125) = z(5)*z(122) + z(6)*z(124);
z(126) = z(5)*z(123) + z(6)*z(122);



%===========================================================================
Output = [];

rEE_No_N(1) = Len1*z(1) + Len2*z(122) + Len3*z(125);
rEE_No_N(2) = Len1*z(2) + Len2*z(123) + Len3*z(126);
rEE_No_N(3) = 0;


%===============================================
end    % End of function RzRzRzManip_EEPosition
%===============================================
