clc; clear; close all;
all_fig = findall(0, 'type', 'figure');
close(all_fig)
%% Script Settings
DOF = 2;
method = 3; % 1 = Jacobian Transpose, 2 = psueudoInverse, 3 = Damped Least Squares
alpha = 1;
k = 0; % redundancy coefficient
errorTol = 10^(-3);
maxIterations = 100;
qDes = pi/2;
stateDes = [ 0.5; 0.5; qDes]; % xPos; yPus, qZ
q0 = [pi/6; pi/6; pi/6];         % Joint 1; Joint2; Joint3


%% Configure Script
params = RzRzRzManip_params('none');
params.DOF = DOF;
params.qDes = qDes;
params.errorTol = errorTol;
params.alpha = alpha;
params.maxIterations = maxIterations;
Len1 = params.Len1;
Len2 = params.Len2;
Len3 = params.Len3;

if DOF == 2
  JacobianFuncHandle = @(q1, q2, q3) RzRzRzManip_J_2DOF( Len1, Len2, Len3, q1, q2, q3 );
  
elseif DOF == 3
  JacobianFuncHandle = @(q1, q2, q3) RzRzRzManip_J_FullDOF( Len1, Len2, Len3, q1, q2, q3 );
end

if method == 1 % Jacobian Transpose Method
  deltaQFuncHandle = @(input ) jacobianTranspose(input);
  
elseif method == 2 % Pseudo Inverse Method
  deltaQFuncHandle = @(input) pseudoInverse(input);
  
elseif method == 3 %Damped Least Squares Method
  deltaQFuncHandle = @(input) dampedLeastSquares(input);
end
params.JacobianFuncHandle = JacobianFuncHandle;
params.deltaQFuncHandle = deltaQFuncHandle;
q = q0;
state = updateState(Len1, Len2, Len3, q, DOF );
error = state(1:DOF) - stateDes(1:DOF);
iter = 0;


figHandle = uifigure();
figHandle.UserData = struct('SetPointHandle' , [], 'L1Handle', [] ...
    , 'L2Handle', [], 'L3Handle', [], 'EEHandle', []);
axesHandle = uiaxes(figHandle,'Position',[10 10 550 400]);
figHandle.set('WindowButtonDownFcn',{@buttonPressCallBack, axesHandle, params, q});

createPlot(figHandle, axesHandle, params, q, stateDes)



function state = updateState(Len1, Len2, Len3, q, DOF)
  q1 = q(1);
  q2 = q(2);
  q3 = q(3);
  rEE_No_N  = RzRzRzManip_EEPosition( Len1, Len2, Len3, q1, q2, q3 );
  L3_R_N = RzRzRzManip_L3_R_N( q1, q2, q3 );
  [~, ~, qL3z] = dcm2angle(L3_R_N, 'XYZ');
  state = [rEE_No_N(1); rEE_No_N(2); qL3z];
  state = state(1:DOF);
end


function deltaQ = jacobianTranspose(input)
    alpha = input.alpha;
    J = input.J;
    error = input.error;
    DOF = input.DOF;
  deltaQ = alpha * J' * error(1:DOF);
end

function deltaQ = pseudoInverse(input)
    alpha = input.alpha;
    J = input.J;
    error = input.error;
    DOF = input.DOF;
  deltaQ = alpha * pinv(J) * error(1:DOF);
end

function deltaQ = dampedLeastSquares(input)
    lambda = input.alpha;
    Je = input.J; 
    Jc = input.zeta';
    J = [Je; Jc];
    errore = [input.error];
    errorc = 0;
    error = [errore; errorc];
    zeta = input.zeta;
    JStar = (J' * J + lambda^2 * eye(3,3) )\J'; 
    JeStar = (Je' * Je + lambda^2 * eye(3,3))\Je';
    deltaQ = JStar * error; %+ (eye(3,3) - JStar*J) * zeta * 0.01 ;
    %deltaQ = pinv(Je)*errore + (eye(3,3) - pinv(Je)*Je)*zeta*20;
    %deltaQ = JeStar * errore + (eye(3,3) - JeStar*Je) * zeta * 20 ;
  %delatQ = deltaQ + (eye(3,3) - JStar*J) * zeta*k ;
  %deltaQ = J' /(J * J' + lambda^2 * eye(2,2) ) * eye(2,2) * error   ;
end

 function buttonPressCallBack(hObject,~, axesHandle, params, q)
    for i=1:7
        fprintf('\b')
    end 
    fprintf(1,'Running');
    mousePos = axesHandle.CurrentPoint(1,1:2);
    if params.DOF ==3
        mousePos = [mousePos, params.qDes];
    end
    updateSetPoint(hObject, axesHandle, params, q, mousePos)
    updateArm(hObject, axesHandle, params, q, mousePos')
 end
 
 
 
function updateSetPoint(figHandle, guiPlot, params, q, stateDes)
    figHandle.UserData.SetPointHandle.XData = stateDes(1);
    figHandle.UserData.SetPointHandle.YData = stateDes(2);
    drawnow
end

function updateArm(figHandle, axesHandle, params, q, setPoint)
iter = 0;
DOF = params.DOF;
Len1 = params.Len1;
Len2 = params.Len2;
Len3 = params.Len3;
state = updateState(Len1, Len2, Len3, q, DOF );
error = state(1:DOF) - setPoint(1:DOF);
while (1)
    if ((norm(error) >= params.errorTol) && (iter <= params.maxIterations))
      q1 = q(1);
      q2 = q(2);
      q3 = q(3);
      J = params.JacobianFuncHandle(q1, q2, q3);
      zeta = RzRzRzManip_zeta( Len1, Len2, Len3, q1, q2, q3 );
      input.alpha = params.alpha;
      input.J = J;
      input.error = error;
      input.zeta = zeta;
      deltaq = params.deltaQFuncHandle(input);
      %deltaq = params.alpha * J' * error(1:DOF);
      q = q - deltaq;
      state = updateState(Len1, Len2, Len3, q, DOF );
      error = state - setPoint(1:DOF);
      iter = iter + 1;
      [rL1o_No_N, rL2o_No_N, rL3o_No_N, rEE_No_N ] = ...
             RzRzRzManip_MGanimationOutputs(Len1, Len2, Len3, q1, q2, q3 );
      figHandle.UserData.L1Handle.XData = [rL1o_No_N(1); rL2o_No_N(1)];
      figHandle.UserData.L1Handle.YData = [rL1o_No_N(2); rL2o_No_N(2)];
      figHandle.UserData.L2Handle.XData = [rL2o_No_N(1); rL3o_No_N(1)];
      figHandle.UserData.L2Handle.YData = [rL2o_No_N(2); rL3o_No_N(2)];
      figHandle.UserData.L3Handle.XData = [rL3o_No_N(1); rEE_No_N(1)];
      figHandle.UserData.L3Handle.YData = [rL3o_No_N(2); rEE_No_N(2)];
      figHandle.UserData.EEHandle.XData = [rEE_No_N(1)];
      figHandle.UserData.EEHandle.YData = [rEE_No_N(2)];
      drawnow
    else
        for i=1:7
            fprintf('\b')
        end
        fprintf('Waiting')
        waitfor(figHandle);
        break
    end
end
end
function createPlot(figHandle, plotHandle, params, q, stateDes)
Len1 = params.Len1;
Len2 = params.Len2;
Len3 = params.Len3;
q1 = q(1);
q2 = q(2);
q3 = q(3);

rMax = params.Len1 + params.Len2 + params.Len3;
xMin = -1.1 * rMax;
xMax = 1.1 * rMax;
yMin = -1.1 * rMax;
yMax = 1.1 * rMax;

[rL1o_No_N, rL2o_No_N, rL3o_No_N, rEE_No_N ] = ...
          RzRzRzManip_MGanimationOutputs(Len1, Len2, Len3, q1, q2, q3 );
setPoint = plot(plotHandle,stateDes(1),stateDes(2) ,  'r.','markersize',params.markerSize+4, 'linewidth',4);
hold(plotHandle,'on')
L1Plot = plot(plotHandle,[rL1o_No_N(1); rL2o_No_N(1)], [rL1o_No_N(2); rL2o_No_N(2)], ...
    'k.-','markersize',params.markerSize, 'linewidth',4);

L2Plot = plot(plotHandle,[rL2o_No_N(1); rL3o_No_N(1)], [rL2o_No_N(2); rL3o_No_N(2)], ...
    'k.-','markersize',params.markerSize, 'linewidth',4);
L3Plot = plot(plotHandle,[rL3o_No_N(1); rEE_No_N(1)], [rL3o_No_N(2); rEE_No_N(2)], ...
    'k.-','markersize',params.markerSize, 'linewidth',4);
EEPlot = plot(plotHandle,[rEE_No_N(1)], [rEE_No_N(2)], ...
    'b.','markersize',params.markerSize, 'linewidth',4);
axis(plotHandle,[xMin xMax yMin yMax])
xlabel(plotHandle,'x Position [m]');
ylabel(plotHandle, 'y Position [m]');
title(plotHandle, 'RzRzRz Manipulator','fontweight', 'bold')
legend(plotHandle,[EEPlot],'End Effector')
figHandle.UserData.SetPointHandle = setPoint;
figHandle.UserData.L1Handle = L1Plot;
figHandle.UserData.L2Handle = L2Plot;
figHandle.UserData.L3Handle = L3Plot;
figHandle.UserData.EEHandle = EEPlot;
 end