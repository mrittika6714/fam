function [statesDot, addionalOutputs] = RzRzRzManip_dynamics(t, states, params, controllerFunc)
u = controllerFunc(t, states);

% Unpack Inputs
T1 = u(1);
T2 = u(2);
T3 = u(3);

% Unpack params
m1 = params.m1;
m2 = params.m2;
m3 = params.m3;
IL1zz = params.IL1zz;
IL2zz = params.IL2zz;
IL3zz = params.IL3zz;
Len1 = params.Len1;
Len2 = params.Len2;
Len3 = params.Len3;
g = params.g;

%Unpack States
q1 = states(1);
q2 = states(2);
q3 = states(3);
q1Dt = states(4);
q2Dt = states(5);
q3Dt = states(6);


[~, statesDot ] = RzRzRzManip_MGdynamics( q1, q2, q3, q1Dt, q2Dt, ...
  q3Dt, T1, T2, T3, m1, m2, m3, IL1zz, IL2zz, IL3zz, Len1, Len2, Len3, g );

[rL1o_No_N, rL2o_No_N, rL3o_No_N, rEE_No_N ] = ...
          RzRzRzManip_MGanimationOutputs(Len1, Len2, Len3, q1, q2, q3 );
[taskPos,taskVel] = RzRzRzManip_ForwardKinematics( q1, q2, q3, q1Dt, ...
    q2Dt, q3Dt, Len1, Len2, Len3, g );
addionalOutputs = [rL1o_No_N; rL2o_No_N; rL3o_No_N; rEE_No_N; taskPos; taskVel ]';

statesDot = statesDot';
end