function [qDes, flag]= RzRzRzManip_IK(params, input)

%% Unpack Params
taskDOF = params.taskDOF;
errorTol = params.errorTol;
maxIterations = params.maxIterations;
IKmethod = params.IKmethod;

Len1 = params.Len1;
Len2 = params.Len2;
Len3 = params.Len3;
alpha = params.alpha;
k = params.k;

if DOF == 2
  JacobianFuncHandle = @(q1, q2, q3) RzRzRzManip_J_2DOF( Len1, Len2, Len3, q1, q2, q3 );
  
elseif DOF == 3
  JacobianFuncHandle = @(q1, q2, q3) RzRzRzManip_J_FullDOF( Len1, Len2, Len3, q1, q2, q3 );
end

if method == 1 % Jacobian Transpose Method
  deltaQFuncHandle = @(input ) jacobianTranspose(input);
  
elseif method == 2 % Pseudo Inverse Method
  deltaQFuncHandle = @(input) pseudoInverse(input);
  
elseif method == 3 %Damped Least Squares Method
  deltaQFuncHandle = @(input) dampedLeastSquares(input);
  
end

%% Unpack input
xDes = input.xDes;
qCurrent = input.qCurrent;
xCurrent = calcX(Len1, Len2, Len3, qCurrent, DOF );
error = xDes(1:DOF)- xCurrent(1:DOF);
iter = 0;

if ((norm(error) >= params.errorTol) && (iter <= params.maxIterations))
  q1 = qCurrent(1);
  q2 = qCurrent(2);
  q3 = qCurrent(3);
  J = params.JacobianFuncHandle(q1, q2, q3);
  zeta = RzRzRzManip_zeta( Len1, Len2, Len3, q1, q2, q3 );
  input.DOF = DOF;
  input.alpha = params.alpha;
  input.J = J;
  input.error = error;
  input.zeta = zeta;
  deltaq = params.deltaQFuncHandle(input);
  %deltaq = params.alpha * J' * error(1:DOF);
  qUpdate = qCurrent + deltaq;
  state = updateState(Len1, Len2, Len3, qUpdate, DOF );
  error = state - setPoint(1:DOF);
  iter = iter + 1;
  [rL1o_No_N, rL2o_No_N, rL3o_No_N, rEE_No_N ] = ...
         RzRzRzManip_MGanimationOutputs(Len1, Len2, Len3, q1, q2, q3 );
    qCurrent = qUpdate;
end

if norm(error) < errorTol
    flag = 0;
else 
    flag = 1;
end

function x = calcX(Len1, Len2, Len3, q, DOF)
  q1 = q(1);
  q2 = q(2);
  q3 = q(3);
  rEE_No_N  = RzRzRzManip_EEPosition( Len1, Len2, Len3, q1, q2, q3 );
  L3_R_N = RzRzRzManip_L3_R_N( q1, q2, q3 );
  [~, ~, qL3z] = dcm2angle(L3_R_N, 'XYZ');
  x = [rEE_No_N(1); rEE_No_N(2); qL3z];
  x= x(1:DOF);
end


function deltaQ = jacobianTranspose(input)
    alpha = input.alpha;
    J = input.J;
    error = input.error;
    DOF = input.DOF;
  deltaQ = alpha * J' * error(1:DOF);
end

function deltaQ = pseudoInverse(input)
    alpha = input.alpha;
    J = input.J;
    error = input.error;
    DOF = input.DOF;
  deltaQ = alpha * pinv(J) * error(1:DOF);
end

function deltaQ = dampedLeastSquares(input)
    lambda = input.alpha;
    Je = input.J; 
    Jc = input.zeta';
    J = [Je; Jc];
    errore = [input.error];
    errorc = 0;
    error = [errore; errorc];
    zeta = input.zeta;
    JStar = (J' * J + lambda^2 * eye(3,3) )\J'; 
    JeStar = (Je' * Je + lambda^2 * eye(3,3))\Je';
    deltaQ = JStar * error; %+ (eye(3,3) - JStar*J) * zeta * 0.01 ;
    %deltaQ = pinv(Je)*errore + (eye(3,3) - pinv(Je)*Je)*zeta*20;
    %deltaQ = JeStar * errore + (eye(3,3) - JeStar*Je) * zeta * 20 ;
  %delatQ = deltaQ + (eye(3,3) - JStar*J) * zeta*k ;
  %deltaQ = J' /(J * J' + lambda^2 * eye(2,2) ) * eye(2,2) * error   ;
end


 
 


function updateQ(figHandle, axesHandle, params, q, setPoint)
iter = 0;
DOF = params.DOF;
Len1 = params.Len1;
Len2 = params.Len2;
Len3 = params.Len3;
state = updateState(Len1, Len2, Len3, q, DOF );
error = state(1:DOF) - setPoint(1:DOF);



    end
end


end