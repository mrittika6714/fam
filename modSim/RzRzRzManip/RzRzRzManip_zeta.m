function zeta = RzRzRzManip_zeta( Len1, Len2, Len3, q1, q2, q3 )
if( nargin ~= 6 ) error( 'RzRzRzManip_zeta expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: RzRzRzManip_zeta.m created Jul 21 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
zeta = zeros( 3, 1 );
z = zeros( 1, 167 );



%===========================================================================
z(1) = cos(q1);
z(2) = sin(q1);
z(145) = sin(q1+q2+q3);
z(146) = sin(q1+q2);
z(147) = Len3^2;
z(148) = z(147)*z(145)^2 + (Len2*z(146)+Len3*z(145))^2 + (Len1*z(2)+Len2*z(146)+Len3*z(145))^2;
z(149) = cos(q1+q2+q3);
z(150) = cos(q1+q2);
z(151) = -z(147)*z(145)*z(149) - (Len2*z(146)+Len3*z(145))*(Len2*z(150)+Len3*z(149)) - (Len1*z(1)+Len2*z(150)+Len3*z(149))*(Len1*  ...
z(2)+Len2*z(146)+Len3*z(145));
z(152) = z(147)*z(149)^2 + (Len2*z(150)+Len3*z(149))^2 + (Len1*z(1)+Len2*z(150)+Len3*z(149))^2;
z(153) = z(148)*z(152) - z(151)^2;
z(155) = 2*z(147)*z(145)*z(149) + 2*(Len2*z(146)+Len3*z(145))*(Len2*z(150)+Len3*z(149)) + 2*(Len1*z(1)+Len2*z(150)+Len3*z(149))*(  ...
Len1*z(2)+Len2*z(146)+Len3*z(145));
z(156) = -2*z(147)*z(145)*z(149) - 2*(Len2*z(146)+Len3*z(145))*(Len2*z(150)+Len3*z(149)) - 2*(Len1*z(1)+Len2*z(150)+Len3*z(149))*(  ...
Len1*z(2)+Len2*z(146)+Len3*z(145));
z(157) = z(147)*z(145)^2 + (Len2*z(146)+Len3*z(145))^2 + (Len1*z(2)+Len2*z(146)+Len3*z(145))^2 - z(147)*z(149)^2 - (Len2*z(150)+Len3*  ...
z(149))^2 - (Len1*z(1)+Len2*z(150)+Len3*z(149))^2;
z(158) = z(148)*z(156) + z(152)*z(155) - 2*z(151)*z(157);
zeta1 = 0.5*z(158)/sqrt(z(153));
z(159) = 2*z(147)*z(145)*z(149) + 4*Len2*z(146)*(Len2*z(150)+Len3*z(149)) + 2*(Len2*z(150)+Len3*z(149))*(Len1*z(2)+2*Len3*z(145));
z(160) = -2*z(147)*z(145)*z(149) - 4*Len2*z(150)*(Len2*z(146)+Len3*z(145)) - 2*(Len2*z(146)+Len3*z(145))*(Len1*z(1)+2*Len3*  ...
z(149));
z(161) = z(147)*z(145)^2 + 2*Len2*z(146)*(Len2*z(146)+Len3*z(145)) + (Len2*z(146)+Len3*z(145))*(Len1*z(2)+2*Len3*z(145))  ...
- z(147)*z(149)^2 - 2*Len2*z(150)*(Len2*z(150)+Len3*z(149)) - (Len2*z(150)+Len3*z(149))*(Len1*z(1)+2*Len3*z(149));
z(162) = z(148)*z(160) + z(152)*z(159) - 2*z(151)*z(161);
zeta2 = 0.5*z(162)/sqrt(z(153));
z(163) = Len2*Len3;
z(164) = z(149)*(z(147)*z(145)+2*z(163)*z(146)+Len3*(Len1*z(2)+2*Len3*z(145)));
z(165) = z(145)*(z(147)*z(149)+2*z(163)*z(150)+Len3*(Len1*z(1)+2*Len3*z(149)));
z(166) = z(147)*z(145)^2 + 2*z(163)*z(145)*z(146) + Len3*z(145)*(Len1*z(2)+2*Len3*z(145)) - 2*z(163)*z(149)*z(150) - z(147)*z(149)^2  ...
- Len3*z(149)*(Len1*z(1)+2*Len3*z(149));
z(167) = 2*z(152)*z(164) - 2*z(148)*z(165) - 2*z(151)*z(166);
zeta3 = 0.5*z(167)/sqrt(z(153));



%===========================================================================
Output = [];

zeta(1) = zeta1;
zeta(2) = zeta2;
zeta(3) = zeta3;


%=========================================
end    % End of function RzRzRzManip_zeta
%=========================================
