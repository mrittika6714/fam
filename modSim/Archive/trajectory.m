classdef trajectory
   properties
      type = 'test'
      tVec = [0; 10];
      wayPoints = [ 0; 10];
      velocities = [0; 0];
      accelerations = [0; 0];
      func = @obj.test
      tStep = 0.001;
      t= 0;
      w = 1;
      A = 1000;
   end
   methods
      function [r, rDot, rDDot, rDot3, rDot4] = getTrajectory(obj,t)
        if strcmp( obj.type, 'test')
           r = obj.test( t, 100);
           rDot = obj.test( t, 100);
           rDDot = obj.test( t, 100);
           rDot3 = obj.test( t, 100);
           rDot4 = obj.test( t, 100);
        elseif strcmp( obj.type, 'sin')
          [r, rDot, rDDot, rDot3, rDot4] = obj.sinTrajectory(t);
        elseif strcmp( obj.type, 'cos')
          [r, rDot, rDDot, rDot3, rDot4] = obj.cosTrajectory(t);
        elseif strcmp( obj.type, 'minJerk')
          [r, rDot, rDDot, rDot3, rDot4] = obj.minJerkTrajectory(t);
        elseif strcmp( obj.type, 'constantPos')
          [r, rDot, rDDot, rDot3, rDot4] = obj.constantPosTrajectory(t);
        else 
          r = 0;
        end
      end
      
      function setTrajectoryFunction(obj)
        func = obj.sineTrajectory(t);
       
      end
   end
   
  methods (Access = protected)
    function r = test(obj,t, v)
      r = v;
    end
    
    function [r, rDot, rDDot,  rDot3, rDot4] = constantPosTrajectory(obj,t)
      % returns the trajectories for r = A*sin(omega*t)
      r = obj.wayPoints * ones(length(t),1);
      rDot = zeros(length(t),1);
      rDDot = zeros(length(t),1); 
      rDot3 = zeros(length(t),1); 
      rDot4 = zeros(length(t),1); 
    end
    
    function [r, rDot, rDDot, rDot3, rDot4] = sinTrajectory(obj,t)
      % returns the trajectories for r = A*sin(omega*t)
      r = obj.A*sin(obj.w.*t);
      rDot = obj.A*cos(obj.w.*t).*obj.w;
      rDDot = -obj.A*sin(obj.w.*t).*(obj.w).^2;
      rDot3 = -obj.A*cos(obj.w.*t).*(obj.w).^3;
      rDot4 = obj.A*sin(obj.w.*t).*(obj.w).^4;
      r = r';
      rDot = rDot';
      rDDot = rDDot';
      rDot3 = rDot3';
      rDot4 = rDot4';
    end

    function [r, rDot, rDDot, rDot3, rDot4] = cosTrajectory(obj,t)
      % returns the trajectories for r = A*cos(omega*t)
      r = obj.A*cos(obj.w.*t);
      rDot = -obj.A*sin(obj.w.*t).*obj.w;
      rDDot = -obj.A*cos(obj.w.*t).*(obj.w).^2; 
      rDot3 = obj.A*sin(obj.w.*t).*(obj.w).^3;
      rDot4 = obj.A*cos(obj.w.*t).*(obj.w).^4;
      r = r';
      rDot = rDot';
      rDDot = rDDot';
      rDot3 = rDot3';
      rDot4 = rDot4';
    end
    
    function [r, rDot, rDDot, rDot3, rDot4] = minJerkTrajectory(obj,t)
      numPoints = length(t);
      if numPoints == 1  % short circuit expensive "unique" operation below
        idx = discretize(t, obj.tVec);
        if isnan(idx)
          r = nan;%(t < obj.tVec(1))*obj.wayPoints(1,:)' + (t > obj.tVec(end))*obj.wayPoints(end,:)';
          rDot = nan;%obj.velocities(end);
          rDDot = nan;%obj.accelerations(end);
          rDot3 = nan;
          rDot4 = nan;
        else
          D = obj.tVec(idx+1) - obj.tVec(idx);
          T = (t-obj.tVec(idx))/D;
          rPoint = [obj.wayPoints(idx,:); obj.wayPoints(idx+1,:)];
          rDotPoint = [obj.velocities(idx,:); obj.velocities(idx+1,:)];
          rDDotPoint =[obj.accelerations(idx,:); obj.accelerations(idx+1,:)];
          [r, rDot, rDDot, rDot3, rDot4] = obj.min_jerk(rPoint, rDotPoint, rDDotPoint, T, D);
          
      end
  else
      xVec = zeros(size(obj.wayPoints,2), numPoints);
      vVec = zeros(size(obj.wayPoints,2), numPoints);
      aVec = zeros(size(obj.wayPoints,2), numPoints);
      rDot3Vec = zeros(size(obj.wayPoints,2), numPoints);
      rDot4Vec = zeros(size(obj.wayPoints,2), numPoints);
      t = reshape(t,numPoints,1);
      index = discretize(t, obj.tVec); % determine which movement each point belongs to
      uindex = unique(index(~isnan(index))); % detemine number of unique movements
      for ii = 1:length(uindex)  % iterate through each unique movement
          idx = uindex(ii);      % determine which movement we are in
          mask = index == idx;   % create mask for points that correspond to this movment
          time = t(mask);        % get the points in time corresponding to this movement
          D = obj.tVec(idx+1) - obj.tVec(idx); 
          T = (time-obj.tVec(idx))./D;
          xPoint = [obj.wayPoints(idx,:); obj.wayPoints(idx+1,:)];
          vPoint = [obj.velocities(idx,:); obj.velocities(idx+1,:)];
          aPoint =[obj.accelerations(idx,:); obj.accelerations(idx+1,:)];
          [x, v, a, rDot3, rDot4] = obj.min_jerk(xPoint, vPoint, aPoint, T, D);
          xVec(:,mask) = x';
          vVec(:,mask) = v';
          aVec(:,mask) = a';
          rDot3Vec(:,mask) = rDot3';
          rDot4Vec(:,mask) = rDot4';
      end

      if any(t<obj.tVec(1))
          xVec(:,t<obj.tVec(1)) = obj.wayPoints(1,:)';
          vVec(:,t<obj.tVec(1)) = 0;
          aVec(:,t<obj.tVec(1)) = 0;
          rDot3Vec(:,t<obj.tVec(1)) = 0;
          rDot4Vec(:,t<obj.tVec(1)) = 0;
      end
      if any(t>obj.tVec(end))
          xVec(:,t>obj.tVec(end)) = nan; % obj.wayPoints(end,:)';
          vVec(:,t>obj.tVec(end)) = nan;
          aVec(:,t>obj.tVec(end)) = nan;
          rDot3Vec(:,t>obj.tVec(end)) = nan;
          rDot4Vec(:,t>obj.tVec(end)) = nan;
      end
      
      r = xVec';
      rDot = vVec';
      rDDot = aVec';
      rDot3 = rDot3Vec';
      rDot4 = rDot4Vec';
    end
    end
    
    function [x, v, a,rDot3, rDot4] = min_jerk(obj, x, v, a, T, D)
    numSteps  = length(T);
    xi = x(1,:);
    xf = x(2,:);
    vi = v(1,:);
    vf= v(2,:);
    ai = a(1,:);
    af = a(2,:);
    a0 = repmat(xi, numSteps, 1);
    a1 = repmat(D*vi, numSteps, 1);
    a2 = repmat((D^2*ai)/2, numSteps, 1);
    a3 = repmat(10*xf - 10*xi - 4*D*vf - 6*D*vi + (D^2*af)/2 - (3*D^2*ai)/2,  numSteps, 1);
    a4 = repmat(15*xi - 15*xf + 7*D*vf + 8*D*vi - D^2*af + (3*D^2*ai)/2, numSteps, 1);
    a5 = repmat(6*xf - 6*xi - 3*D*vf - 3*D*vi + (D^2*af)/2 - (D^2*ai)/2, numSteps, 1);
    x = (a0 +a1.*T+a2.*T.^2+a3.*T.^3+a4.*T.^4+a5.*T.^5);
    v = ((a1 + 2*a2.*T + 3*a3.*T.^2 + 4*a4.*T.^3 + 5*a5.*T.^4)/D);
    a =((2*a2 + 6*a3.*T + 12*a4.*T.^2 + 20*a5.*T.^3)/D^2);
    rDot3 =((6*a3 + 24*a4.*T + 60*a5.*T.^2)/D^3);
    rDot4 = ((24*a4 + 120*a5.*T)/D^4);
  end
  end
     
end