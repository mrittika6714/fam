%% Analysis of the planarQuadRotor
clc;clear;close all
% Define System

params.IB(1,1) = 0.00025;
params.IB(2,2)= 0.00025;
params.IB(3,3) = 0.00025;
params.IB(1,2) = 0;
params.IB(2,3) = 0;
params.IB(3,1) =0;
params.mB = 0.18;
params.lA = 0.086;  
params.g = 9.8; 
syms y z  qBx mB TBx IBxx yDot zDot qBxDot FBz g real
T = [FBz; TBx];
x = [y;  z;  qBx;  yDot;  zDot;  qBxDot];
xDot= [yDot; zDot; qBxDot;  -FBz*sin(qBx)/mB;  FBz*cos(qBx)/mB - g; TBx/IBxx];
symParams = [ mB; IBxx; g];
subParams = [ params.mB; params.IB(1,1); params.g];

% Perform analysis of linearized system
Df=simplify(jacobian(xDot, x));
EqPoints = solve ( xDot  == 0, [qBx; yDot; zDot; qBxDot; FBz; TBx] );

% note from eqpoints we can linearize about any point

eqPoint = [0, 0, EqPoints.qBx, EqPoints.yDot, EqPoints.yDot, EqPoints.yDot]
uEq = [EqPoints.FBz; EqPoints.TBx];
uEqVal = double(subs(uEq, symParams, subParams));
A =subs(Df, [x; T], [eqPoint';EqPoints.FBz; EqPoints.TBx] );
ASub = double(subs(A, symParams,subParams))

eigenvalues=expand(eig(A))

% note stabiility/instability of eigenvalyes 
eigs = eig(ASub)

 % Get B From Motion Genesis ToDO is get in matlab
Df_B=simplify(jacobian(xDot, T));
B_MG = [0, 0;...  
     0, 0; ...
     0, 0;...  
     -sin(qBx)/mB, 0; ... 
     cos(qBx)/mB, 0;...  
     0, 1/IBxx];
     
BSub = subs(Df_B, x, eqPoint');   
BSub = double(subs(BSub, symParams, subParams));
rankSys = rank(ctrb(ASub,BSub));


if rankSys < length(x)
  warning('System Not Controllable')
else
  fprintf('System is controllable. Rank of controllability matrix is = %i \n', rankSys)
end

% Create LQR Gain Matrix
Q = eye*5
R = eye(2)*0.1;

Klqr = lqr(ASub, BSub, Q, R )
eigs = eig(ASub-BSub*Klqr)
save('Klqr.mat', 'Klqr')

save('uEq.mat', 'uEqVal')
