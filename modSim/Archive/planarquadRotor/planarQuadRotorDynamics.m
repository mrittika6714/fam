function [statesDot, AnimationOutput, inputForces] = planarQuadRotorDynamics(~, states, params, inputFunc)
u = inputFunc(states);
FBz = u(1);
TBx = u(2);
mB = params.mB;
y = states(1);
z = states(2);
qBx = states(3);
yDt = states(4);
zDt = states(5); 
qBxDt = states(6);
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBzx = params.IB(3,1);
lA = params.lA;

[~, Output] = planarQuadRotorMatlab( FBz, TBx, qBx, yDt, zDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx );
AnimationOutput = planarQuadRotorAnimationOutputs( lA, qBx, y, z );
inputForces = [u];
statesDot = Output';
end