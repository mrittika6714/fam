%% Setup workspace and define System Paramaters
clc; clear; close all
animate = 1;
tStep = 0.1;
tFinal = 10;
tSpan = 0:tStep:tFinal;

params.IB(1,1) = 0.00025;
params.IB(2,2)= 0.00025;
params.IB(3,3) = 0.00025;
params.IB(1,2) = 0;
params.IB(2,3) = 0;
params.IB(3,1) =0;
params.mB = 0.18;
params.lA = 0.086;        
u0 = [0; 0; 0; 0; 0; 0];
uDesired = [ 5; 5 ; 0; 0; 0; 0];
numInputs = 2;
%% Uncontrolled Dynamics 
u = @(states) zeros(numInputs,size(uDesired,1))*states;
odeFunc = @(t,states) planarQuadRotorDynamics(t, states, params, u);
[tUncon, xUncon] = ode45(odeFunc, tSpan, u0);

%% Linear Controller (linearized about PI)

load('Klqr.mat')
load('uEq.mat')
u = @(states) -Klqr*(states-uDesired) + uEqVal;
odeFunc = @(t,states) planarQuadRotorDynamics(t, states, params, u);
[tControlled, xControlled] = ode45(odeFunc, tSpan, u0);
% 
numPoints = length(tControlled);
extraOutputs=zeros(numPoints, 4);
inputForces = zeros(numPoints, 2);
for i = 1 :  numPoints
    [~,extraOutputs(i,:),inputForces(i,:) ] = planarQuadRotorDynamics(tControlled(i), xControlled(i,:)', params, u);
end
drawStatesControlled = extraOutputs(:,:);

error = uDesired'.*ones(numPoints,size(uDesired,1)) - xControlled;





%% Plot Responses
figure
hold on 
for i = 1: length(uDesired)
  subplot(length(uDesired),1,i)
  plot(tUncon, xUncon(:,i), 'k', 'linewidth', 2)
  hold on
  plot(tControlled, xControlled(:,i), 'r--', 'linewidth', 2)
legend('Uncontrolled', 'Controlled')
end

figure
hold on
for i = 1:size(inputForces,2)
  plot(tControlled, inputForces(:,i), 'linewidth', 2)
end

figure 
plot(tControlled,error)


%% Animate Responses
if animate ==1
params.uDesired = uDesired;    
planarQuadRotorAnimation(tControlled,drawStatesControlled,params)
end
