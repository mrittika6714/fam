% MotionGenesis file:  quadRotor.txt
% Copyright (c) 2018 Motion Genesis LLC.  All rights reserved.
% Problem:  s.
%--------------------------------------------------------------------
autozee(off)

%---------------------------------------------------------------
% Rigid Bodies, Frames, Points and Particles
%---------------------------------------------------------------
NewtonianFrame  N                 % Intertial NED Plane.
RigidBody       B                 % Quadcoptor Frame

%---------------------------------------------------------------
% Variables, Constants, and Specifieds
%---------------------------------------------------------------
Variable y'', z''          % Locates system center of mass.
Variable qBx''   % Roll angle, Pitch angle, yaw angle.
Variable FBz 
Variable TBx
Constant  g = 9.8 m/s^2              % Earth's gravitational acceleration.
Constant  bThrust                    % Aerodynamic damping on propellors
Constant  bTorque                    % Aerodynamic damping on propellors
Constant mB, mProp, m2, m3, m4
Constant IPropxx, IPropyy, IPropzz
Constant lA                          % arm lengths
Points A1(B), A2(B), A3(B), A4(B)   
rollRates = [qBx']                   % phi

%---------------------------------------------------------------
% Set Mass and Inertia Properties
%---------------------------------------------------------------

B.SetMass( mB )
B.SetInertia( BCm,  IBxx, IByy, IBzz,   IBxy, IByz, IBzx )

%--------------------------------------------------------------------
%       Rotational kinematics.
%---------------------------------------------------------------

B.RotateX( N, qBx  )   % z-Yaw-psi, y-pitch-theta, x-roll-phi

%   Change of Variables to Efficient velocity variables
%BzeroVelocity> =  B.GetAngularVelocity(N) - wBx *Bx> - wBy * By>  - wBz * Bz>
%BchangeAngularVelocityVariableEqns = Matrix(B,express(BzeroVelocity>,B))
%solveDt(BchangeAngularVelocityVariableEqns =0, qBx', qBy', qBz')
%B.setAngularVelocityAcceleration(N, wBx*Bx> + wBy*By> + wBz*Bz>)

%---------------------------------------------------------------
%   Translational kinematics.
%---------------------------------------------------------------
Bo.Translate ( No,  y * Ny> +  z * Nz>)
Bcm.Translate(Bo, 0>)

% Set Arm Reference Frame Positions
A2.Translate(Bo, lA * By>)
A4.Translate(Bo, -lA * By>)

%---------------------------------------------------------------
% Add relevant forces and aerodynamic damping torque.
%---------------------------------------------------------------
System.AddForceGravity(-g * Nz>)

% Add forces from propellors
%F1 = bThrust * w1^2
%F2 = bThrust * w2^2
%F3 = bThrust * w3^2
%F4 = bThrust * w4^2

%A2.AddForce( F2 * Bz> )
%A4.AddForce( F4 * Bz> )
Bcm.AddForce(FBz *Bz>)
B.AddTorque(TBx * Bx>)
Matrix(B,express(B.getDynamics(Bcm),B))
matrix(N,express(B.getDynamics(),N))

%--------------------------------------------------------------------
%	Change of Variables to input Forces/Torques
%--------------------------------------------------------------------
u1 = dot(B.getStatics(),Bz>)


%--------------------------------------------------------------------
%  MG Road Map Dynamics
%--------------------------------------------------------------------

%--------------------------------------------------------------------
%	Kane's equations of motion -- [Use System.GetDynamics(Kane,Fx,Fy) for reaction forces].
%--------------------------------------------------------------------
SetGeneralizedSpeed( y', z', qBx')
KaneEOMs = System.GetDynamicsKane()
KaneSolution  =  solve(KaneEOMs =0, y'', z'', qBx'')

%--------------------------------------------------------------------
%  Output functions for Matlab simulation
%--------------------------------------------------------------------
% Outputs to integrate
OutputEncode y', z', qBx'
OutputEncode y'', z'', qBx'' 


Code Algebraic(KaneEOMs := 0, y'', z'', qBx'') &
 planarQuadRotorMatlab.m(FBz, TBx, qBx, y', z', qBx', &
                  mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx)                                      

clearoutputEncode()


%--------------------------------------------------------------------
%  Output functions for Visualizations
%--------------------------------------------------------------------
A2Position = matrix(N,express(A2.getPosition(No),N))
A4Position = matrix(N,express(A4.getPosition(No),N))
OutputEncode A2Position[2], A2Position[3], A4Position[2], A4Position[3]


Code Algebraic() &
 planarQuadRotorAnimationOutputs.m(lA, qBx, y, z )                                      

clear outputEncode


%--------------------------------------------------------------------
%  Extra Ouputs
%--------------------------------------------------------------------




%-------------------------------------------------------
% NT = MUDot+R+G Form
%-------------------------------------------------------
TMat = [ FBz; TBx]
q =[y; z; qBx]
qDot = dt(q)
qDotTrans = getTranspose(qDot)
qDotDot=dt(qDot)
x = [q; qDot]
xDot = dt(x)
MqDotDot = includeA(KaneEOMs)
function = [qDot; kaneSolution]
N =  getCoefficient(-KaneEOMs, TMat) 
M = getCoefficent(KaneEOMS, qDotDot)
R = (KaneEOMs + N*TMat - M*qDotDot)
J = D(function, getTranspose(x))
BMat = D(function, getTranspose(TMat))


stop
%--------------------------------------------------------------------
% Comparison of Expanded Solutions
%--------------------------------------------------------------------
% Symbolic comparison of solution is not feasible. Use matlab generated
% output files to numerically compare solutions. 
%MGRoadMapSolution = solve([MGRoadMapEOMs], vBx', vBy', vBz', wBx', wBy', wBz' )
%KaneSolution = solve([KaneEOMs], vBx', vBy', vBz', wBx', wBy', wBz' )
%isSimplifyEqual( expand(MGRoadMapSolution, 0:3) , expand(KaneSolution, 0:3),  1.0E-11 )


%--------------------------------------------------------------------
%   Set Motion Variables 
%	Provide expressions for specified quantities.
%   For example, if t < 0,  TA = cos(t)  else  TA = exp(t)
% 	TA = IsNegative(t)*cos(t) + IsPositiveOr0(t) * exp(t)
%--------------------------------------------------------------------

% For forward dynamics specify forces/torques and solve for rates
FBz =  ConvertUnits( 0 N, UnitSystem )
TBx =  ConvertUnits( 0 N, UnitSystem )


%--------------------------------------------------------------------
%  Solution using built in ODE command
%--------------------------------------------------------------------

% Parameters for ODE Command
Input  tFinal = 10 sec,  tStep = 0.1 sec,  absError = 1.0E-8,  relError = 1.0E-8

% Initial States
Input y = 1, z = 1, qBx = 0 deg
Input  y' = 0, z' = 0, qBx' = 0    

%   Initial values for parameters
Input   mB = 118.17 g,  &
        IBxx = 1749.31 g*cm^2, IByy = 3080.28 g*cm^2, IBzz = 1706.3 g*cm^2, &
        IBxy = -24.31 g*cm^2, IByz = -9.95 kg*m^2, IBzx = -7.89 g*cm^2  


%   List output quantities for ODE Command
OutputPlot      t sec, y m, z m
OutputPlot      t sec, qBx deg
OutputPlot      t sec, FBz N 

%   Output quantities for animation with Animake.
%Animate(b )

%  Solve ODEs and save input/output
ODE(KaneEOMs := 0,  y'', z'', qBx'')  planarQuadRotorKane.m

% Save planarQuadRotor.html
 



