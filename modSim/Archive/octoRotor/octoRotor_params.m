function params = octoRotor_params()

params.IB = [0.003, 0, 0; 0, 0.0117, 0; 0, 0, 0.0023];
params.mB = 0.18;
params.g = 9.8;

params.mP = 0.0001;
params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 0.00005];
lArm = 0.186;
params.lA = lArm;
numProps = 8;
propPosAngle = linspace(0,2*pi,numProps+1); % rad
xPropPosVec = lArm * cos(propPosAngle');
yPropPosVec  = lArm * sin(propPosAngle');
zPropPosVec = zeros(numProps,1);
params.rP1_Bcm_B = [xPropPosVec(1), yPropPosVec(1), zPropPosVec(1)];
params.qP1_XYZ = [0; 0; 0];
params.rP2_Bcm_B = [xPropPosVec(2), yPropPosVec(2), zPropPosVec(2)];
params.qP2_XYZ = [0; 0; 0];
params.rP3_Bcm_B = [xPropPosVec(3), yPropPosVec(3), zPropPosVec(3)];
params.qP3_XYZ = [0; 0; 0];
params.rP4_Bcm_B = [xPropPosVec(4), yPropPosVec(4), zPropPosVec(4)];
params.qP4_XYZ = [0; 0; 0];
params.rP5_Bcm_B = [xPropPosVec(5), yPropPosVec(5), zPropPosVec(5)];
params.qP5_XYZ = [0; 0; 0];
params.rP6_Bcm_B = [xPropPosVec(6), yPropPosVec(6), zPropPosVec(6)];
params.qP6_XYZ = [0; 0; 0];
params.rP7_Bcm_B = [xPropPosVec(7), yPropPosVec(7), zPropPosVec(7)];
params.qP7_XYZ = [0; 0; 0];
params.rP8_Bcm_B = [xPropPosVec(8), yPropPosVec(8), zPropPosVec(8)];
params.qP8_XYZ = [0; 0; 0];


params.bThrust1 = 6.1100e-08;
params.bThrust2 = 6.1100e-08;
params.bThrust3 = 6.1100e-08;
params.bThrust4 = 6.1100e-08;
params.bThrust5 = 6.1100e-08;
params.bThrust6 = 6.1100e-08;
params.bThrust7 = 6.1100e-08;
params.bThrust8 = 6.1100e-08;


params.propDirections = [ 1, -1, 1, -1, 1, -1, 1, -1];


params.bDrag = 1.5*10^-9;

forceGravity = params.g*(params.mB + params.mP * numProps);
forceP = forceGravity/numProps;
qProp = sqrt(forceP/params.bThrust1);

params.controller.constantValue = qProp * params.propDirections;  % rad/sec

params.plotting.lP = lArm/5;
params.plotting.wP = params.plotting.lP/5;
params.plotting.hP = params.plotting.lP/5;
params.plotting.lBody = lArm*2.125;
params.plotting.wBody = lArm*2.125;
params.plotting.hBody = params.plotting.lP/5;
end