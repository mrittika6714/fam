function trajectoryParams = octoRotor_trajectoryParams()

tFinal = 30;

xB0 = 0; 
yB0 = 0; 
zB0 = 0; 
qBx0 = 0; 
qBy0 = 0; 
qBz0 = 0; 
xBDt0 = 0; 
yBDt0 = 0; 
zBDt0 = 0; 
wBx0 = 0; 
wBy0 = 0; 
wBz0 = 0; 
qP1z0 = 0; 
qP2z0 = 0; 
qP3z0 = 0; 
qP4z0 = 0; 
qP5z0 = 0; 
qP6z0 = 0;  
qP7z0 = 0; 
qP8z0 = 0; 
bodyStates0 = [xB0; yB0; zB0; qBx0; qBy0; qBz0; xBDt0; yBDt0; zBDt0; wBx0; wBy0; wBz0];
propStates0 = [qP1z0; qP2z0; qP3z0; qP4z0; qP5z0; qP6z0; qP7z0; qP8z0  ];
states0 = [bodyStates0; propStates0]; %load('ICs.mat')

% x trajectory 
xTrajectory = trajectory;
xTrajectory.type = 'minJerk';
xTrajectory.wayPoints = [ states0(1); 0];
xTrajectory.velocities = [states0(7); 0 ];
xTrajectory.tVec = [0; tFinal];
xTrajectory.accelerations = [0; 0];

% y trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'minJerk';
yTrajectory.wayPoints = [ states0(2); 0];
yTrajectory.velocities = [states0(8); 0 ];
yTrajectory.tVec = [0; tFinal];
yTrajectory.accelerations = [0; 0];

% z trajectory 
zTrajectory = trajectory;
zTrajectory.type = 'minJerk';
zTrajectory.wayPoints = [ states0(3); 0];
zTrajectory.velocities = [states0(9); 0 ];
zTrajectory.tVec = [0; tFinal];
zTrajectory.accelerations = [0; 0];

% qBx trajectory 
qBxTrajectory = trajectory;
qBxTrajectory.type = 'minJerk';
qBxTrajectory.wayPoints = [ states0(4); 0];
qBxTrajectory.velocities = [states0(10); 0 ];
qBxTrajectory.tVec = [0; tFinal];
qBxTrajectory.accelerations = [0; 0];

% qBy trajectory 
qByTrajectory = trajectory;
qByTrajectory.type = 'minJerk';
qByTrajectory.wayPoints = [ states0(5); 0];
qByTrajectory.velocities = [states0(11); 0 ];
qByTrajectory.tVec = [0; tFinal];
qByTrajectory.accelerations = [0; 0];

% qBz trajectory 
qBzTrajectory = trajectory;
qBzTrajectory.type = 'minJerk';
qBzTrajectory.wayPoints = [ states0(6); 0];
qBzTrajectory.velocities = [states0(12); 0 ];
qBzTrajectory.tVec = [0; tFinal];
qBzTrajectory.accelerations = [0; 0];

trajectoryParams.numTrajectories = 6;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = xTrajectory;
trajectoryParams.trajectories{2} = yTrajectory;
trajectoryParams.trajectories{3} = zTrajectory;
trajectoryParams.trajectories{4} = qBxTrajectory;
trajectoryParams.trajectories{5} = qByTrajectory;
trajectoryParams.trajectories{6} = qBzTrajectory;

end