function  octoRotor_plot(plotData, params, savePlots)

styleVec ={'k-','r--','y-' };

desiredTrajectory = [plotData.posDes, plotData.vDes];
stateError = plotData.states(:,1:12) - desiredTrajectory;

errorVTime = figure; 
plot(plotData.t, stateError)
title('State Error vs Time')
xlabel('Time (s)');
ylabel('Error');
legend('x', 'y', 'z', 'qBx', 'qBy', 'qBz', ...
       'xDot', 'yDot', 'zDot', 'qBxDot', 'qByDot', 'qBzDot')

figure 
plot(plotData.t, stateError(:,1))

if savePlots
  saveas(trajectoires, fullfile(params.resultsFolder,['errorVTime','.png'] ));
end

end