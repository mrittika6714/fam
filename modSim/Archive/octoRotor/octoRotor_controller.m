function control = octoRotor_controller(t, states, trajectories, trajectoryFunc, params)

% Unpack Params

% Unpack states

controllerType = params.controller.controllerType;

if strcmp(controllerType, 'none')
   control = zeros(8, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue ;
else
  error('Controller Type not handled');
end 

end