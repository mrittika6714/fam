%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function octoRotor_animation(results ,params, saveVideo)

t = results.t;
animationTimeScale = params.animationTimeScale;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;
states = results.states;
armWidth = 5;
armColor = [0.5, 0.5, 0.5];

% Unpack results
xB= states(:,1);
yB= states(:,2);
zB= states(:,3);
qBx= states(:,4);
qBy= states(:,5);
qBz= states(:,6);
setPoint = results.posDes;
lA = params.lA;
lP = params.plotting.lP;
wP = params.plotting.wP;
hP = params.plotting.hP;
vehicleDim = [params.plotting.lBody, params.plotting.wBody, params.plotting.hBody];
numPoints = length(t);
rP1_No_N = results.additionalOutputs(:,1:3);
rP2_No_N = results.additionalOutputs(:,4:6);
rP3_No_N = results.additionalOutputs(:,7:9); 
rP4_No_N = results.additionalOutputs(:,10:12); 
rP5_No_N = results.additionalOutputs(:,13:15); 
rP6_No_N = results.additionalOutputs(:,16:18);
rP7_No_N = results.additionalOutputs(:,19:21);
rP8_No_N = results.additionalOutputs(:,22:24);

qP1_ZYX = results.additionalOutputs(:,25:27);
qP2_ZYX = results.additionalOutputs(:,28:30);
qP3_ZYX = results.additionalOutputs(:,31:33);
qP4_ZYX = results.additionalOutputs(:,34:36);
qP5_ZYX = results.additionalOutputs(:,37:39);
qP6_ZYX = results.additionalOutputs(:,40:42);
qP7_ZYX = results.additionalOutputs(:,43:45);
qP8_ZYX = results.additionalOutputs(:,46:48);

%[bodyVertices, bodyFaces] =  plotCube(vehicleDim,[xB(1), yB(1), zB(1)], [qBz(1), qBy(1), qBx(1)]);

[P1Vertices, P1Faces] =  plotCube([lP, wP, hP],rP1_No_N(1,:), qP1_ZYX(1,:));
[P2Vertices, P2Faces] =  plotCube([lP, wP, hP],rP2_No_N(1,:), qP2_ZYX(1,:));
[P3Vertices, P3Faces] =  plotCube([lP, wP, hP],rP3_No_N(1,:), qP3_ZYX(1,:));
[P4Vertices, P4Faces] =  plotCube([lP, wP, hP],rP4_No_N(1,:), qP4_ZYX(1,:));
[P5Vertices, P5Faces] =  plotCube([lP, wP, hP],rP5_No_N(1,:), qP5_ZYX(1,:));
[P6Vertices, P6Faces] =  plotCube([lP, wP, hP],rP6_No_N(1,:), qP6_ZYX(1,:));
[P7Vertices, P7Faces] =  plotCube([lP, wP, hP],rP7_No_N(1,:), qP7_ZYX(1,:));
[P8Vertices, P8Faces] =  plotCube([lP, wP, hP],rP8_No_N(1,:), qP8_ZYX(1,:));

if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end
animationFigure = figure('units','normalized','outerposition',[0 0 1 1]);
%for i = 1: params.additionalPlots
%   subplot(numRows,numCols,i*2);
%   plot(t, params.additionalData{i});
%   hold on
%   limits = [t(1) t(end) ylim*1.1];
%   axis(limits)
%   timeMark{i} = [ limits(1), limits(3); limits(1), limits(4)] ;
%   timeMarkPlot{i} = plot(timeMark{i}(:,1),timeMark{i}(:,2), '-k', 'linewidth', 2);
%   title(params.additionalTitles{i})
% end
% 
% 
% 
% subplot(numRows,numCols,1:2:numRows*numCols)
setPointPlot = plot3(setPoint(1),setPoint(2),setPoint(3), 'r.','markersize',25);
%view(0,90)
hold on
%bodyPlot = patch('Vertices',bodyVertices,'Faces',bodyFaces, 'FaceColor', [0.5, 0.5, 0.5], 'facealpha', 0.25);
A1Plot = plot3([xB(1); rP1_No_N(1,1) ], [yB(1); rP1_No_N(1,2)], [zB(1); rP1_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A2Plot = plot3([xB(1); rP2_No_N(1,1) ], [yB(1); rP2_No_N(1,2)], [zB(1); rP2_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A3Plot = plot3([xB(1); rP3_No_N(1,1) ], [yB(1); rP3_No_N(1,2)], [zB(1); rP3_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A4Plot = plot3([xB(1); rP4_No_N(1,1) ], [yB(1); rP4_No_N(1,2)], [zB(1); rP4_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A5Plot = plot3([xB(1); rP5_No_N(1,1) ], [yB(1); rP5_No_N(1,2)], [zB(1); rP5_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A6Plot = plot3([xB(1); rP6_No_N(1,1) ], [yB(1); rP6_No_N(1,2)], [zB(1); rP6_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A7Plot = plot3([xB(1); rP7_No_N(1,1) ], [yB(1); rP7_No_N(1,2)], [zB(1); rP7_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A8Plot = plot3([xB(1); rP8_No_N(1,1) ], [yB(1); rP8_No_N(1,2)], [zB(1); rP8_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);


P1Plot = patch('Vertices',P1Vertices,'Faces',P1Faces, 'FaceColor', [0, 0, 0]);
P2Plot = patch('Vertices',P2Vertices,'Faces',P2Faces, 'FaceColor', [0, 0, 0]);
P3Plot = patch('Vertices',P3Vertices,'Faces',P3Faces, 'FaceColor', [0, 0, 0]);
P4Plot = patch('Vertices',P4Vertices,'Faces',P4Faces, 'FaceColor', [0, 0, 0]);
P5Plot = patch('Vertices',P5Vertices,'Faces',P5Faces, 'FaceColor', [0, 0, 0]);
P6Plot = patch('Vertices',P6Vertices,'Faces',P6Faces, 'FaceColor', [0, 0, 0]);
P7Plot = patch('Vertices',P7Vertices,'Faces',P7Faces, 'FaceColor', [0, 0, 0]);
P8Plot = patch('Vertices',P8Vertices,'Faces',P8Faces, 'FaceColor', [0, 0, 0]);

pathPlot = plot3(xB(1), yB(1), zB(1), 'k--', 'linewidth',1);
xMin = min( xB) - lA*2.5;
xMax = max( xB) + lA*2.5;
yMin = min( yB) - lA*2.5;
yMax = max( yB) + lA*2.5;
zMin = min( zB) - lA*2.5;
zMax = max( zB) + lA*2.5;
axis([xMin xMax yMin yMax zMin zMax])
xlabel('x Position [m]');
ylabel('y Position [m]');
zlabel('z Position [m]');
legend('Set Point', 'VehicleBody')
for i =1:numPoints
    setPointPlot.XData = setPoint(1);
    setPointPlot.YData = setPoint(2);
    setPointPlot.ZData = setPoint(3);
    
    %bodyPlot.XData = xB(i);
    %bodyPlot.YData = yB(i);
    %bodyPlot.ZData = zB(i);
    %[ bodyPlot.Vertices, bodyPlot.Faces] =  plotCube(vehicleDim,[xB(i), yB(i), zB(i)], [qBz(i), qBy(i), qBx(i)]);
    
    A1Plot.XData = [xB(i); rP1_No_N(i,1) ];
    A1Plot.YData = [yB(i); rP1_No_N(i,2) ];
    A1Plot.ZData = [zB(i); rP1_No_N(i,3) ];
    A2Plot.XData = [xB(i); rP2_No_N(i,1) ];
    A2Plot.YData = [yB(i); rP2_No_N(i,2) ];
    A2Plot.ZData = [zB(i); rP2_No_N(i,3) ];
    A3Plot.XData = [xB(i); rP3_No_N(i,1) ];
    A3Plot.YData = [yB(i); rP3_No_N(i,2) ];
    A3Plot.ZData = [zB(i); rP3_No_N(i,3) ];
    A4Plot.XData = [xB(i); rP4_No_N(i,1) ];
    A4Plot.YData = [yB(i); rP4_No_N(i,2) ];
    A4Plot.ZData = [zB(i); rP4_No_N(i,3) ];
    A5Plot.XData = [xB(i); rP5_No_N(i,1) ];
    A5Plot.YData = [yB(i); rP5_No_N(i,2) ];
    A5Plot.ZData = [zB(i); rP5_No_N(i,3) ];
    A6Plot.XData = [xB(i); rP6_No_N(i,1) ];
    A6Plot.YData = [yB(i); rP6_No_N(i,2) ];
    A6Plot.ZData = [zB(i); rP6_No_N(i,3) ];
    A7Plot.XData = [xB(i); rP7_No_N(i,1) ];
    A7Plot.YData = [yB(i); rP7_No_N(i,2) ];
    A7Plot.ZData = [zB(i); rP7_No_N(i,3) ];
    A8Plot.XData = [xB(i); rP8_No_N(i,1) ];
    A8Plot.YData = [yB(i); rP8_No_N(i,2) ];
    A8Plot.ZData = [zB(i); rP8_No_N(i,3) ];
    
    [ P1Plot.Vertices, P1Plot.Faces] =  plotCube([lP, wP, hP],rP1_No_N(i,:), qP1_ZYX(i,:));
    [ P2Plot.Vertices, P2Plot.Faces] =  plotCube([lP, wP, hP],rP2_No_N(i,:), qP2_ZYX(i,:));
    [ P3Plot.Vertices, P3Plot.Faces] =  plotCube([lP, wP, hP],rP3_No_N(i,:), qP3_ZYX(i,:));
    [ P4Plot.Vertices, P4Plot.Faces] =  plotCube([lP, wP, hP],rP4_No_N(i,:), qP4_ZYX(i,:));
    [ P5Plot.Vertices, P5Plot.Faces] =  plotCube([lP, wP, hP],rP5_No_N(i,:), qP5_ZYX(i,:));
    [ P6Plot.Vertices, P6Plot.Faces] =  plotCube([lP, wP, hP],rP6_No_N(i,:), qP6_ZYX(i,:));
    [ P7Plot.Vertices, P7Plot.Faces] =  plotCube([lP, wP, hP],rP7_No_N(i,:), qP7_ZYX(i,:));
    [ P8Plot.Vertices, P8Plot.Faces] =  plotCube([lP, wP, hP],rP8_No_N(i,:), qP8_ZYX(i,:));
          
    pathPlot.XData = xB(1:i);
    pathPlot.YData = yB(1:i);
    pathPlot.ZData = zB(1:i);
    
%     for ii = 1:params.additionalPlots
%       timeMarkPlot{ii}.XData = [t(i); t(i)];
%     end
  drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end