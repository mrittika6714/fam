%% Setup workspace and define System Paramaters
close all; clear;  clc; %path(pathdef);
recalculate = true;
model= 'octoRotor';
controllerType = 'FF_PID_Task';
controllerSpace = 'Task';
simDescription = 'Test';
plotResults = true;
savePlots = false;
animate = true;
saveAnimation =  true;
animationTimeScale = 0.5;  % e.g. 2 for 2x playback speed 
saveData = false;
videoName = [model, '_', controllerType, '_', simDescription];
resultsFileName = [model, '_', controllerType, '_', simDescription];
tStep = 0.05;
tFinal = 30;
format long

%% Create function handles and load Files from Model Directory
addpath( model)
addpath('plotFuncs')
trajectoryGenFolder = '\\snl\home\mrittik\Matlab\Trajectory_Generation';
addpath(genpath(trajectoryGenFolder))


% Create function handles
dynamicsFunc = str2func([model '_dynamics']);
controllerFunc = str2func([model '_controller']);
trajectoryFunc = str2func([model '_trajGen']);
trajectoryParamsFunc = str2func([model '_trajectoryParams']);
paramsFunc = str2func([model '_params']);
plotFunc = str2func([model '_plot']);
animateFunc = str2func([model '_animation']);

% Load model params
controlParams.type = controllerType;
controlParams.space = controllerSpace;
params = paramsFunc(controlParams);


%% Append Simulation Configuration to params

%% Clear model functions for persistent variables
clear(func2str(dynamicsFunc))
clear(func2str(controllerFunc))
clear(func2str(trajectoryFunc))
clear(func2str(trajectoryParamsFunc))
clear GenerateTrajectoryPoint

%% Set up Numerical Integration 
tSpan = 0:tStep:tFinal;
opts = odeset('RelTol',1e-8,'AbsTol',1e-8, 'OutputFcn', @displayStatus_dots );

%% Define or Get Trajectory Parameters
trajectories = trajectoryParamsFunc(params.controller.space);
states0 = trajectories.ICs;

%% Run Simulation
if recalculate 
u = @(t, states) controllerFunc(t, states, trajectories, trajectoryFunc, params);
odeFunc = @(t,states) dynamicsFunc(t, states, params, u);
[results.t, results.states] = ode15s(odeFunc, tSpan, states0, opts);

[results.posDes, results.vDes, results.aDes] = trajectoryFunc(trajectories, results.t);

numPoints = length(results.t);
% for i = 1 :  numPoints
%     [~,results.additionalOutputs(i,:)] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
% end

[~, testOutput] = dynamicsFunc(results.t(1), results.states(1,:)', params, u);

if isstruct(testOutput)
outputNames = fieldnames(testOutput);
numOutputs = length(outputNames);
emptyCell = cell(numOutputs,1);
results.additionalOutputs = cell2struct(emptyCell, outputNames, 1);
for i = 1: numPoints
    [~, newOutput] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
    for ii = 1: numOutputs
        results.additionalOutputs.(outputNames{ii}) = [results.additionalOutputs.(outputNames{ii}); newOutput.(outputNames{ii})];
    end
end
else 
    for i = 1 :  numPoints
        [~,results.additionalOutputs(i,:)] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
    end
end


end
%% Create Results Folder
  resultsFolderName = fullfile(model, 'generated');
if (saveData || saveAnimation || savePlots)
  if ~exist(resultsFolderName, 'dir')
    mkdir(resultsFolderName);
  end
end

%% Save Data
if recalculate
if saveData
    save([resultsFolderName,'\',resultsFileName], 'results');
end
else
  load([resultsFolderName,'\',resultsFileName])
end
%% Plot Responses
if plotResults == 1
  params.resultsFolder = resultsFolderName;
  plotFunc(results, params, savePlots);
end

%% Animate Responses
if animate == 1
  videoName = fullfile(resultsFolderName, videoName);
  params.videoName = videoName;
  params.animationTimeScale = animationTimeScale;
  animateFunc(results, params, saveAnimation);
end


