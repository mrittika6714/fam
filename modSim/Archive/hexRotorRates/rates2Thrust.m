function [Output] = rates2Thrust( q1Dt, q2Dt, q3Dt, q4Dt, bThrust )
if( nargin ~= 5 ) error( 'rates2Thrust expects 5 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: rates2Thrust.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================
F1 = bThrust*q1Dt^2;
F2 = bThrust*q2Dt^2;
F3 = bThrust*q3Dt^2;
F4 = bThrust*q4Dt^2;



%===========================================================================
Output = zeros( 1, 4 );

Output(1) = F1;
Output(2) = F2;
Output(3) = F3;
Output(4) = F4;

%=====================================
end    % End of function rates2Thrust
%=====================================
