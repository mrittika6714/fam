function [t,VAR,Output] = quadRotorRatesManual
%===========================================================================
% File: quadRotorRatesManual.m created Jan 22 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
eventDetectedByIntegratorTerminate1OrContinue0 = [];
F1=0; F2=0; F3=0; F4=0; qBxDt=0; qByDt=0; qBzDt=0; wBxDt=0; wByDt=0; wBzDt=0; q1DDt=0; q2DDt=0; q3DDt=0; q4DDt=0; xBDDt=0; yBDDt=0;
zBDDt=0; T1=0; T2=0; T3=0; T4=0;
z = zeros( 1, 547 );

%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
bDrag                           =  1.5E-09;                % UNITS               Constant
bThrust                         =  6.11E-08;               % UNITS               Constant
g                               =  9.8;                    % m/s^2               Constant
IBxx                            =  0.0117;                 % kg*m^2              Constant
IBxy                            =  0;                      % UNITS               Constant
IByy                            =  0.0117;                 % kg*m^2              Constant
IByz                            =  0;                      % UNITS               Constant
IBzx                            =  0;                      % UNITS               Constant
IBzz                            =  0.00234;                % kg*m^2              Constant
IPropxx                         =  0;                      % UNITS               Constant
IPropyy                         =  0;                      % UNITS               Constant
IPropzz                         =  0.00005;                % kg*m^2              Constant
lA                              =  0.18;                   % m                   Constant
mB                              =  0.478;                  % kg                  Constant
mProp                           =  0.001;                  % kg                  Constant

q1                              =  0;                      % UNITS               Initial Value
q2                              =  0;                      % UNITS               Initial Value
q3                              =  0;                      % UNITS               Initial Value
q4                              =  0;                      % UNITS               Initial Value
qBx                             =  0;                      % deg                 Initial Value
qBy                             =  0;                      % deg                 Initial Value
qBz                             =  0;                      % deg                 Initial Value
wBx                             =  0;                      % deg/s               Initial Value
wBy                             =  0;                      % deg/s               Initial Value
wBz                             =  0;                      % deg/s               Initial Value
xB                              =  0;                      % m                   Initial Value
yB                              =  0;                      % m                   Initial Value
zB                              =  0;                      % m                   Initial Value
q1Dt                            =  0;                      % UNITS               Initial Value
q2Dt                            =  0;                      % UNITS               Initial Value
q3Dt                            =  0;                      % UNITS               Initial Value
q4Dt                            =  0;                      % UNITS               Initial Value
xBDt                            =  0;                      % m                   Initial Value
yBDt                            =  0;                      % m                   Initial Value
zBDt                            =  0;                      % m                   Initial Value

tInitial                        =  0.0;                    % second              Initial Time
tFinal                          =  60;                     % sec                 Final Time
tStep                           =  0.1;                    % sec                 Integration Step
printIntScreen                  =  1;                      % 0 or +integer       0 is NO screen output
printIntFile                    =  1;                      % 0 or +integer       0 is NO file   output
absError                        =  1.0E-12;                %                     Absolute Error
relError                        =  1.0E-12;                %                     Relative Error
%-------------------------------+--------------------------+-------------------+-----------------

% Unit conversions
DEGtoRAD = pi / 180.0;
RADtoDEG = 180.0 / pi;
qBx = qBx * DEGtoRAD;
qBy = qBy * DEGtoRAD;
qBz = qBz * DEGtoRAD;
wBx = wBx * DEGtoRAD;
wBy = wBy * DEGtoRAD;
wBz = wBz * DEGtoRAD;

% Evaluate constants
z(101) = g*mB;
z(102) = g*mProp;
z(134) = lA*mProp;
z(247) = IBxx + 2*lA*z(134);
z(252) = IByy + 2*lA*z(134);
z(256) = IBzz + 4*lA*z(134);


VAR = SetMatrixFromNamedQuantities;
[t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile );
OutputToScreenOrFile( [], 0, 0 );   % Close output files.


%===========================================================================
function sys = mdlDerivatives( t, VAR, uSimulink )
%===========================================================================
SetNamedQuantitiesFromMatrix( VAR );
q1DDt = 17.4532925199433*sin(t);
q2DDt = 17.4532925199433*sin(t);
q3DDt = 17.4532925199433*sin(t);
q4DDt = 17.4532925199433*sin(t);

z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(71) = z(1)*z(14) + z(4)*z(9);
z(72) = (wBx*z(14)+wBz*z(9))/z(71);
qBxDt = z(72);
z(3) = sin(qBx);
z(73) = (wBx*z(4)-wBz*z(1))/z(71);
z(74) = -wBy - z(3)*z(73);
qByDt = -z(74);
qBzDt = -z(73);
F1 = bThrust*q1Dt^2;
F2 = bThrust*q2Dt^2;
F3 = bThrust*q3Dt^2;
F4 = bThrust*q4Dt^2;
z(2) = cos(qBz);
z(5) = sin(qBz);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(18) = z(1)*z(8)*qByDt - z(3)*z(4)*qBxDt;
z(20) = z(8)*qBxDt*qBzDt;
z(21) = -z(1)*z(3)*qBxDt - z(4)*z(8)*qByDt;
z(22) = z(1)*qBxDt*qByDt + qBzDt*z(21);
z(23) = cos(q1);
z(24) = sin(q1);
z(25) = z(3)*z(24) - z(9)*z(23);
z(26) = z(1)*z(23);
z(27) = z(3)*z(23) + z(9)*z(24);
z(28) = z(1)*z(24);
z(35) = cos(q2);
z(36) = sin(q2);
z(37) = z(3)*z(36) - z(9)*z(35);
z(38) = z(1)*z(35);
z(39) = z(3)*z(35) + z(9)*z(36);
z(40) = z(1)*z(36);
z(47) = cos(q3);
z(48) = sin(q3);
z(49) = z(3)*z(48) - z(9)*z(47);
z(50) = z(1)*z(47);
z(51) = z(3)*z(47) + z(9)*z(48);
z(52) = z(1)*z(48);
z(59) = cos(q4);
z(60) = sin(q4);
z(61) = z(3)*z(60) - z(9)*z(59);
z(62) = z(1)*z(59);
z(63) = z(3)*z(59) + z(9)*z(60);
z(64) = z(1)*z(60);
z(75) = z(4)*qBxDt*qByDt + qBzDt*z(18);
z(78) = -z(1)*qBxDt*qByDt - qBzDt*z(21);
z(83) = lA*wBy;
z(84) = lA*wBz;
z(87) = -wBy*z(83) - wBz*z(84);
z(90) = lA*wBx;
z(93) = -wBx*z(90) - wBz*z(84);
z(95) = wBy*z(83) + wBz*z(84);
z(99) = wBx*z(90) + wBz*z(84);
T1 = bDrag*F1/bThrust;
T2 = -bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = -bDrag*F4/bThrust;
z(103) = T1 + T2 + T3 + T4;
z(128) = z(24)*qByDt + z(25)*qBzDt + z(26)*qBxDt;
z(129) = z(23)*qByDt + z(27)*qBzDt - z(28)*qBxDt;
z(130) = q1Dt + z(4)*qBxDt + z(14)*qBzDt;
z(131) = IPropxx*z(128);
z(132) = IPropyy*z(129);
z(133) = IPropzz*z(130);
z(143) = z(36)*qByDt + z(37)*qBzDt + z(38)*qBxDt;
z(144) = z(35)*qByDt + z(39)*qBzDt - z(40)*qBxDt;
z(145) = q2Dt + z(4)*qBxDt + z(14)*qBzDt;
z(146) = IPropxx*z(143);
z(147) = IPropyy*z(144);
z(148) = IPropzz*z(145);
z(154) = z(48)*qByDt + z(49)*qBzDt + z(50)*qBxDt;
z(155) = z(47)*qByDt + z(51)*qBzDt - z(52)*qBxDt;
z(156) = q3Dt + z(4)*qBxDt + z(14)*qBzDt;
z(157) = IPropxx*z(154);
z(158) = IPropyy*z(155);
z(159) = IPropzz*z(156);
z(162) = z(60)*qByDt + z(61)*qBzDt + z(62)*qBxDt;
z(163) = z(59)*qByDt + z(63)*qBzDt - z(64)*qBxDt;
z(164) = q4Dt + z(4)*qBxDt + z(14)*qBzDt;
z(165) = IPropxx*z(162);
z(166) = IPropyy*z(163);
z(167) = IPropzz*z(164);
z(182) = IPropxx*z(24);
z(183) = IPropxx*z(25);
z(184) = IPropxx*z(26);
z(186) = IPropyy*z(23);
z(187) = IPropyy*z(27);
z(188) = IPropyy*z(28);
z(190) = IPropzz*z(4);
z(191) = IPropzz*z(14);
z(192) = IPropzz*z(22);
z(196) = IPropxx*z(36);
z(197) = IPropxx*z(37);
z(198) = IPropxx*z(38);
z(200) = IPropyy*z(35);
z(201) = IPropyy*z(39);
z(202) = IPropyy*z(40);
z(207) = IPropxx*z(48);
z(208) = IPropxx*z(49);
z(209) = IPropxx*z(50);
z(211) = IPropyy*z(47);
z(212) = IPropyy*z(51);
z(213) = IPropyy*z(52);
z(218) = IPropxx*z(60);
z(219) = IPropxx*z(61);
z(220) = IPropxx*z(62);
z(222) = IPropyy*z(59);
z(223) = IPropyy*z(63);
z(224) = IPropyy*z(64);
z(229) = lA*F1;
z(230) = lA*F2;
z(231) = lA*F3;
z(232) = lA*F4;
z(248) = z(247) + (z(24)*(z(4)*z(187)+z(14)*z(188)-z(3)*z(4)*z(186))+z(36)*(z(4)*z(201)+z(14)*z(202)-z(3)*z(4)*z(200))+z(48)*(z(4)*  ...
z(212)+z(14)*z(213)-z(3)*z(4)*z(211))+z(60)*(z(4)*z(223)+z(14)*z(224)-z(3)*z(4)*z(222))-z(23)*(z(4)*z(183)-z(14)*z(184)-z(3)*z(4)*  ...
z(182))-z(35)*(z(4)*z(197)-z(14)*z(198)-z(3)*z(4)*z(196))-z(47)*(z(4)*z(208)-z(14)*z(209)-z(3)*z(4)*z(207))-z(59)*(z(4)*z(219)-  ...
z(14)*z(220)-z(3)*z(4)*z(218)))/z(71);
z(249) = IBxy + z(23)*z(182) + z(35)*z(196) + z(47)*z(207) + z(59)*z(218) - z(24)*z(186) - z(36)*z(200) - z(48)*z(211) - z(60)*z(222);
z(250) = IBzx + (z(23)*(z(1)*z(183)+z(9)*z(184)-z(1)*z(3)*z(182))+z(35)*(z(1)*z(197)+z(9)*z(198)-z(1)*z(3)*z(196))+z(47)*(z(1)*  ...
z(208)+z(9)*z(209)-z(1)*z(3)*z(207))+z(59)*(z(1)*z(219)+z(9)*z(220)-z(1)*z(3)*z(218))-z(24)*(z(1)*z(187)-z(9)*z(188)-z(1)*z(3)*  ...
z(186))-z(36)*(z(1)*z(201)-z(9)*z(202)-z(1)*z(3)*z(200))-z(48)*(z(1)*z(212)-z(9)*z(213)-z(1)*z(3)*z(211))-z(60)*(z(1)*z(223)-z(9)*  ...
z(224)-z(1)*z(3)*z(222)))/z(71);
z(251) = IBxy - (z(23)*(z(4)*z(187)+z(14)*z(188)-z(3)*z(4)*z(186))+z(35)*(z(4)*z(201)+z(14)*z(202)-z(3)*z(4)*z(200))+z(47)*(z(4)*  ...
z(212)+z(14)*z(213)-z(3)*z(4)*z(211))+z(59)*(z(4)*z(223)+z(14)*z(224)-z(3)*z(4)*z(222))+z(24)*(z(4)*z(183)-z(14)*z(184)-z(3)*z(4)*  ...
z(182))+z(36)*(z(4)*z(197)-z(14)*z(198)-z(3)*z(4)*z(196))+z(48)*(z(4)*z(208)-z(14)*z(209)-z(3)*z(4)*z(207))+z(60)*(z(4)*z(219)-  ...
z(14)*z(220)-z(3)*z(4)*z(218)))/z(71);
z(253) = z(252) + z(23)*z(186) + z(24)*z(182) + z(35)*z(200) + z(36)*z(196) + z(47)*z(211) + z(48)*z(207) + z(59)*z(222) + z(60)*z(218);
z(254) = IByz + (z(24)*(z(1)*z(183)+z(9)*z(184)-z(1)*z(3)*z(182))+z(36)*(z(1)*z(197)+z(9)*z(198)-z(1)*z(3)*z(196))+z(48)*(z(1)*  ...
z(208)+z(9)*z(209)-z(1)*z(3)*z(207))+z(60)*(z(1)*z(219)+z(9)*z(220)-z(1)*z(3)*z(218))+z(23)*(z(1)*z(187)-z(9)*z(188)-z(1)*z(3)*  ...
z(186))+z(35)*(z(1)*z(201)-z(9)*z(202)-z(1)*z(3)*z(200))+z(47)*(z(1)*z(212)-z(9)*z(213)-z(1)*z(3)*z(211))+z(59)*(z(1)*z(223)-z(9)*  ...
z(224)-z(1)*z(3)*z(222)))/z(71);
z(255) = IBzx - 4*(z(4)*z(191)-z(14)*z(190))/z(71);
z(257) = z(256) + 4*(z(1)*z(191)+z(9)*z(190))/z(71);
z(290) = IBxx*wBx + IBxy*wBy + IBzx*wBz;
z(291) = IBxy*wBx + IByy*wBy + IByz*wBz;
z(292) = IByz*wBy + IBzx*wBx + IBzz*wBz;
z(314) = mProp*(z(10)*z(11)-z(6)*z(7)-z(12)*z(13));
z(315) = mProp*(z(3)*z(10)+z(6)*z(9)-z(12)*z(14));
z(318) = mProp*(z(7)*z(9)-z(3)*z(11)-z(13)*z(14));
z(385) = z(84) + z(3)*zBDt + z(11)*yBDt - z(10)*xBDt;
z(386) = z(12)*xBDt + z(13)*yBDt + z(14)*zBDt - z(83);
z(387) = z(6)*xBDt + z(7)*yBDt - z(84) - z(9)*zBDt;
z(389) = z(90) + z(12)*xBDt + z(13)*yBDt + z(14)*zBDt;
z(390) = z(3)*zBDt + z(11)*yBDt - z(84) - z(10)*xBDt;
z(391) = z(83) + z(12)*xBDt + z(13)*yBDt + z(14)*zBDt;
z(392) = z(84) + z(6)*xBDt + z(7)*yBDt - z(9)*zBDt;
z(393) = z(12)*xBDt + z(13)*yBDt + z(14)*zBDt - z(90);
z(394) = z(290) + z(134)*z(389) - z(134)*z(393);
z(395) = z(291) + z(134)*z(391) - z(134)*z(386);
z(396) = z(292) + z(134)*z(385) + z(134)*z(392) + z(133) + z(148) + z(159) + z(167) - z(134)*z(387) - z(134)*z(390);
z(406) = z(3)*z(23)*q1Dt + z(8)*z(24)*qBxDt + z(9)*z(24)*q1Dt - z(23)*z(18);
z(407) = -z(1)*z(24)*q1Dt - z(4)*z(23)*qByDt;
z(408) = z(23)*q1Dt*qByDt + qBxDt*z(407) + qBzDt*z(406);
z(409) = IPropxx*z(408);
z(410) = z(8)*z(23)*qBxDt + z(9)*z(23)*q1Dt + z(24)*z(18) - z(3)*z(24)*q1Dt;
z(411) = z(1)*z(23)*q1Dt - z(4)*z(24)*qByDt;
z(412) = qBzDt*z(410) - z(24)*q1Dt*qByDt - qBxDt*z(411);
z(413) = IPropyy*z(412);
z(414) = z(3)*z(35)*q2Dt + z(8)*z(36)*qBxDt + z(9)*z(36)*q2Dt - z(35)*z(18);
z(415) = -z(1)*z(36)*q2Dt - z(4)*z(35)*qByDt;
z(416) = z(35)*q2Dt*qByDt + qBxDt*z(415) + qBzDt*z(414);
z(417) = IPropxx*z(416);
z(418) = z(8)*z(35)*qBxDt + z(9)*z(35)*q2Dt + z(36)*z(18) - z(3)*z(36)*q2Dt;
z(419) = z(1)*z(35)*q2Dt - z(4)*z(36)*qByDt;
z(420) = qBzDt*z(418) - z(36)*q2Dt*qByDt - qBxDt*z(419);
z(421) = IPropyy*z(420);
z(422) = z(3)*z(47)*q3Dt + z(8)*z(48)*qBxDt + z(9)*z(48)*q3Dt - z(47)*z(18);
z(423) = -z(1)*z(48)*q3Dt - z(4)*z(47)*qByDt;
z(424) = z(47)*q3Dt*qByDt + qBxDt*z(423) + qBzDt*z(422);
z(425) = IPropxx*z(424);
z(426) = z(8)*z(47)*qBxDt + z(9)*z(47)*q3Dt + z(48)*z(18) - z(3)*z(48)*q3Dt;
z(427) = z(1)*z(47)*q3Dt - z(4)*z(48)*qByDt;
z(428) = qBzDt*z(426) - z(48)*q3Dt*qByDt - qBxDt*z(427);
z(429) = IPropyy*z(428);
z(430) = z(3)*z(59)*q4Dt + z(8)*z(60)*qBxDt + z(9)*z(60)*q4Dt - z(59)*z(18);
z(431) = -z(1)*z(60)*q4Dt - z(4)*z(59)*qByDt;
z(432) = z(59)*q4Dt*qByDt + qBxDt*z(431) + qBzDt*z(430);
z(433) = IPropxx*z(432);
z(434) = z(8)*z(59)*qBxDt + z(9)*z(59)*q4Dt + z(60)*z(18) - z(3)*z(60)*q4Dt;
z(435) = z(1)*z(59)*q4Dt - z(4)*z(60)*qByDt;
z(436) = qBzDt*z(434) - z(60)*q4Dt*qByDt - qBxDt*z(435);
z(437) = IPropyy*z(436);
z(438) = mB + 4*mProp*z(6)^2 + 4*mProp*z(10)^2 + 4*mProp*z(12)^2;
z(439) = mB + 4*mProp*z(7)^2 + 4*mProp*z(11)^2 + 4*mProp*z(13)^2;
z(440) = mB + 4*mProp*z(3)^2 + 4*mProp*z(9)^2 + 4*mProp*z(14)^2;
z(441) = mProp*z(10)*(z(93)+z(99)) + (F1+F2+F3+F4)*z(12) - mProp*z(6)*(z(87)+z(95));
z(442) = (F1+F2+F3+F4)*z(13) - mProp*z(7)*(z(87)+z(95)) - mProp*z(11)*(z(93)+z(99));
z(443) = mProp*z(9)*(z(87)+z(95)) + (F1+F2+F3+F4)*z(14) - 4*z(102) - z(101) - mProp*z(3)*(z(93)+z(99));
z(444) = z(230) + wBz*z(395) + z(23)*(z(130)*z(132)+z(182)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-z(409)-(z(184)*(z(9)*z(78)+  ...
z(14)*z(75))+z(183)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(35)*(z(145)*z(147)+z(196)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-  ...
z(417)-(z(198)*(z(9)*z(78)+z(14)*z(75))+z(197)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(47)*(z(156)*z(158)+z(207)*(z(20)+z(3)*(z(1)*  ...
z(78)-z(4)*z(75))/z(71))-z(425)-(z(209)*(z(9)*z(78)+z(14)*z(75))+z(208)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(59)*(z(164)*z(166)+  ...
z(218)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-z(433)-(z(220)*(z(9)*z(78)+z(14)*z(75))+z(219)*(z(1)*z(78)-z(4)*z(75)))/z(71))  ...
+ z(24)*(z(413)+z(130)*z(131)-z(186)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-(z(188)*(z(9)*z(78)+z(14)*z(75))-z(187)*(z(1)*  ...
z(78)-z(4)*z(75)))/z(71)) + z(36)*(z(421)+z(145)*z(146)-z(200)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-(z(202)*(z(9)*z(78)+  ...
z(14)*z(75))-z(201)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(48)*(z(429)+z(156)*z(157)-z(211)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-(  ...
z(213)*(z(9)*z(78)+z(14)*z(75))-z(212)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(60)*(z(437)+z(164)*z(165)-z(222)*(z(20)+z(3)*(z(1)*  ...
z(78)-z(4)*z(75))/z(71))-(z(224)*(z(9)*z(78)+z(14)*z(75))-z(223)*(z(1)*z(78)-z(4)*z(75)))/z(71)) - z(232) - wBy*z(396);
z(445) = z(231) + wBx*z(396) + z(24)*(z(130)*z(132)+z(182)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-z(409)-(z(184)*(z(9)*z(78)+  ...
z(14)*z(75))+z(183)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(36)*(z(145)*z(147)+z(196)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-  ...
z(417)-(z(198)*(z(9)*z(78)+z(14)*z(75))+z(197)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(48)*(z(156)*z(158)+z(207)*(z(20)+z(3)*(z(1)*  ...
z(78)-z(4)*z(75))/z(71))-z(425)-(z(209)*(z(9)*z(78)+z(14)*z(75))+z(208)*(z(1)*z(78)-z(4)*z(75)))/z(71)) + z(60)*(z(164)*z(166)+  ...
z(218)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-z(433)-(z(220)*(z(9)*z(78)+z(14)*z(75))+z(219)*(z(1)*z(78)-z(4)*z(75)))/z(71))  ...
- z(229) - wBz*z(394) - z(23)*(z(413)+z(130)*z(131)-z(186)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-(z(188)*(z(9)*z(78)+z(14)*  ...
z(75))-z(187)*(z(1)*z(78)-z(4)*z(75)))/z(71)) - z(35)*(z(421)+z(145)*z(146)-z(200)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-(  ...
z(202)*(z(9)*z(78)+z(14)*z(75))-z(201)*(z(1)*z(78)-z(4)*z(75)))/z(71)) - z(47)*(z(429)+z(156)*z(157)-z(211)*(z(20)+z(3)*(z(1)*  ...
z(78)-z(4)*z(75))/z(71))-(z(213)*(z(9)*z(78)+z(14)*z(75))-z(212)*(z(1)*z(78)-z(4)*z(75)))/z(71)) - z(59)*(z(437)+z(164)*z(165)-  ...
z(222)*(z(20)+z(3)*(z(1)*z(78)-z(4)*z(75))/z(71))-(z(224)*(z(9)*z(78)+z(14)*z(75))-z(223)*(z(1)*z(78)-z(4)*z(75)))/z(71));
z(446) = z(103) + wBy*z(394) + z(129)*z(131) + z(144)*z(146) + z(155)*z(157) + z(163)*z(165) - wBx*z(395) - 4*z(192) - z(128)*z(132)  ...
- z(143)*z(147) - z(154)*z(158) - z(162)*z(166) - 4*(z(190)*(z(9)*z(78)+z(14)*z(75))+z(191)*(z(1)*z(78)-z(4)*z(75)))/z(71)  ...
- IPropzz*q1DDt - IPropzz*q2DDt - IPropzz*q3DDt - IPropzz*q4DDt;

COEF = zeros( 6, 6 );
COEF(1,1) = z(438);
COEF(1,2) = -4*z(314);
COEF(1,3) = -4*z(315);
COEF(2,1) = -4*z(314);
COEF(2,2) = z(439);
COEF(2,3) = -4*z(318);
COEF(3,1) = -4*z(315);
COEF(3,2) = -4*z(318);
COEF(3,3) = z(440);
COEF(4,4) = z(248);
COEF(4,5) = z(249);
COEF(4,6) = z(250);
COEF(5,4) = z(251);
COEF(5,5) = z(253);
COEF(5,6) = z(254);
COEF(6,4) = z(255);
COEF(6,5) = IByz;
COEF(6,6) = z(257);
RHS = zeros( 1, 6 );
RHS(1) = z(441);
RHS(2) = z(442);
RHS(3) = z(443);
RHS(4) = z(444);
RHS(5) = z(445);
RHS(6) = z(446);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
zBDDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);

sys = transpose( SetMatrixOfDerivativesPriorToIntegrationStep );
end



%===========================================================================
function VAR = SetMatrixFromNamedQuantities
%===========================================================================
VAR = zeros( 1, 20 );
VAR(1) = q1;
VAR(2) = q2;
VAR(3) = q3;
VAR(4) = q4;
VAR(5) = qBx;
VAR(6) = qBy;
VAR(7) = qBz;
VAR(8) = wBx;
VAR(9) = wBy;
VAR(10) = wBz;
VAR(11) = xB;
VAR(12) = yB;
VAR(13) = zB;
VAR(14) = q1Dt;
VAR(15) = q2Dt;
VAR(16) = q3Dt;
VAR(17) = q4Dt;
VAR(18) = xBDt;
VAR(19) = yBDt;
VAR(20) = zBDt;
end


%===========================================================================
function SetNamedQuantitiesFromMatrix( VAR )
%===========================================================================
q1 = VAR(1);
q2 = VAR(2);
q3 = VAR(3);
q4 = VAR(4);
qBx = VAR(5);
qBy = VAR(6);
qBz = VAR(7);
wBx = VAR(8);
wBy = VAR(9);
wBz = VAR(10);
xB = VAR(11);
yB = VAR(12);
zB = VAR(13);
q1Dt = VAR(14);
q2Dt = VAR(15);
q3Dt = VAR(16);
q4Dt = VAR(17);
xBDt = VAR(18);
yBDt = VAR(19);
zBDt = VAR(20);
end


%===========================================================================
function VARp = SetMatrixOfDerivativesPriorToIntegrationStep
%===========================================================================
VARp = zeros( 1, 20 );
VARp(1) = q1Dt;
VARp(2) = q2Dt;
VARp(3) = q3Dt;
VARp(4) = q4Dt;
VARp(5) = qBxDt;
VARp(6) = qByDt;
VARp(7) = qBzDt;
VARp(8) = wBxDt;
VARp(9) = wByDt;
VARp(10) = wBzDt;
VARp(11) = xBDt;
VARp(12) = yBDt;
VARp(13) = zBDt;
VARp(14) = q1DDt;
VARp(15) = q2DDt;
VARp(16) = q3DDt;
VARp(17) = q4DDt;
VARp(18) = xBDDt;
VARp(19) = yBDDt;
VARp(20) = zBDDt;
end



%===========================================================================
function Output = mdlOutputs( t, VAR, uSimulink )
%===========================================================================
Output = zeros( 1, 16 );
Output(1) = t;
Output(2) = xB;
Output(3) = yB;
Output(4) = zB;
Output(5) = xBDt;
Output(6) = yBDt;
Output(7) = zBDt;
Output(8) = xBDDt;
Output(9) = yBDDt;
Output(10) = zBDDt;
Output(11) = qBx*RADtoDEG;
Output(12) = qBy*RADtoDEG;
Output(13) = qBz*RADtoDEG;
Output(14) = wBx*RADtoDEG;
Output(15) = wBy*RADtoDEG;
Output(16) = wBz*RADtoDEG;
end


%===========================================================================
function OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile )
%===========================================================================
persistent FileIdentifier hasHeaderInformationBeenWritten;

if( isempty(Output) ),
   if( ~isempty(FileIdentifier) ),
      fclose( FileIdentifier(1) );
      clear FileIdentifier;
      fprintf( 1, '\n Output is in the file quadRotorRatesManual.1\n' );
      fprintf( 1, '\n Note: To automate plotting, issue the command OutputPlot in MotionGenesis.\n' );
      fprintf( 1, '\n To load and plot columns 1 and 2 with a solid line and columns 1 and 3 with a dashed line, enter:\n' );
      fprintf( 1, '    someName = load( ''quadRotorRatesManual.1'' );\n' );
      fprintf( 1, '    plot( someName(:,1), someName(:,2), ''-'', someName(:,1), someName(:,3), ''--'' )\n\n' );
   end
   clear hasHeaderInformationBeenWritten;
   return;
end

if( isempty(hasHeaderInformationBeenWritten) ),
   if( shouldPrintToScreen ),
      fprintf( 1,                '%%       t             xB             yB             zB             xb''            yB''            zB''           Xb''''           yB''''           zB''''            qBx            qBy            qBz            wBx            wBy            wBz\n' );
      fprintf( 1,                '%%   (second)          (m)            (m)            (m)            (m)            (m)            (m)          (UNITS)        (UNITS)        (UNITS)         (deg)          (deg)          (deg)         (deg/s)        (deg/s)        (deg/s)\n\n' );
   end
   if( shouldPrintToFile && isempty(FileIdentifier) ),
      FileIdentifier(1) = fopen('quadRotorRatesManual.1', 'wt');   if( FileIdentifier(1) == -1 ), error('Error: unable to open file quadRotorRatesManual.1'); end
      fprintf(FileIdentifier(1), '%% FILE: quadRotorRatesManual.1\n%%\n' );
      fprintf(FileIdentifier(1), '%%       t             xB             yB             zB             xb''            yB''            zB''           Xb''''           yB''''           zB''''            qBx            qBy            qBz            wBx            wBy            wBz\n' );
      fprintf(FileIdentifier(1), '%%   (second)          (m)            (m)            (m)            (m)            (m)            (m)          (UNITS)        (UNITS)        (UNITS)         (deg)          (deg)          (deg)         (deg/s)        (deg/s)        (deg/s)\n\n' );
   end
   hasHeaderInformationBeenWritten = 1;
end

if( shouldPrintToScreen ), WriteNumericalData( 1,                 Output(1:16) );  end
if( shouldPrintToFile ),   WriteNumericalData( FileIdentifier(1), Output(1:16) );  end
end


%===========================================================================
function WriteNumericalData( fileIdentifier, Output )
%===========================================================================
numberOfOutputQuantities = length( Output );
if( numberOfOutputQuantities > 0 ),
   for( i = 1 : numberOfOutputQuantities ),
      fprintf( fileIdentifier, ' %- 14.6E', Output(i) );
   end
   fprintf( fileIdentifier, '\n' );
end
end



%===========================================================================
function [functionsToEvaluateForEvent, eventTerminatesIntegration1Otherwise0ToContinue, eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1] = EventDetection( t, VAR, uSimulink )
%===========================================================================
% Detects when designated functions are zero or cross zero with positive or negative slope.
% Step 1: Uncomment call to mdlDerivatives and mdlOutputs.
% Step 2: Change functionsToEvaluateForEvent,                      e.g., change  []  to  [t - 5.67]  to stop at t = 5.67.
% Step 3: Change eventTerminatesIntegration1Otherwise0ToContinue,  e.g., change  []  to  [1]  to stop integrating.
% Step 4: Change eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1,  e.g., change  []  to  [1].
% Step 5: Possibly modify function EventDetectedByIntegrator (if eventTerminatesIntegration1Otherwise0ToContinue is 0).
%---------------------------------------------------------------------------
% mdlDerivatives( t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
% mdlOutputs(     t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
functionsToEvaluateForEvent = [];
eventTerminatesIntegration1Otherwise0ToContinue = [];
eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1 = [];
eventDetectedByIntegratorTerminate1OrContinue0 = eventTerminatesIntegration1Otherwise0ToContinue;
end


%===========================================================================
function [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents )
%===========================================================================
isIntegrationFinished = eventDetectedByIntegratorTerminate1OrContinue0( nIndexOfEvents );
if( ~isIntegrationFinished ),
   SetNamedQuantitiesFromMatrix( VAR );
%  Put code here to modify how integration continues.
   VAR = SetMatrixFromNamedQuantities;
end
end



%===========================================================================
function [t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile )
%===========================================================================
OdeMatlabOptions = odeset( 'RelTol',relError, 'AbsTol',absError, 'MaxStep',tStep, 'Events',@EventDetection );
t = tInitial;                 epsilonT = 0.001*tStep;                   tFinalMinusEpsilonT = tFinal - epsilonT;
printCounterScreen = 0;       integrateForward = tFinal >= tInitial;    tAtEndOfIntegrationStep = t + tStep;
printCounterFile   = 0;       isIntegrationFinished = 0;
mdlDerivatives( t, VAR, 0 );
while 1,
   if( (integrateForward && t >= tFinalMinusEpsilonT) || (~integrateForward && t <= tFinalMinusEpsilonT) ), isIntegrationFinished = 1;  end
   shouldPrintToScreen = printIntScreen && ( isIntegrationFinished || printCounterScreen <= 0.01 );
   shouldPrintToFile   = printIntFile   && ( isIntegrationFinished || printCounterFile   <= 0.01 );
   if( isIntegrationFinished || shouldPrintToScreen || shouldPrintToFile ),
      Output = mdlOutputs( t, VAR, 0 );
      OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile );
      if( isIntegrationFinished ), break;  end
      if( shouldPrintToScreen ), printCounterScreen = printIntScreen;  end
      if( shouldPrintToFile ),   printCounterFile   = printIntFile;    end
   end
   [TimeOdeArray, VarOdeArray, timeEventOccurredInIntegrationStep, nStatesArraysAtEvent, nIndexOfEvents] = ode45( @mdlDerivatives, [t tAtEndOfIntegrationStep], VAR, OdeMatlabOptions, 0 );
   if( isempty(timeEventOccurredInIntegrationStep) ),
      lastIndex = length( TimeOdeArray );
      t = TimeOdeArray( lastIndex );
      VAR = VarOdeArray( lastIndex, : );
      printCounterScreen = printCounterScreen - 1;
      printCounterFile   = printCounterFile   - 1;
      if( abs(tAtEndOfIntegrationStep - t) >= abs(epsilonT) ), warning('numerical integration failed'); break;  end
      tAtEndOfIntegrationStep = t + tStep;
      if( (integrateForward && tAtEndOfIntegrationStep > tFinal) || (~integrateForward && tAtEndOfIntegrationStep < tFinal) ) tAtEndOfIntegrationStep = tFinal;  end
   else
      t = timeEventOccurredInIntegrationStep( 1 );    % time  at firstEvent = 1 during this integration step.
      VAR = nStatesArraysAtEvent( 1, : );             % state at firstEvent = 1 during this integration step.
      printCounterScreen = 0;
      printCounterFile   = 0;
      [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents(1) );
   end
end
end


%=============================================
end    % End of function quadRotorRatesManual
%=============================================
