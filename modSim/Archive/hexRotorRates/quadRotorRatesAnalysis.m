%% Analysis of the planarQuadRotor
clc;clear;close all
% Define System
gcmsq2gmsq = 10;
g2kg = 0.001;

params.IB(1,1) = 0.0117; %kg*m^2% ;
params.IB(2,2)= 0.0117; %kg*m^2;
params.IB(3,3) = 0.00234; %kg*m^2
params.IB(1,2) = 0;%
params.IB(2,3) = 0;%
params.IB(3,1) = 0;%
params.mB = 0.478;
params.g = 9.8; 
params.lA = 0.18;
params.bThrust = 6.11*10^-8;
params.bDrag =  1.5*10^-9;
params.mProp = 0.0001;
params.IProp = [0, 0, 0; 0, 0, 0; 0, 0, 0.00005];

save('params.mat', 'params')

syms xB yB zB qBx qBy qBz xBDt yBDt zBDt qBxDt qByDt qBZDt mB F1 ...
    q1Dt q2Dt q3Dt q4Dt F2 F3 F4 IBxx IByy IBzz IBxy IByz IBzx g wBx ...
    wBy wBz lA bDrag bThrust mProp IPropzz real
T = [q1Dt; q2Dt; q3Dt; q4Dt];
x = [xB; yB;  zB;  qBx; qBy; qBz; xBDt; yBDt; zBDt;  wBx; wBy; wBz;];

% Get from Motion Genesis F File 
F = zeros(12,1);
F = sym(F);
F(1) = xBDt;
F(2) = yBDt;
F(3) = zBDt;
F(4) = sin(qBy)*wBz + cos(qBy)*wBx;
F(5) = wBy + tan(qBx)*(sin(qBy)*wBx-cos(qBy)*wBz);
F(6) = -(sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx);
F(7) = bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
F(8) = bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
F(9) = -(9.8*mB+39.2*mProp-bThrust*cos(qBx)*cos(qBy)*q1Dt^2-bThrust*cos(qBx)*cos(qBy)*q2Dt^2-bThrust*cos(qBx)*cos(qBy)*q3Dt^2-  ...
bThrust*cos(qBx)*cos(qBy)*q4Dt^2)/(mB+4*mProp);
F(10) = ((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)+bDrag*q1Dt^2+bDrag*q3Dt^2-wBx*(IBxy*wBx+IByy*wBy+  ...
IByz*wBz)-bDrag*q2Dt^2-bDrag*q4Dt^2)+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBx*wBz+wBx*(IByz*wBy+IBzx*wBx+  ...
IBzz*wBz)+IPropzz*wBx*q1Dt+IPropzz*wBx*q2Dt+IPropzz*wBx*q4Dt+IPropzz*wBx*(4*wBz+q3Dt)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-bThrust*lA*(q1Dt^2-q3Dt^2))+(  ...
IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBy*wBz+wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)+IPropzz*wBy*q1Dt+  ...
IPropzz*wBy*q2Dt+IPropzz*wBy*q4Dt+IPropzz*wBy*(4*wBz+q3Dt)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-bThrust*lA*(q2Dt^2-q4Dt^2)))/(IByz*(2*  ...
IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*  ...
mProp*lA^2)));
F(11) = ((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)+bDrag*q1Dt^2+bDrag*q3Dt^2-wBx*(IBxy*wBx+IByy*wBy+  ...
IByz*wBz)-bDrag*q2Dt^2-bDrag*q4Dt^2)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBy*wBz+wBy*(IByz*wBy+IBzx*wBx+  ...
IBzz*wBz)+IPropzz*wBy*q1Dt+IPropzz*wBy*q2Dt+IPropzz*wBy*q4Dt+IPropzz*wBy*(4*wBz+q3Dt)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-bThrust*lA*(q2Dt^2-q4Dt^2))-(  ...
IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBx*wBz+wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)+IPropzz*wBx*q1Dt+  ...
IPropzz*wBx*q2Dt+IPropzz*wBx*q4Dt+IPropzz*wBx*(4*wBz+q3Dt)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-bThrust*lA*(q1Dt^2-q3Dt^2)))/(IByz*(2*  ...
IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*  ...
mProp*lA^2)));
F(12) = -((IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)+bDrag*q1Dt^2+bDrag*q3Dt^2-wBx*(IBxy*  ...
wBx+IByy*wBy+IByz*wBz)-bDrag*q2Dt^2-bDrag*q4Dt^2)+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(2*mProp*lA^2*wBy*wBz+wBy*(IByz*wBy+IBzx*wBx+  ...
IBzz*wBz)+IPropzz*wBy*q1Dt+IPropzz*wBy*q2Dt+IPropzz*wBy*q4Dt+IPropzz*wBy*(4*wBz+q3Dt)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-bThrust*lA*(q2Dt^2-q4Dt^2))-(  ...
IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(2*mProp*lA^2*wBx*wBz+wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)+IPropzz*wBx*q1Dt+IPropzz*wBx*q2Dt+  ...
IPropzz*wBx*q4Dt+IPropzz*wBx*(4*wBz+q3Dt)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-bThrust*lA*(q1Dt^2-q3Dt^2)))/(IByz*(2*IBxy*IBzx-IByz*(  ...
IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));




symParams = [ mB; IBxx; IByy; IBzz; IBxy; IByz; IBzx; g; lA; ...
bThrust; bDrag; mProp; IPropzz];
subParams = [ params.mB; params.IB(1,1); params.IB(2,2); params.IB(3,3);...
params.IB(1,2); params.IB(2,3); params.IB(3,1); params.g; params.lA; ...
params.bThrust; params.bDrag; params.mProp; params.IProp(3,3)];

FTest = subs(F,  [IBxy, IByz, IBzx, wBz, IByy, IBzz], [0,0,0,0,0,0 ])


% Perform analysis of linearized system
Df=simplify(jacobian(F, x')); 


J = zeros(12,12);
J = sym(J);
J(1,1) = 0;
J(1,2) = 0;
J(1,3) = 0;
J(1,4) = 0;
J(1,5) = 0;
J(1,6) = 0;
J(1,7) = 1;
J(1,8) = 0;
J(1,9) = 0;
J(1,10) = 0;
J(1,11) = 0;
J(1,12) = 0;
J(2,1) = 0;
J(2,2) = 0;
J(2,3) = 0;
J(2,4) = 0;
J(2,5) = 0;
J(2,6) = 0;
J(2,7) = 0;
J(2,8) = 1;
J(2,9) = 0;
J(2,10) = 0;
J(2,11) = 0;
J(2,12) = 0;
J(3,1) = 0;
J(3,2) = 0;
J(3,3) = 0;
J(3,4) = 0;
J(3,5) = 0;
J(3,6) = 0;
J(3,7) = 0;
J(3,8) = 0;
J(3,9) = 1;
J(3,10) = 0;
J(3,11) = 0;
J(3,12) = 0;
J(4,1) = 0;
J(4,2) = 0;
J(4,3) = 0;
J(4,4) = 0;
J(4,5) = cos(qBy)*wBz - sin(qBy)*wBx;
J(4,6) = 0;
J(4,7) = 0;
J(4,8) = 0;
J(4,9) = 0;
J(4,10) = cos(qBy);
J(4,11) = 0;
J(4,12) = sin(qBy);
J(5,1) = 0;
J(5,2) = 0;
J(5,3) = 0;
J(5,4) = (sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx)^2;
J(5,5) = tan(qBx)*(sin(qBy)*wBz+cos(qBy)*wBx);
J(5,6) = 0;
J(5,7) = 0;
J(5,8) = 0;
J(5,9) = 0;
J(5,10) = sin(qBy)*tan(qBx);
J(5,11) = 1;
J(5,12) = -cos(qBy)*tan(qBx);
J(6,1) = 0;
J(6,2) = 0;
J(6,3) = 0;
J(6,4) = -sin(qBx)*(sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx)^2;
J(6,5) = -(sin(qBy)*wBz+cos(qBy)*wBx)/cos(qBx);
J(6,6) = 0;
J(6,7) = 0;
J(6,8) = 0;
J(6,9) = 0;
J(6,10) = -sin(qBy)/cos(qBx);
J(6,11) = 0;
J(6,12) = cos(qBy)/cos(qBx);
J(7,1) = 0;
J(7,2) = 0;
J(7,3) = 0;
J(7,4) = bThrust*sin(qBz)*cos(qBx)*cos(qBy)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(7,5) = bThrust*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(7,6) = -bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(7,7) = 0;
J(7,8) = 0;
J(7,9) = 0;
J(7,10) = 0;
J(7,11) = 0;
J(7,12) = 0;
J(8,1) = 0;
J(8,2) = 0;
J(8,3) = 0;
J(8,4) = -bThrust*cos(qBx)*cos(qBy)*cos(qBz)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(8,5) = bThrust*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(8,6) = bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(8,7) = 0;
J(8,8) = 0;
J(8,9) = 0;
J(8,10) = 0;
J(8,11) = 0;
J(8,12) = 0;
J(9,1) = 0;
J(9,2) = 0;
J(9,3) = 0;
J(9,4) = -bThrust*sin(qBx)*cos(qBy)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(9,5) = -bThrust*sin(qBy)*cos(qBx)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(9,6) = 0;
J(9,7) = 0;
J(9,8) = 0;
J(9,9) = 0;
J(9,10) = 0;
J(9,11) = 0;
J(9,12) = 0;
J(10,1) = 0;
J(10,2) = 0;
J(10,3) = 0;
J(10,4) = 0;
J(10,5) = 0;
J(10,6) = 0;
J(10,7) = 0;
J(10,8) = 0;
J(10,9) = 0;
J(10,10) = ((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IBxx*wBy-2*IBxy*wBx-IByy*wBy-IByz*wBz)-(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*  ...
IPropzz+4*mProp*lA^2))*(IBxy*wBz-IBzx*wBy)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz-  ...
2*mProp*lA^2*wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(10,11) = -((IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBz-IByz*wBx)+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IByy*wBx-2*  ...
IBxy*wBy-IBxx*wBx-IBzx*wBz)+(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz-2*  ...
mProp*lA^2*wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(10,12) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IByz*wBx-IBzx*wBy)+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBx+IBxy*  ...
wBy+2*IBzx*wBz-4*IPropzz*wBx-IBzz*wBx-2*mProp*lA^2*wBx)+(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBx+IByy*  ...
wBy+2*IByz*wBz-4*IPropzz*wBy-IBzz*wBy-2*mProp*lA^2*wBy))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(11,1) = 0;
J(11,2) = 0;
J(11,3) = 0;
J(11,4) = 0;
J(11,5) = 0;
J(11,6) = 0;
J(11,7) = 0;
J(11,8) = 0;
J(11,9) = 0;
J(11,10) = ((IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBz-IBzx*wBy)+(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxx*wBy-2*IBxy*  ...
wBx-IByy*wBy-IByz*wBz)+(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz-2*mProp*lA^2*  ...
wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*  ...
mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(11,11) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IByy*wBx-2*IBxy*wBy-IBxx*wBx-IBzx*wBz)-(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*  ...
IPropzz+4*mProp*lA^2))*(IBxy*wBz-IByz*wBx)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz-  ...
2*mProp*lA^2*wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(11,12) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IByz*wBx-IBzx*wBy)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBx+IByy*  ...
wBy+2*IByz*wBz-4*IPropzz*wBy-IBzz*wBy-2*mProp*lA^2*wBy)-(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBx+IBxy*  ...
wBy+2*IBzx*wBz-4*IPropzz*wBx-IBzz*wBx-2*mProp*lA^2*wBx))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(12,1) = 0;
J(12,2) = 0;
J(12,3) = 0;
J(12,4) = 0;
J(12,5) = 0;
J(12,6) = 0;
J(12,7) = 0;
J(12,8) = 0;
J(12,9) = 0;
J(12,10) = ((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IBxy*wBz-IBzx*wBy)-(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(IBxx*wBy-2*  ...
IBxy*wBx-IByy*wBy-IByz*wBz)-(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz-2*mProp*lA^2*wBz-IPropzz*  ...
q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+  ...
4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(12,11) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxy*wBz-IByz*wBx)-(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(IByy*wBx-  ...
2*IBxy*wBy-IBxx*wBx-IBzx*wBz)-(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz-2*mProp*lA^2*wBz-  ...
IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(12,12) = ((IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(IByz*wBx-IBzx*wBy)+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IBxy*wBx+IByy*  ...
wBy+2*IByz*wBz-4*IPropzz*wBy-IBzz*wBy-2*mProp*lA^2*wBy)-(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxx*wBx+IBxy*wBy+2*IBzx*wBz-4*  ...
IPropzz*wBx-IBzz*wBx-2*mProp*lA^2*wBx))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*  ...
mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));



checkDf = simplify(Df-J);
numEqualElements = 0;
try
numEqualElements = sum(double(checkDf)== 0, 'all');

catch E
  warning('Matlab Calculated Jacobian doesnt match MG calculated Jacobian')
end

numElements = numel(checkDf);

if numEqualElements ~= numElements 
  warning('Matlab Calculated Jacobian doesnt match MG calculated Jacobian')
else
  fprintf('Jacboian calculations match \n')
end

DfSubs = subs(Df, symParams, subParams );

FSubs= subs(F, [symParams; ], [subParams; ]);
variables  =[xBDt; yBDt; zBDt; wBx; wBy; wBz; qBx; qBy; q1Dt; q2Dt; q3Dt; q4Dt];


% EqPoints2 = solve ( [FSubs(1:6)] == 0, variables(1:6) );
% FSubs2 = subs(F, variables(1:6), [EqPoints2.xBDt(1);EqPoints2.yBDt(1); ...
%   EqPoints2.zBDt(1); EqPoints2.wBx(1);EqPoints2.wBy(1); EqPoints2.wBz(1)  ]);
% 
% EqPoints3 = solve ( [FSubs2(7:8)] == 0, variables(7:8) );
% FSubs3 = subs(FSubs2, variables(7:8), [EqPoints3.qBx(2);EqPoints3.qBy(2); ]);
% FSubsTest = subs(FSubs3, [IBxy, IBzx, IByz], [ 0, 0, 0])
% FSubsTest2 = subs(FSubs3, symParams, subParams)
% assume(q1Dt>0)
% assume(q3Dt>0)
% assume(q2Dt < 0)
% assume(q4Dt < 0)
% EqPoints4 = solve ( [FSubs3(9:12)] == 0, variables(9:12), 'ReturnConditions', true );
%assume(q1Dt>0)
EqPoints = solve ( [FSubs(1:12)] == 0, variables );


% find Eq points where all rates (and therefore thrusts) are positive (
% positive z direction) and the euler angles are 0 ( normal orientation)
names = fieldnames(EqPoints);
desiredIndex = ones(length(EqPoints.(names{1})),1);
zeroVars = [xBDt; yBDt; zBDt; wBx; wBy; wBz; qBx; qBy;]
for i = 1:length(names)
  if i <= 8
    desiredIndex = desiredIndex & (double(EqPoints.(names{i})) == 0);
  elseif (i == 9 || i == 11)
    desiredIndex = desiredIndex & (double(EqPoints.(names{i})) > 0);
   elseif (i == 10 || i ==12)
    desiredIndex = desiredIndex & (double(EqPoints.(names{i})) < 0);
  else
    warning('Case not handeled')
  end
  
  
end

desiredIndex = find(desiredIndex ==1);


FSubs = subs(FSubs, variables, ...
  [EqPoints.xBDt(desiredIndex); EqPoints.yBDt(desiredIndex); ...
  EqPoints.zBDt(desiredIndex); EqPoints.wBx(desiredIndex); ...
  EqPoints.wBy(desiredIndex); EqPoints.wBz(desiredIndex); ...
  EqPoints.qBx(desiredIndex); EqPoints.qBy(desiredIndex); ...
  EqPoints.q1Dt(desiredIndex); EqPoints.q2Dt(desiredIndex);...
  EqPoints.q3Dt(desiredIndex); EqPoints.q4Dt(desiredIndex)]);

FEqCheck = sum(double(FSubs),'all');
if FEqCheck == 0
  fprintf('Calculated equilibrium point Looks good. \n')
else
  warning('Calculated Equilibrium bpoint is not an equilibrium point')
end


% note from eqpoints we can linearize about any position
uEq =([EqPoints.q1Dt(desiredIndex); EqPoints.q2Dt(desiredIndex); ...
  EqPoints.q3Dt(desiredIndex); EqPoints.q4Dt(desiredIndex)]) ;
uEqVal = double(uEq);


eqPoint = [0, 0, 0, ...  % Free Variables
            EqPoints.qBx(desiredIndex), EqPoints.qBy(desiredIndex), 0, ... % qBz is a free variable but must know what we linearized about
            EqPoints.xBDt(desiredIndex), EqPoints.yBDt(desiredIndex), EqPoints.zBDt(desiredIndex), ...
            EqPoints.wBx(desiredIndex), EqPoints.wBy(desiredIndex), EqPoints.wBz(desiredIndex)];
%DfTest = 
A =subs(Df, [x; T], [eqPoint'; uEqVal] );
ASub = (subs(A, symParams(1:11), subParams(1:11)))
ASub = double(subs(A, symParams,subParams));

eigenvalues=expand(eig(A));

% note stabiility/instability of eigenvalyes 
eigs = eig(ASub);

% Get B From Motion Genesis and Matlab
Df_B=simplify(jacobian(F, T')); 
BMat = zeros(12,4);
BMat = sym(BMat);

BMat(1,1) = 0;
BMat(1,2) = 0;
BMat(1,3) = 0;
BMat(1,4) = 0;
BMat(2,1) = 0;
BMat(2,2) = 0;
BMat(2,3) = 0;
BMat(2,4) = 0;
BMat(3,1) = 0;
BMat(3,2) = 0;
BMat(3,3) = 0;
BMat(3,4) = 0;
BMat(4,1) = 0;
BMat(4,2) = 0;
BMat(4,3) = 0;
BMat(4,4) = 0;
BMat(5,1) = 0;
BMat(5,2) = 0;
BMat(5,3) = 0;
BMat(5,4) = 0;
BMat(6,1) = 0;
BMat(6,2) = 0;
BMat(6,3) = 0;
BMat(6,4) = 0;
BMat(7,1) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q1Dt/(mB+4*mProp);
BMat(7,2) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q2Dt/(mB+4*mProp);
BMat(7,3) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q3Dt/(mB+4*mProp);
BMat(7,4) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q4Dt/(mB+4*mProp);
BMat(8,1) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q1Dt/(mB+4*mProp);
BMat(8,2) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q2Dt/(mB+4*mProp);
BMat(8,3) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q3Dt/(mB+4*mProp);
BMat(8,4) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q4Dt/(mB+4*mProp);
BMat(9,1) = 2*bThrust*cos(qBx)*cos(qBy)*q1Dt/(mB+4*mProp);
BMat(9,2) = 2*bThrust*cos(qBx)*cos(qBy)*q2Dt/(mB+4*mProp);
BMat(9,3) = 2*bThrust*cos(qBx)*cos(qBy)*q3Dt/(mB+4*mProp);
BMat(9,4) = 2*bThrust*cos(qBx)*cos(qBy)*q4Dt/(mB+4*mProp);
BMat(10,1) = (IPropzz*(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy+2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*  ...
q1Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx-2*bThrust*lA*q1Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(10,2) = -(2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*q2Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx-(IByz^2-(  ...
IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy-2*bThrust*lA*q2Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(10,3) = (IPropzz*(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy+2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*  ...
q3Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx+2*bThrust*lA*q3Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(10,4) = -(2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*q4Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx-(IByz^2-(  ...
IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy+2*bThrust*lA*q4Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,1) = (2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*q1Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy-(IBzx^2-(  ...
IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx-2*bThrust*lA*q1Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,2) = -(IPropzz*(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx+2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
q2Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy-2*bThrust*lA*q2Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,3) = (2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*q3Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy-(IBzx^2-(  ...
IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx+2*bThrust*lA*q3Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,4) = -(IPropzz*(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx+2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
q4Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy+2*bThrust*lA*q4Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,1) = ((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IPropzz*wBx-2*bThrust*lA*q1Dt)-IPropzz*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*wBy-  ...
2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q1Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,2) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IPropzz*wBy-2*bThrust*lA*q2Dt)-IPropzz*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
wBx-2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q2Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*  ...
mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,3) = ((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IPropzz*wBx+2*bThrust*lA*q3Dt)-IPropzz*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*wBy-  ...
2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q3Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,4) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IPropzz*wBy+2*bThrust*lA*q4Dt)-IPropzz*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
wBx-2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q4Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*  ...
mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));

 
checkDf_B = simplify(Df_B-BMat);
numEqualElements = sum(double(checkDf_B)== 0, 'all');
numElements = numel(checkDf_B);

if numEqualElements ~= numElements 
  warning('Matlab Calculated B Matrix doesnt match MG calculated B BMatrix')
else
  fprintf('B Matrix calculations match \n')
end


BSub = subs(BMat, x, eqPoint');
BSub = subs(BSub, [q1Dt; q2Dt; q3Dt; q4Dt],uEqVal);
BSub = double(subs(BSub, symParams, subParams));
rankSys = rank(ctrb(ASub,BSub));


if rankSys < length(x)
  warningMessage = sprintf('System is not controllable. Rank of controllability matrix is = %i \n', rankSys);
  warning(warningMessage);
else
  fprintf('System is controllable. Rank of controllability matrix is = %i \n', rankSys)
end

% Create LQR Gain Matrix
Q = eye(12)*1000;%diag( v, 0);
R = eye(4)*0.01;

Klqr = lqr(ASub, BSub, Q, R );
eig(ASub-BSub*Klqr)
save('Klqr.mat', 'Klqr')
save('uEq.mat', 'uEqVal')
