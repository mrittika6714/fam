function simplifiedSolution( xBDt, yBDt, zBDt, qBx, qBy, qBz, wBx, wBy, wBz, bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, mProp, IPropzz, q1Dt, q2Dt, q3Dt, q4Dt )
if( nargin ~= 25 ) error( 'simplifiedSolution expects 25 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: simplifiedSolution.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
KaneSimplifiedSolution = zeros( 6, 1 );





%===========================================================================
F1 = bThrust*q1Dt^2;
F2 = bThrust*q2Dt^2;
F3 = bThrust*q3Dt^2;
F4 = bThrust*q4Dt^2;
T1 = bDrag*F1/bThrust;
T2 = -bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = -bDrag*F4/bThrust;



%===========================================================================
Output = [];

KaneSimplifiedSolution(1) = (F1+F2+F3+F4)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))/(mB+4*mProp);
KaneSimplifiedSolution(2) = (F1+F2+F3+F4)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))/(mB+4*mProp);
KaneSimplifiedSolution(3) = -(9.8*mB+39.2*mProp-F1*cos(qBx)*cos(qBy)-F2*cos(qBx)*cos(qBy)-F3*cos(qBx)*cos(qBy)-F4*cos(qBx)*cos(qBy))/(  ...
mB+4*mProp);
KaneSimplifiedSolution(4) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-T1-T2-T3-T4-wBy*(IBxx*wBx+IBxy*  ...
wBy+IBzx*wBz))+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(lA*(F1-F3)+wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-2*mProp*lA^2*wBx*wBz-wBx*(  ...
IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBx*q1Dt-IPropzz*wBx*q2Dt-IPropzz*wBx*q4Dt-IPropzz*wBx*(4*wBz+q3Dt))+(IByz^2-(IByy+2*mProp*lA^2)*(  ...
IBzz+4*IPropzz+4*mProp*lA^2))*(lA*(F2-F4)+wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-2*mProp*lA^2*wBy*wBz-wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)-  ...
IPropzz*wBy*q1Dt-IPropzz*wBy*q2Dt-IPropzz*wBy*q4Dt-IPropzz*wBy*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
KaneSimplifiedSolution(5) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-T1-T2-T3-T4-wBy*(IBxx*wBx+IBxy*  ...
wBy+IBzx*wBz))-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(lA*(F2-F4)+wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-2*mProp*lA^2*wBy*wBz-wBy*(  ...
IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBy*q1Dt-IPropzz*wBy*q2Dt-IPropzz*wBy*q4Dt-IPropzz*wBy*(4*wBz+q3Dt))-(IBzx^2-(IBxx+2*mProp*lA^2)*(  ...
IBzz+4*IPropzz+4*mProp*lA^2))*(lA*(F1-F3)+wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-2*mProp*lA^2*wBx*wBz-wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)-  ...
IPropzz*wBx*q1Dt-IPropzz*wBx*q2Dt-IPropzz*wBx*q4Dt-IPropzz*wBx*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
KaneSimplifiedSolution(6) = ((IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-T1-T2-T3-T4-wBy*(  ...
IBxx*wBx+IBxy*wBy+IBzx*wBz))+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(lA*(F2-F4)+wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-2*mProp*lA^2*wBy*wBz-  ...
wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBy*q1Dt-IPropzz*wBy*q2Dt-IPropzz*wBy*q4Dt-IPropzz*wBy*(4*wBz+q3Dt))-(IBxy*IBzx-IByz*(IBxx+  ...
2*mProp*lA^2))*(lA*(F1-F3)+wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-2*mProp*lA^2*wBx*wBz-wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBx*q1Dt-  ...
IPropzz*wBx*q2Dt-IPropzz*wBx*q4Dt-IPropzz*wBx*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));


%===========================================
end    % End of function simplifiedSolution
%===========================================
