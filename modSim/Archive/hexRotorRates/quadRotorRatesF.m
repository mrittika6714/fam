function quadRotorRatesF( bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, mProp, IPropxx, IPropyy, IPropzz, qBx, qBy, qBz, wBx, wBy, wBz, q1Dt, q2Dt, q3Dt, q4Dt, xBDt, yBDt, zBDt )
if( nargin ~= 27 ) error( 'quadRotorRatesF expects 27 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotorRatesF.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
F = zeros( 12, 1 );





%===========================================================================


%===========================================================================
Output = [];

F(1) = xBDt;
F(2) = yBDt;
F(3) = zBDt;
F(4) = sin(qBy)*wBz + cos(qBy)*wBx;
F(5) = wBy + tan(qBx)*(sin(qBy)*wBx-cos(qBy)*wBz);
F(6) = -(sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx);
F(7) = bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
F(8) = bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
F(9) = -(9.8*mB+39.2*mProp-bThrust*cos(qBx)*cos(qBy)*q1Dt^2-bThrust*cos(qBx)*cos(qBy)*q2Dt^2-bThrust*cos(qBx)*cos(qBy)*q3Dt^2-  ...
bThrust*cos(qBx)*cos(qBy)*q4Dt^2)/(mB+4*mProp);
F(10) = ((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)+bDrag*q1Dt^2+bDrag*q3Dt^2-wBx*(IBxy*wBx+IByy*wBy+  ...
IByz*wBz)-bDrag*q2Dt^2-bDrag*q4Dt^2)+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBx*wBz+wBx*(IByz*wBy+IBzx*wBx+  ...
IBzz*wBz)+IPropzz*wBx*q1Dt+IPropzz*wBx*q2Dt+IPropzz*wBx*q4Dt+IPropzz*wBx*(4*wBz+q3Dt)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-bThrust*lA*(q1Dt^2-q3Dt^2))+(  ...
IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBy*wBz+wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)+IPropzz*wBy*q1Dt+  ...
IPropzz*wBy*q2Dt+IPropzz*wBy*q4Dt+IPropzz*wBy*(4*wBz+q3Dt)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-bThrust*lA*(q2Dt^2-q4Dt^2)))/(IByz*(2*  ...
IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*  ...
mProp*lA^2)));
F(11) = ((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)+bDrag*q1Dt^2+bDrag*q3Dt^2-wBx*(IBxy*wBx+IByy*wBy+  ...
IByz*wBz)-bDrag*q2Dt^2-bDrag*q4Dt^2)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBy*wBz+wBy*(IByz*wBy+IBzx*wBx+  ...
IBzz*wBz)+IPropzz*wBy*q1Dt+IPropzz*wBy*q2Dt+IPropzz*wBy*q4Dt+IPropzz*wBy*(4*wBz+q3Dt)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-bThrust*lA*(q2Dt^2-q4Dt^2))-(  ...
IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(2*mProp*lA^2*wBx*wBz+wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)+IPropzz*wBx*q1Dt+  ...
IPropzz*wBx*q2Dt+IPropzz*wBx*q4Dt+IPropzz*wBx*(4*wBz+q3Dt)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-bThrust*lA*(q1Dt^2-q3Dt^2)))/(IByz*(2*  ...
IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*  ...
mProp*lA^2)));
F(12) = -((IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)+bDrag*q1Dt^2+bDrag*q3Dt^2-wBx*(IBxy*  ...
wBx+IByy*wBy+IByz*wBz)-bDrag*q2Dt^2-bDrag*q4Dt^2)+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(2*mProp*lA^2*wBy*wBz+wBy*(IByz*wBy+IBzx*wBx+  ...
IBzz*wBz)+IPropzz*wBy*q1Dt+IPropzz*wBy*q2Dt+IPropzz*wBy*q4Dt+IPropzz*wBy*(4*wBz+q3Dt)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-bThrust*lA*(q2Dt^2-q4Dt^2))-(  ...
IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(2*mProp*lA^2*wBx*wBz+wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)+IPropzz*wBx*q1Dt+IPropzz*wBx*q2Dt+  ...
IPropzz*wBx*q4Dt+IPropzz*wBx*(4*wBz+q3Dt)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-bThrust*lA*(q1Dt^2-q3Dt^2)))/(IByz*(2*IBxy*IBzx-IByz*(  ...
IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));


%========================================
end    % End of function quadRotorRatesF
%========================================
