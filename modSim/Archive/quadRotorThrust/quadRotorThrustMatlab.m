function [SolutionToAlgebraicEquations,Output] = quadRotorThrustMatlab( F1, F2, F3, F4, xBDt, yBDt, zBDt, qBx, qBy, qBz, wBx, wBy, wBz, bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx )
if( nargin ~= 23 ) error( 'quadRotorThrustMatlab expects 23 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotorThrustMatlab.m created Nov 11 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================
T1 = bDrag*F1/bThrust;
T2 = -bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = -bDrag*F4/bThrust;

COEF = zeros( 6, 6 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = mB;
COEF(4,4) = IBxx;
COEF(4,5) = IBxy;
COEF(4,6) = IBzx;
COEF(5,4) = IBxy;
COEF(5,5) = IByy;
COEF(5,6) = IByz;
COEF(6,4) = IBzx;
COEF(6,5) = IByz;
COEF(6,6) = IBzz;
RHS = zeros( 1, 6 );
RHS(1) = (F1+F2+F3+F4)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy));
RHS(2) = (F1+F2+F3+F4)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz));
RHS(3) = F1*cos(qBx)*cos(qBy) + F2*cos(qBx)*cos(qBy) + F3*cos(qBx)*cos(qBy) + F4*cos(qBx)*cos(qBy) - g*mB;
RHS(4) = lA*(F2-F4) + wBz*(IBxy*wBx+IByy*wBy+IByz*wBz) - wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz);
RHS(5) = wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz) - lA*(F1-F3) - wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz);
RHS(6) = T1 + T2 + T3 + T4 + wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz) - wBx*(IBxy*wBx+IByy*wBy+IByz*wBz);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
zBDDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);

qBxDt = wBx*cos(qBy) + wBz*sin(qBy);
qByDt = wBy + tan(qBx)*(wBx*sin(qBy)-wBz*cos(qBy));
qBzDt = -(wBx*sin(qBy)-wBz*cos(qBy))/cos(qBx);



%===========================================================================
Output = zeros( 1, 12 );

Output(1) = xBDt;
Output(2) = yBDt;
Output(3) = zBDt;
Output(4) = qBxDt;
Output(5) = qByDt;
Output(6) = qBzDt;

Output(7) = xBDDt;
Output(8) = yBDDt;
Output(9) = zBDDt;
Output(10) = wBxDt;
Output(11) = wByDt;
Output(12) = wBzDt;

%==============================================
end    % End of function quadRotorThrustMatlab
%==============================================
