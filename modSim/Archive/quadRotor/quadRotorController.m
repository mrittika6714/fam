function control = planarQuadRotorController(t, states, setPoint, params)

mode = params.mode;

if strcmp(mode, 'none')
    control = zeros(lengthStates, 1);
    
elseif strcmp(mode, 'constant')
   control = params.value;

elseif strcmp(mode, 'sin')
   control = ones(lengthStates, 1) * sin(t);

elseif strcmp(mode, 'linear')
    K = params.value;
    control = -K*(states-setPoint);
   
end    
end