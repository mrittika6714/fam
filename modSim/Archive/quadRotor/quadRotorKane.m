function [t,VAR,Output] = quadRotorKane
%===========================================================================
% File: quadRotorKane.m created Oct 30 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
eventDetectedByIntegratorTerminate1OrContinue0 = [];
FBz=0; TBx=0; TBy=0; TBz=0; qBxDt=0; qByDt=0; qBzDt=0; wBxDt=0; wByDt=0; wBzDt=0; xBDDt=0; yBDDt=0; zBDDt=0;


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
IBxx                            =  1749.31;                % g*cm^2              Constant
IBxy                            = -24.31;                  % g*cm^2              Constant
IByy                            =  3080.28;                % g*cm^2              Constant
IByz                            = -9.95;                   % kg*m^2              Constant
IBzx                            = -7.89;                   % g*cm^2              Constant
IBzz                            =  1706.3;                 % g*cm^2              Constant
mB                              =  118.17;                 % g                   Constant

qBx                             =  0;                      % deg                 Initial Value
qBy                             =  0;                      % UNITS               Initial Value
qBz                             =  0;                      % UNITS               Initial Value
wBx                             =  0;                      % UNITS               Initial Value
wBy                             =  0;                      % UNITS               Initial Value
wBz                             =  0;                      % UNITS               Initial Value
xB                              =  1;                      % UNITS               Initial Value
yB                              =  1;                      % UNITS               Initial Value
zB                              =  1;                      % UNITS               Initial Value
xBDt                            =  0;                      % UNITS               Initial Value
yBDt                            =  0;                      % UNITS               Initial Value
zBDt                            =  0;                      % UNITS               Initial Value

tInitial                        =  0.0;                    % second              Initial Time
tFinal                          =  1;                      % sec                 Final Time
tStep                           =  0.1;                    % sec                 Integration Step
printIntScreen                  =  1;                      % 0 or +integer       0 is NO screen output
printIntFile                    =  1;                      % 0 or +integer       0 is NO file   output
absError                        =  1.0E-8;                 %                     Absolute Error
relError                        =  1.0E-8;                 %                     Relative Error
%-------------------------------+--------------------------+-------------------+-----------------

% Unit conversions.  UnitSystem: kg, meter, second.
DEGtoRAD = pi / 180.0;
RADtoDEG = 180.0 / pi;
IBxx = IBxx * 1.0E-07;                                     %  Converted from g*cm^2 
IBxy = IBxy * 1.0E-07;                                     %  Converted from g*cm^2 
IByy = IByy * 1.0E-07;                                     %  Converted from g*cm^2 
IBzx = IBzx * 1.0E-07;                                     %  Converted from g*cm^2 
IBzz = IBzz * 1.0E-07;                                     %  Converted from g*cm^2 
mB = mB * 0.001;                                           %  Converted from g 
qBx = qBx * DEGtoRAD;                                      %  Converted from deg 

% Evaluate constants
FBz = 0;
TBx = 0;
TBy = 0;
TBz = 0;


VAR = SetMatrixFromNamedQuantities;
[t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile );
OutputToScreenOrFile( [], 0, 0 );   % Close output files.
if( printIntFile ~= 0 ),  PlotOutputFiles;  end


%===========================================================================
function sys = mdlDerivatives( t, VAR, uSimulink )
%===========================================================================
SetNamedQuantitiesFromMatrix( VAR );
qBxDt = wBx*cos(qBy) + wBz*sin(qBy);
qByDt = wBy + tan(qBx)*(wBx*sin(qBy)-wBz*cos(qBy));
qBzDt = -(wBx*sin(qBy)-wBz*cos(qBy))/cos(qBx);

COEF = zeros( 6, 6 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = mB;
COEF(4,4) = IBxx;
COEF(4,5) = IBxy;
COEF(4,6) = IBzx;
COEF(5,4) = IBxy;
COEF(5,5) = IByy;
COEF(5,6) = IByz;
COEF(6,4) = IBzx;
COEF(6,5) = IByz;
COEF(6,6) = IBzz;
RHS = zeros( 1, 6 );
RHS(1) = FBz*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy));
RHS(2) = FBz*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz));
RHS(3) = FBz*cos(qBx)*cos(qBy) - g*mB;
RHS(4) = TBx + wBz*(IBxy*wBx+IByy*wBy+IByz*wBz) - wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz);
RHS(5) = TBy + wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz) - wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz);
RHS(6) = TBz + wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz) - wBx*(IBxy*wBx+IByy*wBy+IByz*wBz);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
zBDDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);

sys = transpose( SetMatrixOfDerivativesPriorToIntegrationStep );
end



%===========================================================================
function VAR = SetMatrixFromNamedQuantities
%===========================================================================
VAR = zeros( 1, 12 );
VAR(1) = qBx;
VAR(2) = qBy;
VAR(3) = qBz;
VAR(4) = wBx;
VAR(5) = wBy;
VAR(6) = wBz;
VAR(7) = xB;
VAR(8) = yB;
VAR(9) = zB;
VAR(10) = xBDt;
VAR(11) = yBDt;
VAR(12) = zBDt;
end


%===========================================================================
function SetNamedQuantitiesFromMatrix( VAR )
%===========================================================================
qBx = VAR(1);
qBy = VAR(2);
qBz = VAR(3);
wBx = VAR(4);
wBy = VAR(5);
wBz = VAR(6);
xB = VAR(7);
yB = VAR(8);
zB = VAR(9);
xBDt = VAR(10);
yBDt = VAR(11);
zBDt = VAR(12);
end


%===========================================================================
function VARp = SetMatrixOfDerivativesPriorToIntegrationStep
%===========================================================================
VARp = zeros( 1, 12 );
VARp(1) = qBxDt;
VARp(2) = qByDt;
VARp(3) = qBzDt;
VARp(4) = wBxDt;
VARp(5) = wByDt;
VARp(6) = wBzDt;
VARp(7) = xBDt;
VARp(8) = yBDt;
VARp(9) = zBDt;
VARp(10) = xBDDt;
VARp(11) = yBDDt;
VARp(12) = zBDDt;
end



%===========================================================================
function Output = mdlOutputs( t, VAR, uSimulink )
%===========================================================================
Output = zeros( 1, 13 );
Output(1) = t;
Output(2) = xB;
Output(3) = yB;
Output(4) = zB;

Output(5) = t;
Output(6) = qBx*RADtoDEG;                             % Converted to deg
Output(7) = qBy*RADtoDEG;                             % Converted to deg
Output(8) = qBz*RADtoDEG;                             % Converted to deg

Output(9) = t;
Output(10) = FBz;
Output(11) = TBx;
Output(12) = TBy;
Output(13) = TBz;
end


%===========================================================================
function OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile )
%===========================================================================
persistent FileIdentifier hasHeaderInformationBeenWritten;

if( isempty(Output) ),
   if( ~isempty(FileIdentifier) ),
      for( i = 1 : 3 ),  fclose( FileIdentifier(i) );  end
      clear FileIdentifier;
      fprintf( 1, '\n Output is in the files quadRotorKane.i  (i=1,2,3)\n\n' );
   end
   clear hasHeaderInformationBeenWritten;
   return;
end

if( isempty(hasHeaderInformationBeenWritten) ),
   if( shouldPrintToScreen ),
      fprintf( 1,                '%%       t             xB             yB             zB\n' );
      fprintf( 1,                '%%     (sec)           (m)            (m)            (m)\n\n' );
   end
   if( shouldPrintToFile && isempty(FileIdentifier) ),
      FileIdentifier = zeros( 1, 3 );
      FileIdentifier(1) = fopen('quadRotorKane.1', 'wt');   if( FileIdentifier(1) == -1 ), error('Error: unable to open file quadRotorKane.1'); end
      fprintf(FileIdentifier(1), '%% FILE: quadRotorKane.1\n%%\n' );
      fprintf(FileIdentifier(1), '%%       t             xB             yB             zB\n' );
      fprintf(FileIdentifier(1), '%%     (sec)           (m)            (m)            (m)\n\n' );
      FileIdentifier(2) = fopen('quadRotorKane.2', 'wt');   if( FileIdentifier(2) == -1 ), error('Error: unable to open file quadRotorKane.2'); end
      fprintf(FileIdentifier(2), '%% FILE: quadRotorKane.2\n%%\n' );
      fprintf(FileIdentifier(2), '%%       t             qBx            qBy            qBz\n' );
      fprintf(FileIdentifier(2), '%%     (sec)          (deg)          (deg)          (deg)\n\n' );
      FileIdentifier(3) = fopen('quadRotorKane.3', 'wt');   if( FileIdentifier(3) == -1 ), error('Error: unable to open file quadRotorKane.3'); end
      fprintf(FileIdentifier(3), '%% FILE: quadRotorKane.3\n%%\n' );
      fprintf(FileIdentifier(3), '%%       t             FBz            TBx            TBy            TBz\n' );
      fprintf(FileIdentifier(3), '%%     (sec)           (N)           (N*m)          (N*M)          (N*M)\n\n' );
   end
   hasHeaderInformationBeenWritten = 1;
end

if( shouldPrintToScreen ), WriteNumericalData( 1,                 Output(1:4) );  end
if( shouldPrintToFile ),   WriteNumericalData( FileIdentifier(1), Output(1:4) );  end
if( shouldPrintToFile ),   WriteNumericalData( FileIdentifier(2), Output(5:8) );  end
if( shouldPrintToFile ),   WriteNumericalData( FileIdentifier(3), Output(9:13) );  end
end


%===========================================================================
function WriteNumericalData( fileIdentifier, Output )
%===========================================================================
numberOfOutputQuantities = length( Output );
if( numberOfOutputQuantities > 0 ),
   for( i = 1 : numberOfOutputQuantities ),
      fprintf( fileIdentifier, ' %- 14.6E', Output(i) );
   end
   fprintf( fileIdentifier, '\n' );
end
end



%===========================================================================
function PlotOutputFiles
%===========================================================================
figure;
data = load( 'quadRotorKane.1' ); 
plot( data(:,1),data(:,2),'-b', data(:,1),data(:,3),'-.g', data(:,1),data(:,4),'--r', 'LineWidth',3 );
legend( 'xB (m)', 'yB (m)', 'zB (m)' );
xlabel('t (sec)');   % ylabel('Some y-axis label');   title('Some plot title');
clear data;

figure;
data = load( 'quadRotorKane.2' ); 
plot( data(:,1),data(:,2),'-b', data(:,1),data(:,3),'-.g', data(:,1),data(:,4),'--r', 'LineWidth',3 );
legend( 'qBx (deg)', 'qBy (deg)', 'qBz (deg)' );
xlabel('t (sec)');   % ylabel('Some y-axis label');   title('Some plot title');
clear data;

figure;
data = load( 'quadRotorKane.3' ); 
plot( data(:,1),data(:,2),'-b', data(:,1),data(:,3),'-.g', data(:,1),data(:,4),'--r', data(:,1),data(:,5),'-m', 'LineWidth',3 );
legend( 'FBz (N)', 'TBx (N*m)', 'TBy (N*M)', 'TBz (N*M)' );
xlabel('t (sec)');   % ylabel('Some y-axis label');   title('Some plot title');
clear data;
end



%===========================================================================
function [functionsToEvaluateForEvent, eventTerminatesIntegration1Otherwise0ToContinue, eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1] = EventDetection( t, VAR, uSimulink )
%===========================================================================
% Detects when designated functions are zero or cross zero with positive or negative slope.
% Step 1: Uncomment call to mdlDerivatives and mdlOutputs.
% Step 2: Change functionsToEvaluateForEvent,                      e.g., change  []  to  [t - 5.67]  to stop at t = 5.67.
% Step 3: Change eventTerminatesIntegration1Otherwise0ToContinue,  e.g., change  []  to  [1]  to stop integrating.
% Step 4: Change eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1,  e.g., change  []  to  [1].
% Step 5: Possibly modify function EventDetectedByIntegrator (if eventTerminatesIntegration1Otherwise0ToContinue is 0).
%---------------------------------------------------------------------------
% mdlDerivatives( t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
% mdlOutputs(     t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
functionsToEvaluateForEvent = [];
eventTerminatesIntegration1Otherwise0ToContinue = [];
eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1 = [];
eventDetectedByIntegratorTerminate1OrContinue0 = eventTerminatesIntegration1Otherwise0ToContinue;
end


%===========================================================================
function [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents )
%===========================================================================
isIntegrationFinished = eventDetectedByIntegratorTerminate1OrContinue0( nIndexOfEvents );
if( ~isIntegrationFinished ),
   SetNamedQuantitiesFromMatrix( VAR );
%  Put code here to modify how integration continues.
   VAR = SetMatrixFromNamedQuantities;
end
end



%===========================================================================
function [t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile )
%===========================================================================
OdeMatlabOptions = odeset( 'RelTol',relError, 'AbsTol',absError, 'MaxStep',tStep, 'Events',@EventDetection );
t = tInitial;                 epsilonT = 0.001*tStep;                   tFinalMinusEpsilonT = tFinal - epsilonT;
printCounterScreen = 0;       integrateForward = tFinal >= tInitial;    tAtEndOfIntegrationStep = t + tStep;
printCounterFile   = 0;       isIntegrationFinished = 0;
mdlDerivatives( t, VAR, 0 );
while 1,
   if( (integrateForward && t >= tFinalMinusEpsilonT) || (~integrateForward && t <= tFinalMinusEpsilonT) ), isIntegrationFinished = 1;  end
   shouldPrintToScreen = printIntScreen && ( isIntegrationFinished || printCounterScreen <= 0.01 );
   shouldPrintToFile   = printIntFile   && ( isIntegrationFinished || printCounterFile   <= 0.01 );
   if( isIntegrationFinished || shouldPrintToScreen || shouldPrintToFile ),
      Output = mdlOutputs( t, VAR, 0 );
      OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile );
      if( isIntegrationFinished ), break;  end
      if( shouldPrintToScreen ), printCounterScreen = printIntScreen;  end
      if( shouldPrintToFile ),   printCounterFile   = printIntFile;    end
   end
   [TimeOdeArray, VarOdeArray, timeEventOccurredInIntegrationStep, nStatesArraysAtEvent, nIndexOfEvents] = ode45( @mdlDerivatives, [t tAtEndOfIntegrationStep], VAR, OdeMatlabOptions, 0 );
   if( isempty(timeEventOccurredInIntegrationStep) ),
      lastIndex = length( TimeOdeArray );
      t = TimeOdeArray( lastIndex );
      VAR = VarOdeArray( lastIndex, : );
      printCounterScreen = printCounterScreen - 1;
      printCounterFile   = printCounterFile   - 1;
      if( abs(tAtEndOfIntegrationStep - t) >= abs(epsilonT) ), warning('numerical integration failed'); break;  end
      tAtEndOfIntegrationStep = t + tStep;
      if( (integrateForward && tAtEndOfIntegrationStep > tFinal) || (~integrateForward && tAtEndOfIntegrationStep < tFinal) ) tAtEndOfIntegrationStep = tFinal;  end
   else
      t = timeEventOccurredInIntegrationStep( 1 );    % time  at firstEvent = 1 during this integration step.
      VAR = nStatesArraysAtEvent( 1, : );             % state at firstEvent = 1 during this integration step.
      printCounterScreen = 0;
      printCounterFile   = 0;
      [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents(1) );
   end
end
end


%======================================
end    % End of function quadRotorKane
%======================================
