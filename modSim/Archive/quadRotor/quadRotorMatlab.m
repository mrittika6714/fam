function [SolutionToAlgebraicEquations,Output] = quadRotorMatlab( FBz, TBx, TBy, TBz, xBDt, yBDt, zBDt, qBx, qBy, qBz, wBx, wBy, wBz, bThrust, bTorque, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx )
if( nargin ~= 23 ) error( 'quadRotorMatlab expects 23 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotorMatlab.m created Oct 28 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================
COEF = zeros( 6, 6 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = mB;
COEF(4,4) = IBxx;
COEF(4,5) = IBxy;
COEF(4,6) = IBzx;
COEF(5,4) = IBxy;
COEF(5,5) = IByy;
COEF(5,6) = IByz;
COEF(6,4) = IBzx;
COEF(6,5) = IByz;
COEF(6,6) = IBzz;
RHS = zeros( 1, 6 );
RHS(1) = FBz*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy));
RHS(2) = FBz*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz));
RHS(3) = FBz*cos(qBx)*cos(qBy) - g*mB;
RHS(4) = TBx + wBz*(IBxy*wBx+IByy*wBy+IByz*wBz) - wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz);
RHS(5) = TBy + wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz) - wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz);
RHS(6) = TBz + wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz) - wBx*(IBxy*wBx+IByy*wBy+IByz*wBz);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
zBDDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);

qBxDt = wBx*cos(qBy) + wBz*sin(qBy);
qByDt = wBy + tan(qBx)*(wBx*sin(qBy)-wBz*cos(qBy));
qBzDt = -(wBx*sin(qBy)-wBz*cos(qBy))/cos(qBx);



%===========================================================================
Output = zeros( 1, 12 );

Output(1) = xBDt;
Output(2) = yBDt;
Output(3) = zBDt;
Output(4) = qBxDt;
Output(5) = qByDt;
Output(6) = qBzDt;

Output(7) = xBDDt;
Output(8) = yBDDt;
Output(9) = zBDDt;
Output(10) = wBxDt;
Output(11) = wByDt;
Output(12) = wBzDt;

%========================================
end    % End of function quadRotorMatlab
%========================================
