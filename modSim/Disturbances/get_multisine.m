function value = get_multisine(t, t0, multisine)
%get_multisine Get value of multisine at specified time (interpolate as
%needed)
%    inputs:
%        t          = time at which to sample multisine signal
%        t0         = Start time for 
%        multisine  = (N x 2) array of multisine data (time, signal)
%
%    outputs:
%        value = value of multisine signal (-1 -> 1) at specified time 
%                empty array [] returned on error
%   
%    NOTE: Matlab will treat the multisine array as a reference (not
%          pass-by-copy) as long as it isn't modified in this function

% Make sure array isn't empty 
if(isempty(multisine))
  fprintf("ERROR: 'multisine' has not been generated!\n");
  value = [];
  return;
end

% Extract time and signal arrays
ms_time   = multisine(:,1);
ms_signal = multisine(:,2);

% Return zero if t is before start time
t = t - t0;  % Shift time relative to 
if(t < 0)
  value = 0;
  return;
end

% Wrap-around if passed end of multisine signal
t_end = ms_time(end);
if( t > t_end)
  while(t > t_end)
    t = t - t_end; % Adjust time to "wrap-around"
  end
elseif(t == t_end)
  value = ms_signal(end);
  return;
end

% t is now guaranteed to be < t_end

% % Get the index of the first multisine time that is less than or equal to t
% i_t = find(t >= ms_time, 1, 'first');
% if(isempty(i_t))
%   fprintf("Couldn't find t > ms_time! (%.3f)\n", t);
%   value = [];
%   return;
% else
% %   fprintf("i_t: %d\n", i_t);
% end
% 
% % Calculate the interpolated multisine signal value corresonding to t
% scale = (t - ms_time(i_t)) / (ms_time(i_t + 1) - ms_time(i_t));
% value = ms_signal(i_t) + scale * (ms_signal(i_t + 1) - ms_signal(i_t));

% Interp1 with default linear interpolation method should be equivalent!
value = interp1(ms_time, ms_signal, t);

end