function [r,t,f,ph,A,Ts] = gen_multisine(f0,df,Nf,Nper,Ntry,Ts)
%gen_multisine Generate multisine signal
%    inputs:
%        f0   = fundamental frequency
%        df   = delta frequency (frequency step size)
%        Nf   = Number of frequencies to include
%        Nper = Number of periods (of f0) to simulate
%        Ntry = Number of tries/attempts to generate signal?
%        Ts   = Sample time (time between samples)
%    outputs:
%        r    = reference signal (-1 -> 1)
%        t    = Time vector (with step size: Ts)
%        ph   = phases
%        A    = Amplitudes
%        Ts   = Sample time (dt)

fprintf("Generating a multisine signal\n");

% Seed random number generator for repeatable simulations
rng(123456);

%
f = (0:df:(df*(Nf-1)))'+f0;
fmax = max(f);

if nargin<5
    Ts = 1/(2*fmax);
else
    if Ts>1/(2*fmax)
        Ts = 1/(2*fmax);
    end
end
t = 0:Ts:(Nper/f0);
w = 2*pi*f; % ^ in rad/s

r_min_range = inf;
for i = 1:Ntry
    fprintf("  Generating candidate signal %02d...\n", i);
    ph_cand = 2*pi*rand(size(f)); % random phasing
    A_cand  = ones(size(f)); %ones for a uniform multisine (ramp down for a pink multisine)
    r_cand  = sum(A_cand.*sin(w*t+ph_cand)); %construct the time series position
    r_range = max(r_cand)-min(r_cand);
    if r_range<r_min_range
        r = r_cand;
        ph = ph_cand;
        A = A_cand;
        r_min_range = r_range;
    end
end
r = 2*r/r_min_range;
r = r-mean(r);
A = A/r_min_range;
end