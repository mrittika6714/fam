function rEE_No_N = hexRotorRRManip_EEPosition( len1, len2, xB, yB, zB, qBx, qBy, qBz, qL1, qL2 )
if( nargin ~= 10 ) error( 'hexRotorRRManip_EEPosition expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_EEPosition.m created Jun 27 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
rEE_No_N = zeros( 3, 1 );
z = zeros( 1, 386 );




%===========================================================================
z(1) = cos(qBy);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(5) = sin(qBz);
z(8) = cos(qBx);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(14) = z(1)*z(8);
z(203) = cos(qL1);
z(204) = sin(qL1);
z(205) = z(3)*z(203) + z(14)*z(204);
z(208) = z(14)*z(203) - z(3)*z(204);
z(215) = cos(qL2);
z(216) = sin(qL2);
z(218) = z(205)*z(215) + z(208)*z(216);
z(280) = z(12)*z(204) - z(10)*z(203);
z(281) = z(10)*z(204) + z(12)*z(203);
z(282) = z(11)*z(203) + z(13)*z(204);
z(283) = z(13)*z(203) - z(11)*z(204);
z(305) = z(215)*z(280) + z(216)*z(281);
z(307) = z(215)*z(282) + z(216)*z(283);



%===========================================================================
Output = [];

rEE_No_N(1) = xB + len1*z(280) + len2*z(305);
rEE_No_N(2) = yB + len1*z(282) + len2*z(307);
rEE_No_N(3) = zB + len1*z(205) + len2*z(218);


%===================================================
end    % End of function hexRotorRRManip_EEPosition
%===================================================
