function [Output] = thrust2rates( F_1, F_2, F_3, F_4, F_5, F_6, bThrust )
if( nargin ~= 7 ) error( 'thrust2rates expects 7 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: thrust2rates.m created May 28 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 1984 );



%===========================================================================
z(1973) = abs(F_1);
z(1974) = sqrt(z(1973))/sqrt(bThrust);
w1 = sign(F_1)*z(1974);
z(1975) = abs(F_2);
z(1976) = sqrt(z(1975))/sqrt(bThrust);
w2 = sign(F_2)*z(1976);
z(1977) = abs(F_3);
z(1978) = sqrt(z(1977))/sqrt(bThrust);
w3 = sign(F_3)*z(1978);
z(1979) = abs(F_4);
z(1980) = sqrt(z(1979))/sqrt(bThrust);
w4 = sign(F_4)*z(1980);
z(1981) = abs(F_5);
z(1982) = sqrt(z(1981))/sqrt(bThrust);
w5 = sign(F_5)*z(1982);
z(1983) = abs(F_6);
z(1984) = sqrt(z(1983))/sqrt(bThrust);
w6 = sign(F_6)*z(1984);



%===========================================================================
Output = zeros( 1, 6 );

Output(1) = w1;
Output(2) = w2;
Output(3) = w3;
Output(4) = w4;
Output(5) = w5;
Output(6) = w6;

%=====================================
end    % End of function thrust2rates
%=====================================
