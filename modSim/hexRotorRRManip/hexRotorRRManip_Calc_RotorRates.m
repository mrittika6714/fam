function [SolutionToAlgebraicEquations] = hexRotorRRManip_Calc_RotorRates( bThrust, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, F1, F2, F3, F4, F5, F6 )
if( nargin ~= 13 ) error( 'hexRotorRRManip_Calc_RotorRates expects 13 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_Calc_RotorRates.m created Aug 26 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
rotorRates = zeros( 1, 6 );
z = zeros( 1, 972 );



%===========================================================================
z(916) = abs(F1/bThrust);
z(917) = sqrt(z(916));
z(918) = abs(F2/bThrust);
z(919) = sqrt(z(918));
z(920) = abs(F3/bThrust);
z(921) = sqrt(z(920));
z(922) = abs(F4/bThrust);
z(923) = sqrt(z(922));
z(924) = abs(F5/bThrust);
z(925) = sqrt(z(924));
z(926) = abs(F6/bThrust);
z(927) = sqrt(z(926));
z(961) = sign(F1);
z(962) = P1Dir*z(917)*z(961);
z(963) = sign(F2);
z(964) = P2Dir*z(919)*z(963);
z(965) = sign(F3);
z(966) = P3Dir*z(921)*z(965);
z(967) = sign(F4);
z(968) = P4Dir*z(923)*z(967);
z(969) = sign(F5);
z(970) = P5Dir*z(925)*z(969);
z(971) = sign(F6);
z(972) = P6Dir*z(927)*z(971);

COEF = zeros( 6, 6 );
COEF(1,1) = -1;
COEF(2,2) = -1;
COEF(3,3) = -1;
COEF(4,4) = -1;
COEF(5,5) = -1;
COEF(6,6) = -1;
RHS = zeros( 1, 6 );
RHS(1) = -z(962);
RHS(2) = -z(964);
RHS(3) = -z(966);
RHS(4) = -z(968);
RHS(5) = -z(970);
RHS(6) = -z(972);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
qP1zDt = SolutionToAlgebraicEquations(1);
qP2zDt = SolutionToAlgebraicEquations(2);
qP3zDt = SolutionToAlgebraicEquations(3);
qP4zDt = SolutionToAlgebraicEquations(4);
qP5zDt = SolutionToAlgebraicEquations(5);
qP6zDt = SolutionToAlgebraicEquations(6);



%===========================================================================
Output = [];

rotorRates(1,1) = qP1zDt;
rotorRates(1,2) = qP2zDt;
rotorRates(1,3) = qP3zDt;
rotorRates(1,4) = qP4zDt;
rotorRates(1,5) = qP5zDt;
rotorRates(1,6) = qP6zDt;


%========================================================
end    % End of function hexRotorRRManip_Calc_RotorRates
%========================================================
