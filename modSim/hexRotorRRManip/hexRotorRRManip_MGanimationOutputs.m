function [rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N, N_R_P1, ...
  N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6, rL2o_No_N, rEE_No_N ]  = hexRotorRRManip_MGanimationOutputs( xB, ...
  yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, ...
  xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, ...
  qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, ...
  qP6y, qP6z, len1, len2, qL1, qL2 )
if( nargin ~= 46 ) error( 'hexRotorRRManip_MGanimationOutputs expects 46 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_MGanimationOutputs.m created Jun 27 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
rP1_No_N = zeros( 3, 1 );
rP2_No_N = zeros( 3, 1 );
rP3_No_N = zeros( 3, 1 );
rP4_No_N = zeros( 3, 1 );
rP5_No_N = zeros( 3, 1 );
rP6_No_N = zeros( 3, 1 );
rL2o_No_N = zeros( 3, 1 );
rEE_No_N = zeros( 3, 1 );
N_R_P1 = zeros( 3, 3 );
N_R_P2 = zeros( 3, 3 );
N_R_P3 = zeros( 3, 3 );
N_R_P4 = zeros( 3, 3 );
N_R_P5 = zeros( 3, 3 );
N_R_P6 = zeros( 3, 3 );
z = zeros( 1, 422 );




%===========================================================================
z(1) = cos(qBy);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(5) = sin(qBz);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(14) = z(1)*z(8);
z(23) = cos(qP1y);
z(24) = sin(qP1x);
z(25) = sin(qP1y);
z(26) = z(24)*z(25);
z(27) = cos(qP1x);
z(28) = z(25)*z(27);
z(29) = z(23)*z(24);
z(30) = z(23)*z(27);
z(31) = cos(qP1z);
z(32) = sin(qP1z);
z(33) = z(23)*z(31);
z(34) = z(23)*z(32);
z(35) = z(26)*z(31) + z(27)*z(32);
z(36) = z(27)*z(31) - z(26)*z(32);
z(37) = z(24)*z(32) - z(28)*z(31);
z(38) = z(24)*z(31) + z(28)*z(32);
z(39) = z(3)*z(35) + z(14)*z(37) - z(9)*z(33);
z(41) = z(3)*z(36) + z(9)*z(34) + z(14)*z(38);
z(44) = z(30)*z(14) - z(25)*z(9) - z(29)*z(3);
z(53) = cos(qP2y);
z(54) = sin(qP2x);
z(55) = sin(qP2y);
z(56) = z(54)*z(55);
z(57) = cos(qP2x);
z(58) = z(55)*z(57);
z(59) = z(53)*z(54);
z(60) = z(53)*z(57);
z(61) = cos(qP2z);
z(62) = sin(qP2z);
z(63) = z(53)*z(61);
z(64) = z(53)*z(62);
z(65) = z(56)*z(61) + z(57)*z(62);
z(66) = z(57)*z(61) - z(56)*z(62);
z(67) = z(54)*z(62) - z(58)*z(61);
z(68) = z(54)*z(61) + z(58)*z(62);
z(69) = z(3)*z(65) + z(14)*z(67) - z(9)*z(63);
z(71) = z(3)*z(66) + z(9)*z(64) + z(14)*z(68);
z(74) = z(60)*z(14) - z(55)*z(9) - z(59)*z(3);
z(83) = cos(qP3y);
z(84) = sin(qP3x);
z(85) = sin(qP3y);
z(86) = z(84)*z(85);
z(87) = cos(qP3x);
z(88) = z(85)*z(87);
z(89) = z(83)*z(84);
z(90) = z(83)*z(87);
z(91) = cos(qP3z);
z(92) = sin(qP3z);
z(93) = z(83)*z(91);
z(94) = z(83)*z(92);
z(95) = z(86)*z(91) + z(87)*z(92);
z(96) = z(87)*z(91) - z(86)*z(92);
z(97) = z(84)*z(92) - z(88)*z(91);
z(98) = z(84)*z(91) + z(88)*z(92);
z(99) = z(3)*z(95) + z(14)*z(97) - z(9)*z(93);
z(101) = z(3)*z(96) + z(9)*z(94) + z(14)*z(98);
z(104) = z(90)*z(14) - z(85)*z(9) - z(89)*z(3);
z(113) = cos(qP4y);
z(114) = sin(qP4x);
z(115) = sin(qP4y);
z(116) = z(114)*z(115);
z(117) = cos(qP4x);
z(118) = z(115)*z(117);
z(119) = z(113)*z(114);
z(120) = z(113)*z(117);
z(121) = cos(qP4z);
z(122) = sin(qP4z);
z(123) = z(113)*z(121);
z(124) = z(113)*z(122);
z(125) = z(116)*z(121) + z(117)*z(122);
z(126) = z(117)*z(121) - z(116)*z(122);
z(127) = z(114)*z(122) - z(118)*z(121);
z(128) = z(114)*z(121) + z(118)*z(122);
z(129) = z(3)*z(125) + z(14)*z(127) - z(9)*z(123);
z(131) = z(3)*z(126) + z(9)*z(124) + z(14)*z(128);
z(134) = z(120)*z(14) - z(115)*z(9) - z(119)*z(3);
z(143) = cos(qP5y);
z(144) = sin(qP5x);
z(145) = sin(qP5y);
z(146) = z(144)*z(145);
z(147) = cos(qP5x);
z(148) = z(145)*z(147);
z(149) = z(143)*z(144);
z(150) = z(143)*z(147);
z(151) = cos(qP5z);
z(152) = sin(qP5z);
z(153) = z(143)*z(151);
z(154) = z(143)*z(152);
z(155) = z(146)*z(151) + z(147)*z(152);
z(156) = z(147)*z(151) - z(146)*z(152);
z(157) = z(144)*z(152) - z(148)*z(151);
z(158) = z(144)*z(151) + z(148)*z(152);
z(159) = z(3)*z(155) + z(14)*z(157) - z(9)*z(153);
z(161) = z(3)*z(156) + z(9)*z(154) + z(14)*z(158);
z(164) = z(150)*z(14) - z(145)*z(9) - z(149)*z(3);
z(173) = cos(qP6y);
z(174) = sin(qP6x);
z(175) = sin(qP6y);
z(176) = z(174)*z(175);
z(177) = cos(qP6x);
z(178) = z(175)*z(177);
z(179) = z(173)*z(174);
z(180) = z(173)*z(177);
z(181) = cos(qP6z);
z(182) = sin(qP6z);
z(183) = z(173)*z(181);
z(184) = z(173)*z(182);
z(185) = z(176)*z(181) + z(177)*z(182);
z(186) = z(177)*z(181) - z(176)*z(182);
z(187) = z(174)*z(182) - z(178)*z(181);
z(188) = z(174)*z(181) + z(178)*z(182);
z(189) = z(3)*z(185) + z(14)*z(187) - z(9)*z(183);
z(191) = z(3)*z(186) + z(9)*z(184) + z(14)*z(188);
z(194) = z(180)*z(14) - z(175)*z(9) - z(179)*z(3);
z(203) = cos(qL1);
z(204) = sin(qL1);
z(205) = z(3)*z(203) + z(14)*z(204);
z(208) = z(14)*z(203) - z(3)*z(204);
z(215) = cos(qL2);
z(216) = sin(qL2);
z(218) = z(205)*z(215) + z(208)*z(216);
z(280) = z(12)*z(204) - z(10)*z(203);
z(281) = z(10)*z(204) + z(12)*z(203);
z(282) = z(11)*z(203) + z(13)*z(204);
z(283) = z(13)*z(203) - z(11)*z(204);
z(305) = z(215)*z(280) + z(216)*z(281);
z(307) = z(215)*z(282) + z(216)*z(283);
z(387) = z(6)*z(33) + z(12)*z(37) - z(10)*z(35);
z(388) = z(7)*z(33) + z(11)*z(35) + z(13)*z(37);
z(389) = z(12)*z(38) - z(6)*z(34) - z(10)*z(36);
z(390) = z(11)*z(36) + z(13)*z(38) - z(7)*z(34);
z(391) = z(25)*z(6) + z(29)*z(10) + z(30)*z(12);
z(392) = z(25)*z(7) + z(30)*z(13) - z(29)*z(11);
z(393) = z(6)*z(63) + z(12)*z(67) - z(10)*z(65);
z(394) = z(7)*z(63) + z(11)*z(65) + z(13)*z(67);
z(395) = z(12)*z(68) - z(6)*z(64) - z(10)*z(66);
z(396) = z(11)*z(66) + z(13)*z(68) - z(7)*z(64);
z(397) = z(55)*z(6) + z(59)*z(10) + z(60)*z(12);
z(398) = z(55)*z(7) + z(60)*z(13) - z(59)*z(11);
z(399) = z(6)*z(93) + z(12)*z(97) - z(10)*z(95);
z(400) = z(7)*z(93) + z(11)*z(95) + z(13)*z(97);
z(401) = z(12)*z(98) - z(6)*z(94) - z(10)*z(96);
z(402) = z(11)*z(96) + z(13)*z(98) - z(7)*z(94);
z(403) = z(85)*z(6) + z(89)*z(10) + z(90)*z(12);
z(404) = z(85)*z(7) + z(90)*z(13) - z(89)*z(11);
z(405) = z(6)*z(123) + z(12)*z(127) - z(10)*z(125);
z(406) = z(7)*z(123) + z(11)*z(125) + z(13)*z(127);
z(407) = z(12)*z(128) - z(6)*z(124) - z(10)*z(126);
z(408) = z(11)*z(126) + z(13)*z(128) - z(7)*z(124);
z(409) = z(115)*z(6) + z(119)*z(10) + z(120)*z(12);
z(410) = z(115)*z(7) + z(120)*z(13) - z(119)*z(11);
z(411) = z(6)*z(153) + z(12)*z(157) - z(10)*z(155);
z(412) = z(7)*z(153) + z(11)*z(155) + z(13)*z(157);
z(413) = z(12)*z(158) - z(6)*z(154) - z(10)*z(156);
z(414) = z(11)*z(156) + z(13)*z(158) - z(7)*z(154);
z(415) = z(145)*z(6) + z(149)*z(10) + z(150)*z(12);
z(416) = z(145)*z(7) + z(150)*z(13) - z(149)*z(11);
z(417) = z(6)*z(183) + z(12)*z(187) - z(10)*z(185);
z(418) = z(7)*z(183) + z(11)*z(185) + z(13)*z(187);
z(419) = z(12)*z(188) - z(6)*z(184) - z(10)*z(186);
z(420) = z(11)*z(186) + z(13)*z(188) - z(7)*z(184);
z(421) = z(175)*z(6) + z(179)*z(10) + z(180)*z(12);
z(422) = z(175)*z(7) + z(180)*z(13) - z(179)*z(11);



%===========================================================================
Output = [];


rP1_No_N(1) = xB + xP1*z(6) + zP1*z(12) - yP1*z(10);
rP1_No_N(2) = yB + xP1*z(7) + yP1*z(11) + zP1*z(13);
rP1_No_N(3) = zB + yP1*z(3) + zP1*z(14) - xP1*z(9);

rP2_No_N(1) = xB + xP2*z(6) + zP2*z(12) - yP2*z(10);
rP2_No_N(2) = yB + xP2*z(7) + yP2*z(11) + zP2*z(13);
rP2_No_N(3) = zB + yP2*z(3) + zP2*z(14) - xP2*z(9);

rP3_No_N(1) = xB + xP3*z(6) + zP3*z(12) - yP3*z(10);
rP3_No_N(2) = yB + xP3*z(7) + yP3*z(11) + zP3*z(13);
rP3_No_N(3) = zB + yP3*z(3) + zP3*z(14) - xP3*z(9);

rP4_No_N(1) = xB + xP4*z(6) + zP4*z(12) - yP4*z(10);
rP4_No_N(2) = yB + xP4*z(7) + yP4*z(11) + zP4*z(13);
rP4_No_N(3) = zB + yP4*z(3) + zP4*z(14) - xP4*z(9);

rP5_No_N(1) = xB + xP5*z(6) + zP5*z(12) - yP5*z(10);
rP5_No_N(2) = yB + xP5*z(7) + yP5*z(11) + zP5*z(13);
rP5_No_N(3) = zB + yP5*z(3) + zP5*z(14) - xP5*z(9);

rP6_No_N(1) = xB + xP6*z(6) + zP6*z(12) - yP6*z(10);
rP6_No_N(2) = yB + xP6*z(7) + yP6*z(11) + zP6*z(13);
rP6_No_N(3) = zB + yP6*z(3) + zP6*z(14) - xP6*z(9);

rL2o_No_N(1) = xB + len1*z(280);
rL2o_No_N(2) = yB + len1*z(282);
rL2o_No_N(3) = zB + len1*z(205);

rEE_No_N(1) = xB + len1*z(280) + len2*z(305);
rEE_No_N(2) = yB + len1*z(282) + len2*z(307);
rEE_No_N(3) = zB + len1*z(205) + len2*z(218);

N_R_P1(1,1) = z(387);
N_R_P1(1,2) = z(388);
N_R_P1(1,3) = z(39);
N_R_P1(2,1) = z(389);
N_R_P1(2,2) = z(390);
N_R_P1(2,3) = z(41);
N_R_P1(3,1) = z(391);
N_R_P1(3,2) = z(392);
N_R_P1(3,3) = z(44);

N_R_P2(1,1) = z(393);
N_R_P2(1,2) = z(394);
N_R_P2(1,3) = z(69);
N_R_P2(2,1) = z(395);
N_R_P2(2,2) = z(396);
N_R_P2(2,3) = z(71);
N_R_P2(3,1) = z(397);
N_R_P2(3,2) = z(398);
N_R_P2(3,3) = z(74);

N_R_P3(1,1) = z(399);
N_R_P3(1,2) = z(400);
N_R_P3(1,3) = z(99);
N_R_P3(2,1) = z(401);
N_R_P3(2,2) = z(402);
N_R_P3(2,3) = z(101);
N_R_P3(3,1) = z(403);
N_R_P3(3,2) = z(404);
N_R_P3(3,3) = z(104);

N_R_P4(1,1) = z(405);
N_R_P4(1,2) = z(406);
N_R_P4(1,3) = z(129);
N_R_P4(2,1) = z(407);
N_R_P4(2,2) = z(408);
N_R_P4(2,3) = z(131);
N_R_P4(3,1) = z(409);
N_R_P4(3,2) = z(410);
N_R_P4(3,3) = z(134);

N_R_P5(1,1) = z(411);
N_R_P5(1,2) = z(412);
N_R_P5(1,3) = z(159);
N_R_P5(2,1) = z(413);
N_R_P5(2,2) = z(414);
N_R_P5(2,3) = z(161);
N_R_P5(3,1) = z(415);
N_R_P5(3,2) = z(416);
N_R_P5(3,3) = z(164);

N_R_P6(1,1) = z(417);
N_R_P6(1,2) = z(418);
N_R_P6(1,3) = z(189);
N_R_P6(2,1) = z(419);
N_R_P6(2,2) = z(420);
N_R_P6(2,3) = z(191);
N_R_P6(3,1) = z(421);
N_R_P6(3,2) = z(422);
N_R_P6(3,3) = z(194);


%===========================================================
end    % End of function hexRotorRRManip_MGanimationOutputs
%===========================================================
