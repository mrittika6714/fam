function rBDot = hexRotorRRManip_calc_rBDot( qBx, qBy, qBz, vBx, vBy, vBz )
if( nargin ~= 6 ) error( 'hexRotorRRManip_calc_rBDot expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_calc_rBDot.m created Sep 09 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
rBDot = zeros( 3, 1 );
z = zeros( 1, 927 );



%===========================================================================
z(1) = cos(qBy);
z(5) = sin(qBz);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(9) = z(4)*z(8);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(343) = z(7)*z(14) + z(9)*z(13);
z(11) = z(2)*z(8);
z(340) = z(11)*z(14) - z(3)*z(13);
z(338) = z(3)*z(7) + z(9)*z(11);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(10) = z(5)*z(8);
z(337) = z(3)*z(6) - z(9)*z(10);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(336) = z(6)*z(11) + z(7)*z(10);
z(339) = z(13)*z(337) - z(12)*z(338) - z(14)*z(336);
z(346) = (vBy*z(343)-vBx*z(340)-vBz*z(338))/z(339);
xBDt = z(346);
z(344) = z(6)*z(14) + z(9)*z(12);
z(341) = -z(3)*z(12) - z(10)*z(14);
z(347) = (vBy*z(344)-vBx*z(341)-vBz*z(337))/z(339);
yBDt = -z(347);
z(345) = z(6)*z(13) - z(7)*z(12);
z(342) = -z(10)*z(13) - z(11)*z(12);
z(348) = (vBy*z(345)-vBx*z(342)-vBz*z(336))/z(339);
zBDt = z(348);



%===========================================================================
Output = [];

rBDot(1) = xBDt;
rBDot(2) = yBDt;
rBDot(3) = zBDt;


%===================================================
end    % End of function hexRotorRRManip_calc_rBDot
%===================================================
