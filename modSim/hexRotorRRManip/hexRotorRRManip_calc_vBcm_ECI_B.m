function [SolutionToAlgebraicEquations] = hexRotorRRManip_calc_vBcm_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot, xBDot, yBDot, zBDot )
if( nargin ~= 9 ) error( 'hexRotorRRManip_calc_vBcm_ECI_B expects 9 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_calc_vBcm_ECI_B.m created Sep 09 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
z = zeros( 1, 936 );



%===========================================================================
z(1) = cos(qBy);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(5) = sin(qBz);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(14) = z(1)*z(8);
z(336) = z(6)*z(11) + z(7)*z(10);
z(337) = z(3)*z(6) - z(9)*z(10);
z(338) = z(3)*z(7) + z(9)*z(11);
z(339) = z(13)*z(337) - z(12)*z(338) - z(14)*z(336);
z(340) = z(11)*z(14) - z(3)*z(13);
z(341) = -z(3)*z(12) - z(10)*z(14);
z(342) = -z(10)*z(13) - z(11)*z(12);
z(343) = z(7)*z(14) + z(9)*z(13);
z(344) = z(6)*z(14) + z(9)*z(12);
z(345) = z(6)*z(13) - z(7)*z(12);
z(492) = z(340)/z(339);
z(493) = z(341)/z(339);
z(494) = z(342)/z(339);
z(495) = z(343)/z(339);
z(496) = z(344)/z(339);
z(497) = z(345)/z(339);
z(498) = z(338)/z(339);
z(499) = z(337)/z(339);
z(500) = z(336)/z(339);

COEF = zeros( 3, 3 );
COEF(1,1) = -z(492);
COEF(1,2) = z(495);
COEF(1,3) = -z(498);
COEF(2,1) = z(493);
COEF(2,2) = -z(496);
COEF(2,3) = z(499);
COEF(3,1) = -z(494);
COEF(3,2) = z(497);
COEF(3,3) = -z(500);
RHS = zeros( 1, 3 );
RHS(1) = xBDot;
RHS(2) = yBDot;
RHS(3) = zBDot;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
vBx = SolutionToAlgebraicEquations(1);
vBy = SolutionToAlgebraicEquations(2);
vBz = SolutionToAlgebraicEquations(3);



%========================================================
end    % End of function hexRotorRRManip_calc_vBcm_ECI_B
%========================================================
