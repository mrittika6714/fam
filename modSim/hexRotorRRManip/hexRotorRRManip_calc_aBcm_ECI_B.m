function [SolutionToAlgebraicEquations] = hexRotorRRManip_calc_aBcm_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot, xBDot, yBDot, zBDot, vBx, vBy, vBz, wBx, wBy, wBz, xBDDot, yBDDot, zBDDot )
if( nargin ~= 18 ) error( 'hexRotorRRManip_calc_aBcm_ECI_B expects 18 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_calc_aBcm_ECI_B.m created Sep 09 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
z = zeros( 1, 939 );



%===========================================================================
z(1) = cos(qBy);
z(5) = sin(qBz);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(9) = z(4)*z(8);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(343) = z(7)*z(14) + z(9)*z(13);
z(11) = z(2)*z(8);
z(340) = z(11)*z(14) - z(3)*z(13);
z(338) = z(3)*z(7) + z(9)*z(11);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(10) = z(5)*z(8);
z(337) = z(3)*z(6) - z(9)*z(10);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(336) = z(6)*z(11) + z(7)*z(10);
z(339) = z(13)*z(337) - z(12)*z(338) - z(14)*z(336);
z(346) = (vBy*z(343)-vBx*z(340)-vBz*z(338))/z(339);
xBDt = z(346);
z(344) = z(6)*z(14) + z(9)*z(12);
z(341) = -z(3)*z(12) - z(10)*z(14);
z(347) = (vBy*z(344)-vBx*z(341)-vBz*z(337))/z(339);
yBDt = -z(347);
z(345) = z(6)*z(13) - z(7)*z(12);
z(342) = -z(10)*z(13) - z(11)*z(12);
z(348) = (vBy*z(345)-vBx*z(342)-vBz*z(336))/z(339);
zBDt = z(348);
z(232) = z(1)*z(14) + z(4)*z(9);
z(233) = (wBx*z(14)+wBz*z(9))/z(232);
qBxDt = z(233);
z(234) = (wBx*z(4)-wBz*z(1))/z(232);
z(235) = -wBy - z(3)*z(234);
qByDt = -z(235);
qBzDt = -z(234);
z(18) = z(1)*z(8)*qByDt - z(3)*z(4)*qBxDt;
z(21) = -z(1)*z(3)*qBxDt - z(4)*z(8)*qByDt;
z(349) = -z(2)*z(4) - z(1)*z(3)*z(5);
z(350) = -z(1)*z(5) - z(2)*z(3)*z(4);
z(351) = z(4)*z(5)*z(8);
z(352) = z(2)*z(4)*z(8);
z(353) = z(1)*z(2)*z(3) - z(4)*z(5);
z(354) = zBDt*z(18) - yBDt*(z(6)*qBzDt+z(352)*qBxDt+z(353)*qByDt) - xBDt*(z(349)*qByDt+z(350)*qBzDt-z(351)*qBxDt);
z(356) = z(3)*z(5);
z(357) = z(2)*z(3);
z(358) = yBDt*(z(10)*qBzDt+z(357)*qBxDt) + xBDt*(z(11)*qBzDt-z(356)*qBxDt) - z(8)*qBxDt*zBDt;
z(360) = z(1)*z(5)*z(8);
z(361) = z(1)*z(2)*z(8);
z(362) = -xBDt*(z(6)*qByDt+z(353)*qBzDt+z(360)*qBxDt) - yBDt*(z(7)*qByDt+z(12)*qBzDt-z(361)*qBxDt) - zBDt*z(21);
z(492) = z(340)/z(339);
z(493) = z(341)/z(339);
z(494) = z(342)/z(339);
z(495) = z(343)/z(339);
z(496) = z(344)/z(339);
z(497) = z(345)/z(339);
z(498) = z(338)/z(339);
z(499) = z(337)/z(339);
z(500) = z(336)/z(339);
z(937) = xBDDot + (z(338)*z(362)+z(340)*z(354)-z(343)*z(358))/z(339);
z(938) = yBDDot - (z(337)*z(362)+z(341)*z(354)-z(344)*z(358))/z(339);
z(939) = zBDDot + (z(336)*z(362)+z(342)*z(354)-z(345)*z(358))/z(339);

COEF = zeros( 3, 3 );
COEF(1,1) = -z(492);
COEF(1,2) = z(495);
COEF(1,3) = -z(498);
COEF(2,1) = z(493);
COEF(2,2) = -z(496);
COEF(2,3) = z(499);
COEF(3,1) = -z(494);
COEF(3,2) = z(497);
COEF(3,3) = -z(500);
RHS = zeros( 1, 3 );
RHS(1) = z(937);
RHS(2) = z(938);
RHS(3) = z(939);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
vBxDt = SolutionToAlgebraicEquations(1);
vByDt = SolutionToAlgebraicEquations(2);
vBzDt = SolutionToAlgebraicEquations(3);



%========================================================
end    % End of function hexRotorRRManip_calc_aBcm_ECI_B
%========================================================
