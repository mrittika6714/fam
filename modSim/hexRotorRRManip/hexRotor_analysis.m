%% Analysis of the hexRotor

clc;clear;close all
addpath('../plotFuncs')
% saveParams = true;
% addpath('..')
% clear GenerateTrajectoryPoint
% % Define System
% DOF = 3;

%% Unpack params
params = hexRotor_params();

mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPzz = params.IP(3,3);
bThrust = params.bThrust;
bDrag = params.bDrag;
g = params.g;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);
P5Dir = params.propDirections(5);
P6Dir = params.propDirections(6);

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(1);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(1);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(1);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);
qP5x = params.qP5_XYZ(1);
qP5y = params.qP5_XYZ(1);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);
qP6x = params.qP6_XYZ(1);
qP6y = params.qP6_XYZ(1);
qBx = 0;
qBy = 0;
qBz = 0;
qP1z = 0; 
qP2z = 0; 
qP3z = 0; 
qP4z = 0; 
qP5z = 0; 
qP6z = 0; 

lA = params.lA;
lP = params.plotting.lP;
wP = params.plotting.wP;
hP = params.plotting.hP;
armWidth = params.armWidth;
armColor = params.armColor;
vehicleDim = [params.plotting.lBody, params.plotting.wBody, params.plotting.hBody];

%% Visualize Propellor Configuration
xB = 0;
yB = 0;
zB = 0;
[rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N, ...
          N_R_P1, N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6 ] = ...
          hexRotorRates_MGanimationOutputs( xB, yB, zB, qBx, qBy, qBz, ...
          xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, ...
          xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, ...
          qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z,...
          qP6x, qP6y, qP6z );
qP1_ZYX = rotm2eul(N_R_P1, 'ZYX');        
qP2_ZYX = rotm2eul(N_R_P2, 'ZYX');  
qP3_ZYX = rotm2eul(N_R_P3, 'ZYX');  
qP4_ZYX = rotm2eul(N_R_P4, 'ZYX');  
qP5_ZYX = rotm2eul(N_R_P5, 'ZYX');  
qP6_ZYX = rotm2eul(N_R_P6, 'ZYX');         
       
figure 
hold on
plot3([xB(1); rP1_No_N(1) ], [yB(1); rP1_No_N(2)], [zB(1); rP1_No_N(3)], 'linewidth', armWidth, 'color', armColor);
plot3([xB(1); rP2_No_N(1) ], [yB(1); rP2_No_N(2)], [zB(1); rP2_No_N(3)], 'linewidth', armWidth, 'color', armColor);
plot3([xB(1); rP3_No_N(1) ], [yB(1); rP3_No_N(2)], [zB(1); rP3_No_N(3)], 'linewidth', armWidth, 'color', armColor);
plot3([xB(1); rP4_No_N(1) ], [yB(1); rP4_No_N(2)], [zB(1); rP4_No_N(3)], 'linewidth', armWidth, 'color', armColor);
plot3([xB(1); rP5_No_N(1) ], [yB(1); rP5_No_N(2)], [zB(1); rP5_No_N(3)], 'linewidth', armWidth, 'color', armColor);
plot3([xB(1); rP6_No_N(1) ], [yB(1); rP6_No_N(2)], [zB(1); rP6_No_N(3)], 'linewidth', armWidth, 'color', armColor);

[P1Vertices, P1Faces] =  plotCube([lP, wP, hP],rP1_No_N', qP1_ZYX');
[P2Vertices, P2Faces] =  plotCube([lP, wP, hP],rP2_No_N', qP2_ZYX');
[P3Vertices, P3Faces] =  plotCube([lP, wP, hP],rP3_No_N', qP3_ZYX');
[P4Vertices, P4Faces] =  plotCube([lP, wP, hP],rP4_No_N', qP4_ZYX');
[P5Vertices, P5Faces] =  plotCube([lP, wP, hP],rP5_No_N', qP5_ZYX');
[P6Vertices, P6Faces] =  plotCube([lP, wP, hP],rP6_No_N', qP6_ZYX');

patch('Vertices',P1Vertices,'Faces',P1Faces, 'FaceColor', [0, 0, 0]);
patch('Vertices',P2Vertices,'Faces',P2Faces, 'FaceColor', [0, 0, 0]);
patch('Vertices',P3Vertices,'Faces',P3Faces, 'FaceColor', [0, 0, 0]);
patch('Vertices',P4Vertices,'Faces',P4Faces, 'FaceColor', [0, 0, 0]);
patch('Vertices',P5Vertices,'Faces',P5Faces, 'FaceColor', [0, 0, 0]);
patch('Vertices',P6Vertices,'Faces',P6Faces, 'FaceColor', [0, 0, 0]);

view(3)
axis([-1 1 -1 1 -1 1]*.2)
%% Perform analysis of linearized system
syms vBx vBy vBz wBx wBy wBz qP1zDt qP2zDt qP3zDt qP4zDt qP5zDt qP6zDt 

func = hexRotorRates_F( qBx, qBy, qBz, mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir );
%%
uEq = vpasolve ( func  == 0, [vBx; vBy; vBz; wBx; wBy; wBz; qP1zDt; qP2zDt; qP3zDt; qP4zDt; qP5zDt; qP6zDt ] );
% 


uEqVal = double([uEq.qP1zDt; uEq.qP2zDt; uEq.qP3zDt; uEq.qP4zDt; uEq.qP5zDt; uEq.qP6zDt; ]);
save('uEqVal.mat', 'uEqVal')

%%
uEqVal = load('uEqVal.mat');
qP1zDt = uEqVal.uEqVal(1);
qP2zDt = uEqVal.uEqVal(2);
qP3zDt = uEqVal.uEqVal(3);
qP4zDt = uEqVal.uEqVal(4);
qP5zDt = uEqVal.uEqVal(5);
qP6zDt = uEqVal.uEqVal(6);
xBDt = 0;
yBDt = 0;
zBDt = 0;
vBx = 0;
vBy = 0;
vBz = 0;
wBx = 0;
wBy = 0;
wBz = 0;
qBxDt = 0;
qByDt = 0;
qBzDt = 0;


FVec = rates2thrust( qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, bThrust );


F1 = FVec(1);
F2 = FVec(2);
F3 = FVec(3);
F4 = FVec(4);
F5 = FVec(5);
F6 = FVec(6);


A = hexRotorRates_J( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, qBx, qBy, qBz, qBxDt, qByDt, qBzDt, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, vBx, vBy, vBz, wBx, wBy, wBz );

eigs = eig(A)

%%
B = hexRotorRates_B( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, qBx, qBy, qBz, qBxDt, qByDt, qBzDt, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir );
  
C = (ctrb(A,B));
rankSys = rank(C)
[U, sig, V] = svd(C, 'econ');
%%
B_FTMap = hexRotorRates_B_FTMap( P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, ...
  P6Dir, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, ...
  xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, ...
  qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, bThrust, bDrag );
  

rank(B_FTMap)


%%
% Create LQR Gain Matrix
v = 10* ones(12,1);
Q = diag(v);
%Q = eye(6,6)*1;
R = eye(6)*0.01;

Klqr = lqr(A, B, Q, R )
eigs = eig(A-B*Klqr)
save('Klqr.mat', 'Klqr')
save('uEq.mat', 'uEqVal')


%% Helper Functions

function forceTorqueNorm = forceTorqueMap(x)
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
fMat = [ 1; 1; 1];
func = B*fMat;
forceTorqueNorm = -(norm(func));
end
  
function [c, cEq ]= nonLinConFunc(x)
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
S = svd(B);
sDiff = (max(S)- min(S))^2;
c = 0;
cEq = 0;

end


function J = objectiveFunc(x)
weight = [ 1; 1; 1; 1; 1; 1];
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
fMat = [ 1; 1; 1];
func = B*fMat;
func = func.*weight;
%S = svd(B);
%sDiff = (max(S)- min(S))^2;
%J = -(norm(func)) + 0 * sDiff;
J = cond(B);
end


function [F_N, T_N] = calcWrench_N(qP2, qP4, qP5, qBx, F2, F4, F5)
lA = 1;
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
N_R_B = [1, 0, 0; 0, cos(qBx), -sin(qBx); 0, sin(qBx), cos(qBx)];

fMat = [ F2; F4; F5];
wrench_B =B(2:4, :) * fMat;
Fy_B = wrench_B(1);
Fz_B = wrench_B(2);
Tx_B = wrench_B(3);

F_B = [ 0; Fy_B; Fz_B];
T_B = [ Tx_B; 0; 0] ;
F_N = (N_R_B * F_B)';

T_N = (N_R_B * T_B)';
end


function J = objectiveFunc2(x)
lA = 1;
IBxx = 1;
mB = 1;
qBx = 0;
% qBxDDt = 0;
% yDDt = 1;
% zDDt =0;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);

  tStep = .001; 
  tInitial = 0;
  tFinal = 60;
  t = (0:tStep:tFinal)';
  omega = 15*pi/180;
  trajectories.type = 'sine';
  trajectories.omega = omega;
  trajectories.qBx = 0*pi/180;
  trajectories.A = 0;
  clear GenerateTrajectoryPoint;
  [posDes, vDes, aDes]  = GenerateTrajectoryPoint(trajectories, t);
  zDDt = aDes;
  
  trajectories = [];
  trajectories.type = 'minJerk';
trajectories.trajectoryIndex = 3; % fieldname index in which trajectories start
trajectories.numTrajectories = 1;
t1 = tFinal/2;
trajectories.ytrajectory.tVec = [0; t1;  tFinal];
trajectories.ytrajectory.wayPoints = [ 0; 100; 100; ];
trajectories.ytrajectory.velocities = [0; 0; 0; ];
trajectories.ytrajectory.accelerations = [0; 0; 0;];
clear GenerateTrajectoryPoint;
[posDes, vDes, aDes] = GenerateTrajectoryPoint(trajectories, t);
yDDt = aDes;
  
  qBx = 0* ones(size(posDes,1),1);
  qBxDot = zeros(size(posDes,1),1);
  qBxDDt = qBxDot;
  [yDDt, zDDt, qBxDDt];
u = planarQuadRotorThrustFullAct_InverseDynamics2( IBxx, lA, mB, qBx, qP2, qP4, qP5, qBxDDt, yDDt, zDDt );
diff = norm(sqrt((u(:,1) - u(:,2)).^2 + (u(:,1) - u(:,3)).^2 +(u(:,2) - u(:,3)).^2));
J = sum(sqrt(sum(u.*u,2))); %+ diff;

end

function [c, cEq ]= nonLinConFunc2(x)

% qP2 = x(1);
% qP4 = x(2);
% qP5 = x(3);

% if numel(unique(round(x,6))) == 1
%   allEqual = 1;
% else
%   allEqual = 0;
% end
c = 0;
cEq = 0;%allEqual;

end
