function [posDes, vDes, aDes] = hexRotorRRManip_trajGen(trajectories, t)

%% Generate Trajectories

posDes = zeros(length(t), trajectories.numTrajectories);
vDes = zeros(length(t), trajectories.numTrajectories);
aDes = zeros(length(t), trajectories.numTrajectories);

for i = 1:trajectories.numTrajectories
  [posDes(:,i), vDes(:,i), aDes(:,i)] = trajectories.trajectories{i}.getTrajectory(t);
end


end