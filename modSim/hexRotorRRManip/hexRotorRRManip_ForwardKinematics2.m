function [taskPos, taskVel] = hexRotorRRManip_ForwardKinematics2( len1, len2, xB, yB, zB, qBx, qBy, qBz, qL1, qL2, vBx, vBy, vBz, wBx, wBy, wBz, qL1Dt, qL2x, qL2y, qL2z )
if( nargin ~= 20 ) error( 'hexRotorRRManip_ForwardKinematics2 expects 20 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_ForwardKinematics2.m created Sep 25 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
taskPos = zeros( 6, 1 );
taskVel = zeros( 6, 1 );
z = zeros( 1, 927 );

%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
qL2Dt                           =  0.0;                    % UNITS               Constant
%-------------------------------+--------------------------+-------------------+-----------------


%===========================================================================
z(1) = cos(qBy);
z(5) = sin(qBz);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(9) = z(4)*z(8);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(343) = z(7)*z(14) + z(9)*z(13);
z(11) = z(2)*z(8);
z(340) = z(11)*z(14) - z(3)*z(13);
z(338) = z(3)*z(7) + z(9)*z(11);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(10) = z(5)*z(8);
z(337) = z(3)*z(6) - z(9)*z(10);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(336) = z(6)*z(11) + z(7)*z(10);
z(339) = z(13)*z(337) - z(12)*z(338) - z(14)*z(336);
z(346) = (vBy*z(343)-vBx*z(340)-vBz*z(338))/z(339);
xBDt = z(346);
z(344) = z(6)*z(14) + z(9)*z(12);
z(341) = -z(3)*z(12) - z(10)*z(14);
z(347) = (vBy*z(344)-vBx*z(341)-vBz*z(337))/z(339);
yBDt = -z(347);
z(345) = z(6)*z(13) - z(7)*z(12);
z(342) = -z(10)*z(13) - z(11)*z(12);
z(348) = (vBy*z(345)-vBx*z(342)-vBz*z(336))/z(339);
zBDt = z(348);
z(232) = z(1)*z(14) + z(4)*z(9);
z(233) = (wBx*z(14)+wBz*z(9))/z(232);
qBxDt = z(233);
z(234) = (wBx*z(4)-wBz*z(1))/z(232);
z(235) = -wBy - z(3)*z(234);
qByDt = -z(235);
qBzDt = -z(234);
z(203) = cos(qL1);
z(204) = sin(qL1);
z(205) = z(3)*z(203) + z(14)*z(204);
z(206) = z(4)*z(204);
z(207) = z(4)*z(203);
z(208) = z(14)*z(203) - z(3)*z(204);
z(215) = cos(qL2);
z(216) = sin(qL2);
z(217) = z(203)*z(215) - z(204)*z(216);
z(218) = z(205)*z(215) + z(208)*z(216);
z(219) = z(206)*z(215) + z(207)*z(216);
z(220) = -z(203)*z(216) - z(204)*z(215);
z(221) = z(208)*z(215) - z(205)*z(216);
z(222) = z(207)*z(215) - z(206)*z(216);
z(280) = z(12)*z(204) - z(10)*z(203);
z(281) = z(10)*z(204) + z(12)*z(203);
z(282) = z(11)*z(203) + z(13)*z(204);
z(283) = z(13)*z(203) - z(11)*z(204);
z(284) = len1*z(1);
z(285) = len1*z(9);
z(286) = len1*z(204);
z(287) = len1*z(207);
z(288) = len1*z(208);
z(305) = z(215)*z(280) + z(216)*z(281);
z(306) = z(215)*z(281) - z(216)*z(280);
z(307) = z(215)*z(282) + z(216)*z(283);
z(308) = z(215)*z(283) - z(216)*z(282);
z(315) = len2*z(1);
z(316) = len2*z(9);
z(317) = len2*z(220);
z(318) = len2*z(221);
z(319) = len2*z(222);
z(370) = z(286) - z(317);
z(371) = -z(287) - z(319);
z(372) = -z(288) - z(318);



%===========================================================================
Output = [];

taskPos(1) = xB + len1*z(280) + len2*z(305);
taskPos(2) = yB + len1*z(282) + len2*z(307);
taskPos(3) = zB + len1*z(205) + len2*z(218);
taskPos(4) = qL2x;
taskPos(5) = qL2y;
taskPos(6) = qL2z;

taskVel(1) = xBDt + z(6)*(z(370)*qByDt+z(371)*qBxDt+z(372)*qBzDt) + z(281)*(len1*qL1Dt+z(284)*qBxDt-z(285)*qBzDt) + z(306)*(len2*  ...
qL1Dt+len2*qL2Dt+z(315)*qBxDt-z(316)*qBzDt);
taskVel(2) = yBDt + z(7)*(z(370)*qByDt+z(371)*qBxDt+z(372)*qBzDt) + z(283)*(len1*qL1Dt+z(284)*qBxDt-z(285)*qBzDt) + z(308)*(len2*  ...
qL1Dt+len2*qL2Dt+z(315)*qBxDt-z(316)*qBzDt);
taskVel(3) = zBDt + z(208)*(len1*qL1Dt+z(284)*qBxDt-z(285)*qBzDt) + z(221)*(len2*qL1Dt+len2*qL2Dt+z(315)*qBxDt-z(316)*qBzDt)  ...
- z(9)*(z(370)*qByDt+z(371)*qBxDt+z(372)*qBzDt);
taskVel(4) = z(305)*(z(217)*qByDt+z(218)*qBzDt+z(219)*qBxDt) + z(306)*(z(220)*qByDt+z(221)*qBzDt+z(222)*qBxDt) + z(6)*(qL1Dt+  ...
qL2Dt+z(1)*qBxDt-z(9)*qBzDt);
taskVel(5) = z(307)*(z(217)*qByDt+z(218)*qBzDt+z(219)*qBxDt) + z(308)*(z(220)*qByDt+z(221)*qBzDt+z(222)*qBxDt) + z(7)*(qL1Dt+  ...
qL2Dt+z(1)*qBxDt-z(9)*qBzDt);
taskVel(6) = z(218)*(z(217)*qByDt+z(218)*qBzDt+z(219)*qBxDt) + z(221)*(z(220)*qByDt+z(221)*qBzDt+z(222)*qBxDt) - z(9)*(qL1Dt+  ...
qL2Dt+z(1)*qBxDt-z(9)*qBzDt);


%===========================================================
end    % End of function hexRotorRRManip_ForwardKinematics2
%===========================================================
