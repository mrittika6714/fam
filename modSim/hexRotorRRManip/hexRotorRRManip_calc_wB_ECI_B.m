function [SolutionToAlgebraicEquations] = hexRotorRRManip_calc_wB_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot )
if( nargin ~= 6 ) error( 'hexRotorRRManip_calc_wB_ECI_B expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_calc_wB_ECI_B.m created Sep 09 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
z = zeros( 1, 933 );



%===========================================================================
z(1) = cos(qBy);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(14) = z(1)*z(8);
z(232) = z(1)*z(14) + z(4)*z(9);
z(928) = z(14)/z(232);
z(929) = z(9)/z(232);
z(930) = z(3)*z(4)/z(232);
z(931) = z(1)*z(3)/z(232);
z(932) = z(4)/z(232);
z(933) = z(1)/z(232);

COEF = zeros( 3, 3 );
COEF(1,1) = z(928);
COEF(1,3) = z(929);
COEF(2,1) = z(930);
COEF(2,2) = 1;
COEF(2,3) = -z(931);
COEF(3,1) = -z(932);
COEF(3,3) = z(933);
RHS = zeros( 1, 3 );
RHS(1) = qBxDot;
RHS(2) = qByDot;
RHS(3) = qBzDot;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBx = SolutionToAlgebraicEquations(1);
wBy = SolutionToAlgebraicEquations(2);
wBz = SolutionToAlgebraicEquations(3);



%======================================================
end    % End of function hexRotorRRManip_calc_wB_ECI_B
%======================================================
