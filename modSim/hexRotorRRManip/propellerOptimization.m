

clc;clear;close all;
format long;
params = hexRotor_params();
options = optimoptions('fmincon');
options = optimoptions(options,'OptimalityTolerance',1e-8,'StepTolerance',1e-12,'constraintTolerance',1e-12,'MaxIterations',100000,'MaxFunctionEvaluations',1000000,'Display', 'iter');
x0 = ones(12,1) * pi/8; 
A= [];
B = [];
lb = ones(12,1) * 0;
ub = ones(12,1) * pi/2;
objectiveFuncHandle = @(x) objectiveFunc(x,params);

[propAngles fval, exitFlag] = fmincon(objectiveFuncHandle, x0, A, B,[],[],lb, ub, @nonLinConFunc, options)
180/pi*propAngles

%save('propAngles.mat' , 'propAngles')




% wrench = calcWrench(out)


% qBx = -pi : 0.01:pi;

% for i = 1: length(qBx)
% [F_N(i,:) T_N(i,:) ] = calcWrench_N(qP2, qP4, qP5, qBx(i));
% end

% figure
% plot(F_N(:,2), F_N(:,3))
% xlabel('yThrust' )
% ylabel('zThrust')
% figure 
% plot(qBx, T_N(:,1))
% %plot(qBx, wrench_N(:,2))
% %plot(qBx, wrench_N(:,3))


%% Private Functions

function [c, cEq ]= nonLinConFunc(x)
c = 0;
cEq = 0;

end


function J = objectiveFunc(x, params)
% Unpack Decision Variables
qP1x = x(1);
qP1y = x(2);
qP2x = x(3);
qP2y = x(4);
qP3x = x(5);
qP3y = x(6);
qP4x = x(7);
qP4y = x(8);
qP5x = x(9);
qP5y = x(10);
qP6x = x(11);
qP6y = x(12);

% Unpack Params
bThrust = params.bThrust;
bDrag = params.bDrag;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);
P5Dir = params.propDirections(5);
P6Dir = params.propDirections(6);

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);

B_FTMap = hexRotorRates_B_FTMap( P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, xP1, ...
    yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, ...
    zP5, xP6, yP6, zP6, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, ...
    qP5x, qP5y, qP6x, qP6y, bThrust, bDrag );
%fMat = [ 1; 1; 1];
%func = B*fMat;
%func = func.*weight;
%S = svd(B);
%sDiff = (max(S)- min(S))^2;
%J = -(norm(func)) + 0 * sDiff;
J = cond(B_FTMap);
end

function func = calcWrench(x)
weight = [ 1; 1; 1; 1; 1; 1];
lA = 1;
qP2 = x(1);
qP4 = x(2);
qP5 = x(3);
B_FTMap(1,1) = P1Dir*sin(qP1y);
B_FTMap(1,2) = P2Dir*sin(qP2y);
B_FTMap(1,3) = P3Dir*sin(qP3y);
B_FTMap(1,4) = P4Dir*sin(qP4y);
B_FTMap(1,5) = P5Dir*sin(qP5y);
B_FTMap(1,6) = P6Dir*sin(qP6y);
B_FTMap(2,1) = -P1Dir*sin(qP1x)*cos(qP1y);
B_FTMap(2,2) = -P2Dir*sin(qP2x)*cos(qP2y);
B_FTMap(2,3) = -P3Dir*sin(qP3x)*cos(qP3y);
B_FTMap(2,4) = -P4Dir*sin(qP4x)*cos(qP4y);
B_FTMap(2,5) = -P5Dir*sin(qP5x)*cos(qP5y);
B_FTMap(2,6) = -P6Dir*sin(qP6x)*cos(qP6y);
B_FTMap(3,1) = P1Dir*cos(qP1x)*cos(qP1y);
B_FTMap(3,2) = P2Dir*cos(qP2x)*cos(qP2y);
B_FTMap(3,3) = P3Dir*cos(qP3x)*cos(qP3y);
B_FTMap(3,4) = P4Dir*cos(qP4x)*cos(qP4y);
B_FTMap(3,5) = P5Dir*cos(qP5x)*cos(qP5y);
B_FTMap(3,6) = P6Dir*cos(qP6x)*cos(qP6y);
B_FTMap(4,1) = bDrag*sin(qP1y)/bThrust + P1Dir*yP1*cos(qP1x)*cos(qP1y)+ P1Dir*zP1*sin(qP1x)*cos(qP1y);
B_FTMap(4,2) = bDrag*sin(qP2y)/bThrust + P2Dir*yP2*cos(qP2x)*cos(qP2y)+ P2Dir*zP2*sin(qP2x)*cos(qP2y);
B_FTMap(4,3) = bDrag*sin(qP3y)/bThrust + P3Dir*yP3*cos(qP3x)*cos(qP3y)+ P3Dir*zP3*sin(qP3x)*cos(qP3y);
B_FTMap(4,4) = bDrag*sin(qP4y)/bThrust + P4Dir*yP4*cos(qP4x)*cos(qP4y)+ P4Dir*zP4*sin(qP4x)*cos(qP4y);
B_FTMap(4,5) = bDrag*sin(qP5y)/bThrust + P5Dir*yP5*cos(qP5x)*cos(qP5y)+ P5Dir*zP5*sin(qP5x)*cos(qP5y);
B_FTMap(4,6) = bDrag*sin(qP6y)/bThrust + P6Dir*yP6*cos(qP6x)*cos(qP6y)+ P6Dir*zP6*sin(qP6x)*cos(qP6y);
B_FTMap(5,1) = -bDrag*sin(qP1x)*cos(qP1y)/bThrust - P1Dir*(cos(qP1x)*sin(qP1z)+sin(qP1x)*sin(qP1y)*cos(qP1z))*(xP1*cos(qP1y)*sin(qP1z)-zP1*(sin(qP1x)*cos(qP1z)+sin(qP1y)*cos(qP1x)*sin(qP1z))) - P1Dir*(cos(qP1x)*cos(qP1z)-sin(qP1x)*sin(qP1y)*sin(qP1z))*(xP1*cos(qP1y)*cos(qP1z)+zP1*(sin(qP1x)*sin(qP1z)-sin(qP1y)*cos(qP1x)*cos(qP1z)));
B_FTMap(5,2) = -bDrag*sin(qP2x)*cos(qP2y)/bThrust - P2Dir*(cos(qP2x)*sin(qP2z)+sin(qP2x)*sin(qP2y)*cos(qP2z))*(xP2*cos(qP2y)*sin(qP2z)-zP2*(sin(qP2x)*cos(qP2z)+sin(qP2y)*cos(qP2x)*sin(qP2z))) - P2Dir*(cos(qP2x)*cos(qP2z)-sin(qP2x)*sin(qP2y)*sin(qP2z))*(xP2*cos(qP2y)*cos(qP2z)+zP2*(sin(qP2x)*sin(qP2z)-sin(qP2y)*cos(qP2x)*cos(qP2z)));
B_FTMap(5,3) = -bDrag*sin(qP3x)*cos(qP3y)/bThrust - P3Dir*(cos(qP3x)*sin(qP3z)+sin(qP3x)*sin(qP3y)*cos(qP3z))*(xP3*cos(qP3y)*sin(qP3z)-zP3*(sin(qP3x)*cos(qP3z)+sin(qP3y)*cos(qP3x)*sin(qP3z))) - P3Dir*(cos(qP3x)*cos(qP3z)-sin(qP3x)*sin(qP3y)*sin(qP3z))*(xP3*cos(qP3y)*cos(qP3z)+zP3*(sin(qP3x)*sin(qP3z)-sin(qP3y)*cos(qP3x)*cos(qP3z)));
B_FTMap(5,4) = -bDrag*sin(qP4x)*cos(qP4y)/bThrust - P4Dir*(cos(qP4x)*sin(qP4z)+sin(qP4x)*sin(qP4y)*cos(qP4z))*(xP4*cos(qP4y)*sin(qP4z)-zP4*(sin(qP4x)*cos(qP4z)+sin(qP4y)*cos(qP4x)*sin(qP4z))) - P4Dir*(cos(qP4x)*cos(qP4z)-sin(qP4x)*sin(qP4y)*sin(qP4z))*(xP4*cos(qP4y)*cos(qP4z)+zP4*(sin(qP4x)*sin(qP4z)-sin(qP4y)*cos(qP4x)*cos(qP4z)));
B_FTMap(5,5) = -bDrag*sin(qP5x)*cos(qP5y)/bThrust - P5Dir*(cos(qP5x)*sin(qP5z)+sin(qP5x)*sin(qP5y)*cos(qP5z))*(xP5*cos(qP5y)*sin(qP5z)-zP5*(sin(qP5x)*cos(qP5z)+sin(qP5y)*cos(qP5x)*sin(qP5z))) - P5Dir*(cos(qP5x)*cos(qP5z)-sin(qP5x)*sin(qP5y)*sin(qP5z))*(xP5*cos(qP5y)*cos(qP5z)+zP5*(sin(qP5x)*sin(qP5z)-sin(qP5y)*cos(qP5x)*cos(qP5z)));
B_FTMap(5,6) = -bDrag*sin(qP6x)*cos(qP6y)/bThrust - P6Dir*(cos(qP6x)*sin(qP6z)+sin(qP6x)*sin(qP6y)*cos(qP6z))*(xP6*cos(qP6y)*sin(qP6z)-zP6*(sin(qP6x)*cos(qP6z)+sin(qP6y)*cos(qP6x)*sin(qP6z))) - P6Dir*(cos(qP6x)*cos(qP6z)-sin(qP6x)*sin(qP6y)*sin(qP6z))*(xP6*cos(qP6y)*cos(qP6z)+zP6*(sin(qP6x)*sin(qP6z)-sin(qP6y)*cos(qP6x)*cos(qP6z)));
B_FTMap(6,1) = bDrag*cos(qP1x)*cos(qP1y)/bThrust - P1Dir*(sin(qP1x)*cos(qP1z)+sin(qP1y)*cos(qP1x)*sin(qP1z))*(xP1*cos(qP1y)*cos(qP1z)+yP1*(cos(qP1x)*sin(qP1z)+sin(qP1x)*sin(qP1y)*cos(qP1z))) - P1Dir*(sin(qP1x)*sin(qP1z)-sin(qP1y)*cos(qP1x)*cos(qP1z))*(xP1*cos(qP1y)*sin(qP1z)-yP1*(cos(qP1x)*cos(qP1z)-sin(qP1x)*sin(qP1y)*sin(qP1z)));
B_FTMap(6,2) = bDrag*cos(qP2x)*cos(qP2y)/bThrust - P2Dir*(sin(qP2x)*cos(qP2z)+sin(qP2y)*cos(qP2x)*sin(qP2z))*(xP2*cos(qP2y)*cos(qP2z)+yP2*(cos(qP2x)*sin(qP2z)+sin(qP2x)*sin(qP2y)*cos(qP2z))) - P2Dir*(sin(qP2x)*sin(qP2z)-sin(qP2y)*cos(qP2x)*cos(qP2z))*(xP2*cos(qP2y)*sin(qP2z)-yP2*(cos(qP2x)*cos(qP2z)-sin(qP2x)*sin(qP2y)*sin(qP2z)));
B_FTMap(6,3) = bDrag*cos(qP3x)*cos(qP3y)/bThrust - P3Dir*(sin(qP3x)*cos(qP3z)+sin(qP3y)*cos(qP3x)*sin(qP3z))*(xP3*cos(qP3y)*cos(qP3z)+yP3*(cos(qP3x)*sin(qP3z)+sin(qP3x)*sin(qP3y)*cos(qP3z))) - P3Dir*(sin(qP3x)*sin(qP3z)-sin(qP3y)*cos(qP3x)*cos(qP3z))*(xP3*cos(qP3y)*sin(qP3z)-yP3*(cos(qP3x)*cos(qP3z)-sin(qP3x)*sin(qP3y)*sin(qP3z)));
B_FTMap(6,4) = bDrag*cos(qP4x)*cos(qP4y)/bThrust - P4Dir*(sin(qP4x)*cos(qP4z)+sin(qP4y)*cos(qP4x)*sin(qP4z))*(xP4*cos(qP4y)*cos(qP4z)+yP4*(cos(qP4x)*sin(qP4z)+sin(qP4x)*sin(qP4y)*cos(qP4z))) - P4Dir*(sin(qP4x)*sin(qP4z)-sin(qP4y)*cos(qP4x)*cos(qP4z))*(xP4*cos(qP4y)*sin(qP4z)-yP4*(cos(qP4x)*cos(qP4z)-sin(qP4x)*sin(qP4y)*sin(qP4z)));
B_FTMap(6,5) = bDrag*cos(qP5x)*cos(qP5y)/bThrust - P5Dir*(sin(qP5x)*cos(qP5z)+sin(qP5y)*cos(qP5x)*sin(qP5z))*(xP5*cos(qP5y)*cos(qP5z)+yP5*(cos(qP5x)*sin(qP5z)+sin(qP5x)*sin(qP5y)*cos(qP5z))) - P5Dir*(sin(qP5x)*sin(qP5z)-sin(qP5y)*cos(qP5x)*cos(qP5z))*(xP5*cos(qP5y)*sin(qP5z)-yP5*(cos(qP5x)*cos(qP5z)-sin(qP5x)*sin(qP5y)*sin(qP5z)));
B_FTMap(6,6) = bDrag*cos(qP6x)*cos(qP6y)/bThrust - P6Dir*(sin(qP6x)*cos(qP6z)+sin(qP6y)*cos(qP6x)*sin(qP6z))*(xP6*cos(qP6y)*cos(qP6z)+yP6*(cos(qP6x)*sin(qP6z)+sin(qP6x)*sin(qP6y)*cos(qP6z))) - P6Dir*(sin(qP6x)*sin(qP6z)-sin(qP6y)*cos(qP6x)*cos(qP6z))*(xP6*cos(qP6y)*sin(qP6z)-yP6*(cos(qP6x)*cos(qP6z)-sin(qP6x)*sin(qP6y)*sin(qP6z)));
fMat = [ 1; 1; 1; 1; 1; 1;];
func = B_FTMap*fMat;

end


function [F_N, T_N] = calcWrench_N(qP2, qP4, qP5, qBx)
lA = 1;
B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(2,1) = -sin(qP2);
B(2,2) = -sin(qP4);
B(2,3) = -sin(qP5);
B(3,1) = cos(qP2);
B(3,2) = cos(qP4);
B(3,3) = cos(qP5);
B(4,1) = lA*cos(qP2);
B(4,2) = -lA*cos(qP4);
B(4,3) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
N_R_B = [1, 0, 0; 0, cos(qBx), -sin(qBx); 0, sin(qBx), cos(qBx)];

fMat = [ .1; 0.1; 1];
wrench_B =B(2:4, :) * fMat
Fy_B = wrench_B(1);
Fz_B = wrench_B(2);
Tx_B = wrench_B(3);

F_B = [ 0; Fy_B; Fz_B]
T_B = [ Tx_B; 0; 0] 
F_N = (N_R_B * F_B)'

T_N = (N_R_B * T_B)'
end
