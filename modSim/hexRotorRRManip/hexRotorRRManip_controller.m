function control = hexRotorRRManip_controller(t, states, trajectories, trajectoryFunc, params)

persistent errorSum
  if isempty(errorSum)
        errorSum = zeros(8,1);
  end
    
% Unpack Params
mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPzz = params.IP(3,3);
bThrust = params.bThrust;
bDrag = params.bDrag;
g = params.g;

IL1xx = params.IL1(1,1);
IL1yy = params.IL1(2,2);
IL1zz  = params.IL1(3,3);
IL2xx = params.IL2(1,1);
IL2yy = params.IL2(2,2);
IL2zz = params.IL2(3,3);
len1 = params.len1;
len2 = params.len2;
mL1 = params.m1;
mL2 = params.m2;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);
P5Dir = params.propDirections(5);
P6Dir = params.propDirections(6);

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(1);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(1);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(1);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);
qP5x = params.qP5_XYZ(1);
qP5y = params.qP5_XYZ(1);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);
qP6x = params.qP6_XYZ(1);
qP6y = params.qP6_XYZ(1);

% Unpack states
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
qL1 = states(7);
qL2 = states(8);
vBx = states(9);
vBy = states(10);
vBz = states(11);
wBx = states(12);
wBy = states(13);
wBz = states(14);
qL1Dt = states(15);
qL2Dt  = states(16);
qP1z = states(17);
qP2z = states(18);
qP3z = states(19);
qP4z = states(20);
qP5z = states(21);
qP6z = states(22);

rBDot = hexRotorRRManip_calc_rBDot( qBx, qBy, qBz, vBx, vBy, vBz );
xBDot = rBDot(1);
yBDot = rBDot(2);
zBDot = rBDot(3);

qBDot  = hexRotorRRManip_calc_qBDot( qBx, qBy, qBz, wBx, wBy, wBz );
qBxDot = qBDot(1);    
qByDot = qBDot(2);  
qBzDot = qBDot(3);  

controllerType = params.controller.type;
controllerSpace = params.controller.space;
numTrajectories =  params.trajectory.numTrajectories;
% Unpack Desired Trajectory 
if (strcmp(controllerSpace, 'Task'))
    [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
    xDes = posDes(1);
    xDotDes = vDes(1);
    xDDotDes = aDes(1);
    yDes = posDes(2);
    yDotDes = vDes(2);
    yDDotDes = aDes(2);
    zDes =  posDes(3);
    zDotDes= vDes(3); 
    zDDotDes = aDes(3);
    
    EE_R_N = hexRotorRRManip_calc_EE_R_N( qBx, qBy, qBz, qL1, qL2);
    
    qEE_XYZ =  rotm2eul(EE_R_N' ,'XYZ');
    qL2x = qEE_XYZ(1);
    qL2y = qEE_XYZ(2);
    qL2z = qEE_XYZ(3);
    [taskPos, taskVel] = hexRotorRRManip_ForwardKinematics2( len1, len2, xB, yB, zB, qBx, qBy, qBz, qL1, qL2, vBx, vBy, vBz, wBx, wBy, wBz, qL1Dt, qL2x, qL2y, qL2z );
    error = posDes(1:numTrajectories)' - taskPos(1:numTrajectories);
    errorDot = vDes(1:numTrajectories)' - taskVel(1:numTrajectories);
else
    [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);

    % Calculate errors
    error = posDes' - states(1:8);

    jointRates = [rBDot; qBDot; qL1Dt; qL2Dt];
    errorDot = vDes' - jointRates;
    errorSum = errorSum + error;
end


if strcmp(controllerType, 'none')
   control = zeros(8, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;
   
elseif strcmp(controllerType, 'FF_PID_Joint')

    kp = params.controller.kp;
    kd = params.controller.kd;
    ki = params.controller.ki;
    a_q_Control = aDes' + kd.*errorDot + kp .* error; %+ ki * errorSum; % control signal for acceleration in joint space
    xBDDot = a_q_Control(1);
    yBDDot = a_q_Control(2);
    zBDDot = a_q_Control(3);
    qBxDDot = a_q_Control(4);
    qByDDot = a_q_Control(5);
    qBzDDot = a_q_Control(6);
    qL1DDot = a_q_Control(7);
    qL2DDot = a_q_Control(8);
    aBcm_ECI_B =  hexRotorRRManip_calc_aBcm_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot, xBDot, yBDot, zBDot, vBx, vBy, vBz, wBx, wBy, wBz, xBDDot, yBDDot, zBDDot );
    alphaB_ECI_B = hexRotorRRManip_calc_alphaB_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz );
    a_q_Control = [aBcm_ECI_B; alphaB_ECI_B; qL1DDot; qL2DDot  ];
    [NMat, MMat, GMat, VMat, ~, ~] = hexRotorRRManip_dynamicMatrices( vBx, vBy, vBz, qBx, qBy, qBz, wBx, wBy, wBz, mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, IL1xx, IL1yy, IL1zz, IL2xx, IL2yy, IL2zz, len1, len2, mL1, mL2, qL1, qL2, qL1Dt, qL2Dt );
    control = pinv(NMat) * (MMat * a_q_Control  +VMat + GMat);

elseif strcmp(controllerType, 'FF_PID_Joint2')
    kp = params.controller.kp;
    kd = params.controller.kd;
    ki = params.controller.ki;
    a_q_Control = aDes' + kd .*errorDot + kp .* error; %+ ki * errorSum; % control signal for acceleration in joint space
    xBDDot = a_q_Control(1);
    yBDDot = a_q_Control(2);
    zBDDot = a_q_Control(3);
    qBxDDot = a_q_Control(4);
    qByDDot = a_q_Control(5);
    qBzDDot = a_q_Control(6);
    qL1DDt = a_q_Control(7);
    qL2DDt = a_q_Control(8);
    aBcm_ECI_B =  hexRotorRRManip_calc_aBcm_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot, xBDot, yBDot, zBDot, vBx, vBy, vBz, wBx, wBy, wBz, xBDDot, yBDDot, zBDDot );
    alphaB_ECI_B = hexRotorRRManip_calc_alphaB_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz );
    vBxDt = aBcm_ECI_B(1);
    vByDt = aBcm_ECI_B(2);
    vBzDt = aBcm_ECI_B(3);
    wBxDt = alphaB_ECI_B(1);
    wByDt = alphaB_ECI_B(2);
    wBzDt = alphaB_ECI_B(3);
    
    control = hexRotorRRManip_MGInverseDynamics( bThrust, bDrag, g, ...
      P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, IL1xx, IL1yy, IL1zz, ...
      IL2xx, IL2yy, IL2zz, len1, len2, mL1, mL2, mB, IBxx, IByy, IBzz, ...
      IBxy, IByz, IBxz, IPzz, mP, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, ...
      qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, ...
      qP6y, qP6z, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, ...
      yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qBx, qBy, qBz, qL1, qL2, ...
      vBx, vBy, vBz, wBx, wBy, wBz, qL1Dt, qL2Dt, vBxDt, vByDt, vBzDt, ...
      wBxDt, wByDt, wBzDt, qL1DDt, qL2DDt );

 elseif strcmp(controllerType, 'FF_PID_Task')
     numTrajectories =  params.trajectory.numTrajectories;
    kp = params.controller.kp;
    kd = params.controller.kd;
    [NMat, MMat, GMat, CMat, J, JDot] = hexRotorRRManip_dynamicMatrices2( vBx, vBy, vBz, qBx, qBy, qBz, wBx, wBy, wBz, mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, IL1xx, IL1yy, IL1zz, IL2xx, IL2yy, IL2zz, len1, len2, mL1, mL2, qL1, qL2, qL1Dt, qL2Dt );
    %     https://www.mecharithm.com/computed-torque-control-of-a-3r-robot-arm-playing-ping-pong/
    % https://menloservice.sandia.gov/https://studywolf.wordpress.com/2013/09/17/robot-control-4-operation-space-control/
     J = J(1:numTrajectories,:);
     JDot = JDot(1:numTrajectories,:);
     kp = kp(1:numTrajectories);
     kd = kd(1:numTrajectories);
     aDes = aDes(1:numTrajectories);
     aCon =  aDes' + kd .* errorDot + kp .* error;
     aCon = aCon(1:numTrajectories);
     vDes = vDes(1:numTrajectories);
     %M_tilda = pinv(J*pinv(MMat)*J');
     %C_tilda = pinv(J*pinv(MMat)*J')* (J*pinv(MMat)*CMat - JDot*qDot); 
     %G_tilda = pinv(J*pinv(MMat)*J')* J*pinv(MMat) *GMat;
     %F = M_tilda*aCon + C_tilda +G_tilda;
     
     control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes') + CMat + GMat);
     
elseif strcmp(controllerType, 'FF_PID_Null_Task')
     % 3.2.3 of Nakanishi, Cory, Mistry, Peters, and Schaal / Operational Space Control: A Theoretical and Empirical Comparison
    numTrajectories =  params.trajectory.numTrajectories;
     kp = params.controller.kp;
    kd = params.controller.kd;
     kp = kp(1:numTrajectories);
     kd = kd(1:numTrajectories);
    [NMat, MMat, GMat, CMat, J, JDot] = hexRotorRRManip_dynamicMatrices( vBx, vBy, vBz, qBx, qBy, qBz, wBx, wBy, wBz, mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, IL1xx, IL1yy, IL1zz, IL2xx, IL2yy, IL2zz, len1, len2, mL1, mL2, qL1, qL2, qL1Dt, qL2Dt );
    %     https://www.mecharithm.com/computed-torque-control-of-a-3r-robot-arm-playing-ping-pong/
    % https://menloservice.sandia.gov/https://studywolf.wordpress.com/2013/09/17/robot-control-4-operation-space-control/
     aDes = aDes(1:numTrajectories);
     aCon =  aDes' + kd .* errorDot + kp .* error;
     aCon = aCon(1:numTrajectories);
     vDes = vDes(1:numTrajectories);
     qPos = [ 0; 0; 0; 0; 0; 0; pi/2; 0];
     J = J(1:numTrajectories,:);
     JDot = JDot(1:3,:);
     nullWeight = [0; 0; 0; 0.00; 0.00; 0.00; 0.000005; 0.000005];
     %control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes(1:2)') + CMat + GMat);
    control = pinv(NMat)*(MMat * pinv(J) *(aCon - JDot*pinv(J)*vDes') + CMat + GMat) + (eye(8,8) - pinv(J)*J) * (nullWeight.*(qPos-[xB; yB; zB; qBx; qBy; qBz; qL1; qL2]));

    
elseif strcmp(controllerType, 'LQR')
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
  xDes = posDes(1);
  xDotDes = vDes(1);
  xDDotDes = aDes(1);
  yDes = posDes(2);
  yDotDes = vDes(2);
  yDDotDes = aDes(2);
  zDes = posDes(3);
  zDotDes = vDes(3);
  zDDotDes = aDes(3);
  qBxDes =  posDes(4);
  qBxDotDes= vDes(4); 
  qBxDotDotDes= aDes(4);
  qByDes =  posDes(5);
  qByDotDes= vDes(5); 
  qByDotDotDes= aDes(5);
  qBzDes =  posDes(6);
  qBzDotDes= vDes(6); 
  qBzDotDotDes= aDes(6);
  
  setPoint = [xDes; yDes; zDes; qBxDes; qByDes; qBzDes; ...
              xDotDes; yDotDes; zDotDes; qBxDotDes; qByDotDes; qBzDotDes; ...
              0; 0; 0; 0; 0; 0; ];
  Klqr = params.controller.Klqr;
  control = -[Klqr, zeros(6,6) ] *(states-setPoint) + params.controller.uEqVal;

     

else
  error('Controller Type not handled');
end 

end