function trajectoryParams = hexRotorRRManip_trajectoryParams(controllerType)

tFinal = 240;
numPoints = 11;
tVec = linspace(0, tFinal, numPoints);
xB0 = 0; 
yB0 = 0; 
zB0 = 0; 
qBx0 = 0; 
qBy0 = 0; 
qBz0 = 0; 
qL10 = 0*(pi/180);
qL20 = 0;
xBDt0 = 0; 
yBDt0 = 0; 
zBDt0 = 0; 
wBx0 = 0; 
wBy0 = 0; 
wBz0 = 0; 
qL1Dt0 = 0;
qL2Dt0 = 0;
qP1z0 = 0; 
qP2z0 = 0; 
qP3z0 = 0; 
qP4z0 = 0; 
qP5z0 = 0; 
qP6z0 = 0;  
bodyStates0 = [xB0; yB0; zB0; qBx0; qBy0; qBz0; qL10; qL20; xBDt0; yBDt0; zBDt0; wBx0; wBy0; wBz0; qL1Dt0; qL2Dt0 ];
propStates0 = [qP1z0; qP2z0; qP3z0; qP4z0; qP5z0; qP6z0 ];
states0 = [bodyStates0; propStates0]; %load('ICs.mat')
%initialOffset = [1; 1; 1; pi/8; -pi/8; pi/3; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0];
initialOffset = zeros(16,1);

if strcmp(controllerType, 'Joint')
% x trajectory 
xTrajectory = trajectory;
xTrajectory.type = 'minJerk';
xTrajectory.wayPoints = zeros(4,1);%[ states0(1)+initialOffset(1); 1; 1; 1];
xTrajectory.velocities = [states0(7)+initialOffset(7); 0; 0; 0  ];
xTrajectory.tVec = tVec;
xTrajectory.accelerations = [0; 0; 0; 0];

% y trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'minJerk';
yTrajectory.wayPoints =  [states0(2)+initialOffset(2)+0.372; 3; 3; 3]; %0.372*ones(4,1);%[
yTrajectory.velocities = [states0(8)+initialOffset(8); 0; 0; 0  ];
yTrajectory.tVec = tVec;
yTrajectory.accelerations = [0; 0; 0; 0];

% z trajectory 
zTrajectory = trajectory;
zTrajectory.type = 'minJerk';
zTrajectory.wayPoints = zeros(4,1);%[ states0(3)+initialOffset(3); 5; 2; 2];
zTrajectory.velocities = [states0(9)+initialOffset(9); 0; 0; 0  ];
zTrajectory.tVec = tVec;
zTrajectory.accelerations = [0; 0; 0; 0];

% qBx trajectory 
qBxTrajectory = trajectory;
qBxTrajectory.type = 'minJerk';
qBxTrajectory.wayPoints = zeros(4,1);%[ states0(4)+initialOffset(4); pi/8; pi/8; pi/8];
qBxTrajectory.velocities = [states0(10)+initialOffset(10); 0; 0; 0  ];
qBxTrajectory.tVec = tVec;
qBxTrajectory.accelerations = [0; 0; 0; 0];

% qBy trajectory 
qByTrajectory = trajectory;
qByTrajectory.type = 'minJerk';
qByTrajectory.wayPoints = zeros(4,1);%[ states0(5)+initialOffset(5); 0; 0; 0];
qByTrajectory.velocities = [states0(11)+initialOffset(11); 0; 0; 0  ];
qByTrajectory.tVec = tVec;
qByTrajectory.accelerations = [0; 0; 0; 0];

% qBz trajectory 
qBzTrajectory = trajectory;
qBzTrajectory.type = 'minJerk';
qBzTrajectory.wayPoints = zeros(4,1);%[ states0(6)+initialOffset(6); 0; 0; 0];
qBzTrajectory.velocities = [states0(12)+initialOffset(12); 0; 0; 0  ];
qBzTrajectory.tVec = tVec;
qBzTrajectory.accelerations = [0; 0; 0; 0];

% qL1 trajectory 
qEEyTrajectory = trajectory;
qEEyTrajectory.type = 'minJerk';
qEEyTrajectory.wayPoints = zeros(4,1);%[ states0(7)+initialOffset(7); pi/2; pi/2; pi/2];
qEEyTrajectory.velocities = [states0(13)+initialOffset(13); 0; 0; 0;  ];
qEEyTrajectory.tVec = tVec;
qEEyTrajectory.accelerations = [0; 0; 0; 0];

% qL2 trajectory 
qL2Trajectory = trajectory;
qL2Trajectory.type = 'minJerk';
qL2Trajectory.wayPoints = zeros(4,1);%[ states0(8)+initialOffset(8); -pi/2; -pi/2; -pi/2];
qL2Trajectory.velocities = [states0(14)+initialOffset(14); 0; 0; 0;  ];
qL2Trajectory.tVec = tVec;
qL2Trajectory.accelerations = [0; 0; 0; 0];


trajectoryParams.numTrajectories = 8;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = xTrajectory;
trajectoryParams.trajectories{2} = yTrajectory;
trajectoryParams.trajectories{3} = zTrajectory;
trajectoryParams.trajectories{4} = qBxTrajectory;
trajectoryParams.trajectories{5} = qByTrajectory;
trajectoryParams.trajectories{6} = qBzTrajectory;
trajectoryParams.trajectories{7} = qEEyTrajectory;
trajectoryParams.trajectories{8} = qL2Trajectory;


elseif strcmp(controllerType, 'Task')
 
 zeroVec = zeros(numPoints,1);
% xEE trajectory 
xTrajectory = trajectory;
xTrajectory.type = 'minJerk';
xTrajectory.wayPoints = [ states0(1)+initialOffset(1); 1; -1; -1; 1; 1; 1; -1; -1; 1; 0 ];
xTrajectory.velocities = zeroVec; 
xTrajectory.tVec = tVec;
xTrajectory.accelerations = zeroVec;

% yEE trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'minJerk';
yTrajectory.wayPoints = [ states0(2)+initialOffset(2)+0.372; 1; 1; -1; -1; 1; 1; 1; -1; -1; 0]; %0.372*ones(4,1);%
yTrajectory.velocities = zeroVec;
yTrajectory.tVec = tVec;
yTrajectory.accelerations = zeroVec;

% zEE trajectory 
zTrajectory = trajectory;
zTrajectory.type = 'minJerk';
zTrajectory.wayPoints = [ states0(3)+initialOffset(3); 1; 1; 1; 1; 1; -1; -1; -1; -1; 0];
zTrajectory.velocities = zeroVec;
zTrajectory.tVec = tVec;
zTrajectory.accelerations = zeroVec;

% qEEx trajectory 
qEExTrajectory = trajectory;
qEExTrajectory.type = 'minJerk';
qEExTrajectory.wayPoints = pi/4*ones(numPoints,1);%[ states0(7)+initialOffset(7); pi/2; pi/2; pi/2];
qEExTrajectory.velocities = zeroVec;
qEExTrajectory.tVec = tVec;
qEExTrajectory.accelerations = zeroVec;

% qEEy trajectory 
qEEyTrajectory = trajectory;
qEEyTrajectory.type = 'minJerk';
qEEyTrajectory.wayPoints = zeros(numPoints,1);%[ states0(7)+initialOffset(7); pi/2; pi/2; pi/2];
qEEyTrajectory.velocities = zeroVec;
qEEyTrajectory.tVec = tVec;
qEEyTrajectory.accelerations = zeroVec;

% qEEz trajectory 
qEEzTrajectory = trajectory;
qEEzTrajectory.type = 'minJerk';
qEEzTrajectory.wayPoints = zeros(numPoints,1);%[ states0(7)+initialOffset(7); pi/2; pi/2; pi/2];
qEEzTrajectory.velocities = zeroVec;
qEEzTrajectory.tVec = tVec;
qEEzTrajectory.accelerations = zeroVec;

trajectoryParams.numTrajectories = 3;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = xTrajectory;
trajectoryParams.trajectories{2} = yTrajectory;
trajectoryParams.trajectories{3} = zTrajectory;
trajectoryParams.trajectories{4} = qEExTrajectory;
trajectoryParams.trajectories{5} = qEEyTrajectory;
trajectoryParams.trajectories{6} = qEEzTrajectory;



else
    error('Controller space not handeled')
end
end