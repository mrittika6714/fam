hexRotorRRManipKane2(params)
function [t,VAR,Output] = hexRotorRRManipKane2(params)
%===========================================================================
% File: hexRotorRRManipKane.m created Aug 19 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
eventDetectedByIntegratorTerminate1OrContinue0 = [];
F1=0; F2=0; F3=0; F4=0; F5=0; F6=0; T1=0; T2=0; T3=0; T4=0; T5=0; T6=0; TL1=0; TL2=0; qBxDt=0; qByDt=0; qBzDt=0; qP1zDt=0; qP2zDt=0;
qP3zDt=0; qP4zDt=0; qP5zDt=0; qP6zDt=0; vBxDt=0; vByDt=0; vBzDt=0; wBxDt=0; wByDt=0; wBzDt=0; xBDt=0; yBDt=0; zBDt=0; qL1DDt=0; qL2DDt=0;
xBDDt=0; yBDDt=0; zBDDt=0;
z = zeros( 1, 972 );

%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
bDrag                           =  0.0;                    % UNITS               Constant
bThrust                         =  0.0;                    % UNITS               Constant
g                               =  0.0;                    % UNITS               Constant
IBxx                            =  0.0117;                 % kg*m^2              Constant
IBxy                            =  0;                      % UNITS               Constant
IBxz                            =  0;                      % UNITS               Constant
IByy                            =  0.0117;                 % kg*m^2              Constant
IByz                            =  0;                      % UNITS               Constant
IBzz                            =  0.00234;                % kg*m^2              Constant
IL1xx                           =  0.0;                    % UNITS               Constant
IL1yy                           =  0.0;                    % UNITS               Constant
IL1zz                           =  0.0;                    % UNITS               Constant
IL2xx                           =  0.0;                    % UNITS               Constant
IL2yy                           =  0.0;                    % UNITS               Constant
IL2zz                           =  0.0;                    % UNITS               Constant
IPzz                            =  0.0;                    % UNITS               Constant
len1                            =  0.0;                    % UNITS               Constant
len2                            =  0.0;                    % UNITS               Constant
mB                              =  0.478;                  % kg                  Constant
mL1                             =  0.0;                    % UNITS               Constant
mL2                             =  0.0;                    % UNITS               Constant
mP                              =  0.0;                    % UNITS               Constant
P1Dir                           =  0.0;                    % UNITS               Constant
P2Dir                           =  0.0;                    % UNITS               Constant
P3Dir                           =  0.0;                    % UNITS               Constant
P4Dir                           =  0.0;                    % UNITS               Constant
P5Dir                           =  0.0;                    % UNITS               Constant
P6Dir                           =  0.0;                    % UNITS               Constant
qP1x                            =  0.0;                    % UNITS               Constant
qP1y                            =  0.0;                    % UNITS               Constant
qP2x                            =  0.0;                    % UNITS               Constant
qP2y                            =  0.0;                    % UNITS               Constant
qP3x                            =  0.0;                    % UNITS               Constant
qP3y                            =  0.0;                    % UNITS               Constant
qP4x                            =  0.0;                    % UNITS               Constant
qP4y                            =  0.0;                    % UNITS               Constant
qP5x                            =  0.0;                    % UNITS               Constant
qP5y                            =  0.0;                    % UNITS               Constant
qP6x                            =  0.0;                    % UNITS               Constant
qP6y                            =  0.0;                    % UNITS               Constant
xP1                             =  0.0;                    % UNITS               Constant
xP2                             =  0.0;                    % UNITS               Constant
xP3                             =  0.0;                    % UNITS               Constant
xP4                             =  0.0;                    % UNITS               Constant
xP5                             =  0.0;                    % UNITS               Constant
xP6                             =  0.0;                    % UNITS               Constant
yP1                             =  0.0;                    % UNITS               Constant
yP2                             =  0.0;                    % UNITS               Constant
yP3                             =  0.0;                    % UNITS               Constant
yP4                             =  0.0;                    % UNITS               Constant
yP5                             =  0.0;                    % UNITS               Constant
yP6                             =  0.0;                    % UNITS               Constant
zP1                             =  0.0;                    % UNITS               Constant
zP2                             =  0.0;                    % UNITS               Constant
zP3                             =  0.0;                    % UNITS               Constant
zP4                             =  0.0;                    % UNITS               Constant
zP5                             =  0.0;                    % UNITS               Constant
zP6                             =  0.0;                    % UNITS               Constant

qBx                             =  0.0;                    % UNITS               Initial Value
qBy                             =  0.0;                    % UNITS               Initial Value
qBz                             =  0.0;                    % UNITS               Initial Value
qL1                             =  0.0;                    % UNITS               Initial Value
qL2                             =  0.0;                    % UNITS               Initial Value
qP1z                            =  0.0;                    % UNITS               Initial Value
qP2z                            =  0.0;                    % UNITS               Initial Value
qP3z                            =  0.0;                    % UNITS               Initial Value
qP4z                            =  0.0;                    % UNITS               Initial Value
qP5z                            =  0.0;                    % UNITS               Initial Value
qP6z                            =  0.0;                    % UNITS               Initial Value
vBx                             =  0.0;                    % UNITS               Initial Value
vBy                             =  0.0;                    % UNITS               Initial Value
vBz                             =  0.0;                    % UNITS               Initial Value
wBx                             =  0.0;                    % UNITS               Initial Value
wBy                             =  0.0;                    % UNITS               Initial Value
wBz                             =  0.0;                    % UNITS               Initial Value
xB                              =  0.0;                    % UNITS               Initial Value
yB                              =  0.0;                    % UNITS               Initial Value
zB                              =  0.0;                    % UNITS               Initial Value
qL1Dt                           =  0.0;                    % UNITS               Initial Value
qL2Dt                           =  0.0;                    % UNITS               Initial Value


mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPzz = params.IP(3,3);
bThrust = params.bThrust;
bDrag = params.bDrag;
g = params.g;

IL1xx = params.IL1(1,1);
IL1yy = params.IL1(2,2);
IL1zz  = params.IL1(3,3);
IL2xx = params.IL2(1,1);
IL2yy = params.IL2(2,2);
IL2zz = params.IL2(3,3);
len1 = params.len1;
len2 = params.len2;
mL1 = params.m1;
mL2 = params.m2;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);
P5Dir = params.propDirections(5);
P6Dir = params.propDirections(6);

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(1);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(1);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(1);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);
qP5x = params.qP5_XYZ(1);
qP5y = params.qP5_XYZ(1);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);
qP6x = params.qP6_XYZ(1);
qP6y = params.qP6_XYZ(1);

tInitial                        =  0.0;                    % second              Initial Time
tFinal                          =  5;                     % sec                 Final Time
tStep                           =  0.1;                    % sec                 Integration Step
printIntScreen                  =  1;                      % 0 or +integer       0 is NO screen output
printIntFile                    =  1;                      % 0 or +integer       0 is NO file   output
absError                        =  1.0E-6;                %                     Absolute Error
relError                        =  1.0E-6;                %                     Relative Error
%-------------------------------+--------------------------+-------------------+-----------------


% Evaluate constants
qP1zDt = 0;
qP2zDt = 0;
qP3zDt = 0;
qP4zDt = 0;
qP5zDt = 0;
qP6zDt = 0;
F1 = bThrust*qP1zDt^2*sign(qP1zDt);
F2 = bThrust*qP2zDt^2*sign(qP2zDt);
F3 = bThrust*qP3zDt^2*sign(qP3zDt);
F4 = bThrust*qP4zDt^2*sign(qP4zDt);
F5 = bThrust*qP5zDt^2*sign(qP5zDt);
F6 = bThrust*qP6zDt^2*sign(qP6zDt);
T1 = bDrag*F1/bThrust;
T2 = bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = bDrag*F4/bThrust;
T5 = bDrag*F5/bThrust;
T6 = bDrag*F6/bThrust;
TL1 = 0;
TL2 = 0;
z(23) = cos(qP1y);
z(24) = sin(qP1x);
z(25) = sin(qP1y);
z(26) = z(24)*z(25);
z(27) = cos(qP1x);
z(28) = z(25)*z(27);
z(29) = z(23)*z(24);
z(30) = z(23)*z(27);
z(53) = cos(qP2y);
z(54) = sin(qP2x);
z(55) = sin(qP2y);
z(56) = z(54)*z(55);
z(57) = cos(qP2x);
z(58) = z(55)*z(57);
z(59) = z(53)*z(54);
z(60) = z(53)*z(57);
z(83) = cos(qP3y);
z(84) = sin(qP3x);
z(85) = sin(qP3y);
z(86) = z(84)*z(85);
z(87) = cos(qP3x);
z(88) = z(85)*z(87);
z(89) = z(83)*z(84);
z(90) = z(83)*z(87);
z(113) = cos(qP4y);
z(114) = sin(qP4x);
z(115) = sin(qP4y);
z(116) = z(114)*z(115);
z(117) = cos(qP4x);
z(118) = z(115)*z(117);
z(119) = z(113)*z(114);
z(120) = z(113)*z(117);
z(143) = cos(qP5y);
z(144) = sin(qP5x);
z(145) = sin(qP5y);
z(146) = z(144)*z(145);
z(147) = cos(qP5x);
z(148) = z(145)*z(147);
z(149) = z(143)*z(144);
z(150) = z(143)*z(147);
z(173) = cos(qP6y);
z(174) = sin(qP6x);
z(175) = sin(qP6y);
z(176) = z(174)*z(175);
z(177) = cos(qP6x);
z(178) = z(175)*z(177);
z(179) = z(173)*z(174);
z(180) = z(173)*z(177);
z(387) = g*mB;
z(388) = g*mL1;
z(389) = g*mL2;
z(390) = g*mP;
z(391) = P1Dir*F1;
z(392) = P2Dir*F2;
z(393) = P3Dir*F3;
z(394) = P4Dir*F4;
z(395) = P5Dir*F5;
z(396) = P6Dir*F6;
z(493) = IPzz*z(29);
z(529) = IPzz*z(59);
z(556) = IPzz*z(89);
z(583) = IPzz*z(119);
z(610) = IPzz*z(149);
z(637) = IPzz*z(179);
z(645) = len1*mL1;
z(650) = len2*mL2;
z(686) = z(29)*z(493) + z(59)*z(529) + z(89)*z(556) + z(119)*z(583) + z(149)*z(610) + z(179)*z(637);
z(709) = IL1xx + IL2xx + 0.25*mL1*len1^2;
z(718) = IL2xx + 0.25*mL2*len2^2;
z(749) = IBxx + mP*(yP1^2+zP1^2) + mP*(yP2^2+zP2^2) + mP*(yP3^2+zP3^2) + mP*(yP4^2+zP4^2) + mP*(yP5^2+zP5^2) + mP*(yP6^2+zP6^2);
z(751) = mP*xP1*yP1;
z(752) = mP*xP2*yP2;
z(753) = mP*xP3*yP3;
z(754) = mP*xP4*yP4;
z(755) = mP*xP5*yP5;
z(756) = mP*xP6*yP6;
z(758) = mP*xP1*zP1;
z(759) = mP*xP2*zP2;
z(760) = mP*xP3*zP3;
z(761) = mP*xP4*zP4;
z(762) = mP*xP5*zP5;
z(763) = mP*xP6*zP6;
z(769) = IBxy - mP*xP1*yP1 - mP*xP2*yP2 - mP*xP3*yP3 - mP*xP4*yP4 - mP*xP5*yP5 - mP*xP6*yP6;
z(771) = IByy + mP*(xP1^2+zP1^2) + mP*(xP2^2+zP2^2) + mP*(xP3^2+zP3^2) + mP*(xP4^2+zP4^2) + mP*(xP5^2+zP5^2) + mP*(xP6^2+zP6^2);
z(773) = mP*yP1*zP1;
z(774) = mP*yP2*zP2;
z(775) = mP*yP3*zP3;
z(776) = mP*yP4*zP4;
z(777) = mP*yP5*zP5;
z(778) = mP*yP6*zP6;
z(784) = IBxz - mP*xP1*zP1 - mP*xP2*zP2 - mP*xP3*zP3 - mP*xP4*zP4 - mP*xP5*zP5 - mP*xP6*zP6;
z(787) = IBzz + mP*(xP1^2+yP1^2) + mP*(xP2^2+yP2^2) + mP*(xP3^2+yP3^2) + mP*(xP4^2+yP4^2) + mP*(xP5^2+yP5^2) + mP*(xP6^2+yP6^2);
z(853) = yP1*z(30);
z(854) = yP2*z(60);
z(855) = yP3*z(90);
z(856) = yP4*z(120);
z(857) = yP5*z(150);
z(858) = yP6*z(180);
z(859) = zP1*z(29);
z(860) = zP1*z(390);
z(861) = zP2*z(59);
z(862) = zP2*z(390);
z(863) = zP3*z(89);
z(864) = zP3*z(390);
z(865) = zP4*z(119);
z(866) = zP4*z(390);
z(867) = zP5*z(149);
z(868) = zP5*z(390);
z(869) = zP6*z(179);
z(870) = zP6*z(390);
z(871) = yP1*z(390);
z(872) = yP2*z(390);
z(873) = yP3*z(390);
z(874) = yP4*z(390);
z(875) = yP5*z(390);
z(876) = yP6*z(390);
z(878) = xP1*z(390);
z(879) = xP2*z(390);
z(880) = xP3*z(390);
z(881) = xP4*z(390);
z(882) = xP5*z(390);
z(883) = xP6*z(390);
z(884) = zP1*z(25);
z(885) = zP2*z(55);
z(886) = zP3*z(85);
z(887) = zP4*z(115);
z(888) = zP5*z(145);
z(889) = zP6*z(175);
z(890) = xP1*z(30);
z(891) = xP2*z(60);
z(892) = xP3*z(90);
z(893) = xP4*z(120);
z(894) = xP5*z(150);
z(895) = xP6*z(180);
z(897) = xP1*z(29);
z(898) = xP2*z(59);
z(899) = xP3*z(89);
z(900) = xP4*z(119);
z(901) = xP5*z(149);
z(902) = xP6*z(179);
z(903) = yP1*z(25);
z(904) = yP2*z(55);
z(905) = yP3*z(85);
z(906) = yP4*z(115);
z(907) = yP5*z(145);
z(908) = yP6*z(175);
z(910) = len1*z(388);
z(912) = len2*z(389);
z(959) = IBxy - z(751) - z(752) - z(753) - z(754) - z(755) - z(756);
z(964) = z(686) + z(771);
z(966) = IByz - z(773) - z(774) - z(775) - z(776) - z(777) - z(778);


VAR = SetMatrixFromNamedQuantities;
[t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile );
OutputToScreenOrFile( [], 0, 0 );   % Close output files.
if( printIntFile ~= 0 ),  PlotOutputFiles;  end


%===========================================================================
function sys = mdlDerivatives( t, VAR, uSimulink )
%===========================================================================
SetNamedQuantitiesFromMatrix( VAR );
z(1) = cos(qBy);
z(5) = sin(qBz);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(9) = z(4)*z(8);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(343) = z(7)*z(14) + z(9)*z(13);
z(11) = z(2)*z(8);
z(340) = z(11)*z(14) - z(3)*z(13);
z(338) = z(3)*z(7) + z(9)*z(11);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(10) = z(5)*z(8);
z(337) = z(3)*z(6) - z(9)*z(10);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(336) = z(6)*z(11) + z(7)*z(10);
z(339) = z(13)*z(337) - z(12)*z(338) - z(14)*z(336);
z(346) = (vBy*z(343)-vBx*z(340)-vBz*z(338))/z(339);
xBDt = z(346);
z(344) = z(6)*z(14) + z(9)*z(12);
z(341) = -z(3)*z(12) - z(10)*z(14);
z(347) = (vBy*z(344)-vBx*z(341)-vBz*z(337))/z(339);
yBDt = -z(347);
z(345) = z(6)*z(13) - z(7)*z(12);
z(342) = -z(10)*z(13) - z(11)*z(12);
z(348) = (vBy*z(345)-vBx*z(342)-vBz*z(336))/z(339);
zBDt = z(348);
z(232) = z(1)*z(14) + z(4)*z(9);
z(233) = (wBx*z(14)+wBz*z(9))/z(232);
qBxDt = z(233);
z(234) = (wBx*z(4)-wBz*z(1))/z(232);
z(235) = -wBy - z(3)*z(234);
qByDt = -z(235);
qBzDt = -z(234);
z(16) = qByDt + z(3)*qBzDt;
z(17) = z(4)*qBxDt + z(14)*qBzDt;
z(18) = z(1)*z(8)*qByDt - z(3)*z(4)*qBxDt;
z(19) = -z(4)*qBxDt*qByDt - qBzDt*z(18);
z(20) = z(8)*qBxDt*qBzDt;
z(21) = -z(1)*z(3)*qBxDt - z(4)*z(8)*qByDt;
z(22) = z(1)*qBxDt*qByDt + qBzDt*z(21);
z(31) = cos(qP1z);
z(32) = sin(qP1z);
z(33) = z(23)*z(31);
z(34) = z(23)*z(32);
z(35) = z(26)*z(31) + z(27)*z(32);
z(36) = z(27)*z(31) - z(26)*z(32);
z(37) = z(24)*z(32) - z(28)*z(31);
z(38) = z(24)*z(31) + z(28)*z(32);
z(39) = z(3)*z(35) + z(14)*z(37) - z(9)*z(33);
z(40) = z(1)*z(33) + z(4)*z(37);
z(41) = z(3)*z(36) + z(9)*z(34) + z(14)*z(38);
z(42) = z(4)*z(38) - z(1)*z(34);
z(43) = z(25)*z(1) + z(30)*z(4);
z(44) = z(30)*z(14) - z(25)*z(9) - z(29)*z(3);
z(52) = z(25)*z(19) + z(30)*z(22) - z(29)*z(20);
z(61) = cos(qP2z);
z(62) = sin(qP2z);
z(63) = z(53)*z(61);
z(64) = z(53)*z(62);
z(65) = z(56)*z(61) + z(57)*z(62);
z(66) = z(57)*z(61) - z(56)*z(62);
z(67) = z(54)*z(62) - z(58)*z(61);
z(68) = z(54)*z(61) + z(58)*z(62);
z(69) = z(3)*z(65) + z(14)*z(67) - z(9)*z(63);
z(70) = z(1)*z(63) + z(4)*z(67);
z(71) = z(3)*z(66) + z(9)*z(64) + z(14)*z(68);
z(72) = z(4)*z(68) - z(1)*z(64);
z(73) = z(55)*z(1) + z(60)*z(4);
z(74) = z(60)*z(14) - z(55)*z(9) - z(59)*z(3);
z(82) = z(55)*z(19) + z(60)*z(22) - z(59)*z(20);
z(91) = cos(qP3z);
z(92) = sin(qP3z);
z(93) = z(83)*z(91);
z(94) = z(83)*z(92);
z(95) = z(86)*z(91) + z(87)*z(92);
z(96) = z(87)*z(91) - z(86)*z(92);
z(97) = z(84)*z(92) - z(88)*z(91);
z(98) = z(84)*z(91) + z(88)*z(92);
z(99) = z(3)*z(95) + z(14)*z(97) - z(9)*z(93);
z(100) = z(1)*z(93) + z(4)*z(97);
z(101) = z(3)*z(96) + z(9)*z(94) + z(14)*z(98);
z(102) = z(4)*z(98) - z(1)*z(94);
z(103) = z(85)*z(1) + z(90)*z(4);
z(104) = z(90)*z(14) - z(85)*z(9) - z(89)*z(3);
z(112) = z(85)*z(19) + z(90)*z(22) - z(89)*z(20);
z(121) = cos(qP4z);
z(122) = sin(qP4z);
z(123) = z(113)*z(121);
z(124) = z(113)*z(122);
z(125) = z(116)*z(121) + z(117)*z(122);
z(126) = z(117)*z(121) - z(116)*z(122);
z(127) = z(114)*z(122) - z(118)*z(121);
z(128) = z(114)*z(121) + z(118)*z(122);
z(129) = z(3)*z(125) + z(14)*z(127) - z(9)*z(123);
z(130) = z(1)*z(123) + z(4)*z(127);
z(131) = z(3)*z(126) + z(9)*z(124) + z(14)*z(128);
z(132) = z(4)*z(128) - z(1)*z(124);
z(133) = z(115)*z(1) + z(120)*z(4);
z(134) = z(120)*z(14) - z(115)*z(9) - z(119)*z(3);
z(142) = z(115)*z(19) + z(120)*z(22) - z(119)*z(20);
z(151) = cos(qP5z);
z(152) = sin(qP5z);
z(153) = z(143)*z(151);
z(154) = z(143)*z(152);
z(155) = z(146)*z(151) + z(147)*z(152);
z(156) = z(147)*z(151) - z(146)*z(152);
z(157) = z(144)*z(152) - z(148)*z(151);
z(158) = z(144)*z(151) + z(148)*z(152);
z(159) = z(3)*z(155) + z(14)*z(157) - z(9)*z(153);
z(160) = z(1)*z(153) + z(4)*z(157);
z(161) = z(3)*z(156) + z(9)*z(154) + z(14)*z(158);
z(162) = z(4)*z(158) - z(1)*z(154);
z(163) = z(145)*z(1) + z(150)*z(4);
z(164) = z(150)*z(14) - z(145)*z(9) - z(149)*z(3);
z(172) = z(145)*z(19) + z(150)*z(22) - z(149)*z(20);
z(181) = cos(qP6z);
z(182) = sin(qP6z);
z(183) = z(173)*z(181);
z(184) = z(173)*z(182);
z(185) = z(176)*z(181) + z(177)*z(182);
z(186) = z(177)*z(181) - z(176)*z(182);
z(187) = z(174)*z(182) - z(178)*z(181);
z(188) = z(174)*z(181) + z(178)*z(182);
z(189) = z(3)*z(185) + z(14)*z(187) - z(9)*z(183);
z(190) = z(1)*z(183) + z(4)*z(187);
z(191) = z(3)*z(186) + z(9)*z(184) + z(14)*z(188);
z(192) = z(4)*z(188) - z(1)*z(184);
z(193) = z(175)*z(1) + z(180)*z(4);
z(194) = z(180)*z(14) - z(175)*z(9) - z(179)*z(3);
z(202) = z(175)*z(19) + z(180)*z(22) - z(179)*z(20);
z(203) = cos(qL1);
z(204) = sin(qL1);
z(205) = z(3)*z(203) + z(14)*z(204);
z(206) = z(4)*z(204);
z(207) = z(4)*z(203);
z(208) = z(14)*z(203) - z(3)*z(204);
z(209) = z(203)*z(16) + z(204)*z(17);
z(210) = z(203)*z(17) - z(204)*z(16);
z(211) = qL1Dt*z(209);
z(212) = qL1Dt*z(210);
z(213) = z(212) + z(203)*z(20) + z(204)*z(22);
z(214) = z(203)*z(22) - z(211) - z(204)*z(20);
z(215) = cos(qL2);
z(216) = sin(qL2);
z(217) = z(203)*z(215) - z(204)*z(216);
z(218) = z(205)*z(215) + z(208)*z(216);
z(219) = z(206)*z(215) + z(207)*z(216);
z(220) = -z(203)*z(216) - z(204)*z(215);
z(221) = z(208)*z(215) - z(205)*z(216);
z(222) = z(207)*z(215) - z(206)*z(216);
z(223) = qL1Dt + z(1)*qBxDt - z(9)*qBzDt;
z(224) = z(203)*qByDt + z(205)*qBzDt + z(206)*qBxDt;
z(225) = z(207)*qBxDt + z(208)*qBzDt - z(204)*qByDt;
z(226) = z(215)*z(224) + z(216)*z(225);
z(227) = z(215)*z(225) - z(216)*z(224);
z(228) = qL2Dt*z(226);
z(229) = qL2Dt*z(227);
z(230) = z(229) + z(215)*z(213) + z(216)*z(214);
z(231) = z(215)*z(214) - z(228) - z(216)*z(213);
z(236) = z(4)*qBxDt*qByDt + qBzDt*z(18);
z(239) = -z(1)*qBxDt*qByDt - qBzDt*z(21);
z(244) = yP1*wBx - xP1*wBy;
z(245) = xP1*wBz - zP1*wBx;
z(246) = zP1*wBy - yP1*wBz;
z(250) = yP2*wBx - xP2*wBy;
z(251) = xP2*wBz - zP2*wBx;
z(252) = zP2*wBy - yP2*wBz;
z(256) = yP3*wBx - xP3*wBy;
z(257) = xP3*wBz - zP3*wBx;
z(258) = zP3*wBy - yP3*wBz;
z(262) = yP4*wBx - xP4*wBy;
z(263) = xP4*wBz - zP4*wBx;
z(264) = zP4*wBy - yP4*wBz;
z(268) = yP5*wBx - xP5*wBy;
z(269) = xP5*wBz - zP5*wBx;
z(270) = zP5*wBy - yP5*wBz;
z(274) = yP6*wBx - xP6*wBy;
z(275) = xP6*wBz - zP6*wBx;
z(276) = zP6*wBy - yP6*wBz;
z(280) = z(12)*z(204) - z(10)*z(203);
z(281) = z(10)*z(204) + z(12)*z(203);
z(282) = z(11)*z(203) + z(13)*z(204);
z(283) = z(13)*z(203) - z(11)*z(204);
z(284) = len1*z(1);
z(285) = len1*z(9);
z(286) = len1*z(204);
z(287) = len1*z(207);
z(288) = len1*z(208);
z(289) = len1*z(19);
z(290) = len1*z(214);
z(291) = 0.5*(len1*qL1Dt+z(284)*qBxDt-z(285)*qBzDt)*z(224) - 0.5*z(290);
z(292) = 0.5*(z(286)*qByDt-z(287)*qBxDt-z(288)*qBzDt)*z(225) - 0.5*(len1*qL1Dt+z(284)*qBxDt-z(285)*qBzDt)*z(223);
z(293) = 0.5*z(289) - 0.5*(z(286)*qByDt-z(287)*qBxDt-z(288)*qBzDt)*z(224);
z(294) = z(286)*qByDt - z(287)*qBxDt - z(288)*qBzDt;
z(295) = len1*qL1Dt + z(284)*qBxDt - z(285)*qBzDt;
z(296) = len1*z(203)*qL1Dt;
z(297) = z(1)*z(203)*qByDt - z(4)*z(204)*qL1Dt;
z(298) = z(203)*z(21) - z(3)*z(203)*qL1Dt - z(8)*z(204)*qBxDt - z(14)*z(204)*qL1Dt;
z(299) = qByDt*z(296) - len1*qBxDt*z(297) - len1*qBzDt*z(298);
z(300) = len1*z(4)*qByDt;
z(301) = -qBxDt*z(300) - len1*qBzDt*z(18);
z(302) = z(299) + z(224)*z(295);
z(303) = z(301) - z(224)*z(294);
z(304) = z(225)*z(294) - z(223)*z(295);
z(305) = z(215)*z(280) + z(216)*z(281);
z(306) = z(215)*z(281) - z(216)*z(280);
z(307) = z(215)*z(282) + z(216)*z(283);
z(308) = z(215)*z(283) - z(216)*z(282);
z(309) = len1*z(216);
z(310) = z(216)*z(284);
z(311) = z(216)*z(285);
z(312) = len1*z(215);
z(313) = z(215)*z(284);
z(314) = z(215)*z(285);
z(315) = len2*z(1);
z(316) = len2*z(9);
z(317) = len2*z(220);
z(318) = len2*z(221);
z(319) = len2*z(222);
z(320) = z(286) - 0.5*z(317);
z(321) = -z(287) - 0.5*z(319);
z(322) = -z(288) - 0.5*z(318);
z(323) = 0.5*len2 + z(312);
z(324) = z(313) + 0.5*z(315);
z(325) = -z(314) - 0.5*z(316);
z(326) = z(215)*z(304) + z(216)*z(303);
z(327) = z(215)*z(303) - z(216)*z(304);
z(328) = len2*z(19);
z(329) = len2*z(231);
z(330) = qL1Dt + qL2Dt + z(1)*qBxDt - z(9)*qBzDt;
z(331) = z(217)*qByDt + z(218)*qBzDt + z(219)*qBxDt;
z(332) = z(220)*qByDt + z(221)*qBzDt + z(222)*qBxDt;
z(333) = z(302) + 0.5*(len2*qL1Dt+len2*qL2Dt+z(315)*qBxDt-z(316)*qBzDt)*z(331) - 0.5*z(329);
z(334) = z(326) - 0.5*(z(317)*qByDt+z(318)*qBzDt+z(319)*qBxDt)*z(332) - 0.5*(len2*qL1Dt+len2*qL2Dt+z(315)*qBxDt-z(316)*qBzDt)*z(330);
z(335) = z(327) + 0.5*z(328) + 0.5*(z(317)*qByDt+z(318)*qBzDt+z(319)*qBxDt)*z(331);
z(349) = -z(2)*z(4) - z(1)*z(3)*z(5);
z(350) = -z(1)*z(5) - z(2)*z(3)*z(4);
z(351) = z(4)*z(5)*z(8);
z(352) = z(2)*z(4)*z(8);
z(353) = z(1)*z(2)*z(3) - z(4)*z(5);
z(354) = zBDt*z(18) - yBDt*(z(6)*qBzDt+z(352)*qBxDt+z(353)*qByDt) - xBDt*(z(349)*qByDt+z(350)*qBzDt-z(351)*qBxDt);
z(356) = z(3)*z(5);
z(357) = z(2)*z(3);
z(358) = yBDt*(z(10)*qBzDt+z(357)*qBxDt) + xBDt*(z(11)*qBzDt-z(356)*qBxDt) - z(8)*qBxDt*zBDt;
z(360) = z(1)*z(5)*z(8);
z(361) = z(1)*z(2)*z(8);
z(362) = -xBDt*(z(6)*qByDt+z(353)*qBzDt+z(360)*qBxDt) - yBDt*(z(7)*qByDt+z(12)*qBzDt-z(361)*qBxDt) - zBDt*z(21);
z(397) = IBxx*wBx + IBxy*wBy + IBxz*wBz;
z(398) = IBxy*wBx + IByy*wBy + IByz*wBz;
z(399) = IBxz*wBx + IByz*wBy + IBzz*wBz;
z(400) = wBx*z(398) - wBy*z(397);
z(401) = wBz*z(397) - wBx*z(399);
z(402) = wBy*z(399) - wBz*z(398);
z(403) = IL1xx*z(223);
z(404) = IL1yy*z(224);
z(405) = IL1zz*z(225);
z(406) = (z(1)*z(14)+z(4)*z(9))/z(232);
z(407) = (z(4)*z(205)-z(14)*z(206)-z(3)*z(4)*z(203))/z(232);
z(408) = (z(14)*z(207)-z(4)*z(208)-z(3)*z(4)*z(204))/z(232);
z(409) = (z(1)*z(205)+z(9)*z(206)-z(1)*z(3)*z(203))/z(232);
z(410) = (z(1)*z(208)+z(9)*z(207)+z(1)*z(3)*z(204))/z(232);
z(411) = IL1xx*z(1);
z(412) = IL1xx*z(9);
z(413) = IL1xx*z(19);
z(414) = IL1yy*z(203);
z(415) = IL1yy*z(205);
z(416) = IL1yy*z(206);
z(417) = IL1yy*z(213);
z(418) = IL1zz*z(207);
z(419) = IL1zz*z(208);
z(420) = IL1zz*z(204);
z(421) = IL1zz*z(214);
z(422) = z(223)*z(404) - z(224)*z(403);
z(423) = z(225)*z(403) - z(223)*z(405);
z(424) = z(224)*z(405) - z(225)*z(404);
z(425) = (z(6)*z(340)-z(7)*z(341)-z(9)*z(342))/z(339);
z(426) = (z(205)*z(342)+z(280)*z(340)-z(282)*z(341))/z(339);
z(427) = (z(208)*z(342)+z(281)*z(340)-z(283)*z(341))/z(339);
z(428) = (z(6)*z(343)-z(7)*z(344)-z(9)*z(345))/z(339);
z(429) = (z(205)*z(345)+z(280)*z(343)-z(282)*z(344))/z(339);
z(430) = (z(208)*z(345)+z(281)*z(343)-z(283)*z(344))/z(339);
z(431) = (z(6)*z(338)-z(7)*z(337)-z(9)*z(336))/z(339);
z(432) = (z(205)*z(336)+z(280)*z(338)-z(282)*z(337))/z(339);
z(433) = (z(208)*z(336)+z(281)*z(338)-z(283)*z(337))/z(339);
z(434) = (z(14)*z(287)-z(4)*z(288)-z(3)*z(4)*z(286))/z(232);
z(435) = (z(4)*z(285)+z(14)*z(284))/z(232);
z(436) = (z(1)*z(288)+z(9)*z(287)+z(1)*z(3)*z(286))/z(232);
z(437) = (z(1)*z(285)-z(9)*z(284))/z(232);
z(438) = IL2xx*z(330);
z(439) = IL2yy*z(331);
z(440) = IL2zz*z(332);
z(441) = (z(4)*z(218)-z(14)*z(219)-z(3)*z(4)*z(217))/z(232);
z(442) = (z(4)*z(221)-z(14)*z(222)-z(3)*z(4)*z(220))/z(232);
z(443) = (z(1)*z(218)+z(9)*z(219)-z(1)*z(3)*z(217))/z(232);
z(444) = (z(1)*z(221)+z(9)*z(222)-z(1)*z(3)*z(220))/z(232);
z(445) = IL2xx*z(1);
z(446) = IL2xx*z(9);
z(447) = IL2xx*z(19);
z(448) = IL2yy*z(217);
z(449) = IL2yy*z(218);
z(450) = IL2yy*z(219);
z(451) = IL2yy*z(230);
z(452) = IL2zz*z(220);
z(453) = IL2zz*z(221);
z(454) = IL2zz*z(222);
z(455) = IL2zz*z(231);
z(456) = z(330)*z(439) - z(331)*z(438);
z(457) = z(332)*z(438) - z(330)*z(440);
z(458) = z(331)*z(440) - z(332)*z(439);
z(459) = (z(218)*z(342)+z(305)*z(340)-z(307)*z(341))/z(339);
z(460) = (z(221)*z(342)+z(306)*z(340)-z(308)*z(341))/z(339);
z(461) = (z(218)*z(345)+z(305)*z(343)-z(307)*z(344))/z(339);
z(462) = (z(221)*z(345)+z(306)*z(343)-z(308)*z(344))/z(339);
z(463) = (z(218)*z(336)+z(305)*z(338)-z(307)*z(337))/z(339);
z(464) = (z(221)*z(336)+z(306)*z(338)-z(308)*z(337))/z(339);
z(465) = (z(4)*z(322)-z(14)*z(321)-z(3)*z(4)*z(320))/z(232);
z(466) = (z(4)*z(311)+z(14)*z(310))/z(232);
z(467) = (z(4)*z(325)-z(14)*z(324))/z(232);
z(468) = (z(1)*z(322)+z(9)*z(321)-z(1)*z(3)*z(320))/z(232);
z(469) = (z(1)*z(311)-z(9)*z(310))/z(232);
z(470) = (z(1)*z(325)+z(9)*z(324))/z(232);
z(471) = z(35)*qByDt + z(39)*qBzDt + z(40)*qBxDt;
z(472) = z(36)*qByDt + z(41)*qBzDt + z(42)*qBxDt;
z(473) = qP1zDt + z(43)*qBxDt + z(44)*qBzDt - z(29)*qByDt;
z(476) = IPzz*z(473);
z(477) = (z(4)*z(39)-z(14)*z(40)-z(3)*z(4)*z(35))/z(232);
z(478) = (z(4)*z(41)-z(14)*z(42)-z(3)*z(4)*z(36))/z(232);
z(479) = (z(14)*z(43)-z(4)*z(44)-z(29)*z(3)*z(4))/z(232);
z(480) = (z(1)*z(39)+z(9)*z(40)-z(1)*z(3)*z(35))/z(232);
z(481) = (z(1)*z(41)+z(9)*z(42)-z(1)*z(3)*z(36))/z(232);
z(482) = (z(1)*z(44)+z(9)*z(43)+z(29)*z(1)*z(3))/z(232);
z(491) = IPzz*z(43);
z(492) = IPzz*z(44);
z(494) = IPzz*z(52);
z(498) = z(340)/z(339);
z(499) = z(341)/z(339);
z(500) = z(342)/z(339);
z(501) = z(343)/z(339);
z(502) = z(344)/z(339);
z(503) = z(345)/z(339);
z(504) = z(338)/z(339);
z(505) = z(337)/z(339);
z(506) = z(336)/z(339);
z(507) = z(65)*qByDt + z(69)*qBzDt + z(70)*qBxDt;
z(508) = z(66)*qByDt + z(71)*qBzDt + z(72)*qBxDt;
z(509) = qP2zDt + z(73)*qBxDt + z(74)*qBzDt - z(59)*qByDt;
z(512) = IPzz*z(509);
z(513) = (z(4)*z(69)-z(14)*z(70)-z(3)*z(4)*z(65))/z(232);
z(514) = (z(4)*z(71)-z(14)*z(72)-z(3)*z(4)*z(66))/z(232);
z(515) = (z(14)*z(73)-z(4)*z(74)-z(59)*z(3)*z(4))/z(232);
z(516) = (z(1)*z(69)+z(9)*z(70)-z(1)*z(3)*z(65))/z(232);
z(517) = (z(1)*z(71)+z(9)*z(72)-z(1)*z(3)*z(66))/z(232);
z(518) = (z(1)*z(74)+z(9)*z(73)+z(59)*z(1)*z(3))/z(232);
z(527) = IPzz*z(73);
z(528) = IPzz*z(74);
z(530) = IPzz*z(82);
z(534) = z(95)*qByDt + z(99)*qBzDt + z(100)*qBxDt;
z(535) = z(96)*qByDt + z(101)*qBzDt + z(102)*qBxDt;
z(536) = qP3zDt + z(103)*qBxDt + z(104)*qBzDt - z(89)*qByDt;
z(539) = IPzz*z(536);
z(540) = (z(4)*z(99)-z(14)*z(100)-z(3)*z(4)*z(95))/z(232);
z(541) = (z(4)*z(101)-z(14)*z(102)-z(3)*z(4)*z(96))/z(232);
z(542) = (z(14)*z(103)-z(4)*z(104)-z(89)*z(3)*z(4))/z(232);
z(543) = (z(1)*z(99)+z(9)*z(100)-z(1)*z(3)*z(95))/z(232);
z(544) = (z(1)*z(101)+z(9)*z(102)-z(1)*z(3)*z(96))/z(232);
z(545) = (z(1)*z(104)+z(9)*z(103)+z(89)*z(1)*z(3))/z(232);
z(554) = IPzz*z(103);
z(555) = IPzz*z(104);
z(557) = IPzz*z(112);
z(561) = z(125)*qByDt + z(129)*qBzDt + z(130)*qBxDt;
z(562) = z(126)*qByDt + z(131)*qBzDt + z(132)*qBxDt;
z(563) = qP4zDt + z(133)*qBxDt + z(134)*qBzDt - z(119)*qByDt;
z(566) = IPzz*z(563);
z(567) = (z(4)*z(129)-z(14)*z(130)-z(3)*z(4)*z(125))/z(232);
z(568) = (z(4)*z(131)-z(14)*z(132)-z(3)*z(4)*z(126))/z(232);
z(569) = (z(14)*z(133)-z(4)*z(134)-z(119)*z(3)*z(4))/z(232);
z(570) = (z(1)*z(129)+z(9)*z(130)-z(1)*z(3)*z(125))/z(232);
z(571) = (z(1)*z(131)+z(9)*z(132)-z(1)*z(3)*z(126))/z(232);
z(572) = (z(1)*z(134)+z(9)*z(133)+z(119)*z(1)*z(3))/z(232);
z(581) = IPzz*z(133);
z(582) = IPzz*z(134);
z(584) = IPzz*z(142);
z(588) = z(155)*qByDt + z(159)*qBzDt + z(160)*qBxDt;
z(589) = z(156)*qByDt + z(161)*qBzDt + z(162)*qBxDt;
z(590) = qP5zDt + z(163)*qBxDt + z(164)*qBzDt - z(149)*qByDt;
z(593) = IPzz*z(590);
z(594) = (z(4)*z(159)-z(14)*z(160)-z(3)*z(4)*z(155))/z(232);
z(595) = (z(4)*z(161)-z(14)*z(162)-z(3)*z(4)*z(156))/z(232);
z(596) = (z(14)*z(163)-z(4)*z(164)-z(149)*z(3)*z(4))/z(232);
z(597) = (z(1)*z(159)+z(9)*z(160)-z(1)*z(3)*z(155))/z(232);
z(598) = (z(1)*z(161)+z(9)*z(162)-z(1)*z(3)*z(156))/z(232);
z(599) = (z(1)*z(164)+z(9)*z(163)+z(149)*z(1)*z(3))/z(232);
z(608) = IPzz*z(163);
z(609) = IPzz*z(164);
z(611) = IPzz*z(172);
z(615) = z(185)*qByDt + z(189)*qBzDt + z(190)*qBxDt;
z(616) = z(186)*qByDt + z(191)*qBzDt + z(192)*qBxDt;
z(617) = qP6zDt + z(193)*qBxDt + z(194)*qBzDt - z(179)*qByDt;
z(620) = IPzz*z(617);
z(621) = (z(4)*z(189)-z(14)*z(190)-z(3)*z(4)*z(185))/z(232);
z(622) = (z(4)*z(191)-z(14)*z(192)-z(3)*z(4)*z(186))/z(232);
z(623) = (z(14)*z(193)-z(4)*z(194)-z(179)*z(3)*z(4))/z(232);
z(624) = (z(1)*z(189)+z(9)*z(190)-z(1)*z(3)*z(185))/z(232);
z(625) = (z(1)*z(191)+z(9)*z(192)-z(1)*z(3)*z(186))/z(232);
z(626) = (z(1)*z(194)+z(9)*z(193)+z(179)*z(1)*z(3))/z(232);
z(635) = IPzz*z(193);
z(636) = IPzz*z(194);
z(638) = IPzz*z(202);
z(642) = mL1*(z(9)*z(425)-z(205)*z(426)-z(208)*z(427)) + mL2*(z(9)*z(425)-z(218)*z(459)-z(221)*z(460)) - 6*mP*z(500);
z(643) = -6*mP*z(498) - mL1*(z(6)*z(425)+z(280)*z(426)+z(281)*z(427)) - mL2*(z(6)*z(425)+z(305)*z(459)+z(306)*z(460));
z(644) = 6*mP*z(499) - mL1*(z(7)*z(425)+z(282)*z(426)+z(283)*z(427)) - mL2*(z(7)*z(425)+z(307)*z(459)+z(308)*z(460));
z(646) = -0.5*z(645)*z(427) - mL2*(z(309)*z(459)+z(323)*z(460));
z(647) = -0.5*mL1*(z(284)*z(427)-z(287)*z(425)) - mL2*(z(310)*z(459)+z(321)*z(425)+z(324)*z(460));
z(648) = 0.5*mL1*(z(285)*z(427)+z(288)*z(425)) + mL2*(z(311)*z(459)-z(322)*z(425)-z(325)*z(460));
z(649) = z(425)*(mL1*z(286)+2*mL2*z(320));
z(651) = z(650)*z(460);
z(653) = 6*mP*z(503) - mL1*(z(9)*z(428)-z(205)*z(429)-z(208)*z(430)) - mL2*(z(9)*z(428)-z(218)*z(461)-z(221)*z(462));
z(654) = 6*mP*z(501) + mL1*(z(6)*z(428)+z(280)*z(429)+z(281)*z(430)) + mL2*(z(6)*z(428)+z(305)*z(461)+z(306)*z(462));
z(655) = mL1*(z(7)*z(428)+z(282)*z(429)+z(283)*z(430)) + mL2*(z(7)*z(428)+z(307)*z(461)+z(308)*z(462)) - 6*mP*z(502);
z(656) = 0.5*z(645)*z(430) + mL2*(z(309)*z(461)+z(323)*z(462));
z(657) = 0.5*mL1*(z(284)*z(430)-z(287)*z(428)) + mL2*(z(310)*z(461)+z(321)*z(428)+z(324)*z(462));
z(658) = -0.5*mL1*(z(285)*z(430)+z(288)*z(428)) - mL2*(z(311)*z(461)-z(322)*z(428)-z(325)*z(462));
z(659) = z(428)*(mL1*z(286)+2*mL2*z(320));
z(660) = z(650)*z(462);
z(662) = mL1*(z(9)*z(431)-z(205)*z(432)-z(208)*z(433)) + mL2*(z(9)*z(431)-z(218)*z(463)-z(221)*z(464)) - 6*mP*z(506);
z(663) = -6*mP*z(504) - mL1*(z(6)*z(431)+z(280)*z(432)+z(281)*z(433)) - mL2*(z(6)*z(431)+z(305)*z(463)+z(306)*z(464));
z(664) = 6*mP*z(505) - mL1*(z(7)*z(431)+z(282)*z(432)+z(283)*z(433)) - mL2*(z(7)*z(431)+z(307)*z(463)+z(308)*z(464));
z(665) = -0.5*z(645)*z(433) - mL2*(z(309)*z(463)+z(323)*z(464));
z(666) = -0.5*mL1*(z(284)*z(433)-z(287)*z(431)) - mL2*(z(310)*z(463)+z(321)*z(431)+z(324)*z(464));
z(667) = 0.5*mL1*(z(285)*z(433)+z(288)*z(431)) + mL2*(z(311)*z(463)-z(322)*z(431)-z(325)*z(464));
z(668) = z(431)*(mL1*z(286)+2*mL2*z(320));
z(669) = z(650)*z(464);
z(671) = IL1xx*z(406) + IL2xx*z(406) + 0.25*z(645)*z(435) + mL2*(z(309)*z(466)-z(323)*z(467));
z(675) = 0.5*mL1*(z(9)*z(434)+z(208)*z(435)) + mP*(yP1*z(14)-zP1*z(3)) + mP*(yP2*z(14)-zP2*z(3)) + mP*(yP3*z(14)-zP3*z(3))  ...
+ mP*(yP4*z(14)-zP4*z(3)) + mP*(yP5*z(14)-zP5*z(3)) + mP*(yP6*z(14)-zP6*z(3)) + mL2*(z(9)*z(465)+z(218)*z(466)-z(221)*z(467));
z(676) = mP*(yP1*z(12)+zP1*z(10)) + mP*(yP2*z(12)+zP2*z(10)) + mP*(yP3*z(12)+zP3*z(10)) + mP*(yP4*z(12)+zP4*z(10)) + mP*(yP5*  ...
z(12)+zP5*z(10)) + mP*(yP6*z(12)+zP6*z(10)) + mL2*(z(305)*z(466)-z(6)*z(465)-z(306)*z(467)) - 0.5*mL1*(z(6)*z(434)-z(281)*z(435));
z(677) = mP*(yP1*z(13)-zP1*z(11)) + mP*(yP2*z(13)-zP2*z(11)) + mP*(yP3*z(13)-zP3*z(11)) + mP*(yP4*z(13)-zP4*z(11)) + mP*(yP5*  ...
z(13)-zP5*z(11)) + mP*(yP6*z(13)-zP6*z(11)) + mL2*(z(307)*z(466)-z(7)*z(465)-z(308)*z(467)) - 0.5*mL1*(z(7)*z(434)-z(283)*z(435));
z(678) = IL2xx*z(406) - 0.5*z(650)*z(467);
z(690) = mL2*z(6)*z(320) + 0.5*mL1*z(6)*z(286) - mP*(xP1*z(12)-zP1*z(6)) - mP*(xP2*z(12)-zP2*z(6)) - mP*(xP3*z(12)-zP3*z(6))  ...
- mP*(xP4*z(12)-zP4*z(6)) - mP*(xP5*z(12)-zP5*z(6)) - mP*(xP6*z(12)-zP6*z(6));
z(691) = mL2*z(7)*z(320) + 0.5*mL1*z(7)*z(286) - mP*(xP1*z(13)-zP1*z(7)) - mP*(xP2*z(13)-zP2*z(7)) - mP*(xP3*z(13)-zP3*z(7))  ...
- mP*(xP4*z(13)-zP4*z(7)) - mP*(xP5*z(13)-zP5*z(7)) - mP*(xP6*z(13)-zP6*z(7));
z(692) = -mL2*z(9)*z(320) - 0.5*mL1*z(9)*z(286) - mP*(xP1*z(14)+zP1*z(9)) - mP*(xP2*z(14)+zP2*z(9)) - mP*(xP3*z(14)+zP3*z(9))  ...
- mP*(xP4*z(14)+zP4*z(9)) - mP*(xP5*z(14)+zP5*z(9)) - mP*(xP6*z(14)+zP6*z(9));
z(697) = -0.25*z(645)*z(437) - mL2*(z(309)*z(469)-z(323)*z(470));
z(698) = mP*(xP1*z(3)+yP1*z(9)) + mP*(xP2*z(3)+yP2*z(9)) + mP*(xP3*z(3)+yP3*z(9)) + mP*(xP4*z(3)+yP4*z(9)) + mP*(xP5*z(3)+yP5*z(9))  ...
+ mP*(xP6*z(3)+yP6*z(9)) + 0.5*mL1*(z(9)*z(436)-z(208)*z(437)) - mL2*(z(9)*z(468)+z(218)*z(469)-z(221)*z(470));
z(699) = -mP*(xP1*z(10)+yP1*z(6)) - mP*(xP2*z(10)+yP2*z(6)) - mP*(xP3*z(10)+yP3*z(6)) - mP*(xP4*z(10)+yP4*z(6)) - mP*(xP5*z(10)+yP5*  ...
z(6)) - mP*(xP6*z(10)+yP6*z(6)) - 0.5*mL1*(z(6)*z(436)+z(281)*z(437)) - mL2*(z(305)*z(469)-z(6)*z(468)-z(306)*z(470));
z(700) = mP*(xP1*z(11)-yP1*z(7)) + mP*(xP2*z(11)-yP2*z(7)) + mP*(xP3*z(11)-yP3*z(7)) + mP*(xP4*z(11)-yP4*z(7)) + mP*(xP5*z(11)-yP5*  ...
z(7)) + mP*(xP6*z(11)-yP6*z(7)) - 0.5*mL1*(z(7)*z(436)+z(283)*z(437)) - mL2*(z(307)*z(469)-z(7)*z(468)-z(308)*z(470));
z(701) = z(650)*z(470);
z(710) = z(709) + mL2*(z(309)^2+z(323)^2);
z(711) = z(411) + z(445) + 0.25*z(645)*z(284) + mL2*(z(309)*z(310)+z(323)*z(324));
z(712) = -z(412) - z(446) - 0.25*z(645)*z(285) - mL2*(z(309)*z(311)-z(323)*z(325));
z(713) = 0.5*z(645)*z(208) + mL2*(z(218)*z(309)+z(221)*z(323));
z(714) = 0.5*z(645)*z(281) + mL2*(z(305)*z(309)+z(306)*z(323));
z(715) = 0.5*z(645)*z(283) + mL2*(z(307)*z(309)+z(308)*z(323));
z(716) = IL2xx + 0.5*z(650)*z(323);
z(717) = z(413) + z(424) + z(447) + z(458) + 0.5*z(645)*z(293) + mL2*(z(309)*z(334)+z(323)*z(335));
z(719) = z(445) + 0.5*z(650)*z(324);
z(720) = 0.5*z(650)*z(325) - z(446);
z(721) = z(650)*z(221);
z(722) = z(650)*z(306);
z(723) = z(650)*z(308);
z(724) = z(447) + z(458) + 0.5*z(650)*z(335);
z(725) = mB + (z(341)*z(644)-z(340)*z(643)-z(342)*z(642))/z(339);
z(726) = (z(344)*z(644)-z(343)*z(643)-z(345)*z(642))/z(339);
z(727) = (z(337)*z(644)-z(336)*z(642)-z(338)*z(643))/z(339);
z(728) = 0.5*(2*z(14)*z(647)-2*z(4)*z(648)-z(3)*z(4)*z(649))/z(232) + mP*(yP1*z(13)*z(499)+zP1*z(3)*z(500)-yP1*z(12)*z(498)-yP1*  ...
z(14)*z(500)-zP1*z(10)*z(498)-zP1*z(11)*z(499)) + mP*(yP2*z(13)*z(499)+zP2*z(3)*z(500)-yP2*z(12)*z(498)-yP2*z(14)*z(500)-zP2*  ...
z(10)*z(498)-zP2*z(11)*z(499)) + mP*(yP3*z(13)*z(499)+zP3*z(3)*z(500)-yP3*z(12)*z(498)-yP3*z(14)*z(500)-zP3*z(10)*z(498)-zP3*  ...
z(11)*z(499)) + mP*(yP4*z(13)*z(499)+zP4*z(3)*z(500)-yP4*z(12)*z(498)-yP4*z(14)*z(500)-zP4*z(10)*z(498)-zP4*z(11)*z(499))  ...
+ mP*(yP5*z(13)*z(499)+zP5*z(3)*z(500)-yP5*z(12)*z(498)-yP5*z(14)*z(500)-zP5*z(10)*z(498)-zP5*z(11)*z(499)) + mP*(yP6*z(13)*  ...
z(499)+zP6*z(3)*z(500)-yP6*z(12)*z(498)-yP6*z(14)*z(500)-zP6*z(10)*z(498)-zP6*z(11)*z(499));
z(729) = -0.5*z(649) - mP*(xP1*z(13)*z(499)+zP1*z(6)*z(498)-xP1*z(12)*z(498)-xP1*z(14)*z(500)-zP1*z(7)*z(499)-zP1*z(9)*z(500))  ...
- mP*(xP2*z(13)*z(499)+zP2*z(6)*z(498)-xP2*z(12)*z(498)-xP2*z(14)*z(500)-zP2*z(7)*z(499)-zP2*z(9)*z(500)) - mP*(xP3*z(13)*z(499)+  ...
zP3*z(6)*z(498)-xP3*z(12)*z(498)-xP3*z(14)*z(500)-zP3*z(7)*z(499)-zP3*z(9)*z(500)) - mP*(xP4*z(13)*z(499)+zP4*z(6)*z(498)-xP4*  ...
z(12)*z(498)-xP4*z(14)*z(500)-zP4*z(7)*z(499)-zP4*z(9)*z(500)) - mP*(xP5*z(13)*z(499)+zP5*z(6)*z(498)-xP5*z(12)*z(498)-xP5*z(14)*  ...
z(500)-zP5*z(7)*z(499)-zP5*z(9)*z(500)) - mP*(xP6*z(13)*z(499)+zP6*z(6)*z(498)-xP6*z(12)*z(498)-xP6*z(14)*z(500)-zP6*z(7)*z(499)-  ...
zP6*z(9)*z(500));
z(730) = 0.5*(2*z(1)*z(648)+2*z(9)*z(647)+z(1)*z(3)*z(649))/z(232) + mP*(xP1*z(10)*z(498)+xP1*z(11)*z(499)+yP1*z(6)*z(498)-xP1*z(  ...
3)*z(500)-yP1*z(7)*z(499)-yP1*z(9)*z(500)) + mP*(xP2*z(10)*z(498)+xP2*z(11)*z(499)+yP2*z(6)*z(498)-xP2*z(3)*z(500)-yP2*z(7)*  ...
z(499)-yP2*z(9)*z(500)) + mP*(xP3*z(10)*z(498)+xP3*z(11)*z(499)+yP3*z(6)*z(498)-xP3*z(3)*z(500)-yP3*z(7)*z(499)-yP3*z(9)*z(500))  ...
+ mP*(xP4*z(10)*z(498)+xP4*z(11)*z(499)+yP4*z(6)*z(498)-xP4*z(3)*z(500)-yP4*z(7)*z(499)-yP4*z(9)*z(500)) + mP*(xP5*z(10)*z(498)+xP5*  ...
z(11)*z(499)+yP5*z(6)*z(498)-xP5*z(3)*z(500)-yP5*z(7)*z(499)-yP5*z(9)*z(500)) + mP*(xP6*z(10)*z(498)+xP6*z(11)*z(499)+yP6*z(6)*  ...
z(498)-xP6*z(3)*z(500)-yP6*z(7)*z(499)-yP6*z(9)*z(500));
z(731) = mP*(z(3)*z(500)*(z(244)*wBx-z(246)*wBz)+z(7)*z(499)*(z(244)*wBy-z(245)*wBz)+z(9)*z(500)*(z(244)*wBy-z(245)*wBz)+z(13)*  ...
z(499)*(z(245)*wBx-z(246)*wBy)-z(6)*z(498)*(z(244)*wBy-z(245)*wBz)-z(10)*z(498)*(z(244)*wBx-z(246)*wBz)-z(11)*z(499)*(z(244)*wBx-  ...
z(246)*wBz)-z(12)*z(498)*(z(245)*wBx-z(246)*wBy)-z(14)*z(500)*(z(245)*wBx-z(246)*wBy)) + mP*(z(3)*z(500)*(z(250)*wBx-z(252)*wBz)+  ...
z(7)*z(499)*(z(250)*wBy-z(251)*wBz)+z(9)*z(500)*(z(250)*wBy-z(251)*wBz)+z(13)*z(499)*(z(251)*wBx-z(252)*wBy)-z(6)*z(498)*(z(250)*  ...
wBy-z(251)*wBz)-z(10)*z(498)*(z(250)*wBx-z(252)*wBz)-z(11)*z(499)*(z(250)*wBx-z(252)*wBz)-z(12)*z(498)*(z(251)*wBx-z(252)*wBy)-  ...
z(14)*z(500)*(z(251)*wBx-z(252)*wBy)) + mP*(z(3)*z(500)*(z(256)*wBx-z(258)*wBz)+z(7)*z(499)*(z(256)*wBy-z(257)*wBz)+z(9)*z(500)*(  ...
z(256)*wBy-z(257)*wBz)+z(13)*z(499)*(z(257)*wBx-z(258)*wBy)-z(6)*z(498)*(z(256)*wBy-z(257)*wBz)-z(10)*z(498)*(z(256)*wBx-z(258)*wBz)-  ...
z(11)*z(499)*(z(256)*wBx-z(258)*wBz)-z(12)*z(498)*(z(257)*wBx-z(258)*wBy)-z(14)*z(500)*(z(257)*wBx-z(258)*wBy)) + mP*(z(3)*  ...
z(500)*(z(262)*wBx-z(264)*wBz)+z(7)*z(499)*(z(262)*wBy-z(263)*wBz)+z(9)*z(500)*(z(262)*wBy-z(263)*wBz)+z(13)*z(499)*(z(263)*wBx-  ...
z(264)*wBy)-z(6)*z(498)*(z(262)*wBy-z(263)*wBz)-z(10)*z(498)*(z(262)*wBx-z(264)*wBz)-z(11)*z(499)*(z(262)*wBx-z(264)*wBz)-z(12)*  ...
z(498)*(z(263)*wBx-z(264)*wBy)-z(14)*z(500)*(z(263)*wBx-z(264)*wBy)) + mP*(z(3)*z(500)*(z(268)*wBx-z(270)*wBz)+z(7)*z(499)*(  ...
z(268)*wBy-z(269)*wBz)+z(9)*z(500)*(z(268)*wBy-z(269)*wBz)+z(13)*z(499)*(z(269)*wBx-z(270)*wBy)-z(6)*z(498)*(z(268)*wBy-z(269)*wBz)-  ...
z(10)*z(498)*(z(268)*wBx-z(270)*wBz)-z(11)*z(499)*(z(268)*wBx-z(270)*wBz)-z(12)*z(498)*(z(269)*wBx-z(270)*wBy)-z(14)*z(500)*(  ...
z(269)*wBx-z(270)*wBy)) + mP*(z(3)*z(500)*(z(274)*wBx-z(276)*wBz)+z(7)*z(499)*(z(274)*wBy-z(275)*wBz)+z(9)*z(500)*(z(274)*wBy-  ...
z(275)*wBz)+z(13)*z(499)*(z(275)*wBx-z(276)*wBy)-z(6)*z(498)*(z(274)*wBy-z(275)*wBz)-z(10)*z(498)*(z(274)*wBx-z(276)*wBz)-z(11)*  ...
z(499)*(z(274)*wBx-z(276)*wBz)-z(12)*z(498)*(z(275)*wBx-z(276)*wBy)-z(14)*z(500)*(z(275)*wBx-z(276)*wBy)) + 0.5*z(649)*(z(20)+z(3)*(  ...
z(1)*z(239)-z(4)*z(236))/z(232)) + (z(647)*(z(9)*z(239)+z(14)*z(236))+z(648)*(z(1)*z(239)-z(4)*z(236)))/z(232) - mB*(vBy*wBz-vBz*  ...
wBy) - mL1*(z(425)*z(291)+z(426)*z(292)+z(427)*z(293)) - mL2*(z(425)*z(333)+z(459)*z(334)+z(460)*z(335)) - (z(642)*(z(336)*  ...
z(362)+z(342)*z(354)-z(345)*z(358))+z(643)*(z(338)*z(362)+z(340)*z(354)-z(343)*z(358))-z(644)*(z(337)*z(362)+z(341)*z(354)-  ...
z(344)*z(358)))/z(339);
z(732) = (z(341)*z(655)-z(340)*z(654)-z(342)*z(653))/z(339);
z(733) = mB - (z(344)*z(655)-z(343)*z(654)-z(345)*z(653))/z(339);
z(734) = (z(337)*z(655)-z(336)*z(653)-z(338)*z(654))/z(339);
z(735) = -0.5*(2*z(4)*z(658)-2*z(14)*z(657)-z(3)*z(4)*z(659))/z(232) - mP*(yP1*z(13)*z(502)+zP1*z(3)*z(503)-yP1*z(12)*z(501)-yP1*  ...
z(14)*z(503)-zP1*z(10)*z(501)-zP1*z(11)*z(502)) - mP*(yP2*z(13)*z(502)+zP2*z(3)*z(503)-yP2*z(12)*z(501)-yP2*z(14)*z(503)-zP2*  ...
z(10)*z(501)-zP2*z(11)*z(502)) - mP*(yP3*z(13)*z(502)+zP3*z(3)*z(503)-yP3*z(12)*z(501)-yP3*z(14)*z(503)-zP3*z(10)*z(501)-zP3*  ...
z(11)*z(502)) - mP*(yP4*z(13)*z(502)+zP4*z(3)*z(503)-yP4*z(12)*z(501)-yP4*z(14)*z(503)-zP4*z(10)*z(501)-zP4*z(11)*z(502))  ...
- mP*(yP5*z(13)*z(502)+zP5*z(3)*z(503)-yP5*z(12)*z(501)-yP5*z(14)*z(503)-zP5*z(10)*z(501)-zP5*z(11)*z(502)) - mP*(yP6*z(13)*  ...
z(502)+zP6*z(3)*z(503)-yP6*z(12)*z(501)-yP6*z(14)*z(503)-zP6*z(10)*z(501)-zP6*z(11)*z(502));
z(736) = 0.5*z(659) + mP*(xP1*z(13)*z(502)+zP1*z(6)*z(501)-xP1*z(12)*z(501)-xP1*z(14)*z(503)-zP1*z(7)*z(502)-zP1*z(9)*z(503))  ...
+ mP*(xP2*z(13)*z(502)+zP2*z(6)*z(501)-xP2*z(12)*z(501)-xP2*z(14)*z(503)-zP2*z(7)*z(502)-zP2*z(9)*z(503)) + mP*(xP3*z(13)*z(502)+  ...
zP3*z(6)*z(501)-xP3*z(12)*z(501)-xP3*z(14)*z(503)-zP3*z(7)*z(502)-zP3*z(9)*z(503)) + mP*(xP4*z(13)*z(502)+zP4*z(6)*z(501)-xP4*  ...
z(12)*z(501)-xP4*z(14)*z(503)-zP4*z(7)*z(502)-zP4*z(9)*z(503)) + mP*(xP5*z(13)*z(502)+zP5*z(6)*z(501)-xP5*z(12)*z(501)-xP5*z(14)*  ...
z(503)-zP5*z(7)*z(502)-zP5*z(9)*z(503)) + mP*(xP6*z(13)*z(502)+zP6*z(6)*z(501)-xP6*z(12)*z(501)-xP6*z(14)*z(503)-zP6*z(7)*z(502)-  ...
zP6*z(9)*z(503));
z(737) = 0.5*(2*z(1)*z(658)+2*z(9)*z(657)-z(1)*z(3)*z(659))/z(232) - mP*(xP1*z(10)*z(501)+xP1*z(11)*z(502)+yP1*z(6)*z(501)-xP1*z(  ...
3)*z(503)-yP1*z(7)*z(502)-yP1*z(9)*z(503)) - mP*(xP2*z(10)*z(501)+xP2*z(11)*z(502)+yP2*z(6)*z(501)-xP2*z(3)*z(503)-yP2*z(7)*  ...
z(502)-yP2*z(9)*z(503)) - mP*(xP3*z(10)*z(501)+xP3*z(11)*z(502)+yP3*z(6)*z(501)-xP3*z(3)*z(503)-yP3*z(7)*z(502)-yP3*z(9)*z(503))  ...
- mP*(xP4*z(10)*z(501)+xP4*z(11)*z(502)+yP4*z(6)*z(501)-xP4*z(3)*z(503)-yP4*z(7)*z(502)-yP4*z(9)*z(503)) - mP*(xP5*z(10)*z(501)+xP5*  ...
z(11)*z(502)+yP5*z(6)*z(501)-xP5*z(3)*z(503)-yP5*z(7)*z(502)-yP5*z(9)*z(503)) - mP*(xP6*z(10)*z(501)+xP6*z(11)*z(502)+yP6*z(6)*  ...
z(501)-xP6*z(3)*z(503)-yP6*z(7)*z(502)-yP6*z(9)*z(503));
z(738) = mB*(vBx*wBz-vBz*wBx) + mL1*(z(428)*z(291)+z(429)*z(292)+z(430)*z(293)) + mL2*(z(428)*z(333)+z(461)*z(334)+z(462)*z(335))  ...
+ (z(657)*(z(9)*z(239)+z(14)*z(236))+z(658)*(z(1)*z(239)-z(4)*z(236)))/z(232) - mP*(z(3)*z(503)*(z(244)*wBx-z(246)*wBz)+z(7)*  ...
z(502)*(z(244)*wBy-z(245)*wBz)+z(9)*z(503)*(z(244)*wBy-z(245)*wBz)+z(13)*z(502)*(z(245)*wBx-z(246)*wBy)-z(6)*z(501)*(z(244)*wBy-  ...
z(245)*wBz)-z(10)*z(501)*(z(244)*wBx-z(246)*wBz)-z(11)*z(502)*(z(244)*wBx-z(246)*wBz)-z(12)*z(501)*(z(245)*wBx-z(246)*wBy)-z(14)*  ...
z(503)*(z(245)*wBx-z(246)*wBy)) - mP*(z(3)*z(503)*(z(250)*wBx-z(252)*wBz)+z(7)*z(502)*(z(250)*wBy-z(251)*wBz)+z(9)*z(503)*(  ...
z(250)*wBy-z(251)*wBz)+z(13)*z(502)*(z(251)*wBx-z(252)*wBy)-z(6)*z(501)*(z(250)*wBy-z(251)*wBz)-z(10)*z(501)*(z(250)*wBx-z(252)*wBz)-  ...
z(11)*z(502)*(z(250)*wBx-z(252)*wBz)-z(12)*z(501)*(z(251)*wBx-z(252)*wBy)-z(14)*z(503)*(z(251)*wBx-z(252)*wBy)) - mP*(z(3)*  ...
z(503)*(z(256)*wBx-z(258)*wBz)+z(7)*z(502)*(z(256)*wBy-z(257)*wBz)+z(9)*z(503)*(z(256)*wBy-z(257)*wBz)+z(13)*z(502)*(z(257)*wBx-  ...
z(258)*wBy)-z(6)*z(501)*(z(256)*wBy-z(257)*wBz)-z(10)*z(501)*(z(256)*wBx-z(258)*wBz)-z(11)*z(502)*(z(256)*wBx-z(258)*wBz)-z(12)*  ...
z(501)*(z(257)*wBx-z(258)*wBy)-z(14)*z(503)*(z(257)*wBx-z(258)*wBy)) - mP*(z(3)*z(503)*(z(262)*wBx-z(264)*wBz)+z(7)*z(502)*(  ...
z(262)*wBy-z(263)*wBz)+z(9)*z(503)*(z(262)*wBy-z(263)*wBz)+z(13)*z(502)*(z(263)*wBx-z(264)*wBy)-z(6)*z(501)*(z(262)*wBy-z(263)*wBz)-  ...
z(10)*z(501)*(z(262)*wBx-z(264)*wBz)-z(11)*z(502)*(z(262)*wBx-z(264)*wBz)-z(12)*z(501)*(z(263)*wBx-z(264)*wBy)-z(14)*z(503)*(  ...
z(263)*wBx-z(264)*wBy)) - mP*(z(3)*z(503)*(z(268)*wBx-z(270)*wBz)+z(7)*z(502)*(z(268)*wBy-z(269)*wBz)+z(9)*z(503)*(z(268)*wBy-  ...
z(269)*wBz)+z(13)*z(502)*(z(269)*wBx-z(270)*wBy)-z(6)*z(501)*(z(268)*wBy-z(269)*wBz)-z(10)*z(501)*(z(268)*wBx-z(270)*wBz)-z(11)*  ...
z(502)*(z(268)*wBx-z(270)*wBz)-z(12)*z(501)*(z(269)*wBx-z(270)*wBy)-z(14)*z(503)*(z(269)*wBx-z(270)*wBy)) - mP*(z(3)*z(503)*(  ...
z(274)*wBx-z(276)*wBz)+z(7)*z(502)*(z(274)*wBy-z(275)*wBz)+z(9)*z(503)*(z(274)*wBy-z(275)*wBz)+z(13)*z(502)*(z(275)*wBx-z(276)*wBy)-  ...
z(6)*z(501)*(z(274)*wBy-z(275)*wBz)-z(10)*z(501)*(z(274)*wBx-z(276)*wBz)-z(11)*z(502)*(z(274)*wBx-z(276)*wBz)-z(12)*z(501)*(  ...
z(275)*wBx-z(276)*wBy)-z(14)*z(503)*(z(275)*wBx-z(276)*wBy)) - 0.5*z(659)*(z(20)+z(3)*(z(1)*z(239)-z(4)*z(236))/z(232))  ...
- (z(653)*(z(336)*z(362)+z(342)*z(354)-z(345)*z(358))+z(654)*(z(338)*z(362)+z(340)*z(354)-z(343)*z(358))-z(655)*(z(337)*z(362)+  ...
z(341)*z(354)-z(344)*z(358)))/z(339);
z(739) = (z(341)*z(664)-z(340)*z(663)-z(342)*z(662))/z(339);
z(740) = (z(344)*z(664)-z(343)*z(663)-z(345)*z(662))/z(339);
z(741) = mB + (z(337)*z(664)-z(336)*z(662)-z(338)*z(663))/z(339);
z(742) = 0.5*(2*z(14)*z(666)-2*z(4)*z(667)-z(3)*z(4)*z(668))/z(232) + mP*(yP1*z(13)*z(505)+zP1*z(3)*z(506)-yP1*z(12)*z(504)-yP1*  ...
z(14)*z(506)-zP1*z(10)*z(504)-zP1*z(11)*z(505)) + mP*(yP2*z(13)*z(505)+zP2*z(3)*z(506)-yP2*z(12)*z(504)-yP2*z(14)*z(506)-zP2*  ...
z(10)*z(504)-zP2*z(11)*z(505)) + mP*(yP3*z(13)*z(505)+zP3*z(3)*z(506)-yP3*z(12)*z(504)-yP3*z(14)*z(506)-zP3*z(10)*z(504)-zP3*  ...
z(11)*z(505)) + mP*(yP4*z(13)*z(505)+zP4*z(3)*z(506)-yP4*z(12)*z(504)-yP4*z(14)*z(506)-zP4*z(10)*z(504)-zP4*z(11)*z(505))  ...
+ mP*(yP5*z(13)*z(505)+zP5*z(3)*z(506)-yP5*z(12)*z(504)-yP5*z(14)*z(506)-zP5*z(10)*z(504)-zP5*z(11)*z(505)) + mP*(yP6*z(13)*  ...
z(505)+zP6*z(3)*z(506)-yP6*z(12)*z(504)-yP6*z(14)*z(506)-zP6*z(10)*z(504)-zP6*z(11)*z(505));
z(743) = -0.5*z(668) - mP*(xP1*z(13)*z(505)+zP1*z(6)*z(504)-xP1*z(12)*z(504)-xP1*z(14)*z(506)-zP1*z(7)*z(505)-zP1*z(9)*z(506))  ...
- mP*(xP2*z(13)*z(505)+zP2*z(6)*z(504)-xP2*z(12)*z(504)-xP2*z(14)*z(506)-zP2*z(7)*z(505)-zP2*z(9)*z(506)) - mP*(xP3*z(13)*z(505)+  ...
zP3*z(6)*z(504)-xP3*z(12)*z(504)-xP3*z(14)*z(506)-zP3*z(7)*z(505)-zP3*z(9)*z(506)) - mP*(xP4*z(13)*z(505)+zP4*z(6)*z(504)-xP4*  ...
z(12)*z(504)-xP4*z(14)*z(506)-zP4*z(7)*z(505)-zP4*z(9)*z(506)) - mP*(xP5*z(13)*z(505)+zP5*z(6)*z(504)-xP5*z(12)*z(504)-xP5*z(14)*  ...
z(506)-zP5*z(7)*z(505)-zP5*z(9)*z(506)) - mP*(xP6*z(13)*z(505)+zP6*z(6)*z(504)-xP6*z(12)*z(504)-xP6*z(14)*z(506)-zP6*z(7)*z(505)-  ...
zP6*z(9)*z(506));
z(744) = 0.5*(2*z(1)*z(667)+2*z(9)*z(666)+z(1)*z(3)*z(668))/z(232) + mP*(xP1*z(10)*z(504)+xP1*z(11)*z(505)+yP1*z(6)*z(504)-xP1*z(  ...
3)*z(506)-yP1*z(7)*z(505)-yP1*z(9)*z(506)) + mP*(xP2*z(10)*z(504)+xP2*z(11)*z(505)+yP2*z(6)*z(504)-xP2*z(3)*z(506)-yP2*z(7)*  ...
z(505)-yP2*z(9)*z(506)) + mP*(xP3*z(10)*z(504)+xP3*z(11)*z(505)+yP3*z(6)*z(504)-xP3*z(3)*z(506)-yP3*z(7)*z(505)-yP3*z(9)*z(506))  ...
+ mP*(xP4*z(10)*z(504)+xP4*z(11)*z(505)+yP4*z(6)*z(504)-xP4*z(3)*z(506)-yP4*z(7)*z(505)-yP4*z(9)*z(506)) + mP*(xP5*z(10)*z(504)+xP5*  ...
z(11)*z(505)+yP5*z(6)*z(504)-xP5*z(3)*z(506)-yP5*z(7)*z(505)-yP5*z(9)*z(506)) + mP*(xP6*z(10)*z(504)+xP6*z(11)*z(505)+yP6*z(6)*  ...
z(504)-xP6*z(3)*z(506)-yP6*z(7)*z(505)-yP6*z(9)*z(506));
z(745) = mP*(z(3)*z(506)*(z(244)*wBx-z(246)*wBz)+z(7)*z(505)*(z(244)*wBy-z(245)*wBz)+z(9)*z(506)*(z(244)*wBy-z(245)*wBz)+z(13)*  ...
z(505)*(z(245)*wBx-z(246)*wBy)-z(6)*z(504)*(z(244)*wBy-z(245)*wBz)-z(10)*z(504)*(z(244)*wBx-z(246)*wBz)-z(11)*z(505)*(z(244)*wBx-  ...
z(246)*wBz)-z(12)*z(504)*(z(245)*wBx-z(246)*wBy)-z(14)*z(506)*(z(245)*wBx-z(246)*wBy)) + mP*(z(3)*z(506)*(z(250)*wBx-z(252)*wBz)+  ...
z(7)*z(505)*(z(250)*wBy-z(251)*wBz)+z(9)*z(506)*(z(250)*wBy-z(251)*wBz)+z(13)*z(505)*(z(251)*wBx-z(252)*wBy)-z(6)*z(504)*(z(250)*  ...
wBy-z(251)*wBz)-z(10)*z(504)*(z(250)*wBx-z(252)*wBz)-z(11)*z(505)*(z(250)*wBx-z(252)*wBz)-z(12)*z(504)*(z(251)*wBx-z(252)*wBy)-  ...
z(14)*z(506)*(z(251)*wBx-z(252)*wBy)) + mP*(z(3)*z(506)*(z(256)*wBx-z(258)*wBz)+z(7)*z(505)*(z(256)*wBy-z(257)*wBz)+z(9)*z(506)*(  ...
z(256)*wBy-z(257)*wBz)+z(13)*z(505)*(z(257)*wBx-z(258)*wBy)-z(6)*z(504)*(z(256)*wBy-z(257)*wBz)-z(10)*z(504)*(z(256)*wBx-z(258)*wBz)-  ...
z(11)*z(505)*(z(256)*wBx-z(258)*wBz)-z(12)*z(504)*(z(257)*wBx-z(258)*wBy)-z(14)*z(506)*(z(257)*wBx-z(258)*wBy)) + mP*(z(3)*  ...
z(506)*(z(262)*wBx-z(264)*wBz)+z(7)*z(505)*(z(262)*wBy-z(263)*wBz)+z(9)*z(506)*(z(262)*wBy-z(263)*wBz)+z(13)*z(505)*(z(263)*wBx-  ...
z(264)*wBy)-z(6)*z(504)*(z(262)*wBy-z(263)*wBz)-z(10)*z(504)*(z(262)*wBx-z(264)*wBz)-z(11)*z(505)*(z(262)*wBx-z(264)*wBz)-z(12)*  ...
z(504)*(z(263)*wBx-z(264)*wBy)-z(14)*z(506)*(z(263)*wBx-z(264)*wBy)) + mP*(z(3)*z(506)*(z(268)*wBx-z(270)*wBz)+z(7)*z(505)*(  ...
z(268)*wBy-z(269)*wBz)+z(9)*z(506)*(z(268)*wBy-z(269)*wBz)+z(13)*z(505)*(z(269)*wBx-z(270)*wBy)-z(6)*z(504)*(z(268)*wBy-z(269)*wBz)-  ...
z(10)*z(504)*(z(268)*wBx-z(270)*wBz)-z(11)*z(505)*(z(268)*wBx-z(270)*wBz)-z(12)*z(504)*(z(269)*wBx-z(270)*wBy)-z(14)*z(506)*(  ...
z(269)*wBx-z(270)*wBy)) + mP*(z(3)*z(506)*(z(274)*wBx-z(276)*wBz)+z(7)*z(505)*(z(274)*wBy-z(275)*wBz)+z(9)*z(506)*(z(274)*wBy-  ...
z(275)*wBz)+z(13)*z(505)*(z(275)*wBx-z(276)*wBy)-z(6)*z(504)*(z(274)*wBy-z(275)*wBz)-z(10)*z(504)*(z(274)*wBx-z(276)*wBz)-z(11)*  ...
z(505)*(z(274)*wBx-z(276)*wBz)-z(12)*z(504)*(z(275)*wBx-z(276)*wBy)-z(14)*z(506)*(z(275)*wBx-z(276)*wBy)) + 0.5*z(668)*(z(20)+z(3)*(  ...
z(1)*z(239)-z(4)*z(236))/z(232)) + (z(666)*(z(9)*z(239)+z(14)*z(236))+z(667)*(z(1)*z(239)-z(4)*z(236)))/z(232) - mB*(vBx*wBy-vBy*  ...
wBx) - mL1*(z(431)*z(291)+z(432)*z(292)+z(433)*z(293)) - mL2*(z(431)*z(333)+z(463)*z(334)+z(464)*z(335)) - (z(662)*(z(336)*  ...
z(362)+z(342)*z(354)-z(345)*z(358))+z(663)*(z(338)*z(362)+z(340)*z(354)-z(343)*z(358))-z(664)*(z(337)*z(362)+z(341)*z(354)-  ...
z(344)*z(358)))/z(339);
z(746) = (z(341)*z(677)-z(340)*z(676)-z(342)*z(675))/z(339);
z(747) = (z(344)*z(677)-z(343)*z(676)-z(345)*z(675))/z(339);
z(748) = (z(337)*z(677)-z(336)*z(675)-z(338)*z(676))/z(339);
z(766) = (z(341)*z(691)-z(340)*z(690)-z(342)*z(692))/z(339);
z(767) = (z(344)*z(691)-z(343)*z(690)-z(345)*z(692))/z(339);
z(768) = (z(337)*z(691)-z(336)*z(692)-z(338)*z(690))/z(339);
z(781) = (z(341)*z(700)-z(340)*z(699)-z(342)*z(698))/z(339);
z(782) = (z(344)*z(700)-z(343)*z(699)-z(345)*z(698))/z(339);
z(783) = (z(337)*z(700)-z(336)*z(698)-z(338)*z(699))/z(339);
z(790) = (z(341)*z(715)-z(340)*z(714)-z(342)*z(713))/z(339);
z(791) = (z(344)*z(715)-z(343)*z(714)-z(345)*z(713))/z(339);
z(792) = (z(337)*z(715)-z(336)*z(713)-z(338)*z(714))/z(339);
z(793) = (z(4)*z(712)-z(14)*z(711))/z(232);
z(794) = (z(1)*z(712)+z(9)*z(711))/z(232);
z(795) = z(717) + (z(711)*(z(9)*z(239)+z(14)*z(236))+z(712)*(z(1)*z(239)-z(4)*z(236)))/z(232) - (z(713)*(z(336)*z(362)+z(342)*  ...
z(354)-z(345)*z(358))+z(714)*(z(338)*z(362)+z(340)*z(354)-z(343)*z(358))-z(715)*(z(337)*z(362)+z(341)*z(354)-z(344)*z(358)))/z(339);
z(796) = (z(341)*z(723)-z(340)*z(722)-z(342)*z(721))/z(339);
z(797) = (z(344)*z(723)-z(343)*z(722)-z(345)*z(721))/z(339);
z(798) = (z(337)*z(723)-z(336)*z(721)-z(338)*z(722))/z(339);
z(799) = (z(4)*z(720)-z(14)*z(719))/z(232);
z(800) = (z(1)*z(720)+z(9)*z(719))/z(232);
z(801) = z(724) + (z(719)*(z(9)*z(239)+z(14)*z(236))+z(720)*(z(1)*z(239)-z(4)*z(236)))/z(232) - 0.5*(z(721)*(z(336)*z(362)+  ...
z(342)*z(354)-z(345)*z(358))+z(722)*(z(338)*z(362)+z(340)*z(354)-z(343)*z(358))-z(723)*(z(337)*z(362)+z(341)*z(354)-z(344)*  ...
z(358)))/z(339);
z(804) = z(25)*z(6) + z(29)*z(10) + z(30)*z(12);
z(807) = z(25)*z(7) + z(30)*z(13) - z(29)*z(11);
z(812) = z(55)*z(6) + z(59)*z(10) + z(60)*z(12);
z(815) = z(55)*z(7) + z(60)*z(13) - z(59)*z(11);
z(820) = z(85)*z(6) + z(89)*z(10) + z(90)*z(12);
z(823) = z(85)*z(7) + z(90)*z(13) - z(89)*z(11);
z(828) = z(115)*z(6) + z(119)*z(10) + z(120)*z(12);
z(831) = z(115)*z(7) + z(120)*z(13) - z(119)*z(11);
z(836) = z(145)*z(6) + z(149)*z(10) + z(150)*z(12);
z(839) = z(145)*z(7) + z(150)*z(13) - z(149)*z(11);
z(844) = z(175)*z(6) + z(179)*z(10) + z(180)*z(12);
z(847) = z(175)*z(7) + z(180)*z(13) - z(179)*z(11);
z(850) = z(387)*z(9) + 6*z(390)*z(500) + z(391)*z(499)*z(807) + z(392)*z(499)*z(815) + z(393)*z(499)*z(823) + z(394)*z(499)*z(831)  ...
+ z(395)*z(499)*z(839) + z(396)*z(499)*z(847) - z(44)*z(391)*z(500) - z(74)*z(392)*z(500) - z(104)*z(393)*z(500) - z(134)*z(394)*z(500)  ...
- z(164)*z(395)*z(500) - z(194)*z(396)*z(500) - z(391)*z(498)*z(804) - z(392)*z(498)*z(812) - z(393)*z(498)*z(820) - z(394)*  ...
z(498)*z(828) - z(395)*z(498)*z(836) - z(396)*z(498)*z(844) - z(388)*(z(9)*z(425)-z(205)*z(426)-z(208)*z(427)) - z(389)*(z(9)*  ...
z(425)-z(218)*z(459)-z(221)*z(460));
z(851) = z(44)*z(391)*z(503) + z(74)*z(392)*z(503) + z(104)*z(393)*z(503) + z(134)*z(394)*z(503) + z(164)*z(395)*z(503)  ...
+ z(194)*z(396)*z(503) + z(391)*z(501)*z(804) + z(392)*z(501)*z(812) + z(393)*z(501)*z(820) + z(394)*z(501)*z(828) + z(395)*  ...
z(501)*z(836) + z(396)*z(501)*z(844) + z(388)*(z(9)*z(428)-z(205)*z(429)-z(208)*z(430)) + z(389)*(z(9)*z(428)-z(218)*z(461)-  ...
z(221)*z(462)) - 6*z(390)*z(503) - z(387)*z(3) - z(391)*z(502)*z(807) - z(392)*z(502)*z(815) - z(393)*z(502)*z(823) - z(394)*  ...
z(502)*z(831) - z(395)*z(502)*z(839) - z(396)*z(502)*z(847);
z(852) = 6*z(390)*z(506) + z(391)*z(505)*z(807) + z(392)*z(505)*z(815) + z(393)*z(505)*z(823) + z(394)*z(505)*z(831) + z(395)*  ...
z(505)*z(839) + z(396)*z(505)*z(847) - z(387)*z(14) - z(44)*z(391)*z(506) - z(74)*z(392)*z(506) - z(104)*z(393)*z(506)  ...
- z(134)*z(394)*z(506) - z(164)*z(395)*z(506) - z(194)*z(396)*z(506) - z(391)*z(504)*z(804) - z(392)*z(504)*z(812) - z(393)*  ...
z(504)*z(820) - z(394)*z(504)*z(828) - z(395)*z(504)*z(836) - z(396)*z(504)*z(844) - z(388)*(z(9)*z(431)-z(205)*z(432)-z(208)*  ...
z(433)) - z(389)*(z(9)*z(431)-z(218)*z(463)-z(221)*z(464));
z(877) = z(853)*z(391) + z(854)*z(392) + z(855)*z(393) + z(856)*z(394) + z(857)*z(395) + z(858)*z(396) + z(859)*z(391)  ...
+ z(860)*z(3) + z(861)*z(392) + z(862)*z(3) + z(863)*z(393) + z(864)*z(3) + z(865)*z(394) + z(866)*z(3) + z(867)*z(395)  ...
+ z(868)*z(3) + z(869)*z(396) + z(870)*z(3) + T1*z(479) + T2*z(515) + T3*z(542) + T4*z(569) + T5*z(596) + T6*z(623) + TL1*z(406)  ...
+ TL2*z(406) - z(871)*z(14) - z(872)*z(14) - z(873)*z(14) - z(874)*z(14) - z(875)*z(14) - z(876)*z(14) - 0.5*z(388)*(z(9)*z(434)+  ...
z(208)*z(435)) - z(389)*(z(9)*z(465)+z(218)*z(466)-z(221)*z(467));
z(896) = z(860)*z(9) + z(862)*z(9) + z(864)*z(9) + z(866)*z(9) + z(868)*z(9) + z(870)*z(9) + z(878)*z(14) + z(879)*z(14)  ...
+ z(880)*z(14) + z(881)*z(14) + z(882)*z(14) + z(883)*z(14) + z(884)*z(391) + z(885)*z(392) + z(886)*z(393) + z(887)*z(394)  ...
+ z(888)*z(395) + z(889)*z(396) + z(389)*z(9)*z(320) + 0.5*z(388)*z(9)*z(286) - z(29)*T1 - z(59)*T2 - z(89)*T3 - z(119)*T4  ...
- z(149)*T5 - z(179)*T6 - z(890)*z(391) - z(891)*z(392) - z(892)*z(393) - z(893)*z(394) - z(894)*z(395) - z(895)*z(396);
z(909) = T1*z(482) + T2*z(518) + T3*z(545) + T4*z(572) + T5*z(599) + T6*z(626) + z(389)*(z(9)*z(468)+z(218)*z(469)-z(221)*z(470))  ...
- z(871)*z(9) - z(872)*z(9) - z(873)*z(9) - z(874)*z(9) - z(875)*z(9) - z(876)*z(9) - z(878)*z(3) - z(879)*z(3) - z(880)*z(3)  ...
- z(881)*z(3) - z(882)*z(3) - z(883)*z(3) - z(897)*z(391) - z(898)*z(392) - z(899)*z(393) - z(900)*z(394) - z(901)*z(395)  ...
- z(902)*z(396) - z(903)*z(391) - z(904)*z(392) - z(905)*z(393) - z(906)*z(394) - z(907)*z(395) - z(908)*z(396) - 0.5*z(388)*(z(9)*  ...
z(436)-z(208)*z(437));
z(911) = TL1 + TL2 - 0.5*z(910)*z(208) - z(389)*(z(218)*z(309)+z(221)*z(323));
z(913) = TL2 - 0.5*z(912)*z(221);
z(914) = z(731) - z(850);
z(915) = z(738) - z(851);
z(916) = z(745) - z(852);
z(920) = z(795) - z(911);
z(921) = z(801) - z(913);
z(958) = z(749) - 0.25*(z(3)*z(4)*(4*z(493)*z(479)+4*z(529)*z(515)+4*z(556)*z(542)+4*z(583)*z(569)+4*z(610)*z(596)+4*z(637)*  ...
z(623)+4*z(407)*z(414)+4*z(408)*z(420)+4*z(441)*z(448)+4*z(442)*z(452)+mL1*z(286)*z(434)+4*mL2*z(320)*z(465))+z(4)*(4*z(408)*  ...
z(419)+4*z(479)*z(492)+4*z(515)*z(528)+4*z(542)*z(555)+4*z(569)*z(582)+4*z(596)*z(609)+4*z(623)*z(636)-4*z(406)*z(412)-4*z(406)*  ...
z(446)-4*z(407)*z(415)-4*z(441)*z(449)-4*z(442)*z(453)-mL1*(z(285)*z(435)-z(288)*z(434))-4*mL2*(z(311)*z(466)+z(322)*z(465)+  ...
z(325)*z(467)))+z(14)*(4*z(407)*z(416)+4*z(441)*z(450)+4*z(442)*z(454)-4*z(406)*z(411)-4*z(406)*z(445)-4*z(408)*z(418)-4*z(479)*  ...
z(491)-4*z(515)*z(527)-4*z(542)*z(554)-4*z(569)*z(581)-4*z(596)*z(608)-4*z(623)*z(635)-mL1*(z(284)*z(435)+z(287)*z(434))-4*mL2*(  ...
z(310)*z(466)-z(321)*z(465)-z(324)*z(467))))/z(232);
z(960) = z(959) - z(493)*z(479) - z(529)*z(515) - z(556)*z(542) - z(583)*z(569) - z(610)*z(596) - z(637)*z(623) - z(407)*z(414)  ...
- z(408)*z(420) - z(441)*z(448) - z(442)*z(452) - mL2*z(320)*z(465) - 0.25*mL1*z(286)*z(434);
z(961) = IBxz + 0.25*(z(1)*z(3)*(4*z(493)*z(479)+4*z(529)*z(515)+4*z(556)*z(542)+4*z(583)*z(569)+4*z(610)*z(596)+4*z(637)*z(623)+  ...
4*z(407)*z(414)+4*z(408)*z(420)+4*z(441)*z(448)+4*z(442)*z(452)+mL1*z(286)*z(434)+4*mL2*z(320)*z(465))+z(1)*(4*z(408)*z(419)+4*  ...
z(479)*z(492)+4*z(515)*z(528)+4*z(542)*z(555)+4*z(569)*z(582)+4*z(596)*z(609)+4*z(623)*z(636)-4*z(406)*z(412)-4*z(406)*z(446)-4*  ...
z(407)*z(415)-4*z(441)*z(449)-4*z(442)*z(453)-mL1*(z(285)*z(435)-z(288)*z(434))-4*mL2*(z(311)*z(466)+z(322)*z(465)+z(325)*z(467)))-  ...
z(9)*(4*z(407)*z(416)+4*z(441)*z(450)+4*z(442)*z(454)-4*z(406)*z(411)-4*z(406)*z(445)-4*z(408)*z(418)-4*z(479)*z(491)-4*z(515)*  ...
z(527)-4*z(542)*z(554)-4*z(569)*z(581)-4*z(596)*z(608)-4*z(623)*z(635)-mL1*(z(284)*z(435)+z(287)*z(434))-4*mL2*(z(310)*z(466)-  ...
z(321)*z(465)-z(324)*z(467))))/z(232) - z(758) - z(759) - z(760) - z(761) - z(762) - z(763);
z(962) = z(877) + z(407)*z(417) + z(407)*z(423) + z(441)*z(451) + z(441)*z(457) + z(442)*z(455) + z(442)*z(456) + z(477)*z(472)*z(476)  ...
+ z(513)*z(508)*z(512) + z(540)*z(535)*z(539) + z(567)*z(562)*z(566) + z(594)*z(589)*z(593) + z(621)*z(616)*z(620) + 0.5*mL1*(  ...
z(434)*z(291)-z(435)*z(293)) + (z(675)*(z(336)*z(362)+z(342)*z(354)-z(345)*z(358))+z(676)*(z(338)*z(362)+z(340)*z(354)-z(343)*  ...
z(358))-z(677)*(z(337)*z(362)+z(341)*z(354)-z(344)*z(358)))/z(339) + 0.25*((4*z(407)*z(416)+4*z(441)*z(450)+4*z(442)*z(454)-4*  ...
z(406)*z(411)-4*z(406)*z(445)-4*z(408)*z(418)-4*z(479)*z(491)-4*z(515)*z(527)-4*z(542)*z(554)-4*z(569)*z(581)-4*z(596)*z(608)-4*  ...
z(623)*z(635)-mL1*(z(284)*z(435)+z(287)*z(434))-4*mL2*(z(310)*z(466)-z(321)*z(465)-z(324)*z(467)))*(z(9)*z(239)+z(14)*z(236))-(4*  ...
z(408)*z(419)+4*z(479)*z(492)+4*z(515)*z(528)+4*z(542)*z(555)+4*z(569)*z(582)+4*z(596)*z(609)+4*z(623)*z(636)-4*z(406)*z(412)-4*  ...
z(406)*z(446)-4*z(407)*z(415)-4*z(441)*z(449)-4*z(442)*z(453)-mL1*(z(285)*z(435)-z(288)*z(434))-4*mL2*(z(311)*z(466)+z(322)*  ...
z(465)+z(325)*z(467)))*(z(1)*z(239)-z(4)*z(236)))/z(232) - mP*(yP1*(z(245)*wBx-z(246)*wBy)+zP1*(z(244)*wBx-z(246)*wBz))  ...
- mP*(yP2*(z(251)*wBx-z(252)*wBy)+zP2*(z(250)*wBx-z(252)*wBz)) - mP*(yP3*(z(257)*wBx-z(258)*wBy)+zP3*(z(256)*wBx-z(258)*wBz))  ...
- mP*(yP4*(z(263)*wBx-z(264)*wBy)+zP4*(z(262)*wBx-z(264)*wBz)) - mP*(yP5*(z(269)*wBx-z(270)*wBy)+zP5*(z(268)*wBx-z(270)*wBz))  ...
- mP*(yP6*(z(275)*wBx-z(276)*wBy)+zP6*(z(274)*wBx-z(276)*wBz)) - z(402) - z(406)*z(413) - z(406)*z(424) - z(406)*z(447)  ...
- z(406)*z(458) - z(408)*z(421) - z(408)*z(422) - z(479)*z(494) - z(515)*z(530) - z(542)*z(557) - z(569)*z(584) - z(596)*z(611)  ...
- z(623)*z(638) - z(478)*z(471)*z(476) - z(514)*z(507)*z(512) - z(541)*z(534)*z(539) - z(568)*z(561)*z(566) - z(595)*z(588)*z(593)  ...
- z(622)*z(615)*z(620) - mL2*(z(466)*z(334)-z(465)*z(333)-z(467)*z(335)) - 0.25*(4*z(493)*z(479)+4*z(529)*z(515)+4*z(556)*z(542)+  ...
4*z(583)*z(569)+4*z(610)*z(596)+4*z(637)*z(623)+4*z(407)*z(414)+4*z(408)*z(420)+4*z(441)*z(448)+4*z(442)*z(452)+mL1*z(286)*  ...
z(434)+4*mL2*z(320)*z(465))*(z(20)+z(3)*(z(1)*z(239)-z(4)*z(236))/z(232));
z(963) = z(769) + 0.25*(z(3)*z(4)*(4*z(686)+4*z(203)*z(414)+4*z(204)*z(420)+4*z(217)*z(448)+4*z(220)*z(452)+mL1*z(286)^2+4*mL2*z(320)^2)+  ...
z(4)*(4*z(29)*z(492)+4*z(59)*z(528)+4*z(89)*z(555)+4*z(119)*z(582)+4*z(149)*z(609)+4*z(179)*z(636)+4*z(204)*z(419)+mL1*z(286)*  ...
z(288)-4*z(203)*z(415)-4*z(217)*z(449)-4*z(220)*z(453)-4*mL2*z(320)*z(322))-z(14)*(4*z(29)*z(491)+4*z(59)*z(527)+4*z(89)*z(554)+4*  ...
z(119)*z(581)+4*z(149)*z(608)+4*z(179)*z(635)+4*z(204)*z(418)+mL1*z(286)*z(287)-4*z(203)*z(416)-4*z(217)*z(450)-4*z(220)*z(454)-4*  ...
mL2*z(320)*z(321)))/z(232);
z(965) = z(964) + z(203)*z(414) + z(204)*z(420) + z(217)*z(448) + z(220)*z(452) + mL2*z(320)^2 + 0.25*mL1*z(286)^2;
z(967) = z(966) - 0.25*(z(1)*z(3)*(4*z(686)+4*z(203)*z(414)+4*z(204)*z(420)+4*z(217)*z(448)+4*z(220)*z(452)+mL1*z(286)^2+4*mL2*z(320)^2)+  ...
z(1)*(4*z(29)*z(492)+4*z(59)*z(528)+4*z(89)*z(555)+4*z(119)*z(582)+4*z(149)*z(609)+4*z(179)*z(636)+4*z(204)*z(419)+mL1*z(286)*  ...
z(288)-4*z(203)*z(415)-4*z(217)*z(449)-4*z(220)*z(453)-4*mL2*z(320)*z(322))+z(9)*(4*z(29)*z(491)+4*z(59)*z(527)+4*z(89)*z(554)+4*  ...
z(119)*z(581)+4*z(149)*z(608)+4*z(179)*z(635)+4*z(204)*z(418)+mL1*z(286)*z(287)-4*z(203)*z(416)-4*z(217)*z(450)-4*z(220)*z(454)-4*  ...
mL2*z(320)*z(321)))/z(232);
z(968) = z(896) + mP*(xP1*(z(245)*wBx-z(246)*wBy)-zP1*(z(244)*wBy-z(245)*wBz)) + mP*(xP2*(z(251)*wBx-z(252)*wBy)-zP2*(z(250)*wBy-  ...
z(251)*wBz)) + mP*(xP3*(z(257)*wBx-z(258)*wBy)-zP3*(z(256)*wBy-z(257)*wBz)) + mP*(xP4*(z(263)*wBx-z(264)*wBy)-zP4*(z(262)*wBy-  ...
z(263)*wBz)) + mP*(xP5*(z(269)*wBx-z(270)*wBy)-zP5*(z(268)*wBy-z(269)*wBz)) + mP*(xP6*(z(275)*wBx-z(276)*wBy)-zP6*(z(274)*wBy-  ...
z(275)*wBz)) + z(29)*z(494) + z(59)*z(530) + z(89)*z(557) + z(119)*z(584) + z(149)*z(611) + z(179)*z(638) + z(204)*z(421)  ...
+ z(204)*z(422) + z(36)*z(471)*z(476) + z(66)*z(507)*z(512) + z(96)*z(534)*z(539) + z(126)*z(561)*z(566) + z(156)*z(588)*z(593)  ...
+ z(186)*z(615)*z(620) + 0.25*(4*z(686)+4*z(203)*z(414)+4*z(204)*z(420)+4*z(217)*z(448)+4*z(220)*z(452)+mL1*z(286)^2+4*mL2*z(320)^2)*(  ...
z(20)+z(3)*(z(1)*z(239)-z(4)*z(236))/z(232)) + 0.25*((4*z(29)*z(491)+4*z(59)*z(527)+4*z(89)*z(554)+4*z(119)*z(581)+4*z(149)*  ...
z(608)+4*z(179)*z(635)+4*z(204)*z(418)+mL1*z(286)*z(287)-4*z(203)*z(416)-4*z(217)*z(450)-4*z(220)*z(454)-4*mL2*z(320)*z(321))*(z(  ...
9)*z(239)+z(14)*z(236))+(4*z(29)*z(492)+4*z(59)*z(528)+4*z(89)*z(555)+4*z(119)*z(582)+4*z(149)*z(609)+4*z(179)*z(636)+4*z(204)*  ...
z(419)+mL1*z(286)*z(288)-4*z(203)*z(415)-4*z(217)*z(449)-4*z(220)*z(453)-4*mL2*z(320)*z(322))*(z(1)*z(239)-z(4)*z(236)))/z(232)  ...
- z(401) - z(203)*z(417) - z(203)*z(423) - z(217)*z(451) - z(217)*z(457) - z(220)*z(455) - z(220)*z(456) - mL2*z(320)*z(333)  ...
- z(35)*z(472)*z(476) - z(65)*z(508)*z(512) - z(95)*z(535)*z(539) - z(125)*z(562)*z(566) - z(155)*z(589)*z(593) - z(185)*z(616)*z(620)  ...
- 0.5*mL1*z(286)*z(291) - (z(691)*(z(337)*z(362)+z(341)*z(354)-z(344)*z(358))-z(690)*(z(338)*z(362)+z(340)*z(354)-z(343)*z(358))-  ...
z(692)*(z(336)*z(362)+z(342)*z(354)-z(345)*z(358)))/z(339);
z(969) = z(784) - 0.25*(z(3)*z(4)*(4*z(493)*z(482)+4*z(529)*z(518)+4*z(556)*z(545)+4*z(583)*z(572)+4*z(610)*z(599)+4*z(637)*  ...
z(626)+4*z(410)*z(420)+mL1*z(286)*z(436)-4*z(409)*z(414)-4*z(443)*z(448)-4*z(444)*z(452)-4*mL2*z(320)*z(468))+z(4)*(4*z(409)*  ...
z(415)+4*z(410)*z(419)+4*z(443)*z(449)+4*z(444)*z(453)+4*z(482)*z(492)+4*z(518)*z(528)+4*z(545)*z(555)+4*z(572)*z(582)+4*z(599)*  ...
z(609)+4*z(626)*z(636)+mL1*(z(285)*z(437)+z(288)*z(436))+4*mL2*(z(311)*z(469)+z(322)*z(468)+z(325)*z(470)))-z(14)*(4*z(409)*  ...
z(416)+4*z(410)*z(418)+4*z(443)*z(450)+4*z(444)*z(454)+4*z(482)*z(491)+4*z(518)*z(527)+4*z(545)*z(554)+4*z(572)*z(581)+4*z(599)*  ...
z(608)+4*z(626)*z(635)-mL1*(z(284)*z(437)-z(287)*z(436))-4*mL2*(z(310)*z(469)-z(321)*z(468)-z(324)*z(470))))/z(232);
z(970) = IByz + z(409)*z(414) + z(443)*z(448) + z(444)*z(452) + mL2*z(320)*z(468) - z(773) - z(774) - z(775) - z(776) - z(777)  ...
- z(778) - z(493)*z(482) - z(529)*z(518) - z(556)*z(545) - z(583)*z(572) - z(610)*z(599) - z(637)*z(626) - z(410)*z(420) - 0.25*mL1*  ...
z(286)*z(436);
z(971) = z(787) + 0.25*(z(1)*z(3)*(4*z(493)*z(482)+4*z(529)*z(518)+4*z(556)*z(545)+4*z(583)*z(572)+4*z(610)*z(599)+4*z(637)*  ...
z(626)+4*z(410)*z(420)+mL1*z(286)*z(436)-4*z(409)*z(414)-4*z(443)*z(448)-4*z(444)*z(452)-4*mL2*z(320)*z(468))+z(1)*(4*z(409)*  ...
z(415)+4*z(410)*z(419)+4*z(443)*z(449)+4*z(444)*z(453)+4*z(482)*z(492)+4*z(518)*z(528)+4*z(545)*z(555)+4*z(572)*z(582)+4*z(599)*  ...
z(609)+4*z(626)*z(636)+mL1*(z(285)*z(437)+z(288)*z(436))+4*mL2*(z(311)*z(469)+z(322)*z(468)+z(325)*z(470)))+z(9)*(4*z(409)*  ...
z(416)+4*z(410)*z(418)+4*z(443)*z(450)+4*z(444)*z(454)+4*z(482)*z(491)+4*z(518)*z(527)+4*z(545)*z(554)+4*z(572)*z(581)+4*z(599)*  ...
z(608)+4*z(626)*z(635)-mL1*(z(284)*z(437)-z(287)*z(436))-4*mL2*(z(310)*z(469)-z(321)*z(468)-z(324)*z(470))))/z(232);
z(972) = z(909) + mP*(xP1*(z(244)*wBx-z(246)*wBz)+yP1*(z(244)*wBy-z(245)*wBz)) + mP*(xP2*(z(250)*wBx-z(252)*wBz)+yP2*(z(250)*wBy-  ...
z(251)*wBz)) + mP*(xP3*(z(256)*wBx-z(258)*wBz)+yP3*(z(256)*wBy-z(257)*wBz)) + mP*(xP4*(z(262)*wBx-z(264)*wBz)+yP4*(z(262)*wBy-  ...
z(263)*wBz)) + mP*(xP5*(z(268)*wBx-z(270)*wBz)+yP5*(z(268)*wBy-z(269)*wBz)) + mP*(xP6*(z(274)*wBx-z(276)*wBz)+yP6*(z(274)*wBy-  ...
z(275)*wBz)) + z(481)*z(471)*z(476) + z(517)*z(507)*z(512) + z(544)*z(534)*z(539) + z(571)*z(561)*z(566) + z(598)*z(588)*z(593)  ...
+ z(625)*z(615)*z(620) + 0.5*mL1*(z(436)*z(291)+z(437)*z(293)) + mL2*(z(469)*z(334)-z(468)*z(333)-z(470)*z(335)) + (z(698)*(  ...
z(336)*z(362)+z(342)*z(354)-z(345)*z(358))+z(699)*(z(338)*z(362)+z(340)*z(354)-z(343)*z(358))-z(700)*(z(337)*z(362)+z(341)*  ...
z(354)-z(344)*z(358)))/z(339) - z(400) - z(409)*z(417) - z(409)*z(423) - z(410)*z(421) - z(410)*z(422) - z(443)*z(451)  ...
- z(443)*z(457) - z(444)*z(455) - z(444)*z(456) - z(482)*z(494) - z(518)*z(530) - z(545)*z(557) - z(572)*z(584) - z(599)*z(611)  ...
- z(626)*z(638) - z(480)*z(472)*z(476) - z(516)*z(508)*z(512) - z(543)*z(535)*z(539) - z(570)*z(562)*z(566) - z(597)*z(589)*z(593)  ...
- z(624)*z(616)*z(620) - 0.25*(4*z(493)*z(482)+4*z(529)*z(518)+4*z(556)*z(545)+4*z(583)*z(572)+4*z(610)*z(599)+4*z(637)*z(626)+4*  ...
z(410)*z(420)+mL1*z(286)*z(436)-4*z(409)*z(414)-4*z(443)*z(448)-4*z(444)*z(452)-4*mL2*z(320)*z(468))*(z(20)+z(3)*(z(1)*z(239)-z(4)*  ...
z(236))/z(232)) - 0.25*((4*z(409)*z(415)+4*z(410)*z(419)+4*z(443)*z(449)+4*z(444)*z(453)+4*z(482)*z(492)+4*z(518)*z(528)+4*  ...
z(545)*z(555)+4*z(572)*z(582)+4*z(599)*z(609)+4*z(626)*z(636)+mL1*(z(285)*z(437)+z(288)*z(436))+4*mL2*(z(311)*z(469)+z(322)*  ...
z(468)+z(325)*z(470)))*(z(1)*z(239)-z(4)*z(236))+(4*z(409)*z(416)+4*z(410)*z(418)+4*z(443)*z(450)+4*z(444)*z(454)+4*z(482)*  ...
z(491)+4*z(518)*z(527)+4*z(545)*z(554)+4*z(572)*z(581)+4*z(599)*z(608)+4*z(626)*z(635)-mL1*(z(284)*z(437)-z(287)*z(436))-4*mL2*(  ...
z(310)*z(469)-z(321)*z(468)-z(324)*z(470)))*(z(9)*z(239)+z(14)*z(236)))/z(232);

COEF = zeros( 8, 8 );
COEF(1,1) = z(725);
COEF(1,2) = -z(726);
COEF(1,3) = z(727);
COEF(1,4) = z(728);
COEF(1,5) = z(729);
COEF(1,6) = z(730);
COEF(1,7) = z(646);
COEF(1,8) = -0.5*z(651);
COEF(2,1) = z(732);
COEF(2,2) = z(733);
COEF(2,3) = z(734);
COEF(2,4) = z(735);
COEF(2,5) = z(736);
COEF(2,6) = z(737);
COEF(2,7) = z(656);
COEF(2,8) = 0.5*z(660);
COEF(3,1) = z(739);
COEF(3,2) = -z(740);
COEF(3,3) = z(741);
COEF(3,4) = z(742);
COEF(3,5) = z(743);
COEF(3,6) = z(744);
COEF(3,7) = z(665);
COEF(3,8) = -0.5*z(669);
COEF(4,1) = z(746);
COEF(4,2) = -z(747);
COEF(4,3) = z(748);
COEF(4,4) = z(958);
COEF(4,5) = z(960);
COEF(4,6) = z(961);
COEF(4,7) = z(671);
COEF(4,8) = z(678);
COEF(5,1) = z(766);
COEF(5,2) = -z(767);
COEF(5,3) = z(768);
COEF(5,4) = z(963);
COEF(5,5) = z(965);
COEF(5,6) = z(967);
COEF(6,1) = z(781);
COEF(6,2) = -z(782);
COEF(6,3) = z(783);
COEF(6,4) = z(969);
COEF(6,5) = z(970);
COEF(6,6) = z(971);
COEF(6,7) = z(697);
COEF(6,8) = 0.5*z(701);
COEF(7,1) = z(790);
COEF(7,2) = -z(791);
COEF(7,3) = z(792);
COEF(7,4) = -z(793);
COEF(7,6) = z(794);
COEF(7,7) = z(710);
COEF(7,8) = z(716);
COEF(8,1) = 0.5*z(796);
COEF(8,2) = -0.5*z(797);
COEF(8,3) = 0.5*z(798);
COEF(8,4) = -z(799);
COEF(8,6) = z(800);
COEF(8,7) = z(716);
COEF(8,8) = z(718);
RHS = zeros( 1, 8 );
RHS(1) = -z(914);
RHS(2) = -z(915);
RHS(3) = -z(916);
RHS(4) = z(962);
RHS(5) = z(968);
RHS(6) = z(972);
RHS(7) = -z(920);
RHS(8) = -z(921);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
vBxDt = SolutionToAlgebraicEquations(1);
vByDt = SolutionToAlgebraicEquations(2);
vBzDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);
qL1DDt = SolutionToAlgebraicEquations(7);
qL2DDt = SolutionToAlgebraicEquations(8);

sys = transpose( SetMatrixOfDerivativesPriorToIntegrationStep );
end



%===========================================================================
function VAR = SetMatrixFromNamedQuantities
%===========================================================================
VAR = zeros( 1, 22 );
VAR(1) = qBx;
VAR(2) = qBy;
VAR(3) = qBz;
VAR(4) = qL1;
VAR(5) = qL2;
VAR(6) = qP1z;
VAR(7) = qP2z;
VAR(8) = qP3z;
VAR(9) = qP4z;
VAR(10) = qP5z;
VAR(11) = qP6z;
VAR(12) = vBx;
VAR(13) = vBy;
VAR(14) = vBz;
VAR(15) = wBx;
VAR(16) = wBy;
VAR(17) = wBz;
VAR(18) = xB;
VAR(19) = yB;
VAR(20) = zB;
VAR(21) = qL1Dt;
VAR(22) = qL2Dt;
end


%===========================================================================
function SetNamedQuantitiesFromMatrix( VAR )
%===========================================================================
qBx = VAR(1);
qBy = VAR(2);
qBz = VAR(3);
qL1 = VAR(4);
qL2 = VAR(5);
qP1z = VAR(6);
qP2z = VAR(7);
qP3z = VAR(8);
qP4z = VAR(9);
qP5z = VAR(10);
qP6z = VAR(11);
vBx = VAR(12);
vBy = VAR(13);
vBz = VAR(14);
wBx = VAR(15);
wBy = VAR(16);
wBz = VAR(17);
xB = VAR(18);
yB = VAR(19);
zB = VAR(20);
qL1Dt = VAR(21);
qL2Dt = VAR(22);
end


%===========================================================================
function VARp = SetMatrixOfDerivativesPriorToIntegrationStep
%===========================================================================
VARp = zeros( 1, 22 );
VARp(1) = qBxDt;
VARp(2) = qByDt;
VARp(3) = qBzDt;
VARp(4) = qL1Dt;
VARp(5) = qL2Dt;
VARp(6) = qP1zDt;
VARp(7) = qP2zDt;
VARp(8) = qP3zDt;
VARp(9) = qP4zDt;
VARp(10) = qP5zDt;
VARp(11) = qP6zDt;
VARp(12) = vBxDt;
VARp(13) = vByDt;
VARp(14) = vBzDt;
VARp(15) = wBxDt;
VARp(16) = wByDt;
VARp(17) = wBzDt;
VARp(18) = xBDt;
VARp(19) = yBDt;
VARp(20) = zBDt;
VARp(21) = qL1DDt;
VARp(22) = qL2DDt;
end



%===========================================================================
function Output = mdlOutputs( t, VAR, uSimulink )
%===========================================================================
z(363) = -vBzDt - z(362);
z(355) = -vBxDt - z(354);
z(359) = -vByDt - z(358);
z(364) = (z(338)*z(363)+z(340)*z(355)-z(343)*z(359))/z(339);
xBDDt = z(364);
z(365) = (z(337)*z(363)+z(341)*z(355)-z(344)*z(359))/z(339);
yBDDt = -z(365);
z(366) = (z(336)*z(363)+z(342)*z(355)-z(345)*z(359))/z(339);
zBDDt = z(366);

Output = zeros( 1, 17 );
Output(1) = t;
Output(2) = xB;
Output(3) = yB;
Output(4) = zB;
Output(5) = xBDt;
Output(6) = yBDt;
Output(7) = zBDt;
Output(8) = xBDDt;
Output(9) = yBDDt;
Output(10) = zBDDt;
Output(11) = qBx;
Output(12) = qBy;
Output(13) = qBz;
Output(14) = wBx;
Output(15) = wBy;
Output(16) = wBz;

Output(17) = qL1DDt;
end


%===========================================================================
function OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile )
%===========================================================================
persistent FileIdentifier hasHeaderInformationBeenWritten;

if( isempty(Output) ),
   if( ~isempty(FileIdentifier) ),
      for( i = 1 : 2 ),  fclose( FileIdentifier(i) );  end
      clear FileIdentifier;
      fprintf( 1, '\n Output is in the files hexRotorRRManipKane.i  (i=1,2)\n\n' );
   end
   clear hasHeaderInformationBeenWritten;
   return;
end

if( isempty(hasHeaderInformationBeenWritten) ),
   if( shouldPrintToScreen ),
      fprintf( 1,                '%%       t             xB             yB             zB             xb''            yB''            zB''           Xb''''           yB''''           zB''''            qBx            qBy            qBz            wBx            wBy            wBz\n' );
      fprintf( 1,                '%%   (second)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)\n\n' );
   end
   if( shouldPrintToFile && isempty(FileIdentifier) ),
      FileIdentifier = zeros( 1, 2 );
      FileIdentifier(1) = fopen('hexRotorRRManipKane.1', 'wt');   if( FileIdentifier(1) == -1 ), error('Error: unable to open file hexRotorRRManipKane.1'); end
      fprintf(FileIdentifier(1), '%% FILE: hexRotorRRManipKane.1\n%%\n' );
      fprintf(FileIdentifier(1), '%%       t             xB             yB             zB             xb''            yB''            zB''           Xb''''           yB''''           zB''''            qBx            qBy            qBz            wBx            wBy            wBz\n' );
      fprintf(FileIdentifier(1), '%%   (second)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)        (UNITS)\n\n' );
      FileIdentifier(2) = fopen('hexRotorRRManipKane.2', 'wt');   if( FileIdentifier(2) == -1 ), error('Error: unable to open file hexRotorRRManipKane.2'); end
      fprintf(FileIdentifier(2), '%% FILE: hexRotorRRManipKane.2\n%%\n' );
      fprintf(FileIdentifier(2), '%%     qL1''''\n' );
      fprintf(FileIdentifier(2), '%%    (UNITS)\n\n' );
   end
   hasHeaderInformationBeenWritten = 1;
end

if( shouldPrintToScreen ), WriteNumericalData( 1,                 Output(1:16) );  end
if( shouldPrintToFile ),   WriteNumericalData( FileIdentifier(1), Output(1:16) );  end
if( shouldPrintToFile ),   WriteNumericalData( FileIdentifier(2), Output(17:17) );  end
end


%===========================================================================
function WriteNumericalData( fileIdentifier, Output )
%===========================================================================
numberOfOutputQuantities = length( Output );
if( numberOfOutputQuantities > 0 ),
   for( i = 1 : numberOfOutputQuantities ),
      fprintf( fileIdentifier, ' %- 14.6E', Output(i) );
   end
   fprintf( fileIdentifier, '\n' );
end
end



%===========================================================================
function PlotOutputFiles
%===========================================================================

figure;
data = load( 'hexRotorRRManipKane.2' ); 
plot( [1:length(data)], data(:,1), '-b', 'LineWidth',3 );
legend( 'qL1''''' );
xlabel('index');   % ylabel('Some y-axis label');   title('Some plot title');
clear data;
end



%===========================================================================
function [functionsToEvaluateForEvent, eventTerminatesIntegration1Otherwise0ToContinue, eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1] = EventDetection( t, VAR, uSimulink )
%===========================================================================
% Detects when designated functions are zero or cross zero with positive or negative slope.
% Step 1: Uncomment call to mdlDerivatives and mdlOutputs.
% Step 2: Change functionsToEvaluateForEvent,                      e.g., change  []  to  [t - 5.67]  to stop at t = 5.67.
% Step 3: Change eventTerminatesIntegration1Otherwise0ToContinue,  e.g., change  []  to  [1]  to stop integrating.
% Step 4: Change eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1,  e.g., change  []  to  [1].
% Step 5: Possibly modify function EventDetectedByIntegrator (if eventTerminatesIntegration1Otherwise0ToContinue is 0).
%---------------------------------------------------------------------------
% mdlDerivatives( t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
% mdlOutputs(     t, VAR, uSimulink );        % UNCOMMENT FOR EVENT HANDLING
functionsToEvaluateForEvent = [];
eventTerminatesIntegration1Otherwise0ToContinue = [];
eventDirection_AscendingIs1_CrossingIs0_DescendingIsNegative1 = [];
eventDetectedByIntegratorTerminate1OrContinue0 = eventTerminatesIntegration1Otherwise0ToContinue;
end


%===========================================================================
function [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents )
%===========================================================================
isIntegrationFinished = eventDetectedByIntegratorTerminate1OrContinue0( nIndexOfEvents );
if( ~isIntegrationFinished ),
   SetNamedQuantitiesFromMatrix( VAR );
%  Put code here to modify how integration continues.
   VAR = SetMatrixFromNamedQuantities;
end
end



%===========================================================================
function [t,VAR,Output] = IntegrateForwardOrBackward( tInitial, tFinal, tStep, absError, relError, VAR, printIntScreen, printIntFile )
%===========================================================================
OdeMatlabOptions = odeset( 'RelTol',relError, 'AbsTol',absError, 'MaxStep',tStep, 'Events',@EventDetection );
t = tInitial;                 epsilonT = 0.001*tStep;                   tFinalMinusEpsilonT = tFinal - epsilonT;
printCounterScreen = 0;       integrateForward = tFinal >= tInitial;    tAtEndOfIntegrationStep = t + tStep;
printCounterFile   = 0;       isIntegrationFinished = 0;
mdlDerivatives( t, VAR, 0 );
while 1,
   if( (integrateForward && t >= tFinalMinusEpsilonT) || (~integrateForward && t <= tFinalMinusEpsilonT) ), isIntegrationFinished = 1;  end
   shouldPrintToScreen = printIntScreen && ( isIntegrationFinished || printCounterScreen <= 0.01 );
   shouldPrintToFile   = printIntFile   && ( isIntegrationFinished || printCounterFile   <= 0.01 );
   if( isIntegrationFinished || shouldPrintToScreen || shouldPrintToFile ),
      Output = mdlOutputs( t, VAR, 0 );
      OutputToScreenOrFile( Output, shouldPrintToScreen, shouldPrintToFile );
      if( isIntegrationFinished ), break;  end
      if( shouldPrintToScreen ), printCounterScreen = printIntScreen;  end
      if( shouldPrintToFile ),   printCounterFile   = printIntFile;    end
   end
   [TimeOdeArray, VarOdeArray, timeEventOccurredInIntegrationStep, nStatesArraysAtEvent, nIndexOfEvents] = ode45( @mdlDerivatives, [t tAtEndOfIntegrationStep], VAR, OdeMatlabOptions, 0 );
   if( isempty(timeEventOccurredInIntegrationStep) ),
      lastIndex = length( TimeOdeArray );
      t = TimeOdeArray( lastIndex );
      VAR = VarOdeArray( lastIndex, : );
      printCounterScreen = printCounterScreen - 1;
      printCounterFile   = printCounterFile   - 1;
      if( abs(tAtEndOfIntegrationStep - t) >= abs(epsilonT) ), warning('numerical integration failed'); break;  end
      tAtEndOfIntegrationStep = t + tStep;
      if( (integrateForward && tAtEndOfIntegrationStep > tFinal) || (~integrateForward && tAtEndOfIntegrationStep < tFinal) ) tAtEndOfIntegrationStep = tFinal;  end
   else
      t = timeEventOccurredInIntegrationStep( 1 );    % time  at firstEvent = 1 during this integration step.
      VAR = nStatesArraysAtEvent( 1, : );             % state at firstEvent = 1 during this integration step.
      printCounterScreen = 0;
      printCounterFile   = 0;
      [isIntegrationFinished, VAR] = EventDetectedByIntegrator( t, VAR, nIndexOfEvents(1) );
   end
end
end


%============================================
end    % End of function hexRotorRRManipKane
%============================================
