function qBDot  = hexRotorRRManip_calc_qBDot( qBx, qBy, qBz, wBx, wBy, wBz )
if( nargin ~= 6 ) error( 'hexRotorRRManip_calc_qBDot expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_calc_qBDot.m created Sep 09 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
qBDot = zeros( 3, 1 );
z = zeros( 1, 927 );



%===========================================================================
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(232) = z(1)*z(14) + z(4)*z(9);
z(233) = (wBx*z(14)+wBz*z(9))/z(232);
qBxDt = z(233);
z(3) = sin(qBx);
z(234) = (wBx*z(4)-wBz*z(1))/z(232);
z(235) = -wBy - z(3)*z(234);
qByDt = -z(235);
qBzDt = -z(234);



%===========================================================================
Output = [];

qBDot(1) = qBxDt;
qBDot(2) = qByDt;
qBDot(3) = qBzDt;


%===================================================
end    % End of function hexRotorRRManip_calc_qBDot
%===================================================
