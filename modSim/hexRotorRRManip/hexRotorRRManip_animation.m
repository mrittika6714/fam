%% PROGRAM INFORMATION
%FILENAMEt: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function hexRotorRRManip_animation(results, params, saveVideo)

t = results.t;
animationTimeScale = params.animationTimeScale;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;
states = results.states;
armWidth = params.armWidth;
armColor = params.armColor;
animationOutputs = results.additionalOutputs.animationOutputs;
% Unpack results
xB= states(:,1);
yB= states(:,2);
zB= states(:,3);
qBx= states(:,4);
qBy= states(:,5);
qBz= states(:,6);
setPoint = results.posDes;
lA = params.lA;
lP = params.plotting.lP;
wP = params.plotting.wP;
hP = params.plotting.hP;
vehicleDim = [params.plotting.lBody, params.plotting.wBody, params.plotting.hBody];
numPoints = length(t);
rP1_No_N = animationOutputs(:,1:3);
rP2_No_N = animationOutputs(:,4:6);
rP3_No_N = animationOutputs(:,7:9); 
rP4_No_N = animationOutputs(:,10:12); 
rP5_No_N = animationOutputs(:,13:15); 
rP6_No_N = animationOutputs(:,16:18);
qP1_ZYX = animationOutputs(:,19:21);
qP2_ZYX = animationOutputs(:,22:24);
qP3_ZYX = animationOutputs(:,25:27);
qP4_ZYX = animationOutputs(:,28:30);
qP5_ZYX = animationOutputs(:,31:33);
qP6_ZYX = animationOutputs(:,34:36);
rL2o_No_N = animationOutputs(:,37:39);
rEE_No_N = animationOutputs(:,40:42);

%[bodyVertices, bodyFaces] =  plotCube(vehicleDim,[xB(1), yB(1), zB(1)], [qBz(1), qBy(1), qBx(1)]);

[P1Vertices, P1Faces] =  plotCube([lP, wP, hP],rP1_No_N(1,:), qP1_ZYX(1,:));
[P2Vertices, P2Faces] =  plotCube([lP, wP, hP],rP2_No_N(1,:), qP2_ZYX(1,:));
[P3Vertices, P3Faces] =  plotCube([lP, wP, hP],rP3_No_N(1,:), qP3_ZYX(1,:));
[P4Vertices, P4Faces] =  plotCube([lP, wP, hP],rP4_No_N(1,:), qP4_ZYX(1,:));
[P5Vertices, P5Faces] =  plotCube([lP, wP, hP],rP5_No_N(1,:), qP5_ZYX(1,:));
[P6Vertices, P6Faces] =  plotCube([lP, wP, hP],rP6_No_N(1,:), qP6_ZYX(1,:));

if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end
animationFigure = figure('units','normalized','outerposition',[0 0 1 1]);
figure(animationFigure);
%animationax = gca
%for i = 1: params.additionalPlots
%   subplot(numRows,numCols,i*2);
%   plot(t, params.additionalData{i});
%   hold on
%   limits = [t(1) t(end) ylim*1.1];
%   axis(limits)
%   timeMark{i} = [ limits(1), limits(3); limits(1), limits(4)] ;
%   timeMarkPlot{i} = plot(timeMark{i}(:,1),timeMark{i}(:,2), '-k', 'linewidth', 2);
%   title(params.additionalTitles{i})
% end
% 
% 
% 
% subplot(numRows,numCols,1:2:numRows*numCols)
setPointPlot = plot3(setPoint(1),setPoint(2),setPoint(3), 'r.','markersize',25);
view(45,45)
hold on
%bodyPlot = patch('Vertices',bodyVertices,'Faces',bodyFaces, 'FaceColor', [0.5, 0.5, 0.5], 'facealpha', 0.25);
A1Plot = plot3([xB(1); rP1_No_N(1,1) ], [yB(1); rP1_No_N(1,2)], [zB(1); rP1_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A2Plot = plot3([xB(1); rP2_No_N(1,1) ], [yB(1); rP2_No_N(1,2)], [zB(1); rP2_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A3Plot = plot3([xB(1); rP3_No_N(1,1) ], [yB(1); rP3_No_N(1,2)], [zB(1); rP3_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A4Plot = plot3([xB(1); rP4_No_N(1,1) ], [yB(1); rP4_No_N(1,2)], [zB(1); rP4_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A5Plot = plot3([xB(1); rP5_No_N(1,1) ], [yB(1); rP5_No_N(1,2)], [zB(1); rP5_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A6Plot = plot3([xB(1); rP6_No_N(1,1) ], [yB(1); rP6_No_N(1,2)], [zB(1); rP6_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);

P1Plot = patch('Vertices',P1Vertices,'Faces',P1Faces, 'FaceColor', [0, 0, 0]);
P2Plot = patch('Vertices',P2Vertices,'Faces',P2Faces, 'FaceColor', [0, 0, 0]);
P3Plot = patch('Vertices',P3Vertices,'Faces',P3Faces, 'FaceColor', [0, 0, 0]);
P4Plot = patch('Vertices',P4Vertices,'Faces',P4Faces, 'FaceColor', [0, 0, 0]);
P5Plot = patch('Vertices',P5Vertices,'Faces',P5Faces, 'FaceColor', [0, 0, 0]);
P6Plot = patch('Vertices',P6Vertices,'Faces',P6Faces, 'FaceColor', [0, 0, 0]);

L1Plot = plot3([xB(1); rL2o_No_N(1,1) ], [yB(1); rL2o_No_N(1,2)], [zB(1); rL2o_No_N(1,3)], 'k.-', 'markersize',params.plotting.markerSize, 'linewidth', armWidth);
L2Plot = plot3([rL2o_No_N(1,1); rEE_No_N(1,1) ], [rL2o_No_N(1,2); rEE_No_N(1,2)], [rL2o_No_N(1,3); rEE_No_N(1,3)], 'k.-', 'markersize',params.plotting.markerSize, 'linewidth', armWidth);


% pathPlot = plot3(xB(1), yB(1), zB(1), 'k--', 'linewidth',1);
EEPathPlot = plot3(rEE_No_N(1,1), rEE_No_N(1,2), rEE_No_N(1,3), 'r--', 'linewidth',1);
xMin = min( [xB;setPoint(:,1)]) - lA*2.5;
xMax = max( [xB;setPoint(:,1)]) + lA*2.5;
yMin = min( [yB;setPoint(:,2)]) - lA*2.5;
yMax = max( [yB;setPoint(:,2)]) + lA*2.5;
zMin = min( [zB;setPoint(:,3)]) - lA*2.5;
zMax = max( [zB;setPoint(:,3)]) + lA*2.5;
axis([xMin xMax yMin yMax zMin zMax])
xlabel('x Position [m]');
ylabel('y Position [m]');
zlabel('z Position [m]');
legend('Set Point', 'VehicleBody')
for i =1:numPoints
    setPointPlot.XData = setPoint(i,1);
    setPointPlot.YData = setPoint(i,2);
    setPointPlot.ZData = setPoint(i,3);
    
    %bodyPlot.XData = xB(i);
    %bodyPlot.YData = yB(i);
    %bodyPlot.ZData = zB(i);
    %[ bodyPlot.Vertices, bodyPlot.Faces] =  plotCube(vehicleDim,[xB(i), yB(i), zB(i)], [qBz(i), qBy(i), qBx(i)]);
    
    A1Plot.XData = [xB(i); rP1_No_N(i,1) ];
    A1Plot.YData = [yB(i); rP1_No_N(i,2) ];
    A1Plot.ZData = [zB(i); rP1_No_N(i,3) ];
    A2Plot.XData = [xB(i); rP2_No_N(i,1) ];
    A2Plot.YData = [yB(i); rP2_No_N(i,2) ];
    A2Plot.ZData = [zB(i); rP2_No_N(i,3) ];
    A3Plot.XData = [xB(i); rP3_No_N(i,1) ];
    A3Plot.YData = [yB(i); rP3_No_N(i,2) ];
    A3Plot.ZData = [zB(i); rP3_No_N(i,3) ];
    A4Plot.XData = [xB(i); rP4_No_N(i,1) ];
    A4Plot.YData = [yB(i); rP4_No_N(i,2) ];
    A4Plot.ZData = [zB(i); rP4_No_N(i,3) ];
    A5Plot.XData = [xB(i); rP5_No_N(i,1) ];
    A5Plot.YData = [yB(i); rP5_No_N(i,2) ];
    A5Plot.ZData = [zB(i); rP5_No_N(i,3) ];
    A6Plot.XData = [xB(i); rP6_No_N(i,1) ];
    A6Plot.YData = [yB(i); rP6_No_N(i,2) ];
    A6Plot.ZData = [zB(i); rP6_No_N(i,3) ];
    
    L1Plot.XData = [xB(i); rL2o_No_N(i,1) ];
    L1Plot.YData = [yB(i); rL2o_No_N(i,2) ];
    L1Plot.ZData = [zB(i); rL2o_No_N(i,3) ];
    L2Plot.XData = [rL2o_No_N(i,1); rEE_No_N(i,1) ];
    L2Plot.YData = [rL2o_No_N(i,2); rEE_No_N(i,2) ];
    L2Plot.ZData = [rL2o_No_N(i,3); rEE_No_N(i,3) ];
    
    [ P1Plot.Vertices, P1Plot.Faces] =  plotCube([lP, wP, hP],rP1_No_N(i,:), qP1_ZYX(i,:));
    [ P2Plot.Vertices, P2Plot.Faces] =  plotCube([lP, wP, hP],rP2_No_N(i,:), qP2_ZYX(i,:));
    [ P3Plot.Vertices, P3Plot.Faces] =  plotCube([lP, wP, hP],rP3_No_N(i,:), qP3_ZYX(i,:));
    [ P4Plot.Vertices, P4Plot.Faces] =  plotCube([lP, wP, hP],rP4_No_N(i,:), qP4_ZYX(i,:));
    [ P5Plot.Vertices, P5Plot.Faces] =  plotCube([lP, wP, hP],rP5_No_N(i,:), qP5_ZYX(i,:));
    [ P6Plot.Vertices, P6Plot.Faces] =  plotCube([lP, wP, hP],rP6_No_N(i,:), qP6_ZYX(i,:));
        
%     pathPlot.XData = xB(1:i);
%     pathPlot.YData = yB(1:i);
%     pathPlot.ZData = zB(1:i);
    
    EEPathPlot.XData = rEE_No_N(1:i,1);
    EEPathPlot.YData = rEE_No_N(1:i,2);
    EEPathPlot.ZData = rEE_No_N(1:i,3);
    %xMin = xB(i) - lA*2.5;
    %xMax = xB(i) + lA*2.5;
    %yMin = yB(i) - lA*2.5;
    %yMax = yB(i) + lA*2.5;
    %zMin = zB(i) - lA*2.5;
    %zMax = zB(i) + lA*2.5;

    %axis(animationax, [xMin xMax yMin yMax zMin zMax])
%     for ii = 1:params.additionalPlots
%       timeMarkPlot{ii}.XData = [t(i); t(i)];
%     end
  drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end