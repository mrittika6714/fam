function EE_R_N = hexRotorRRManip_calc_EE_R_N( qBx, qBy, qBz, qL1, qL2 )
if( nargin ~= 5 ) error( 'hexRotorRRManip_calc_EE_R_N expects 5 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_calc_EE_R_N.m created Sep 25 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
EE_R_N = zeros( 3, 3 );
z = zeros( 1, 927 );



%===========================================================================
z(1) = cos(qBy);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(5) = sin(qBz);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(14) = z(1)*z(8);
z(203) = cos(qL1);
z(204) = sin(qL1);
z(205) = z(3)*z(203) + z(14)*z(204);
z(208) = z(14)*z(203) - z(3)*z(204);
z(215) = cos(qL2);
z(216) = sin(qL2);
z(218) = z(205)*z(215) + z(208)*z(216);
z(221) = z(208)*z(215) - z(205)*z(216);
z(280) = z(12)*z(204) - z(10)*z(203);
z(281) = z(10)*z(204) + z(12)*z(203);
z(282) = z(11)*z(203) + z(13)*z(204);
z(283) = z(13)*z(203) - z(11)*z(204);
z(305) = z(215)*z(280) + z(216)*z(281);
z(306) = z(215)*z(281) - z(216)*z(280);
z(307) = z(215)*z(282) + z(216)*z(283);
z(308) = z(215)*z(283) - z(216)*z(282);



%===========================================================================
Output = [];

EE_R_N(1,1) = z(6);
EE_R_N(1,2) = z(7);
EE_R_N(1,3) = -z(9);
EE_R_N(2,1) = z(305);
EE_R_N(2,2) = z(307);
EE_R_N(2,3) = z(218);
EE_R_N(3,1) = z(306);
EE_R_N(3,2) = z(308);
EE_R_N(3,3) = z(221);


%====================================================
end    % End of function hexRotorRRManip_calc_EE_R_N
%====================================================
