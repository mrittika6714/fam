function [SolutionToAlgebraicEquations] = hexRotorRRManip_calc_alphaB_ECI_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz )
if( nargin ~= 12 ) error( 'hexRotorRRManip_calc_alphaB_ECI_B expects 12 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRRManip_calc_alphaB_ECI_B.m created Sep 09 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
%  Add Manipulator Joint Torques
%===========================================================================
z = zeros( 1, 936 );



%===========================================================================
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(232) = z(1)*z(14) + z(4)*z(9);
z(233) = (wBx*z(14)+wBz*z(9))/z(232);
qBxDt = z(233);
z(3) = sin(qBx);
z(234) = (wBx*z(4)-wBz*z(1))/z(232);
z(235) = -wBy - z(3)*z(234);
qByDt = -z(235);
qBzDt = -z(234);
z(18) = z(1)*z(8)*qByDt - z(3)*z(4)*qBxDt;
z(20) = z(8)*qBxDt*qBzDt;
z(21) = -z(1)*z(3)*qBxDt - z(4)*z(8)*qByDt;
z(236) = z(4)*qBxDt*qByDt + qBzDt*z(18);
z(239) = -z(1)*qBxDt*qByDt - qBzDt*z(21);
z(928) = z(14)/z(232);
z(929) = z(9)/z(232);
z(930) = z(3)*z(4)/z(232);
z(931) = z(1)*z(3)/z(232);
z(932) = z(4)/z(232);
z(933) = z(1)/z(232);
z(934) = qBxDDot - (z(9)*z(239)+z(14)*z(236))/z(232);
z(935) = qByDDot + z(20) + z(3)*(z(1)*z(239)-z(4)*z(236))/z(232);
z(936) = qBzDDot - (z(1)*z(239)-z(4)*z(236))/z(232);

COEF = zeros( 3, 3 );
COEF(1,1) = z(928);
COEF(1,3) = z(929);
COEF(2,1) = z(930);
COEF(2,2) = 1;
COEF(2,3) = -z(931);
COEF(3,1) = -z(932);
COEF(3,3) = z(933);
RHS = zeros( 1, 3 );
RHS(1) = z(934);
RHS(2) = z(935);
RHS(3) = z(936);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBxDt = SolutionToAlgebraicEquations(1);
wByDt = SolutionToAlgebraicEquations(2);
wBzDt = SolutionToAlgebraicEquations(3);



%==========================================================
end    % End of function hexRotorRRManip_calc_alphaB_ECI_B
%==========================================================
