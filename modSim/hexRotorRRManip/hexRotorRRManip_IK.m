
clc; clear; close all;
addpath('../plotFuncs')
%% Script Settings
DOF = 6;
numJoints = 8;
method = 1; % 1 = Jacobian Transpose, 2 = psueudoInverse1
alpha = 0.05;
errorTol = 10^(-6);
maxIterations = 10^4;
xDes = [ 1; 1; 1; pi/2; 0; pi/2]; % [position, orientation]
q0 = [0; 0; 0; 0; 0; 0; 0; 0];    % [rB; qB; qL1; qL2]


%% Configure Script
params = hexRotorRRManip_params();
len1 = params.len1;
len2 = params.len2;


if DOF == 6
  JacobianFuncHandle = @(xB, yB, zB, qBx, qBy, qBz, qL1, qL2) hexRotorRRManip_J( len1, len2, xB, yB, zB, qBx, qBy, qBz, qL1, qL2 );
  
elseif DOF == 3
  JacobianFuncHandle = @(q1, q2, q3) RzRzRzManip_J_FullDOF( len1, len2, Len3, q1, q2, q3 );
end

if method == 1
  deltaQFuncHandle = @(alpha, J, deltaX) jacobianTranspose(alpha, J, deltaX, DOF);
elseif method == 2
  deltaQFuncHandle = @(alpha, J, deltaX) pseudoInverse(alpha, J, deltaX, DOF);
end
  
q = q0;
x = updateX(len1, len2, q, DOF );
error = x(1:DOF) - xDes(1:DOF);
iter = 0;

while (norm(error) > errorTol && iter < maxIterations)
  xB = q(1);
  yB = q(2);
  zB = q(3);
  qBx = q(4);
  qBy = q(5);
  qBz = q(6);
  qL1 = q(7);
  qL2 = q(8);
  J = JacobianFuncHandle(xB, yB, zB, qBx, qBy, qBz, qL1, qL2);
  deltaq = alpha * J' * error(1:DOF);
  q = q - deltaq;
  x = updateX(len1, len2, q, DOF );
  error = x - xDes(1:DOF);
  iter = iter + 1;
end

if iter >=  maxIterations
  warning('Solution not found. Reached max number of iterations')
end


%% Plot
xB = q(1);
yB = q(2);
zB = q(3);
qBx = q(4);
qBy = q(5);
qBz = q(6);
qL1 = q(7);
qL2 = q(8);
rMax = params.len1 + params.len2;
xMin = -1.1 * rMax;
xMax = 1.1 * rMax;
yMin = -1.1 * rMax;
yMax = 1.1 * rMax;

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(1);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(1);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(1);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);
qP5x = params.qP5_XYZ(1);
qP5y = params.qP5_XYZ(1);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);
qP6x = params.qP6_XYZ(1);
qP6y = params.qP6_XYZ(1);

qP1z = 0;
qP2z = 0;
qP3z = 0;
qP4z = 0;
qP5z = 0;
qP6z = 0;


[rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N, N_R_P1, ...
  N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6, rL2o_No_N, rEE_No_N ]  = hexRotorRRManip_MGanimationOutputs( xB, ...
  yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, ...
  xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, ...
  qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, ...
  qP6y, qP6z, len1, len2, qL1, qL2 );
results.states = q';
results.t = 0;
results.posDes = xDes;
saveVideo = 0;
params.animationTimeScale = 1;
propPositions = [rP1_No_N; rP2_No_N; rP3_No_N; rP4_No_N; rP5_No_N; rP6_No_N ]';

qP1_ZYX = rotm2eul(N_R_P1, 'ZYX');        
qP2_ZYX = rotm2eul(N_R_P2, 'ZYX');  
qP3_ZYX = rotm2eul(N_R_P3, 'ZYX');  
qP4_ZYX = rotm2eul(N_R_P4, 'ZYX');  
qP5_ZYX = rotm2eul(N_R_P5, 'ZYX');  
qP6_ZYX = rotm2eul(N_R_P6, 'ZYX');  
propOrientations = [qP1_ZYX, qP2_ZYX, qP3_ZYX, qP4_ZYX, qP5_ZYX, qP6_ZYX];
animationOutputs = [propPositions, propOrientations, rL2o_No_N', rEE_No_N'];
additonalOutputs = [animationOutputs];
results.additionalOutputs = additonalOutputs ;
hexRotorRRManip_animation(results, params, saveVideo)
        
%         animationFigure = figure();
% L1Plot = plot([rL1o_No_N(1); rL2o_No_N(1)], [rL1o_No_N(2); rL2o_No_N(2)], ...
%     'k.-','markersize',params.markerSize, 'linewidth',4);
% hold on
% L2Plot = plot([rL2o_No_N(1); rL3o_No_N(1)], [rL2o_No_N(2); rL3o_No_N(2)], ...
%     'k.-','markersize',params.markerSize, 'linewidth',4);
% L3Plot = plot([rL3o_No_N(1); rEE_No_N(1)], [rL3o_No_N(2); rEE_No_N(2)], ...
%     'k.-','markersize',params.markerSize, 'linewidth',4);
% EEPlot = plot([rEE_No_N(1)], [rEE_No_N(2)], ...
%     'r.','markersize',params.markerSize, 'linewidth',4);
% axis([xMin xMax yMin yMax])
% xlabel('x Position [m]');
% ylabel('y Position [m]');
% title('RzRzRz Manipulator','fontweight', 'bold')
% legend([EEPlot],'End Effector')

function x = updateX(len1, len2, q, DOF)
  xB = q(1);
  yB = q(2);
  zB = q(3);
  qBx = q(4);
  qBy = q(5);
  qBz = q(6);
  qL1 = q(7);
  qL2 = q(8);
  rEE_No_N = hexRotorRRManip_EEPosition( len1, len2, xB, yB, zB, qBx, qBy, qBz, qL1, qL2 );
  L2_R_N = hexRotorRRManip_L2_R_N( qBx, qBy, qBz, qL1, qL2 );
  [qL2x, qL2y, qL2z] = dcm2angle(L2_R_N, 'XYZ');
  x = [rEE_No_N; qL2x; qL2y; qL2z];
  x = x(1:DOF);
end

function deltaQ = jacobianTranspose(alpha, J, deltaX, DOF)
  deltaQ = alpha * J' * error(1:DOF);
end

function deltaQ = pseudoInverse(alpha, J, deltaX,DOF)
  deltaQ = alpha * pinv(J) * error(1:DOF);
end

function deltaQ = dampedLeastSquares(input)
    lambda = input.alpha;
    Je = input.J; 
    Jc = input.zeta';
    J = [Je; Jc];
    error = [input.error; 0];
    zeta = input.zeta;
    JStar = (J' * J + lambda^2 * eye(3,3) )\J'; 
    JeStar = Je' * Je + lambda^2 * eye(3,3)\Je';
    %deltaQ = JStar * error %+ (eye(3,3) - JStar*J) * zeta * 0.01 ;
    deltaQ = JeStar * error + (eye(3,3) - JeStar*Je) * zeta * 20 ;
end
