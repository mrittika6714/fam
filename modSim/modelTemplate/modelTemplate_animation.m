%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function modelTemplate_animation(results ,params, saveVideo)

t = results.t;
animationTimeScale = params.animationTimeScale;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;
states = results.states;
% Unpack results
numPoints = length(t);
setPoint = results.posDes;
posAct = results.states(:,1);
if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end
xMin = min( t) - 0.1*min( t);
xMax = max( t) + 0.1*max( t);
yMin = min(0, min( setPoint) - 0.1*min( setPoint));
yMax = max(1, max( setPoint) + 0.1*max( setPoint));

animationFigure = figure();
posActPlot = plot(t(1),posAct(1), 'k.','markersize',params.animation.markerSize);
hold on
setPointPlot = plot(t(1),setPoint(1), 'r.','markersize',params.animation.markerSize);
axis([xMin xMax yMin yMax])
xlabel('time [s]');
ylabel('y Position [m]');
title('Model Template Plot','fontweight', 'bold')

legend('Actual Position', 'Desired Position')
for i =1:numPoints
  setPointPlot.XData = t(i);
  setPointPlot.YData = setPoint(i);
  posActPlot.XData = t(i);
  posActPlot.YData = posAct(i);
  drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end