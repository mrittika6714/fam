function [statesDot, addionalOutputs] = modelTemplate_dynamics(t, states, params, controllerFunc)
u = controllerFunc(t, states);
F = [0; u(1)];



statesDot = [states(2)] + F ;
addionalOutputs = [nan];

end