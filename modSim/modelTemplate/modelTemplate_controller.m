function control = modelTemplate_controller(t, states, trajectories, trajectoryFunc, params)

% Unpack Params

% Unpack states
y = states(1);
yDot = states(2);

controllerType = params.controller.controllerType;

if strcmp(controllerType, 'none')
    control = zeros(1, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;
else
  error('Controller Type not handled');
end 

end