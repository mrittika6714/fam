function  modelTemplate_plot(plotData, params, savePlots)

styleVec ={'k-','r--','y-' };

desiredTrajectory = [plotData.posDes, plotData.vDes];
stateError = plotData.states - desiredTrajectory;

trajectories = figure;
plot(plotData.t, plotData.states(:,1),styleVec{1})
hold on
plot(plotData.t, desiredTrajectory(:,1), styleVec{2})

title(' Trajectory Time Histories');
xlabel('Time [s]');
ylabel('y [units]');
legend('Actual', 'Desired' )

if savePlots
  saveas(trajectories, fullfile(params.resultsFolder,['trajectories','.png'] ));
end

end