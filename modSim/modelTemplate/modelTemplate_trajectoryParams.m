function trajectoryParams = modelTemplate_trajectoryParams(vargin)

tFinal = 30;

  
states0 = [0; 0;]; %load('ICs.mat')


% y trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'minJerk';
yTrajectory.wayPoints = [ states0(1); 0];
yTrajectory.velocities = [states0(2); 0 ];
yTrajectory.tVec = [0; tFinal];
yTrajectory.accelerations = [0; 0];


trajectoryParams.numTrajectories = 1;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = yTrajectory;



end