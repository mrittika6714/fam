function quadRotorRatesB( bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, mProp, IPropxx, IPropyy, IPropzz, qBx, qBy, qBz, wBx, wBy, wBz, q1Dt, q2Dt, q3Dt, q4Dt, xBDt, yBDt, zBDt )
if( nargin ~= 27 ) error( 'quadRotorRatesB expects 27 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotorRatesB.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
BMat = zeros( 12, 4 );





%===========================================================================


%===========================================================================
Output = [];

BMat(1,1) = 0;
BMat(1,2) = 0;
BMat(1,3) = 0;
BMat(1,4) = 0;
BMat(2,1) = 0;
BMat(2,2) = 0;
BMat(2,3) = 0;
BMat(2,4) = 0;
BMat(3,1) = 0;
BMat(3,2) = 0;
BMat(3,3) = 0;
BMat(3,4) = 0;
BMat(4,1) = 0;
BMat(4,2) = 0;
BMat(4,3) = 0;
BMat(4,4) = 0;
BMat(5,1) = 0;
BMat(5,2) = 0;
BMat(5,3) = 0;
BMat(5,4) = 0;
BMat(6,1) = 0;
BMat(6,2) = 0;
BMat(6,3) = 0;
BMat(6,4) = 0;
BMat(7,1) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q1Dt/(mB+4*mProp);
BMat(7,2) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q2Dt/(mB+4*mProp);
BMat(7,3) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q3Dt/(mB+4*mProp);
BMat(7,4) = 2*bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*q4Dt/(mB+4*mProp);
BMat(8,1) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q1Dt/(mB+4*mProp);
BMat(8,2) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q2Dt/(mB+4*mProp);
BMat(8,3) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q3Dt/(mB+4*mProp);
BMat(8,4) = 2*bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*q4Dt/(mB+4*mProp);
BMat(9,1) = 2*bThrust*cos(qBx)*cos(qBy)*q1Dt/(mB+4*mProp);
BMat(9,2) = 2*bThrust*cos(qBx)*cos(qBy)*q2Dt/(mB+4*mProp);
BMat(9,3) = 2*bThrust*cos(qBx)*cos(qBy)*q3Dt/(mB+4*mProp);
BMat(9,4) = 2*bThrust*cos(qBx)*cos(qBy)*q4Dt/(mB+4*mProp);
BMat(10,1) = (IPropzz*(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy+2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*  ...
q1Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx-2*bThrust*lA*q1Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(10,2) = -(2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*q2Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx-(IByz^2-(  ...
IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy-2*bThrust*lA*q2Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(10,3) = (IPropzz*(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy+2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*  ...
q3Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx+2*bThrust*lA*q3Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(10,4) = -(2*bDrag*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*q4Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx-(IByz^2-(  ...
IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy+2*bThrust*lA*q4Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,1) = (2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*q1Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy-(IBzx^2-(  ...
IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx-2*bThrust*lA*q1Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,2) = -(IPropzz*(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx+2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
q2Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy-2*bThrust*lA*q2Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,3) = (2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*q3Dt-IPropzz*(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*wBy-(IBzx^2-(  ...
IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBx+2*bThrust*lA*q3Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(11,4) = -(IPropzz*(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*wBx+2*bDrag*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
q4Dt+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IPropzz*wBy+2*bThrust*lA*q4Dt))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,1) = ((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IPropzz*wBx-2*bThrust*lA*q1Dt)-IPropzz*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*wBy-  ...
2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q1Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,2) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IPropzz*wBy-2*bThrust*lA*q2Dt)-IPropzz*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
wBx-2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q2Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*  ...
mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,3) = ((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IPropzz*wBx+2*bThrust*lA*q3Dt)-IPropzz*(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*wBy-  ...
2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q3Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
BMat(12,4) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IPropzz*wBy+2*bThrust*lA*q4Dt)-IPropzz*(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*  ...
wBx-2*bDrag*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*q4Dt)/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*  ...
mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));


%========================================
end    % End of function quadRotorRatesB
%========================================
