function params = quadRotor_params(simParams)

%% Store simulation Params
params.sim = simParams;


params.IB = [1.16*10^-2, 0, 0; 0, 1.13*10^-2, 0; 0, 0, 1.13*10^-2];
params.mB = 0.892;
params.g = 9.8;

params.mP = 0.0001;
%params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 0];
params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 3.41*10^-5];
lArm = 0.186;
params.lA = lArm;
axisLength = sqrt(3*lArm^3);

numProps = 4;
% Configuration for "standard" quadrotor
propPosAngle = linspace(0,2*pi,numProps+1); % rad
xPropPosVec = lArm * cos(propPosAngle');
yPropPosVec  = lArm * sin(propPosAngle');
zPropPosVec = zeros(numProps,1);

qP1x = 0;
qP1y = 0;
qP2x = 0;
qP2y = 0;
qP3x = 0;
qP3y = 0;
qP4x = 0;
qP4y = 0;

% Set prop position and angles
params.rP1_Bcm_B = [xPropPosVec(1), yPropPosVec(1), zPropPosVec(1)];
params.qP1_XYZ = [qP1x; qP1y; 0];
params.rP2_Bcm_B = [xPropPosVec(2), yPropPosVec(2), zPropPosVec(2)];
params.qP2_XYZ = [qP2x; qP2y; 0];
params.rP3_Bcm_B = [xPropPosVec(3), yPropPosVec(3), zPropPosVec(3)];
params.qP3_XYZ = [qP3x; qP3y; 0];
params.rP4_Bcm_B = [xPropPosVec(4), yPropPosVec(4), zPropPosVec(4)];
params.qP4_XYZ = [qP4x; qP4y; 0];

params.bThrust = 6.1100e-08;
params.bDrag = 1.5*10^-9;
params.propDirections = [ 1, -1, 1, -1];

forceGravity = params.g*(params.mB + params.mP * numProps);
forceP = forceGravity/numProps;
qProp = sqrt(forceP/params.bThrust);

uEqVal = 0;%load('uEqVal.mat');


%% Controller Parameters
params.controller.constantValue = qProp * params.propDirections;  % rad/sec
%Klqr = load('Klqr.mat');
%params.controller.Klqr = Klqr.Klqr;
%params.controller.uEqVal = uEqVal.uEqVal;
params.controller.type = simParams.control.type;
params.controller.space = simParams.control.space;
params.controller.kp = [20; 20; 20;  2; ];
params.controller.kd = [25; 25; 20; 1.5; ];
params.controller.ki = 0.0001;
params.controller.func = simParams.control.func;
%% Trajectory Params
params.trajectory = octoRotor_trajectoryParams(params.sim.tFinal);
params.trajectory.trajectoryFunc = simParams.trajectoryFunc;

%% Plotting/Animation Parameters
params.plotting.lP = lArm/3;
params.plotting.wP = params.plotting.lP/3;
params.plotting.hP = params.plotting.lP/10;
params.plotting.lBody = lArm*2;
params.plotting.wBody = lArm*2;
params.plotting.hBody = params.plotting.hP/2;
params.plotting.armWidth = 5;
params.plotting.armColor = [0.5, 0.5, 0.5];

end



function trajectoryParams = octoRotor_trajectoryParams(tFinal)

xB0 = 2; 
yB0 = 0; 
zB0 = 0; 
qBx0 = 0; 
qBy0 = 0; 
qBz0 = 0; 
vBx0 = 0; 
vBy0 = 0; 
vBz0 = 0; 
wBx0 = 0; 
wBy0 = 0; 
wBz0 = 0; 
qP1z0 = 0; 
qP2z0 = 0; 
qP3z0 = 0; 
qP4z0 = 0; 
qP5z0 = 0; 
qP6z0 = 0;  
qP7z0 = 0; 
qP8z0 = 0; 
qP1zDt0 = 0; 
qP2zDt0 = 0; 
qP3zDt0 = 0; 
qP4zDt0 = 0; 

bodyStates0 = [xB0; yB0; zB0; qBx0; qBy0; qBz0; vBx0; vBy0; vBz0; wBx0; wBy0; wBz0];
propStates0 = [qP1z0; qP2z0; qP3z0; qP4z0;];
states0 = [bodyStates0; propStates0]; %load('ICs.mat')

% x trajectory 
xTrajectory = trajectory;
xTrajectory.type = 'cos';
%xTrajectory.wayPoints = [ states0(1); 0];
%xTrajectory.velocities = [states0(7); 0 ];
xTrajectory.A = 1;
xTrajectory.w = 1;
xTrajectory.tVec = [0; tFinal];
xTrajectory.accelerations = [0; 0];

% y trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'sin';
yTrajectory.wayPoints = [ states0(2); 0];
yTrajectory.velocities = [states0(8); 0 ];
yTrajectory.tVec = [0; tFinal];
yTrajectory.accelerations = [0; 0];
yTrajectory.A = 1;
yTrajectory.w = 1;

% z trajectory 
zTrajectory = trajectory;
zTrajectory.type = 'sin';
%zTrajectory.wayPoints = [ states0(3); 5];
%zTrajectory.velocities = [states0(9); 0 ];
zTrajectory.A = 1;
zTrajectory.w = 2;
%zTrajectory.tVec = [0; tFinal];
%zTrajectory.accelerations = [0; 0];


% qBz trajectory 
qBzTrajectory = trajectory;
qBzTrajectory.type = 'cos';
qBzTrajectory.wayPoints = [ states0(6); 0];
qBzTrajectory.velocities = [states0(12); 0 ];
qBzTrajectory.tVec = [0; tFinal];
qBzTrajectory.accelerations = [0; 0];
qBzTrajectory.A = pi/4;
qBzTrajectory.w = 0.2;


trajectoryParams.numTrajectories = 4;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = xTrajectory;
trajectoryParams.trajectories{2} = yTrajectory;
trajectoryParams.trajectories{3} = zTrajectory;
trajectoryParams.trajectories{4} = qBzTrajectory;


end