%% Setup workspace and define System Paramaters
clc; clear; close all
trajectoryGenFolder = '\\snl\home\mrittik\Matlab\Trajectory Generation';
addpath(genpath(trajectoryGenFolder))
clear GenerateTrajectoryPoint
clear quadRotorRatesController

animate = 1;
saveVideo = 0;
videoName = 'quadRotorRatesAnimation';
tStep = 0.01;
tFinal = 15;
tSpan = 0:tStep:tFinal;
opts = odeset('RelTol',1e-6,'AbsTol',1e-6);
params.IB(1,1) = 0.0117; %kg*m^2% ;
params.IB(2,2)= 0.0117; %kg*m^2;
params.IB(3,3) = 0.00234; %kg*m^2
params.IB(1,2) = 0;%-24.31 * gcmsq2kgmsq;
params.IB(2,3) = 0;%-9.95 * gcmsq2kgmsq;
params.IB(3,1) = 0;%-7.89 * gcmsq2kgmsq;
params.mB = 0.478;
params.lA = 0.18;
params.bThrust = 6.11*10^-8;
params.bDrag = 1.5*10^-9;
params.mProp = 0.0001;
params.g = 9.8;
params.IProp = [0, 0, 0; 0, 0, 0; 0, 0, 0.00005];
x0 = [0; 0; 5; 0; 0; 0; 0; 0; 0; 0; 0; 0];
xDesired = [ 5; 8; 15; 0; 0; 0; 0; 0; 0; 0; 0; 0];
numInputs = 4;

%% Trajectory Generation 

trajectoryType = 'minJerk';
if strcmp(trajectoryType, 'minJerk')
  trajectories.type = trajectoryType;
  trajectories.trajectoryIndex = 3; % fieldname index in which trajectories start
  trajectories.numTrajectories = 3;
  trajectories.xtrajectory.tVec = [0; tFinal/2; tFinal];
  trajectories.xtrajectory.wayPoints = [ 0; 1; 1];
  trajectories.xtrajectory.velocities = [0; 0; 0];
  trajectories.xtrajectory.accelerations = [0; 0; 0];
  trajectories.ytrajectory.tVec = [0; tFinal/2; tFinal];
  trajectories.ytrajectory.wayPoints = [ 0; 1; 1];
  trajectories.ytrajectory.velocities = [0; 0; 0];
  trajectories.ytrajectory.accelerations = [0; 0; 0];
  trajectories.ztrajectory.tVec = [0; tFinal/2;  tFinal];
  trajectories.ztrajectory.wayPoints = [ 0; 5; 5];
  trajectories.ztrajectory.velocities = [0; 0; 0];
  trajectories.ztrajectory.accelerations = [0; 0; 0];
  trajectories.qBxtrajectory.tVec = [0; tFinal/2;  tFinal];
  trajectories.qBxtrajectory.wayPoints = [ 0; 5; 5];
  trajectories.qBxtrajectory.velocities = [0; 0; 0];
  trajectories.qBxtrajectory.accelerations = [0; 0; 0];
  
elseif strcmp(trajectoryType, 'sine')
  tStep = .01; 
  tInitial = 0;
  tFinal = 30;
  t = (0:tStep:tFinal)';
  omega = 45*pi/180;
  trajectories.type = trajectoryType;
  trajectories.omega = omega;
end

%% Uncontrolled Dynamics 
params.mode = 'none';
setPoint = xDesired;
trajectoryFuncHandle = @(trajectories, t) GenerateTrajectoryPoint(trajectories, t);
u = @(t, states) quadRotorRatesController(t, states, trajectories, trajectoryFuncHandle, params);
odeFunc = @(t,states) quadRotorRatesDynamics(t, states, params, u);
[tUncon, xUncon] = ode45(odeFunc, tSpan, x0, opts);

%% Constant Controller

%  u = @(states) ();
%  odeFunc = @(t,states) quadRotorRatesDynamics(t, states, params, u);
%  [tUncon, xUncon] = ode45(odeFunc, tSpan, x0);


%% LQR Controller 
disp('Beginning simulation with LQR Control');
load('Klqr.mat')
load('uEq.mat')
params.mode = 'LQR';
params.Klqr = Klqr;
params.uEq = uEqVal;
statesDesired = xDesired;
trajectoryFuncHandle = @(trajectories, t) GenerateTrajectoryPoint(trajectories, t);
u = @(t, states) quadRotorRatesController(t, states, trajectories, trajectoryFuncHandle, params);

odeFunc = @(t,states) quadRotorRatesDynamics(t, states, params, u);
[tLQR, xLQR] = ode45(odeFunc, tSpan, x0);

numPoints = length(tLQR);
propRatesLQR = zeros(numPoints, 4);
thrustLQR = zeros(numPoints, 4);
for i = 1 :  numPoints
    [~, propRatesLQR(i,:), thrustLQR(i,:)] = quadRotorRatesDynamics(tLQR(i), xLQR(i,:)', params, u);
end
xDesiredMat = repmat(xDesired', size(xLQR,1),1);
xErrorLQR = xLQR - xDesiredMat;
drawStatesLQR= xLQR(:,:);

%save('rates_rates_hover.mat', 'propRates')
%% Plot Responses
figure
hold on 
for i = 1: 6
  subplot(6,1,i)
  %plot(tUncon, xUncon(:,i), 'k', 'linewidth', 2)
  hold on
  plot(tLQR, xLQR(:,i), 'r--', 'linewidth', 2)
%legend('Uncontrolled', 'Controlled')
end
figure
 plot(tLQR, xErrorLQR, 'linewidth', 1)

figure 
plot(tLQR, propRatesLQR)
title('Propellor Rate Time Histories')


figure 
plot(tLQR, thrustLQR)
title('Thrust Force Time Histories')

%% Animate Responses
if animate ==1
params.videoName = videoName;
params.xDesired = xDesired;    
params.additionalPlots = 3;
params.additionalData{1} = propRatesLQR;
params.additionalData{2} = thrustLQR;
params.additionalData{3} = xErrorLQR;
params.additionalTitles{1} = 'Propellor Rates';
params.additionalTitles{2} = 'Thrust Forces';
params.additionalTitles{3} = 'State Error';
quadRotorRatesAnimation(tLQR,drawStatesLQR,params,saveVideo)
end


