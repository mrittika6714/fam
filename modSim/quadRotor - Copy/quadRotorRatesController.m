function control = quadRotorThrustController(t, states, trajectories, trajectoryFunc, params)

mode = params.mode;
g = params.g;
[posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
statesDesired =  [ 5; 8; 15; 0; 0; 0; 0; 0; 0; 0; 0; 0];
if strcmp(mode, 'none')
    control = zeros(length(states), 1);
    
elseif strcmp(mode, 'constant')
   control = params.value;

elseif strcmp(mode, 'sin')
   control = ones(lengthStates, 1) * sin(t);

elseif strcmp(mode, 'LQR')
%  xBDes = statesDesired(1) = posDes(1);
%  yBDes = statesDesired(2) = posDes(2);
%  zBDes = statesDesiredt(3) =  posDes(3);
%  qBxDes = statesDesired(4);
%  qByDes = statesDesired(5);
%  qBzDes = statesDesired(6)=  posDes(4);
%  xBDtDes = statesDesired(7);
%  yBDtDes = statesDesired(8);
%  zBDtDes = statesDesired(9); 

%wBDes = qBDotTowB( qBxDes, qBxDotDes, qByDes, qByDotDes, qBzDotDes );
%  wBxDes = statesDesired(10) = wBDes(1);
%  wByDes = statesDesired(11)= wBDes(2);
%  wBzDes = statesDesired(12)= wBDes(3);

  Klqr = params.Klqr;
  uEq = params.uEq;
  yDes = posDes(1);
  yDotDes = vDes(1);
  yDDotDes = aDes(1);
  zDes = posDes(2);
  zDotDes = vDes(2);
  zDDotDes = aDes(2);
  qBxCalc = -yDDotDes/(g+zDDotDes) ;
  setPoint = [ yDes; zDes; qBxCalc ; yDotDes; zDotDes; 0 ];
  control = -Klqr*(states - statesDesired) + uEq;
  
  %% Nonlinear PID 
elseif strcmp(mode, 'PID')
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
  yDes = posDes(1);
  yDotDes = vDes(1);
  yDDotDes = aDes(1);
  zDes = posDes(2);
  zDotDes = vDes(2);
  zDDotDes = aDes(2);
  Kpid= params.Kpid;
  y = states(1);
  z = states(2);
  kpz = 80;
  kdz = 20;
  kiz = 0;
  kpy = 20;
  kdy = 5;
  kiy = 0;
  kdq = 10;
  kpq = 1000;
  kiq = 0;
  qBx = states(3);
  yDot = states(4);
  zDot = states(5); 
  qBxDot = states(6);
  qBxDes = -yDDotDes/(g+zDDotDes);
  
  
   currentError = [yDes - y, zDes - z, qBxDes - qBx];
   errorSum = [ 0 0 0];
   if isempty(errorSum)
     errorSum = [0 0 0];
   else
     errorSum = errorSum+currentError;
   end
  
  zDDotCon = zDDotDes + kdz*(zDotDes - zDot) +kpz * (zDes - z) + kiz*errorSum(2);
  yDDotCon = yDDotDes + kdy*(yDotDes - yDot) +kpy * (yDes - y) + kiy*errorSum(1);
  qBxCalc = -atan(yDDotCon/(g+zDDotCon)) ;
  zDDDotDes = 0;
  yDDDotDes = 0;
  qBxDotCalc= 0;
  qBxDDotCalc = 0;
  qBxDDotCon = qBxDDotCalc+kpq*(qBxCalc - qBx) + kdq*(qBxDotCalc - qBxDot) + kiq*errorSum(3);

  F2 = 0.5*(qBxDDotCon * IBxx)/lA  + 0.5*(mB*(zDDotCon+g))/cos(qBxCalc);
  F4 = 0.5*(mB*(zDDotCon+g))/cos(qBxCalc) - 0.5*(qBxDDotCon * IBxx)/lA ; 

  control = [F2; F4] ;
  
end    

end