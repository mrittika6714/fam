function [SolutionToAlgebraicEquations,Output] = quadRotorRatesSimpleMatlab( xBDt, yBDt, zBDt, qBx, qBy, qBz, wBx, wBy, wBz, bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, mProp, IPropzz, q1Dt, q2Dt, q3Dt, q4Dt )
if( nargin ~= 25 ) error( 'quadRotorRatesSimpleMatlab expects 25 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotorRatesSimpleMatlab.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================
qBxDt = wBx*cos(qBy) + wBz*sin(qBy);
qByDt = wBy + tan(qBx)*(wBx*sin(qBy)-wBz*cos(qBy));
qBzDt = -(wBx*sin(qBy)-wBz*cos(qBy))/cos(qBx);
F1 = bThrust*q1Dt^2;
F2 = bThrust*q2Dt^2;
F3 = bThrust*q3Dt^2;
F4 = bThrust*q4Dt^2;
T1 = bDrag*F1/bThrust;
T2 = -bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = -bDrag*F4/bThrust;

COEF = zeros( 6, 6 );
COEF(1,1) = mB + 4*mProp;
COEF(2,2) = mB + 4*mProp;
COEF(3,3) = mB + 4*mProp;
COEF(4,4) = IBxx + 2*mProp*lA^2;
COEF(4,5) = IBxy;
COEF(4,6) = IBzx;
COEF(5,4) = IBxy;
COEF(5,5) = IByy + 2*mProp*lA^2;
COEF(5,6) = IByz;
COEF(6,4) = IBzx;
COEF(6,5) = IByz;
COEF(6,6) = IBzz + 4*IPropzz + 4*mProp*lA^2;
RHS = zeros( 1, 6 );
RHS(1) = (F1+F2+F3+F4)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy));
RHS(2) = (F1+F2+F3+F4)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz));
RHS(3) = F1*cos(qBx)*cos(qBy) + F2*cos(qBx)*cos(qBy) + F3*cos(qBx)*cos(qBy) + F4*cos(qBx)*cos(qBy) - 39.2*mProp - 9.8*mB;
RHS(4) = lA*(F2-F4) + wBz*(IBxy*wBx+IByy*wBy+IByz*wBz) - 2*mProp*lA^2*wBy*wBz - wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz) - IPropzz*q2Dt*(  ...
qByDt+sin(qBx)*qBzDt) - IPropzz*q4Dt*(qByDt+sin(qBx)*qBzDt) - IPropzz*(qByDt+sin(qBx)*qBzDt)*(q1Dt+4*sin(qBy)*qBxDt) - IPropzz*(  ...
qByDt+sin(qBx)*qBzDt)*(q3Dt+4*cos(qBx)*cos(qBy)*qBzDt);
RHS(5) = 2*mProp*lA^2*wBx*wBz + wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz) + IPropzz*q2Dt*(cos(qBy)*qBxDt-sin(qBy)*cos(qBx)*qBzDt)  ...
+ IPropzz*q4Dt*(cos(qBy)*qBxDt-sin(qBy)*cos(qBx)*qBzDt) + IPropzz*(q1Dt+4*sin(qBy)*qBxDt)*(cos(qBy)*qBxDt-sin(qBy)*cos(qBx)*  ...
qBzDt) + IPropzz*(q3Dt+4*cos(qBx)*cos(qBy)*qBzDt)*(cos(qBy)*qBxDt-sin(qBy)*cos(qBx)*qBzDt) - lA*(F1-F3) - wBz*(IBxx*wBx+IBxy*wBy+  ...
IBzx*wBz);
RHS(6) = T1 + T2 + T3 + T4 + wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz) - wBx*(IBxy*wBx+IByy*wBy+IByz*wBz);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
zBDDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);



%===========================================================================
Output = zeros( 1, 12 );

Output(1) = xBDt;
Output(2) = yBDt;
Output(3) = zBDt;
Output(4) = qBxDt;
Output(5) = qByDt;
Output(6) = qBzDt;

Output(7) = xBDDt;
Output(8) = yBDDt;
Output(9) = zBDDt;
Output(10) = wBxDt;
Output(11) = wByDt;
Output(12) = wBzDt;

%===================================================
end    % End of function quadRotorRatesSimpleMatlab
%===================================================
