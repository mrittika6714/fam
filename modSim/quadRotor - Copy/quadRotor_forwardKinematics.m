function [taskPos, taskVel] = quadRotor_forwardKinematics( xB, yB, zB, qBx, qBy, qBz, vBx, vBy, vBz, wBx, wBy, wBz )
if( nargin ~= 12 ) error( 'quadRotor_forwardKinematics expects 12 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotor_forwardKinematics.m created Nov 07 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
taskPos = zeros( 4, 1 );
taskVel = zeros( 4, 1 );
z = zeros( 1, 932 );



%===========================================================================
z(1) = cos(qBy);
z(5) = sin(qBz);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(9) = z(4)*z(8);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(226) = z(7)*z(14) + z(9)*z(13);
z(11) = z(2)*z(8);
z(223) = z(11)*z(14) - z(3)*z(13);
z(221) = z(3)*z(7) + z(9)*z(11);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(10) = z(5)*z(8);
z(220) = z(3)*z(6) - z(9)*z(10);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(219) = z(6)*z(11) + z(7)*z(10);
z(222) = z(13)*z(220) - z(12)*z(221) - z(14)*z(219);
z(229) = (vBy*z(226)-vBx*z(223)-vBz*z(221))/z(222);
xBDt = z(229);
z(227) = z(6)*z(14) + z(9)*z(12);
z(224) = -z(3)*z(12) - z(10)*z(14);
z(230) = (vBy*z(227)-vBx*z(224)-vBz*z(220))/z(222);
yBDt = -z(230);
z(228) = z(6)*z(13) - z(7)*z(12);
z(225) = -z(10)*z(13) - z(11)*z(12);
z(231) = (vBy*z(228)-vBx*z(225)-vBz*z(219))/z(222);
zBDt = z(231);
z(183) = z(1)*z(14) + z(4)*z(9);
z(185) = (wBx*z(4)-wBz*z(1))/z(183);
qBzDt = -z(185);



%===========================================================================
Output = [];

taskPos(1) = xB;
taskPos(2) = yB;
taskPos(3) = zB;
taskPos(4) = qBz;

taskVel(1) = xBDt;
taskVel(2) = yBDt;
taskVel(3) = zBDt;
taskVel(4) = qBzDt;


%====================================================
end    % End of function quadRotor_forwardKinematics
%====================================================
