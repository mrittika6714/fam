function [SolutionToAlgebraicEquations] = quadRotor_calc_alphaB_N_B( qBx, qBy, qBz, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz )
if( nargin ~= 9 ) error( 'quadRotor_calc_alphaB_N_B expects 9 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotor_calc_alphaB_N_B.m created Nov 19 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 965 );



%===========================================================================
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(183) = z(1)*z(14) + z(4)*z(9);
z(184) = (wBx*z(14)+wBz*z(9))/z(183);
qBxDt = z(184);
z(3) = sin(qBx);
z(185) = (wBx*z(4)-wBz*z(1))/z(183);
z(186) = -wBy - z(3)*z(185);
qByDt = -z(186);
qBzDt = -z(185);
z(18) = z(1)*z(8)*qByDt - z(3)*z(4)*qBxDt;
z(20) = z(8)*qBxDt*qBzDt;
z(21) = -z(1)*z(3)*qBxDt - z(4)*z(8)*qByDt;
z(187) = z(4)*qBxDt*qByDt + qBzDt*z(18);
z(190) = -z(1)*qBxDt*qByDt - qBzDt*z(21);
z(957) = z(14)/z(183);
z(958) = z(9)/z(183);
z(959) = qBxDDot - (z(9)*z(190)+z(14)*z(187))/z(183);
z(960) = z(3)*z(4)/z(183);
z(961) = z(1)*z(3)/z(183);
z(962) = qByDDot + z(20) + z(3)*(z(1)*z(190)-z(4)*z(187))/z(183);
z(963) = z(4)/z(183);
z(964) = z(1)/z(183);
z(965) = qBzDDot - (z(1)*z(190)-z(4)*z(187))/z(183);

COEF = zeros( 3, 3 );
COEF(1,1) = z(957);
COEF(1,3) = z(958);
COEF(2,1) = z(960);
COEF(2,2) = 1;
COEF(2,3) = -z(961);
COEF(3,1) = -z(963);
COEF(3,3) = z(964);
RHS = zeros( 1, 3 );
RHS(1) = z(959);
RHS(2) = z(962);
RHS(3) = z(965);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBxDt = SolutionToAlgebraicEquations(1);
wByDt = SolutionToAlgebraicEquations(2);
wBzDt = SolutionToAlgebraicEquations(3);



%==================================================
end    % End of function quadRotor_calc_alphaB_N_B
%==================================================
