function [statesDot, propRates, thrust] = quadRotorRatesDynamics(t, states, params, inputFunc)
u = inputFunc(t, states);
q1Dt = u(1);
q2Dt = u(2);
q3Dt = u(3);
q4Dt = u(4);
mB = params.mB;
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
xBDt = states(7);
yBDt = states(8);
zBDt = states(9); 
wBx = states(10);
wBy = states(11);
wBz = states(12);
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBzx = params.IB(3,1);
lA = params.lA;
bThrust = params.bThrust;
bDrag = params.bDrag;
IPropxx = params.IProp(1,1);
IPropyy = params.IProp(2,2);
IPropzz = params.IProp(3,3);
mProp = params.mProp;
propRates = [q1Dt, q2Dt, q3Dt, q4Dt];
thrust = rates2Thrust( q1Dt, q2Dt, q3Dt, q4Dt, bThrust );

[~, Output] = quadRotorRatesSimpleMatlab( xBDt, yBDt, zBDt, qBx, qBy, qBz,...
  wBx, wBy, wBz, bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz,...
  IBzx, mProp, IPropzz, q1Dt, q2Dt, q3Dt, q4Dt );
%extraOutput = planarQuadRotorAnimationOutputs( lA, qBx, y, z );

statesDot = Output';
end