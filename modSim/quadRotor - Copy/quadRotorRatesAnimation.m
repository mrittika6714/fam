%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function quadRotorRatesAnimation(t, states,params,saveVideo)

numPlots = params.additionalPlots + 1;
numRows = max(params.additionalPlots,1);
if numPlots > 1
  numCols = 2;
else
  numCols =1;
end
% Unpack positions
xB= states(:,1);
yB= states(:,2);
zB= states(:,3);
qBx= states(:,4);
qBy= states(:,5);
qBz= states(:,6);
setPoint = params.xDesired;
lA = params.lA;
bHeight = lA/2;
numPoints = length(t);

[bodyVertices, bodyFaces] =  plotCube([lA, lA, bHeight],[xB(1), yB(1), zB(1)], [qBz(1), qBy(1), qBx(1)]);
if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = 100;
  open(myVideo)
end
animationFigure = figure('units','normalized','outerposition',[0 0 1 1]);
for i = 1: params.additionalPlots
  subplot(numRows,numCols,i*2);
  plot(t, params.additionalData{i});
  hold on
  limits = [t(1) t(end) ylim*1.1];
  axis(limits)
  timeMark{i} = [ limits(1), limits(3); limits(1), limits(4)] ;
  timeMarkPlot{i} = plot(timeMark{i}(:,1),timeMark{i}(:,2), '-k', 'linewidth', 2);
  title(params.additionalTitles{i})
end



subplot(numRows,numCols,1:2:numRows*numCols)
setPointPlot = plot3(setPoint(1),setPoint(2),setPoint(3), 'r.','markersize',25);
%view(0,90)
hold on
bodyPlot = patch('Vertices',bodyVertices,'Faces',bodyFaces, 'FaceColor', [0.5, 0.5, 0.5]);
pathPlot = plot3(xB(1), yB(1), zB(1), 'k--', 'linewidth',1);
xMin = min( xB) - lA*2.5;
xMax = max( xB) + lA*2.5;
yMin = min( yB) - lA*2.5;
yMax = max( yB) + lA*2.5;
zMin = min( zB) - lA*2.5;
zMax = max( zB) + lA*2.5;
axis([xMin xMax yMin yMax zMin zMax])
xlabel('x Position [m]');
ylabel('y Position [m]');
zlabel('z Position [m]');
legend('Set Point', 'VehicleBody')
for i =1:numPoints
    setPointPlot.XData = setPoint(1);
    setPointPlot.YData = setPoint(2);
    setPointPlot.ZData = setPoint(3);
    %bodyPlot.XData = xB(i);
    %bodyPlot.YData = yB(i);
    %bodyPlot.ZData = zB(i);
    
    [ bodyPlot.Vertices, bodyPlot.Faces] =  plotCube([lA, lA, bHeight],[xB(i), yB(i), zB(i)], [qBz(i), qBy(i), qBx(i)]);
    
    
    pathPlot.XData = xB(1:i);
    pathPlot.YData = yB(1:i);
    pathPlot.ZData = zB(1:i);
    
    for ii = 1:params.additionalPlots
      timeMarkPlot{ii}.XData = [t(i); t(i)];
    end
    drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end