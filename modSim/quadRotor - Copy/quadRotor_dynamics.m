function [statesDot, additonalOutputs] = quadRotor_dynamics(t, states, params, controllerFunc)

% Unpack parameters
mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPxx = params.IP(1,1);
IPyy = params.IP(2,2);
IPzz = params.IP(3,3);
IPxy = params.IP(1,2);
IPyz = params.IP(2,3);
IPxz = params.IP(1,3);
bThrust = params.bThrust;
bDrag = params.bDrag;
g = params.g;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);


xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(2);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(2);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(2);


% Set Propellor tilt angles to be stativ
qP1xDt = 0;
qP1yDt = 0;
qP2xDt = 0;
qP2yDt = 0;
qP3xDt = 0;
qP3yDt = 0;
qP4xDt = 0;
qP4yDt = 0;
qP1xDDt = 0;
qP1yDDt = 0;
qP2xDDt = 0;
qP2yDDt = 0;
qP3xDDt = 0;
qP3yDDt = 0;
qP4xDDt = 0;
qP4yDDt = 0;

% Determine Desired inputs
u = controllerFunc(t, states);

% Unpack Inputs
qP1zDt = u(1);
qP2zDt = u(2);
qP3zDt = u(3);
qP4zDt = u(4);


% Assume rotor accelerations negligible
qP1zDDt = 0;
qP2zDDt = 0;
qP3zDDt = 0;
qP4zDDt = 0;


%Unpack States
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
vBx = states(7);
vBy = states(8);
vBz = states(9);
wBx = states(10);
wBy = states(11);
wBz = states(12);
qP1z = states(13);
qP2z = states(14);
qP3z = states(15);
qP4z = states(16);





[~, bodyStatesDot] = quadRotor_MGdynamics( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPxx, IPzz, IPyy, IPxy, IPyz, IPxz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP1xDt, qP1yDt, qP2xDt, qP2yDt, qP3xDt, qP3yDt, qP4xDt, qP4yDt, qP1xDDt, qP1yDDt, qP2xDDt, qP2yDDt, qP3xDDt, qP3yDDt, qP4xDDt, qP4yDDt, qBx, qBy, qBz, qP1z, qP2z, qP3z, qP4z, vBx, vBy, vBz, wBx, wBy, wBz, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP1zDDt, qP2zDDt, qP3zDDt, qP4zDDt );

propStatesDot = [qP1zDt; qP2zDt; qP3zDt; qP4zDt];

[rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, ...
          N_R_P1, N_R_P2, N_R_P3, N_R_P4, N_R_B  ] = ...
          quadRotor_MGanimationOutputs( xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z );

propPositions = [rP1_No_N; rP2_No_N; rP3_No_N; rP4_No_N;]';

qP1_ZYX = rotm2eul(N_R_P1, 'ZYX');        
qP2_ZYX = rotm2eul(N_R_P2, 'ZYX');  
qP3_ZYX = rotm2eul(N_R_P3, 'ZYX');  
qP4_ZYX = rotm2eul(N_R_P4, 'ZYX');  
qB_ZYX = rotm2eul(N_R_B, 'ZYX');

bodyOrientations = [qP1_ZYX, qP2_ZYX, qP3_ZYX, qP4_ZYX, qB_ZYX];
animationOutputs = [propPositions, bodyOrientations];
additonalOutputs.animationOutputs = animationOutputs;

statesDot = [bodyStatesDot'; propStatesDot];

end