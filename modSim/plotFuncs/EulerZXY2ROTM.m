function N_R_B = EulerZXY2ROTM( qBx, qBy, qBz )
if( nargin ~= 3 ) error( 'EulerZXY2ROTM expects 3 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: EulerZXY2ROTM.m created Oct 28 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
N_R_B = zeros( 3, 3 );
z = zeros( 1, 14 );



%===========================================================================
z(1) = cos(qBy);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(5) = sin(qBz);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(14) = z(1)*z(8);



%===========================================================================
Output = [];

N_R_B(1,1) = z(6);
N_R_B(1,2) = z(7);
N_R_B(1,3) = -z(9);
N_R_B(2,1) = -z(10);
N_R_B(2,2) = z(11);
N_R_B(2,3) = z(3);
N_R_B(3,1) = z(12);
N_R_B(3,2) = z(13);
N_R_B(3,3) = z(14);


%======================================
end    % End of function EulerZXY2ROTM
%======================================
