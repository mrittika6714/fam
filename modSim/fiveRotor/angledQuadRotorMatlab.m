function [SolutionToAlgebraicEquations,Output] = angledQuadRotorMatlab( F1, F2, F3, F4, xBDt, yBDt, zBDt, qBx, qBy, qBz, wBx, wBy, wBz, bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y )
if( nargin ~= 31 ) error( 'angledQuadRotorMatlab expects 31 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: angledQuadRotorMatlab.m created Nov 11 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================


%-------------------------------+--------------------------+-------------------+-----------------
% Quantity                      | Value                    | Units             | Description
%-------------------------------|--------------------------|-------------------|-----------------
g                               =  9.8;                    % m/s^2               Constant
%-------------------------------+--------------------------+-------------------+-----------------



%===========================================================================
T1 = bDrag*F1/bThrust;
T2 = -bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = -bDrag*F4/bThrust;

COEF = zeros( 6, 6 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = mB;
COEF(4,4) = IBxx;
COEF(4,5) = IBxy;
COEF(4,6) = IBzx;
COEF(5,4) = IBxy;
COEF(5,5) = IByy;
COEF(5,6) = IByz;
COEF(6,4) = IBzx;
COEF(6,5) = IByz;
COEF(6,6) = IBzz;
RHS = zeros( 1, 6 );
RHS(1) = F1*(sin(qP1x)*cos(qP1y)*sin(qBz)*cos(qBx)+sin(qP1y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP1x)*cos(qP1y)*(  ...
sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))) + F2*(sin(qP2x)*cos(qP2y)*sin(qBz)*cos(qBx)+sin(qP2y)*(cos(qBy)*cos(qBz)-sin(qBx)*  ...
sin(qBy)*sin(qBz))+cos(qP2x)*cos(qP2y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))) + F3*(sin(qP3x)*cos(qP3y)*sin(qBz)*cos(qBx)+  ...
sin(qP3y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP3x)*cos(qP3y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))  ...
+ F4*(sin(qP4x)*cos(qP4y)*sin(qBz)*cos(qBx)+sin(qP4y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP4x)*cos(qP4y)*(sin(qBy)*  ...
cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)));
RHS(2) = -F1*(sin(qP1x)*cos(qP1y)*cos(qBx)*cos(qBz)-sin(qP1y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP1x)*cos(qP1y)*(  ...
sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))) - F2*(sin(qP2x)*cos(qP2y)*cos(qBx)*cos(qBz)-sin(qP2y)*(sin(qBz)*cos(qBy)+sin(qBx)*  ...
sin(qBy)*cos(qBz))-cos(qP2x)*cos(qP2y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))) - F3*(sin(qP3x)*cos(qP3y)*cos(qBx)*cos(qBz)-  ...
sin(qP3y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP3x)*cos(qP3y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))  ...
- F4*(sin(qP4x)*cos(qP4y)*cos(qBx)*cos(qBz)-sin(qP4y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP4x)*cos(qP4y)*(sin(qBy)*  ...
sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)));
RHS(3) = -g*mB - F1*(sin(qP1x)*cos(qP1y)*sin(qBx)+sin(qP1y)*sin(qBy)*cos(qBx)-cos(qP1x)*cos(qP1y)*cos(qBx)*cos(qBy)) - F2*(sin(qP2x)*  ...
cos(qP2y)*sin(qBx)+sin(qP2y)*sin(qBy)*cos(qBx)-cos(qP2x)*cos(qP2y)*cos(qBx)*cos(qBy)) - F3*(sin(qP3x)*cos(qP3y)*sin(qBx)+sin(qP3y)*  ...
sin(qBy)*cos(qBx)-cos(qP3x)*cos(qP3y)*cos(qBx)*cos(qBy)) - F4*(sin(qP4x)*cos(qP4y)*sin(qBx)+sin(qP4y)*sin(qBy)*cos(qBx)-cos(qP4x)*  ...
cos(qP4y)*cos(qBx)*cos(qBy));
RHS(4) = lA*(cos(qP2x)*cos(qP2y)*F2-cos(qP4x)*cos(qP4y)*F4) + wBz*(IBxy*wBx+IByy*wBy+IByz*wBz) - wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz);
RHS(5) = wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz) - lA*(cos(qP1x)*cos(qP1y)*F1-cos(qP3x)*cos(qP3y)*F3) - wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz);
RHS(6) = lA*sin(qP4y)*F4 + lA*sin(qP3x)*cos(qP3y)*F3 + T1 + T2 + T3 + T4 + wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz) - lA*sin(qP2y)*F2  ...
- lA*sin(qP1x)*cos(qP1y)*F1 - wBx*(IBxy*wBx+IByy*wBy+IByz*wBz);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
xBDDt = SolutionToAlgebraicEquations(1);
yBDDt = SolutionToAlgebraicEquations(2);
zBDDt = SolutionToAlgebraicEquations(3);
wBxDt = SolutionToAlgebraicEquations(4);
wByDt = SolutionToAlgebraicEquations(5);
wBzDt = SolutionToAlgebraicEquations(6);

qBxDt = wBx*cos(qBy) + wBz*sin(qBy);
qByDt = wBy + tan(qBx)*(wBx*sin(qBy)-wBz*cos(qBy));
qBzDt = -(wBx*sin(qBy)-wBz*cos(qBy))/cos(qBx);



%===========================================================================
Output = zeros( 1, 12 );

Output(1) = xBDt;
Output(2) = yBDt;
Output(3) = zBDt;
Output(4) = qBxDt;
Output(5) = qByDt;
Output(6) = qBzDt;

Output(7) = xBDDt;
Output(8) = yBDDt;
Output(9) = zBDDt;
Output(10) = wBxDt;
Output(11) = wByDt;
Output(12) = wBzDt;

%==============================================
end    % End of function angledQuadRotorMatlab
%==============================================
