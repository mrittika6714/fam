function [statesDot, propRates] = angledQuadRotorDynamics(~, states, params, inputFunc)
u = inputFunc(states);
F1 = u(1);
F2 = u(2);
F3 = u(3);
F4 = u(4);
mB = params.mB;
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
xBDt = states(7);
yBDt = states(8);
zBDt = states(9); 
wBx = states(10);
wBy = states(11);
wBz = states(12);
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBzx = params.IB(3,1);
lA = params.lA;
bThrust = params.bThrust;
bDrag = params.bDrag;
qP1x = params.qP1x;
qP1y = params.qP1y;
qP2x = params.qP2x;
qP2y = params.qP2y;
qP3x = params.qP3x;
qP3y = params.qP3y;
qP4x = params.qP4x;
qP4y = params.qP4y;


[~, Output] = angledQuadRotorMatlab( F1, F2, F3, F4, xBDt, yBDt, zBDt, qBx, qBy, qBz, wBx, wBy, wBz, bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y );
%extraOutput = planarQuadRotorAnimationOutputs( lA, qBx, y, z );
propRates = thrust2rates( F1, F2, F3, F4, bThrust );
statesDot = Output';
end