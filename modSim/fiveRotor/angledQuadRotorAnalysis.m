%% Analysis of the planarQuadRotor
clc;clear;close all
% Define System
gcmsq2gmsq = 10;
g2kg = 0.001;

params.IB(1,1) = 0.0117; %kg*m^2% ;
params.IB(2,2)= 0.0117; %kg*m^2;
params.IB(3,3) = 0.00234; %kg*m^2
params.IB(1,2) = 0;%
params.IB(2,3) = 0;%
params.IB(3,1) = 0;%
params.mB = 0.478;
params.g = 9.8; 
params.lA = 0.18;
params.bThrust = 6.11*10^-8;
params.bDrag =  1.5*10^-9;
params.qP1x = 45*pi/180;
params.qP1y = 0*pi/180;
params.qP2x = 0*pi/180;
params.qP2y = -45*pi/180;
params.qP3x = -45*pi/180;
params.qP3y = 0*pi/180;
params.qP4x = 0*pi/180;
params.qP4y = 45*pi/180;

syms xB yB zB qBx qBy qBz xBDot yBDot zBDot qBxDot qByDot qBZDot mB F1 F2 F3 F4 IBxx IByy IBzz IBxy IByz IBzx g wBx wBy wBz lA bDrag bThrust real
syms qP1x qP1y qP2x qP2y qP3x qP3y qP4x qP4y real
T = [F1; F2; F3; F4];
x = [xB; yB;  zB;  qBx; qBy; qBz; xBDot; yBDot; zBDot;  wBx; wBy; wBz;];

% Get from Motion Genesis
F = zeros(12,1);
F = sym(F);
F(1) = xBDot;
F(2) = yBDot;
F(3) = zBDot;
F(4) = sin(qBy)*wBz + cos(qBy)*wBx;
F(5) = wBy + tan(qBx)*(sin(qBy)*wBx-cos(qBy)*wBz);
F(6) = -(sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx);
F(7) = (F1*(sin(qP1x)*cos(qP1y)*sin(qBz)*cos(qBx)+sin(qP1y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP1x)*cos(qP1y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))+F2*(sin(qP2x)*cos(qP2y)*sin(qBz)*cos(qBx)+sin(qP2y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP2x)*cos(qP2y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))+F3*(sin(qP3x)*cos(qP3y)*sin(qBz)*cos(qBx)+sin(qP3y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP3x)*cos(qP3y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))+F4*(sin(qP4x)*cos(qP4y)*sin(qBz)*cos(qBx)+sin(qP4y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP4x)*cos(qP4y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))))/mB;
F(8) = -(F1*(sin(qP1x)*cos(qP1y)*cos(qBx)*cos(qBz)-sin(qP1y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP1x)*cos(qP1y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))+F2*(sin(qP2x)*cos(qP2y)*cos(qBx)*cos(qBz)-sin(qP2y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP2x)*cos(qP2y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))+F3*(sin(qP3x)*cos(qP3y)*cos(qBx)*cos(qBz)-sin(qP3y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP3x)*cos(qP3y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))+F4*(sin(qP4x)*cos(qP4y)*cos(qBx)*cos(qBz)-sin(qP4y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP4x)*cos(qP4y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))))/mB;
F(9) = -g - (F1*(sin(qP1x)*cos(qP1y)*sin(qBx)+sin(qP1y)*sin(qBy)*cos(qBx)-cos(qP1x)*cos(qP1y)*cos(qBx)*cos(qBy))+F2*(sin(qP2x)*cos(qP2y)*sin(qBx)+sin(qP2y)*sin(qBy)*cos(qBx)-cos(qP2x)*cos(qP2y)*cos(qBx)*cos(qBy))+F3*(sin(qP3x)*cos(qP3y)*sin(qBx)+sin(qP3y)*sin(qBy)*cos(qBx)-cos(qP3x)*cos(qP3y)*cos(qBx)*cos(qBy))+F4*(sin(qP4x)*cos(qP4y)*sin(qBx)+sin(qP4y)*sin(qBy)*cos(qBx)-cos(qP4x)*cos(qP4y)*cos(qBx)*cos(qBy)))/mB;
F(10) = ((IBxy*IBzz-IByz*IBzx)*(wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)-lA*(cos(qP1x)*cos(qP1y)*F1-cos(qP3x)*cos(qP3y)*F3)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz))+(IByy*IBzz-IByz^2)*(wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)-lA*(cos(qP2x)*cos(qP2y)*F2-cos(qP4x)*cos(qP4y)*F4)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz))+(IBxy*IByz-IByy*IBzx)*(lA*sin(qP2y)*F2+lA*sin(qP1x)*cos(qP1y)*F1+wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-lA*sin(qP4y)*F4-lA*sin(qP3x)*cos(qP3y)*F3-bDrag*(F1+F3-F2-F4)/bThrust-wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
F(11) = -((IBxx*IBzz-IBzx^2)*(wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)-lA*(cos(qP1x)*cos(qP1y)*F1-cos(qP3x)*cos(qP3y)*F3)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz))+(IBxy*IBzz-IByz*IBzx)*(wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)-lA*(cos(qP2x)*cos(qP2y)*F2-cos(qP4x)*cos(qP4y)*F4)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz))+(IBxx*IByz-IBxy*IBzx)*(lA*sin(qP2y)*F2+lA*sin(qP1x)*cos(qP1y)*F1+wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-lA*sin(qP4y)*F4-lA*sin(qP3x)*cos(qP3y)*F3-bDrag*(F1+F3-F2-F4)/bThrust-wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
F(12) = ((IBxx*IByz-IBxy*IBzx)*(wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)-lA*(cos(qP1x)*cos(qP1y)*F1-cos(qP3x)*cos(qP3y)*F3)-wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz))+(IBxy*IByz-IByy*IBzx)*(wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)-lA*(cos(qP2x)*cos(qP2y)*F2-cos(qP4x)*cos(qP4y)*F4)-wBz*(IBxy*wBx+IByy*wBy+IByz*wBz))+(IBxx*IByy-IBxy^2)*(lA*sin(qP2y)*F2+lA*sin(qP1x)*cos(qP1y)*F1+wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-lA*sin(qP4y)*F4-lA*sin(qP3x)*cos(qP3y)*F3-bDrag*(F1+F3-F2-F4)/bThrust-wBy*(IBxx*wBx+IBxy*wBy+IBzx*wBz)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));


symParams = [ mB; IBxx; IByy; IBzz; IBxy; IByz; IBzx; g; lA; bThrust; ...
    bDrag; qP1x; qP1y; qP2x; qP2y; qP3x; qP3y; qP4x; qP4y];
subParams = [ params.mB; params.IB(1,1); params.IB(2,2); params.IB(3,3);...
  params.IB(1,2); params.IB(2,3); params.IB(3,1); params.g; params.lA; params.bThrust; ...
  params.bDrag; params.qP1x; params.qP1y; params.qP2x; params.qP2y; params.qP3x;...
  params.qP3y; params.qP4x; params.qP4y];

% Perform analysis of linearized system
Df=simplify(jacobian(F, x')); 


J = zeros(12,12);
J = sym(J);
J(1,1) = 0;
J(1,2) = 0;
J(1,3) = 0;
J(1,4) = 0;
J(1,5) = 0;
J(1,6) = 0;
J(1,7) = 1;
J(1,8) = 0;
J(1,9) = 0;
J(1,10) = 0;
J(1,11) = 0;
J(1,12) = 0;
J(2,1) = 0;
J(2,2) = 0;
J(2,3) = 0;
J(2,4) = 0;
J(2,5) = 0;
J(2,6) = 0;
J(2,7) = 0;
J(2,8) = 1;
J(2,9) = 0;
J(2,10) = 0;
J(2,11) = 0;
J(2,12) = 0;
J(3,1) = 0;
J(3,2) = 0;
J(3,3) = 0;
J(3,4) = 0;
J(3,5) = 0;
J(3,6) = 0;
J(3,7) = 0;
J(3,8) = 0;
J(3,9) = 1;
J(3,10) = 0;
J(3,11) = 0;
J(3,12) = 0;
J(4,1) = 0;
J(4,2) = 0;
J(4,3) = 0;
J(4,4) = 0;
J(4,5) = cos(qBy)*wBz - sin(qBy)*wBx;
J(4,6) = 0;
J(4,7) = 0;
J(4,8) = 0;
J(4,9) = 0;
J(4,10) = cos(qBy);
J(4,11) = 0;
J(4,12) = sin(qBy);
J(5,1) = 0;
J(5,2) = 0;
J(5,3) = 0;
J(5,4) = (sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx)^2;
J(5,5) = tan(qBx)*(sin(qBy)*wBz+cos(qBy)*wBx);
J(5,6) = 0;
J(5,7) = 0;
J(5,8) = 0;
J(5,9) = 0;
J(5,10) = sin(qBy)*tan(qBx);
J(5,11) = 1;
J(5,12) = -cos(qBy)*tan(qBx);
J(6,1) = 0;
J(6,2) = 0;
J(6,3) = 0;
J(6,4) = -sin(qBx)*(sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx)^2;
J(6,5) = -(sin(qBy)*wBz+cos(qBy)*wBx)/cos(qBx);
J(6,6) = 0;
J(6,7) = 0;
J(6,8) = 0;
J(6,9) = 0;
J(6,10) = -sin(qBy)/cos(qBx);
J(6,11) = 0;
J(6,12) = cos(qBy)/cos(qBx);
J(7,1) = 0;
J(7,2) = 0;
J(7,3) = 0;
J(7,4) = -sin(qBz)*(F1*(sin(qP1x)*cos(qP1y)*sin(qBx)+sin(qP1y)*sin(qBy)*cos(qBx)-cos(qP1x)*cos(qP1y)*cos(qBx)*cos(qBy))+F2*(sin(qP2x)*cos(qP2y)*sin(qBx)+sin(qP2y)*sin(qBy)*cos(qBx)-cos(qP2x)*cos(qP2y)*cos(qBx)*cos(qBy))+F3*(sin(qP3x)*cos(qP3y)*sin(qBx)+sin(qP3y)*sin(qBy)*cos(qBx)-cos(qP3x)*cos(qP3y)*cos(qBx)*cos(qBy))+F4*(sin(qP4x)*cos(qP4y)*sin(qBx)+sin(qP4y)*sin(qBy)*cos(qBx)-cos(qP4x)*cos(qP4y)*cos(qBx)*cos(qBy)))/mB;
J(7,5) = -(F1*(sin(qP1y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))-cos(qP1x)*cos(qP1y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz)))+F2*(sin(qP2y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))-cos(qP2x)*cos(qP2y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz)))+F3*(sin(qP3y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))-cos(qP3x)*cos(qP3y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz)))+F4*(sin(qP4y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))-cos(qP4x)*cos(qP4y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))))/mB;
J(7,6) = (F1*(sin(qP1x)*cos(qP1y)*cos(qBx)*cos(qBz)-sin(qP1y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP1x)*cos(qP1y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))+F2*(sin(qP2x)*cos(qP2y)*cos(qBx)*cos(qBz)-sin(qP2y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP2x)*cos(qP2y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))+F3*(sin(qP3x)*cos(qP3y)*cos(qBx)*cos(qBz)-sin(qP3y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP3x)*cos(qP3y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))+F4*(sin(qP4x)*cos(qP4y)*cos(qBx)*cos(qBz)-sin(qP4y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP4x)*cos(qP4y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))))/mB;
J(7,7) = 0;
J(7,8) = 0;
J(7,9) = 0;
J(7,10) = 0;
J(7,11) = 0;
J(7,12) = 0;
J(8,1) = 0;
J(8,2) = 0;
J(8,3) = 0;
J(8,4) = cos(qBz)*(F1*(sin(qP1x)*cos(qP1y)*sin(qBx)+sin(qP1y)*sin(qBy)*cos(qBx)-cos(qP1x)*cos(qP1y)*cos(qBx)*cos(qBy))+F2*(sin(qP2x)*cos(qP2y)*sin(qBx)+sin(qP2y)*sin(qBy)*cos(qBx)-cos(qP2x)*cos(qP2y)*cos(qBx)*cos(qBy))+F3*(sin(qP3x)*cos(qP3y)*sin(qBx)+sin(qP3y)*sin(qBy)*cos(qBx)-cos(qP3x)*cos(qP3y)*cos(qBx)*cos(qBy))+F4*(sin(qP4x)*cos(qP4y)*sin(qBx)+sin(qP4y)*sin(qBy)*cos(qBx)-cos(qP4x)*cos(qP4y)*cos(qBx)*cos(qBy)))/mB;
J(8,5) = -(F1*(sin(qP1y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))-cos(qP1x)*cos(qP1y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz)))+F2*(sin(qP2y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))-cos(qP2x)*cos(qP2y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz)))+F3*(sin(qP3y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))-cos(qP3x)*cos(qP3y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz)))+F4*(sin(qP4y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))-cos(qP4x)*cos(qP4y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))))/mB;
J(8,6) = (F1*(sin(qP1x)*cos(qP1y)*sin(qBz)*cos(qBx)+sin(qP1y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP1x)*cos(qP1y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))+F2*(sin(qP2x)*cos(qP2y)*sin(qBz)*cos(qBx)+sin(qP2y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP2x)*cos(qP2y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))+F3*(sin(qP3x)*cos(qP3y)*sin(qBz)*cos(qBx)+sin(qP3y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP3x)*cos(qP3y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))+F4*(sin(qP4x)*cos(qP4y)*sin(qBz)*cos(qBx)+sin(qP4y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP4x)*cos(qP4y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))))/mB;
J(8,7) = 0;
J(8,8) = 0;
J(8,9) = 0;
J(8,10) = 0;
J(8,11) = 0;
J(8,12) = 0;
J(9,1) = 0;
J(9,2) = 0;
J(9,3) = 0;
J(9,4) = (F1*(sin(qP1y)*sin(qBx)*sin(qBy)-sin(qP1x)*cos(qP1y)*cos(qBx)-cos(qP1x)*cos(qP1y)*sin(qBx)*cos(qBy))+F2*(sin(qP2y)*sin(qBx)*sin(qBy)-sin(qP2x)*cos(qP2y)*cos(qBx)-cos(qP2x)*cos(qP2y)*sin(qBx)*cos(qBy))+F3*(sin(qP3y)*sin(qBx)*sin(qBy)-sin(qP3x)*cos(qP3y)*cos(qBx)-cos(qP3x)*cos(qP3y)*sin(qBx)*cos(qBy))+F4*(sin(qP4y)*sin(qBx)*sin(qBy)-sin(qP4x)*cos(qP4y)*cos(qBx)-cos(qP4x)*cos(qP4y)*sin(qBx)*cos(qBy)))/mB;
J(9,5) = -cos(qBx)*(F1*(sin(qP1y)*cos(qBy)+cos(qP1x)*cos(qP1y)*sin(qBy))+F2*(sin(qP2y)*cos(qBy)+cos(qP2x)*cos(qP2y)*sin(qBy))+F3*(sin(qP3y)*cos(qBy)+cos(qP3x)*cos(qP3y)*sin(qBy))+F4*(sin(qP4y)*cos(qBy)+cos(qP4x)*cos(qP4y)*sin(qBy)))/mB;
J(9,6) = 0;
J(9,7) = 0;
J(9,8) = 0;
J(9,9) = 0;
J(9,10) = 0;
J(9,11) = 0;
J(9,12) = 0;
J(10,1) = 0;
J(10,2) = 0;
J(10,3) = 0;
J(10,4) = 0;
J(10,5) = 0;
J(10,6) = 0;
J(10,7) = 0;
J(10,8) = 0;
J(10,9) = 0;
J(10,10) = -((IByy*IBzz-IByz^2)*(IBxy*wBz-IBzx*wBy)+(IBxy*IByz-IByy*IBzx)*(IBxx*wBy-2*IBxy*wBx-IByy*wBy-IByz*wBz)+(IBxy*IBzz-IByz*IBzx)*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(10,11) = ((IBxy*IByz-IByy*IBzx)*(IByy*wBx-2*IBxy*wBy-IBxx*wBx-IBzx*wBz)-(IBxy*IBzz-IByz*IBzx)*(IBxy*wBz-IByz*wBx)-(IByy*IBzz-IByz^2)*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(10,12) = ((IBxy*IByz-IByy*IBzx)*(IByz*wBx-IBzx*wBy)-(IBxy*IBzz-IByz*IBzx)*(IBxx*wBx+IBxy*wBy+2*IBzx*wBz-IBzz*wBx)-(IByy*IBzz-IByz^2)*(IBxy*wBx+IByy*wBy+2*IByz*wBz-IBzz*wBy))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(11,1) = 0;
J(11,2) = 0;
J(11,3) = 0;
J(11,4) = 0;
J(11,5) = 0;
J(11,6) = 0;
J(11,7) = 0;
J(11,8) = 0;
J(11,9) = 0;
J(11,10) = ((IBxy*IBzz-IByz*IBzx)*(IBxy*wBz-IBzx*wBy)+(IBxx*IByz-IBxy*IBzx)*(IBxx*wBy-2*IBxy*wBx-IByy*wBy-IByz*wBz)+(IBxx*IBzz-IBzx^2)*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(11,11) = -((IBxx*IByz-IBxy*IBzx)*(IByy*wBx-2*IBxy*wBy-IBxx*wBx-IBzx*wBz)-(IBxx*IBzz-IBzx^2)*(IBxy*wBz-IByz*wBx)-(IBxy*IBzz-IByz*IBzx)*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(11,12) = -((IBxx*IByz-IBxy*IBzx)*(IByz*wBx-IBzx*wBy)-(IBxx*IBzz-IBzx^2)*(IBxx*wBx+IBxy*wBy+2*IBzx*wBz-IBzz*wBx)-(IBxy*IBzz-IByz*IBzx)*(IBxy*wBx+IByy*wBy+2*IByz*wBz-IBzz*wBy))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(12,1) = 0;
J(12,2) = 0;
J(12,3) = 0;
J(12,4) = 0;
J(12,5) = 0;
J(12,6) = 0;
J(12,7) = 0;
J(12,8) = 0;
J(12,9) = 0;
J(12,10) = -((IBxy*IByz-IByy*IBzx)*(IBxy*wBz-IBzx*wBy)+(IBxx*IByy-IBxy^2)*(IBxx*wBy-2*IBxy*wBx-IByy*wBy-IByz*wBz)+(IBxx*IByz-IBxy*IBzx)*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(12,11) = ((IBxx*IByy-IBxy^2)*(IByy*wBx-2*IBxy*wBy-IBxx*wBx-IBzx*wBz)-(IBxx*IByz-IBxy*IBzx)*(IBxy*wBz-IByz*wBx)-(IBxy*IByz-IByy*IBzx)*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
J(12,12) = ((IBxx*IByy-IBxy^2)*(IByz*wBx-IBzx*wBy)-(IBxx*IByz-IBxy*IBzx)*(IBxx*wBx+IBxy*wBy+2*IBzx*wBz-IBzz*wBx)-(IBxy*IByz-IByy*IBzx)*(IBxy*wBx+IByy*wBy+2*IByz*wBz-IBzz*wBy))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));

checkDf = simplify(Df-J)

DfSubs = subs(Df, symParams, subParams );

FSubs= subs(F, [symParams; ], [subParams; ])

EqPoints = solve ( [FSubs(1:12)] == 0, [xBDot; yBDot; zBDot; wBx; wBy; wBz; qBx; qBy;F1; F2; F3; F4] )

FSubs2 = subs(FSubs, [xBDot; yBDot; zBDot; wBx; wBy; wBz; qBx; qBy;F1; F2; F3; F4 ], ...
  [EqPoints.xBDot(3); EqPoints.yBDot(3); EqPoints.zBDot(3); ...
  EqPoints.wBx(3); EqPoints.wBy(3); EqPoints.wBz(3); EqPoints.qBx(3); ...
  EqPoints.qBy(3); EqPoints.F1(3); EqPoints.F2(3); EqPoints.F3(3); EqPoints.F4(3);])



%EqPoints = solve ( subs(xDot, symParams, subParams ) == 0, [xBDot; yBDot; zBDot; qBx; qBy; qByDot; qBZDot; qBxDot; FBz; TBx; TBy; TBz] );
%EqPoints2 = solve([FSubs2(7:8); ] == 0, [qBx; qBy; ]) % Have to break up solving euquilibrium points due to matlab failure to solve
%FSubs3 = subs(FSubs2, [qBx; qBy ],[EqPoints2.qBx(2); EqPoints2.qBy(2);])
%EqPoints3 = solve([FSubs3(9:12); ] == 0, [F1; F2; F3; F4 ]) % Have to break up solving euquilibrium points due to matlab failure to solve

% note from eqpoints we can linearize about any point
uEq =([EqPoints.F1(3); EqPoints.F2(3); EqPoints.F3(3); EqPoints.F4(3)]) ;
uEqVal = double(uEq);
eqPoint = [0, 0, 0, ...  % Free Variables
            EqPoints.qBx(3), EqPoints.qBy(3), 0, ... % qBz is a free variable but must know what we linearized about
            EqPoints.xBDot(3), EqPoints.yBDot(3), EqPoints.zBDot(3), ...
            EqPoints.wBx(3), EqPoints.wBy(3), EqPoints.wBz(3)];
A =subs(Df, [x; T], [eqPoint'; uEqVal] );
ASub = double(subs(A, symParams,subParams));

eigenvalues=expand(eig(A));

% note stabiility/instability of eigenvalyes 
eigs = eig(ASub);

 % Get B From Motion Genesis and Matlab
 Df_B=simplify(jacobian(F, T')); 
 B = zeros(12,4);
 B = sym(B);

B(1,1) = 0;
B(1,2) = 0;
B(1,3) = 0;
B(1,4) = 0;
B(2,1) = 0;
B(2,2) = 0;
B(2,3) = 0;
B(2,4) = 0;
B(3,1) = 0;
B(3,2) = 0;
B(3,3) = 0;
B(3,4) = 0;
B(4,1) = 0;
B(4,2) = 0;
B(4,3) = 0;
B(4,4) = 0;
B(5,1) = 0;
B(5,2) = 0;
B(5,3) = 0;
B(5,4) = 0;
B(6,1) = 0;
B(6,2) = 0;
B(6,3) = 0;
B(6,4) = 0;
B(7,1) = (sin(qP1x)*cos(qP1y)*sin(qBz)*cos(qBx)+sin(qP1y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP1x)*cos(qP1y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))/mB;
B(7,2) = (sin(qP2x)*cos(qP2y)*sin(qBz)*cos(qBx)+sin(qP2y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP2x)*cos(qP2y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))/mB;
B(7,3) = (sin(qP3x)*cos(qP3y)*sin(qBz)*cos(qBx)+sin(qP3y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP3x)*cos(qP3y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))/mB;
B(7,4) = (sin(qP4x)*cos(qP4y)*sin(qBz)*cos(qBx)+sin(qP4y)*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))+cos(qP4x)*cos(qP4y)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy)))/mB;
B(8,1) = -(sin(qP1x)*cos(qP1y)*cos(qBx)*cos(qBz)-sin(qP1y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP1x)*cos(qP1y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))/mB;
B(8,2) = -(sin(qP2x)*cos(qP2y)*cos(qBx)*cos(qBz)-sin(qP2y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP2x)*cos(qP2y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))/mB;
B(8,3) = -(sin(qP3x)*cos(qP3y)*cos(qBx)*cos(qBz)-sin(qP3y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP3x)*cos(qP3y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))/mB;
B(8,4) = -(sin(qP4x)*cos(qP4y)*cos(qBx)*cos(qBz)-sin(qP4y)*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))-cos(qP4x)*cos(qP4y)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz)))/mB;
B(9,1) = -(sin(qP1x)*cos(qP1y)*sin(qBx)+sin(qP1y)*sin(qBy)*cos(qBx)-cos(qP1x)*cos(qP1y)*cos(qBx)*cos(qBy))/mB;
B(9,2) = -(sin(qP2x)*cos(qP2y)*sin(qBx)+sin(qP2y)*sin(qBy)*cos(qBx)-cos(qP2x)*cos(qP2y)*cos(qBx)*cos(qBy))/mB;
B(9,3) = -(sin(qP3x)*cos(qP3y)*sin(qBx)+sin(qP3y)*sin(qBy)*cos(qBx)-cos(qP3x)*cos(qP3y)*cos(qBx)*cos(qBy))/mB;
B(9,4) = -(sin(qP4x)*cos(qP4y)*sin(qBx)+sin(qP4y)*sin(qBy)*cos(qBx)-cos(qP4x)*cos(qP4y)*cos(qBx)*cos(qBy))/mB;
B(10,1) = -((IBxy*IBzz-IByz*IBzx)*lA*cos(qP1x)*cos(qP1y)+(IBxy*IByz-IByy*IBzx)*(bDrag/bThrust-lA*sin(qP1x)*cos(qP1y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(10,2) = -((IByy*IBzz-IByz^2)*lA*cos(qP2x)*cos(qP2y)-(IBxy*IByz-IByy*IBzx)*(bDrag/bThrust+lA*sin(qP2y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(10,3) = ((IBxy*IBzz-IByz*IBzx)*lA*cos(qP3x)*cos(qP3y)-(IBxy*IByz-IByy*IBzx)*(bDrag/bThrust+lA*sin(qP3x)*cos(qP3y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(10,4) = ((IByy*IBzz-IByz^2)*lA*cos(qP4x)*cos(qP4y)+(IBxy*IByz-IByy*IBzx)*(bDrag/bThrust-lA*sin(qP4y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(11,1) = ((IBxx*IBzz-IBzx^2)*lA*cos(qP1x)*cos(qP1y)+(IBxx*IByz-IBxy*IBzx)*(bDrag/bThrust-lA*sin(qP1x)*cos(qP1y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(11,2) = ((IBxy*IBzz-IByz*IBzx)*lA*cos(qP2x)*cos(qP2y)-(IBxx*IByz-IBxy*IBzx)*(bDrag/bThrust+lA*sin(qP2y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(11,3) = -((IBxx*IBzz-IBzx^2)*lA*cos(qP3x)*cos(qP3y)-(IBxx*IByz-IBxy*IBzx)*(bDrag/bThrust+lA*sin(qP3x)*cos(qP3y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(11,4) = -((IBxy*IBzz-IByz*IBzx)*lA*cos(qP4x)*cos(qP4y)+(IBxx*IByz-IBxy*IBzx)*(bDrag/bThrust-lA*sin(qP4y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(12,1) = -((IBxx*IByz-IBxy*IBzx)*lA*cos(qP1x)*cos(qP1y)+(IBxx*IByy-IBxy^2)*(bDrag/bThrust-lA*sin(qP1x)*cos(qP1y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(12,2) = -((IBxy*IByz-IByy*IBzx)*lA*cos(qP2x)*cos(qP2y)-(IBxx*IByy-IBxy^2)*(bDrag/bThrust+lA*sin(qP2y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(12,3) = ((IBxx*IByz-IBxy*IBzx)*lA*cos(qP3x)*cos(qP3y)-(IBxx*IByy-IBxy^2)*(bDrag/bThrust+lA*sin(qP3x)*cos(qP3y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
B(12,4) = ((IBxy*IByz-IByy*IBzx)*lA*cos(qP4x)*cos(qP4y)+(IBxx*IByy-IBxy^2)*(bDrag/bThrust-lA*sin(qP4y)))/(IBxx*IByz^2-IBzz*(IBxx*IByy-IBxy^2)-IBzx*(2*IBxy*IByz-IByy*IBzx));
 
checkDf_B = simplify(Df_B-B)
BSub = subs(B, x, eqPoint')   ;
BSub = double(subs(BSub, symParams, subParams));
rankSys = rank(ctrb(ASub,BSub));


if rankSys < length(x)
  warning('System Not Controllable')
else
  fprintf('System is controllable. Rank of controllability matrix is = %i \n', rankSys)
end

% Create LQR Gain Matrix
v = [100 100 100 1000 1000 1000 100 100 100 100 100 100 ];
Q = diag( v, 0);
%Q = eye(12)*100;
R = eye(4)*0.1;

Klqr = lqr(ASub, BSub, Q, R );
eig(ASub-BSub*Klqr)
save('Klqr.mat', 'Klqr')
save('uEq.mat', 'uEqVal')
