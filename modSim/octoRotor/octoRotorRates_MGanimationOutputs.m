function [rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N, rP7_No_N, rP8_No_N,...
          N_R_P1, N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6, N_R_P7, N_R_P8, N_R_B  ] = ...
          octoRotorRates_MGanimationOutputs( xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z )
if( nargin ~= 54 ) error( 'octoRotorRates_MGanimationOutputs expects 54 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotorRates_MGanimationOutputs.m created Oct 27 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
rP1_No_N = zeros( 3, 1 );
rP2_No_N = zeros( 3, 1 );
rP3_No_N = zeros( 3, 1 );
rP4_No_N = zeros( 3, 1 );
rP5_No_N = zeros( 3, 1 );
rP6_No_N = zeros( 3, 1 );
rP7_No_N = zeros( 3, 1 );
rP8_No_N = zeros( 3, 1 );
N_R_P1 = zeros( 3, 3 );
N_R_P2 = zeros( 3, 3 );
N_R_P3 = zeros( 3, 3 );
N_R_P4 = zeros( 3, 3 );
N_R_P5 = zeros( 3, 3 );
N_R_P6 = zeros( 3, 3 );
N_R_P7 = zeros( 3, 3 );
N_R_P8 = zeros( 3, 3 );
N_R_B = zeros( 3, 3 );
z = zeros( 1, 1645 );



%===========================================================================
z(1) = cos(qBy);
z(2) = cos(qBz);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(5) = sin(qBz);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(14) = z(1)*z(8);
z(23) = cos(qP1y);
z(24) = sin(qP1x);
z(25) = sin(qP1y);
z(26) = z(24)*z(25);
z(27) = cos(qP1x);
z(28) = z(25)*z(27);
z(29) = z(23)*z(24);
z(30) = z(23)*z(27);
z(31) = cos(qP1z);
z(32) = sin(qP1z);
z(44) = z(30)*z(14) - z(25)*z(9) - z(29)*z(3);
z(53) = cos(qP2y);
z(54) = sin(qP2x);
z(55) = sin(qP2y);
z(56) = z(54)*z(55);
z(57) = cos(qP2x);
z(58) = z(55)*z(57);
z(59) = z(53)*z(54);
z(60) = z(53)*z(57);
z(61) = cos(qP2z);
z(62) = sin(qP2z);
z(74) = z(60)*z(14) - z(55)*z(9) - z(59)*z(3);
z(83) = cos(qP3y);
z(84) = sin(qP3x);
z(85) = sin(qP3y);
z(86) = z(84)*z(85);
z(87) = cos(qP3x);
z(88) = z(85)*z(87);
z(89) = z(83)*z(84);
z(90) = z(83)*z(87);
z(91) = cos(qP3z);
z(92) = sin(qP3z);
z(104) = z(90)*z(14) - z(85)*z(9) - z(89)*z(3);
z(113) = cos(qP4y);
z(114) = sin(qP4x);
z(115) = sin(qP4y);
z(116) = z(114)*z(115);
z(117) = cos(qP4x);
z(118) = z(115)*z(117);
z(119) = z(113)*z(114);
z(120) = z(113)*z(117);
z(121) = cos(qP4z);
z(122) = sin(qP4z);
z(134) = z(120)*z(14) - z(115)*z(9) - z(119)*z(3);
z(143) = cos(qP5y);
z(144) = sin(qP5x);
z(145) = sin(qP5y);
z(146) = z(144)*z(145);
z(147) = cos(qP5x);
z(148) = z(145)*z(147);
z(149) = z(143)*z(144);
z(150) = z(143)*z(147);
z(151) = cos(qP5z);
z(152) = sin(qP5z);
z(164) = z(150)*z(14) - z(145)*z(9) - z(149)*z(3);
z(173) = cos(qP6y);
z(174) = sin(qP6x);
z(175) = sin(qP6y);
z(176) = z(174)*z(175);
z(177) = cos(qP6x);
z(178) = z(175)*z(177);
z(179) = z(173)*z(174);
z(180) = z(173)*z(177);
z(181) = cos(qP6z);
z(182) = sin(qP6z);
z(194) = z(180)*z(14) - z(175)*z(9) - z(179)*z(3);
z(203) = cos(qP7y);
z(204) = sin(qP7x);
z(205) = sin(qP7y);
z(206) = z(204)*z(205);
z(207) = cos(qP7x);
z(208) = z(205)*z(207);
z(209) = z(203)*z(204);
z(210) = z(203)*z(207);
z(211) = cos(qP7z);
z(212) = sin(qP7z);
z(224) = z(210)*z(14) - z(205)*z(9) - z(209)*z(3);
z(233) = cos(qP8y);
z(234) = sin(qP8x);
z(235) = sin(qP8y);
z(236) = z(234)*z(235);
z(237) = cos(qP8x);
z(238) = z(235)*z(237);
z(239) = z(233)*z(234);
z(240) = z(233)*z(237);
z(241) = cos(qP8z);
z(242) = sin(qP8z);
z(254) = z(240)*z(14) - z(235)*z(9) - z(239)*z(3);
z(357) = z(23)*z(6) - z(26)*z(10) - z(28)*z(12);
z(358) = z(24)*z(12) - z(27)*z(10);
z(359) = z(25)*z(6) + z(29)*z(10) + z(30)*z(12);
z(360) = z(23)*z(7) + z(26)*z(11) - z(28)*z(13);
z(361) = z(24)*z(13) + z(27)*z(11);
z(362) = z(25)*z(7) + z(30)*z(13) - z(29)*z(11);
z(363) = z(26)*z(3) - z(23)*z(9) - z(28)*z(14);
z(364) = z(24)*z(14) + z(27)*z(3);
z(365) = z(53)*z(6) - z(56)*z(10) - z(58)*z(12);
z(366) = z(54)*z(12) - z(57)*z(10);
z(367) = z(55)*z(6) + z(59)*z(10) + z(60)*z(12);
z(368) = z(53)*z(7) + z(56)*z(11) - z(58)*z(13);
z(369) = z(54)*z(13) + z(57)*z(11);
z(370) = z(55)*z(7) + z(60)*z(13) - z(59)*z(11);
z(371) = z(56)*z(3) - z(53)*z(9) - z(58)*z(14);
z(372) = z(54)*z(14) + z(57)*z(3);
z(373) = z(83)*z(6) - z(86)*z(10) - z(88)*z(12);
z(374) = z(84)*z(12) - z(87)*z(10);
z(375) = z(85)*z(6) + z(89)*z(10) + z(90)*z(12);
z(376) = z(83)*z(7) + z(86)*z(11) - z(88)*z(13);
z(377) = z(84)*z(13) + z(87)*z(11);
z(378) = z(85)*z(7) + z(90)*z(13) - z(89)*z(11);
z(379) = z(86)*z(3) - z(83)*z(9) - z(88)*z(14);
z(380) = z(84)*z(14) + z(87)*z(3);
z(381) = z(113)*z(6) - z(116)*z(10) - z(118)*z(12);
z(382) = z(114)*z(12) - z(117)*z(10);
z(383) = z(115)*z(6) + z(119)*z(10) + z(120)*z(12);
z(384) = z(113)*z(7) + z(116)*z(11) - z(118)*z(13);
z(385) = z(114)*z(13) + z(117)*z(11);
z(386) = z(115)*z(7) + z(120)*z(13) - z(119)*z(11);
z(387) = z(116)*z(3) - z(113)*z(9) - z(118)*z(14);
z(388) = z(114)*z(14) + z(117)*z(3);
z(389) = z(143)*z(6) - z(146)*z(10) - z(148)*z(12);
z(390) = z(144)*z(12) - z(147)*z(10);
z(391) = z(145)*z(6) + z(149)*z(10) + z(150)*z(12);
z(392) = z(143)*z(7) + z(146)*z(11) - z(148)*z(13);
z(393) = z(144)*z(13) + z(147)*z(11);
z(394) = z(145)*z(7) + z(150)*z(13) - z(149)*z(11);
z(395) = z(146)*z(3) - z(143)*z(9) - z(148)*z(14);
z(396) = z(144)*z(14) + z(147)*z(3);
z(397) = z(173)*z(6) - z(176)*z(10) - z(178)*z(12);
z(398) = z(174)*z(12) - z(177)*z(10);
z(399) = z(175)*z(6) + z(179)*z(10) + z(180)*z(12);
z(400) = z(173)*z(7) + z(176)*z(11) - z(178)*z(13);
z(401) = z(174)*z(13) + z(177)*z(11);
z(402) = z(175)*z(7) + z(180)*z(13) - z(179)*z(11);
z(403) = z(176)*z(3) - z(173)*z(9) - z(178)*z(14);
z(404) = z(174)*z(14) + z(177)*z(3);
z(405) = z(203)*z(6) - z(206)*z(10) - z(208)*z(12);
z(406) = z(204)*z(12) - z(207)*z(10);
z(407) = z(205)*z(6) + z(209)*z(10) + z(210)*z(12);
z(408) = z(203)*z(7) + z(206)*z(11) - z(208)*z(13);
z(409) = z(204)*z(13) + z(207)*z(11);
z(410) = z(205)*z(7) + z(210)*z(13) - z(209)*z(11);
z(411) = z(206)*z(3) - z(203)*z(9) - z(208)*z(14);
z(412) = z(204)*z(14) + z(207)*z(3);
z(413) = z(233)*z(6) - z(236)*z(10) - z(238)*z(12);
z(414) = z(234)*z(12) - z(237)*z(10);
z(415) = z(235)*z(6) + z(239)*z(10) + z(240)*z(12);
z(416) = z(233)*z(7) + z(236)*z(11) - z(238)*z(13);
z(417) = z(234)*z(13) + z(237)*z(11);
z(418) = z(235)*z(7) + z(240)*z(13) - z(239)*z(11);
z(419) = z(236)*z(3) - z(233)*z(9) - z(238)*z(14);
z(420) = z(234)*z(14) + z(237)*z(3);
z(1598) = z(31)*z(357) + z(32)*z(358);
z(1599) = z(31)*z(360) + z(32)*z(361);
z(1600) = z(31)*z(363) + z(32)*z(364);
z(1601) = z(31)*z(358) - z(32)*z(357);
z(1602) = z(31)*z(361) - z(32)*z(360);
z(1603) = z(31)*z(364) - z(32)*z(363);
z(1604) = z(61)*z(365) + z(62)*z(366);
z(1605) = z(61)*z(368) + z(62)*z(369);
z(1606) = z(61)*z(371) + z(62)*z(372);
z(1607) = z(61)*z(366) - z(62)*z(365);
z(1608) = z(61)*z(369) - z(62)*z(368);
z(1609) = z(61)*z(372) - z(62)*z(371);
z(1610) = z(91)*z(373) + z(92)*z(374);
z(1611) = z(91)*z(376) + z(92)*z(377);
z(1612) = z(91)*z(379) + z(92)*z(380);
z(1613) = z(91)*z(374) - z(92)*z(373);
z(1614) = z(91)*z(377) - z(92)*z(376);
z(1615) = z(91)*z(380) - z(92)*z(379);
z(1616) = z(121)*z(381) + z(122)*z(382);
z(1617) = z(121)*z(384) + z(122)*z(385);
z(1618) = z(121)*z(387) + z(122)*z(388);
z(1619) = z(121)*z(382) - z(122)*z(381);
z(1620) = z(121)*z(385) - z(122)*z(384);
z(1621) = z(121)*z(388) - z(122)*z(387);
z(1622) = z(151)*z(389) + z(152)*z(390);
z(1623) = z(151)*z(392) + z(152)*z(393);
z(1624) = z(151)*z(395) + z(152)*z(396);
z(1625) = z(151)*z(390) - z(152)*z(389);
z(1626) = z(151)*z(393) - z(152)*z(392);
z(1627) = z(151)*z(396) - z(152)*z(395);
z(1628) = z(181)*z(397) + z(182)*z(398);
z(1629) = z(181)*z(400) + z(182)*z(401);
z(1630) = z(181)*z(403) + z(182)*z(404);
z(1631) = z(181)*z(398) - z(182)*z(397);
z(1632) = z(181)*z(401) - z(182)*z(400);
z(1633) = z(181)*z(404) - z(182)*z(403);
z(1634) = z(211)*z(405) + z(212)*z(406);
z(1635) = z(211)*z(408) + z(212)*z(409);
z(1636) = z(211)*z(411) + z(212)*z(412);
z(1637) = z(211)*z(406) - z(212)*z(405);
z(1638) = z(211)*z(409) - z(212)*z(408);
z(1639) = z(211)*z(412) - z(212)*z(411);
z(1640) = z(241)*z(413) + z(242)*z(414);
z(1641) = z(241)*z(416) + z(242)*z(417);
z(1642) = z(241)*z(419) + z(242)*z(420);
z(1643) = z(241)*z(414) - z(242)*z(413);
z(1644) = z(241)*z(417) - z(242)*z(416);
z(1645) = z(241)*z(420) - z(242)*z(419);



%===========================================================================
Output = [];


rP1_No_N(1) = xB + xP1*z(6) + zP1*z(12) - yP1*z(10);
rP1_No_N(2) = yB + xP1*z(7) + yP1*z(11) + zP1*z(13);
rP1_No_N(3) = zB + yP1*z(3) + zP1*z(14) - xP1*z(9);

rP2_No_N(1) = xB + xP2*z(6) + zP2*z(12) - yP2*z(10);
rP2_No_N(2) = yB + xP2*z(7) + yP2*z(11) + zP2*z(13);
rP2_No_N(3) = zB + yP2*z(3) + zP2*z(14) - xP2*z(9);

rP3_No_N(1) = xB + xP3*z(6) + zP3*z(12) - yP3*z(10);
rP3_No_N(2) = yB + xP3*z(7) + yP3*z(11) + zP3*z(13);
rP3_No_N(3) = zB + yP3*z(3) + zP3*z(14) - xP3*z(9);

rP4_No_N(1) = xB + xP4*z(6) + zP4*z(12) - yP4*z(10);
rP4_No_N(2) = yB + xP4*z(7) + yP4*z(11) + zP4*z(13);
rP4_No_N(3) = zB + yP4*z(3) + zP4*z(14) - xP4*z(9);

rP5_No_N(1) = xB + xP5*z(6) + zP5*z(12) - yP5*z(10);
rP5_No_N(2) = yB + xP5*z(7) + yP5*z(11) + zP5*z(13);
rP5_No_N(3) = zB + yP5*z(3) + zP5*z(14) - xP5*z(9);

rP6_No_N(1) = xB + xP6*z(6) + zP6*z(12) - yP6*z(10);
rP6_No_N(2) = yB + xP6*z(7) + yP6*z(11) + zP6*z(13);
rP6_No_N(3) = zB + yP6*z(3) + zP6*z(14) - xP6*z(9);

rP7_No_N(1) = xB + xP7*z(6) + zP7*z(12) - yP7*z(10);
rP7_No_N(2) = yB + xP7*z(7) + yP7*z(11) + zP7*z(13);
rP7_No_N(3) = zB + yP7*z(3) + zP7*z(14) - xP7*z(9);

rP8_No_N(1) = xB + xP8*z(6) + zP8*z(12) - yP8*z(10);
rP8_No_N(2) = yB + xP8*z(7) + yP8*z(11) + zP8*z(13);
rP8_No_N(3) = zB + yP8*z(3) + zP8*z(14) - xP8*z(9);

N_R_P1(1,1) = z(1598);
N_R_P1(1,2) = z(1599);
N_R_P1(1,3) = z(1600);
N_R_P1(2,1) = z(1601);
N_R_P1(2,2) = z(1602);
N_R_P1(2,3) = z(1603);
N_R_P1(3,1) = z(359);
N_R_P1(3,2) = z(362);
N_R_P1(3,3) = z(44);

N_R_P2(1,1) = z(1604);
N_R_P2(1,2) = z(1605);
N_R_P2(1,3) = z(1606);
N_R_P2(2,1) = z(1607);
N_R_P2(2,2) = z(1608);
N_R_P2(2,3) = z(1609);
N_R_P2(3,1) = z(367);
N_R_P2(3,2) = z(370);
N_R_P2(3,3) = z(74);

N_R_P3(1,1) = z(1610);
N_R_P3(1,2) = z(1611);
N_R_P3(1,3) = z(1612);
N_R_P3(2,1) = z(1613);
N_R_P3(2,2) = z(1614);
N_R_P3(2,3) = z(1615);
N_R_P3(3,1) = z(375);
N_R_P3(3,2) = z(378);
N_R_P3(3,3) = z(104);

N_R_P4(1,1) = z(1616);
N_R_P4(1,2) = z(1617);
N_R_P4(1,3) = z(1618);
N_R_P4(2,1) = z(1619);
N_R_P4(2,2) = z(1620);
N_R_P4(2,3) = z(1621);
N_R_P4(3,1) = z(383);
N_R_P4(3,2) = z(386);
N_R_P4(3,3) = z(134);

N_R_P5(1,1) = z(1622);
N_R_P5(1,2) = z(1623);
N_R_P5(1,3) = z(1624);
N_R_P5(2,1) = z(1625);
N_R_P5(2,2) = z(1626);
N_R_P5(2,3) = z(1627);
N_R_P5(3,1) = z(391);
N_R_P5(3,2) = z(394);
N_R_P5(3,3) = z(164);

N_R_P6(1,1) = z(1628);
N_R_P6(1,2) = z(1629);
N_R_P6(1,3) = z(1630);
N_R_P6(2,1) = z(1631);
N_R_P6(2,2) = z(1632);
N_R_P6(2,3) = z(1633);
N_R_P6(3,1) = z(399);
N_R_P6(3,2) = z(402);
N_R_P6(3,3) = z(194);

N_R_P7(1,1) = z(1634);
N_R_P7(1,2) = z(1635);
N_R_P7(1,3) = z(1636);
N_R_P7(2,1) = z(1637);
N_R_P7(2,2) = z(1638);
N_R_P7(2,3) = z(1639);
N_R_P7(3,1) = z(407);
N_R_P7(3,2) = z(410);
N_R_P7(3,3) = z(224);

N_R_P8(1,1) = z(1640);
N_R_P8(1,2) = z(1641);
N_R_P8(1,3) = z(1642);
N_R_P8(2,1) = z(1643);
N_R_P8(2,2) = z(1644);
N_R_P8(2,3) = z(1645);
N_R_P8(3,1) = z(415);
N_R_P8(3,2) = z(418);
N_R_P8(3,3) = z(254);

N_R_B(1,1) = z(6);
N_R_B(1,2) = z(7);
N_R_B(1,3) = -z(9);
N_R_B(2,1) = -z(10);
N_R_B(2,2) = z(11);
N_R_B(2,3) = z(3);
N_R_B(3,1) = z(12);
N_R_B(3,2) = z(13);
N_R_B(3,3) = z(14);


%==========================================================
end    % End of function octoRotorRates_MGanimationOutputs
%==========================================================
