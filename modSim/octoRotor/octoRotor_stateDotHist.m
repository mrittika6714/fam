function [tOut, stateDottOut] = octoRotor_stateDotHist(operation, t, stateDot)
persistent stateDotHist tHist
if isempty(stateDotHist)
    stateDotHist= zeros(1,20);
    tHist = 0;
end

if strcmp(operation, 'add')
    stateDotHist = [stateDotHist; stateDot];
    stateDottOut = stateDotHist;
    tHist = [tHist; t'];
    tOut = tHist;
elseif strcmp(operation, 'get')
    stateDottOut = stateDotHist;
    tOut = tHist;
elseif strcmp(operation, 'clear')
    clear stateDotHist tHist
    stateDottOut = [];
    tOut = [];
elseif strcmp(operation,'getLast')
    stateDottOut = stateDotHist(end,:);
    tOut = tHist(end);
else
    error('Case not Handeled');
end

end
