function FTMap = octoRotorRates_FTMap3( P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, qA1x, qA1z, qA2x, qA2z, qA3x, qA3z, qA4x, qA4z, qA5x, qA5z, qA6x, qA6z, qA7x, qA7z, qA8x, qA8z, bThrust, bDrag, lArm )
if( nargin ~= 43 ) error( 'octoRotorRates_FTMap3 expects 43 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotorRates_FTMap3.m created Dec 03 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
FTMap = zeros( 6, 8 );
z = zeros( 1, 685 );



%===========================================================================


%===========================================================================
Output = [];

FTMap(1,1) = sin(qP1y)*cos(qA1z) + sin(qA1z)*cos(qP1y)*sin(qA1x+qP1x);
FTMap(1,2) = sin(qP2y)*cos(qA2z) + sin(qA2z)*cos(qP2y)*sin(qA2x+qP2x);
FTMap(1,3) = sin(qP3y)*cos(qA3z) + sin(qA3z)*cos(qP3y)*sin(qA3x+qP3x);
FTMap(1,4) = sin(qP4y)*cos(qA4z) + sin(qA4z)*cos(qP4y)*sin(qA4x+qP4x);
FTMap(1,5) = sin(qP5y)*cos(qA5z) + sin(qA5z)*cos(qP5y)*sin(qA5x+qP5x);
FTMap(1,6) = sin(qP6y)*cos(qA6z) + sin(qA6z)*cos(qP6y)*sin(qA6x+qP6x);
FTMap(1,7) = sin(qP7y)*cos(qA7z) + sin(qA7z)*cos(qP7y)*sin(qA7x+qP7x);
FTMap(1,8) = sin(qP8y)*cos(qA8z) + sin(qA8z)*cos(qP8y)*sin(qA8x+qP8x);
FTMap(2,1) = sin(qA1z)*sin(qP1y) - cos(qA1z)*cos(qP1y)*sin(qA1x+qP1x);
FTMap(2,2) = sin(qA2z)*sin(qP2y) - cos(qA2z)*cos(qP2y)*sin(qA2x+qP2x);
FTMap(2,3) = sin(qA3z)*sin(qP3y) - cos(qA3z)*cos(qP3y)*sin(qA3x+qP3x);
FTMap(2,4) = sin(qA4z)*sin(qP4y) - cos(qA4z)*cos(qP4y)*sin(qA4x+qP4x);
FTMap(2,5) = sin(qA5z)*sin(qP5y) - cos(qA5z)*cos(qP5y)*sin(qA5x+qP5x);
FTMap(2,6) = sin(qA6z)*sin(qP6y) - cos(qA6z)*cos(qP6y)*sin(qA6x+qP6x);
FTMap(2,7) = sin(qA7z)*sin(qP7y) - cos(qA7z)*cos(qP7y)*sin(qA7x+qP7x);
FTMap(2,8) = sin(qA8z)*sin(qP8y) - cos(qA8z)*cos(qP8y)*sin(qA8x+qP8x);
FTMap(3,1) = cos(qP1y)*cos(qA1x+qP1x);
FTMap(3,2) = cos(qP2y)*cos(qA2x+qP2x);
FTMap(3,3) = cos(qP3y)*cos(qA3x+qP3x);
FTMap(3,4) = cos(qP4y)*cos(qA4x+qP4x);
FTMap(3,5) = cos(qP5y)*cos(qA5x+qP5x);
FTMap(3,6) = cos(qP6y)*cos(qA6x+qP6x);
FTMap(3,7) = cos(qP7y)*cos(qA7x+qP7x);
FTMap(3,8) = cos(qP8y)*cos(qA8x+qP8x);
FTMap(4,1) = lArm*(sin(qA1z)*sin(qP1x)*sin(qP1y)*cos(qA1x+qP1x)+cos(qP1x)*(cos(qA1z)*cos(qP1y)-sin(qA1z)*sin(qP1y)*sin(qA1x+qP1x)));
FTMap(4,2) = lArm*(sin(qA2z)*sin(qP2x)*sin(qP2y)*cos(qA2x+qP2x)+cos(qP2x)*(cos(qA2z)*cos(qP2y)-sin(qA2z)*sin(qP2y)*sin(qA2x+qP2x)));
FTMap(4,3) = lArm*(sin(qA3z)*sin(qP3x)*sin(qP3y)*cos(qA3x+qP3x)+cos(qP3x)*(cos(qA3z)*cos(qP3y)-sin(qA3z)*sin(qP3y)*sin(qA3x+qP3x)));
FTMap(4,4) = lArm*(sin(qA4z)*sin(qP4x)*sin(qP4y)*cos(qA4x+qP4x)+cos(qP4x)*(cos(qA4z)*cos(qP4y)-sin(qA4z)*sin(qP4y)*sin(qA4x+qP4x)));
FTMap(4,5) = lArm*(sin(qA5z)*sin(qP5x)*sin(qP5y)*cos(qA5x+qP5x)+cos(qP5x)*(cos(qA5z)*cos(qP5y)-sin(qA5z)*sin(qP5y)*sin(qA5x+qP5x)));
FTMap(4,6) = lArm*(sin(qA6z)*sin(qP6x)*sin(qP6y)*cos(qA6x+qP6x)+cos(qP6x)*(cos(qA6z)*cos(qP6y)-sin(qA6z)*sin(qP6y)*sin(qA6x+qP6x)));
FTMap(4,7) = lArm*(sin(qA7z)*sin(qP7x)*sin(qP7y)*cos(qA7x+qP7x)+cos(qP7x)*(cos(qA7z)*cos(qP7y)-sin(qA7z)*sin(qP7y)*sin(qA7x+qP7x)));
FTMap(4,8) = lArm*(sin(qA8z)*sin(qP8x)*sin(qP8y)*cos(qA8x+qP8x)+cos(qP8x)*(cos(qA8z)*cos(qP8y)-sin(qA8z)*sin(qP8y)*sin(qA8x+qP8x)));
FTMap(5,1) = -lArm*(sin(qP1x)*sin(qP1y)*cos(qA1z)*cos(qA1x+qP1x)-cos(qP1x)*(sin(qA1z)*cos(qP1y)+sin(qP1y)*cos(qA1z)*sin(qA1x+qP1x)));
FTMap(5,2) = -lArm*(sin(qP2x)*sin(qP2y)*cos(qA2z)*cos(qA2x+qP2x)-cos(qP2x)*(sin(qA2z)*cos(qP2y)+sin(qP2y)*cos(qA2z)*sin(qA2x+qP2x)));
FTMap(5,3) = -lArm*(sin(qP3x)*sin(qP3y)*cos(qA3z)*cos(qA3x+qP3x)-cos(qP3x)*(sin(qA3z)*cos(qP3y)+sin(qP3y)*cos(qA3z)*sin(qA3x+qP3x)));
FTMap(5,4) = -lArm*(sin(qP4x)*sin(qP4y)*cos(qA4z)*cos(qA4x+qP4x)-cos(qP4x)*(sin(qA4z)*cos(qP4y)+sin(qP4y)*cos(qA4z)*sin(qA4x+qP4x)));
FTMap(5,5) = -lArm*(sin(qP5x)*sin(qP5y)*cos(qA5z)*cos(qA5x+qP5x)-cos(qP5x)*(sin(qA5z)*cos(qP5y)+sin(qP5y)*cos(qA5z)*sin(qA5x+qP5x)));
FTMap(5,6) = -lArm*(sin(qP6x)*sin(qP6y)*cos(qA6z)*cos(qA6x+qP6x)-cos(qP6x)*(sin(qA6z)*cos(qP6y)+sin(qP6y)*cos(qA6z)*sin(qA6x+qP6x)));
FTMap(5,7) = -lArm*(sin(qP7x)*sin(qP7y)*cos(qA7z)*cos(qA7x+qP7x)-cos(qP7x)*(sin(qA7z)*cos(qP7y)+sin(qP7y)*cos(qA7z)*sin(qA7x+qP7x)));
FTMap(5,8) = -lArm*(sin(qP8x)*sin(qP8y)*cos(qA8z)*cos(qA8x+qP8x)-cos(qP8x)*(sin(qA8z)*cos(qP8y)+sin(qP8y)*cos(qA8z)*sin(qA8x+qP8x)));
FTMap(6,1) = -lArm*sin(qP1y)*cos(qA1x);
FTMap(6,2) = -lArm*sin(qP2y)*cos(qA2x);
FTMap(6,3) = -lArm*sin(qP3y)*cos(qA3x);
FTMap(6,4) = -lArm*sin(qP4y)*cos(qA4x);
FTMap(6,5) = -lArm*sin(qP5y)*cos(qA5x);
FTMap(6,6) = -lArm*sin(qP6y)*cos(qA6x);
FTMap(6,7) = -lArm*sin(qP7y)*cos(qA7x);
FTMap(6,8) = -lArm*sin(qP8y)*cos(qA8x);


%==============================================
end    % End of function octoRotorRates_FTMap3
%==============================================
