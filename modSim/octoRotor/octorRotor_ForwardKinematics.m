function [taskPos, taskVel] = octorRotor_ForwardKinematics( qBx, qBy, qBz, xB, yB, zB, wBx, wBy, wBz, xBDt, yBDt, zBDt )
if( nargin ~= 12 ) error( 'octorRotor_ForwardKinematics expects 12 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octorRotor_ForwardKinematics.m created Oct 19 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
taskPos = zeros( 6, 1 );
taskVel = zeros( 6, 1 );
z = zeros( 1, 1645 );



%===========================================================================
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(263) = z(1)*z(14) + z(4)*z(9);
z(264) = (wBx*z(14)+wBz*z(9))/z(263);
qBxDt = z(264);
z(3) = sin(qBx);
z(265) = (wBx*z(4)-wBz*z(1))/z(263);
z(266) = -wBy - z(3)*z(265);
qByDt = -z(266);
qBzDt = -z(265);



%===========================================================================
Output = [];

taskPos(1) = xB;
taskPos(2) = yB;
taskPos(3) = zB;
taskPos(4) = qBx;
taskPos(5) = qBy;
taskPos(6) = qBz;

taskVel(1) = xBDt;
taskVel(2) = yBDt;
taskVel(3) = zBDt;
taskVel(4) = qBxDt;
taskVel(5) = qByDt;
taskVel(6) = qBzDt;


%=====================================================
end    % End of function octorRotor_ForwardKinematics
%=====================================================
