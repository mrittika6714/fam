

clc;clear;close all;
format long;
simParams.control.type = 'none';
simParams.control.space = 'none';
simParams.control.func = @() zeros(1,1);
simParams.trajectoryFunc = @() zeros(1,1);
simParams.tFinal = 0;
params = octoRotor_params(simParams);
options = optimoptions('fmincon');
options = optimoptions(options,'OptimalityTolerance',1e-8,'StepTolerance',1e-12,'constraintTolerance',1e-12,'MaxIterations',100000000,'MaxFunctionEvaluations',1000000000,'Display', 'iter');
load('propAngles.mat')
propAngles =  ones(16,1) * acos(2/sqrt(6));
x0 = propAngles;%ones(16,1) * pi/4; 
A= [];
B = [];
lb = ones(16,1) * (-pi/2);
ub = ones(16,1) * pi/2;
objectiveFuncHandle = @(x) objectiveFunc(x,params);

[propAngles fval, exitFlag] = fmincon(objectiveFuncHandle, x0, A, B,[],[],lb, ub, @nonLinConFunc, options);
 180/pi*propAngles


FTMap = calc_FTMap(propAngles, params)
wrench = FTMap * ones(8,1)
%Note wrench seems wrong here
rank(FTMap)
[U,S,V] = svd(FTMap)
objectiveFunc(propAngles,params)


%% Visualize Octo Rotor

xB = 0;
yB = 0;
zB = 0;
qBx = 0;
qBy = 0;
qBz = 0;

qP1x = propAngles(1);
qP1y = propAngles(2);
qP2x = propAngles(3);
qP2y = propAngles(4);
qP3x = propAngles(5);
qP3y = propAngles(6);
qP4x = propAngles(7);
qP4y = propAngles(8);
qP5x = propAngles(9);
qP5y = propAngles(10);
qP6x = propAngles(11);
qP6y = propAngles(12);
qP7x = propAngles(13);
qP7y = propAngles(14);
qP8x = propAngles(15);
qP8y = propAngles(16);
qP1z = 0;
qP2z = 0;
qP3z = 0;
qP4z = 0;
qP5z = 0;
qP6z = 0;
qP7z = 0;
qP8z = 0;

qA1x = acos(2/sqrt(6));
qA1z = pi/4;
qA2x = acos(2/sqrt(6));
qA2z = 3*pi/4;
qA3x = acos(2/sqrt(6));
qA3z = 5*pi/4;
qA4x = acos(2/sqrt(6));
qA4z = 7*pi/4;
qA5x = -acos(2/sqrt(6));
qA5z = pi/4;
qA6x = -acos(2/sqrt(6));
qA6z = 3*pi/4;
qA7x = -acos(2/sqrt(6));
qA7z = 5*pi/4;
qA8x = -acos(2/sqrt(6));
qA8z = 7*pi/4;
%MzMat = octoRotorOptimiation2_CalcMzMat( qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, qA1x, qA1z, qA2x, qA2z, qA3x, qA3z, qA4x, qA4z, qA5x, qA5z, qA6x, qA6z, qA7x, qA7z, qA8x, qA8z )



bThrust = 1;
bDrag = 0;
lArm = 1;

plotData.states = [0, 0, 0];
[rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N, rP7_No_N, rP8_No_N,...
          N_R_P1, N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6, N_R_P7, N_R_P8, N_R_B  ] = ...
          octoRotorOptimiation2_MGanimationOutputs( xB, yB, zB, qBx, qBy, qBz, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, qA1x, qA1z, qA2x, qA2z, qA3x, qA3z, qA4x, qA4z, qA5x, qA5z, qA6x, qA6z, qA7x, qA7z, qA8x, qA8z, qP1z, qP2z, qP3z, qP4z, qP5z, qP6z, qP7z, qP8z, bThrust, bDrag, lArm );
propPositions = [rP1_No_N; rP2_No_N; rP3_No_N; rP4_No_N; rP5_No_N; rP6_No_N; rP7_No_N; rP8_No_N ]';

qP1_ZYX = rotm2eul(N_R_P1, 'ZYX');        
qP2_ZYX = rotm2eul(N_R_P2, 'ZYX');  
qP3_ZYX = rotm2eul(N_R_P3, 'ZYX');  
qP4_ZYX = rotm2eul(N_R_P4, 'ZYX');  
qP5_ZYX = rotm2eul(N_R_P5, 'ZYX');  
qP6_ZYX = rotm2eul(N_R_P6, 'ZYX');  
qP7_ZYX = rotm2eul(N_R_P7, 'ZYX');  
qP8_ZYX = rotm2eul(N_R_P8, 'ZYX');
qB_ZYX = rotm2eul(N_R_B, 'ZYX');
propOrientations = [qP1_ZYX, qP2_ZYX, qP3_ZYX, qP4_ZYX, qP5_ZYX, qP6_ZYX, qP7_ZYX, qP8_ZYX, qB_ZYX];
plotData.animationOutputs = [propPositions, propOrientations];
visInfo = octoRotor_visualization(plotData, params);



% save('propAngles2.mat' , 'propAngles2')




% wrench = calcWrench(out)


% qBx = -pi : 0.01:pi;

% for i = 1: length(qBx)
% [F_N(i,:) T_N(i,:) ] = calcWrench_N(qP2, qP4, qP5, qBx(i));
% end

% figure
% plot(F_N(:,2), F_N(:,3))
% xlabel('yThrust' )
% ylabel('zThrust')
% figure 
% plot(qBx, T_N(:,1))
% %plot(qBx, wrench_N(:,2))
% %plot(qBx, wrench_N(:,3))


%% Private Functions

function [c, cEq ]= nonLinConFunc(x)
c = 0;
cEq = 0;

end


function J = objectiveFunc(x, params)

FTMap = calc_FTMap(x,params);
J = cond(FTMap);
end

function J = objectiveFunc2(x, params)

FTMap = calc_FTMap(x,params);
wrench = FTMap*ones(8,1);
J = -norm(wrench);
end

function FTMap = calc_FTMap(x, params)
% Unpack Decision Variables
qP1x = x(1);
qP1y = x(2);
qP2x = x(3);
qP2y = x(4);
qP3x = x(5);
qP3y = x(6);
qP4x = x(7);
qP4y = x(8);
qP5x = x(9);
qP5y = x(10);
qP6x = x(11);
qP6y = x(12);
qP7x = x(13);
qP7y = x(14);
qP8x = x(15);
qP8y = x(16);

% Unpack parameters
mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPzz = params.IP(3,3);
bThrust = 1;%params.bThrust;
bDrag = 0;%params.bDrag;
g = params.g;

P1Dir = 1;%params.propDirections(1);
P2Dir = 1;%params.propDirections(2);
P3Dir = 1;%params.propDirections(3);
P4Dir = 1;%params.propDirections(4);
P5Dir = 1;%params.propDirections(5);
P6Dir = 1;%params.propDirections(6);
P7Dir = 1;%params.propDirections(7);
P8Dir = 1;%params.propDirections(8);

% P1Dir = params.propDirections(1);
% P2Dir = params.propDirections(2);
% P3Dir = params.propDirections(3);
% P4Dir = params.propDirections(4);
% P5Dir = params.propDirections(5);
% P6Dir = params.propDirections(6);
% P7Dir = params.propDirections(7);
% P8Dir = params.propDirections(8);

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);

xP7 = params.rP7_Bcm_B(1);
yP7 = params.rP7_Bcm_B(2);
zP7 = params.rP7_Bcm_B(3);

xP8 = params.rP8_Bcm_B(1);
yP8 = params.rP8_Bcm_B(2);
zP8 = params.rP8_Bcm_B(3);


FTMap = octoRotorRates_FTMap( P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, bThrust, bDrag );

end

