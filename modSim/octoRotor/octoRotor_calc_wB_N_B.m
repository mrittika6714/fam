function [SolutionToAlgebraicEquations] = octoRotor_calc_wB_N_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot )
if( nargin ~= 6 ) error( 'octoRotor_calc_wB_N_B expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotor_calc_wB_N_B.m created Oct 19 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 1651 );



%===========================================================================
z(1) = cos(qBy);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(14) = z(1)*z(8);
z(263) = z(1)*z(14) + z(4)*z(9);
z(1646) = z(14)/z(263);
z(1647) = z(9)/z(263);
z(1648) = z(3)*z(4)/z(263);
z(1649) = z(1)*z(3)/z(263);
z(1650) = z(4)/z(263);
z(1651) = z(1)/z(263);

COEF = zeros( 3, 3 );
COEF(1,1) = z(1646);
COEF(1,3) = z(1647);
COEF(2,1) = z(1648);
COEF(2,2) = 1;
COEF(2,3) = -z(1649);
COEF(3,1) = -z(1650);
COEF(3,3) = z(1651);
RHS = zeros( 1, 3 );
RHS(1) = qBxDot;
RHS(2) = qByDot;
RHS(3) = qBzDot;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBx = SolutionToAlgebraicEquations(1);
wBy = SolutionToAlgebraicEquations(2);
wBz = SolutionToAlgebraicEquations(3);



%==============================================
end    % End of function octoRotor_calc_wB_N_B
%==============================================
