function MzMat = octoRotorOptimiation2_CalcMzMat( qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, qA1x, qA1z, qA2x, qA2z, qA3x, qA3z, qA4x, qA4z, qA5x, qA5z, qA6x, qA6z, qA7x, qA7z, qA8x, qA8z )
if( nargin ~= 32 ) error( 'octoRotorOptimiation2_CalcMzMat expects 32 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotorOptimiation2_CalcMzMat.m created Dec 03 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
MzMat = zeros( 3, 8 );
z = zeros( 1, 733 );



%===========================================================================
z(23) = cos(qA1z);
z(24) = sin(qA1z);
z(25) = cos(qA1x);
z(26) = z(24)*z(25);
z(27) = z(23)*z(25);
z(28) = sin(qA1x);
z(29) = z(24)*z(28);
z(30) = z(23)*z(28);
z(31) = cos(qP1y);
z(32) = sin(qP1x);
z(33) = sin(qP1y);
z(35) = cos(qP1x);
z(37) = z(31)*z(32);
z(38) = z(31)*z(35);
z(43) = z(23)*z(33) + z(26)*z(37) + z(29)*z(38);
z(46) = z(24)*z(33) - z(27)*z(37) - z(30)*z(38);
z(49) = z(25)*z(38) - z(28)*z(37);
z(70) = cos(qA2z);
z(71) = sin(qA2z);
z(72) = cos(qA2x);
z(73) = z(71)*z(72);
z(74) = z(70)*z(72);
z(75) = sin(qA2x);
z(76) = z(71)*z(75);
z(77) = z(70)*z(75);
z(78) = cos(qP2y);
z(79) = sin(qP2x);
z(80) = sin(qP2y);
z(82) = cos(qP2x);
z(84) = z(78)*z(79);
z(85) = z(78)*z(82);
z(90) = z(70)*z(80) + z(73)*z(84) + z(76)*z(85);
z(93) = z(71)*z(80) - z(74)*z(84) - z(77)*z(85);
z(96) = z(72)*z(85) - z(75)*z(84);
z(117) = cos(qA3z);
z(118) = sin(qA3z);
z(119) = cos(qA3x);
z(120) = z(118)*z(119);
z(121) = z(117)*z(119);
z(122) = sin(qA3x);
z(123) = z(118)*z(122);
z(124) = z(117)*z(122);
z(125) = cos(qP3y);
z(126) = sin(qP3x);
z(127) = sin(qP3y);
z(129) = cos(qP3x);
z(131) = z(125)*z(126);
z(132) = z(125)*z(129);
z(137) = z(117)*z(127) + z(120)*z(131) + z(123)*z(132);
z(140) = z(118)*z(127) - z(121)*z(131) - z(124)*z(132);
z(143) = z(119)*z(132) - z(122)*z(131);
z(164) = cos(qA4z);
z(165) = sin(qA4z);
z(166) = cos(qA4x);
z(167) = z(165)*z(166);
z(168) = z(164)*z(166);
z(169) = sin(qA4x);
z(170) = z(165)*z(169);
z(171) = z(164)*z(169);
z(172) = cos(qP4y);
z(173) = sin(qP4x);
z(174) = sin(qP4y);
z(176) = cos(qP4x);
z(178) = z(172)*z(173);
z(179) = z(172)*z(176);
z(184) = z(164)*z(174) + z(167)*z(178) + z(170)*z(179);
z(187) = z(165)*z(174) - z(168)*z(178) - z(171)*z(179);
z(190) = z(166)*z(179) - z(169)*z(178);
z(211) = cos(qA5z);
z(212) = sin(qA5z);
z(213) = cos(qA5x);
z(214) = z(212)*z(213);
z(215) = z(211)*z(213);
z(216) = sin(qA5x);
z(217) = z(212)*z(216);
z(218) = z(211)*z(216);
z(219) = cos(qP5y);
z(220) = sin(qP5x);
z(221) = sin(qP5y);
z(223) = cos(qP5x);
z(225) = z(219)*z(220);
z(226) = z(219)*z(223);
z(231) = z(211)*z(221) + z(214)*z(225) + z(217)*z(226);
z(234) = z(212)*z(221) - z(215)*z(225) - z(218)*z(226);
z(237) = z(213)*z(226) - z(216)*z(225);
z(258) = cos(qA6z);
z(259) = sin(qA6z);
z(260) = cos(qA6x);
z(261) = z(259)*z(260);
z(262) = z(258)*z(260);
z(263) = sin(qA6x);
z(264) = z(259)*z(263);
z(265) = z(258)*z(263);
z(266) = cos(qP6y);
z(267) = sin(qP6x);
z(268) = sin(qP6y);
z(270) = cos(qP6x);
z(272) = z(266)*z(267);
z(273) = z(266)*z(270);
z(278) = z(258)*z(268) + z(261)*z(272) + z(264)*z(273);
z(281) = z(259)*z(268) - z(262)*z(272) - z(265)*z(273);
z(284) = z(260)*z(273) - z(263)*z(272);
z(305) = cos(qA7z);
z(306) = sin(qA7z);
z(307) = cos(qA7x);
z(308) = z(306)*z(307);
z(309) = z(305)*z(307);
z(310) = sin(qA7x);
z(311) = z(306)*z(310);
z(312) = z(305)*z(310);
z(313) = cos(qP7y);
z(314) = sin(qP7x);
z(315) = sin(qP7y);
z(317) = cos(qP7x);
z(319) = z(313)*z(314);
z(320) = z(313)*z(317);
z(325) = z(305)*z(315) + z(308)*z(319) + z(311)*z(320);
z(328) = z(306)*z(315) - z(309)*z(319) - z(312)*z(320);
z(331) = z(307)*z(320) - z(310)*z(319);
z(352) = cos(qA8z);
z(353) = sin(qA8z);
z(354) = cos(qA8x);
z(355) = z(353)*z(354);
z(356) = z(352)*z(354);
z(357) = sin(qA8x);
z(358) = z(353)*z(357);
z(359) = z(352)*z(357);
z(360) = cos(qP8y);
z(361) = sin(qP8x);
z(362) = sin(qP8y);
z(364) = cos(qP8x);
z(366) = z(360)*z(361);
z(367) = z(360)*z(364);
z(372) = z(352)*z(362) + z(355)*z(366) + z(358)*z(367);
z(375) = z(353)*z(362) - z(356)*z(366) - z(359)*z(367);
z(378) = z(354)*z(367) - z(357)*z(366);



%===========================================================================
Output = [];

MzMat(1,1) = z(43);
MzMat(1,2) = z(90);
MzMat(1,3) = z(137);
MzMat(1,4) = z(184);
MzMat(1,5) = z(231);
MzMat(1,6) = z(278);
MzMat(1,7) = z(325);
MzMat(1,8) = z(372);
MzMat(2,1) = z(46);
MzMat(2,2) = z(93);
MzMat(2,3) = z(140);
MzMat(2,4) = z(187);
MzMat(2,5) = z(234);
MzMat(2,6) = z(281);
MzMat(2,7) = z(328);
MzMat(2,8) = z(375);
MzMat(3,1) = z(49);
MzMat(3,2) = z(96);
MzMat(3,3) = z(143);
MzMat(3,4) = z(190);
MzMat(3,5) = z(237);
MzMat(3,6) = z(284);
MzMat(3,7) = z(331);
MzMat(3,8) = z(378);


%========================================================
end    % End of function octoRotorOptimiation2_CalcMzMat
%========================================================
