function [SolutionToAlgebraicEquations] = octoRotor_calc_alphaB_N_B( qBx, qBy, qBz, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz )
if( nargin ~= 9 ) error( 'octoRotor_calc_alphaB_N_B expects 9 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotor_calc_alphaB_N_B.m created Oct 19 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 1654 );



%===========================================================================
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(263) = z(1)*z(14) + z(4)*z(9);
z(264) = (wBx*z(14)+wBz*z(9))/z(263);
qBxDt = z(264);
z(3) = sin(qBx);
z(265) = (wBx*z(4)-wBz*z(1))/z(263);
z(266) = -wBy - z(3)*z(265);
qByDt = -z(266);
qBzDt = -z(265);
z(18) = z(1)*z(8)*qByDt - z(3)*z(4)*qBxDt;
z(20) = z(8)*qBxDt*qBzDt;
z(21) = -z(1)*z(3)*qBxDt - z(4)*z(8)*qByDt;
z(267) = z(4)*qBxDt*qByDt + qBzDt*z(18);
z(270) = -z(1)*qBxDt*qByDt - qBzDt*z(21);
z(1646) = z(14)/z(263);
z(1647) = z(9)/z(263);
z(1648) = z(3)*z(4)/z(263);
z(1649) = z(1)*z(3)/z(263);
z(1650) = z(4)/z(263);
z(1651) = z(1)/z(263);
z(1652) = qBxDDot - (z(9)*z(270)+z(14)*z(267))/z(263);
z(1653) = qByDDot + z(20) + z(3)*(z(1)*z(270)-z(4)*z(267))/z(263);
z(1654) = qBzDDot - (z(1)*z(270)-z(4)*z(267))/z(263);

COEF = zeros( 3, 3 );
COEF(1,1) = z(1646);
COEF(1,3) = z(1647);
COEF(2,1) = z(1648);
COEF(2,2) = 1;
COEF(2,3) = -z(1649);
COEF(3,1) = -z(1650);
COEF(3,3) = z(1651);
RHS = zeros( 1, 3 );
RHS(1) = z(1652);
RHS(2) = z(1653);
RHS(3) = z(1654);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBxDt = SolutionToAlgebraicEquations(1);
wByDt = SolutionToAlgebraicEquations(2);
wBzDt = SolutionToAlgebraicEquations(3);



%==================================================
end    % End of function octoRotor_calc_alphaB_N_B
%==================================================
