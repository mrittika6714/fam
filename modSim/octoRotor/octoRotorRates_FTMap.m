function FTMap = octoRotorRates_FTMap( P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, bThrust, bDrag )
if( nargin ~= 50 ) error( 'octoRotorRates_FTMap expects 50 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotorRates_FTMap.m created Dec 02 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
FTMap = zeros( 6, 8 );
z = zeros( 1, 1645 );



%===========================================================================


%===========================================================================
Output = [];

FTMap(1,1) = P1Dir*sin(qP1y);
FTMap(1,2) = P2Dir*sin(qP2y);
FTMap(1,3) = P3Dir*sin(qP3y);
FTMap(1,4) = P4Dir*sin(qP4y);
FTMap(1,5) = P5Dir*sin(qP5y);
FTMap(1,6) = P6Dir*sin(qP6y);
FTMap(1,7) = P7Dir*sin(qP7y);
FTMap(1,8) = P8Dir*sin(qP8y);
FTMap(2,1) = -P1Dir*sin(qP1x)*cos(qP1y);
FTMap(2,2) = -P2Dir*sin(qP2x)*cos(qP2y);
FTMap(2,3) = -P3Dir*sin(qP3x)*cos(qP3y);
FTMap(2,4) = -P4Dir*sin(qP4x)*cos(qP4y);
FTMap(2,5) = -P5Dir*sin(qP5x)*cos(qP5y);
FTMap(2,6) = -P6Dir*sin(qP6x)*cos(qP6y);
FTMap(2,7) = -P7Dir*sin(qP7x)*cos(qP7y);
FTMap(2,8) = -P8Dir*sin(qP8x)*cos(qP8y);
FTMap(3,1) = P1Dir*cos(qP1x)*cos(qP1y);
FTMap(3,2) = P2Dir*cos(qP2x)*cos(qP2y);
FTMap(3,3) = P3Dir*cos(qP3x)*cos(qP3y);
FTMap(3,4) = P4Dir*cos(qP4x)*cos(qP4y);
FTMap(3,5) = P5Dir*cos(qP5x)*cos(qP5y);
FTMap(3,6) = P6Dir*cos(qP6x)*cos(qP6y);
FTMap(3,7) = P7Dir*cos(qP7x)*cos(qP7y);
FTMap(3,8) = P8Dir*cos(qP8x)*cos(qP8y);
FTMap(4,1) = bDrag*sin(qP1y)/bThrust + P1Dir*cos(qP1y)*(yP1*cos(qP1x)+zP1*sin(qP1x));
FTMap(4,2) = bDrag*sin(qP2y)/bThrust + P2Dir*cos(qP2y)*(yP2*cos(qP2x)+zP2*sin(qP2x));
FTMap(4,3) = bDrag*sin(qP3y)/bThrust + P3Dir*cos(qP3y)*(yP3*cos(qP3x)+zP3*sin(qP3x));
FTMap(4,4) = bDrag*sin(qP4y)/bThrust + P4Dir*cos(qP4y)*(yP4*cos(qP4x)+zP4*sin(qP4x));
FTMap(4,5) = bDrag*sin(qP5y)/bThrust + P5Dir*cos(qP5y)*(yP5*cos(qP5x)+zP5*sin(qP5x));
FTMap(4,6) = bDrag*sin(qP6y)/bThrust + P6Dir*cos(qP6y)*(yP6*cos(qP6x)+zP6*sin(qP6x));
FTMap(4,7) = bDrag*sin(qP7y)/bThrust + P7Dir*cos(qP7y)*(yP7*cos(qP7x)+zP7*sin(qP7x));
FTMap(4,8) = bDrag*sin(qP8y)/bThrust + P8Dir*cos(qP8y)*(yP8*cos(qP8x)+zP8*sin(qP8x));
FTMap(5,1) = P1Dir*zP1*sin(qP1y) - P1Dir*xP1*cos(qP1x)*cos(qP1y) - bDrag*sin(qP1x)*cos(qP1y)/bThrust;
FTMap(5,2) = P2Dir*zP2*sin(qP2y) - P2Dir*xP2*cos(qP2x)*cos(qP2y) - bDrag*sin(qP2x)*cos(qP2y)/bThrust;
FTMap(5,3) = P3Dir*zP3*sin(qP3y) - P3Dir*xP3*cos(qP3x)*cos(qP3y) - bDrag*sin(qP3x)*cos(qP3y)/bThrust;
FTMap(5,4) = P4Dir*zP4*sin(qP4y) - P4Dir*xP4*cos(qP4x)*cos(qP4y) - bDrag*sin(qP4x)*cos(qP4y)/bThrust;
FTMap(5,5) = P5Dir*zP5*sin(qP5y) - P5Dir*xP5*cos(qP5x)*cos(qP5y) - bDrag*sin(qP5x)*cos(qP5y)/bThrust;
FTMap(5,6) = P6Dir*zP6*sin(qP6y) - P6Dir*xP6*cos(qP6x)*cos(qP6y) - bDrag*sin(qP6x)*cos(qP6y)/bThrust;
FTMap(5,7) = P7Dir*zP7*sin(qP7y) - P7Dir*xP7*cos(qP7x)*cos(qP7y) - bDrag*sin(qP7x)*cos(qP7y)/bThrust;
FTMap(5,8) = P8Dir*zP8*sin(qP8y) - P8Dir*xP8*cos(qP8x)*cos(qP8y) - bDrag*sin(qP8x)*cos(qP8y)/bThrust;
FTMap(6,1) = bDrag*cos(qP1x)*cos(qP1y)/bThrust - P1Dir*yP1*sin(qP1y) - P1Dir*xP1*sin(qP1x)*cos(qP1y);
FTMap(6,2) = bDrag*cos(qP2x)*cos(qP2y)/bThrust - P2Dir*yP2*sin(qP2y) - P2Dir*xP2*sin(qP2x)*cos(qP2y);
FTMap(6,3) = bDrag*cos(qP3x)*cos(qP3y)/bThrust - P3Dir*yP3*sin(qP3y) - P3Dir*xP3*sin(qP3x)*cos(qP3y);
FTMap(6,4) = bDrag*cos(qP4x)*cos(qP4y)/bThrust - P4Dir*yP4*sin(qP4y) - P4Dir*xP4*sin(qP4x)*cos(qP4y);
FTMap(6,5) = bDrag*cos(qP5x)*cos(qP5y)/bThrust - P5Dir*yP5*sin(qP5y) - P5Dir*xP5*sin(qP5x)*cos(qP5y);
FTMap(6,6) = bDrag*cos(qP6x)*cos(qP6y)/bThrust - P6Dir*yP6*sin(qP6y) - P6Dir*xP6*sin(qP6x)*cos(qP6y);
FTMap(6,7) = bDrag*cos(qP7x)*cos(qP7y)/bThrust - P7Dir*yP7*sin(qP7y) - P7Dir*xP7*sin(qP7x)*cos(qP7y);
FTMap(6,8) = bDrag*cos(qP8x)*cos(qP8y)/bThrust - P8Dir*yP8*sin(qP8y) - P8Dir*xP8*sin(qP8x)*cos(qP8y);


%=============================================
end    % End of function octoRotorRates_FTMap
%=============================================
