function [NMat, MMat, GMat, VMat, JVel_N, JDot_Vel_N] = octoRotor_dynamicMatrices( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz )
if( nargin ~= 77 ) error( 'octoRotor_dynamicMatrices expects 77 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotor_dynamicMatrices.m created Oct 18 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
NMat = zeros( 6, 8 );
MMat = zeros( 6, 6 );
GMat = zeros( 6, 1 );
VMat = zeros( 6, 1 );
JVel_N = zeros( 6, 6 );
JDot_Vel_N = zeros( 6, 6 );
z = zeros( 1, 1645 );



%===========================================================================
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(263) = z(1)*z(14) + z(4)*z(9);
z(264) = (wBx*z(14)+wBz*z(9))/z(263);
qBxDt = z(264);
z(3) = sin(qBx);
z(265) = (wBx*z(4)-wBz*z(1))/z(263);
z(266) = -wBy - z(3)*z(265);
qByDt = -z(266);
qBzDt = -z(265);
z(2) = cos(qBz);
z(5) = sin(qBz);
z(6) = z(1)*z(2) - z(3)*z(4)*z(5);
z(7) = z(1)*z(5) + z(2)*z(3)*z(4);
z(10) = z(5)*z(8);
z(11) = z(2)*z(8);
z(12) = z(2)*z(4) + z(1)*z(3)*z(5);
z(13) = z(4)*z(5) - z(1)*z(2)*z(3);
z(23) = cos(qP1y);
z(24) = sin(qP1x);
z(25) = sin(qP1y);
z(27) = cos(qP1x);
z(29) = z(23)*z(24);
z(30) = z(23)*z(27);
z(43) = z(25)*z(1) + z(30)*z(4);
z(44) = z(30)*z(14) - z(25)*z(9) - z(29)*z(3);
z(53) = cos(qP2y);
z(54) = sin(qP2x);
z(55) = sin(qP2y);
z(57) = cos(qP2x);
z(59) = z(53)*z(54);
z(60) = z(53)*z(57);
z(73) = z(55)*z(1) + z(60)*z(4);
z(74) = z(60)*z(14) - z(55)*z(9) - z(59)*z(3);
z(83) = cos(qP3y);
z(84) = sin(qP3x);
z(85) = sin(qP3y);
z(87) = cos(qP3x);
z(89) = z(83)*z(84);
z(90) = z(83)*z(87);
z(103) = z(85)*z(1) + z(90)*z(4);
z(104) = z(90)*z(14) - z(85)*z(9) - z(89)*z(3);
z(113) = cos(qP4y);
z(114) = sin(qP4x);
z(115) = sin(qP4y);
z(117) = cos(qP4x);
z(119) = z(113)*z(114);
z(120) = z(113)*z(117);
z(133) = z(115)*z(1) + z(120)*z(4);
z(134) = z(120)*z(14) - z(115)*z(9) - z(119)*z(3);
z(143) = cos(qP5y);
z(144) = sin(qP5x);
z(145) = sin(qP5y);
z(147) = cos(qP5x);
z(149) = z(143)*z(144);
z(150) = z(143)*z(147);
z(163) = z(145)*z(1) + z(150)*z(4);
z(164) = z(150)*z(14) - z(145)*z(9) - z(149)*z(3);
z(173) = cos(qP6y);
z(174) = sin(qP6x);
z(175) = sin(qP6y);
z(177) = cos(qP6x);
z(179) = z(173)*z(174);
z(180) = z(173)*z(177);
z(193) = z(175)*z(1) + z(180)*z(4);
z(194) = z(180)*z(14) - z(175)*z(9) - z(179)*z(3);
z(203) = cos(qP7y);
z(204) = sin(qP7x);
z(205) = sin(qP7y);
z(207) = cos(qP7x);
z(209) = z(203)*z(204);
z(210) = z(203)*z(207);
z(223) = z(205)*z(1) + z(210)*z(4);
z(224) = z(210)*z(14) - z(205)*z(9) - z(209)*z(3);
z(233) = cos(qP8y);
z(234) = sin(qP8x);
z(235) = sin(qP8y);
z(237) = cos(qP8x);
z(239) = z(233)*z(234);
z(240) = z(233)*z(237);
z(253) = z(235)*z(1) + z(240)*z(4);
z(254) = z(240)*z(14) - z(235)*z(9) - z(239)*z(3);
z(275) = yP1*wBx - xP1*wBy;
z(276) = xP1*wBz - zP1*wBx;
z(277) = zP1*wBy - yP1*wBz;
z(281) = yP2*wBx - xP2*wBy;
z(282) = xP2*wBz - zP2*wBx;
z(283) = zP2*wBy - yP2*wBz;
z(287) = yP3*wBx - xP3*wBy;
z(288) = xP3*wBz - zP3*wBx;
z(289) = zP3*wBy - yP3*wBz;
z(293) = yP4*wBx - xP4*wBy;
z(294) = xP4*wBz - zP4*wBx;
z(295) = zP4*wBy - yP4*wBz;
z(299) = yP5*wBx - xP5*wBy;
z(300) = xP5*wBz - zP5*wBx;
z(301) = zP5*wBy - yP5*wBz;
z(305) = yP6*wBx - xP6*wBy;
z(306) = xP6*wBz - zP6*wBx;
z(307) = zP6*wBy - yP6*wBz;
z(311) = yP7*wBx - xP7*wBy;
z(312) = xP7*wBz - zP7*wBx;
z(313) = zP7*wBy - yP7*wBz;
z(317) = yP8*wBx - xP8*wBy;
z(318) = xP8*wBz - zP8*wBx;
z(319) = zP8*wBy - yP8*wBz;
z(359) = z(25)*z(6) + z(29)*z(10) + z(30)*z(12);
z(362) = z(25)*z(7) + z(30)*z(13) - z(29)*z(11);
z(367) = z(55)*z(6) + z(59)*z(10) + z(60)*z(12);
z(370) = z(55)*z(7) + z(60)*z(13) - z(59)*z(11);
z(375) = z(85)*z(6) + z(89)*z(10) + z(90)*z(12);
z(378) = z(85)*z(7) + z(90)*z(13) - z(89)*z(11);
z(383) = z(115)*z(6) + z(119)*z(10) + z(120)*z(12);
z(386) = z(115)*z(7) + z(120)*z(13) - z(119)*z(11);
z(391) = z(145)*z(6) + z(149)*z(10) + z(150)*z(12);
z(394) = z(145)*z(7) + z(150)*z(13) - z(149)*z(11);
z(399) = z(175)*z(6) + z(179)*z(10) + z(180)*z(12);
z(402) = z(175)*z(7) + z(180)*z(13) - z(179)*z(11);
z(407) = z(205)*z(6) + z(209)*z(10) + z(210)*z(12);
z(410) = z(205)*z(7) + z(210)*z(13) - z(209)*z(11);
z(415) = z(235)*z(6) + z(239)*z(10) + z(240)*z(12);
z(418) = z(235)*z(7) + z(240)*z(13) - z(239)*z(11);
z(900) = mB + 8*mP;
z(1200) = IBxx*wBx + IBxy*wBy + IBxz*wBz;
z(1201) = IBxy*wBx + IByy*wBy + IByz*wBz;
z(1202) = IBxz*wBx + IByz*wBy + IBzz*wBz;
z(1203) = wBx*z(1201) - wBy*z(1200);
z(1204) = wBz*z(1200) - wBx*z(1202);
z(1205) = wBy*z(1202) - wBz*z(1201);
z(1208) = (z(14)*z(43)-z(4)*z(44)-z(29)*z(3)*z(4))/z(263);
z(1211) = (z(1)*z(44)+z(9)*z(43)+z(29)*z(1)*z(3))/z(263);
z(1214) = (z(14)*z(73)-z(4)*z(74)-z(59)*z(3)*z(4))/z(263);
z(1217) = (z(1)*z(74)+z(9)*z(73)+z(59)*z(1)*z(3))/z(263);
z(1220) = (z(14)*z(103)-z(4)*z(104)-z(89)*z(3)*z(4))/z(263);
z(1223) = (z(1)*z(104)+z(9)*z(103)+z(89)*z(1)*z(3))/z(263);
z(1226) = (z(14)*z(133)-z(4)*z(134)-z(119)*z(3)*z(4))/z(263);
z(1229) = (z(1)*z(134)+z(9)*z(133)+z(119)*z(1)*z(3))/z(263);
z(1232) = (z(14)*z(163)-z(4)*z(164)-z(149)*z(3)*z(4))/z(263);
z(1235) = (z(1)*z(164)+z(9)*z(163)+z(149)*z(1)*z(3))/z(263);
z(1238) = (z(14)*z(193)-z(4)*z(194)-z(179)*z(3)*z(4))/z(263);
z(1241) = (z(1)*z(194)+z(9)*z(193)+z(179)*z(1)*z(3))/z(263);
z(1244) = (z(14)*z(223)-z(4)*z(224)-z(209)*z(3)*z(4))/z(263);
z(1247) = (z(1)*z(224)+z(9)*z(223)+z(209)*z(1)*z(3))/z(263);
z(1250) = (z(14)*z(253)-z(4)*z(254)-z(239)*z(3)*z(4))/z(263);
z(1253) = (z(1)*z(254)+z(9)*z(253)+z(239)*z(1)*z(3))/z(263);
z(1261) = mP*(yP1*z(12)+yP2*z(12)+yP3*z(12)+yP4*z(12)+yP5*z(12)+yP6*z(12)+yP7*z(12)+yP8*z(12)+zP1*z(10)+zP2*z(10)+zP3*z(10)+zP4*  ...
z(10)+zP5*z(10)+zP6*z(10)+zP7*z(10)+zP8*z(10));
z(1262) = mP*(yP1*z(13)+yP2*z(13)+yP3*z(13)+yP4*z(13)+yP5*z(13)+yP6*z(13)+yP7*z(13)+yP8*z(13)-zP1*z(11)-zP2*z(11)-zP3*z(11)-zP4*  ...
z(11)-zP5*z(11)-zP6*z(11)-zP7*z(11)-zP8*z(11));
z(1263) = mP*(yP1*z(14)+yP2*z(14)+yP3*z(14)+yP4*z(14)+yP5*z(14)+yP6*z(14)+yP7*z(14)+yP8*z(14)-zP1*z(3)-zP2*z(3)-zP3*z(3)-zP4*z(3)-  ...
zP5*z(3)-zP6*z(3)-zP7*z(3)-zP8*z(3));
z(1276) = mP*(xP1*z(12)+xP2*z(12)+xP3*z(12)+xP4*z(12)+xP5*z(12)+xP6*z(12)+xP7*z(12)+xP8*z(12)-zP1*z(6)-zP2*z(6)-zP3*z(6)-zP4*z(6)-  ...
zP5*z(6)-zP6*z(6)-zP7*z(6)-zP8*z(6));
z(1277) = mP*(xP1*z(13)+xP2*z(13)+xP3*z(13)+xP4*z(13)+xP5*z(13)+xP6*z(13)+xP7*z(13)+xP8*z(13)-zP1*z(7)-zP2*z(7)-zP3*z(7)-zP4*z(7)-  ...
zP5*z(7)-zP6*z(7)-zP7*z(7)-zP8*z(7));
z(1278) = mP*(xP1*z(14)+xP2*z(14)+xP3*z(14)+xP4*z(14)+xP5*z(14)+xP6*z(14)+xP7*z(14)+xP8*z(14)+zP1*z(9)+zP2*z(9)+zP3*z(9)+zP4*z(9)+  ...
zP5*z(9)+zP6*z(9)+zP7*z(9)+zP8*z(9));
z(1284) = mP*(xP1*z(3)+xP2*z(3)+xP3*z(3)+xP4*z(3)+xP5*z(3)+xP6*z(3)+xP7*z(3)+xP8*z(3)+yP1*z(9)+yP2*z(9)+yP3*z(9)+yP4*z(9)+yP5*z(9)+  ...
yP6*z(9)+yP7*z(9)+yP8*z(9));
z(1285) = mP*(xP1*z(11)+xP2*z(11)+xP3*z(11)+xP4*z(11)+xP5*z(11)+xP6*z(11)+xP7*z(11)+xP8*z(11)-yP1*z(7)-yP2*z(7)-yP3*z(7)-yP4*z(7)-  ...
yP5*z(7)-yP6*z(7)-yP7*z(7)-yP8*z(7));
z(1286) = mP*(xP1*z(10)+xP2*z(10)+xP3*z(10)+xP4*z(10)+xP5*z(10)+xP6*z(10)+xP7*z(10)+xP8*z(10)+yP1*z(6)+yP2*z(6)+yP3*z(6)+yP4*z(6)+  ...
yP5*z(6)+yP6*z(6)+yP7*z(6)+yP8*z(6));
z(1295) = mP*(z(6)*(z(275)*wBy-z(276)*wBz)+z(6)*(z(281)*wBy-z(282)*wBz)+z(6)*(z(287)*wBy-z(288)*wBz)+z(6)*(z(293)*wBy-z(294)*wBz)+  ...
z(6)*(z(299)*wBy-z(300)*wBz)+z(6)*(z(305)*wBy-z(306)*wBz)+z(6)*(z(311)*wBy-z(312)*wBz)+z(6)*(z(317)*wBy-z(318)*wBz)+z(10)*(  ...
z(275)*wBx-z(277)*wBz)+z(10)*(z(281)*wBx-z(283)*wBz)+z(10)*(z(287)*wBx-z(289)*wBz)+z(10)*(z(293)*wBx-z(295)*wBz)+z(10)*(z(299)*wBx-  ...
z(301)*wBz)+z(10)*(z(305)*wBx-z(307)*wBz)+z(10)*(z(311)*wBx-z(313)*wBz)+z(10)*(z(317)*wBx-z(319)*wBz)+z(12)*(z(276)*wBx-z(277)*wBy)+  ...
z(12)*(z(282)*wBx-z(283)*wBy)+z(12)*(z(288)*wBx-z(289)*wBy)+z(12)*(z(294)*wBx-z(295)*wBy)+z(12)*(z(300)*wBx-z(301)*wBy)+z(12)*(  ...
z(306)*wBx-z(307)*wBy)+z(12)*(z(312)*wBx-z(313)*wBy)+z(12)*(z(318)*wBx-z(319)*wBy));
z(1296) = mP*(z(11)*(z(275)*wBx-z(277)*wBz)+z(11)*(z(281)*wBx-z(283)*wBz)+z(11)*(z(287)*wBx-z(289)*wBz)+z(11)*(z(293)*wBx-z(295)*  ...
wBz)+z(11)*(z(299)*wBx-z(301)*wBz)+z(11)*(z(305)*wBx-z(307)*wBz)+z(11)*(z(311)*wBx-z(313)*wBz)+z(11)*(z(317)*wBx-z(319)*wBz)-z(7)*(  ...
z(275)*wBy-z(276)*wBz)-z(7)*(z(281)*wBy-z(282)*wBz)-z(7)*(z(287)*wBy-z(288)*wBz)-z(7)*(z(293)*wBy-z(294)*wBz)-z(7)*(z(299)*wBy-  ...
z(300)*wBz)-z(7)*(z(305)*wBy-z(306)*wBz)-z(7)*(z(311)*wBy-z(312)*wBz)-z(7)*(z(317)*wBy-z(318)*wBz)-z(13)*(z(276)*wBx-z(277)*wBy)-  ...
z(13)*(z(282)*wBx-z(283)*wBy)-z(13)*(z(288)*wBx-z(289)*wBy)-z(13)*(z(294)*wBx-z(295)*wBy)-z(13)*(z(300)*wBx-z(301)*wBy)-z(13)*(  ...
z(306)*wBx-z(307)*wBy)-z(13)*(z(312)*wBx-z(313)*wBy)-z(13)*(z(318)*wBx-z(319)*wBy));
z(1297) = mP*(z(3)*(z(275)*wBx-z(277)*wBz)+z(3)*(z(281)*wBx-z(283)*wBz)+z(3)*(z(287)*wBx-z(289)*wBz)+z(3)*(z(293)*wBx-z(295)*wBz)+  ...
z(3)*(z(299)*wBx-z(301)*wBz)+z(3)*(z(305)*wBx-z(307)*wBz)+z(3)*(z(311)*wBx-z(313)*wBz)+z(3)*(z(317)*wBx-z(319)*wBz)+z(9)*(z(275)*  ...
wBy-z(276)*wBz)+z(9)*(z(281)*wBy-z(282)*wBz)+z(9)*(z(287)*wBy-z(288)*wBz)+z(9)*(z(293)*wBy-z(294)*wBz)+z(9)*(z(299)*wBy-z(300)*wBz)+  ...
z(9)*(z(305)*wBy-z(306)*wBz)+z(9)*(z(311)*wBy-z(312)*wBz)+z(9)*(z(317)*wBy-z(318)*wBz)-z(14)*(z(276)*wBx-z(277)*wBy)-z(14)*(  ...
z(282)*wBx-z(283)*wBy)-z(14)*(z(288)*wBx-z(289)*wBy)-z(14)*(z(294)*wBx-z(295)*wBy)-z(14)*(z(300)*wBx-z(301)*wBy)-z(14)*(z(306)*wBx-  ...
z(307)*wBy)-z(14)*(z(312)*wBx-z(313)*wBy)-z(14)*(z(318)*wBx-z(319)*wBy));
z(1298) = IBxx + mP*(yP1^2+zP1^2) + mP*(yP2^2+zP2^2) + mP*(yP3^2+zP3^2) + mP*(yP4^2+zP4^2) + mP*(yP5^2+zP5^2) + mP*(yP6^2+zP6^2)  ...
+ mP*(yP7^2+zP7^2) + mP*(yP8^2+zP8^2);
z(1300) = mP*xP1*yP1;
z(1301) = mP*xP2*yP2;
z(1302) = mP*xP3*yP3;
z(1303) = mP*xP4*yP4;
z(1304) = mP*xP5*yP5;
z(1305) = mP*xP6*yP6;
z(1306) = mP*xP7*yP7;
z(1307) = mP*xP8*yP8;
z(1309) = mP*xP1*zP1;
z(1310) = mP*xP2*zP2;
z(1311) = mP*xP3*zP3;
z(1312) = mP*xP4*zP4;
z(1313) = mP*xP5*zP5;
z(1314) = mP*xP6*zP6;
z(1315) = mP*xP7*zP7;
z(1316) = mP*xP8*zP8;
z(1319) = IBxy - mP*xP1*yP1 - mP*xP2*yP2 - mP*xP3*yP3 - mP*xP4*yP4 - mP*xP5*yP5 - mP*xP6*yP6 - mP*xP7*yP7 - mP*xP8*yP8;
z(1321) = IByy + mP*(xP1^2+zP1^2) + mP*(xP2^2+zP2^2) + mP*(xP3^2+zP3^2) + mP*(xP4^2+zP4^2) + mP*(xP5^2+zP5^2) + mP*(xP6^2+zP6^2)  ...
+ mP*(xP7^2+zP7^2) + mP*(xP8^2+zP8^2);
z(1323) = mP*yP1*zP1;
z(1324) = mP*yP2*zP2;
z(1325) = mP*yP3*zP3;
z(1326) = mP*yP4*zP4;
z(1327) = mP*yP5*zP5;
z(1328) = mP*yP6*zP6;
z(1329) = mP*yP7*zP7;
z(1330) = mP*yP8*zP8;
z(1333) = IBxz - mP*xP1*zP1 - mP*xP2*zP2 - mP*xP3*zP3 - mP*xP4*zP4 - mP*xP5*zP5 - mP*xP6*zP6 - mP*xP7*zP7 - mP*xP8*zP8;
z(1336) = IBzz + mP*(xP1^2+yP1^2) + mP*(xP2^2+yP2^2) + mP*(xP3^2+yP3^2) + mP*(xP4^2+yP4^2) + mP*(xP5^2+yP5^2) + mP*(xP6^2+yP6^2)  ...
+ mP*(xP7^2+yP7^2) + mP*(xP8^2+yP8^2);
z(1342) = yP1*z(30);
z(1343) = yP2*z(60);
z(1344) = yP3*z(90);
z(1345) = yP4*z(120);
z(1346) = yP5*z(150);
z(1347) = yP6*z(180);
z(1348) = yP7*z(210);
z(1349) = yP8*z(240);
z(1350) = zP1*z(29);
z(1351) = zP2*z(59);
z(1352) = zP3*z(89);
z(1353) = zP4*z(119);
z(1354) = zP5*z(149);
z(1355) = zP6*z(179);
z(1356) = zP7*z(209);
z(1357) = zP8*z(239);
z(1359) = zP1*z(25);
z(1360) = zP2*z(55);
z(1361) = zP3*z(85);
z(1362) = zP4*z(115);
z(1363) = zP5*z(145);
z(1364) = zP6*z(175);
z(1365) = zP7*z(205);
z(1366) = zP8*z(235);
z(1367) = xP1*z(30);
z(1368) = xP2*z(60);
z(1369) = xP3*z(90);
z(1370) = xP4*z(120);
z(1371) = xP5*z(150);
z(1372) = xP6*z(180);
z(1373) = xP7*z(210);
z(1374) = xP8*z(240);
z(1376) = xP1*z(29);
z(1377) = xP2*z(59);
z(1378) = xP3*z(89);
z(1379) = xP4*z(119);
z(1380) = xP5*z(149);
z(1381) = xP6*z(179);
z(1382) = xP7*z(209);
z(1383) = xP8*z(239);
z(1384) = yP1*z(25);
z(1385) = yP2*z(55);
z(1386) = yP3*z(85);
z(1387) = yP4*z(115);
z(1388) = yP5*z(145);
z(1389) = yP6*z(175);
z(1390) = yP7*z(205);
z(1391) = yP8*z(235);



%===========================================================================
Output = [];

NMat(1,1) = P1Dir*z(359);
NMat(1,2) = P2Dir*z(367);
NMat(1,3) = P3Dir*z(375);
NMat(1,4) = P4Dir*z(383);
NMat(1,5) = P5Dir*z(391);
NMat(1,6) = P6Dir*z(399);
NMat(1,7) = P7Dir*z(407);
NMat(1,8) = P8Dir*z(415);
NMat(2,1) = P1Dir*z(362);
NMat(2,2) = P2Dir*z(370);
NMat(2,3) = P3Dir*z(378);
NMat(2,4) = P4Dir*z(386);
NMat(2,5) = P5Dir*z(394);
NMat(2,6) = P6Dir*z(402);
NMat(2,7) = P7Dir*z(410);
NMat(2,8) = P8Dir*z(418);
NMat(3,1) = P1Dir*z(44);
NMat(3,2) = P2Dir*z(74);
NMat(3,3) = P3Dir*z(104);
NMat(3,4) = P4Dir*z(134);
NMat(3,5) = P5Dir*z(164);
NMat(3,6) = P6Dir*z(194);
NMat(3,7) = P7Dir*z(224);
NMat(3,8) = P8Dir*z(254);
NMat(4,1) = P1Dir*z(1342) + P1Dir*z(1350) + bDrag*z(1208)/bThrust;
NMat(4,2) = P2Dir*z(1343) + P2Dir*z(1351) + bDrag*z(1214)/bThrust;
NMat(4,3) = P3Dir*z(1344) + P3Dir*z(1352) + bDrag*z(1220)/bThrust;
NMat(4,4) = P4Dir*z(1345) + P4Dir*z(1353) + bDrag*z(1226)/bThrust;
NMat(4,5) = P5Dir*z(1346) + P5Dir*z(1354) + bDrag*z(1232)/bThrust;
NMat(4,6) = P6Dir*z(1347) + P6Dir*z(1355) + bDrag*z(1238)/bThrust;
NMat(4,7) = P7Dir*z(1348) + P7Dir*z(1356) + bDrag*z(1244)/bThrust;
NMat(4,8) = P8Dir*z(1349) + P8Dir*z(1357) + bDrag*z(1250)/bThrust;
NMat(5,1) = P1Dir*z(1359) - P1Dir*z(1367) - bDrag*z(29)/bThrust;
NMat(5,2) = P2Dir*z(1360) - P2Dir*z(1368) - bDrag*z(59)/bThrust;
NMat(5,3) = P3Dir*z(1361) - P3Dir*z(1369) - bDrag*z(89)/bThrust;
NMat(5,4) = P4Dir*z(1362) - P4Dir*z(1370) - bDrag*z(119)/bThrust;
NMat(5,5) = P5Dir*z(1363) - P5Dir*z(1371) - bDrag*z(149)/bThrust;
NMat(5,6) = P6Dir*z(1364) - P6Dir*z(1372) - bDrag*z(179)/bThrust;
NMat(5,7) = P7Dir*z(1365) - P7Dir*z(1373) - bDrag*z(209)/bThrust;
NMat(5,8) = P8Dir*z(1366) - P8Dir*z(1374) - bDrag*z(239)/bThrust;
NMat(6,1) = bDrag*z(1211)/bThrust - P1Dir*z(1376) - P1Dir*z(1384);
NMat(6,2) = bDrag*z(1217)/bThrust - P2Dir*z(1377) - P2Dir*z(1385);
NMat(6,3) = bDrag*z(1223)/bThrust - P3Dir*z(1378) - P3Dir*z(1386);
NMat(6,4) = bDrag*z(1229)/bThrust - P4Dir*z(1379) - P4Dir*z(1387);
NMat(6,5) = bDrag*z(1235)/bThrust - P5Dir*z(1380) - P5Dir*z(1388);
NMat(6,6) = bDrag*z(1241)/bThrust - P6Dir*z(1381) - P6Dir*z(1389);
NMat(6,7) = bDrag*z(1247)/bThrust - P7Dir*z(1382) - P7Dir*z(1390);
NMat(6,8) = bDrag*z(1253)/bThrust - P8Dir*z(1383) - P8Dir*z(1391);

MMat(1,1) = z(900);
MMat(1,2) = 0;
MMat(1,3) = 0;
MMat(1,4) = z(1261);
MMat(1,5) = -z(1276);
MMat(1,6) = -z(1286);
MMat(2,1) = 0;
MMat(2,2) = z(900);
MMat(2,3) = 0;
MMat(2,4) = z(1262);
MMat(2,5) = -z(1277);
MMat(2,6) = z(1285);
MMat(3,1) = 0;
MMat(3,2) = 0;
MMat(3,3) = z(900);
MMat(3,4) = z(1263);
MMat(3,5) = -z(1278);
MMat(3,6) = z(1284);
MMat(4,1) = z(1261);
MMat(4,2) = z(1262);
MMat(4,3) = z(1263);
MMat(4,4) = z(1298);
MMat(4,5) = IBxy - z(1300) - z(1301) - z(1302) - z(1303) - z(1304) - z(1305) - z(1306) - z(1307);
MMat(4,6) = IBxz - z(1309) - z(1310) - z(1311) - z(1312) - z(1313) - z(1314) - z(1315) - z(1316);
MMat(5,1) = -z(1276);
MMat(5,2) = -z(1277);
MMat(5,3) = -z(1278);
MMat(5,4) = z(1319);
MMat(5,5) = z(1321);
MMat(5,6) = IByz - z(1323) - z(1324) - z(1325) - z(1326) - z(1327) - z(1328) - z(1329) - z(1330);
MMat(6,1) = -z(1286);
MMat(6,2) = z(1285);
MMat(6,3) = z(1284);
MMat(6,4) = z(1333);
MMat(6,5) = IByz - z(1323) - z(1324) - z(1325) - z(1326) - z(1327) - z(1328) - z(1329) - z(1330);
MMat(6,6) = z(1336);

GMat(1) = 0;
GMat(2) = 0;
GMat(3) = g*(mB+8*mP);
GMat(4) = g*mP*(yP1*z(14)+yP2*z(14)+yP3*z(14)+yP4*z(14)+yP5*z(14)+yP6*z(14)+yP7*z(14)+yP8*z(14)-zP1*z(3)-zP2*z(3)-zP3*z(3)-zP4*z(  ...
3)-zP5*z(3)-zP6*z(3)-zP7*z(3)-zP8*z(3));
GMat(5) = -g*mP*(xP1*z(14)+xP2*z(14)+xP3*z(14)+xP4*z(14)+xP5*z(14)+xP6*z(14)+xP7*z(14)+xP8*z(14)+zP1*z(9)+zP2*z(9)+zP3*z(9)+zP4*z(  ...
9)+zP5*z(9)+zP6*z(9)+zP7*z(9)+zP8*z(9));
GMat(6) = g*mP*(xP1*z(3)+xP2*z(3)+xP3*z(3)+xP4*z(3)+xP5*z(3)+xP6*z(3)+xP7*z(3)+xP8*z(3)+yP1*z(9)+yP2*z(9)+yP3*z(9)+yP4*z(9)+yP5*z(  ...
9)+yP6*z(9)+yP7*z(9)+yP8*z(9));

VMat(1) = z(1295);
VMat(2) = -z(1296);
VMat(3) = -z(1297);
VMat(4) = mP*(yP1*(z(276)*wBx-z(277)*wBy)+zP1*(z(275)*wBx-z(277)*wBz)) + mP*(yP2*(z(282)*wBx-z(283)*wBy)+zP2*(z(281)*wBx-z(283)*wBz))  ...
+ mP*(yP3*(z(288)*wBx-z(289)*wBy)+zP3*(z(287)*wBx-z(289)*wBz)) + mP*(yP4*(z(294)*wBx-z(295)*wBy)+zP4*(z(293)*wBx-z(295)*wBz))  ...
+ mP*(yP5*(z(300)*wBx-z(301)*wBy)+zP5*(z(299)*wBx-z(301)*wBz)) + mP*(yP6*(z(306)*wBx-z(307)*wBy)+zP6*(z(305)*wBx-z(307)*wBz))  ...
+ mP*(yP7*(z(312)*wBx-z(313)*wBy)+zP7*(z(311)*wBx-z(313)*wBz)) + mP*(yP8*(z(318)*wBx-z(319)*wBy)+zP8*(z(317)*wBx-z(319)*wBz)) + z(1205);
VMat(5) = z(1204) - mP*(xP1*(z(276)*wBx-z(277)*wBy)-zP1*(z(275)*wBy-z(276)*wBz)) - mP*(xP2*(z(282)*wBx-z(283)*wBy)-zP2*(z(281)*wBy-  ...
z(282)*wBz)) - mP*(xP3*(z(288)*wBx-z(289)*wBy)-zP3*(z(287)*wBy-z(288)*wBz)) - mP*(xP4*(z(294)*wBx-z(295)*wBy)-zP4*(z(293)*wBy-  ...
z(294)*wBz)) - mP*(xP5*(z(300)*wBx-z(301)*wBy)-zP5*(z(299)*wBy-z(300)*wBz)) - mP*(xP6*(z(306)*wBx-z(307)*wBy)-zP6*(z(305)*wBy-  ...
z(306)*wBz)) - mP*(xP7*(z(312)*wBx-z(313)*wBy)-zP7*(z(311)*wBy-z(312)*wBz)) - mP*(xP8*(z(318)*wBx-z(319)*wBy)-zP8*(z(317)*wBy-  ...
z(318)*wBz));
VMat(6) = z(1203) - mP*(xP1*(z(275)*wBx-z(277)*wBz)+yP1*(z(275)*wBy-z(276)*wBz)) - mP*(xP2*(z(281)*wBx-z(283)*wBz)+yP2*(z(281)*wBy-  ...
z(282)*wBz)) - mP*(xP3*(z(287)*wBx-z(289)*wBz)+yP3*(z(287)*wBy-z(288)*wBz)) - mP*(xP4*(z(293)*wBx-z(295)*wBz)+yP4*(z(293)*wBy-  ...
z(294)*wBz)) - mP*(xP5*(z(299)*wBx-z(301)*wBz)+yP5*(z(299)*wBy-z(300)*wBz)) - mP*(xP6*(z(305)*wBx-z(307)*wBz)+yP6*(z(305)*wBy-  ...
z(306)*wBz)) - mP*(xP7*(z(311)*wBx-z(313)*wBz)+yP7*(z(311)*wBy-z(312)*wBz)) - mP*(xP8*(z(317)*wBx-z(319)*wBz)+yP8*(z(317)*wBy-  ...
z(318)*wBz));

JVel_N(:) = 0;
JVel_N(1,1) = 1;
JVel_N(2,2) = 1;
JVel_N(3,3) = 1;
JVel_N(4,4) = cos(qBy)*cos(qBz) - sin(qBx)*sin(qBy)*sin(qBz);
JVel_N(4,5) = -sin(qBz)*cos(qBx);
JVel_N(4,6) = sin(qBy)*cos(qBz) + sin(qBx)*sin(qBz)*cos(qBy);
JVel_N(5,4) = sin(qBz)*cos(qBy) + sin(qBx)*sin(qBy)*cos(qBz);
JVel_N(5,5) = cos(qBx)*cos(qBz);
JVel_N(5,6) = sin(qBy)*sin(qBz) - sin(qBx)*cos(qBy)*cos(qBz);
JVel_N(6,4) = -sin(qBy)*cos(qBx);
JVel_N(6,5) = sin(qBx);
JVel_N(6,6) = cos(qBx)*cos(qBy);

JDot_Vel_N(:) = 0;
JDot_Vel_N(4,4) = -sin(qBy)*cos(qBz)*qByDt - sin(qBz)*cos(qBy)*qBzDt - sin(qBx)*sin(qBy)*cos(qBz)*qBzDt - sin(qBx)*sin(qBz)*cos(qBy)*qByDt  ...
- sin(qBy)*sin(qBz)*cos(qBx)*qBxDt;
JDot_Vel_N(4,5) = sin(qBx)*sin(qBz)*qBxDt - cos(qBx)*cos(qBz)*qBzDt;
JDot_Vel_N(4,6) = cos(qBy)*cos(qBz)*qByDt + sin(qBx)*cos(qBy)*cos(qBz)*qBzDt + sin(qBz)*cos(qBx)*cos(qBy)*qBxDt - sin(qBy)*sin(qBz)*qBzDt  ...
- sin(qBx)*sin(qBy)*sin(qBz)*qByDt;
JDot_Vel_N(5,4) = cos(qBy)*cos(qBz)*qBzDt + sin(qBx)*cos(qBy)*cos(qBz)*qByDt + sin(qBy)*cos(qBx)*cos(qBz)*qBxDt - sin(qBy)*sin(qBz)*qByDt  ...
- sin(qBx)*sin(qBy)*sin(qBz)*qBzDt;
JDot_Vel_N(5,5) = -sin(qBx)*cos(qBz)*qBxDt - sin(qBz)*cos(qBx)*qBzDt;
JDot_Vel_N(5,6) = sin(qBy)*cos(qBz)*qBzDt + sin(qBz)*cos(qBy)*qByDt + sin(qBx)*sin(qBy)*cos(qBz)*qByDt + sin(qBx)*sin(qBz)*cos(qBy)*qBzDt  ...
- cos(qBx)*cos(qBy)*cos(qBz)*qBxDt;
JDot_Vel_N(6,4) = sin(qBx)*sin(qBy)*qBxDt - cos(qBx)*cos(qBy)*qByDt;
JDot_Vel_N(6,5) = cos(qBx)*qBxDt;
JDot_Vel_N(6,6) = -sin(qBx)*cos(qBy)*qBxDt - sin(qBy)*cos(qBx)*qByDt;


%==================================================
end    % End of function octoRotor_dynamicMatrices
%==================================================
