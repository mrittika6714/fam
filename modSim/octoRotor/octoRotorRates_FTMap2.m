function FTMap = octoRotorRates_FTMap2( P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, bThrust, bDrag )
if( nargin ~= 50 ) error( 'octoRotorRates_FTMap2 expects 50 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: octoRotorRates_FTMap2.m created Nov 29 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
FTMap = zeros( 6, 8 );
z = zeros( 1, 476 );



%===========================================================================


%===========================================================================
Output = [];

FTMap(1,1) = sin(qP1y);
FTMap(1,2) = sin(qP2y);
FTMap(1,3) = sin(qP3y);
FTMap(1,4) = sin(qP4y);
FTMap(1,5) = sin(qP5y);
FTMap(1,6) = sin(qP6y);
FTMap(1,7) = sin(qP7y);
FTMap(1,8) = sin(qP8y);
FTMap(2,1) = -sin(qP1x)*cos(qP1y);
FTMap(2,2) = -sin(qP2x)*cos(qP2y);
FTMap(2,3) = -sin(qP3x)*cos(qP3y);
FTMap(2,4) = -sin(qP4x)*cos(qP4y);
FTMap(2,5) = -sin(qP5x)*cos(qP5y);
FTMap(2,6) = -sin(qP6x)*cos(qP6y);
FTMap(2,7) = -sin(qP7x)*cos(qP7y);
FTMap(2,8) = -sin(qP8x)*cos(qP8y);
FTMap(3,1) = cos(qP1x)*cos(qP1y);
FTMap(3,2) = cos(qP2x)*cos(qP2y);
FTMap(3,3) = cos(qP3x)*cos(qP3y);
FTMap(3,4) = cos(qP4x)*cos(qP4y);
FTMap(3,5) = cos(qP5x)*cos(qP5y);
FTMap(3,6) = cos(qP6x)*cos(qP6y);
FTMap(3,7) = cos(qP7x)*cos(qP7y);
FTMap(3,8) = cos(qP8x)*cos(qP8y);
FTMap(4,1) = cos(qP1y)*(yP1*cos(qP1x)+zP1*sin(qP1x));
FTMap(4,2) = cos(qP2y)*(yP2*cos(qP2x)+zP2*sin(qP2x));
FTMap(4,3) = cos(qP3y)*(yP3*cos(qP3x)+zP3*sin(qP3x));
FTMap(4,4) = cos(qP4y)*(yP4*cos(qP4x)+zP4*sin(qP4x));
FTMap(4,5) = cos(qP5y)*(yP5*cos(qP5x)+zP5*sin(qP5x));
FTMap(4,6) = cos(qP6y)*(yP6*cos(qP6x)+zP6*sin(qP6x));
FTMap(4,7) = cos(qP7y)*(yP7*cos(qP7x)+zP7*sin(qP7x));
FTMap(4,8) = cos(qP8y)*(yP8*cos(qP8x)+zP8*sin(qP8x));
FTMap(5,1) = zP1*sin(qP1y) - xP1*cos(qP1x)*cos(qP1y);
FTMap(5,2) = zP2*sin(qP2y) - xP2*cos(qP2x)*cos(qP2y);
FTMap(5,3) = zP3*sin(qP3y) - xP3*cos(qP3x)*cos(qP3y);
FTMap(5,4) = zP4*sin(qP4y) - xP4*cos(qP4x)*cos(qP4y);
FTMap(5,5) = zP5*sin(qP5y) - xP5*cos(qP5x)*cos(qP5y);
FTMap(5,6) = zP6*sin(qP6y) - xP6*cos(qP6x)*cos(qP6y);
FTMap(5,7) = zP7*sin(qP7y) - xP7*cos(qP7x)*cos(qP7y);
FTMap(5,8) = zP8*sin(qP8y) - xP8*cos(qP8x)*cos(qP8y);
FTMap(6,1) = -yP1*sin(qP1y) - xP1*sin(qP1x)*cos(qP1y);
FTMap(6,2) = -yP2*sin(qP2y) - xP2*sin(qP2x)*cos(qP2y);
FTMap(6,3) = -yP3*sin(qP3y) - xP3*sin(qP3x)*cos(qP3y);
FTMap(6,4) = -yP4*sin(qP4y) - xP4*sin(qP4x)*cos(qP4y);
FTMap(6,5) = -yP5*sin(qP5y) - xP5*sin(qP5x)*cos(qP5y);
FTMap(6,6) = -yP6*sin(qP6y) - xP6*sin(qP6x)*cos(qP6y);
FTMap(6,7) = -yP7*sin(qP7y) - xP7*sin(qP7x)*cos(qP7y);
FTMap(6,8) = -yP8*sin(qP8y) - xP8*sin(qP8x)*cos(qP8y);


%==============================================
end    % End of function octoRotorRates_FTMap2
%==============================================
