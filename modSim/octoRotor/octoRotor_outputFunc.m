function status = octoRotor_outputFunc(t,y,flag,params) 


if strcmp(flag, 'init')
    
elseif strcmp(flag, 'done')
   
else
    trajectories = params.trajectory;
    trajectoryFunc = params.trajectory.trajectoryFunc;
    u = @(t, states)  params.controller.func(t, states, trajectories, trajectoryFunc, params);
     for i = 1:length(t)
    statesDot = octoRotor_dynamics( t(i), y(:,i), params, u);
    octoRotor_stateDotHist('add', t, statesDot');
     end
end
displayStatus_dots(t,y,flag); 
status = 0;
end



