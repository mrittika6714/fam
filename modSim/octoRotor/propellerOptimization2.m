

clc;clear;close all;
format long;
simParams.control.type = 'none';
simParams.control.space = 'none';
simParams.control.func = @() zeros(1,1);
simParams.trajectoryFunc = @() zeros(1,1);
simParams.tFinal = 0;
params = octoRotor_params(simParams);
options = optimoptions('fmincon');
options = optimoptions(options,'OptimalityTolerance',1e-14,'StepTolerance',1e-14,'constraintTolerance',1e-14,'MaxIterations',100000000,'MaxFunctionEvaluations',1000000000,'Display', 'iter');
load('propAngles.mat')
propAngles = ones(8,1) * acos(2/sqrt(6)).*[-1;-1;-1;-1;1;1;1;1];
x0 = propAngles(1:8);%ones(16,1) * pi/4; 
A= [];
B = [];
lb = ones(8,1) * (-2*pi);
ub = ones(8,1) * 2*pi;
objectiveFuncHandle = @(x) objectiveFunc(x,params);

[propAngles fval, exitFlag] = fmincon(objectiveFuncHandle, x0, A, B,[],[],lb, ub, @nonLinConFunc, options);
 180/pi*propAngles

FTMap = calc_FTMap3(propAngles, params)
wrench = FTMap * ones(8,1).*[-1;-1;-1;1;1;1]
%Note wrench seems wrong here
rank(FTMap)
[U,S,V] = svd(FTMap)
objectiveFunc(propAngles,params)
xB = 0;
yB = 0;
zB = 0;
qBx = 0;
qBy = 0;
qBz = 0;

qP1y = propAngles(1);
qP2y = propAngles(2);
qP3y = propAngles(3);
qP4y = propAngles(4);
qP5y = propAngles(5);
qP6y = propAngles(6);
qP7y = propAngles(7);
qP8y = propAngles(8);


qP1x = 0;
qP2x = 0;
qP3x = 0;
qP4x = 0;
qP5x = 0;
qP6x = 0;
qP7x = 0;
qP8x = 0;


qA1x = acos(2/sqrt(6));
qA1z = pi/4;
qA2x = acos(2/sqrt(6));
qA2z = 3*pi/4;
qA3x = acos(2/sqrt(6));
qA3z = 5*pi/4;
qA4x = acos(2/sqrt(6));
qA4z = 7*pi/4;
qA5x = -acos(2/sqrt(6));
qA5z = pi/4;
qA6x = -acos(2/sqrt(6));
qA6z = 3*pi/4;
qA7x = -acos(2/sqrt(6));
qA7z = 5*pi/4;
qA8x = -acos(2/sqrt(6));
qA8z = 7*pi/4;
MzMat = octoRotorOptimiation2_CalcMzMat( qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, qA1x, qA1z, qA2x, qA2z, qA3x, qA3z, qA4x, qA4z, qA5x, qA5z, qA6x, qA6z, qA7x, qA7z, qA8x, qA8z )




qP1z = 0;
qP2z = 0;
qP3z = 0;
qP4z = 0;
qP5z = 0;
qP6z = 0;
qP7z = 0;
qP8z = 0;
bThrust = 1;
bDrag = 0;
lArm = 1;

plotData.states = [0, 0, 0];
[rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N, rP7_No_N, rP8_No_N,...
          N_R_P1, N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6, N_R_P7, N_R_P8, N_R_B  ] = ...
          octoRotorOptimiation2_MGanimationOutputs( xB, yB, zB, qBx, qBy, qBz, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, qA1x, qA1z, qA2x, qA2z, qA3x, qA3z, qA4x, qA4z, qA5x, qA5z, qA6x, qA6z, qA7x, qA7z, qA8x, qA8z, qP1z, qP2z, qP3z, qP4z, qP5z, qP6z, qP7z, qP8z, bThrust, bDrag, lArm );
propPositions = [rP1_No_N; rP2_No_N; rP3_No_N; rP4_No_N; rP5_No_N; rP6_No_N; rP7_No_N; rP8_No_N ]';

qP1_ZYX = rotm2eul(N_R_P1, 'ZYX');        
qP2_ZYX = rotm2eul(N_R_P2, 'ZYX');  
qP3_ZYX = rotm2eul(N_R_P3, 'ZYX');  
qP4_ZYX = rotm2eul(N_R_P4, 'ZYX');  
qP5_ZYX = rotm2eul(N_R_P5, 'ZYX');  
qP6_ZYX = rotm2eul(N_R_P6, 'ZYX');  
qP7_ZYX = rotm2eul(N_R_P7, 'ZYX');  
qP8_ZYX = rotm2eul(N_R_P8, 'ZYX');
qB_ZYX = rotm2eul(N_R_B, 'ZYX');
propOrientations = [qP1_ZYX, qP2_ZYX, qP3_ZYX, qP4_ZYX, qP5_ZYX, qP6_ZYX, qP7_ZYX, qP8_ZYX, qB_ZYX];
plotData.animationOutputs = [propPositions, propOrientations];
visInfo = octoRotor_visualization(plotData, params);


save('propAngles2Long.mat' , 'propAngles')




% wrench = calcWrench(out)


% qBx = -pi : 0.01:pi;

% for i = 1: length(qBx)
% [F_N(i,:) T_N(i,:) ] = calcWrench_N(qP2, qP4, qP5, qBx(i));
% end

% figure
% plot(F_N(:,2), F_N(:,3))
% xlabel('yThrust' )
% ylabel('zThrust')
% figure 
% plot(qBx, T_N(:,1))
% %plot(qBx, wrench_N(:,2))
% %plot(qBx, wrench_N(:,3))


%% Private Functions

function [c, cEq ]= nonLinConFunc(x)
c = 0;
cEq = 0;

end


function J = objectiveFunc(x, params)

FTMap = calc_FTMap3(x,params);
J = cond(FTMap);
end

function J = objectiveFunc2(x, params)

FTMap = calc_FTMap(x,params);
wrench = FTMap*ones(8,1);
J = -norm(wrench);
end



function FTMap = calc_FTMap3(x, params)
% Unpack Decision Variables
qP1y = x(1);
qP2y = x(2);
qP3y = x(3);
qP4y = x(4);
qP5y = x(5);
qP6y = x(6);
qP7y = x(7);
qP8y = x(8);

qP1x = 0;
qP2x = 0;
qP3x = 0;
qP4x = 0;
qP5x = 0;
qP6x = 0;
qP7x = 0;
qP8x = 0;


bThrust = 1;%params.bThrust;
bDrag = 0;%params.bDrag;

P1Dir = 1;%params.propDirections(1);
P2Dir = 1;%params.propDirections(2);
P3Dir = 1;%params.propDirections(3);
P4Dir = 1;%params.propDirections(4);
P5Dir = 1;%params.propDirections(5);
P6Dir = 1;%params.propDirections(6);
P7Dir = 1;%params.propDirections(7);
P8Dir = 1;%params.propDirections(8);

% P1Dir = params.propDirections(1);
% P2Dir = params.propDirections(2);
% P3Dir = params.propDirections(3);
% P4Dir = params.propDirections(4);
% P5Dir = params.propDirections(5);
% P6Dir = params.propDirections(6);
% P7Dir = params.propDirections(7);
% P8Dir = params.propDirections(8);


qA1x = acos(2/sqrt(6));
qA1z = pi/4;
qA2x = acos(2/sqrt(6));
qA2z = 3*pi/4;
qA3x = acos(2/sqrt(6));
qA3z = 5*pi/4;
qA4x = acos(2/sqrt(6));
qA4z = 7*pi/4;
qA5x = -acos(2/sqrt(6));
qA5z = pi/4;
qA6x = -acos(2/sqrt(6));
qA6z = 3*pi/4;
qA7x = -acos(2/sqrt(6));
qA7z = 5*pi/4;
qA8x = -acos(2/sqrt(6));
qA8z = 7*pi/4;

lArm = 1;
FTMap = octoRotorRates_FTMap3( P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y, qP5x, qP5y, qP6x, qP6y, qP7x, qP7y, qP8x, qP8y, qA1x, qA1z, qA2x, qA2z, qA3x, qA3z, qA4x, qA4z, qA5x, qA5z, qA6x, qA6z, qA7x, qA7z, qA8x, qA8z, bThrust, bDrag, lArm );
end


