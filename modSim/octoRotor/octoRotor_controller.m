% ToDo Look into using Jacobian for acceleration defined in task space to
% acceleration in generalized coordinates i.e. (joint space).

function control = octoRotor_controller(t, states, trajectories, trajectoryFunc, params)


% if strcmp(params.controlCallFlag , 'ODE45OutputFcn' 
% 
% else
% 
% end

[tLast, stateDottLast] = octoRotor_stateDotHist('getLast', 0, 0);
% Unpack parameters
mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPzz = params.IP(3,3);
bThrust = params.bThrust;
bDrag = params.bDrag;
g = params.g;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);
P5Dir = params.propDirections(5);
P6Dir = params.propDirections(6);
P7Dir = params.propDirections(7);
P8Dir = params.propDirections(8);

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(2);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(2);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(2);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);
qP5x = params.qP5_XYZ(1);
qP5y = params.qP5_XYZ(2);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);
qP6x = params.qP6_XYZ(1);
qP6y = params.qP6_XYZ(2);

xP7 = params.rP7_Bcm_B(1);
yP7 = params.rP7_Bcm_B(2);
zP7 = params.rP7_Bcm_B(3);
qP7x = params.qP7_XYZ(1);
qP7y = params.qP7_XYZ(2);

xP8 = params.rP8_Bcm_B(1);
yP8 = params.rP8_Bcm_B(2);
zP8 = params.rP8_Bcm_B(3);
qP8x = params.qP8_XYZ(1);
qP8y = params.qP8_XYZ(2);

controllerType = params.controller.type;
controllerSpace = params.controller.space;
numTrajectories =  params.trajectory.numTrajectories;
epsilon = 0.0000001;
maxIter = 1000;


%Unpack States
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
xBDt = states(7);
yBDt = states(8);
zBDt = states(9);
wBx = states(10);
wBy = states(11);
wBz = states(12);
qP1z = states(13);
qP2z = states(14);
qP3z = states(15);
qP4z = states(16);
qP5z = states(17);
qP6z = states(18);
qP7z = states(19);
qP8z = states(20);

qP1zDt = stateDottLast(13);
qP2zDt = stateDottLast(14);
qP3zDt = stateDottLast(15);
qP4zDt = stateDottLast(16);
qP5zDt = stateDottLast(17);
qP6zDt = stateDottLast(18);
qP7zDt = stateDottLast(19);
qP8zDt = stateDottLast(20);

% Unpack Desired Trajectory 
if (strcmp(controllerSpace, 'Task') || strcmp(controllerSpace, 'task') )
    [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);

    [taskPos, taskVel] = octorRotor_ForwardKinematics( qBx, qBy, qBz, xB, yB, zB, wBx, wBy, wBz, xBDt, yBDt, zBDt );
    
    error = taskPos - posDes';
    errorDot = taskVel - vDes';
else
    [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);

    % Calculate errors
%     error = posDes' - states(1:8);
% 
%     jointRates = [rBDot; qBDot; qL1Dt; qL2Dt];
%     errorDot = vDes' - jointRates;
%     errorSum = errorSum + error;
end
if strcmp(controllerType, 'none')
   control = zeros(8, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;

elseif strcmp(controllerType, 'LQR')

elseif strcmp(controllerType, 'FF_PID_Task')

    kp = params.controller.kp;
    kd = params.controller.kd;
    ki = params.controller.ki;
    a_Control_Task = aDes' - kd.*errorDot - kp .* error; %- ki * errorSum; 
    xBDDot = a_Control_Task(1);
    yBDDot = a_Control_Task(2);
    zBDDot = a_Control_Task(3);
    qBxDDot = a_Control_Task(4);
    qByDDot = a_Control_Task(5);
    qBzDDot = a_Control_Task(6);
    qBxDot = vDes(4); 
    qByDot = vDes(5); 
    qBzDot = vDes(6); 

    aBcm_N_N =  [xBDDot; yBDDot; zBDDot];
%     [wB_N_B] = octoRotor_calc_wB_N_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot );
%     wBx = wB_N_B(1);
%     wBy = wB_N_B(2);
%     wBz = wB_N_B(3);
    alphaB_N_B = octoRotor_calc_alphaB_N_B( qBx, qBy, qBz, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz );
    a_Control_GenCoord = [aBcm_N_N; alphaB_N_B; ];
    rateError = mean(stateDottLast(13:20).^2);
    i=0;
    while ((rateError >= epsilon && i<maxIter) || i==0)
        [NMat, MMat, GMat, VMat, ~, ~] = octoRotor_dynamicMatricesSimple( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, qP7zDt, qP8zDt );
        %[NMat, MMat, GMat, VMat, ~, ~] = octoRotor_dynamicMatricesSuperSimple(mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz );
        control = pinv(NMat) * (MMat * a_Control_GenCoord  + VMat + GMat);
    
    % Convert Forces to propellor rates
    control = sign(control).*sqrt(abs(control)/bThrust);

    FFPropRates = [qP1zDt; qP2zDt;  qP3zDt; qP4zDt; qP5zDt; qP6zDt; qP7zDt; qP8zDt];
    qP1zDt = control(1);
    qP2zDt = control(2);
    qP3zDt = control(3);
    qP4zDt = control(4);
    qP5zDt = control(5);
    qP6zDt = control(6);
    qP7zDt = control(7);
    qP8zDt = control(8);
    rateError = immse(control,FFPropRates);
    i = i+1;
    end
    
elseif strcmp(controllerType, 'FF_Task')

    a_Control_Task = aDes';
    xBDDot = a_Control_Task(1);
    yBDDot = a_Control_Task(2);
    zBDDot = a_Control_Task(3);
    qBxDDot = a_Control_Task(4);
    qByDDot = a_Control_Task(5);
    qBzDDot = a_Control_Task(6);
    qBxDot = vDes(4); 
    qByDot = vDes(5); 
    qBzDot = vDes(6); 

    aBcm_N_N =  [xBDDot; yBDDot; zBDDot];
%     [wB_N_B] = octoRotor_calc_wB_N_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot );
%     wBx = wB_N_B(1);
%     wBy = wB_N_B(2);
%     wBz = wB_N_B(3);
    alphaB_N_B = octoRotor_calc_alphaB_N_B( qBx, qBy, qBz, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz );
    a_Control_GenCoord = [aBcm_N_N; alphaB_N_B; ];
    rateError = mean(stateDottLast(13:20).^2);
    i=0;
    while ((rateError >= epsilon && i<maxIter) || i==0)
        [NMat, MMat, GMat, VMat, ~, ~] = octoRotor_dynamicMatricesSimple( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, qP7zDt, qP8zDt );
        %[NMat, MMat, GMat, VMat, ~, ~] = octoRotor_dynamicMatricesSuperSimple(mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz );
        control = pinv(NMat) * (MMat * a_Control_GenCoord  + VMat + GMat);
    
    % Convert Forces to propellor rates
    control = sign(control).*sqrt(abs(control)/bThrust);

    FFPropRates = [qP1zDt; qP2zDt;  qP3zDt; qP4zDt; qP5zDt; qP6zDt; qP7zDt; qP8zDt];
    qP1zDt = control(1);
    qP2zDt = control(2);
    qP3zDt = control(3);
    qP4zDt = control(4);
    qP5zDt = control(5);
    qP6zDt = control(6);
    qP7zDt = control(7);
    qP8zDt = control(8);
    rateError = immse(control,FFPropRates);
    i = i+1;
    end

else
  error('Controller Type not handled');
end 

end