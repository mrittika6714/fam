function params = octoRotor_params(simParams)

%% Store simulation Params
params.sim = simParams;

propAngles = load('propAngles.mat');
qP1x = propAngles.propAngles(1);
qP1y = propAngles.propAngles(2);
qP2x = propAngles.propAngles(3);
qP2y = propAngles.propAngles(4);
qP3x = propAngles.propAngles(5);
qP3y = propAngles.propAngles(6);
qP4x = propAngles.propAngles(7);
qP4y = propAngles.propAngles(8);
qP5x = propAngles.propAngles(9);
qP5y = propAngles.propAngles(10);
qP6x = propAngles.propAngles(11);
qP6y = propAngles.propAngles(12);
qP7x = propAngles.propAngles(13);
qP7y = propAngles.propAngles(14);
qP8x = propAngles.propAngles(15);
qP8y = propAngles.propAngles(16);

params.IB = [1.16*10^-2, 0, 0; 0, 1.13*10^-2, 0; 0, 0, 1.13*10^-2];
params.mB = 0.892;
params.g = 9.8;

params.mP = 0.0001;
%params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 0];
params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 3.41*10^-5];
lArm = 1;%0.186;
params.lA = lArm;


numProps = 8;
% Configuration for planar octocoptor
% propPosAngle = linspace(0,2*pi,numProps+1); % rad
% xPropPosVec = lArm * cos(propPosAngle');
% yPropPosVec  = lArm * sin(propPosAngle');
% zPropPosVec = zeros(numProps,1);

% Configuration for cubic octocoptor
axisLength = sqrt(lArm^2/3);
xPropPosVec = [axisLength  -axisLength  axisLength  -axisLength ...
               axisLength -axisLength axisLength -axisLength ];

yPropPosVec = [-axisLength  -axisLength  axisLength  axisLength ...
               -axisLength -axisLength axisLength axisLength ];

zPropPosVec = [axisLength  axisLength  axisLength  axisLength ...
               -axisLength -axisLength -axisLength -axisLength ];

params.rP1_Bcm_B = [xPropPosVec(1), yPropPosVec(1), zPropPosVec(1)];
params.qP1_XYZ = [qP1x; qP1y; 0];
params.rP2_Bcm_B = [xPropPosVec(2), yPropPosVec(2), zPropPosVec(2)];
params.qP2_XYZ = [qP2x; qP2y; 0];
params.rP3_Bcm_B = [xPropPosVec(3), yPropPosVec(3), zPropPosVec(3)];
params.qP3_XYZ = [qP3x; qP3y; 0];
params.rP4_Bcm_B = [xPropPosVec(4), yPropPosVec(4), zPropPosVec(4)];
params.qP4_XYZ = [qP4x; qP4y; 0];
params.rP5_Bcm_B = [xPropPosVec(5), yPropPosVec(5), zPropPosVec(5)];
params.qP5_XYZ = [qP5x; qP5y; 0];
params.rP6_Bcm_B = [xPropPosVec(6), yPropPosVec(6), zPropPosVec(6)];
params.qP6_XYZ = [qP6x; qP6y; 0];
params.rP7_Bcm_B = [xPropPosVec(7), yPropPosVec(7), zPropPosVec(7)];
params.qP7_XYZ = [qP7x; qP7y; 0];
params.rP8_Bcm_B = [xPropPosVec(8), yPropPosVec(8), zPropPosVec(8)];
params.qP8_XYZ = [qP8x; qP8y; 0];

params.bThrust = 6.1100e-08;
params.bDrag = 1.5*10^-9;
params.propDirections = [ 1, -1, 1, -1, -1, 1, -1, 1];

forceGravity = params.g*(params.mB + params.mP * numProps);
forceP = forceGravity/numProps;
qProp = sqrt(forceP/params.bThrust);

uEqVal = 0;%load('uEqVal.mat');


%% Controller Parameters
params.controller.constantValue = qProp * params.propDirections;  % rad/sec
%Klqr = load('Klqr.mat');
%params.controller.Klqr = Klqr.Klqr;
%params.controller.uEqVal = uEqVal.uEqVal;
params.controller.type = simParams.control.type;
params.controller.space = simParams.control.space;
params.controller.kp = [20; 20; 20; 2; 2; 2; ];
params.controller.kd = [25; 25; 20; 1.5; 1.5; 1.5; ];
params.controller.ki = 0.0001;
params.controller.func = simParams.control.func;
%% Trajectory Params
params.trajectory = octoRotor_trajectoryParams(params.sim.tFinal);
params.trajectory.trajectoryFunc = simParams.trajectoryFunc;

%% Plotting/Animation Parameters
params.plotting.lP = lArm/3;
params.plotting.wP = params.plotting.lP/3;
params.plotting.hP = params.plotting.lP/10;
params.plotting.lBody = axisLength*2;
params.plotting.wBody = axisLength*2;
params.plotting.hBody = axisLength*2;
params.plotting.armWidth = 5;
params.plotting.armColor = [0.5, 0.5, 0.5];

end



function trajectoryParams = octoRotor_trajectoryParams(tFinal)

xB0 = 1; 
yB0 = 0; 
zB0 = 0; 
qBx0 = 0; 
qBy0 = 0; 
qBz0 = pi/4; 
xBDt0 = 0; 
yBDt0 = 0; 
zBDt0 = 0; 
wBx0 = 0; 
wBy0 = 0; 
wBz0 = 0; 
qP1z0 = 0; 
qP2z0 = 0; 
qP3z0 = 0; 
qP4z0 = 0; 
qP5z0 = 0; 
qP6z0 = 0;  
qP7z0 = 0; 
qP8z0 = 0; 
qP1zDt0 = 0; 
qP2zDt0 = 0; 
qP3zDt0 = 0; 
qP4zDt0 = 0; 
qP5zDt0 = 0; 
qP6zDt0 = 0;  
qP7zDt0 = 0; 
qP8zDt0 = 0; 

bodyStates0 = [xB0; yB0; zB0; qBx0; qBy0; qBz0; xBDt0; yBDt0; zBDt0; wBx0; wBy0; wBz0];
propStates0 = [qP1z0; qP2z0; qP3z0; qP4z0; qP5z0; qP6z0; qP7z0; qP8z0;];
states0 = [bodyStates0; propStates0]; %load('ICs.mat')

% x trajectory 
xTrajectory = trajectory;
xTrajectory.type = 'cos';
%xTrajectory.wayPoints = [ states0(1); 0];
%xTrajectory.velocities = [states0(7); 0 ];
xTrajectory.A = 1;
xTrajectory.w = 1;
xTrajectory.tVec = [0; tFinal];
xTrajectory.accelerations = [0; 0];

% y trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'sin';
yTrajectory.wayPoints = [ states0(2); 0];
yTrajectory.velocities = [states0(8); 0 ];
yTrajectory.tVec = [0; tFinal];
yTrajectory.accelerations = [0; 0];
yTrajectory.A = 1;
yTrajectory.w = 1;

% z trajectory 
zTrajectory = trajectory;
zTrajectory.type = 'sin';
%zTrajectory.wayPoints = [ states0(3); 5];
%zTrajectory.velocities = [states0(9); 0 ];
zTrajectory.A = 1;
zTrajectory.w = 2;
%zTrajectory.tVec = [0; tFinal];
%zTrajectory.accelerations = [0; 0];

% qBx trajectory 
qBxTrajectory = trajectory;
qBxTrajectory.type = 'sin';
%qBxTrajectory.wayPoints = [ states0(4); 0];
%qBxTrajectory.velocities = [states0(10); 0 ];
%qBxTrajectory.tVec = [0; tFinal];
%qBxTrajectory.accelerations = [0; 0];
qBxTrajectory.A = pi/4;
qBxTrajectory.w = .2;

% qBy trajectory 
qByTrajectory = trajectory;
qByTrajectory.type = 'sin';
qByTrajectory.wayPoints = [ states0(5); 0];
qByTrajectory.velocities = [states0(11); 0 ];
qByTrajectory.tVec = [0; tFinal];
qByTrajectory.accelerations = [0; 0];
qByTrajectory.A = pi/4;
qByTrajectory.w = .2;


% qBz trajectory 
qBzTrajectory = trajectory;
qBzTrajectory.type = 'cos';
qBzTrajectory.wayPoints = [ states0(6); 0];
qBzTrajectory.velocities = [states0(12); 0 ];
qBzTrajectory.tVec = [0; tFinal];
qBzTrajectory.accelerations = [0; 0];
qBzTrajectory.A = pi/4;
qBzTrajectory.w = 0.2;


trajectoryParams.numTrajectories = 6;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = xTrajectory;
trajectoryParams.trajectories{2} = yTrajectory;
trajectoryParams.trajectories{3} = zTrajectory;
trajectoryParams.trajectories{4} = qBxTrajectory;
trajectoryParams.trajectories{5} = qByTrajectory;
trajectoryParams.trajectories{6} = qBzTrajectory;

end