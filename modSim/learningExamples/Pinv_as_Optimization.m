% Psueduo Inverse of Jacobian as an optomization
addpath(genpath())
Len1 = 1;
Len2 = 1;
Len3 = 1;
q1 = pi/4;
q2 = pi/4;
q3 = pi/4;

xDot = [-5; -5; 0 ]
J = RzRzRzManip_J_FullDOF( Len1, Len2, Len3, q1, q2, q3 )
rank(J)
JInv = pinv(J)
qDot = JInv * xDot  
C = J
d = xDot;
A = eye(3,3)
b = [5;5; 5]
x = lsqlin(C,d,A,b)