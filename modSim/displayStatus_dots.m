function status = displayStatus_dots(t,y,flag) 
persistent tfinal
persistent strCR

if strcmp(flag, 'init')
    % initialize
    tic;
    tfinal = t(end);
    fprintf('Percent Complete: ');
    strCR = -1;
elseif strcmp(flag, 'done')
    % terminate
    strCR = [];
    runTime = toc;
    runTimeRatio = runTime/tfinal*100;
    fprintf(' \nFinished! It took %0.4f sec to simulate %0.4f sec,  %0.2f%% of Real Time \n',runTime, tfinal, runTimeRatio);
else
    % Vizualization parameters
    strPercentageLength = 10;   %   Length of percentage string (must be >5)
    strDotsMaximum      = 10;   %   The total number of dots in a progress bar
    
    % Progress bar - normal progress
    c = floor((t/tfinal)*100);
    percentageOut = [num2str(c(end)) '%%'];
    percentageOut = [percentageOut];
    %nDots = floor(c/100*strDotsMaximum);
    %dotOut = ['[' repmat('.',1,nDots) repmat(' ',1,strDotsMaximum-nDots) ']'];
    str1 = percentageOut;
    str2 = sprintf('Sim Time: %0.3f', t(end) );
    str3 = sprintf('Run Time: %0.3f', toc );
    space = repmat(' ',1, 3);
    strOut = [str1 space str2 space str3];
    
    % Print it on the screen
    if strCR == -1
        % Don't do carriage return during first run
        fprintf(strOut);
    else
        % Do it during all the other runs
        fprintf([strCR strOut]);
    end
    
    % Update carriage return
    strCR = repmat('\b',1,length(strOut)-1);
end
status = 0;
end