function params = planarQuadRotorThrust_params(simParams)

%% Store simulation Params
params.sim = simParams;
params.sim.fSolveOpts = optimset('Display','off');

%% Model Params
params.IB = [0.003, 0, 0; 0, 0.0117, 0; 0, 0, 0.0023];
params.mB = 0.18;
params.g = 9.8;
params.mP = 0.0001;
params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 0.00005];
params.lA = 0.186;
params.qP2 = -0.7854;
params.qP4 = 0.7854;
params.qP5 = 0.2618;
params.bThrust = 6.1100e-08;
params.IPxx = 0.0001;
params.IPyy = 0.0001;

%% Controller Params
params.controller.type = simParams.control.type;
params.controller.constantValue = 0;  % rad/sec
params.controller.Klqr = load('Klqr.mat');
params.controller.Klqr = params.controller.Klqr.Klqr;
params.controller.uEq = load('uEq.mat');
params.controller.uEq = params.controller.uEq.uEqVal;
params.controller.kPID = 50;
params.controller.kp = 250;
params.controller.kd = 250;


%% Plot Parms
params.markerSize = 35;
params.bodyLength = 2;
params.bodyWidth = 0.5;
params.LProp = 0.75;
params.WProp = 0.125;
params.propOffset = 1;

%% Trajectory Params
params.trajectory = planarQuadRotorThrust_trajectoryParams(params.sim.tFinal);
params.trajectory.trajectoryFunc = simParams.trajectoryFunc;

function trajectoryParams = planarQuadRotorThrust_trajectoryParams(tFinal)





%% Circle Trajectory
% y trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'cos';
%yTrajectory.wayPoints = [ states0(1); 5; 5];
%yTrajectory.velocities = [states0(4); 0; 0];
%yTrajectory.tVec = [0; tFinal/2; tFinal];
%yTrajectory.accelerations = [0; 0; 0];
yTrajectory.w = 10*pi/180;
yTrajectory.A = 1;

% z trajectory 
zTrajectory = trajectory;
zTrajectory.type = 'sin';
zTrajectory.w = 10*pi/180;
zTrajectory.A = 1;


%% Constant Pos Trajectory
% % y trajectory 
% yTrajectory = trajectory;
% yTrajectory.type = 'constantPos';
% yTrajectory.wayPoints = [0;];
% 
% 
% % z trajectory 
% zTrajectory = trajectory;
% zTrajectory.type = 'constantPos';
% zTrajectory.wayPoints = [0;];




[yB0, yBDt0, yBDDt0, yBDDDt0, ~] = yTrajectory.getTrajectory(0); 
[zB0, zBDt0, zBDDt0, zBDDDt0, ~] = zTrajectory.getTrajectory(0); 

qBx0 = planarQuadRotorThrust_calcqBx( params.g, yBDDt0, zBDDt0 );
qBxDt0 = planarQuadRotorThrust_calcqBxDot( params.g, qBx0, yBDDt0, yBDDDt0, zBDDt0, zBDDDt0 );
bodyStates0 = [yB0-10; zB0; qBx0; yBDt0; zBDt0; qBxDt0; ];

states0 = [bodyStates0]; %; propStates0

%%
% y trajectory 
% yTrajectory = trajectory;
% yTrajectory.type = 'minJerk';
% yTrajectory.wayPoints = [ states0(1); 5; 5];
% yTrajectory.velocities = [states0(4); 0; 0];
% yTrajectory.tVec = [0; tFinal/2; tFinal];
% yTrajectory.accelerations = [0; 0; 0];
% 
% % z trajectory 
% zTrajectory = trajectory;
% zTrajectory.type = 'minJerk';
% zTrajectory.wayPoints = [ states0(2); 0; 0];
% zTrajectory.velocities = [states0(5); 0; 0];
% zTrajectory.tVec = [0; tFinal/2; tFinal];
% zTrajectory.accelerations = [0; 0; 0];



trajectoryParams.numTrajectories = 2;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = yTrajectory;
trajectoryParams.trajectories{2} = zTrajectory;

end

end

