%% Setup workspace and define System Paramaters
clc; clear; close all
clear planarQuadRotorThrustController
trajectoryGenFolder = '\\snl\home\mrittik\Matlab\Trajectory Generation';
addpath(genpath(trajectoryGenFolder))
clear GenerateTrajectoryPoint
videoName = 'underActuatedPlanar_15 deg';
saveVideo = 1;
animate = 1;
LQR = true;
PID = false;
trajectoryType = 'circle';
tStep = 0.01;
tFinal = 30;
tSpan = 0:tStep:tFinal;
opts = odeset('RelTol',1e-6,'AbsTol',1e-6);
params.IB(1,1) = 0.00025;
params.IB(2,2)= 0.0117;
params.IB(3,3) = 0.00234;
params.IB(1,2) = 0;
params.IB(2,3) = 0;
params.IB(3,1) =0;
params.mB = 0.18;
params.lA = 0.186;
params.g  = 9.8;
params.bThrust = 6.11 * 10^-8; %N/rpm^2
x0 = [0; 0; 15*pi/180; 0; 0; 0];
xDesired = [ 1; 5 ; 0; 0; 0; 0];
numInputs = 2;

if strcmp(trajectoryType, 'minJerk')
t1 = 10;
t2 = 20;
t3 = tFinal;
trajectories.type = 'minJerk';
trajectories.trajectoryIndex = 3; % fieldname index in which trajectories start
trajectories.numTrajectories = 2;
trajectories.ytrajectory.tVec = [0; t1;  t2; t3];
trajectories.ytrajectory.wayPoints = [ 0; 5; 0; 5];
trajectories.ytrajectory.velocities = [0; 0; 0; 0];
trajectories.ytrajectory.accelerations = [0; 0; 0; 0];
trajectories.ztrajectory.tVec = [0; t1;  t2; t3];
trajectories.ztrajectory.wayPoints = [ 0; 0; 0; 0];
trajectories.ztrajectory.velocities = [0; 0; 0; 0];
trajectories.ztrajectory.accelerations = [0; 0; 0; 0];

elseif strcmp(trajectoryType, 'sine')
  tStep = .01; 
  tInitial = 0;
  tFinal = 30;
  t = (0:tStep:tFinal)';
  omega = 15*pi/180;
  trajectories.type = "sine";
  trajectories.omega = omega;
elseif strcmp(trajectoryType, 'circle')
  tStep = .01; 
  tInitial = 0;
  tFinal = 10;
  t = (0:tStep:tFinal)';
  omega = 60*pi/180;
  trajectories.type =  trajectoryType ;
  trajectories.omega = omega;
  trajectories.qBx = 15*pi/180;
  trajectories.r = 2;
else
  
  error('Trajectory type not handeled')
end


%% Uncontrolled Dynamics 
params.mode = 'none';
setPoint = xDesired;
trajectoryFuncHandle = @(trajectories, t) GenerateTrajectoryPoint(trajectories, t);
u = @(t, states) planarQuadRotorThrustController(t, states, trajectories, trajectoryFuncHandle, params);
odeFunc = @(t,states) planarQuadRotorThrustDynamics(t, states, params, u);
[tUncon, xUncon] = ode45(odeFunc, tSpan, x0, opts);

%% Linear Quadratic Regulator Controller (linearized about PI)
if(LQR)
disp('Beginning simulation with LQR Control');
load('Klqr.mat')
load('uEq.mat')
params.mode = 'LQR';
params.Klqr = Klqr;
params.uEq = uEqVal;
statesDesired = xDesired;
trajectoryFuncHandle = @(trajectories, t) generatePlanarQuadRotorTrajectories(trajectories, t);
u = @(t, states) planarQuadRotorThrustController(t, states, trajectories, trajectoryFuncHandle, params);
odeFunc = @(t,states) planarQuadRotorThrustDynamics(t, states, params, u);
[tLQR, xLQR] = ode45(odeFunc, tSpan, x0, opts);

numPoints = length(tLQR);
extraOutputsLQR=zeros(numPoints, 4);
inputForcesLQR = zeros(numPoints, 2);
propRatesLQR = inputForcesLQR;
for i = 1 :  numPoints
    [~,extraOutputsLQR(i,:),inputForcesLQR(i,:),propRatesLQR(i,:) ] = planarQuadRotorThrustDynamics(tLQR(i), xLQR(i,:)', params, u);
end
drawStatesLQR = extraOutputsLQR(:,:);
clear GenerateTrajectoryPoint
[posDes, vDes, aDes] = GenerateTrajectoryPoint(trajectories, tLQR);
errorLQR =  xLQR(:,1:2) - posDes;

% Plot Responses
figure
hold on 
for i = 1: length(xDesired)
  subplot(length(xDesired),1,i)
  plot(tUncon, xUncon(:,i), 'k', 'linewidth', 2)
  hold on
  plot(tLQR, xLQR(:,i), 'r--', 'linewidth', 2)
legend('Uncontrolled', 'LQR Controlled')
end

figure
hold on
for i = 1:size(inputForcesLQR,2)
  plot(tLQR, inputForcesLQR(:,i), 'linewidth', 2)
end
title(' Input Force Time Histories')

figure 
plot(tLQR,errorLQR)
title('State Error vs Time with LQR Control')
xlabel('Time (s)');
ylabel('Error');

figure 
plot(tLQR, xLQR(:,1:2), tLQR, posDes(:,1:2))
end
%% PID Controller 
if PID
disp('Beginning simulation with PID Control');
load('uEq.mat');
params.uEq = uEqVal;
Kpid = [0 0 0];
params.mode = 'PID';
params.Kpid = Kpid;
statesDesired = xDesired;
params.uEq = uEqVal;
trajectoryFuncHandle = @(trajectories, t) GenerateTrajectoryPoint(trajectories, t);
u = @(t, states) planarQuadRotorThrustController(t, states, trajectories, trajectoryFuncHandle, params);
odeFunc = @(t,states) planarQuadRotorThrustDynamics(t, states, params, u);
[tPID, xPID] = ode45(odeFunc, tSpan, x0);
 
%  
numPoints = length(tPID);
extraOutputsPID=zeros(numPoints, 4);
inputForcesPID = zeros(numPoints, 2);
propRatesPID = inputForcesPID;
for i = 1 :  numPoints
    [~,extraOutputsPID(i,:),inputForcesPID(i,:),propRatesPID(i,:) ] = planarQuadRotorThrustDynamics(tPID(i), xPID(i,:)', params, u);
end
drawStatesPID = extraOutputsPID(:,:);
errorPID =  xPID - xDesired'.*ones(numPoints,size(xDesired,1));
[posDesPID, vDesPID, aDesPID] = GenerateTrajectoryPoint(trajectories, tPID);
figure
hold on 
for i = 1: length(xDesired)
  subplot(length(xDesired),1,i)
  plot(tUncon, xUncon(:,i), 'k', 'linewidth', 2)
  hold on
  plot(tPID, xPID(:,i), 'r--', 'linewidth', 2)
legend('Uncontrolled', 'PID Controlled')
end

figure 
plot(tPID,errorPID)
title('State Error vs Time with PID Control')
xlabel('Time (s)');
ylabel('Error');

figure 
plot(tPID, xPID(:,1), 'r-', tPID, posDesPID(1,:)', 'k*')
end

%% Animate Responses
if animate ==1
params.videoName = videoName;
params.xDesired = posDes';  
planarQuadRotorThrustAnimation(tLQR,drawStatesLQR,params, saveVideo)
% params.xDesired = posDesPID';  
% planarQuadRotorThrustAnimation(tPID,drawStatesPID,params)
end