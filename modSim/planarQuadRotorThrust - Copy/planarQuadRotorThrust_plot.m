function  planarQuadRotorThrust_plot(plotData, params, savePlots)

styleVec ={'k-','r--','y-' };

desiredTrajectory = [plotData.posDes, plotData.vDes];
stateError = [plotData.states(:,1:2), plotData.states(:,4:5) ]- desiredTrajectory(:,1:4);

errorVTime = figure; 
plot(plotData.t, stateError)
title('State Error vs Time')
xlabel('Time (s)');
ylabel('Error');
legend('yB', 'zB', 'yBDot', 'zBDot')

errorVTime = figure; 
plot(plotData.t, stateError(:,1:2))
title('State Error vs Time')
xlabel('Time (s)');
ylabel('Error');
legend('yB', 'zB')

% figure 
% plot(plotData.t, stateError(:,1))
% 
% figure 
% plot(plotData.t, plotData.states(:,7))
% 
% numTrajectories = params.trajectoryParams.numTrajectories;
% desiredTrajectory = [plotData.posDes, plotData.vDes];
% stateError = plotData.states(:,1:12) - desiredTrajectory;
% taskError = taskStates - desiredTrajectory ;
% trajectories = figure;
% hold on
% for i = 1:numTrajectories
% plot(plotData.t, taskStates(:,i),styleVec{1})
% hold on
% plot(plotData.t, desiredTrajectory(:,i), styleVec{2})
% 
% title(' Trajectory Time Histories');
% xlabel('Time [s]');
% ylabel('y [units]');
% legend( 'Actual','Desired')
% end

% 3D Vis of Initial propeller config
% figure 
% [P1Vertices, P1Faces] =  plotCube([lP, wP, hP],rP1_No_N(1,:), qP1_ZYX(1,:));
% [P2Vertices, P2Faces] =  plotCube([lP, wP, hP],rP2_No_N(1,:), qP2_ZYX(1,:));
% [P3Vertices, P3Faces] =  plotCube([lP, wP, hP],rP3_No_N(1,:), qP3_ZYX(1,:));
% [P4Vertices, P4Faces] =  plotCube([lP, wP, hP],rP4_No_N(1,:), qP4_ZYX(1,:));
% [P5Vertices, P5Faces] =  plotCube([lP, wP, hP],rP5_No_N(1,:), qP5_ZYX(1,:));
% [P6Vertices, P6Faces] =  plotCube([lP, wP, hP],rP6_No_N(1,:), qP6_ZYX(1,:));
% setPointPlot = plot3(setPoint(1),setPoint(2),setPoint(3), 'r.','markersize',25);
% A1Plot = plot3([xB(1); rP1_No_N(1,1) ], [yB(1); rP1_No_N(1,2)], [zB(1); rP1_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
% A2Plot = plot3([xB(1); rP2_No_N(1,1) ], [yB(1); rP2_No_N(1,2)], [zB(1); rP2_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
% A3Plot = plot3([xB(1); rP3_No_N(1,1) ], [yB(1); rP3_No_N(1,2)], [zB(1); rP3_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
% A4Plot = plot3([xB(1); rP4_No_N(1,1) ], [yB(1); rP4_No_N(1,2)], [zB(1); rP4_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
% A5Plot = plot3([xB(1); rP5_No_N(1,1) ], [yB(1); rP5_No_N(1,2)], [zB(1); rP5_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
% A6Plot = plot3([xB(1); rP6_No_N(1,1) ], [yB(1); rP6_No_N(1,2)], [zB(1); rP6_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
% 
% P1Plot = patch('Vertices',P1Vertices,'Faces',P1Faces, 'FaceColor', [0, 0, 0]);
% P2Plot = patch('Vertices',P2Vertices,'Faces',P2Faces, 'FaceColor', [0, 0, 0]);
% P3Plot = patch('Vertices',P3Vertices,'Faces',P3Faces, 'FaceColor', [0, 0, 0]);
% P4Plot = patch('Vertices',P4Vertices,'Faces',P4Faces, 'FaceColor', [0, 0, 0]);
% P5Plot = patch('Vertices',P5Vertices,'Faces',P5Faces, 'FaceColor', [0, 0, 0]);
% P6Plot = patch('Vertices',P6Vertices,'Faces',P6Faces, 'FaceColor', [0, 0, 0]);

% if savePlots
%   saveas(errorVTime, fullfile(params.resultsFolder,['errorVTime','.png'] ));
% end

end