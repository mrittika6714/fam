function [posDes, vDes, aDes] = generatePlanarQuadRotorTrajectories(trajectories, t)


[posDes, vDes, aDes] = GenerateTrajectoryPoint(trajectories, t);

if strcmp(trajectories.type, 'minJerk')
  %Do Nothing assuming all trajectories are given as mininmumJerk
  
elseif strcmp(trajectories.type, 'sine') 
  % need to define qBx as sine only gives trajectories for y and z
  qBx = trajectories.qBx* ones(size(posDes,1),1);
  qBxDot = zeros(size(posDes,1),1);
  qBxDDot = qBxDot;
  
  posDes = [posDes, qBx];
  vDes = [vDes, qBxDot];
  aDes = [aDes, qBxDDot];
  
elseif strcmp(trajectories.type, 'circle')
    % need to define qBx as sine only gives trajectories for y and z
  qBx = trajectories.qBx* ones(size(posDes,1),1);
  qBxDot = zeros(size(posDes,1),1);
  qBxDDot = qBxDot;
  posDes = [posDes, qBx];
  vDes = [vDes, qBxDot];
  aDes = [aDes, qBxDDot];
  
end

end