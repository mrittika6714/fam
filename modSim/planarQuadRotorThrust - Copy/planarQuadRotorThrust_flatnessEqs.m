function FlatnessEQ = planarQuadRotorThrust_flatnessEqs( g, qBx, qBxDt, qBxDDt, yBDDt, yBDDDt, yBDDDDt, zBDDt, zBDDDt, zBDDDDt )
if( nargin ~= 10 ) error( 'planarQuadRotorThrust_flatnessEqs expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_flatnessEqs.m created Jan 06 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
FlatnessEQ = zeros( 3, 1 );




%===========================================================================


%===========================================================================
Output = [];

FlatnessEQ(1) = tan(qBx) + yBDDt/(g+zBDDt);
FlatnessEQ(2) = qBxDt/cos(qBx)^2 + yBDDDt/(g+zBDDt) - yBDDt*zBDDDt/(g+zBDDt)^2;
FlatnessEQ(3) = 2*sin(qBx)*qBxDt^2/cos(qBx)^3 + qBxDDt/cos(qBx)^2 + (2*yBDDt*zBDDDt^2+(g+zBDDt)^2*yBDDDDt-2*(g+zBDDt)*yBDDDt*  ...
zBDDDt-yBDDt*(g+zBDDt)*zBDDDDt)/(g+zBDDt)^3;


%==========================================================
end    % End of function planarQuadRotorThrust_flatnessEqs
%==========================================================
