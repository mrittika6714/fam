function control = planarQuadRotorThrust_controller(t, states, trajectories, trajectoryFunc, params)
persistent errorSum

%Unpack Params
IBxx = params.IB(1,1);
mB = params.mB;
lA = params.lA;
g = params.g;

% Unpack States
yB = states(1);
zB = states(2);
qBx = states(3);
yBDt = states(4);
zBDt = states(5);
qBxDt = states(6);

% Unpack Desired States
[rDes, rDotDes, rDDotDes, rDDDotDes, rDDDDotDes] = trajectoryFunc(trajectories, t);
yBDes = rDes(1);
yBDtDes = rDotDes(1);
yBDDtDes = rDDotDes(1);
yBDDDtDes = rDDDotDes(1);
yBDDDDtDes = rDDDDotDes(1);
zBDes = rDes(2);
zBDtDes = rDotDes(2);
zBDDtDes = rDDotDes(2);
zBDDDtDes = rDDDotDes(2);
zBDDDDtDes  = rDDDDotDes(2);

% Calculate Desired qBx utilizing differential Flatness
qBxDes = planarQuadRotorThrust_calcqBx( g, yBDDtDes, zBDDtDes );
qBxDtDes = planarQuadRotorThrust_calcqBxDot( g, qBxDes, yBDDtDes, yBDDDtDes, zBDDtDes, zBDDDtDes );
qBxDDtDes = planarQuadRotorThrust_calcqBxDDot( g, qBxDes, qBxDtDes, yBDDtDes, yBDDDtDes, yBDDDDtDes, zBDDtDes, zBDDDtDes, zBDDDDtDes );
    
controllerType = params.controller.type;
error = [yB - yBDes, zB- zBDes, qBx- qBxDes];
errorDot = [yBDt - yBDtDes, zBDt - zBDtDes, qBxDt -  qBxDtDes];

if isempty(errorSum)
 errorSum = [0 0 0];
else
 errorSum = errorSum+error;
end

if strcmp(controllerType, 'none')
    control = zeros(length(states), 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.uEq;

elseif strcmp(controllerType, 'sin')
   control = ones(lengthStates, 1) * sin(t);
   
%% LQR Controllerl
elseif strcmp(controllerType, 'LQR')
  setPoint = [ yBDes; zBDes; qBxDes ; yBDtDes; zBDtDes; qBxDtDes ];
  Klqr = params.controller.Klqr ;
  control = -Klqr*(states-setPoint) + params.controller.uEq;

elseif strcmp(controllerType, 'test')
   kpz = 10;
  kdz = 10;
  kiz = 0.0006;
  kpy = 100;
  kdy = 100;
  kiy = 0;
  kdq = 25;
  kpq = 25;
  kiq = 0; 
     %qBxCon =   qBxDes +  kp .* error(3);
%      yBDDtCon = yBDDtDes + kd.*errorDot(1) + kp .* error(1) + kiy*errorSum(1)
%      yBDDtDes
%      zBDDtCon = zBDDtDes + kd.*errorDot(2) + kp .* error(2) + kiz*errorSum(2);
%      %qBxCon = 2 .* (qBxDes - qBx);
%      %qBxDDtCon = qBxDDtDes + kd.*errorDot(3) + kp .* error(3); % control signal for acceleration 
%      %qBxDDtCon = qBxDDtDes + kp*(qBxDes - qBx) + kd*(qBxDtDes - qBxDt);% + kiq*errorSum(3);
%      qBxCon = planarQuadRotorThrust_calcqBx( g, yBDDtCon, zBDDtCon )
%      qBxDes
%      qBxDtCon = planarQuadRotorThrust_calcqBxDot( g, qBxCon, yBDDtCon, yBDDDtDes, zBDDtCon, zBDDDtDes )
%      %qBxDDtCon = planarQuadRotorThrust_calcqBxDDot( g, qBxDes, qBxDtDes, yBDDtDes, yBDDDtDes, yBDDDDtDes, zBDDtDes, zBDDDtDes, zBDDDDtDes );
%     
%      qBxDDtCon = planarQuadRotorThrust_calcqBxDDot( g, qBxCon, qBxDtCon, yBDDtCon, yBDDDtDes, yBDDDDtDes, zBDDtCon, zBDDDtDes, zBDDDDtDes )
%      qBxDDtDes
yBDDtCon = yBDDtDes + kdy*(yBDtDes - yBDt) + kpy*(yBDes - yB);
zBDDtCon = zBDDtDes + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB);
%qBxCon = -yBDDtCon/g;
qBxCon = planarQuadRotorThrust_calcqBx( g, yBDDtCon, zBDDtCon );
%qBxCon = qBxDes + kdq*(qBxDtDes-qBxDt) + kpq *(qBxDes-qBx);  
qBxDtCon = -(1/g)*(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt));
%qBxDtCon = qBxDtDes + kpq*(qBxCon - qBx);
%qBxDtCon = planarQuadRotorThrust_calcqBxDot( g, qBxCon, yBDDtCon, 0, zBDDtCon, 0);
%qBxDtCon = -(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt))/g;
qBxDDtCon = qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt);
%qBxDDtCon = planarQuadRotorThrust_calcqBxDDot( g, qBxCon, qBxDtCon, yBDDtCon, yBDDtDes, yBDDDDtDes, zBDDtCon, zBDDDtDes, zBDDDDtDes );
%     
    control= planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBx, qBxDDtCon, yBDDtCon, zBDDtCon, g, lA ); 
    
%% PID 
elseif strcmp(controllerType, 'PID')
  %Kpid= params.controller.kPID;
  kpz = 10;
  kdz = 10;
  kiz = 0.0006;
  kpy = 10;
  kdy = 10;
  kiy = 0;
  kdq = 25;
  kpq = 25;
  kiq = 0;  

  % Control Equations
  yBDDotCon = (kdq*(yBDtDes - yBDt) + kpq*(yBDes- yB));
qBxCon = -yBDDotCon/g;
% phi_c_dot = 0; %near hovering
qBxDtCon = -(kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt))/g;
u1 = mB*(g+zBDDtDes) + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB) - kiz * errorSum(2) ;
u2 = IBxx*(qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt));

lA = 0.186;

X =linsolve([lA -lA; 1 1 ], [u2; u1] );
f2= X(1);
f4 = X(2);
%   zDDotCon = zBDDtDes + kdz*(zBDtDes - zBDt) +kpz * (zBDes - zB) + kiz*errorSum(2);
%   yDDotCon = yBDDtDes + kdy*(yBDtDes - yBDt) +kpy * (yBDes - yB) + kiy*errorSum(1);
%   qBxDDotCon = qBxDDtDes+kpq*(qBxDes - qBx) + kdq*(qBxDtDes - qBxDt) + kiq*errorSum(3);
% 
%   F2 = 0.5*(qBxDDotCon * IBxx)/lA  + 0.5*(mB*(zDDotCon+g))/cos(qBxDes);
%   F4 = 0.5*(mB*(zDDotCon+g))/cos(qBxDes) - 0.5*(qBxDDotCon * IBxx)/lA ; 

  control = [f2; f4] ;
  

%% Feed Forward (Computed Torque) Controller
 elseif strcmp(controllerType, 'FF')
    control = planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBxDes, qBxDDtDes, yBDDtDes, zBDDtDes, g, lA ); 
%% Feed Forward (Computed Torque) Controller w PID
 elseif strcmp(controllerType, 'FF_PID')
  kpz = 10;
  kdz = 10;
  kiz = 0.0006;
  kpy = 30;
  kdy = 20;
  kiy = 0;
  kdq = 25;
  kpq = 25;
  kiq = 0; 
     %qBxCon =   qBxDes +  kp .* error(3);
%      yBDDtCon = yBDDtDes + kd.*errorDot(1) + kp .* error(1) + kiy*errorSum(1)
%      yBDDtDes
%      zBDDtCon = zBDDtDes + kd.*errorDot(2) + kp .* error(2) + kiz*errorSum(2);
%      %qBxCon = 2 .* (qBxDes - qBx);
%      %qBxDDtCon = qBxDDtDes + kd.*errorDot(3) + kp .* error(3); % control signal for acceleration 
%      %qBxDDtCon = qBxDDtDes + kp*(qBxDes - qBx) + kd*(qBxDtDes - qBxDt);% + kiq*errorSum(3);
%      qBxCon = planarQuadRotorThrust_calcqBx( g, yBDDtCon, zBDDtCon )
%      qBxDes
%      qBxDtCon = planarQuadRotorThrust_calcqBxDot( g, qBxCon, yBDDtCon, yBDDDtDes, zBDDtCon, zBDDDtDes )
%      %qBxDDtCon = planarQuadRotorThrust_calcqBxDDot( g, qBxDes, qBxDtDes, yBDDtDes, yBDDDtDes, yBDDDDtDes, zBDDtDes, zBDDDtDes, zBDDDDtDes );
%     
%      qBxDDtCon = planarQuadRotorThrust_calcqBxDDot( g, qBxCon, qBxDtCon, yBDDtCon, yBDDDtDes, yBDDDDtDes, zBDDtCon, zBDDDtDes, zBDDDDtDes )
%      qBxDDtDes
yBDDtCon = yBDDtDes + kdy*(yBDtDes - yBDt) + kpy*(yBDes - yB);
zBDDtCon = zBDDtDes + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB);
qBxCon = -yBDDtCon/g;
%qBxCon = planarQuadRotorThrust_calcqBx( g, yBDDtCon, zBDDtCon );
%qBxCon = qBxDes + kdq*(qBxDtDes-qBxDt) + kpq *(qBxDes-qBx);  
qBxDtCon = -(1/g)*(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt));
%qBxDtCon = qBxDtDes + kpq*(qBxCon - qBx);
%qBxDtCon = planarQuadRotorThrust_calcqBxDot( g, qBxCon, yBDDtCon, 0, zBDDtCon, 0);
%qBxDtCon = -(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt))/g;
qBxDDtCon = qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt);
%qBxDDtCon = planarQuadRotorThrust_calcqBxDDot( g, qBxCon, qBxDtCon, yBDDtCon, yBDDtDes, yBDDDDtDes, zBDDtCon, zBDDDtDes, zBDDDDtDes );
%     
    control= planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBx, qBxDDtCon, yBDDtCon, zBDDtCon, g, lA ); 
    
else
  error('Controller mode not handled');
end 



end