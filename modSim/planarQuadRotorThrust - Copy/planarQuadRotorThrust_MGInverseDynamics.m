function [SolutionToAlgebraicEquations] = planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBx, qBxDDt, yBDDt, zBDDt, g, lA )
if( nargin ~= 8 ) error( 'planarQuadRotorThrust_MGInverseDynamics expects 8 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_MGInverseDynamics.m created Jan 06 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================




%===========================================================================
COEF = zeros( 2, 2 );
COEF(1,1) = -cos(qBx);
COEF(1,2) = -cos(qBx);
COEF(2,1) = -lA;
COEF(2,2) = lA;
RHS = zeros( 1, 2 );
RHS(1) = -mB*(g+zBDDt);
RHS(2) = -IBxx*qBxDDt;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
F2 = SolutionToAlgebraicEquations(1);
F4 = SolutionToAlgebraicEquations(2);



%================================================================
end    % End of function planarQuadRotorThrust_MGInverseDynamics
%================================================================
