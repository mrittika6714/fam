function [Output] = thrust2rates( F2, F4, bThrust )
if( nargin ~= 3 ) error( 'thrust2rates expects 3 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: thrust2rates.m created Nov 10 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================
w2 = sqrt(F2/bThrust);
w4 = sqrt(F4/bThrust);



%===========================================================================
Output = zeros( 1, 2 );

Output(1) = w2;
Output(2) = w4;

%=====================================
end    % End of function thrust2rates
%=====================================
