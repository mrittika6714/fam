function [SolutionToAlgebraicEquations] = quadRotor_calc_wB_N_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot )
if( nargin ~= 6 ) error( 'quadRotor_calc_wB_N_B expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotor_calc_wB_N_B.m created Jan 15 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 1024 );



%===========================================================================
z(1) = cos(qBy);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(14) = z(1)*z(8);
z(191) = z(1)*z(14) + z(4)*z(9);
z(1019) = z(14)/z(191);
z(1020) = z(9)/z(191);
z(1021) = z(3)*z(4)/z(191);
z(1022) = z(1)*z(3)/z(191);
z(1023) = z(4)/z(191);
z(1024) = z(1)/z(191);

COEF = zeros( 3, 3 );
COEF(1,1) = z(1019);
COEF(1,3) = z(1020);
COEF(2,1) = z(1021);
COEF(2,2) = 1;
COEF(2,3) = -z(1022);
COEF(3,1) = -z(1023);
COEF(3,3) = z(1024);
RHS = zeros( 1, 3 );
RHS(1) = qBxDot;
RHS(2) = qByDot;
RHS(3) = qBzDot;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBx = SolutionToAlgebraicEquations(1);
wBy = SolutionToAlgebraicEquations(2);
wBz = SolutionToAlgebraicEquations(3);



%==============================================
end    % End of function quadRotor_calc_wB_N_B
%==============================================
