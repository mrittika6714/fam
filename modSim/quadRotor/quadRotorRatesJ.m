function quadRotorRatesJ( bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, mProp, IPropxx, IPropyy, IPropzz, qBx, qBy, qBz, wBx, wBy, wBz, q1Dt, q2Dt, q3Dt, q4Dt, xBDt, yBDt, zBDt )
if( nargin ~= 27 ) error( 'quadRotorRatesJ expects 27 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotorRatesJ.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
J = zeros( 12, 12 );





%===========================================================================


%===========================================================================
Output = [];

J(1,1) = 0;
J(1,2) = 0;
J(1,3) = 0;
J(1,4) = 0;
J(1,5) = 0;
J(1,6) = 0;
J(1,7) = 1;
J(1,8) = 0;
J(1,9) = 0;
J(1,10) = 0;
J(1,11) = 0;
J(1,12) = 0;
J(2,1) = 0;
J(2,2) = 0;
J(2,3) = 0;
J(2,4) = 0;
J(2,5) = 0;
J(2,6) = 0;
J(2,7) = 0;
J(2,8) = 1;
J(2,9) = 0;
J(2,10) = 0;
J(2,11) = 0;
J(2,12) = 0;
J(3,1) = 0;
J(3,2) = 0;
J(3,3) = 0;
J(3,4) = 0;
J(3,5) = 0;
J(3,6) = 0;
J(3,7) = 0;
J(3,8) = 0;
J(3,9) = 1;
J(3,10) = 0;
J(3,11) = 0;
J(3,12) = 0;
J(4,1) = 0;
J(4,2) = 0;
J(4,3) = 0;
J(4,4) = 0;
J(4,5) = cos(qBy)*wBz - sin(qBy)*wBx;
J(4,6) = 0;
J(4,7) = 0;
J(4,8) = 0;
J(4,9) = 0;
J(4,10) = cos(qBy);
J(4,11) = 0;
J(4,12) = sin(qBy);
J(5,1) = 0;
J(5,2) = 0;
J(5,3) = 0;
J(5,4) = (sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx)^2;
J(5,5) = tan(qBx)*(sin(qBy)*wBz+cos(qBy)*wBx);
J(5,6) = 0;
J(5,7) = 0;
J(5,8) = 0;
J(5,9) = 0;
J(5,10) = sin(qBy)*tan(qBx);
J(5,11) = 1;
J(5,12) = -cos(qBy)*tan(qBx);
J(6,1) = 0;
J(6,2) = 0;
J(6,3) = 0;
J(6,4) = -sin(qBx)*(sin(qBy)*wBx-cos(qBy)*wBz)/cos(qBx)^2;
J(6,5) = -(sin(qBy)*wBz+cos(qBy)*wBx)/cos(qBx);
J(6,6) = 0;
J(6,7) = 0;
J(6,8) = 0;
J(6,9) = 0;
J(6,10) = -sin(qBy)/cos(qBx);
J(6,11) = 0;
J(6,12) = cos(qBy)/cos(qBx);
J(7,1) = 0;
J(7,2) = 0;
J(7,3) = 0;
J(7,4) = bThrust*sin(qBz)*cos(qBx)*cos(qBy)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(7,5) = bThrust*(cos(qBy)*cos(qBz)-sin(qBx)*sin(qBy)*sin(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(7,6) = -bThrust*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(7,7) = 0;
J(7,8) = 0;
J(7,9) = 0;
J(7,10) = 0;
J(7,11) = 0;
J(7,12) = 0;
J(8,1) = 0;
J(8,2) = 0;
J(8,3) = 0;
J(8,4) = -bThrust*cos(qBx)*cos(qBy)*cos(qBz)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(8,5) = bThrust*(sin(qBz)*cos(qBy)+sin(qBx)*sin(qBy)*cos(qBz))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(8,6) = bThrust*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(8,7) = 0;
J(8,8) = 0;
J(8,9) = 0;
J(8,10) = 0;
J(8,11) = 0;
J(8,12) = 0;
J(9,1) = 0;
J(9,2) = 0;
J(9,3) = 0;
J(9,4) = -bThrust*sin(qBx)*cos(qBy)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(9,5) = -bThrust*sin(qBy)*cos(qBx)*(q1Dt^2+q2Dt^2+q3Dt^2+q4Dt^2)/(mB+4*mProp);
J(9,6) = 0;
J(9,7) = 0;
J(9,8) = 0;
J(9,9) = 0;
J(9,10) = 0;
J(9,11) = 0;
J(9,12) = 0;
J(10,1) = 0;
J(10,2) = 0;
J(10,3) = 0;
J(10,4) = 0;
J(10,5) = 0;
J(10,6) = 0;
J(10,7) = 0;
J(10,8) = 0;
J(10,9) = 0;
J(10,10) = ((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IBxx*wBy-2*IBxy*wBx-IByy*wBy-IByz*wBz)-(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*  ...
IPropzz+4*mProp*lA^2))*(IBxy*wBz-IBzx*wBy)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz-  ...
2*mProp*lA^2*wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(10,11) = -((IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBz-IByz*wBx)+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IByy*wBx-2*  ...
IBxy*wBy-IBxx*wBx-IBzx*wBz)+(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz-2*  ...
mProp*lA^2*wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(10,12) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IByz*wBx-IBzx*wBy)+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBx+IBxy*  ...
wBy+2*IBzx*wBz-4*IPropzz*wBx-IBzz*wBx-2*mProp*lA^2*wBx)+(IByz^2-(IByy+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBx+IByy*  ...
wBy+2*IByz*wBz-4*IPropzz*wBy-IBzz*wBy-2*mProp*lA^2*wBy))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(11,1) = 0;
J(11,2) = 0;
J(11,3) = 0;
J(11,4) = 0;
J(11,5) = 0;
J(11,6) = 0;
J(11,7) = 0;
J(11,8) = 0;
J(11,9) = 0;
J(11,10) = ((IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBz-IBzx*wBy)+(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxx*wBy-2*IBxy*  ...
wBx-IByy*wBy-IByz*wBz)+(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz-2*mProp*lA^2*  ...
wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*  ...
mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(11,11) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IByy*wBx-2*IBxy*wBy-IBxx*wBx-IBzx*wBz)-(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*  ...
IPropzz+4*mProp*lA^2))*(IBxy*wBz-IByz*wBx)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz-  ...
2*mProp*lA^2*wBz-IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(11,12) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IByz*wBx-IBzx*wBy)-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxy*wBx+IByy*  ...
wBy+2*IByz*wBz-4*IPropzz*wBy-IBzz*wBy-2*mProp*lA^2*wBy)-(IBzx^2-(IBxx+2*mProp*lA^2)*(IBzz+4*IPropzz+4*mProp*lA^2))*(IBxx*wBx+IBxy*  ...
wBy+2*IBzx*wBz-4*IPropzz*wBx-IBzz*wBx-2*mProp*lA^2*wBx))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(12,1) = 0;
J(12,2) = 0;
J(12,3) = 0;
J(12,4) = 0;
J(12,5) = 0;
J(12,6) = 0;
J(12,7) = 0;
J(12,8) = 0;
J(12,9) = 0;
J(12,10) = ((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IBxy*wBz-IBzx*wBy)-(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(IBxx*wBy-2*  ...
IBxy*wBx-IByy*wBy-IByz*wBz)-(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxx*wBz-2*IBzx*wBx-IByz*wBy-IBzz*wBz-2*mProp*lA^2*wBz-IPropzz*  ...
q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+  ...
4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(12,11) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxy*wBz-IByz*wBx)-(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(IByy*wBx-  ...
2*IBxy*wBy-IBxx*wBx-IBzx*wBz)-(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IByy*wBz-2*IByz*wBy-IBzx*wBx-IBzz*wBz-2*mProp*lA^2*wBz-  ...
IPropzz*q1Dt-IPropzz*q2Dt-IPropzz*q4Dt-IPropzz*(4*wBz+q3Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
J(12,12) = ((IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(IByz*wBx-IBzx*wBy)+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(IBxy*wBx+IByy*  ...
wBy+2*IByz*wBz-4*IPropzz*wBy-IBzz*wBy-2*mProp*lA^2*wBy)-(IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(IBxx*wBx+IBxy*wBy+2*IBzx*wBz-4*  ...
IPropzz*wBx-IBzz*wBx-2*mProp*lA^2*wBx))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*  ...
mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));


%========================================
end    % End of function quadRotorRatesJ
%========================================
