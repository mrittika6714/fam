function [SolutionToAlgebraicEquations] = qBDotTowB( qBx, qBxDot, qBy, qByDot, qBzDot )
if( nargin ~= 5 ) error( 'qBDotTowB expects 5 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: qBDotTowB.m created Feb 10 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 550 );




%===========================================================================
z(1) = cos(qBy);
z(3) = sin(qBx);
z(4) = sin(qBy);
z(8) = cos(qBx);
z(9) = z(4)*z(8);
z(14) = z(1)*z(8);
z(71) = z(1)*z(14) + z(4)*z(9);
z(545) = z(14)/z(71);
z(546) = z(9)/z(71);
z(547) = z(3)*z(4)/z(71);
z(548) = z(1)*z(3)/z(71);
z(549) = z(4)/z(71);
z(550) = z(1)/z(71);

COEF = zeros( 3, 3 );
COEF(1,1) = z(545);
COEF(1,3) = z(546);
COEF(2,1) = z(547);
COEF(2,2) = 1;
COEF(2,3) = -z(548);
COEF(3,1) = -z(549);
COEF(3,3) = z(550);
RHS = zeros( 1, 3 );
RHS(1) = qBxDot;
RHS(2) = qByDot;
RHS(3) = qBzDot;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBx = SolutionToAlgebraicEquations(1);
wBy = SolutionToAlgebraicEquations(2);
wBz = SolutionToAlgebraicEquations(3);



%==================================
end    % End of function qBDotTowB
%==================================
