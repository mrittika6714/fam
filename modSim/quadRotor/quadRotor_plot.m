function  octoRotor_plot(plotData, params, savePlots)

% styleVec ={'k-','r--','y-' };

% Unpack Parameters
lA = params.lA;
lP = params.plotting.lP;
wP = params.plotting.wP;
hP = params.plotting.hP;
armWidth = params.plotting.armWidth;
armColor = params.plotting.armColor;
vehicleDim = [params.plotting.lBody, params.plotting.wBody, params.plotting.hBody];

% Unpack plotData
xB = plotData.states(:,1);
yB = plotData.states(:,2);
zB = plotData.states(:,3);
qBx = plotData.states(:,4);
qBy = plotData.states(:,5);
qBz = plotData.states(:,6);
rP1_No_N = plotData.additionalOutputs.animationOutputs(:,1:3);
rP2_No_N = plotData.additionalOutputs.animationOutputs(:,4:6);
rP3_No_N = plotData.additionalOutputs.animationOutputs(:,7:9); 
rP4_No_N = plotData.additionalOutputs.animationOutputs(:,10:12); 
qP1_ZYX = plotData.additionalOutputs.animationOutputs(:,13:15);
qP2_ZYX = plotData.additionalOutputs.animationOutputs(:,16:18);
qP3_ZYX = plotData.additionalOutputs.animationOutputs(:,19:21);
qP4_ZYX = plotData.additionalOutputs.animationOutputs(:,22:24);
qB_ZYX = plotData.additionalOutputs.animationOutputs(:,25:27);

desiredTrajectory = [plotData.posDes];
stateError = plotData.states(:,1:4) - desiredTrajectory;

errorVTime = figure; 
plot(plotData.t, stateError)
title('State Error vs Time')
xlabel('Time (s)');
ylabel('Error');
legend('x', 'y', 'z', 'qBz')


% 3D Vis of Initial propeller config
[P1Vertices, P1Faces] =  plotCube([lP, wP, hP],rP1_No_N(1,:), qP1_ZYX(1,:), 'ZYX');
[P2Vertices, P2Faces] =  plotCube([lP, wP, hP],rP2_No_N(1,:), qP2_ZYX(1,:), 'ZYX');
[P3Vertices, P3Faces] =  plotCube([lP, wP, hP],rP3_No_N(1,:), qP3_ZYX(1,:), 'ZYX');
[P4Vertices, P4Faces] =  plotCube([lP, wP, hP],rP4_No_N(1,:), qP4_ZYX(1,:), 'ZYX');


animationFigure = figure('units','normalized','outerposition',[0 0 1 1]);
view(3);
hold on

[bodyVertices, bodyFaces] =  plotCube(vehicleDim,[xB(1), yB(1), zB(1)], qB_ZYX, 'ZYX');
bodyPlot = patch('Vertices',bodyVertices,'Faces',bodyFaces, 'FaceColor', [0.5, 0.5, 0.5], 'facealpha', 0.25);


A1Plot = plot3([xB(1); rP1_No_N(1,1) ], [yB(1); rP1_No_N(1,2)], [zB(1); rP1_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A2Plot = plot3([xB(1); rP2_No_N(1,1) ], [yB(1); rP2_No_N(1,2)], [zB(1); rP2_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A3Plot = plot3([xB(1); rP3_No_N(1,1) ], [yB(1); rP3_No_N(1,2)], [zB(1); rP3_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A4Plot = plot3([xB(1); rP4_No_N(1,1) ], [yB(1); rP4_No_N(1,2)], [zB(1); rP4_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);

P1Plot = patch('Vertices',P1Vertices,'Faces',P1Faces, 'FaceColor', [0, 0, 0]);
P2Plot = patch('Vertices',P2Vertices,'Faces',P2Faces, 'FaceColor', [0, 0, 0]);
P3Plot = patch('Vertices',P3Vertices,'Faces',P3Faces, 'FaceColor', [0, 0, 0]);
P4Plot = patch('Vertices',P4Vertices,'Faces',P4Faces, 'FaceColor', [0, 0, 0]);


xlabel('x Position [m]');
ylabel('y Position [m]');
zlabel('z Position [m]');




if savePlots
  saveas(errorVTime, fullfile(params.resultsFolder,['errorVTime','.png'] ));
end

end