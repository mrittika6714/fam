function [SolutionToAlgebraicEquations] = quadRotor_calc_alphaB_N_B( qBx, qBy, qBz, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz )
if( nargin ~= 9 ) error( 'quadRotor_calc_alphaB_N_B expects 9 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotor_calc_alphaB_N_B.m created Jan 15 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 1027 );



%===========================================================================
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(4) = sin(qBy);
z(9) = z(4)*z(8);
z(191) = z(1)*z(14) + z(4)*z(9);
z(192) = (wBx*z(14)+wBz*z(9))/z(191);
qBxDt = z(192);
z(3) = sin(qBx);
z(193) = (wBx*z(4)-wBz*z(1))/z(191);
z(194) = -wBy - z(3)*z(193);
qByDt = -z(194);
qBzDt = -z(193);
z(18) = z(1)*z(8)*qByDt - z(3)*z(4)*qBxDt;
z(20) = z(8)*qBxDt*qBzDt;
z(21) = -z(1)*z(3)*qBxDt - z(4)*z(8)*qByDt;
z(195) = z(4)*qBxDt*qByDt + qBzDt*z(18);
z(198) = -z(1)*qBxDt*qByDt - qBzDt*z(21);
z(1019) = z(14)/z(191);
z(1020) = z(9)/z(191);
z(1021) = qBxDDot - (z(9)*z(198)+z(14)*z(195))/z(191);
z(1022) = z(3)*z(4)/z(191);
z(1023) = z(1)*z(3)/z(191);
z(1024) = qByDDot + z(20) + z(3)*(z(1)*z(198)-z(4)*z(195))/z(191);
z(1025) = z(4)/z(191);
z(1026) = z(1)/z(191);
z(1027) = qBzDDot - (z(1)*z(198)-z(4)*z(195))/z(191);

COEF = zeros( 3, 3 );
COEF(1,1) = z(1019);
COEF(1,3) = z(1020);
COEF(2,1) = z(1022);
COEF(2,2) = 1;
COEF(2,3) = -z(1023);
COEF(3,1) = -z(1025);
COEF(3,3) = z(1026);
RHS = zeros( 1, 3 );
RHS(1) = z(1021);
RHS(2) = z(1024);
RHS(3) = z(1027);
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
wBxDt = SolutionToAlgebraicEquations(1);
wByDt = SolutionToAlgebraicEquations(2);
wBzDt = SolutionToAlgebraicEquations(3);



%==================================================
end    % End of function quadRotor_calc_alphaB_N_B
%==================================================
