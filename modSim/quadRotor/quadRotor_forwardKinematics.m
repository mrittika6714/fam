function [taskPos, taskVel] = quadRotor_forwardKinematics( xB, yB, zB, qBx, qBy, qBz, xBDt, yBDt, zBDt, wBx, wBy, wBz )
if( nargin ~= 12 ) error( 'quadRotor_forwardKinematics expects 12 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: quadRotor_forwardKinematics.m created Jan 15 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
taskPos = zeros( 4, 1 );
taskVel = zeros( 4, 1 );
z = zeros( 1, 1018 );



%===========================================================================
z(4) = sin(qBy);
z(1) = cos(qBy);
z(8) = cos(qBx);
z(14) = z(1)*z(8);
z(9) = z(4)*z(8);
z(191) = z(1)*z(14) + z(4)*z(9);
z(193) = (wBx*z(4)-wBz*z(1))/z(191);
qBzDt = -z(193);



%===========================================================================
Output = [];

taskPos(1) = xB;
taskPos(2) = yB;
taskPos(3) = zB;
taskPos(4) = qBz;

taskVel(1) = xBDt;
taskVel(2) = yBDt;
taskVel(3) = zBDt;
taskVel(4) = qBzDt;


%====================================================
end    % End of function quadRotor_forwardKinematics
%====================================================
