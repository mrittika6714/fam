% MotionGenesis file:  quadRotor.txt
% Copyright (c) 2018 Motion Genesis LLC.  All rights reserved.
% Problem:  s.
%--------------------------------------------------------------------
autozee(off)

%---------------------------------------------------------------
% Rigid Bodies, Frames, Points and Particles
%---------------------------------------------------------------
NewtonianFrame  N                 % Intertial NED Plane.
RigidBody       B                 % Quadcoptor Frame
RigidBody       Prop1, Prop2, Prop3, Prop4
%---------------------------------------------------------------
% Variables, Constants, and Specifieds
%---------------------------------------------------------------
Variable q1'', q2'', q3'', q4''	    % Propellor Angular Rates
Variable  xB'',  yB'', zB''         % Locates system center of mass.
Variable vBx', vBy', vBz'
Variable  qBx'',  qBy'',  qBz''     % Roll angle, Pitch angle, yaw angle.
Variable wBx', wBy', wBz'
Constant  g = 9.8 m/s^2             % Earth's gravitational acceleration.
Constant  bThrust                   % Aerodynamic damping on propellors
Constant  bDrag                     % Aerodynamic damping on propellors
Constant mB, IBxx, IByy, IBzz,   IBxy, IByz, IBzx  
Constant mProp, IPropxx, IPropyy, IPropzz
Constant lA          % arm lengths
Variables F1, F2, F3, F4
%Variables T1, T2, T3, T4
Points A1(B), A2(B), A3(B), A4(B)   
rollRates = [qBx',  qBy',  qBz']    % phi, theta, psi

%---------------------------------------------------------------
% Set Mass and Inertia Properties
%---------------------------------------------------------------

B.SetMass( mB )
B.SetInertia( BCm,  IBxx, IByy, IBzz,   IBxy, IByz, IBzx )

Prop1.SetMass( mProp)
Prop1.SetInertia( Prop1Cm,  IPropxx, IPropyy, IPropzz,   0, 0, 0 )
Prop2.SetMass( mProp)
Prop2.SetInertia( Prop2Cm,  IPropxx, IPropyy, IPropzz,   0, 0, 0 )
Prop3.SetMass( mProp)
Prop3.SetInertia( Prop3Cm,  IPropxx, IPropyy, IPropzz,   0, 0, 0 )
Prop4.SetMass( mProp)
Prop4.SetInertia( Prop4Cm,  IPropxx, IPropyy, IPropzz,   0, 0, 0 )

%--------------------------------------------------------------------
%       Rotational kinematics.
%---------------------------------------------------------------

B.Rotate( N, BodyZXY, qBz, qBx, qBy )   % z-Yaw-psi, y-pitch-theta, x-roll-phi

Prop1.Rotatez(B, q1)
Prop2.Rotatez(B, q2 )
Prop3.Rotatez(B, q3  )
Prop4.Rotatez(B, q4  )

%   Change of Variables to Efficient angular velocity variables
BzeroAngularVelocity> = wBx*Bx> + wBy*By> + wBz*Bz> - B.GetAngularVelocity(N)
BchangeAngularVelocityVariableEqns = Matrix(B,express(BzeroAngularVelocity>,B))
solveDt(BchangeAngularVelocityVariableEqns = 0, qBx', qBy', qBz')
B.SetAngularVelocityAcceleration( N, wBx*Bx> + wBy*By> + wBz*Bz> )

%---------------------------------------------------------------
%   Translational kinematics.
%---------------------------------------------------------------
Bo.Translate ( No, xB * Nx> + yB * Ny> + zB * Nz>)
Bcm.Translate(Bo, 0>)

% Set Arm Positions
A1.Translate(Bo, lA * Bx>)
A2.Translate(Bo, lA * By>)
A3.Translate(Bo, -lA * Bx>)
A4.Translate(Bo, -lA * By>)


Prop1o.Translate(A1, 0>)
Prop1cm.Translate(Prop1o, 0>)
Prop2o.Translate(A2, 0>)
Prop2cm.Translate(Prop2o, 0>)
Prop3o.Translate(A3, 0>)
Prop3cm.Translate(Prop3o, 0>)
Prop4o.Translate(A4, 0>)
Prop4cm.Translate(Prop4o, 0>)


%   Change of Variables to Efficient velocity variables
BzeroVelocity> = vBx*Bx> + vBy*By> + vBz*Bz> - Bcm.GetVelocity(N)
BchangeVelocityVariableEqns = Matrix(B,BzeroVelocity>)
solveDt(BchangeVelocityVariableEqns =0, xB', yB', zB')
Bcm.setVelocityAcceleration(N,  vBx*Bx>+ vBy*By> + vBz*Bz>)


%test1> = express(prop1.getangularacceleration(N), B)
%test2> = express(B.getangularAcceleration(N) + dt(prop1.getangularvelocity(B),prop1) + cross(prop1.getangularvelocity(N), Prop1.getangularvelocity(B)) , B)

%issimplifyequal(test1>, test2>)

%---------------------------------------------------------------
% Add relevant forces and aerodynamic damping torque.
%---------------------------------------------------------------
System.AddForceGravity(-g * Nz>)

% Add forces from propellors
F1 = bThrust * q1'^2
F2 = bThrust * q2'^2
F3 = bThrust * q3'^2
F4 = bThrust * q4'^2
A1.AddForce( F1 * Bz> )
A2.AddForce( F2 * Bz> )
A3.AddForce( F3 * Bz> )
A4.AddForce( F4 * Bz> )

% Add Drag torque from propellors
T1 = F1 * (bDrag/bThrust)
T2 = -F2 * (bDrag/bThrust)
T3 = F3 * (bDrag/bThrust)
T4 = -F4* (bDrag/bThrust)
B.AddTorque( (T1 + T2 + T3 + T4) * Bz> )



%--------------------------------------------------------------------
%  MG Road Map Dynamics
%--------------------------------------------------------------------
% MGRoadMapEOMs[1] = Dot(Bx>, System.GetDynamics())  		% 
% MGRoadMapEOMs[2] = Dot(By>, System.GetDynamics())  		% 
% MGRoadMapEOMs[3] = Dot(Bz>, System.GetDynamics())  		%  
% MGRoadMapEOMs[4] = Dot(Bx>, System.GetDynamics(Bcm))  	% 
% MGRoadMapEOMs[5] = Dot(By>, System.GetDynamics(Bcm))  	% 
% MGRoadMapEOMs[6] = Dot(Bz>, System.GetDynamics(Bcm))  	%  
% MGRoadMapSolution = solve(MGRoadMapEOMs =0, vBx', vBy', vBz', wBx', wBy', wBz')

%--------------------------------------------------------------------
%	Kane's equations of motion -- [Use System.GetDynamics(Kane,Fx,Fy) for reaction forces].
%--------------------------------------------------------------------
SetGeneralizedSpeed( vBx, vBy, vBz, wBx, wBy, wBz)
KaneEOMs = System.GetDynamicsKane()
KaneSolution  =  solve(KaneEOMs =0, vBx', vBy', vBz', wBx', wBy', wBz')

% Note this simplifies quations based off certain assumptions
input   IPropxx = 0, IPropyy = 0, q1'' = 0, q2'' = 0, q3'' = 0, q4'' = 0
KaneSimplifiedEOMs = evaluateatinput(KaneEOMs)
KaneSimplifiedSolution = evaluateatinput(kaneSolution)
clearInput(IPropxx, IPropyy, q1'', q2'', q3'', q4'')

%--------------------------------------------------------------------
%  Manual MG Road Map Dynamics
%--------------------------------------------------------------------
% ManualEOMs[1] = Dot(Nx>, mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
% + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) + mprop*prop4cm.getacceleration(N) - System.GetStatics())  	
% ManualEOMs[2] = Dot(Ny>, mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
% + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) + mprop*prop4cm.getacceleration(N) - System.GetStatics())  	
% ManualEOMs[3] = Dot(Nz>, mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
% + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) + mprop*prop4cm.getacceleration(N) - System.GetStatics())  
% ManualEOMs[4] = Dot(Bx>, dt(System.getangularmomentum(Bo),N) - System.GetStatics(Bo))  
% ManualEOMs[5] = Dot(By>, dt(System.getangularmomentum(Bo),N) - System.GetStatics(Bo))  
% ManualEOMs[6] = Dot(Bz>, dt(System.getangularmomentum(Bo),N) - System.GetStatics(Bo))  
% ManualSolution = solve(ManualEOMs =0, vBx', vBy', vBz', wBx', wBy', wBz')




%--------------------------------------------------------------------
%  Manual MG Road Map Dynamics 2 (Fully decomposed expressions)
%--------------------------------------------------------------------
% manualSolution2Eq1> = mB*Bcm.getacceleration(N) + mprop*prop1cm.getacceleration(N) &
%                    + mprop*prop2cm.getacceleration(N) + mprop*prop3cm.getacceleration(N) &
%                    + mprop*prop4cm.getacceleration(N) - ((F1 + F2 + F3 + F4) * Bz> - g*(mB + 4*mProp)*Nz> )
% 
% BChangeAngMom> = dot(B.getInertiaDyadic(Bo), B.getangularAcceleration(N)) + cross(B.getangularvelocity(N), dot(B.getInertiaDyadic(Bo), B.getangularVelocity(N)))
% P1ChangeAngMom> = dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularAcceleration(N)) &
%                 + cross(prop1.getangularVelocity(N),dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) ) &
%                 + cross(prop1cm.getvelocityrelative(Bo,N), mprop*prop1cm.getVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getacceleration(N) )
% 
% P2ChangeAngMom> =  dot(Prop2.getInertiaDyadic(Prop2cm), Prop2.getangularAcceleration(N)) &
%                 + cross(prop2.getangularVelocity(N),dot(Prop2.getInertiaDyadic(Prop2cm), Prop2.getangularVelocity(N)) ) &
%                 + cross(prop2cm.getvelocityrelative(Bo,N), mprop*prop2cm.getVelocity(N)) + cross(prop2cm.getposition(Bo), mprop*prop2cm.getacceleration(N) )
% P3ChangeAngMom> =  dot(Prop3.getInertiaDyadic(Prop3cm), Prop3.getangularAcceleration(N)) &
%                 + cross(prop3.getangularVelocity(N),dot(Prop3.getInertiaDyadic(Prop3cm), Prop3.getangularVelocity(N)) ) &
%                 + cross(prop3cm.getvelocityrelative(Bo,N), mprop*prop3cm.getVelocity(N)) + cross(prop3cm.getposition(Bo), mprop*prop3cm.getacceleration(N) )
% P4ChangeAngMom> =  dot(Prop4.getInertiaDyadic(Prop4cm), Prop4.getangularAcceleration(N)) &
%                 + cross(prop4.getangularVelocity(N),dot(Prop4.getInertiaDyadic(Prop4cm), Prop4.getangularVelocity(N)) ) &
%                 + cross(prop4cm.getvelocityrelative(Bo,N), mprop*prop4cm.getVelocity(N)) + cross(prop4cm.getposition(Bo), mprop*prop4cm.getacceleration(N) )
% 
% manualSolution2Eq2> = BChangeAngMom> + P1ChangeAngMom> + P2ChangeAngMom> + P3ChangeAngMom>+ P4ChangeAngMom> & 
%                     -( (T1 + T2 + T3 + T4) * Bz>)   
% 
% ManualEOMs2[1] = Dot(Nx>, manualSolution2Eq1>)  	
% ManualEOMs2[2] = Dot(Ny>, manualSolution2Eq1>)  	
% ManualEOMs2[3] = Dot(Nz>, manualSolution2Eq1>)  
% ManualEOMs2[4] = Dot(Bx>, manualSolution2Eq2>)  
% ManualEOMs2[5] = Dot(By>, manualSolution2Eq2>)  
% ManualEOMs2[6] = Dot(Bz>, manualSolution2Eq2>)  
% ManualSolution2 = solve(ManualEOMs2 =0, xB'', yB'', zB'', wBx', wBy', wBz')
% 
% test3> = explicit((prop1.getangularmomentum(Bo)))
% p1angmom> = dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N))
% issimplifyequal(expand(explicit(test3>), 1:3), expand(explicit(p1angmom>),1:3))
% 
% test8> = dt(dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)), N) + dt( cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N)),N)
% test9> = dt( p1angmom>,N)
% test10> = dt(dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N)),N)
% issimplifyequal(expand(explicit(test10>), 1:3), expand(explicit(test9>),1:3))
% issimplifyequal(expand(explicit(dot(test8>,bx>)), 1:3), expand(explicit(dot(test9>,bx>)),1:3))
% issimplifyzero(expand(explicit(dot(test8>,by>)), 1:3) - expand(explicit(dot(test9>,by>)),1:3))
% issimplifyequal(expand(explicit(dot(test8>,bz>)), 1:3), expand(explicit(dot(test9>,bz>)),1:3))
% 
% issimplifyzero(expand(explicit(dot(test8> - test9>,Nx>)),1:3))
% issimplifyzero(expand(explicit(dot(test8> - test9>,Ny>)),1:3))
% issimplifyzero(expand(explicit(dot(test8> - test9>,Nz>)),1:3))
% issimplifyzero(expand(explicit(test8> - test9>),1:3))
% 
% 
% test6> = dt(dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)),N)
% test7> = dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularAcceleration(N)) &
%                 + cross(prop1.getangularVelocity(N),dot(Prop1.getInertiaDyadic(Prop1cm), Prop1.getangularVelocity(N)) )
% issimplifyequal(expand(explicit(test6>), 1:3), expand(explicit(test7>),1:3))
% 
% test4> = explicit(dt(cross(prop1cm.getposition(Bo), mprop*prop1cm.getVelocity(N)),N))
% test5> = explicit(cross(prop1cm.getvelocityrelative(Bo,N), mprop*prop1cm.getVelocity(N)) + cross(prop1cm.getposition(Bo), mprop*prop1cm.getacceleration(N) ))
% issimplifyequal(expand(explicit(test4>), 1:3), expand(explicit(test5>),1:3))
% 



%--------------------------------------------------------------------
%	Compare solutions for a sanity Check 
%--------------------------------------------------------------------
% If Symbolic comparison of solution is not feasible. Use matlab generated
% output files to numerically compare solutions. 
%issimplifyzero(expand(explicit(Kanesolution), 1:3), expandexplicit((MGRoadMapSolution),1:3))
%issimplifyequal(expand(explicit(Kanesolution), 1:3), expandexplicit((ManualSolution),1:3))

%issimplifyequal(expand(explicit(ManualEOMS), 1:3), expand(explicit(manualEOMS2),1:3))

%--------------------------------------------------------------------
%  Output functions for Matlab simulation
%--------------------------------------------------------------------
% % Outputs to integrate
% OutputEncode xB', yB', zB', qBx', qBy', qBz'
% OutputEncode xB'', yB'', zB'', wBx', wBy', wBz' 


% Code Algebraic(KaneSimplifiedEOMs := 0, xB'', yB'', zB'', wBx', wBy', wBz') &
%  quadRotorRatesSimpleMatlab.m(xB', yB', zB', qBx, qBy, qBz, &
%                   wBx, wBy, wBz, bThrust, bDrag, lA, &
%                   mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, &
%                   mProp, IPropzz, q1', q2', q3', q4')    
% 
% Code Algebraic(KaneEOMs := 0, xB'', yB'', zB'', wBx', wBy', wBz') &
%  quadRotorRatesMatlab.m(xB', yB', zB', qBx, qBy, qBz, &
%                   wBx, wBy, wBz, bThrust, bDrag, lA, &
%                   mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, &
%                   mProp, IPropxx, IPropyy, IPropzz, &
%                   q1, q2, q3, q4, q1', q2', q3', q4', &
%                   q1'', q2'', q3'', q4'')                                      
% 
% clear outputEncode
% 
% 
% OutputEncode KaneSimplifiedSolution
% Code Algebraic() simplifiedSolution.m(xB', yB', zB', qBx, qBy, qBz, &
%                   wBx, wBy, wBz, bThrust, bDrag, lA, &
%                   mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, &
%                   mProp, IPropzz, q1', q2', q3', q4')  
% 
% clear outputEncode                   




variables F_1, F_2, F_3, F_4
w1 = sign(F_1) * sqrt(abs(F_1)/bThrust)
w2 = sign(F_2) * sqrt(abs(F_2)/bThrust)
w3 = sign(F_3) * sqrt(abs(F_3)/bThrust)
w4 = sign(F_4) * sqrt(abs(F_4)/bThrust)


OutputEncode w1, w2, w3, w4
Code Algebraic() thrust2rates.m(F_1, F_2, F_3, F_4, bThrust) 
clearoutputEncode()

OutputEncode F1, F2, F3, F4
Code Algebraic() rates2Thrust.m(q1', q2', q3', q4', bThrust) 
clearoutputEncode()


Variables qBxDot, qByDot, qBzDot
qBDot = [qBx'; qBy'; qBz']
qBDotTowB_Eqn = qBDot -  [qBxDot; qByDot; qBzDot]
code Algebraic(qBDottowB_Eqn , wBx, wBy, wBz) qBDotTowB.m( qBx, qBxDot rad/s, qBy, qByDot rad/s, qBzDot rad/s)



%--------------------------------------------------------------------
%  Output functions for Visualizations
%--------------------------------------------------------------------
%BCOM = matrix(N,express(Bcm.getPosition(No),N))
%A4Position = matrix(N,express(A4.getPosition(No),N))
%OutputEncode BCOM[1], BCOM


%Code Algebraic() &
% quadRotorRatesAnimationOutputs.m(lA, qBx, y, z )                                      

%clear outputEncode

%--------------------------------------------------------------------
%  Extra Ouputs for Optimization
%--------------------------------------------------------------------
FVec = [ F1; F2; F3; F4;]
UVec = [ q1'; q2'; q3'; q4';]
appliedForces> =  Exclude( system.getStatics(), g)
appliedTorques> = Exclude( system.getStatics(Bcm), g)
appliedForcesMat = matrix(B,express(appliedForces>,B))
appliedTorquesMat = matrix(B,express(appliedTorques>,B))
wrench = explicit([appliedForcesMat; appliedTorquesMat], FVec)

B_FTMap = explicit(getCoefficient(wrench, FVec))

OutputEncode B_FTMap
Code Algebraic() hexRotorRates_B_FTMap.m(bDrag, bThrust, lA ) 
clearoutputEncode()

 %--------------------------------------------------------------------
%  Extra Ouputs for Analysis and Linearization
%-------------------------------------------------------------------- 

q =[xB; yB; zB; qBx; qBy; qBz]
qDot = dt(q)
qDotTrans = getTranspose(qDot)
qDotDot=dt(qDot)
x = [xB; yB; zB; qBx; qBy; qBz; vBx; vBy; vBz; wBx; wBy; wBz ]
xDot = dt(x)

% Note must delete the hardcoded 0 values for equilibrum variables and replace with sym variable definition below
% syms xBDt yBDt zBDt qBxDt qByDt qBzDt qP1zDt qP2zDt qP3zDt qP4zDt qP5zDt qP6zDt 
func= explicit([qDot; kaneSimplifiedSolution])
OutputEncode func
Code Algebraic() hexRotorRates_F.m( ) 
clearoutputEncode()

stop
%func2= ([qDot; kaneSimplifiedSolution])
%input xP1 = 0.186, yP1 =0 , zP1 = 0, qP1x = 0.49, qP1y = 0.33, qP1z = 0, xP2 = 0.093, yP2 = 0.161080725103906, zP2 = 0, &
%      qP2x = -0.49, qP2y = -0.33, qP2z = 0, xP3 = -0.093, yP3 = 0.161080725103906, zP3 = 0, qP3x = 0.49, qP3y = 0.33, qP3z = 0, & 
%      xP4 =  -0.1860, yP4 = 0, zP4 = 0, qP4x = -0.49, qP4y = -0.33, qP1z = 0, xP5 = -0.93, yP5 = -0.161080725103906, zP5 = 0, & 
%      qP5x = 0.49, qP5y = 0.33, qP5z = 0, xP6 = 0.093, yP6 = -0.161080725103906, zP6 = 0, qP6x = -0.49, qP6y = -0.33, qP6z = 0, & 
%      mB = 0.18, IBxx = 0.003, IByy = 0.0117, IBzz = 0.0023, IBxy = 0, IByz = 0, IBxz = 0, & 
%      P1Dir = 1, P2Dir = -1, P3Dir = 1, P4Dir = -1, P5Dir = 1, P6Dir = -1, &
%      mP = 1*10^(-4), IPzz = 5*10^(-5), bThrust = 6.11*10^(-8), bDrag = 1.5*10^(-9)

input xP1 = 0.186, yP1 =0 , zP1 = 0, qP1x = 0.945918894403333, qP1y = -0.214243042887366, qP1z = 0, xP2 = 0.093, yP2 = 0.161080725103906, zP2 = 0, &
      qP2x = -0.776367920195479, qP2y = -0.776367920195479, qP2z = 0, xP3 = -0.093, yP3 = 0.161080725103906, zP3 = 0, qP3x = -0.351433670913579, qP3y = -0.351433670913579, qP3z = 0, & 
      xP4 =  -0.1860, yP4 = 0, zP4 = 0, qP4x = 0.932315021837052, qP4y = 0.932315021837052, qP1z = 0, xP5 = -0.93, yP5 = -0.161080725103906, zP5 = 0, & 
      qP5x = -0.792711501943671, qP5y =  -0.792711501943671, qP5z = 0, xP6 = 0.093, yP6 = -0.161080725103906, zP6 = 0, qP6x = -0.354225075147182, qP6y = -0.354225075147182, qP6z = 0, & 
      mB = 0.18, IBxx = 0.003, IByy = 0.0117, IBzz = 0.0023, IBxy = 0, IByz = 0, IBxz = 0, & 
      P1Dir = 1, P2Dir = -1, P3Dir = 1, P4Dir = -1, P5Dir = 1, P6Dir = -1, &
      mP = 1*10^(-4), IPzz = 5*10^(-5), bThrust = 6.11*10^(-8), bDrag = 1.5*10^(-9)
funcVal = evaluateatInput(func)
J = D(funcVal, getTranspose(x))

OutputEncode J
Code Algebraic() hexRotorRates_J.m(mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, &
                  xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, &
                  xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, & 
                  qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, &
                  qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, &
                  qP1z', qP2z', qP3z', qP4z', qP5z', qP6z', qBx, qBy, qBz, qBx', qBy', qBz', &
                  mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir) 
clearoutputEncode()



BMat = D(funcVal, getTranspose(UVec))

OutputEncode BMat
Code Algebraic() hexRotorRates_B.m(mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, &
                  xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, &
                  xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, & 
                  qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, &
                  qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, &
                  qP1z', qP2z', qP3z', qP4z', qP5z', qP6z', qBx, qBy, qBz, qBx', qBy', qBz', &
                  mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir) 
clearoutputEncode()


OutputEncode F1, F2, F3, F4, F5, F6
Code Algebraic() rates2thrust.m(qP1z', qP2z', qP3z', qP4z', qP5z', qP6z', bThrust) 
clearoutputEncode()


stop


%-------------------------------------------------------
% NT = MUDot+R+G Form
%-------------------------------------------------------
TMat = [ q1'; q2'; q3'; q4']
q =[xB; yB; zB; qBx; qBy; qBz]
qDot = dt(q)
qDotTrans = getTranspose(qDot)
qDotDot=dt(qDot)
x = [xB; yB; zB; qBx; qBy; qBz; xB'; yB'; zB'; wBx; wBy; wBz ]
xDot = dt(x)
MqDotDot = includeA(KaneEOMs)
F = [explicit(qDot); explicit(kaneSimplifiedSolution, [x;TMat])]

%N =  getCoefficient(-KaneEOMs, TMat) 
%M = getCoefficent(KaneEOMS, qDotDot)
%R = (KaneEOMs + N*TMat - M*qDotDot)
J = D(F, getTranspose(x))

BMat = D(F, getTranspose(TMat))


OutputEncode F
Code Algebraic() quadRotorRatesF.m(bThrust, bDrag, lA, &
                  mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, &
                  mProp, IPropxx, IPropyy, IPropzz, &
                  qBx, qBy, qBz, wBx, wBy, wBz, q1', q2', q3', q4', &
                  xB', yB', zB') 
clearoutputEncode()

OutputEncode J
Code Algebraic() quadRotorRatesJ.m(bThrust, bDrag, lA, &
                  mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, &
                  mProp, IPropxx, IPropyy, IPropzz, &
                  qBx, qBy, qBz, wBx, wBy, wBz, q1', q2', q3', q4', &
                  xB', yB', zB')  
clearoutputEncode()

OutputEncode BMat
Code Algebraic() quadRotorRatesB.m(bThrust, bDrag, lA, &
                  mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, &
                  mProp, IPropxx, IPropyy, IPropzz, &
                  qBx, qBy, qBz, wBx, wBy, wBz, q1', q2', q3', q4', &
                  xB', yB', zB') 
clearoutputEncode()

%--------------------------------------------------------------------
%   Set Motion Variables 
%	Provide expressions for specified quantities.
%   For example, if t < 0,  TA = cos(t)  else  TA = exp(t)
% 	TA = IsNegative(t)*cos(t) + IsPositiveOr0(t) * exp(t)
%--------------------------------------------------------------------

% For forward dynamics specify forces/torques and solve for rates
% Note the propellor rates do not correspond to a DOF but to force inputs
q1'' =  ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )
q2'' =  ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )
q3'' = ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )
q4'' = ConvertUnits( 1000*sin(t) deg/sec^2, UnitSystem )


% For inverse dynamics specify rates and solve for forces
% Note System is underactuated so need to look into inverse 
% dynamics for underactuated systems (differential flatness)
%xB'' =  ConvertUnits( 0 m/sec^2, UnitSystem )
%yB'' =  ConvertUnits( 0 m/sec^2, UnitSystem )
%zB'' =  ConvertUnits( 0 m/sec^2, UnitSystem )
%wBx' =  ConvertUnits( 0 deg/sec^2, UnitSystem )
%wBy' =  ConvertUnits( 0 deg/sec^2, UnitSystem )
%wBz' =  ConvertUnits( 0 detg/sec^2, UnitSystem )


% For mixed dynamics specify a mixture of above and solve for
% appropriate forces and rates


%--------------------------------------------------------------------
%       Integration parameters and initial values for variables (e.g., for ODE command).
%--------------------------------------------------------------------
Input  tFinal = 60 sec,  tStep = 0.1 sec,  absError = 1.0E-12,  relError = 1.0E-12

Output t, xB, yB, zB, xb', yB', zB', Xb'', yB'', zB'', qBx, qBy, qBz, wBx, wBy, wBz

% State Initial Conditions
Input   xB = 0 m, yB = 0 m, zB = 0 m, &
        xB' = 0 m, yB' = 0 m, zB' = 0 m, &
        qBx = 0 deg, qBy = 0 deg, qBz = 0 deg, & 
        wBx = 0 deg/s, wBy = 0 deg/s, wBz = 0 deg/s, &
        q1 = 0, q2 = 0, q3 = 0, q4 = 0, &
        q1' = 0, q2' = 0, q3' =0, q4' = 0
% System Paramerter Inputs
Input mB = 0.478 kg, IBxx = 0.0117 kg*m^2, IByy =  0.0117 kg*m^2, IBzz = 0.00234 kg*m^2, &
      IBxy = 0, IByz = 0, IBzx = 0, lA = 0.18 m
Input mProp = 0.001 kg, IPropxx = 0, IPropyy = 0, IPropzz = 0.00005 kg*m^2, & 
      bThrust = 6.11*10^(-8), bDrag = 1.5*10^(-9)


%--------------------------------------------------------------------
%  Solve ODEs and save input/output
%--------------------------------------------------------------------
ODE(KaneEOMs := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesKane.m
ODE(MGRoadMapEOMs := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesMG.m
ODE(ManualEOMs := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesManual.m
ODE(ManualEOMs2 := 0,  xB'', yB'', zB'', wBx', wBy', wBz')  quadRotorRatesManua2.m