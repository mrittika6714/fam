% ToDo Look into using Jacobian for acceleration defined in task space to
% acceleration in generalized coordinates i.e. (joint space).

function control = quadRotor_controller(t, states, trajectories, trajectoryFunc, params)


% if strcmp(params.controlCallFlag , 'ODE45OutputFcn' 
% 
% else
% 
% end

[~, stateDottLast] = quadRotor_stateDotHist('getLast', 0, 0);
% Unpack parameters
mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPxx = params.IP(1,1);
IPyy = params.IP(2,2);
IPzz = params.IP(3,3);
IPxy = params.IP(1,2);
IPyz = params.IP(2,3);
IPxz = params.IP(1,3);
bThrust = params.bThrust;
bDrag = params.bDrag;
g = params.g;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);


xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(2);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(2);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(2);

% Set Propellor tilt angles to be static
qP1xDt = 0;
qP1yDt = 0;
qP2xDt = 0;
qP2yDt = 0;
qP3xDt = 0;
qP3yDt = 0;
qP4xDt = 0;
qP4yDt = 0;
qP1xDDt = 0;
qP1yDDt = 0;
qP2xDDt = 0;
qP2yDDt = 0;
qP3xDDt = 0;
qP3yDDt = 0;
qP4xDDt = 0;
qP4yDDt = 0;

controllerType = params.controller.type;
controllerSpace = params.controller.space;
numTrajectories =  params.trajectory.numTrajectories;
epsilon = 0.0000001;
maxIter = 1000;


%Unpack States
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
xBDt = states(7);
yBDt = states(8);
zBDt = states(9);
wBx = states(10);
wBy = states(11);
wBz = states(12);
qP1z = states(13);
qP2z = states(14);
qP3z = states(15);
qP4z = states(16);


qP1zDt = stateDottLast(13);
qP2zDt = stateDottLast(14);
qP3zDt = stateDottLast(15);
qP4zDt = stateDottLast(16);

% Assume rotor accelerations negligible
qP1zDDt = 0;
qP2zDDt = 0;
qP3zDDt = 0;
qP4zDDt = 0;


% Unpack Desired Trajectory 
if (strcmp(controllerSpace, 'Task') || strcmp(controllerSpace, 'task'))
    [rDes, rDotDes, rDDotDes, rDDDotDes, rDDDDotDes] = trajectoryFunc(trajectories, t);
    [taskPos, taskVel] = quadRotor_forwardKinematics( xB, yB, zB, qBx, qBy, qBz, xBDt, yBDt, zBDt, wBx, wBy, wBz );
    error = taskPos - rDes';
    errorDot = taskVel - rDotDes';
else
    [rDes, rDotDes, rDDotDes, rDDDotDes, rDDDDotDes] = trajectoryFunc(trajectories, t);

    % Calculate errors
%     error = posDes' - states(1:8);
% 
%     jointRates = [rBDot; qBDot; qL1Dt; qL2Dt];
%     errorDot = vDes' - jointRates;
%     errorSum = errorSum + error;
end
if strcmp(controllerType, 'none')
   control = zeros(4, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;

elseif strcmp(controllerType, 'LQR')

elseif strcmp(controllerType, 'FF')
    
    % Unpack Desired Trajectory
    xBDDtDes = rDDotDes(1);
    yBDDtDes = rDDotDes(2);
    zBDDtDes = rDDotDes(3);
    qBzDDtDes = rDDotDes(4);
    
    xBDtDes = rDotDes(1); 
    yBDtDes = rDotDes(2); 
    zBDtDes = rDotDes(3); 
    qBzDtDes = rDotDes(4); 
    
    xBDes = rDes(1); 
    yBDes = rDes(2); 
    zBDes = rDes(3); 
    qBzDes = rDes(4); 

    % Utilize Differential Flatness 
    %qByDDtDes = aDes(5);
    %qBzDDtDes = aDes(6);



    aBcm_N_NDes =  [xBDDtDes; yBDDtDes; zBDDtDes];
    wB_N_BDes = quadRotor_calc_wB_N_B( qBxDes, qByDes, qBzDes, qBxDotDes, qByDotDes, qBzDotDes );
    wBxDes = wB_N_BDes(1);
    wByDes = wB_N_BDes(2);
    wBzDes = wB_N_BDes(3);
    alphaB_N_BDes = quadRotor_calc_alphaB_N_B( qBxDes, qByDes, qBzDes, qBxDDtDes, qByDDtDes, qBzDDtDes, wBxDes, wByDes, wBzDes );
    a_Des_GenCoord = [aBcm_N_NDes; alphaB_N_BDes; ];
    rateError = mean(stateDottLast(13:16).^2);
    i=0;
    while ((rateError >= epsilon && i<maxIter) || i==0)
        %[NMat, MMat, GMat, VMat, ~, ~] = quadRotor_dynamicMatricesSimple( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, qP7zDt, qP8zDt );
        [NMat, MMat, GMat, VMat, ~, ~] = quadRotor_dynamicMatricesSuperSimple(mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4,qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, wBx, wBy, wBz );
        controlForces = pinv(NMat) * (MMat * a_Control_GenCoord  + VMat + GMat);
    
        % Convert Forces to propellor rates
        control = sign(controlForces).*sqrt(abs(controlForces)/bThrust);
    
        FFPropRates = [qP1zDt; qP2zDt;  qP3zDt; qP4zDt; ];
        qP1zDt = control(1);
        qP2zDt = control(2);
        qP3zDt = control(3);
        qP4zDt = control(4);
        rateError = immse(control,FFPropRates);
        i = i+1;
    end

elseif strcmp(controllerType, 'FF_PID_Task')

    kp = params.controller.kp;
    kd = params.controller.kd;
    ki = params.controller.ki;
    a_Control_Task = aDes' - kd.*errorDot - kp .* error; %- ki * errorSum; 
    xBDDot = a_Control_Task(1);
    yBDDot = a_Control_Task(2);
    zBDDot = a_Control_Task(3);
    qBxDDot = a_Control_Task(4);
    qByDDot = a_Control_Task(5);
    qBzDDot = a_Control_Task(6);
    qBxDot = vDes(4); 
    qByDot = vDes(5); 
    qBzDot = vDes(6); 

    aBcm_N_N =  [xBDDot; yBDDot; zBDDot];
%     [wB_N_B] = octoRotor_calc_wB_N_B( qBx, qBy, qBz, qBxDot, qByDot, qBzDot );
%     wBx = wB_N_B(1);
%     wBy = wB_N_B(2);
%     wBz = wB_N_B(3);
    alphaB_N_B = octoRotor_calc_alphaB_N_B( qBx, qBy, qBz, qBxDDot, qByDDot, qBzDDot, wBx, wBy, wBz );
    a_Control_GenCoord = [aBcm_N_N; alphaB_N_B; ];
    rateError = mean(stateDottLast(13:20).^2);
    i=0;
    while ((rateError >= epsilon && i<maxIter) || i==0)
        [NMat, MMat, GMat, VMat, ~, ~] = octoRotor_dynamicMatricesSimple( mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, qP7zDt, qP8zDt );
        %[NMat, MMat, GMat, VMat, ~, ~] = octoRotor_dynamicMatricesSuperSimple(mB, IBxx, IByy, IBzz, IBxy, IByz, IBxz, mP, IPzz, bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir, P7Dir, P8Dir, xB, yB, zB, qBx, qBy, qBz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, xP5, yP5, zP5, xP6, yP6, zP6, xP7, yP7, zP7, xP8, yP8, zP8, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, qP6z, qP7x, qP7y, qP7z, qP8x, qP8y, qP8z, wBx, wBy, wBz );
        control = pinv(NMat) * (MMat * a_Control_GenCoord  + VMat + GMat);
    
    % Convert Forces to propellor rates
    control = sign(control).*sqrt(abs(control)/bThrust);

    FFPropRates = [qP1zDt; qP2zDt;  qP3zDt; qP4zDt; qP5zDt; qP6zDt; qP7zDt; qP8zDt];
    qP1zDt = control(1);
    qP2zDt = control(2);
    qP3zDt = control(3);
    qP4zDt = control(4);
    qP5zDt = control(5);
    qP6zDt = control(6);
    qP7zDt = control(7);
    qP8zDt = control(8);
    rateError = immse(control,FFPropRates);
    i = i+1;
    end
    

    
else
  error('Controller Type not handled');
end 

end