%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function quadRotor_animation(results ,params, saveVideo)

% Unpack Parameters
lA = params.lA;
lP = params.plotting.lP;
wP = params.plotting.wP;
hP = params.plotting.hP;
armWidth = params.plotting.armWidth;
armColor = params.plotting.armColor;
vehicleDim = [params.plotting.lBody, params.plotting.wBody, params.plotting.hBody];
animationTimeScale = params.animationTimeScale;

% Unpack results
t = results.t;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;
numPoints = length(t);
states = results.states;
xB = results.states(:,1);
yB = results.states(:,2);
zB = results.states(:,3);
qBx = results.states(:,4);
qBy = results.states(:,5);
qBz = results.states(:,6);
rP1_No_N = results.additionalOutputs.animationOutputs(:,1:3);
rP2_No_N = results.additionalOutputs.animationOutputs(:,4:6);
rP3_No_N = results.additionalOutputs.animationOutputs(:,7:9); 
rP4_No_N = results.additionalOutputs.animationOutputs(:,10:12); 
qP1_ZYX = results.additionalOutputs.animationOutputs(:,13:15);
qP2_ZYX = results.additionalOutputs.animationOutputs(:,16:18);
qP3_ZYX = results.additionalOutputs.animationOutputs(:,19:21);
qP4_ZYX = results.additionalOutputs.animationOutputs(:,22:24);
qB_ZYX = results.additionalOutputs.animationOutputs(:,25:27);

% Unpack Desired Trajectories
setPoint = results.posDes;
% qB_ZXY_desired = [setPoint(:,6), setPoint(:,4), setPoint(:,5)];

[P1Vertices, P1Faces] =  plotCube([lP, wP, hP],rP1_No_N(1,:), qP1_ZYX(1,:), 'ZYX');
[P2Vertices, P2Faces] =  plotCube([lP, wP, hP],rP2_No_N(1,:), qP2_ZYX(1,:), 'ZYX');
[P3Vertices, P3Faces] =  plotCube([lP, wP, hP],rP3_No_N(1,:), qP3_ZYX(1,:), 'ZYX');
[P4Vertices, P4Faces] =  plotCube([lP, wP, hP],rP4_No_N(1,:), qP4_ZYX(1,:), 'ZYX');

if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end
animationFigure = figure('units','normalized','outerposition',[0 0 1 1]);
%for i = 1: params.additionalPlots
%   subplot(numRows,numCols,i*2);
%   plot(t, params.additionalData{i});
%   hold on
%   limits = [t(1) t(end) ylim*1.1];
%   axis(limits)
%   timeMark{i} = [ limits(1), limits(3); limits(1), limits(4)] ;
%   timeMarkPlot{i} = plot(timeMark{i}(:,1),timeMark{i}(:,2), '-k', 'linewidth', 2);
%   title(params.additionalTitles{i})
% end
% 
% 
% 
% subplot(numRows,numCols,1:2:numRows*numCols)
setPointPlot = plot3(setPoint(1),setPoint(2),setPoint(3), 'r.','markersize',25);
%view(0,90)
hold on

[bodyVertices, bodyFaces] =  plotCube(vehicleDim,[xB(1), yB(1), zB(1)], qB_ZYX, 'ZYX');
bodyPlot = patch('Vertices',bodyVertices,'Faces',bodyFaces, 'FaceColor', [0.5, 0.5, 0.5], 'facealpha', 0.25);

% [refBodyVertices, refBodyFaces] =  plotCube(vehicleDim,[setPoint(1,1), setPoint(1,2), setPoint(1,3)], qB_ZXY_desired(1,:), 'ZXY');
% refBodyPlot = patch('Vertices',refBodyVertices,'Faces',refBodyFaces, 'FaceColor', [1, 0.5, 0.5], 'facealpha', 0.25);


A1Plot = plot3([xB(1); rP1_No_N(1,1) ], [yB(1); rP1_No_N(1,2)], [zB(1); rP1_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A2Plot = plot3([xB(1); rP2_No_N(1,1) ], [yB(1); rP2_No_N(1,2)], [zB(1); rP2_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A3Plot = plot3([xB(1); rP3_No_N(1,1) ], [yB(1); rP3_No_N(1,2)], [zB(1); rP3_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);
A4Plot = plot3([xB(1); rP4_No_N(1,1) ], [yB(1); rP4_No_N(1,2)], [zB(1); rP4_No_N(1,3)], 'linewidth', armWidth, 'color', armColor);

P1Plot = patch('Vertices',P1Vertices,'Faces',P1Faces, 'FaceColor', [0, 0, 0]);
P2Plot = patch('Vertices',P2Vertices,'Faces',P2Faces, 'FaceColor', [0, 0, 0]);
P3Plot = patch('Vertices',P3Vertices,'Faces',P3Faces, 'FaceColor', [0, 0, 0]);
P4Plot = patch('Vertices',P4Vertices,'Faces',P4Faces, 'FaceColor', [0, 0, 0]);


pathPlot = plot3(xB(1), yB(1), zB(1), 'k--', 'linewidth',1);
desiredPathPlot = plot3(setPoint(:,1), setPoint(:,2), setPoint(:,3), 'r--', 'linewidth',1);
xMin = min([xB; setPoint(:,1)]) - lA*2.5;
xMax = max([xB; setPoint(:,1)]) + lA*2.5;
yMin = min([yB; setPoint(:,2)]) - lA*2.5;
yMax = max([yB; setPoint(:,2)]) + lA*2.5;
zMin = min([zB; setPoint(:,3)]) - lA*2.5;
zMax = max([zB; setPoint(:,3)]) + lA*2.5;
axis([xMin xMax yMin yMax zMin zMax])
xlabel('x Position [m]');
ylabel('y Position [m]');
zlabel('z Position [m]');
legend('Set Point', 'VehicleBody')
for i =1:numPoints
    setPointPlot.XData = setPoint(i,1);
    setPointPlot.YData = setPoint(i,2);
    setPointPlot.ZData = setPoint(i,3);
    
    bodyPlot.XData = xB(i);
    bodyPlot.YData = yB(i);
    bodyPlot.ZData = zB(i);
    [ bodyPlot.Vertices, bodyPlot.Faces] =  plotCube(vehicleDim,[xB(i), yB(i), zB(i)], qB_ZYX(i,:), 'ZYX');
    
%     refBodyPlot.XData = setPoint(i,1);
%     refBodyPlot.YData = setPoint(i,2);
%     refBodyPlot.ZData = setPoint(i,3);
%     [refBodyPlot.Vertices, refBodyPlot.Faces] =  plotCube(vehicleDim,[setPoint(i,1), setPoint(i,2), setPoint(i,3)], qB_ZXY_desired(i,:), 'ZXY');


    A1Plot.XData = [xB(i); rP1_No_N(i,1) ];
    A1Plot.YData = [yB(i); rP1_No_N(i,2) ];
    A1Plot.ZData = [zB(i); rP1_No_N(i,3) ];
    A2Plot.XData = [xB(i); rP2_No_N(i,1) ];
    A2Plot.YData = [yB(i); rP2_No_N(i,2) ];
    A2Plot.ZData = [zB(i); rP2_No_N(i,3) ];
    A3Plot.XData = [xB(i); rP3_No_N(i,1) ];
    A3Plot.YData = [yB(i); rP3_No_N(i,2) ];
    A3Plot.ZData = [zB(i); rP3_No_N(i,3) ];
    A4Plot.XData = [xB(i); rP4_No_N(i,1) ];
    A4Plot.YData = [yB(i); rP4_No_N(i,2) ];
    A4Plot.ZData = [zB(i); rP4_No_N(i,3) ];


    [ P1Plot.Vertices, P1Plot.Faces] =  plotCube([lP, wP, hP],rP1_No_N(i,:), qP1_ZYX(i,:), 'ZYX');
    [ P2Plot.Vertices, P2Plot.Faces] =  plotCube([lP, wP, hP],rP2_No_N(i,:), qP2_ZYX(i,:), 'ZYX');
    [ P3Plot.Vertices, P3Plot.Faces] =  plotCube([lP, wP, hP],rP3_No_N(i,:), qP3_ZYX(i,:), 'ZYX');
    [ P4Plot.Vertices, P4Plot.Faces] =  plotCube([lP, wP, hP],rP4_No_N(i,:), qP4_ZYX(i,:), 'ZYX');

    pathPlot.XData = xB(1:i);
    pathPlot.YData = yB(1:i);
    pathPlot.ZData = zB(1:i);
    desiredPathPlot.XData = setPoint(1:i,1);
    desiredPathPlot.YData = setPoint(1:i,2);
    desiredPathPlot.ZData = setPoint(1:i,3);

%     for ii = 1:params.additionalPlots
%       timeMarkPlot{ii}.XData = [t(i); t(i)];
%     end
  drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end


end