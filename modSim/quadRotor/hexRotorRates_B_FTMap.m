function hexRotorRates_B_FTMap( bDrag, bThrust, lA )
if( nargin ~= 3 ) error( 'hexRotorRates_B_FTMap expects 3 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: hexRotorRates_B_FTMap.m created May 30 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
B_FTMap = zeros( 6, 4 );




%===========================================================================


%===========================================================================
Output = [];

B_FTMap(:) = 0;
B_FTMap(3,1) = 1;
B_FTMap(3,2) = 1;
B_FTMap(3,3) = 1;
B_FTMap(3,4) = 1;
B_FTMap(4,2) = lA;
B_FTMap(4,4) = -lA;
B_FTMap(5,1) = -lA;
B_FTMap(5,3) = lA;
B_FTMap(6,1) = bDrag/bThrust;
B_FTMap(6,2) = -bDrag/bThrust;
B_FTMap(6,3) = bDrag/bThrust;
B_FTMap(6,4) = -bDrag/bThrust;


%==============================================
end    % End of function hexRotorRates_B_FTMap
%==============================================
