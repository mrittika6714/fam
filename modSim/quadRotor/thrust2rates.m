function [Output] = thrust2rates( F_1, F_2, F_3, F_4, bThrust )
if( nargin ~= 5 ) error( 'thrust2rates expects 5 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: thrust2rates.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================
w1 = sign(F_1)*sqrt(abs(F_1))/sqrt(bThrust);
w2 = sign(F_2)*sqrt(abs(F_2))/sqrt(bThrust);
w3 = sign(F_3)*sqrt(abs(F_3))/sqrt(bThrust);
w4 = sign(F_4)*sqrt(abs(F_4))/sqrt(bThrust);



%===========================================================================
Output = zeros( 1, 4 );

Output(1) = w1;
Output(2) = w2;
Output(3) = w3;
Output(4) = w4;

%=====================================
end    % End of function thrust2rates
%=====================================
