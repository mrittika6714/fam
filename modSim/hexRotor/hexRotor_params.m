function params = hexRotor_params(simParms)

propAngles = load('propAngles.mat');
qP1x = propAngles.propAngles(1);
qP1y = propAngles.propAngles(2);
qP2x = propAngles.propAngles(3);
qP2y = propAngles.propAngles(4);
qP3x = propAngles.propAngles(5);
qP3y = propAngles.propAngles(6);
qP4x = propAngles.propAngles(7);
qP4y = propAngles.propAngles(8);
qP5x = propAngles.propAngles(9);
qP5y = propAngles.propAngles(10);
qP6x = propAngles.propAngles(11);
qP6y = propAngles.propAngles(12);

params.IB = [0.003, 0, 0; 0, 0.0117, 0; 0, 0, 0.0023];
params.mB = 0.18;
params.g = 9.8;

params.mP = 0.0001;
params.IP = [0, 0, 0; 0, 0, 0; 0, 0, 0.00005];
lArm = 0.186;
params.lA = lArm;

params.armWidth = 5;
params.armColor = [0.5, 0.5, 0.5];
numProps = 6;
propPosAngle = linspace(0,2*pi,numProps+1); % rad
xPropPosVec = lArm * cos(propPosAngle');
yPropPosVec  = lArm * sin(propPosAngle');
zPropPosVec = zeros(numProps,1);
params.rP1_Bcm_B = [xPropPosVec(1), yPropPosVec(1), zPropPosVec(1)];
params.qP1_XYZ = [qP1x; qP1y; 0];
params.rP2_Bcm_B = [xPropPosVec(2), yPropPosVec(2), zPropPosVec(2)];
params.qP2_XYZ = [qP2x; qP2y; 0];
params.rP3_Bcm_B = [xPropPosVec(3), yPropPosVec(3), zPropPosVec(3)];
params.qP3_XYZ = [qP3x; qP3y; 0];
params.rP4_Bcm_B = [xPropPosVec(4), yPropPosVec(4), zPropPosVec(4)];
params.qP4_XYZ = [qP4x; qP4y; 0];
params.rP5_Bcm_B = [xPropPosVec(5), yPropPosVec(5), zPropPosVec(5)];
params.qP5_XYZ = [qP5x; qP5y; 0];
params.rP6_Bcm_B = [xPropPosVec(6), yPropPosVec(6), zPropPosVec(6)];
params.qP6_XYZ = [qP6x; qP6y; 0];


params.bThrust = 6.1100e-08;
params.bDrag = 1.5*10^-9;
params.propDirections = [ 1, -1, 1, -1, 1, -1];

forceGravity = params.g*(params.mB + params.mP * numProps);
forceP = forceGravity/numProps;
qProp = sqrt(forceP/params.bThrust);

uEqVal = load('uEqVal.mat');


params.controller.constantValue = uEqVal.uEqVal;%qProp * params.propDirections;  % rad/sec
Klqr = load('Klqr.mat');
params.controller.Klqr = Klqr.Klqr;
params.controller.uEqVal = uEqVal.uEqVal;
params.plotting.lP = lArm/3;
params.plotting.wP = params.plotting.lP/3;
params.plotting.hP = params.plotting.lP/10;
params.plotting.lBody = lArm*2.125;
params.plotting.wBody = lArm*2.125;
params.plotting.hBody = params.plotting.lP/5;


end