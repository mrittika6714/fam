function control = hexRotor_controller(t, states, trajectories, trajectoryFunc, params)

% Unpack Params

% Unpack states

controllerType = params.controller.controllerType;

if strcmp(controllerType, 'none')
   control = zeros(6, 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.constantValue;
elseif strcmp(controllerType, 'LQR')
  [posDes, vDes, aDes] = trajectoryFunc(trajectories, t);
  xDes = posDes(1);
  xDotDes = vDes(1);
  xDDotDes = aDes(1);
  yDes = posDes(2);
  yDotDes = vDes(2);
  yDDotDes = aDes(2);
  zDes = posDes(3);
  zDotDes = vDes(3);
  zDDotDes = aDes(3);
  qBxDes =  posDes(4);
  qBxDotDes= vDes(4); 
  qBxDotDotDes= aDes(4);
  qByDes =  posDes(5);
  qByDotDes= vDes(5); 
  qByDotDotDes= aDes(5);
  qBzDes =  posDes(6);
  qBzDotDes= vDes(6); 
  qBzDotDotDes= aDes(6);
  
  setPoint = [xDes; yDes; zDes; qBxDes; qByDes; qBzDes; ...
              xDotDes; yDotDes; zDotDes; qBxDotDes; qByDotDes; qBzDotDes; ...
              0; 0; 0; 0; 0; 0; ];
  Klqr = params.controller.Klqr;
  control = -[Klqr, zeros(6,6) ] *(states-setPoint) + params.controller.uEqVal;
      
else
  error('Controller Type not handled');
end 

end