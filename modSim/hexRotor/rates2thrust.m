function [Output] = rates2thrust( qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, bThrust )
if( nargin ~= 7 ) error( 'rates2thrust expects 7 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: rates2thrust.m created May 30 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
z = zeros( 1, 2233 );



%===========================================================================
F1 = bThrust*qP1zDt^2*sign(qP1zDt);
F2 = bThrust*qP2zDt^2*sign(qP2zDt);
F3 = bThrust*qP3zDt^2*sign(qP3zDt);
F4 = bThrust*qP4zDt^2*sign(qP4zDt);
F5 = bThrust*qP5zDt^2*sign(qP5zDt);
F6 = bThrust*qP6zDt^2*sign(qP6zDt);



%===========================================================================
Output = zeros( 1, 6 );

Output(1) = F1;
Output(2) = F2;
Output(3) = F3;
Output(4) = F4;
Output(5) = F5;
Output(6) = F6;

%=====================================
end    % End of function rates2thrust
%=====================================
