function [statesDot, additonalOutputs] = hexRotor_dynamics(t, states, params, controllerFunc)
u = controllerFunc(t, states);

% Unpack Inputs
qP1zDt = u(1);
qP2zDt = u(2);
qP3zDt = u(3);
qP4zDt = u(4);
qP5zDt = u(5);
qP6zDt = u(6);

%Unpack States
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
xBDt = states(7);
yBDt = states(8);
zBDt = states(9);
wBx = states(10);
wBy = states(11);
wBz = states(12);
qP1z = states(13);
qP2z = states(14);
qP3z = states(15);
qP4z = states(16);
qP5z = states(17);
qP6z = states(18);

% Unpack params

mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBxz = params.IB(1,3);

mP = params.mP;
IPzz = params.IP(3,3);
bThrust = params.bThrust;
bDrag = params.bDrag;
g = params.g;

P1Dir = params.propDirections(1);
P2Dir = params.propDirections(2);
P3Dir = params.propDirections(3);
P4Dir = params.propDirections(4);
P5Dir = params.propDirections(5);
P6Dir = params.propDirections(6);

xP1 = params.rP1_Bcm_B(1);
yP1 = params.rP1_Bcm_B(2);
zP1 = params.rP1_Bcm_B(3);
qP1x = params.qP1_XYZ(1);
qP1y = params.qP1_XYZ(2);

xP2 = params.rP2_Bcm_B(1);
yP2 = params.rP2_Bcm_B(2);
zP2 = params.rP2_Bcm_B(3);
qP2x = params.qP2_XYZ(1);
qP2y = params.qP2_XYZ(1);

xP3 = params.rP3_Bcm_B(1);
yP3 = params.rP3_Bcm_B(2);
zP3 = params.rP3_Bcm_B(3);
qP3x = params.qP3_XYZ(1);
qP3y = params.qP3_XYZ(1);

xP4 = params.rP4_Bcm_B(1);
yP4 = params.rP4_Bcm_B(2);
zP4 = params.rP4_Bcm_B(3);
qP4x = params.qP4_XYZ(1);
qP4y = params.qP4_XYZ(1);

xP5 = params.rP5_Bcm_B(1);
yP5 = params.rP5_Bcm_B(2);
zP5 = params.rP5_Bcm_B(3);
qP5x = params.qP5_XYZ(1);
qP5y = params.qP5_XYZ(1);

xP6 = params.rP6_Bcm_B(1);
yP6 = params.rP6_Bcm_B(2);
zP6 = params.rP6_Bcm_B(3);
qP6x = params.qP6_XYZ(1);
qP6y = params.qP6_XYZ(1);

[~, bodyStatesDot] = hexRotorRates_MGdynamicsSimplified2( xBDt, yBDt, zBDt, ...
    qBx, qBy, qBz, wBx, wBy, wBz, mB, IBxx, IByy, IBzz, IBxy, IByz, ...
    IBxz, xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, ...
    xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, qP2z, ...
    qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z, qP6x, qP6y, ...
    qP6z, qP1zDt, qP2zDt, qP3zDt, qP4zDt, qP5zDt, qP6zDt, mP, IPzz, ....
    bThrust, bDrag, g, P1Dir, P2Dir, P3Dir, P4Dir, P5Dir, P6Dir );

propStatesDot = [qP1zDt; qP2zDt; qP3zDt; qP4zDt; qP5zDt; qP6zDt];

[rP1_No_N, rP2_No_N, rP3_No_N, rP4_No_N, rP5_No_N, rP6_No_N, ...
          N_R_P1, N_R_P2, N_R_P3, N_R_P4, N_R_P5, N_R_P6 ] = ...
          hexRotorRates_MGanimationOutputs( xB, yB, zB, qBx, qBy, qBz, ...
          xP1, yP1, zP1, xP2, yP2, zP2, xP3, yP3, zP3, xP4, yP4, zP4, ...
          xP5, yP5, zP5, xP6, yP6, zP6, qP1x, qP1y, qP1z, qP2x, qP2y, ...
          qP2z, qP3x, qP3y, qP3z, qP4x, qP4y, qP4z, qP5x, qP5y, qP5z,...
          qP6x, qP6y, qP6z );
        
propPositions = [rP1_No_N; rP2_No_N; rP3_No_N; rP4_No_N; rP5_No_N; rP6_No_N ]';

qP1_ZYX = rotm2eul(N_R_P1, 'ZYX');        
qP2_ZYX = rotm2eul(N_R_P2, 'ZYX');  
qP3_ZYX = rotm2eul(N_R_P3, 'ZYX');  
qP4_ZYX = rotm2eul(N_R_P4, 'ZYX');  
qP5_ZYX = rotm2eul(N_R_P5, 'ZYX');  
qP6_ZYX = rotm2eul(N_R_P6, 'ZYX');  
propOrientations = [qP1_ZYX, qP2_ZYX, qP3_ZYX, qP4_ZYX, qP5_ZYX, qP6_ZYX];
animationOutputs = [propPositions, propOrientations];
additonalOutputs = [animationOutputs];

statesDot = [bodyStatesDot'; propStatesDot];

end