%% Setup workspace and define System Paramaters
 clear all; clc;  %close all; %#ok<CLALL>  %path(pathdef);
recalculate = true;
directory = '\\snl\home\mrittik\GitRepos\fam\modSim\octoRotor';
dynamics = 'forward';
model= 'planarQuadRotorThrust';
controllerType = 'PX4';
controllerSpace = 'task';
simDescription = 'test';
outputFunc = 'none';
plotResults = true;
savePlots = false;
animate = true;
saveAnimation =  false;
animationTimeScale = 1;  % e.g. 2 for 2x playback speed 
saveData = false;
videoName = [model, '_', controllerType, '_', simDescription];
resultsFileName = [model, '_', controllerType, '_', simDescription];
tStep = 0.1;
tFinal = 30;
format long

%% Create function handles and load Files from Model Directory
addpath(genpath(directory))
addpath(model)
addpath('plotFuncs')
addpath('Disturbances\')
trajectoryGenFolder = '\\snl\home\mrittik\Matlab\Trajectory_Generation';
addpath(genpath(trajectoryGenFolder))

% Create function handles
dynamicsFunc = str2func([model '_dynamics']);
controllerFunc = str2func([model '_controller']);
trajectoryFunc = str2func([model '_trajGen']);
trajectoryParamsFunc = str2func([model '_trajectoryParams']);
paramsFunc = str2func([model '_params']);
plotFunc = str2func([model '_plot']);
animateFunc = str2func([model '_animation']);

% Load model params
controlParams.type = controllerType;
controlParams.space = controllerSpace;
controlParams.func = controllerFunc;
simParams.control = controlParams;
simParams.tFinal = tFinal;
simParams.tStep = tStep;
simParams.trajectoryFunc = trajectoryFunc;
simParams.dynamics = dynamics;
simParams.model = model;
simParams.description = simDescription;
%params.videoName = videoName;
simParams.outputHeader = [model, '_', controllerType, '_', simDescription];
params = paramsFunc(simParams);

% Set ODE45 OutputFunc 
if strcmp(outputFunc,'custom')
    ODE45OutputFunc = str2func([model '_outputFunc']);
    ODE45OutputFunc = @(t,y,flag) ODE45OutputFunc(t,y,flag,params) ;
else
    ODE45OutputFunc = @displayStatus_dots;
end
%% Clear model functions for persistent variables
clear(func2str(dynamicsFunc))
clear(func2str(controllerFunc))
clear(func2str(trajectoryFunc))
clear(func2str(trajectoryParamsFunc))
clear GenerateTrajectoryPoint

%% Set up Numerical Integration 
tSpan = 0:tStep:tFinal;
opts = odeset('RelTol',1e-8,'AbsTol',1e-8, 'OutputFcn', ODE45OutputFunc );

%% Trajectory Params
trajectories = params.trajectory;
states0 = trajectories.ICs;

%% Run Simulation
if recalculate 

% Inverse Dynamics
if strcmp(dynamics, 'inverse')
    inverseDynamicsFunc = str2func([model '_inverseDynamics']);
    inverseDynamicsFunc = @(t) inverseDynamicsFunc(t, params );
    results.t = tSpan;
    numPoints = length(results.t);
    
[~, testOutput] = inverseDynamicsFunc(tSpan(1));

if isstruct(testOutput)
outputNames = fieldnames(testOutput);
numOutputs = length(outputNames);
emptyCell = cell(numOutputs,1);
results.additionalOutputs = cell2struct(emptyCell, outputNames, 1);
for i = 1: numPoints
    [results.states(i,:), newOutput] = inverseDynamicsFunc(tSpan(i));
    for ii = 1: numOutputs
        results.additionalOutputs.(outputNames{ii}) = [results.additionalOutputs.(outputNames{ii}); newOutput.(outputNames{ii})];
    end
end
else 
    for i = 1 :  numPoints
        [results.states(i,:),results.additionalOutputs(i,:)] = inverseDynamicsFunc(tSpan(i));
    end
end

% Forward Dynamics
else 
u = @(t, states) controllerFunc(t, states, trajectories, trajectoryFunc, params);
odeFunc = @(t,states) dynamicsFunc(t, states, params, u);
[results.t, results.states] = ode45(odeFunc, tSpan, states0, opts);

[results.posDes, results.vDes, results.aDes] = trajectoryFunc(trajectories, results.t);

numPoints = length(results.t);
% for i = 1 :  numPoints
%     [~,results.additionalOutputs(i,:)] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
% end

[~, testOutput] = dynamicsFunc(results.t(1), results.states(1,:)', params, u);

if isstruct(testOutput)
outputNames = fieldnames(testOutput);
numOutputs = length(outputNames);
emptyCell = cell(numOutputs,1);
results.additionalOutputs = cell2struct(emptyCell, outputNames, 1);
for i = 1: numPoints
    [~, newOutput] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
    for ii = 1: numOutputs
        results.additionalOutputs.(outputNames{ii}) = [results.additionalOutputs.(outputNames{ii}); newOutput.(outputNames{ii})];
    end
end
else 
    for i = 1 :  numPoints
        [~,results.additionalOutputs(i,:)] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
    end
end

end
end
%% Create Results Folder
  resultsFolderName = fullfile(directory, 'generated');
if (saveData || saveAnimation || savePlots)
  if ~exist(resultsFolderName, 'dir')
    mkdir(resultsFolderName);
  end
end

%% Save Data
if recalculate
if saveData
    save([resultsFolderName,'\',resultsFileName], 'results');
end
else
  load([resultsFolderName,'\',resultsFileName])
end
%% Plot Responses
if plotResults == 1
  params.resultsFolder = resultsFolderName;
  plotFunc(results, params, savePlots);
end

%% Animate Responses
if animate == 1
  videoName = fullfile(resultsFolderName, videoName);
  params.animationTimeScale = animationTimeScale;
  animateFunc(results, params, saveAnimation);
end


