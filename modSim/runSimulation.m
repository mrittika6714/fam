%% Setup workspace and define System Paramaters
close all; clear all; clc; restoredefaultpath; %#ok<CLALL>

model = 'planarQuadRotorThrust';
controllerType = 'FF_PID';
controllerSpace = 'task';
recalculate = true;
directory = strcat(".", filesep);
dynamics = 'forward';
disturbance = 'multisine';
% simDescription = 'ms_y_mg_div_10_dt_005_tF_60_f0_05_Nf_200_admY_mbk_8_2_10';
simDescription = 'dt_005_tF_30_spr_y_k_001000_eq_0_hy_0_1_admY_mbk_8_18_10_gainx5_test';
additionalDescription = 'test';
outputFunc = 'custom';
plotResults = true;
savePlots = false;
animate = true;
saveAnimation = true;
animationTimeScale = 1;  % e.g. 2 for 2x playback speed 

saveData = true;
videoName = [model, '_', controllerType, '_', simDescription];
resultsFileName = [model, '_', controllerType, '_', simDescription];
tStep  = 0.005;
tFinal = 10;
% tStep  = 0.05;
% tFinal = 11;
format long


%% Setup Working Paths
workingDir = pwd;
workingDirParts=regexp(workingDir,'\','split');
%addpath(genpath(directory))
% addpath(model)
% addpath(strcat(".", filesep, "plotFuncs"))
% trajectoryGenFolder = strcat(".", filesep);
% addpath(genpath(trajectoryGenFolder))

switch workingDirParts{end}
  case 'modSim'
    addpath(genpath('./Disturbances'))
    addpath(genpath('./PlotFuncs'))
    addpath(genpath(strcat('./',model)))
    generatedFolderPath = fullfile(workingDir ,model, '/generated');
  otherwise
    error('Please run in a modSim Directory')
end

%% Create function handles and load Files from Model Directory


% Create function handles
dynamicsFunc = str2func([model '_dynamics']);
controllerFunc = str2func([model '_controller']);
trajectoryFunc = str2func([model '_trajGen']);
trajectoryParamsFunc = str2func([model '_trajectoryParams']);
paramsFunc = str2func([model '_params']);
plotFunc = str2func([model '_plot']);
animateFunc = str2func([model '_animation']);

% Load model params
controlParams.type = controllerType;
controlParams.space = controllerSpace;
controlParams.func = controllerFunc;
simConfig.control = controlParams;
simConfig.additionalDescription = additionalDescription;
simConfig.tFinal = tFinal;
simConfig.tStep = tStep;
simConfig.trajectoryFunc = trajectoryFunc;
simConfig.dynamics = dynamics;
simConfig.model = model;
simConfig.description = simDescription;
%params.videoName = videoName;
simConfig.outputHeader = [model, '_', controllerType, '_', simDescription];
simConfig.disturbance.type = disturbance;
%% Generate a multisine signal (test)
f0   = 0.05;  % 1/20 Hz
df   = 0.05;
Nf   = 200;   % Number of frequencies to include (max Hz = 200*1/20 = 10 Hz)
Nper = 1;     % Number of periods (of f0) to generate
Ntry = 1;     % Number of tries to generate signal
Ts   = .001;  % Sample time

% Generate multisine signal
[ms_sig,ms_t,f,ph,A,Ts] = gen_multisine(f0,df,Nf,Nper,Ntry,Ts);
disp("Multisine generation complete!");
multisine = [ms_t', ms_sig'];


simConfig.multisine = multisine;
simConfig.ms_f0 = f0;
simConfig.ms_df = df;
simConfig.ms_Nf = Nf;
simConfig.ms_Ts = Ts;

params = paramsFunc(simConfig);

% Set ODE45 OutputFunc 
if strcmp(outputFunc,'custom')
    ODE45OutputFunc = str2func([model '_outputFunc']);
    ODE45OutputFunc = @(t,y,flag) ODE45OutputFunc(t,y,flag,params) ;
else
    ODE45OutputFunc = @displayStatus_dots;
end
%% Clear model functions for persistent variables
clear(func2str(dynamicsFunc))
clear(func2str(controllerFunc))
clear(func2str(trajectoryFunc))
clear(func2str(trajectoryParamsFunc))
clear GenerateTrajectoryPoint


%% Plot multisine and test interpolation

% Get individual values from multisine corresponding to sim times (test
% interpolation)
% t = 0:tStep:tFinal;
t = 0:0.0001:2;
ms_interp = zeros(size(t));
for i = 1:length(t)
  ms_interp(i) = get_multisine(t(i), 0, multisine);
end

% Plot Multisine
f = figure('Name', 'PX4 CTRL Errors', 'Position', [200, 100, 1200, 800]);
set(gca, 'FontSize', 12);
 plot(ms_t, ms_sig, 'o');
%plot(ms_t, ms_sig, '-');
 hold on;
 plot(t, ms_interp);
title('Multisine Signal', 'FontSize', 20, 'FontWeight', 'bold');
xlabel('Time (s)', 'FontSize', 14, 'FontWeight', 'bold');
ylabel('Signal', 'FontSize', 14, 'FontWeight', 'bold');
% legend('Mulisine', 'Multisine (interpolated)');
grid on;
grid minor;

%% Set up Numerical Integration 
tSpan = 0:tStep:tFinal;
opts = odeset('RelTol',1e-8,'AbsTol',1e-8, 'OutputFcn', ODE45OutputFunc );

%% Trajectory Params
trajectories = params.trajectory;
states0 = trajectories.ICs;

%% Run Simulation
if recalculate 
  % Inverse Dynamics
  if strcmp(dynamics, 'inverse')
    inverseDynamicsFunc = str2func([model '_inverseDynamics']);
    inverseDynamicsFunc = @(t) inverseDynamicsFunc(t, params );
    results.t = tSpan;
    numPoints = length(results.t);
    [~, testOutput] = inverseDynamicsFunc(tSpan(1));
    if isstruct(testOutput)
      outputNames = fieldnames(testOutput);
      numOutputs = length(outputNames);
      emptyCell = cell(numOutputs,1);
      results.additionalOutputs = cell2struct(emptyCell, outputNames, 1);
      for i = 1: numPoints
          [results.states(i,:), newOutput] = inverseDynamicsFunc(tSpan(i));
          for ii = 1: numOutputs
              results.additionalOutputs.(outputNames{ii}) = [results.additionalOutputs.(outputNames{ii}); newOutput.(outputNames{ii})];
          end
      end
    else 
        for i = 1 :  numPoints
            [results.states(i,:),results.additionalOutputs(i,:)] = inverseDynamicsFunc(tSpan(i));
        end
    end
  % Forward Dynamics
  else 
    u = @(t, states) controllerFunc(t, states, trajectories, trajectoryFunc, params);
    odeFunc = @(t,states) dynamicsFunc(t, states, params, u);
    [results.t, results.states] = ode23(odeFunc, tSpan, states0, opts);
    statesDot = zeros(length(results.t),6);
    
    [results.posDes, results.vDes, results.aDes] = trajectoryFunc(trajectories, results.t);
    
    numPoints = length(results.t);
    % for i = 1 :  numPoints
    %     [~,results.additionalOutputs(i,:)] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
    % end
    
    [~, testOutput] = dynamicsFunc(results.t(1), results.states(1,:)', params, u);
    
    if isstruct(testOutput)
      outputNames = fieldnames(testOutput);
      numOutputs = length(outputNames);
      emptyCell = cell(numOutputs,1);
      results.additionalOutputs = cell2struct(emptyCell, outputNames, 1);
      for i = 1: numPoints
        [~, newOutput] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
        for ii = 1: numOutputs
            results.additionalOutputs.(outputNames{ii}) = [results.additionalOutputs.(outputNames{ii}); newOutput.(outputNames{ii})];
        end
      end
    else 
      for i = 1 :  numPoints
          [statesDot(i,:),results.additionalOutputs(i,:)] = dynamicsFunc(results.t(i), results.states(i,:)', params, u);
      end
    end
  end %if inverseDynamics
end % if recalculate
results.params    = params;    % Save parameters
results.statesDot = statesDot; % Save state derivatives

%% Create Results Folder
  resultsFolderName = fullfile(directory, 'generated');
if (saveData || saveAnimation || savePlots)
  if ~exist(resultsFolderName, 'dir')
    mkdir(resultsFolderName);
  end
end

%% Create Generated Folders if desired
simConfigName = generateSimConfigName(simConfig);
simFolderPath = fullfile(generatedFolderPath , simConfigName);

i = 1;
if (saveData || saveAnimation || savePlots)
  simConfigNameTemp = simConfigName;
  while exist(simFolderPath, 'dir')
    warning('Desired folder already Exists')
    simConfigNameTemp = [simConfigName ,'_', num2str(i)];
    simFolderPath = fullfile(generatedFolderPath, simConfigNameTemp);
    i = i+1;
  end
  fprintf('Generated Folder:  %s\n', simFolderPath)
  mkdir(simFolderPath);
  simConfig.simParams.generatedPath = simFolderPath;
  simConfig.name = simConfigNameTemp;
  
else
  fprintf('Simulation not saved \n')
end

%% Save Data
if recalculate
if saveData
    save(strcat(resultsFolderName, filesep, resultsFileName), 'results');
end
else
  load(strcat(resultsFolderName, filesep, resultsFileName));
end
%% Plot Responses
if plotResults == 1
  params.resultsFolder = resultsFolderName;
  params.plotHeader = videoName;
  plotFunc(results, simConfig, savePlots);
end

%% Animate Responses
if animate == 1
  params.videoName = fullfile(resultsFolderName, videoName);
  params.visualization.animationTimeScale = animationTimeScale;
  params.visualization.maxFPS = 60;
  params.visualization.minFPS = 30;
  animateFunc(results, params, saveAnimation);
end


%% Helper Functions
function simConfigName = generateSimConfigName(simConfig)
additionalDescription = simConfig.additionalDescription;
  
  simConfigName = [simConfig.model,'_',simConfig.control.type, '_', simConfig.disturbance.type];
  if ~isempty(additionalDescription)
    simConfigName = [simConfigName, '_', additionalDescription];
  end
end