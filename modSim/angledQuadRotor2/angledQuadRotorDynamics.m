function [statesDot, propRates, thrust] = angledQuadRotorDynamics(t, states, params, inputFunc)
u = inputFunc(t, states);
%control = quadRotorThrustController(t, states, setPoint, params)
q1Dt = u(1);
q2Dt = u(2);
q3Dt = u(3);
q4Dt = u(4);
mB = params.mB;
xB = states(1);
yB = states(2);
zB = states(3);
qBx = states(4);
qBy = states(5);
qBz = states(6);
xBDt = states(7);
yBDt = states(8);
zBDt = states(9); 
wBx = states(10);
wBy = states(11);
wBz = states(12);
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBzx = params.IB(3,1);
lA = params.lA;
bThrust = params.bThrust;
bDrag = params.bDrag;
IPropxx = params.IProp(1,1);
IPropyy = params.IProp(2,2);
IPropzz = params.IProp(3,3);
qP1x = params.qP1x;
qP1y = params.qP1y;
qP2x = params.qP2x;
qP2y = params.qP2y;
qP3x = params.qP3x;
qP3y = params.qP3y;
qP4x = params.qP4x;
qP4y = params.qP4y;


mProp = params.mProp;
propRates = [q1Dt, q2Dt, q3Dt, q4Dt];
thrust = rates2Thrust( q1Dt, q2Dt, q3Dt, q4Dt, bThrust );
q1 = 0;
q2 = 0;
q3 = 0;
q4 = 0;

[~, Output] = angledQuadRotorSimpleMatlab( xBDt, yBDt, zBDt, qBx, qBy,...
  qBz, wBx, wBy, wBz, bThrust, bDrag, lA, q1, q2, q3, q4, mB, IBxx,...
  IByy, IBzz, IBxy, IByz, IBzx, mProp, IPropzz, q1Dt, q2Dt, q3Dt, ...
  q4Dt, qP1x, qP1y, qP2x, qP2y, qP3x, qP3y, qP4x, qP4y );

%extraOutput = planarQuadRotorAnimationOutputs( lA, qBx, y, z );

statesDot = Output';
end