function [Output] = thrust2rates( F_1, F_2, F_3, F_4, bThrust )
if( nargin ~= 5 ) error( 'thrust2rates expects 5 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: thrust2rates.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================





%===========================================================================
w1 = sign(F_1)*sqrt(abs(F_1))/sqrt(bThrust);
w2 = sign(F_2)*sqrt(abs(F_2))/sqrt(bThrust);
w3 = sign(F_3)*sqrt(abs(F_3))/sqrt(bThrust);
w4 = sign(F_4)*sqrt(abs(F_4))/sqrt(bThrust);



%===========================================================================
Output = zeros( 1, 4 );

Output(1) = w1;
Output(2) = w2;
Output(3) = w3;
Output(4) = w4;

%=====================================
end    % End of function thrust2rates
%=====================================
