function [Output] = rates2Thrust( q1Dt, q2Dt, q3Dt, q4Dt, bThrust )
if( nargin ~= 5 ) error( 'rates2Thrust expects 5 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: rates2Thrust.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================





%===========================================================================
F1 = bThrust*q1Dt^2;
F2 = bThrust*q2Dt^2;
F3 = bThrust*q3Dt^2;
F4 = bThrust*q4Dt^2;



%===========================================================================
Output = zeros( 1, 4 );

Output(1) = F1;
Output(2) = F2;
Output(3) = F3;
Output(4) = F4;

%=====================================
end    % End of function rates2Thrust
%=====================================
