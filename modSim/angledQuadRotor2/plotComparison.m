clc; clear; close all;
tStep = 0.01;
tFinal = 15;
tSpan = 0:tStep:tFinal;

%% setpoint tracking
rates_rates = load('rates_rates.mat');
rates_rates = rates_rates.propRates;
rates_angledSimple = load('rates_angled_simple.mat');
rates_angledSimple = rates_angledSimple.propRates;
rates_angled_angled = load('rates_angled_angled.mat');
rates_angled_angled = rates_angled_angled.propRates;
figure
plot(tSpan, rates_rates, 'o','linewidth', 2);
hold on
plot(tSpan, rates_angledSimple, '*','linewidth', 2);
plot(tSpan, rates_angled_angled, '.','linewidth', 2);
title('Propellor Time Histories: x0 = [ 0 0 0], xd = [ -5 -5 5]')
xlabel('Time [s]', 'fontweight', 'bold')
ylabel('Rates [RPM]', 'fontweight', 'bold')
legend('Prop1 Standard', 'Prop2 Standard','Prop3 Standard', 'Prop4 Standard', ...
  'Prop1 Simplified', 'Prop2 Simplified','Prop3 Simplified', 'Prop4 Simplified', ...
  'Prop1 Angled', 'Prop2 Angled','Prop3 Angled', 'Prop4 Angled')

%% Hover Condition

rates_rates_hover = load('rates_rates_hover.mat');
rates_rates_hover = rates_rates_hover.propRates;
rates_angled_simple_hover = load('rates_angled_simple_hover.mat');
rates_angled_simple_hover = rates_angled_simple_hover.propRates;
rates_angled_angled_hover = load('rates_angled_angled_hover.mat');
rates_angled_angled_hover = rates_angled_angled_hover.propRates;
figure
plot(tSpan, rates_rates_hover, 'o','linewidth', 2);
%set(gca,'ColorOrderIndex',1)
hold on
plot(tSpan, rates_angled_simple_hover, '*','linewidth', 2);
%set(gca,'ColorOrderIndex',1)
plot(tSpan, rates_angled_angled_hover, '.','linewidth', 2);
title('Propellor Time Histories: x0 = [ 0 0 5], xd = [ 0 0 5]')
xlabel('Time [s]', 'fontweight', 'bold')
ylabel('Rates [RPM]', 'fontweight', 'bold')
legend('Prop1 Standard', 'Prop2 Standard','Prop3 Standard', 'Prop4 Standard', ...
  'Prop1 Simplified', 'Prop2 Simplified','Prop3 Simplified', 'Prop4 Simplified', ...
  'Prop1 Angled', 'Prop2 Angled','Prop3 Angled', 'Prop4 Angled')


