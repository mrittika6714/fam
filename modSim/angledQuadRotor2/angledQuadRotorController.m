function control = angledQuadRotorController(t, states, statesDesired, params)

mode = params.mode;

if strcmp(mode, 'none')
    control = zeros(length(states), 1);
    
elseif strcmp(mode, 'constant')
   control = params.value;

elseif strcmp(mode, 'sin')
   control = ones(lengthStates, 1) * sin(t);

elseif strcmp(mode, 'LQR')
  Klqr = params.Klqr;
  uEq = params.uEq;
  control = -Klqr*(states-statesDesired) + uEq;
  
elseif strcmp(mode, 'PID')
  
  Kpid= params.Kpid;
  uEq = params.uEq;
  control = -Kpid(1)*(states-statesDesired) + uEq;
end    


end