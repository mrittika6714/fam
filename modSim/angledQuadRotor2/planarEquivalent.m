function planarEquivalent( xBDt, yBDt, zBDt, qBx, qBy, qBz, wBx, wBy, wBz, bThrust, bDrag, lA, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, mProp, IPropzz, q1Dt, q2Dt, q3Dt, q4Dt )
if( nargin ~= 25 ) error( 'planarEquivalent expects 25 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarEquivalent.m created Dec 05 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
KanePlanarSolution = zeros( 6, 1 );





%===========================================================================
F1 = bThrust*q1Dt^2;
F2 = bThrust*q2Dt^2;
F3 = bThrust*q3Dt^2;
F4 = bThrust*q4Dt^2;
T1 = bDrag*F1/bThrust;
T2 = -bDrag*F2/bThrust;
T3 = bDrag*F3/bThrust;
T4 = -bDrag*F4/bThrust;



%===========================================================================
Output = [];

KanePlanarSolution(1) = (F1+F2+F3+F4)*(sin(qBy)*cos(qBz)+sin(qBx)*sin(qBz)*cos(qBy))/(mB+4*mProp);
KanePlanarSolution(2) = (F1+F2+F3+F4)*(sin(qBy)*sin(qBz)-sin(qBx)*cos(qBy)*cos(qBz))/(mB+4*mProp);
KanePlanarSolution(3) = -(9.8*mB+39.2*mProp-F1*cos(qBx)*cos(qBy)-F2*cos(qBx)*cos(qBy)-F3*cos(qBx)*cos(qBy)-F4*cos(qBx)*cos(qBy))/(  ...
mB+4*mProp);
KanePlanarSolution(4) = -((IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-T1-T2-T3-T4-wBy*(IBxx*wBx+IBxy*wBy+  ...
IBzx*wBz))+(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(lA*F1+wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-lA*F3-2*mProp*lA^2*wBx*wBz-wBx*(  ...
IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBx*q2Dt-IPropzz*wBx*q3Dt-IPropzz*wBx*q4Dt-IPropzz*wBx*(4*wBz+q1Dt))+(IByz^2-(IByy+2*mProp*lA^2)*(  ...
IBzz+4*IPropzz+4*mProp*lA^2))*(lA*F2+wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-lA*F4-2*mProp*lA^2*wBy*wBz-wBy*(IByz*wBy+IBzx*wBx+IBzz*wBz)-  ...
IPropzz*wBy*q2Dt-IPropzz*wBy*q3Dt-IPropzz*wBy*q4Dt-IPropzz*wBy*(4*wBz+q1Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
KanePlanarSolution(5) = -((IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))*(wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-T1-T2-T3-T4-wBy*(IBxx*wBx+IBxy*wBy+  ...
IBzx*wBz))-(IByz*IBzx-IBxy*(IBzz+4*IPropzz+4*mProp*lA^2))*(lA*F2+wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-lA*F4-2*mProp*lA^2*wBy*wBz-wBy*(  ...
IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBy*q2Dt-IPropzz*wBy*q3Dt-IPropzz*wBy*q4Dt-IPropzz*wBy*(4*wBz+q1Dt))-(IBzx^2-(IBxx+2*mProp*lA^2)*(  ...
IBzz+4*IPropzz+4*mProp*lA^2))*(lA*F1+wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-lA*F3-2*mProp*lA^2*wBx*wBz-wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)-  ...
IPropzz*wBx*q2Dt-IPropzz*wBx*q3Dt-IPropzz*wBx*q4Dt-IPropzz*wBx*(4*wBz+q1Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(  ...
IByy+2*mProp*lA^2)-(IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));
KanePlanarSolution(6) = ((IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2))*(wBx*(IBxy*wBx+IByy*wBy+IByz*wBz)-T1-T2-T3-T4-wBy*(IBxx*  ...
wBx+IBxy*wBy+IBzx*wBz))+(IBxy*IByz-IBzx*(IByy+2*mProp*lA^2))*(lA*F2+wBz*(IBxy*wBx+IByy*wBy+IByz*wBz)-lA*F4-2*mProp*lA^2*wBy*wBz-wBy*(  ...
IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBy*q2Dt-IPropzz*wBy*q3Dt-IPropzz*wBy*q4Dt-IPropzz*wBy*(4*wBz+q1Dt))-(IBxy*IBzx-IByz*(IBxx+2*  ...
mProp*lA^2))*(lA*F1+wBz*(IBxx*wBx+IBxy*wBy+IBzx*wBz)-lA*F3-2*mProp*lA^2*wBx*wBz-wBx*(IByz*wBy+IBzx*wBx+IBzz*wBz)-IPropzz*wBx*q2Dt-  ...
IPropzz*wBx*q3Dt-IPropzz*wBx*q4Dt-IPropzz*wBx*(4*wBz+q1Dt)))/(IByz*(2*IBxy*IBzx-IByz*(IBxx+2*mProp*lA^2))-IBzx^2*(IByy+2*mProp*lA^2)-(  ...
IBzz+4*IPropzz+4*mProp*lA^2)*(IBxy^2-(IBxx+2*mProp*lA^2)*(IByy+2*mProp*lA^2)));


%=========================================
end    % End of function planarEquivalent
%=========================================
