%% Setup workspace and define System Paramaters
clc; clear; close all
animate = 1;
saveVideo = 0;
simulateLQR = 1;
simulatePID = 0;
videoName = 'angledQuadRotorAnimation_angled';
tStep = 0.01;
tFinal = 15;
tSpan = 0:tStep:tFinal;

load('params.mat')
x0 = [0; 0; 5; 0; 0; 0; 0; 0; 0; 0; 0; 0];
xDesired = [ 1; 1; 2; 0; 0; 0; 0; 0; 0; 0; 0; 0];
numInputs = 4;

%% Uncontrolled Dynamics 
params.mode = 'none';
setPoint = xDesired;
u = @(t, states) angledQuadRotorController(t, states, setPoint, params);
odeFunc = @(t,states) angledQuadRotorDynamics(t, states, params, u);
[tUncon, xUncon] = ode45(odeFunc, tSpan, x0);

%% LQR Controller 
if simulateLQR
load('Klqr.mat')
load('uEq.mat')
params.mode = 'LQR';
params.Klqr = Klqr;
params.uEq = uEqVal;
statesDesired = xDesired;
u = @(t, states) angledQuadRotorController(t, states, statesDesired, params);

odeFunc = @(t,states) angledQuadRotorDynamics(t, states, params, u);
[tControlled, xControlled] = ode45(odeFunc, tSpan, x0);

numPoints = length(tControlled);
propRates = zeros(numPoints, 4);
thrust = zeros(numPoints, 4);
for i = 1 :  numPoints
    [~, propRates(i,:), thrust(i,:)] = angledQuadRotorDynamics(tControlled(i), xControlled(i,:)', params, u);
end
xDesiredMat = repmat(xDesired', size(xControlled,1),1);
xError = xControlled- xDesiredMat;
drawStatesControlled = xControlled(:,:);

save('rates_angled_simple_hover.mat', 'propRates')
end
%% PID Controller 
if simulatePID
%load('Klqr.mat')
%load('uEq.mat')
params.mode = 'PID';
params.Kpid = [ 1 1 1];
statesDesired = xDesired;
u = @(t, states) angledQuadRotorController(t, states, statesDesired, params);
odeFunc = @(t,states) angledQuadRotorDynamics(t, states, params, u);
[tPID, xPID] = ode45(odeFunc, tSpan, x0);

numPoints = length(tPID);
propRatesPID = zeros(numPoints, 4);
thrustPID = zeros(numPoints, 4);
for i = 1 :  numPoints
    [~, propRatesPID(i,:), thrustPID(i,:)] = angledQuadRotorDynamics(tPID(i), xPID(i,:)', params, u);
end
xDesiredMat = repmat(xDesired', size(xPID,1),1);
xErrorPID = xPID- xDesiredMat;
drawStatesPID = xPID(:,:);

%save('rates_angled_simple_hover.mat', 'propRatesPID')
end
%% Plot Responses
figure
hold on 
for i = 1: 6
  subplot(6,1,i)
  %plot(tUncon, xUncon(:,i), 'k', 'linewidth', 2)
  hold on
  plot(tControlled, xControlled(:,i), 'r--', 'linewidth', 2)
%legend('Uncontrolled', 'Controlled')
end
figure
 plot(tControlled, xError, 'linewidth', 1)

figure 
plot(tControlled, propRates)
title('Propellor Rate Time Histories')


figure 
plot(tControlled, thrust)
title('Thrust Force Time Histories')
%% Animate Responses
if animate ==1
params.videoName = videoName;
params.xDesired = xDesired;    
params.additionalPlots = 3;
params.additionalData{1} = propRates;
params.additionalData{2} = thrust;
params.additionalData{3} = xError;
params.additionalTitles{1} = 'Propellor Rates';
params.additionalTitles{2} = 'Thrust Forces';
params.additionalTitles{3} = 'State Error';
angledQuadRotorAnimation(tControlled,drawStatesControlled,params,saveVideo)
end
