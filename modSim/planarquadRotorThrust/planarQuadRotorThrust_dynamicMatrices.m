function [NMat, MMat, GMat, CMat, J, JDot] = planarQuadRotorThrust_dynamicMatrices( mB, IBxx, qBx, g, lA )
if( nargin ~= 5 ) error( 'planarQuadRotorThrust_dynamicMatrices expects 5 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_dynamicMatrices.m created Jan 06 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
NMat = zeros( 3, 2 );
MMat = zeros( 3, 3 );
GMat = zeros( 3, 1 );
VMat = zeros( 3, 1 );
JVel_N = zeros( 3, 3 );
JDot_Vel_N = zeros( 3, 3 );




%===========================================================================


%===========================================================================
Output = [];

NMat(1,1) = -sin(qBx);
NMat(1,2) = -sin(qBx);
NMat(2,1) = cos(qBx);
NMat(2,2) = cos(qBx);
NMat(3,1) = lA;
NMat(3,2) = -lA;

MMat(:) = 0;
MMat(1,1) = mB;
MMat(2,2) = mB;
MMat(3,3) = IBxx;

GMat(1) = 0;
GMat(2) = g*mB;
GMat(3) = 0;

VMat(:) = 0;

JVel_N(:) = 0;
JVel_N(1,1) = 1;
JVel_N(2,2) = 1;
JVel_N(3,3) = 1;

JDot_Vel_N(:) = 0;


%==============================================================
end    % End of function planarQuadRotorThrust_dynamicMatrices
%==============================================================
