function qBxVal = planarQuadRotorThrust_calcqBx( g, yBDDt, zBDDt )
if( nargin ~= 3 ) error( 'planarQuadRotorThrust_calcqBx expects 3 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_calcqBx.m created Jan 06 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================




%===========================================================================
qBxVal = -atan(yBDDt/(g+zBDDt));



%===========================================================================
Output = zeros( 1, 1 );

Output(1) = qBxVal;

%======================================================
end    % End of function planarQuadRotorThrust_calcqBx
%======================================================
