function [NMat, MMat, GMat, VMat, JVel_N, JDot_Vel_N] = planarQuadRotorThrust_dynamicMatrices2( mB, IBxx, qBx, g );
if( nargin ~= 4 ) error( 'planarQuadRotorThrust_dynamicMatrices2 expects 4 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_dynamicMatrices2.m created Aug 30 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
NMat = zeros( 3, 2 );
MMat = zeros( 3, 3 );
GMat = zeros( 3, 1 );
VMat = zeros( 3, 1 );
JVel_N = zeros( 3, 3 );
JDot_Vel_N = zeros( 3, 3 );




%===========================================================================


%===========================================================================
Output = [];

NMat(1,1) = -sin(qBx);
NMat(1,2) = 0;
NMat(2,1) = cos(qBx);
NMat(2,2) = 0;
NMat(3,1) = 0;
NMat(3,2) = 1;

MMat(:) = 0;
MMat(1,1) = mB;
MMat(2,2) = mB;
MMat(3,3) = IBxx;

GMat(1) = 0;
GMat(2) = g*mB;
GMat(3) = 0;

VMat(:) = 0;

JVel_N(:) = 0;
JVel_N(1,1) = 1;
JVel_N(2,2) = 1;
JVel_N(3,3) = 1;

JDot_Vel_N(:) = 0;


%===============================================================
end    % End of function planarQuadRotorThrust_dynamicMatrices2
%===============================================================
