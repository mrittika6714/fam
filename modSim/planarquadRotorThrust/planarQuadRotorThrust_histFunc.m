function [tOut, stateDotOut, errorOut, errorIntOut, admCtrlOut] = planarQuadRotorThrust_histFunc(operation, t, stateDot, error, admCtrl)
persistent stateDotHist tHist errorHist errorIntHist admCtrlHist

% Initialize persistent variables
if isempty(stateDotHist)
    numStates     = 6;
    stateDotHist  = zeros(1,numStates);
    tHist         = 0;
    errorHist     = zeros(1,numStates);
    errorIntHist  = zeros(1,numStates);
    admCtrlHist   = zeros(1,5); % init, y0, y, yd, ydd
end

if strcmp(operation, 'add')
    stateDotHist  = [stateDotHist; stateDot];
    stateDotOut   = stateDotHist;
    tHist         = [tHist; t'];
    tOut          = tHist;
    errorHist     = [errorHist; error];
    errorOut      = errorHist;
    
    % Compute integral control terms
    dt            = tHist(end) - tHist(end-1);
    nextInt       = error.*dt;
    intSum        = errorIntHist(end,:) + nextInt;
    for i = 1:length(error)
      if abs(error(i)) < 0.001
        %fprintf("Resetting error sum %d\n", i);
%         intSum = zeros(1,6); % Reset sum at zero crossing
        intSum(end,i) = 0; % Reset sum at zero crossing
      end
    end
    errorIntHist  = [errorIntHist; intSum];
    errorIntOut   = errorIntHist;
    
    % Compute Admittance control terms (init, pos0, pos, vel, acc)
    % Use backward Euler method to integrate for velocity and position
    admPosPrev  = admCtrlHist(end,3);
    admVelPrev  = admCtrlHist(end,4);
    admAcc      = admCtrl(5);   % Get latest accel computed by controller
    admVel      = admVelPrev + admAcc*dt;  % Velocity
    admPos      = admPosPrev + admVel*dt;  % Position
    admCtrl(4)  = admVel;
    admCtrl(3)  = admPos;
    admCtrlHist = [admCtrlHist; admCtrl];
    admCtrlOut  = admCtrlHist;
    
elseif strcmp(operation, 'get')
    stateDotOut   = stateDotHist;
    tOut          = tHist;
    errorIntOut   = errorIntHist;
    errorOut      = errorHist;
    admCtrlOut    = admCtrlHist;
    
elseif strcmp(operation, 'clear')
    clear stateDotHist tHist
    stateDotOut   = [];
    tOut          = [];
    errorIntOut   = [];
    errorOut      = [];
    admCtrlOut    = [];
    
elseif strcmp(operation,'getLast')
    if(t == 0)
      index = 1;
    else
      index = find(tHist < t, 1, 'last');
    end
    
    stateDotOut   = stateDotHist(index,:);
    tOut          = tHist(index);
    errorIntOut   = errorIntHist(index,:);
    errorOut      = errorHist(index,:);
    admCtrlOut    = admCtrlHist(index,:);

    
%     if (~isempty(index))
%       stateDotOut   = stateDotHist(index,:);
%       tOut          = tHist(index);
%       errorIntOut   = errorIntHist(index,:);
%       errorOut      = errorHist(index,:);
%       admCtrlOut    = admCtrlHist(index,:);
%     else
%       stateDotOut   = stateDotHist(end,:);
%       tOut          = tHist(end);
%       errorIntOut   = errorIntHist(end,:);
%       errorOut      = errorHist(end,:);
%       admCtrlOut    = admCtrlHist(end,:);
%     end
    
else
    error('Case not Handled');
end

end
