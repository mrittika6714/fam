function [Output] = planarQuadRotorThrust_flatnessEqs2( g, qBx, qBxDt, qBxDDt, yBDDt, yBDDDt, yBDDDDt, zBDDt, zBDDDt, zBDDDDt )
if( nargin ~= 10 ) error( 'planarQuadRotorThrust_flatnessEqs2 expects 10 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_flatnessEqs2.m created Jan 06 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
qBxDot = zeros( 1, 1 );
qBxDDot = zeros( 1, 1 );




%===========================================================================


%===========================================================================
Output = zeros( 1, 1 );

Output(1) = qBx;

qBx = -atan(yBDDt/(g+zBDDt));
qBxDot(1) = cos(qBx)^2*(yBDDt*zBDDDt-(g+zBDDt)*yBDDDt)/(g+zBDDt)^2;

qBxDDot(1) = -cos(qBx)^2*(2*sin(qBx)*qBxDt^2/cos(qBx)^3+(2*yBDDt*zBDDDt^2+(g+zBDDt)^2*yBDDDDt-2*(g+zBDDt)*yBDDDt*zBDDDt-yBDDt*(g+  ...
zBDDt)*zBDDDDt)/(g+zBDDt)^3);


%===========================================================
end    % End of function planarQuadRotorThrust_flatnessEqs2
%===========================================================
