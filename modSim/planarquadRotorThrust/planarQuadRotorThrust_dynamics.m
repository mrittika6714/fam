function [statesDot, additonalOutputs, errors, admCtrl] = planarQuadRotorThrust_dynamics(t, states, params, inputFunc)
[u, ctrlDes, errors, admCtrl]= inputFunc(t, states);
F2 = u(1);
F4 = u(2);

% Unpack Params
mB        = params.mB;
IBxx      = params.IB(1,1);
IByy      = params.IB(2,2);
IBzz      = params.IB(3,3);
IBxy      = params.IB(1,2);
IByz      = params.IB(2,3);
IBzx      = params.IB(3,1);
lA        = params.lA;
g         = params.g;
multisine = params.multisine;
msAxes    = params.msAxes;

% Unpack States
y     = states(1);
z     = states(2);
qBx   = states(3);
yBDt  = states(4);
zBDt  = states(5); 
qBxDt = states(6);

FBy = 0.0; 
FBz = 0.0;
TBx = 0;

% Add multisine forces, if applied
if(~isempty(multisine))
  if(contains(msAxes, "Y"))
    FBy = get_multisine(t, 0, multisine);
  end
  if(contains(msAxes, "Z"))
    FBz = get_multisine(t, 0, multisine);
  end
  if(contains(msAxes, "X"))
    TBx = get_multisine(t, 0, multisine);
  end
end

% Add step forces, if applied
stepForces = params.stepForces;
szSteps    = size(stepForces);
nSteps     = szSteps(1);
stepAxes   = params.stepAxes;
for i = 1:nSteps
  isSpring = ~isnan(stepForces(i,3));
  
  % Apply forces at specified times
  if t >= stepForces(i,1)
    if(contains(stepAxes, "X"))
      TBx = stepForces(i,2);
%       F2  = stepForces(i,2);
%       F4  = -stepForces(i,2);
    end
    if(contains(stepAxes, "Y"))
      f_cur = stepForces(i,2);
      if isSpring
        k  = stepForces(i,2);
        eq = stepForces(i,3);
        dy    = y - eq;
        f_cur = -k*dy;
        comp_only = stepForces(i,4); % Check if compression only
        if(comp_only == 1 && dy < 0)
          f_cur = 0; % apply no tension
        end
      end
      FBy = f_cur;
    end
    if(contains(stepAxes, "Z"))
      f_cur = stepForces(i,2);
      if isSpring
        k  = stepForces(i,2);
        eq = stepForces(i,3);
        dz    = z - eq;
        f_cur = -k*dz;
        comp_only = stepForces(i,4); % Check if compression only
        if(comp_only == 1 && dz < 0)
          f_cur = 0; % apply no tension
        end
      end
      FBz = f_cur;
    end
  end
end

[~, integrationOutputs ] = planarQuadRotorThrust_MGdynamics( F2, F4, FBy, FBz, TBx, qBx, yBDt, zBDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, g, lA );
%integrationOutputs = planarQuadRotorThrust_MGdynamics2( FB, TB, qBx, yBDt, zBDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, g );

AnimationOutput = planarQuadRotorThrustAnimationOutputs( lA, qBx, y, z );
inputForces = u;

statesDot = [integrationOutputs'];

additonalOutputs = [AnimationOutput, inputForces', FBy, FBz, TBx, ctrlDes, errors, admCtrl  ];
end