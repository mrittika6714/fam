%% Analysis of the planarQuadRotor
clc;clear;close all
% Define System
DOF = 3
params.IB(1,1) = 0.00025;
params.IB(2,2)= 0.0117;
params.IB(3,3) = 0.00234;
params.IB(1,2) = 0;
params.IB(2,3) = 0;
params.IB(3,1) =0;
params.mB = 0.18;
params.lA = 0.186;
params.g  = 9.8;
params.bThrust = 6.11 * 10^-8; %N/rpm^2
syms y z  qBx mB F4 IBxx yDot zDot qBxDot F2 g lA real
T = [F2; F4];
x = [y;  z;  qBx;  yDot;  zDot;  qBxDot];
xDot= [yDot; zDot; qBxDot;  -sin(qBx)*(F2+F4)/mB;  cos(qBx)*(F2+F4)/mB - g; lA*(F2-F4)/IBxx];
symParams = [ mB; IBxx; g; lA];
subParams = [ params.mB; params.IB(1,1); params.g; params.lA];

%% Check Acutatuon
Mat2 = zeros(6,3);
Mat2 = sym(Mat2);
Mat2(1,1) = 0;
Mat2(1,2) = 0;
Mat2(2,1) = 0;
Mat2(2,2) = 0;
Mat2(3,1) = 0;
Mat2(3,2) = 0;
Mat2(4,1) = -sin(qBx)/mB;
Mat2(4,2) = -sin(qBx)/mB;
Mat2(5,1) = cos(qBx)/mB;
Mat2(5,2) = cos(qBx)/mB;
Mat2(6,1) = lA/IBxx;
Mat2(6,2) = -lA/IBxx;

controlledDOF = rank(Mat2);

if controlledDOF < DOF
  warning('System is Underactuated')
end
%% Perform analysis of linearized system
Df=simplify(jacobian(xDot, x));
EqPoints = solve ( xDot  == 0, [qBx; yDot; zDot; qBxDot; F2; F4] );

J = zeros(6,6);
J = sym(J);
J(1,1) = 0;
J(1,2) = 0;
J(1,3) = 0;
J(1,4) = 1;
J(1,5) = 0;
J(1,6) = 0;
J(2,1) = 0;
J(2,2) = 0;
J(2,3) = 0;
J(2,4) = 0;
J(2,5) = 1;
J(2,6) = 0;
J(3,1) = 0;
J(3,2) = 0;
J(3,3) = 0;
J(3,4) = 0;
J(3,5) = 0;
J(3,6) = 1;
J(4,1) = 0;
J(4,2) = 0;
J(4,3) = -cos(qBx)*(F2+F4)/mB;
J(4,4) = 0;
J(4,5) = 0;
J(4,6) = 0;
J(5,1) = 0;
J(5,2) = 0;
J(5,3) = -sin(qBx)*(F2+F4)/mB;
J(5,4) = 0;
J(5,5) = 0;
J(5,6) = 0;
J(6,1) = 0;
J(6,2) = 0;
J(6,3) = 0;
J(6,4) = 0;
J(6,5) = 0;
J(6,6) = 0;
checkDf = simplify(Df-J)


% note from eqpoints we can linearize about any point

eqPoint = [0, 0, EqPoints.qBx(2), EqPoints.yDot(2), EqPoints.zDot(2), EqPoints.qBxDot(2)]
uEq = [EqPoints.F2(2); EqPoints.F4(2)];
uEqVal = double(subs(uEq, symParams, subParams));
A =subs(Df, [x; T], [eqPoint';EqPoints.F2(2); EqPoints.F4(2)] );
ASub = double(subs(A, symParams,subParams))

eigenvalues=expand(eig(A))

% note stabiility/instability of eigenvalyes 
eigs = eig(ASub)

 % Get B From Motion Genesis ToDO is get in matlab
Df_B=simplify(jacobian(xDot, T));
B_MG = [0, 0;...  
     0, 0; ...
     0, 0;...  
     -sin(qBx)/mB, 0; ... 
     cos(qBx)/mB, 0;...  
     0, 1/IBxx];
     
BSub = subs(Df_B, x, eqPoint');   
BSub = double(subs(BSub, symParams, subParams));
rankSys = rank(ctrb(ASub,BSub));


if rankSys < length(x)
  warning('System Not Controllable')
else
  fprintf('System is controllable. Rank of controllability matrix is = %i \n', rankSys)
end

% Create LQR Gain Matrix
v = [ 10 10 1000 1 1 1];
Q = diag(v);
R = eye(2)*0.01;

Klqr = lqr(ASub, BSub, Q, R )
eigs = eig(ASub-BSub*Klqr)
save('Klqr.mat', 'Klqr')
save('uEq.mat', 'uEqVal')
