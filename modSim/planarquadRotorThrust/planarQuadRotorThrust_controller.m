function [control, ctrlDes, errors, admCtrl]= planarQuadRotorThrust_controller(t, states, trajectories, trajectoryFunc, params)
%Unpack Params
IBxx = params.IB(1,1);
mB   = params.mB;
lA   = params.lA;
g    = params.g;

% Unpack States
yB    = states(1);
zB    = states(2);
qBx   = states(3);
yBDt  = states(4);
zBDt  = states(5);
qBxDt = states(6);
test = 1;
% Grab StateDot at last timestep
[tLast, stateDottLast, errorsLast, errorIntLast, admCtrlLast] = planarQuadRotorThrust_histFunc('getLast', t, [], [], []);
yBDtLast   = stateDottLast(1);
zBDtLast   = stateDottLast(2);
qBxDtLast  = stateDottLast(3);
yBDDtLast  = stateDottLast(4);
zBDDtLast  = stateDottLast(5);
qBxDDtLast = stateDottLast(6);

% Get desired states computed in last admittance control iteration (PX4)
admInit = admCtrlLast(1); % Admittance control initialization flag
admPos0 = admCtrlLast(2); % Position setpoint for admittance controller
admPos  = admCtrlLast(3); % Desired position to achieve desired admittance
admVel  = admCtrlLast(4); % Desired velocity to achieve desired admittance
admAcc  = admCtrlLast(5); % Desired acceleration to achieve desired admittance

%fprintf('Current time: %f, zBDt: %10.10f, zBDtLast: %10.10f \n', t, zBDt, zBDtLast)
% Unpack Desired States
[rDes, rDotDes, rDDotDes, rDDDotDes, rDDDDotDes] = trajectoryFunc(trajectories, t);
yBDes = rDes(1);
yBDtDes = rDotDes(1);
yBDDtDes = rDDotDes(1);
yBDDDtDes = rDDDotDes(1);
yBDDDDtDes = rDDDDotDes(1);
zBDes = rDes(2);
zBDtDes = rDotDes(2);
zBDDtDes = rDDotDes(2);
zBDDDtDes = rDDDotDes(2);
zBDDDDtDes  = rDDDDotDes(2);

% Calculate Desired qBx utilizing differential Flatness
qBxDes = planarQuadRotorThrust_calcqBx( g, yBDDtDes, zBDDtDes );
qBxDtDes = planarQuadRotorThrust_calcqBxDot( g, qBxDes, yBDDtDes, yBDDDtDes, zBDDtDes, zBDDDtDes );
qBxDDtDes = planarQuadRotorThrust_calcqBxDDot( g, qBxDes, qBxDtDes, yBDDtDes, yBDDDtDes, yBDDDDtDes, zBDDtDes, zBDDDtDes, zBDDDDtDes );
    
controllerType = params.controller.type;
error = [yB - yBDes, zB-zBDes, qBx- qBxDes];
errorDot = [yBDt - yBDtDes, zBDt - zBDtDes, qBxDt -  qBxDtDes];
errors = [error, errorDot];

% Compute time since last solution
dt = t - tLast;

% Compute current sum of errors for integral control
errorIntCurrent = errorIntLast + errors.*dt;

if strcmp(controllerType, 'none')
    control = zeros(length(states), 1);
  
    % Store the controller-computed setpoints for each state
    ctrlDes = zeros(1,8);
    %ctrlDes = [yBDes, zBDes, qBxCon, yBDtCon, zBDtCon, qBxDtCon, yBDDtCon, zBDDtCon];

    % Return error values so they can be summed for integral control
%     errors = px4Err;
    errors = zeros(1,6);

    % Only the admAcc is used
    admCtrl = zeros(1,5);
%     admCtrl = [admInit, admPos0, 0, 0, admAcc];
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.uEq;

elseif strcmp(controllerType, 'sin')
   control = ones(lengthStates, 1) * sin(t);
   
elseif strcmp(controllerType, 'LQR')
  setPoint = [ yBDes; zBDes; qBxDes ; yBDtDes; zBDtDes; qBxDtDes ];
  Klqr = params.controller.Klqr ;
  control = -Klqr*(states-setPoint) + params.controller.uEq;

elseif strcmp(controllerType, 'test')
  kpz = 10;
  kdz = 10;
  kiz = 0.0006;
  kpy = 100;
  kdy = 100;
  kiy = 0;
  kdq = 25;
  kpq = 25;
  kiq = 0; 
  planarQuadRotorThrust_calcqBxDDot( g, qBxCon, qBxDtCon, yBDDtCon, yBDDDtDes, yBDDDDtDes, zBDDtCon, zBDDDtDes, zBDDDDtDes )
  yBDDtCon = yBDDtDes + kdy*(yBDtDes - yBDt) + kpy*(yBDes - yB);
  zBDDtCon = zBDDtDes + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB);
  qBxCon = planarQuadRotorThrust_calcqBx( g, yBDDtCon, zBDDtCon );
  qBxDtCon = -(1/g)*(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt));
  qBxDDtCon = qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt);
  control= planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBx, qBxDDtCon, yBDDtCon, zBDDtCon, g, lA ); 

elseif strcmp(controllerType, 'PX4')
  
  % Read in external applied forces
  FBy = 0;
  FBz = 0;
  
  % Read in multisine force, if applied
  multisine  = params.multisine;
  msAxes     = params.msAxes;
  msApplied  = false;
  if(~isempty(multisine))
    if(contains(msAxes, "Y"))
      FBy = get_multisine(t, 0, multisine);
      msApplied = true;
    end
    if(contains(msAxes, "Z"))
      FBz = get_multisine(t, 0, multisine);
      msApplied = true;
    end
    if(contains(msAxes, "X"))
      TBx = get_multisine(t, 0, multisine);
      msApplied = true;
    end
  end
  
  % Add step forces, if applied
  stepForces = params.stepForces;
  szSteps    = size(stepForces);
  nSteps     = szSteps(1);
  stepAxes   = params.stepAxes;
  for i = 1:nSteps
    isSpring = ~isnan(stepForces(i,3));
    % Apply forces at specified times
    if t > stepForces(i,1)
      if(contains(stepAxes, "X"))
        TBx = stepForces(i,2);
      end
      if(contains(stepAxes, "Y"))
        f_cur = stepForces(i,2);
        if isSpring
          k  = stepForces(i,2);
          eq = stepForces(i,3);
          dy    = yB - eq;
          f_cur = -k*dy;
          comp_only = stepForces(i,4); % Check if compression only
          if(comp_only == 1 && dy < 0)
            f_cur = 0; % apply no tension
          end
        end
        FBy = f_cur;
      end
      if(contains(stepAxes, "Z"))
        f_cur = stepForces(i,2);
        if isSpring
          k  = stepForces(i,2);
          eq = stepForces(i,3);
          dz    = zB - eq;
          f_cur = -k*dz;
          comp_only = stepForces(i,4); % Check if compression only
          if(comp_only == 1 && dz < 0)
            f_cur = 0; % apply no tension
          end
        end
        FBz = f_cur;
      end
    end
  end

  % Compute state errors (desired - actual... y, z, th, ydot, zdot, thdot)
  px4Err = [yBDes - yB, zBDes - zB, 0, 0, 0, 0];
  
  % Compute current sum of errors for integral control
  errorIntCurrent(1) = errorIntLast(1) + px4Err(1)*dt;
  errorIntCurrent(2) = errorIntLast(2) + px4Err(2)*dt;
  
  % **********************************************************************
  %   Unpack PX4 gains
  % **********************************************************************
  Kpx     = params.Kpx;     % MPC_XY_P
  Kpz     = params.Kpz;     % MPC_Z_P
  Kpth    = params.Kpth;    % MC_PITCH_P
  Kpxdot  = params.Kpxdot;  % MPC_XY_VEL_P_ACC
  Kpzdot  = params.Kpzdot;  % MPC_Z_VEL_P_ACC
  Kpthdot = params.Kpthdot; % MC_PITCHRATE_P
  Kixdot  = params.Kixdot;  % MPC_XY_VEL_I_ACC
  Kizdot  = params.Kizdot;  % MPC_Z_VEL_I_ACC
  Kithdot = params.Kithdot; % MC_PITCHRATE_I
  Kdx     = params.Kdx;     % Defined in custom adm controller
  Kdz     = params.Kdz;     % Defined in custom adm controller
  Kdxdot  = params.Kdxdot;  % MPC_XY_VEL_D_ACC
  Kdzdot  = params.Kdzdot;  % MPC_Z_VEL_D_ACC
  Kdthdot = params.Kdthdot; % MC_PITCHRATE_D
  Kffth   = params.Kffth;   % MC_PITCHRATE_FF
  % ---------------------------------------------------------------------
  
  % **********************************************************************
  % Compute desired acceleration for admittance control from 2nd order EOM
  % **********************************************************************
  if(params.admCtrlAxes ~= "")
    % Set admittance setpoint from 
    if(contains(params.admCtrlAxes, "Y"))
      admPos0 = yBDes;
    elseif(contains(params.admCtrlAxes, "Z"))
      admPos0 = zBDes;
    end

    % Initialize Adm Ctrl variables
    if(admInit == 0)
      %disp("Initializing adm ctrl...");

      % Initialize state of desired dynamics with zeros
      admPos  = admPos0;  % Start w/desired position at setpoint
      admVel  = 0;
      admAcc  = 0;
      admInit = 1;
    end
    
    % Store previously computed desired admittance control state
    admPosPrev = admPos;
    admVelPrev = admVel;

    % Set desired admittance parameters
    admM = params.admM;  % Mass
    admB = params.admB;  % Damping
    admK = params.admK;  % Stiffness
    Fd   = params.admFd; % Desired applied force

    % Compute desired acceleration
    if(contains(params.admCtrlAxes, "Y"))
      admAcc = (1/admM)*( FBy + Fd - admB*(admVel) - admK*(admPos - admPos0) );
    elseif(contains(params.admCtrlAxes, "Z"))
      admAcc = (1/admM)*( FBz + Fd - admB*(admVel) - admK*(admPos - admPos0) );
    end

    % Use backward Euler method to integrate for velocity and position
    % of desired dynamics:   y(n) = y(n-1) + yd(n)*dt
    admVel = admVelPrev + admAcc*dt;
    admPos = admPosPrev + admVel*dt;

    % Update desired Y/Z position from admittance controller
    if(contains(params.admCtrlAxes, "Y"))
      px4Err(1) = admPos - yB;
      Kpx = 5*Kpx;
      Kdx = 5*Kdx;
    elseif(contains(params.admCtrlAxes, "Z"))
      px4Err(2) = admPos - zB;
%       Kpz = 5*Kpz;
%       Kdz = 5*Kdz;
    end
  end
  % ---------------------------------------------------------------------
  
  % **********************************************************************
  % Update Cascaded PX4 Controllers
  % **********************************************************************
  
  % Compute desired Y,Z velocity from PD controller
  yBDtCon  = Kpx*px4Err(1) - Kdx*(yBDt);
  zBDtCon  = Kpz*px4Err(2) - Kdz*(zBDt);
  
  % Compute error in velocities
  px4Err(4) = yBDtCon - yBDt;
  px4Err(5) = zBDtCon - zBDt;
  errorIntCurrent(4) = errorIntLast(4) + px4Err(4)*dt;
  errorIntCurrent(5) = errorIntLast(5) + px4Err(5)*dt;
  
  % Clamp integral effort
  if Kixdot*errorIntCurrent(4) > g
    %disp('clamping Xvel integral');
    errorIntCurrent(4) = g / Kixdot;
  elseif Kixdot*errorIntCurrent(4) < -g
    errorIntCurrent(4) = -g / Kixdot;
  end
  
  if Kizdot*errorIntCurrent(5) > g
    %disp('clamping Zvel integral');
    errorIntCurrent(5) = g / Kizdot;
  elseif Kizdot*errorIntCurrent(5) < -g
    errorIntCurrent(5) = -g / Kizdot;
  end
  
  % Compute desired Y,Z acceleration from PI+D controller
  yBDDtCon = Kpxdot*px4Err(4) + Kixdot*errorIntCurrent(4) - Kdxdot*yBDDtLast;
  zBDDtCon = Kpzdot*px4Err(5) + Kizdot*errorIntCurrent(5) - Kdzdot*zBDDtLast;
  
  % Calculate desired pitch angle from desired Y/Z accelerations
  denom = sqrt(yBDDtCon^2 + (zBDDtCon + g)^2); % denominator for normalizing
  qBxCon = -1*atan2( (yBDDtCon/denom), ((zBDDtCon + g)/denom) );
  
  % Limit to desired pitch to +/- 45 deg?
  if(qBxCon > pi/4)
    qBxCon = pi/4;
  elseif(qBxCon < -pi/4)
    qBxCon = -pi/4;
  end
  
%   qBxCon = -1*( (yBDDtCon/denom) / ((zBDDtCon + g)/denom) );

  % DEBUG
  % Set desired pitch from yBDes
  %qBxCon = yBDes;
  
  % Calculate error in  pitch
  px4Err(3) = qBxCon - qBx;
  errorIntCurrent(3) = errorIntLast(3) + px4Err(3)*dt;

  % Calculate desired pitch rate from P controller
  qBxDtCon  = Kpth*px4Err(3);
  
  % Calculate error in  pitchrate
  px4Err(6) = qBxDtCon - qBxDt;
  errorIntCurrent(6) = errorIntLast(6) + px4Err(6)*dt;
  % Clamp integral effort
  if Kithdot*errorIntCurrent(6) > g
    %disp('clamping Pitchrate integral');
    errorIntCurrent(6) = g / Kithdot;
  elseif Kithdot*errorIntCurrent(6) < -g
    errorIntCurrent(6) = -g / Kithdot;
  end
   
  % Calculate u1 from desired vertical acceleration
  %   F = m*a ... a = z_accel + g
  u1 = mB*(zBDDtCon + g);
  
  % Calculate u2 from pitch rate PI+D controller
  %   Note that PX4 controller directly computes torque from here...this is
  %   NOT computing the desired rotation accel (e.g. alpha), in which case
  %   it would need to be multiplied by IBxx
  u2 = Kffth*qBxDtCon + Kpthdot*px4Err(6) + Kithdot*errorIntCurrent(6) - Kdthdot*qBxDDtLast;
  
  % Reset integral effort if error crosses zero
%   if abs(u2) < 0.001
%     disp('resetting integral');
%     errorIntCurrent(6) = 0;
%   end
  
  % Solve for F2 and F4 (front and back prop thrusts) to achieve u1 & u2
  X  = linsolve( [lA -lA; 1 1 ], [u2; u1] );
  f2 = X(1);
  f4 = X(2);
  control = [f2; f4];
  
  % Store the controller-computed setpoints for each state
  ctrlDes = [yBDes, zBDes, qBxCon, yBDtCon, zBDtCon, qBxDtCon, yBDDtCon, zBDDtCon];
  
  % Return error values so they can be summed for integral control
  errors = px4Err;
  
  % Only the admAcc is used
  admCtrl = [admInit, admPos0, 0, 0, admAcc];
  
elseif strcmp(controllerType, 'PID')
  %Kpid= params.controller.kPID;
  kpz = 10;
  kdz = 10;
  kiz = 0.0006;
  kpy = 10;
  kdy = 10;
  kiy = 0;
  kdq = 25;
  kpq = 25;
  kiq = 0;  

  % Control Equations
  yBDDotCon = (kdq*(yBDtDes - yBDt) + kpq*(yBDes- yB));
  qBxCon = -yBDDotCon/g;
  % phi_c_dot = 0; %near hovering
  qBxDtCon = -(kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt))/g;
  u1 = mB*(g+zBDDtDes) + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB) - kiz * errorIntCurrent(2) ;
  u2 = IBxx*(qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt));
  params.lA = 0.186;
  X =linsolve([lA -lA; 1 1 ], [u2; u1] );
  f2= X(1);
  f4 = X(2);
  control = [f2; f4] ;
  
elseif strcmp(controllerType, 'FF')
  control = planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBxDes, qBxDDtDes, yBDDtDes, zBDDtDes, g, lA ); 

elseif strcmp(controllerType, 'FF_PID')
  kpz = 100;
  kdz = 25;
  kiz = 10;
  kpy = 100;
  kdy = 25;
  kiy = 10;
  kdq = 25;
  kpq = 25;
  kiq = 0; 
  yBDDtCon = yBDDtDes + kdy*(yBDtDes - yBDt) + kpy*(yBDes - yB) - kiy * errorIntCurrent(1);
  zBDDtCon = zBDDtDes + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB) - kiz * errorIntCurrent(2) ;
  qBxCon = -yBDDtCon/g;
  qBxDtCon = -(1/g)*(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt));
  qBxDDtCon = qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt);   
  control = planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBx, qBxDDtCon, yBDDtCon, zBDDtCon, g, lA ); 
  ctrlDes = 0;
  admCtrl = [0 0 0 0 0];
else
  error('Controller mode not handled');
end 

end