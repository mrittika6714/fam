%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function planarQuadRotorThrust_animation(results, params, saveVideo)

t = results.t;
animationOutputs = results.additionalOutputs(:,1:4);
% Unpack positions
A2y= animationOutputs(:,1);
A2z= animationOutputs(:,2);
A4y= animationOutputs(:,3);
A4z= animationOutputs(:,4);
setPoint = results.posDes;
lA = params.lA;
numPoints = length(t);
maxFPS = params.visualization.maxFPS;
minFPS = params.visualization.minFPS;
animationTimeScale = params.visualization.animationTimeScale;
tLast = 0;
  stepSize = (t(end)- t(1)) / (length(t)-1); %
  frameRateRaw = round(1/stepSize);
  frameRateAccelerated = frameRateRaw * animationTimeScale;
  if frameRateAccelerated > maxFPS
    frameRate = maxFPS;
  elseif frameRateAccelerated <= minFPS
    frameRate = minFPS;
  else
    frameRate = frameRateAccelerated;
  end
  tFrame = (1/frameRate)*animationTimeScale*0.99999;
  if tFrame < stepSize 
    warning('Simulation step size is %0.4f while desired time between frames for desired time scaling is %0.4f', stepSize, tFrame)
  end
if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = 200;
  open(myVideo)
end
animationFigure = figure();
animationAxis = gca;
setPointPlot = plot(setPoint(1,1),setPoint(1,2), 'r.','markersize',25);
hold on
bodyPlot = plot([A2y(1);A2z(1)],[A4y(1);A4z(1)], 'k-', 'linewidth',3);
xMin = min( [A2y; setPoint(:,1)]) - lA*2.5;
xMax = max( [A2y; setPoint(:,1)]) + lA*2.5;
yMin = min( [A2z; setPoint(:,2)]) - lA*2.5;
yMax = max( [A2z; setPoint(:,2)]) + lA*2.5;

axis([xMin xMax yMin yMax])
xlabel('y Position [m]');
ylabel('z Position [m]');
title('Planar Quad Rotor Simulation','fontweight', 'bold')
legend('Set Point', 'VehicleBody')
for i =1:numPoints
  if (frameRateAccelerated == frameRate || t(i) == 0 || t(i)-tLast >= tFrame || t(i) == t(end))
    title(animationAxis,sprintf('Planar Quadrotor Simulation \n Sim Time: %f', t(i)))
    setPointPlot.XData = setPoint(i,1);
    setPointPlot.YData = setPoint(i,2);
    bodyPlot.XData = [A2y(i);A4y(i)];
    bodyPlot.YData = [A2z(i); A4z(i) ];
    tLast = t(i);
    drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
  end
end
if saveVideo
  close(myVideo)
end



end