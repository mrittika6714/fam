function  planarQuadRotorThrust_plot(plotData, params, savePlots)

styleVec ={'k-','r--','y-' };

desiredTrajectory = [plotData.posDes, plotData.vDes];
stateError = [plotData.states(:,1:2), plotData.states(:,4:5) ]- desiredTrajectory(:,1:4);
%px4Errors  = plotData.additionalOutputs(:,end-10:end-5);
%px4Dist    = plotData.additionalOutputs(:,end-21:end-19);
state      = plotData.states;

errorVTime = figure; 
plot(plotData.t, stateError)
title('State Error vs Time')
xlabel('Time (s)');
ylabel('Error');
legend('yB', 'zB', 'yBDot', 'zBDot')
grid on;
grid minor;

% 
% StatesVTime = figure; 
% plot(plotData.t, states(:,3))
% title('QBx vs Time')
% xlabel('Time (s)');
% ylabel('Error');
% grid on;
% grid minor;

% px4TransErrorFig = figure('Name', 'PX4 CTRL Translation Errors', 'Position', [200, 100, 1200, 800]);
% set(gca, 'FontSize', 12);
% plot(plotData.t, px4Errors(:,1:2));
% hold on;
% plot(plotData.t, px4Errors(:,4:5));
% title('PX4 Controller Translation Errors vs Time', 'FontSize', 20, 'FontWeight', 'bold');
% xlabel('Time (s)', 'FontSize', 14, 'FontWeight', 'bold');
% ylabel('Errors (m, m/s)', 'FontSize', 14, 'FontWeight', 'bold');
% legend('yB', 'zB','yBDot', 'zBDot');
% grid on;
% grid minor;
% 
% px4RotErrorFig = figure('Name', 'PX4 CTRL Rotation Errors', 'Position', [200, 100, 1200, 800]);
% set(gca, 'FontSize', 12);
% plot(plotData.t, px4Errors(:,3)*180/pi);
% hold on;
% plot(plotData.t, px4Errors(:,6)*180/pi);
% title('PX4 Controller Rotation Errors vs Time', 'FontSize', 20, 'FontWeight', 'bold');
% xlabel('Time (s)', 'FontSize', 14, 'FontWeight', 'bold');
% ylabel('Errors (deg, deg/s)', 'FontSize', 14, 'FontWeight', 'bold');
% legend('qBx', 'qBxDot');
% grid on;
% grid minor;

% px4DisturbanceFig = figure('Name', 'PX4 CTRL Disturbances', 'Position', [200, 100, 1200, 800]);
% set(gca, 'FontSize', 12);
% plot(plotData.t, px4Dist(:,1));
% hold on;
% plot(plotData.t, px4Dist(:,2));
% plot(plotData.t, px4Dist(:,3));
% title('PX4 Controller Disturbances', 'FontSize', 20, 'FontWeight', 'bold');
% xlabel('Time (s)', 'FontSize', 14, 'FontWeight', 'bold');
% ylabel('Force/Torque (N, N*m)', 'FontSize', 14, 'FontWeight', 'bold');
% legend('Fy', 'Fz', 'Tx');
% grid on;
% grid minor;

if savePlots
  saveas(errorVTime, fullfile(params.resultsFolder, [params.plotHeader, '_','errorVTime','.png'] ));
end

end