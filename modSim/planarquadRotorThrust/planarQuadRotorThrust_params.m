function params = planarQuadRotorThrust_params(simParams)

%% Store simulation Params
params.sim = simParams;
params.sim.fSolveOpts = optimset('Display','off');

%% Initial Position
y0   = -0.0;  % Initial Y position
z0   = -0.0;  % Initial Z position
yDt0 =  0.0;  % Initial Y velocity
zDt0 =  0.0;  % Initial Z velocity
% z0 = -0.05;

%% Desired hover position
% hy0  = -1*pi/180;  % Set angle to pass directly to pitch controller for debugging (Degrees)
hy0  = 0.1; 
hz0  = 0;

%% Select external force application
params.msAxes      = ""; % Select axes for ms force (Y, Z, X, YZ, etc.)
params.stepAxes    = "Y"; % Select axes for step force (Y, Z, or YZ)
params.admCtrlAxes = "Y"; % Select axis for adm ctrl (Y, or Z, CAN'T DO BOTH "YZ")

%% Model Params
m   = 5;           % Mass (kg)
g   = 9.81;        % Gravity (m/s/s)
% r   = (0.65/2) / sqrt(2);  % Radius (m)
r   = (0.65/2);  % Radius (m)
L   = 2*r;         % Length of approximated cylinder (e.g. forward/backward offset of rotors from CG)
Ixx = ((2*m/3)*L^2)/12;  % Pitch-inertia based on a simple circular rod/cylinder rotating about central diameter
Iyy = Ixx;         % Symmetric vertical-plane rotational inertia (unused in 3DOF dynamics)
Izz = 1;           % Nominal horizontal-plane (XY) inertia (unused in 3DOF dynamics)

params.IB = [Ixx, 0, 0; 0, Iyy, 0; 0, 0, Izz]; % Vehicle moments of inertia
params.mB = m;     % Vehicle mass
params.g  = g;     % gravity
params.lA = (0.65/2) / sqrt(2);  % length of arm from CoM / Radius (m)

%% Multisine force application
params.multisine = simParams.multisine;
params.ms_mag = m*g/10;
params.multisine(:,2) = params.multisine(:,2)*params.ms_mag; % Scale to force relative to vehicle mass

%% Step force application
% stepForce is array of [t, F], where 'F' is applied after 't' has elapsed
% OR  [t, K, eq, tension], where K is spring constant, eq is equilibrium point

% If a third term is appended at the end, the step force will change to a
% spring coupling about the equilbrium point 'eq', the second term will
% be used as the spring constant, and the 4th term will indicate
% compression-only spring or bi-directional...
 
params.stepForces = nan(1,4);
% params.stepForces(1,:) = [ 1,  m*g/10 ];
% params.stepForces(1,:) = [ 1,  10 ];
% params.stepForces(1,:) = [ 0,  100, 0.0, 1 ];
params.stepForces(1,:) = [ 0, 1000, 0.0, 0 ];
% params.stepForces(2,:) = [ 11, 0 ];
% params.stepForces = [ 1, m*g/10;
%                       11, 0     ];

%% Admittance Control Settings

% Set desired virtual admittance parameters
params.admM  =   8;   % Mass      (kg)
% params.admB  =  30;  % Damping   (Ns/m)
params.admK  =  10;   % Stiffness (N/m)
params.admB  = 2*sqrt(params.admK * params.admM); % Critically-damped
params.admFd =   0;   % Desired applied force (N)

%% Controller Params

% **********************************************************************
%   Define gains from PX4 params (Note X is forward in params' notation
% **********************************************************************
% Horizontal position controller (PD)
params.Kpx     = 0.95;           % MPC_XY_P
params.Kdx     = params.Kpx / 3; % Defined in custom adm controller

% Horizontal velocity controller
mult = 1;
params.Kpxdot  = 1.8*mult;         % MPC_XY_VEL_P_ACC
params.Kixdot  = 0.4*mult;            % MPC_XY_VEL_I_ACC
params.Kdxdot  = 0.2*mult;            % MPC_XY_VEL_D_ACC
% params.Kpxdot  = 0.2;         % MPC_XY_VEL_P_ACC
% params.Kixdot  = 0.01;            % MPC_XY_VEL_I_ACC
% params.Kdxdot  = 0.5;            % MPC_XY_VEL_D_ACC

% params.Kpxdot  = 0.2;         % MPC_XY_VEL_P_ACC
% params.Kixdot  = 0.01;            % MPC_XY_VEL_I_ACC
% params.Kdxdot  = 0.5;            % MPC_XY_VEL_D_ACC

% Vertical position controller (PD)
params.Kpz     = 1.00;           % MPC_Z_P
params.Kdz     = params.Kpz / 3; % Defined in custom adm controller

% Vertical velocity controller
mult = 1;
params.Kpzdot  = 4.0*mult;            % MPC_Z_VEL_P_ACC
params.Kizdot  = 2.0*mult;            % MPC_Z_VEL_I_ACC
params.Kdzdot  = 0.0*mult;            % MPC_Z_VEL_D_ACC

% Pitch position controller (P)
params.Kpth    = 6.50;           % MC_PITCH_P

% Pitchrate controller
mult = 10;
params.Kffth   = 0.0;            % MC_PITCHRATE_FF
params.Kpthdot = 0.150*mult;     % MC_PITCHRATE_P
params.Kithdot = 0.020*mult;     % MC_PITCHRATE_I
params.Kdthdot = 0.003*mult;     % MC_PITCHRATE_D
% ---------------------------------------------------------------------

params.controller.type = simParams.control.type;
params.controller.constantValue = 0;  % rad/sec
params.controller.Klqr = load('Klqr.mat');
params.controller.Klqr = params.controller.Klqr.Klqr;
params.controller.uEq = load('uEq.mat');
params.controller.uEq = params.controller.uEq.uEqVal;
params.controller.kPID = 50;
params.controller.kp = 250;
params.controller.kd = 250;
params.controller.func = simParams.control.func;

%% Plot Parms
params.markerSize = 35;
params.bodyLength = 2;
params.bodyWidth = 0.5;
params.LProp = 0.75;
params.WProp = 0.125;
params.propOffset = 1;

%% Trajectory Params
params.trajectory = planarQuadRotorThrust_trajectoryParams(params.sim.tFinal);
params.trajectory.trajectoryFunc = simParams.trajectoryFunc;

function trajectoryParams = planarQuadRotorThrust_trajectoryParams(tFinal)





% %% Circle Trajectory
% % y trajectory 
% yTrajectory = trajectory;
% yTrajectory.type = 'cos';
% %yTrajectory.wayPoints = [ states0(1); 5; 5];
% %yTrajectory.velocities = [states0(4); 0; 0];
% %yTrajectory.tVec = [0; tFinal/2; tFinal];
% %yTrajectory.accelerations = [0; 0; 0];
% yTrajectory.w = 10*pi/180;
% yTrajectory.A = 1;
% 
% % z trajectory 
% zTrajectory = trajectory;
% zTrajectory.type = 'sin';
% zTrajectory.w = 10*pi/180;
% zTrajectory.A = 1;


%% Constant Pos Trajectory
% y trajectory 
yTrajectory = trajectory;
yTrajectory.type = 'constantPos';
yTrajectory.wayPoints = [hy0;];

% z trajectory 
zTrajectory = trajectory;
zTrajectory.type = 'constantPos';
zTrajectory.wayPoints = [hz0;];

[yB0, yBDt0, yBDDt0, yBDDDt0, ~] = yTrajectory.getTrajectory(0); 
[zB0, zBDt0, zBDDt0, zBDDDt0, ~] = zTrajectory.getTrajectory(0); 

qBx0 = planarQuadRotorThrust_calcqBx( params.g, yBDDt0, zBDDt0 );
qBxDt0 = planarQuadRotorThrust_calcqBxDot( params.g, qBx0, yBDDt0, yBDDDt0, zBDDt0, zBDDDt0 );

bodyStates0 = [yB0 + y0; zB0 + z0; qBx0; yBDt0 + yDt0; zBDt0 + zDt0; qBxDt0; ];

states0 = [bodyStates0]; %; propStates0

%%
% y trajectory 
% yTrajectory = trajectory;
% yTrajectory.type = 'minJerk';
% yTrajectory.wayPoints = [ states0(1); 5; 5];
% yTrajectory.velocities = [states0(4); 0; 0];
% yTrajectory.tVec = [0; tFinal/2; tFinal];
% yTrajectory.accelerations = [0; 0; 0];
% 
% % z trajectory 
% zTrajectory = trajectory;
% zTrajectory.type = 'minJerk';
% zTrajectory.wayPoints = [ states0(2); 0; 0];
% zTrajectory.velocities = [states0(5); 0; 0];
% zTrajectory.tVec = [0; tFinal/2; tFinal];
% zTrajectory.accelerations = [0; 0; 0];



trajectoryParams.numTrajectories = 2;
trajectoryParams.ICs = states0;
trajectoryParams.trajectories{1} = yTrajectory;
trajectoryParams.trajectories{2} = zTrajectory;

end

end

