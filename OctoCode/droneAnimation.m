%% PROGRAM INFORMATION
% FILENAME: 
% DESCRIPTION: 
% INPUTS: 
% OUTPUTS: 
% AUTHOR: Michal Rittikaidachar
% REVISIION HISTORY:`REV A - x/x/x
% NOTES: 
function droneAnimation(results, params, saveVideo)



% Unpack Results 
t = results.t;
states = results.states;
numPoints = length(t);

% Unpack State Matrix
xB= states(8,:);
yB= states(9,:);
zB= states(10,:);
eulerParams = states(  1:4,:)';

% Convert Euler Parameters (quaternion) to Body Fixed ZYX Euler Angles
eulerAngles = quat2eul(eulerParams);
qBz= eulerAngles(:,1);
qBy= eulerAngles(:,2);
qBx= eulerAngles(:,3);

% Unpack Desired State/Trajectory
%stateDes = results.stateDes;

% Unpack Animation Parameters
 animationTimeScale = params.animationTimeScale;
 stepSize = (t(end)- t(1)) / (length(t)-1);
 frameRate = round(1/stepSize) * animationTimeScale;
vehicleDim = [1, 1, 1];

if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end

%% Generate Initial Plots 
animationFigure = figure('units','normalized','outerposition',[0 0 1 1]);
figure(animationFigure);
%setPointPlot = plot3(setPoint(1),setPoint(2),setPoint(3), 'r.','markersize',25);
view(45,45)
hold on
[bodyVertices, bodyFaces] =  plotCube(vehicleDim,[xB(1), yB(1), zB(1)], [qBz(1), qBy(1), qBx(1)]);
bodyPlot = patch('Vertices',bodyVertices,'Faces',bodyFaces, 'FaceColor', [0.5, 0.5, 0.5], 'facealpha', 0.25);
pathPlot = plot3(xB(1), yB(1), zB(1), 'k--', 'linewidth',1);

xMin = -12;
xMax = 12;
yMin = -12;
yMax = 12;
zMin = -12;
zMax = 12;
axis([xMin xMax yMin yMax zMin zMax]);
xlabel('x Position [m]');
ylabel('y Position [m]');
zlabel('z Position [m]');
legend('Set Point', 'VehicleBody')

%% Step through plot for animation
for i = 1:numPoints
%     setPointPlot.XData = setPoint(i,1);
%     setPointPlot.YData = setPoint(i,2);
%     setPointPlot.ZData = setPoint(i,3);
    
    bodyPlot.XData = xB(i);
    bodyPlot.YData = yB(i);
    bodyPlot.ZData = zB(i);
    [ bodyPlot.Vertices, bodyPlot.Faces] =  plotCube(vehicleDim,[xB(i), yB(i), zB(i)], [qBz(i), qBy(i), qBx(i)]);
   
    pathPlot.XData = xB(1:i);
    pathPlot.YData = yB(1:i);
    pathPlot.ZData = zB(1:i);
    
    drawnow

    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
%% Save Video and Cleanup 
if saveVideo
  close(myVideo)
end


end