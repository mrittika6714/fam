%the main program

clc
clear
close all
% RRR Parameters
L1 = 2 ;
L2 = 2;
L3 = 2;
m1 = 0.1;
m2 = 0.1;
m3 = 0.1;
W = 0.01;
g = 9.81;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Controller
kv = 40;
kp = 20;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% simulation parameters

T_f = 5; % simulation interval

AT = 1e-6; % absolute tolerance
RT = 1e-6; % relative tolerance
RF = 4; % Refine factor



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start simulation

q1 = [];
q2 =[];
q3 = [];
dq1 =[];
dq2 = [];
dq3 =[];
x = [];
y = [];
theta = [];
dx = [];
dy = [];
dtheta = [];
F1 = [];
F2 = [];
F3 = [];

sim('RRR_with_CTC_task_space_ball')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[t,yball]=BallPosition(y,dy);

%Plot the links
figure()

for i = 1:length(yball)
    
    x0 = [0 0];
    x1 = [L1*cos(q1(i)) L1*sin(q1(i))];
    x2 = x1 + [L2*cos(q1(i)+q2(i)) L2*sin(q1(i)+q2(i))];
    x3 = x2 + [L3*cos(q1(i)+q2(i)+q3(i)) L3*sin(q1(i)+q2(i)+q3(i))];
   
    clf
    
    plot([x0(1) x1(1)], [x0(2) x1(2)], 'r-');
    hold on
    plot([x1(1) x2(1)], [x1(2) x2(2)], 'g-');
    plot([x2(1) x3(1)], [x2(2) x3(2)], 'b-');
    if(~isinf(yball(i)))
        hold on
       plot(xd(i)-0.8,yball(i),'o','LineWidth',4)
    end
    xlim([-1 4])
    ylim([-1 4])
    axis equal
    drawnow 
end
figure
plot(t,yball,'r')
hold on
plot(t,y(1:length(t)))
