function FK = Forward_kinematics(z)
%this function calculated the forward kinematics of the robot


q1 = z(1);
q2 = z(2);
q3 = z(3);
dq1 = z(4);
dq2 = z(5);
dq3 = z(6);
L1 = z(7);
L2 = z(8);
L3 = z(9);

    
X1 = L1*cos(q1)+L2*cos(q1+q2)+L3*cos(q1+q2+q3);
X2 = L1*sin(q1)+L2*sin(q1+q2)+L3*sin(q1+q2+q3);
X3 = q1 + q2 + q3;
X = [X1;X2;X3];

x = [q1;q2;q3;L1;L2;L3];
J = Jacobi(x);

dq = [dq1;dq2;dq3];
dX1 = J(1,1:3)*dq;
dX2 = J(2,1:3)*dq;
dX3 = J(3,1:3)*dq;

dX = [dX1;dX2;dX3];

FK = [X;dX];
end

