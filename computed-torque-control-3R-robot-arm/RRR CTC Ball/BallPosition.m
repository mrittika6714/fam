
function [t,h]=BallPosition(y,dy)
% initial heigh and velocity of the ball
x0 = [8 -5];
tspan = 0:0.1:10;
[t,x] = ode45(@ball,tspan,x0);
% coefficient of restitution
alpha = 0.8;
h = x(:,1);
v = x(:,2);
t(1) = 0;
dt = 0.01;
g = 9.81;
a = -g;
i = 1;
% 5 is the number of times ball hits the paddle
for n = 1:5
while h(i) > y(i)
    t(i+1) = t(i) + dt;
    v(i+1)=v(i)+a*dt;
    h(i+1)=h(i)+v(i)*dt;
    i=i+1;
end
i = i-1;
v(i) = alpha*(dy(i)-v(i))+dy(i);
end
  
end







