% Note not all trajectories currenlty work for vector inputs

classdef trajectory
   properties
      type = 'test'
      tVec = [0; 10];
      wayPoints = [ 0; 10];
      velocities = [0; 0];
      accelerations = [0; 0];
      func = @obj.test
      tStep = 0.001;
      t = 0;
      w = 1;
      A = 1000;
      a = 1;
      aMax = 1;
      vMax = 1;
      ratioMaxAccel = 1;
   end
   methods
      function [r, rDot, rDDot, rDot3, rDot4] = getTrajectory(obj,t)
        if strcmp( obj.type, 'test')
           r = obj.test( t, 100);
           rDot = obj.test( t, 100);
           rDDot = obj.test( t, 100);
           rDot3 = obj.test( t, 100);
           rDot4 = obj.test( t, 100);
        elseif strcmp( obj.type, 'sin')
          [r, rDot, rDDot, rDot3, rDot4] = obj.sinTrajectory(t);
        elseif strcmp( obj.type, 'cos')
          [r, rDot, rDDot, rDot3, rDot4] = obj.cosTrajectory(t);
        elseif strcmp( obj.type, 'minJerk')
          [r, rDot, rDDot, rDot3, rDot4] = obj.minJerkTrajectory(t);
        elseif strcmp( obj.type, 'constantPos')
          [r, rDot, rDDot, rDot3, rDot4] = obj.constantPosTrajectory(t);
        elseif strcmp( obj.type, 'constantVel')
          [r, rDot, rDDot, rDot3, rDot4] = obj.constantVelTrajectory(t);
        elseif strcmp( obj.type, 'triangle')
          [r, rDot, rDDot, rDot3, rDot4] = obj.triangleTrajectory(t);  
        elseif strcmp( obj.type, 'trapezoidal')
          [r, rDot, rDDot, rDot3, rDot4] = obj.trapezoidalTrajectory(t); 
        else 
          r = 0;
        end
      end
      
      function setTrajectoryFunction(obj)
        func = obj.sineTrajectory(t);
       
      end
   end
   
  methods (Access = protected)
    function r = test(obj,t, v)
      r = v;
    end
    
    function [r, rDot, rDDot,  rDot3, rDot4] = constantPosTrajectory(obj,t)
      % returns the trajectories for r = A*sin(omega*t)
      r = obj.wayPoints * ones(length(t),1);
      rDot = zeros(length(t),1);
      rDDot = zeros(length(t),1); 
      rDot3 = zeros(length(t),1); 
      rDot4 = zeros(length(t),1); 
    end
    
    function [r, rDot, rDDot,  rDot3, rDot4] = constantVelTrajectory(obj,t)
      r = obj.wayPoints * ones(length(t),1);
      rDot = zeros(length(t),1);
      rDDot = zeros(length(t),1); 
      rDot3 = zeros(length(t),1); 
      rDot4 = zeros(length(t),1); 
    end
    
    function [r, rDot, rDDot, rDot3, rDot4] = triangleTrajectory(obj,t)
        numPoints = length(t);
        % determine which movement each point belongs to
        trajSections = discretize(t, obj.tVec,'includedEdge','Right');
        if numPoints == 1
            [r, rDot, rDDot, rDot3, rDot4] = trianglePoint(obj, t, trajSections);
        else
            r = zeros(numPoints,1);
            rDot = r;
            rDDot = r;
            rDot3 = r;
            rDot4 = r;
            t = reshape(t,numPoints,1);
            for i = 1:numPoints
                [r(i), rDot(i), rDDot(i), rDot3(i), rDot4(i)] = trianglePoint(obj, t(i), trajSections(i));
            end

        end
    end

        function [r, rDot, rDDot, rDot3, rDot4] = trapezoidalTrajectory(obj,t)
        numPoints = length(t);
        % determine which movement each point belongs to
        trajSections = discretize(t, obj.tVec,'includedEdge','Right');
        if numPoints == 1
            [r, rDot, rDDot, rDot3, rDot4] = trapezoidalPoint(obj, t, trajSections);
        else
            r = zeros(numPoints,1);
            rDot = r;
            rDDot = r;
            rDot3 = r;
            rDot4 = r;
            t = reshape(t,numPoints,1);
            for i = 1:numPoints
                [r(i), rDot(i), rDDot(i), rDot3(i), rDot4(i)] = trapezoidalPoint(obj, t(i), trajSections(i));
            end

        end
        end


    
    function [r, rDot, rDDot, rDot3, rDot4] = sinTrajectory(obj,t)
      % returns the trajectories for r = A*sin(omega*t)
      r = obj.A*sin(obj.w.*t);
      rDot = obj.A*cos(obj.w.*t).*obj.w;
      rDDot = -obj.A*sin(obj.w.*t).*(obj.w).^2;
      rDot3 = -obj.A*cos(obj.w.*t).*(obj.w).^3;
      rDot4 = obj.A*sin(obj.w.*t).*(obj.w).^4;
      r = r';
      rDot = rDot';
      rDDot = rDDot';
      rDot3 = rDot3';
      rDot4 = rDot4';
    end

    function [r, rDot, rDDot, rDot3, rDot4] = cosTrajectory(obj,t)
      % returns the trajectories for r = A*cos(omega*t)
      r = obj.A*cos(obj.w.*t);
      rDot = -obj.A*sin(obj.w.*t).*obj.w;
      rDDot = -obj.A*cos(obj.w.*t).*(obj.w).^2; 
      rDot3 = obj.A*sin(obj.w.*t).*(obj.w).^3;
      rDot4 = obj.A*cos(obj.w.*t).*(obj.w).^4;
      r = r';
      rDot = rDot';
      rDDot = rDDot';
      rDot3 = rDot3';
      rDot4 = rDot4';
    end
    
    function [r, rDot, rDDot, rDot3, rDot4] = minJerkTrajectory(obj,t)
      numPoints = length(t);
      if numPoints == 1  % short circuit expensive "unique" operation below
        idx = discretize(t, obj.tVec);
        if isnan(idx)
          r = nan;%(t < obj.tVec(1))*obj.wayPoints(1,:)' + (t > obj.tVec(end))*obj.wayPoints(end,:)';
          rDot = nan;%obj.velocities(end);
          rDDot = nan;%obj.accelerations(end);
          rDot3 = nan;
          rDot4 = nan;
        else
          D = obj.tVec(idx+1) - obj.tVec(idx);
          T = (t-obj.tVec(idx))/D;
          rPoint = [obj.wayPoints(idx,:); obj.wayPoints(idx+1,:)];
          rDotPoint = [obj.velocities(idx,:); obj.velocities(idx+1,:)];
          rDDotPoint =[obj.accelerations(idx,:); obj.accelerations(idx+1,:)];
          [r, rDot, rDDot, rDot3, rDot4] = obj.min_jerk(rPoint, rDotPoint, rDDotPoint, T, D);
          
      end
  else
      xVec = zeros(size(obj.wayPoints,2), numPoints);
      vVec = zeros(size(obj.wayPoints,2), numPoints);
      aVec = zeros(size(obj.wayPoints,2), numPoints);
      rDot3Vec = zeros(size(obj.wayPoints,2), numPoints);
      rDot4Vec = zeros(size(obj.wayPoints,2), numPoints);
      t = reshape(t,numPoints,1);
      index = discretize(t, obj.tVec); % determine which movement each point belongs to
      uindex = unique(index(~isnan(index))); % detemine number of unique movements
      for ii = 1:length(uindex)  % iterate through each unique movement
          idx = uindex(ii);      % determine which movement we are in
          mask = index == idx;   % create mask for points that correspond to this movment
          time = t(mask);        % get the points in time corresponding to this movement
          D = obj.tVec(idx+1) - obj.tVec(idx); 
          T = (time-obj.tVec(idx))./D;
          xPoint = [obj.wayPoints(idx,:); obj.wayPoints(idx+1,:)];
          vPoint = [obj.velocities(idx,:); obj.velocities(idx+1,:)];
          aPoint =[obj.accelerations(idx,:); obj.accelerations(idx+1,:)];
          [x, v, a, rDot3, rDot4] = obj.min_jerk(xPoint, vPoint, aPoint, T, D);
          xVec(:,mask) = x';
          vVec(:,mask) = v';
          aVec(:,mask) = a';
          rDot3Vec(:,mask) = rDot3';
          rDot4Vec(:,mask) = rDot4';
      end

      if any(t<obj.tVec(1))
          xVec(:,t<obj.tVec(1)) = obj.wayPoints(1,:)';
          vVec(:,t<obj.tVec(1)) = 0;
          aVec(:,t<obj.tVec(1)) = 0;
          rDot3Vec(:,t<obj.tVec(1)) = 0;
          rDot4Vec(:,t<obj.tVec(1)) = 0;
      end
      if any(t>obj.tVec(end))
          xVec(:,t>obj.tVec(end)) = nan; % obj.wayPoints(end,:)';
          vVec(:,t>obj.tVec(end)) = nan;
          aVec(:,t>obj.tVec(end)) = nan;
          rDot3Vec(:,t>obj.tVec(end)) = nan;
          rDot4Vec(:,t>obj.tVec(end)) = nan;
      end
      
      r = xVec';
      rDot = vVec';
      rDDot = aVec';
      rDot3 = rDot3Vec';
      rDot4 = rDot4Vec';
    end
    end
    
    function [x, v, a, rDot3, rDot4] = min_jerk(obj, x, v, a, T, D)
    numSteps  = length(T);
    xi = x(1,:);
    xf = x(2,:);
    vi = v(1,:);
    vf= v(2,:);
    ai = a(1,:);
    af = a(2,:);
    a0 = repmat(xi, numSteps, 1);
    a1 = repmat(D*vi, numSteps, 1);
    a2 = repmat((D^2*ai)/2, numSteps, 1);
    a3 = repmat(10*xf - 10*xi - 4*D*vf - 6*D*vi + (D^2*af)/2 - (3*D^2*ai)/2,  numSteps, 1);
    a4 = repmat(15*xi - 15*xf + 7*D*vf + 8*D*vi - D^2*af + (3*D^2*ai)/2, numSteps, 1);
    a5 = repmat(6*xf - 6*xi - 3*D*vf - 3*D*vi + (D^2*af)/2 - (D^2*ai)/2, numSteps, 1);
    x = (a0 +a1.*T+a2.*T.^2+a3.*T.^3+a4.*T.^4+a5.*T.^5);
    v = ((a1 + 2*a2.*T + 3*a3.*T.^2 + 4*a4.*T.^3 + 5*a5.*T.^4)/D);
    a =((2*a2 + 6*a3.*T + 12*a4.*T.^2 + 20*a5.*T.^3)/D^2);
    rDot3 =((6*a3 + 24*a4.*T + 60*a5.*T.^2)/D^3);
    rDot4 = ((24*a4 + 120*a5.*T)/D^4);
    end
  
    
    function [r, rDot, rDDot, rDot3, rDot4] = trianglePoint(obj, t, trajSections)
        tStart = obj.tVec(trajSections);
        tFinal = obj.tVec(trajSections+1);
        r0 = obj.wayPoints(trajSections);
        rTravel = obj.wayPoints(trajSections+1) - obj.wayPoints(trajSections);
        tTravel = tFinal - tStart; 
        tRel = t - tStart;
        rDot0 = 0;
        rDDot0 = 0;
        obj.a = 4*rTravel/tTravel.^2;
        if ((tRel>=0) && (tRel<= tTravel/2))
            rDDot = obj.a;
            rDot = obj.a*tRel+rDot0;
            r = 0.5*obj.a*tRel^2 + rDot0*tRel + r0;
        elseif (tRel > tTravel/2) && (tRel<tTravel)
            rDDot = -obj.a;
            rDot = -obj.a*t+obj.a*tFinal +rDot0;
            r = 0.5*obj.a*(tTravel/2)^2 + rDot0*tTravel -0.5*obj.a*tRel^2+obj.a*tTravel*tRel +0.5*obj.a*(tTravel/2)^2-obj.a*tTravel^2/2 + r0;

        elseif tRel == tTravel
            r = obj.a*(tTravel/2)^2 + rDot0*(tTravel) + r0;
            rDot = 0;
            rDDot = -obj.a;
        else 
            error('trajectory: Case not handeled in triangleTrajectory. t = %f', t)
        end
          r = r';
          rDot = rDot';
          rDDot = rDDot';
          rDot3 = 0;
          rDot4 = 0;
    end
    
    function [r, rDot, rDDot, rDot3, rDot4] = trapezoidalPoint(obj, t, trajSections)
        tStart = obj.tVec(trajSections);
        tFinal = obj.tVec(trajSections+1);
        r0 = obj.wayPoints(trajSections);
        rTravel = obj.wayPoints(trajSections+1) - obj.wayPoints(trajSections);
        tTravel = tFinal - tStart; 
        tRel = t - tStart;
        rDot0 = 0;
        rDDot0 = 0;
        rDotAvg = rTravel/tTravel;
        
        
        
        accelDesired = obj.aMax * obj.ratioMaxAccel;
        %Evaluate Conditions at max acceleration (approimately rectangular profile)
       if abs(rTravel) > 0
            tAccel = roots([accelDesired, -accelDesired*tTravel, rTravel]);
            tAccel(tAccel<=0) = inf;
            tAccel = min(tAccel);
        else
            tAccel = 0;
        end
        vMinReq = tAccel * accelDesired * sign(rTravel);

        
        % Evaluate conditions at min feasible accel (triangular profile)
        accelMinReq = 4*rTravel/tTravel^2;
        vMaxReq = 2 * rTravel/tTravel;

        accel = accelDesired*sign(rTravel) ;
        if obj.aMax < accelMinReq 
            error('Trapezoidal trajectory Infeasible. Min required acceleration is: %f, %f Percent of curent accelMax', accelMinReq, 100*accelMinReq/obj.aMax);
        elseif obj.vMax < vMinReq 
            error('Trapezoidal trajectory Infeasible. Min required velociy at current maxAccel is: %f', vMinReq);
        elseif (obj.aMax == accelMinReq && obj.vMax < vMaxReq)
            error('Trapezoidal trajectory Infeasible. Max required velocity is: %f', vMaxReq);  
        elseif (accelDesired < accelMinReq )
                accel = accelMinReq;
                warning('Desired acceleration rate is to small setting acceleration to minimum required %f. Try increasing accel ratio', accelMinReq);
        end
        
        
        if abs(rTravel) > 0
            tAccel = roots([accel, -accel*tTravel, rTravel]);
            tAccel(tAccel<=0) = inf;
            tAccel = min(tAccel);
        else
            tAccel = 0;
        end
        if ((tRel>=0) && (tRel<= tAccel)) % Acceleration Phase
            rDDot = accel;
            rDot = accel*tRel+rDot0;
            r = 0.5*accel*tRel^2 + rDot0*tRel + r0;
        elseif (tRel > tAccel) && (tRel <=  (tTravel-tAccel)) % Constnat Velocity Phase
            rDDot = 0;
            rDot = tAccel*accel;
            r = accel*tAccel*(tRel-tAccel) + 0.5*accel*tAccel^2 + rDot0*tRel + r0;
        elseif (tRel > (tTravel-tAccel)) && (tRel<tTravel) % Negaive Acceleration Phase
            rDDot = -accel;
            rDot = rDot0 + accel*tAccel - accel*(tRel- tTravel+ tAccel) ;
            r =  r0 + accel*tAccel*(tTravel - tAccel) - 0.5*accel*tAccel^2  + accel*tAccel*(tRel - tTravel + tAccel ) - 0.5*accel*(tRel - tTravel + tAccel)^2;

        elseif tRel == tTravel  %End of Trajectory 
            r = r0 + accel * tAccel*tTravel - accel * tAccel^2;
            rDot = rDot0;
            rDDot = -accel;
        else 
            error('trajectory: Case not handeled in trapezoidalTrajectory. t = %f', t)
        end
          r = r';
          rDot = rDot';
          rDDot = rDDot';
          rDot3 = 0;
          rDot4 = 0;




    end
    
  end
     
end