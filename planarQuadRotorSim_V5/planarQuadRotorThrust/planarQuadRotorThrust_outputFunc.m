function status = planarQuadRotorThrust_outputFunc(t,y,flag,params) 

if strcmp(flag, 'init')
    
elseif strcmp(flag, 'done')
   
else
    trajectories = params.trajectory;
    trajectoryFunc = params.trajectory.trajectoryFunc;
    u = @(t, states)  params.controller.func(t, states, trajectories, trajectoryFunc, params);
    for i = 1:length(t)
        [statesDot, ~, errors ]= planarQuadRotorThrust_dynamics( t(i), y(:,i), params, u);
        planarQuadRotorThrust_histFunc('add', t, statesDot', errors);
         
     end
end
displayStatus_dots(t,y,flag); 
status = 0;
end



