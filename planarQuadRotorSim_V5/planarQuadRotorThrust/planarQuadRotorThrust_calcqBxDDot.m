function qBxDDot = planarQuadRotorThrust_calcqBxDDot( g, qBx, qBxDt, yBDDt, yBDDDt, yBDDDDt, zBDDt, zBDDDt, zBDDDDt )
if( nargin ~= 9 ) error( 'planarQuadRotorThrust_calcqBxDDot expects 9 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_calcqBxDDot.m created Jan 06 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
qBxDDot = zeros( 1, 1 );




%===========================================================================


%===========================================================================
Output = [];

qBxDDot(1) = -cos(qBx)^2*(2*sin(qBx)*qBxDt^2/cos(qBx)^3+(2*yBDDt*zBDDDt^2+(g+zBDDt)^2*yBDDDDt-2*(g+zBDDt)*yBDDDt*zBDDDt-yBDDt*(g+  ...
zBDDt)*zBDDDDt)/(g+zBDDt)^3);


%==========================================================
end    % End of function planarQuadRotorThrust_calcqBxDDot
%==========================================================
