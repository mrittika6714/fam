function integrationOutputs = planarQuadRotorThrust_MGdynamics2( FB, TB, qBx, yBDt, zBDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, g )
if( nargin ~= 14 ) error( 'planarQuadRotorThrust_MGdynamics2 expects 14 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_MGdynamics2.m created Aug 30 2022 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
integrationOutputs = zeros( 1, 6 );




%===========================================================================
COEF = zeros( 3, 3 );
COEF(1,1) = mB;
COEF(2,2) = mB;
COEF(3,3) = IBxx;
RHS = zeros( 1, 3 );
RHS(1) = -FB*sin(qBx);
RHS(2) = FB*cos(qBx) - g*mB;
RHS(3) = TB;
SolutionToAlgebraicEquations = COEF \ transpose(RHS);

% Update variables after uncoupling equations
yBDDt = SolutionToAlgebraicEquations(1);
zBDDt = SolutionToAlgebraicEquations(2);
qBxDDt = SolutionToAlgebraicEquations(3);



%===========================================================================
Output = [];

integrationOutputs(1,1) = yBDt;
integrationOutputs(1,2) = zBDt;
integrationOutputs(1,3) = qBxDt;
integrationOutputs(1,4) = yBDDt;
integrationOutputs(1,5) = zBDDt;
integrationOutputs(1,6) = qBxDDt;


%==========================================================
end    % End of function planarQuadRotorThrust_MGdynamics2
%==========================================================
