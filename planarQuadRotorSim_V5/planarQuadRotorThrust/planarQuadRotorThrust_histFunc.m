function [tOut, stateDotOut, errorOut, errorIntOut] = planarQuadRotorThrust_histFunc(operation, t, stateDot, error)
persistent stateDotHist tHist errorHist errorIntHist
if isempty(stateDotHist)
    numStates = 6;
    stateDotHist= zeros(1,numStates);
    tHist = 0;
    errorHist = zeros(1,numStates);
    errorIntHist = zeros(1,numStates);
end

if strcmp(operation, 'add')
    stateDotHist = [stateDotHist; stateDot];
    stateDotOut = stateDotHist;
    tHist = [tHist; t'];
    tOut = tHist;
    errorHist = [ errorHist; error];
    errorOut = errorHist;
    dt = tHist(end) - tHist(end-1);
    errorIntHist = [errorIntHist; errorIntHist(end,:)+error.*dt];
    errorIntOut = errorIntHist;
elseif strcmp(operation, 'get')
    stateDotOut = stateDotHist;
    tOut = tHist;
    errorIntOut = errorIntHist;
    errorOut = errorHist;
elseif strcmp(operation, 'clear')
    clear stateDotHist tHist
    stateDotOut = [];
    tOut = [];
    errorIntOut = [];
    errorOut = [];
elseif strcmp(operation,'getLast')
    stateDotOut = stateDotHist(end,:);
    tOut = tHist(end);
    errorIntOut = errorIntHist(end,:);
    errorOut = errorHist(end,:);
else
    error('Case not Handeled');
end

end
