%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function planarQuadRotorThrust_animation(results, params, saveVideo)

states = results.states;
t = results.t;
animationOutputs = results.additionalOutputs(:,1:4);
animationTimeScale = params.animationTimeScale;
stepSize = (t(end)- t(1)) / (length(t)-1);
frameRate = round(1/stepSize) * animationTimeScale;


% Unpack positions
A2y= animationOutputs(:,1);
A2z= animationOutputs(:,2);
A4y= animationOutputs(:,3);
A4z= animationOutputs(:,4);
setPoint = results.posDes;
lA = params.lA;
numPoints = length(t);

if saveVideo
  videoName = params.videoName;
  myVideo = VideoWriter(videoName);
  myVideo.FrameRate = frameRate;
  open(myVideo)
end
animationFigure = figure();
animationAxis = gca;
setPointPlot = plot(setPoint(1,1),setPoint(1,2), 'r.','markersize',25);
hold on
bodyPlot = plot([A2y(1);A2z(1)],[A4y(1);A4z(1)], 'k-', 'linewidth',3);
xMin = min( [A2y; setPoint(:,1)]) - lA*2.5;
xMax = max( [A2y; setPoint(:,1)]) + lA*2.5;
yMin = min( [A2z; setPoint(:,2)]) - lA*2.5;
yMax = max( [A2z; setPoint(:,2)]) + lA*2.5;

axis([xMin xMax yMin yMax])
xlabel('y Position [m]');
ylabel('z Position [m]');
title('Planar Quad Rotor Simulation','fontweight', 'bold')
legend('Set Point', 'VehicleBody')
for i =1:numPoints
    title(animationAxis,sprintf('Planar Quadrotor Simulation \nCurrent Sim Time: %f', t(i)))
    setPointPlot.XData = setPoint(i,1);
    setPointPlot.YData = setPoint(i,2);
    bodyPlot.XData = [A2y(i); A4y(i)];
    bodyPlot.YData = [A2z(i); A4z(i) ];
    drawnow
    if saveVideo
      frame = getframe(animationFigure);
      writeVideo(myVideo,frame);
    end
end
if saveVideo
  close(myVideo)
end



end