function  planarQuadRotorThrust_plot(plotData, params, savePlots)

styleVec ={'k-','r--','y-' };

desiredTrajectory = [plotData.posDes, plotData.vDes];
stateError = [plotData.states(:,1:2), plotData.states(:,4:5) ]- desiredTrajectory(:,1:4);

errorVTime = figure; 
plot(plotData.t, stateError)
title('State Error vs Time')
xlabel('Time (s)');
ylabel('Error');
legend('yB', 'zB', 'yBDot', 'zBDot')


if savePlots
  saveas(errorVTime, fullfile(params.resultsFolder, [params.plotHeader, '_','errorVTime','.png'] ));
end

end