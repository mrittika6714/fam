function [posDes, vDes, aDes,rDot3, rDot4] = planarQuadRotorThrust_trajGen(trajectories, t)

%% Generate Trajectories


posDes = zeros(length(t), trajectories.numTrajectories);
vDes = zeros(length(t), trajectories.numTrajectories);
aDes = zeros(length(t), trajectories.numTrajectories);
rDot3 = zeros(length(t), trajectories.numTrajectories);
rDot4 = zeros(length(t), trajectories.numTrajectories);
for i = 1:trajectories.numTrajectories
  [posDes(:,i), vDes(:,i), aDes(:,i), rDot3(:,i), rDot4(:,i)] = trajectories.trajectories{i}.getTrajectory(t);
end

end