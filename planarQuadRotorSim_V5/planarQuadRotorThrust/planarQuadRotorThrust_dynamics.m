function [statesDot, additonalOutputs, errors] = planarQuadRotorThrust_dynamics(t, states, params, inputFunc)
[u, errors] = inputFunc(t, states);
F2 = u(1);
F4 = u(2);

% Unpack Params
mB = params.mB;
IBxx = params.IB(1,1);
IByy = params.IB(2,2);
IBzz = params.IB(3,3);
IBxy = params.IB(1,2);
IByz = params.IB(2,3);
IBzx = params.IB(3,1);
lA = params.lA;
g = params.g;

% Unpack States
y = states(1);
z = states(2);
qBx = states(3);
yBDt = states(4);
zBDt = states(5); 
qBxDt = states(6);

FBy = 0.1; 
FBz = 0.1;
TBx = 0;

[~, integrationOutputs ] = planarQuadRotorThrust_MGdynamics( F2, F4, FBy, FBz, TBx, qBx, yBDt, zBDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, g, lA );
%integrationOutputs = planarQuadRotorThrust_MGdynamics2( FB, TB, qBx, yBDt, zBDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, g );

AnimationOutput = planarQuadRotorThrustAnimationOutputs( lA, qBx, y, z );
inputForces = u;

statesDot = [integrationOutputs'];

additonalOutputs = [AnimationOutput, inputForces', FBy, FBz, TBx, errors ];
end