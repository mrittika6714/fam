function [control, errors]= planarQuadRotorThrust_controller(t, states, trajectories, trajectoryFunc, params)
%Unpack Params
IBxx = params.IB(1,1);
mB = params.mB;
lA = params.lA;
g = params.g;

% Unpack States
yB = states(1);
zB = states(2);
qBx = states(3);
yBDt = states(4);
zBDt = states(5);
qBxDt = states(6);

% Grab StateDot at last timestep
[tLast, stateDottLast, errorsLast, errorIntLast] = planarQuadRotorThrust_histFunc('getLast', 0, 0);
yBDtLast = stateDottLast(1);
zBDtLast = stateDottLast(2);
qBxDtLast = stateDottLast(3);
yBDDtLast = stateDottLast(4);
zBDDtLast = stateDottLast(5);
qBxDDtLast = stateDottLast(6);

%fprintf('Current time: %f, zBDt: %10.10f, zBDtLast: %10.10f \n', t, zBDt, zBDtLast)
% Unpack Desired States
[rDes, rDotDes, rDDotDes, rDDDotDes, rDDDDotDes] = trajectoryFunc(trajectories, t);
yBDes = rDes(1);
yBDtDes = rDotDes(1);
yBDDtDes = rDDotDes(1);
yBDDDtDes = rDDDotDes(1);
yBDDDDtDes = rDDDDotDes(1);
zBDes = rDes(2);
zBDtDes = rDotDes(2);
zBDDtDes = rDDotDes(2);
zBDDDtDes = rDDDotDes(2);
zBDDDDtDes  = rDDDDotDes(2);

% Calculate Desired qBx utilizing differential Flatness
qBxDes = planarQuadRotorThrust_calcqBx( g, yBDDtDes, zBDDtDes );
qBxDtDes = planarQuadRotorThrust_calcqBxDot( g, qBxDes, yBDDtDes, yBDDDtDes, zBDDtDes, zBDDDtDes );
qBxDDtDes = planarQuadRotorThrust_calcqBxDDot( g, qBxDes, qBxDtDes, yBDDtDes, yBDDDtDes, yBDDDDtDes, zBDDtDes, zBDDDtDes, zBDDDDtDes );
    
controllerType = params.controller.type;
error = [yB - yBDes, zB-zBDes, qBx- qBxDes];
errorDot = [yBDt - yBDtDes, zBDt - zBDtDes, qBxDt -  qBxDtDes];
errors = [error, errorDot];

dt = t - tLast;
errorIntCurrent = errorIntLast + errors.*dt;
if strcmp(controllerType, 'none')
    control = zeros(length(states), 1);
    
elseif strcmp(controllerType, 'constant')
   control = params.controller.uEq;

elseif strcmp(controllerType, 'sin')
   control = ones(lengthStates, 1) * sin(t);
   
elseif strcmp(controllerType, 'LQR')
  setPoint = [ yBDes; zBDes; qBxDes ; yBDtDes; zBDtDes; qBxDtDes ];
  Klqr = params.controller.Klqr ;
  control = -Klqr*(states-setPoint) + params.controller.uEq;

elseif strcmp(controllerType, 'test')
  kpz = 10;
  kdz = 10;
  kiz = 0.0006;
  kpy = 100;
  kdy = 100;
  kiy = 0;
  kdq = 25;
  kpq = 25;
  kiq = 0; 
  planarQuadRotorThrust_calcqBxDDot( g, qBxCon, qBxDtCon, yBDDtCon, yBDDDtDes, yBDDDDtDes, zBDDtCon, zBDDDtDes, zBDDDDtDes )
  yBDDtCon = yBDDtDes + kdy*(yBDtDes - yBDt) + kpy*(yBDes - yB);
  zBDDtCon = zBDDtDes + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB);
  qBxCon = planarQuadRotorThrust_calcqBx( g, yBDDtCon, zBDDtCon );
  qBxDtCon = -(1/g)*(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt));
  qBxDDtCon = qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt);
  control= planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBx, qBxDDtCon, yBDDtCon, zBDDtCon, g, lA ); 
elseif strcmp(controllerType, 'PX4')
  f2= 0;
  f4 = 0;
  [~, integrationOutputs ] = planarQuadRotorThrust_MGdynamics( F2, F4, FBy, FBz, TBx, qBx, yBDt, zBDt, qBxDt, mB, IBxx, IByy, IBzz, IBxy, IByz, IBzx, g, lA );
  errors = px4Err;
  control = [f2; f4] ;
elseif strcmp(controllerType, 'PID')
  %Kpid= params.controller.kPID;
  kpz = 10;
  kdz = 10;
  kiz = 0.0006;
  kpy = 10;
  kdy = 10;
  kiy = 0;
  kdq = 25;
  kpq = 25;
  kiq = 0;  

  % Control Equations
  yBDDotCon = (kdq*(yBDtDes - yBDt) + kpq*(yBDes- yB));
  qBxCon = -yBDDotCon/g;
  % phi_c_dot = 0; %near hovering
  qBxDtCon = -(kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt))/g;
  u1 = mB*(g+zBDDtDes) + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB) - kiz * errorIntCurrent(2) ;
  u2 = IBxx*(qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt));
  params.lA = 0.186;
  X =linsolve([lA -lA; 1 1 ], [u2; u1] );
  f2= X(1);
  f4 = X(2);
  control = [f2; f4] ;
  
elseif strcmp(controllerType, 'FF')
  control = planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBxDes, qBxDDtDes, yBDDtDes, zBDDtDes, g, lA ); 

elseif strcmp(controllerType, 'FF_PID')
  kpz = 100;
  kdz = 25;
  kiz = 10;
  kpy = 100;
  kdy = 25;
  kiy = 10;
  kdq = 25;
  kpq = 25;
  kiq = 0; 
  yBDDtCon = yBDDtDes + kdy*(yBDtDes - yBDt) + kpy*(yBDes - yB) - kiy * errorIntCurrent(1);
  zBDDtCon = zBDDtDes + kdz*(zBDtDes - zBDt) + kpz*(zBDes - zB) - kiz * errorIntCurrent(2) ;
  qBxCon = -yBDDtCon/g;
  qBxDtCon = -(1/g)*(yBDDDtDes + kdq*(yBDDtDes + g*qBx) + kpq*(yBDtDes - yBDt));
  qBxDDtCon = qBxDDtDes + kpq*(qBxCon-qBx)+ kdq*(qBxDtCon-qBxDt);   
  control= planarQuadRotorThrust_MGInverseDynamics( IBxx, mB, qBx, qBxDDtCon, yBDDtCon, zBDDtCon, g, lA ); 
  

else
  error('Controller mode not handled');
end 

end