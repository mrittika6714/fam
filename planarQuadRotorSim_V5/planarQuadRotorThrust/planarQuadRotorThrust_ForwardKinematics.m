function [taskPos,taskVel] =planarQuadRotorThrust_ForwardKinematics( yB, yBDt, yBDDt, zB, zBDt, zBDDt )
if( nargin ~= 6 ) error( 'planarQuadRotorThrust_ForwardKinematics expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_ForwardKinematics.m created Aug 28 2022 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================
taskPos = zeros( 2, 1 );
taskVel = zeros( 2, 1 );
taskAccel = zeros( 2, 1 );





%===========================================================================


%===========================================================================
Output = [];

taskPos(1) = yB;
taskPos(2) = zB;

taskVel(1) = yBDt;
taskVel(2) = zBDt;

taskAccel(1) = yBDDt;
taskAccel(2) = zBDDt;


%================================================================
end    % End of function planarQuadRotorThrust_ForwardKinematics
%================================================================
