function qBxDot = planarQuadRotorThrust_calcqBxDot( g, qBx, yBDDt, yBDDDt, zBDDt, zBDDDt )
if( nargin ~= 6 ) error( 'planarQuadRotorThrust_calcqBxDot expects 6 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorThrust_calcqBxDot.m created Jan 06 2023 by MotionGenesis 6.1.
% Portions copyright (c) 2009-2021 Motion Genesis LLC. are licensed under
% the 3-clause BSD license.  https://opensource.org/licenses/BSD-3-Clause.
% This copyright notice must appear in all copies and distributions.
% MotionGenesis Professional Licensee: Michal Rittikaidachar.
%===========================================================================
qBxDot = zeros( 1, 1 );




%===========================================================================


%===========================================================================
Output = [];

qBxDot(1) = cos(qBx)^2*(yBDDt*zBDDDt-(g+zBDDt)*yBDDDt)/(g+zBDDt)^2;


%=========================================================
end    % End of function planarQuadRotorThrust_calcqBxDot
%=========================================================
